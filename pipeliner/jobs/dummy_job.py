import os
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.data_structure import Node


class DummyJob(PipelinerJob):
    """A dummy pipeliner job that is used to get the universal job options

    This is currently only used for creating a job.star file in the older
    relion-compatible format.  Pipeliner jobs do not include running options
    which they do not use (MPI, threads, & etc...) Relion job.star files always
    include all of these options even of not needed.  The dummyjob allows the
    addition of these joboptions to pipeliner jobs that do not contain them.
    """

    PROCESS_NAME = "dummyjob"
    OUT_DIR = "Test"

    def __init__(self, queue_enabled=True, do_mpi=True, do_threads=True):
        PipelinerJob.__init__(self)

        self.jobinfo.programs = ["touch"]

        if queue_enabled:
            self.make_queue_options()
        self.get_runtab_options(do_mpi, do_threads)

    def get_commands(self):
        outfile = os.path.join(self.output_name, "test_file.txt")
        self.output_nodes.append(Node(outfile, "TestFile.txt.dummyjob"))
        com = ["touch", outfile]
        return [com]

    def post_run_actions(self):
        with open(os.path.join(self.output_name, "test_file.txt", "w")) as f:
            f.write("post_run_actions() wrote this line")
