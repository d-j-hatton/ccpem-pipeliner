#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from .relion_job import RelionJob
from pipeliner.job_options import (
    FileNameJobOption,
    InputNodeJobOption,
    StringJobOption,
    files_exts,
    EXT_MRC_MAP,
    EXT_STARFILE,
)
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    RELION_EXTERNAL_NAME,
    EXTERNAL_DIR,
    Node,
)

# input nodes
INPUT_NODE_MOVIES = "MicrographMoviesData.star.relion"
INPUT_NODE_MICS = "MicrographsData.star.relion"
INPUT_NODE_PARTS = "ParticlesData.star.relion"
INPUT_NODE_COORDS = "MicrographsCoords.star.relion"
INPUT_NODE_REF3D = "DensityMap.mrc"
INPUT_NODE_MASK = "Mask3D.mrc"

# output Nodes
OUTPUT_NODE_UNKNOWN = "UnknownNodeType"


class RelionExternal(RelionJob):
    OUT_DIR = EXTERNAL_DIR
    PROCESS_NAME = RELION_EXTERNAL_NAME

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Run external program"
        self.jobinfo.programs = []
        self.jobinfo.short_desc = "Run programs through RELION's external job function"
        self.jobinfo.long_desc = (
            "This function has mostly been replaced by the pipeliner's plugin feature"
            " but is still available."
        )

        ## I/O
        self.joboptions["fn_exe"] = FileNameJobOption(
            label="External executable:",
            default_value="",
            pattern="",
            directory=".",
            help_text=(
                "Location of the script that will launch the external program. This"
                " script should write all its output in the directory specified with"
                " --o. Also, it should write in that same directory a file called {}"
                " upon successful exit, and {} upon failure.".format(
                    SUCCESS_FILE, FAIL_FILE
                )
            ),
        )

        ## Optional input nodes
        self.joboptions["in_mov"] = InputNodeJobOption(
            label="Input movies:",
            node_type=INPUT_NODE_MOVIES,
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR files", EXT_STARFILE),
            help_text=(
                "Input movies. This will be passed with a --in_movies argument to the"
                " executable."
            ),
        )

        self.joboptions["in_mic"] = InputNodeJobOption(
            label="Input micrographs:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR files", EXT_STARFILE),
            help_text=(
                "Input micrographs. This will be passed with a --in_mics argument to"
                " the executable."
            ),
        )

        self.joboptions["in_part"] = InputNodeJobOption(
            label="Input particles:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "Input particles. This will be passed with a --in_parts argument to the"
                " executable."
            ),
        )

        self.joboptions["in_coords"] = InputNodeJobOption(
            label="Input coordinates:",
            node_type=INPUT_NODE_COORDS,
            default_value="",
            directory="",
            pattern=files_exts(
                "coords STAR files", ["coords_suffix*.star"], exact=True
            ),
            help_text=(
                "Input coordinates. This will be passed with a --in_coords argument to"
                " the executable."
            ),
        )

        self.joboptions["in_3dref"] = InputNodeJobOption(
            label="Input 3D reference:",
            node_type=INPUT_NODE_REF3D,
            default_value="",
            directory="",
            pattern=files_exts("MRC Map", EXT_MRC_MAP),
            help_text=(
                "Input 3D reference map. This will be passed with a --in_3dref argument"
                " to the executable."
            ),
        )

        self.joboptions["in_mask"] = InputNodeJobOption(
            label="Input 3D mask:",
            node_type=INPUT_NODE_MASK,
            default_value="",
            directory="",
            pattern=files_exts("MRC Mask", EXT_MRC_MAP),
            help_text=(
                "Input 3D mask. This will be passed with a --in_mask argument to the"
                " executable."
            ),
        )

        ## Optional parameters
        self.joboptions["param1_label"] = StringJobOption(
            label="Param1 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param1_value"] = StringJobOption(
            label="Param1 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param2_label"] = StringJobOption(
            label="Param2 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param2_value"] = StringJobOption(
            label="Param2 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param3_label"] = StringJobOption(
            label="Param3 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param3_value"] = StringJobOption(
            label="Param3 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param4_label"] = StringJobOption(
            label="Param4 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param4_value"] = StringJobOption(
            label="Param4 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param5_label"] = StringJobOption(
            label="Param5 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param5_value"] = StringJobOption(
            label="Param5 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param6_label"] = StringJobOption(
            label="Param6 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param6_value"] = StringJobOption(
            label="Param6 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param7_label"] = StringJobOption(
            label="Param7 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param7_value"] = StringJobOption(
            label="Param7 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param8_label"] = StringJobOption(
            label="Param8 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param8_value"] = StringJobOption(
            label="Param8 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param9_label"] = StringJobOption(
            label="Param9 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param9_value"] = StringJobOption(
            label="Param9 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param10_label"] = StringJobOption(
            label="Param10 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param10_value"] = StringJobOption(
            label="Param10 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.get_runtab_options(False, True)

        ## addition for making output nodes

        self.joboptions["fn_output"] = StringJobOption(
            label="Output file name (optional):",
            default_value="",
            help_text=(
                "The name of the output file (if known) this will allow the creationof"
                " an output node for this job, otherwise the resulting file will have"
                " to be imported "
            ),
        )

        self.joboptions["output_nodetype"] = StringJobOption(
            label="Output node type (optional):",
            default_value="",
            help_text=(
                "The type of file that will be output. See the conventions on node"
                " naming in the documentation. If there is an existing node type that"
                " describes your data using it will more effectively integrate the file"
                " into the pipeline."
            ),
        )

    def get_commands(self):

        fn_exe = self.joboptions["fn_exe"].get_string(
            True,
            "ERROR: empty field for the external executable script...",
        )
        command = [fn_exe]

        # add the exe to the list of programs needed to run
        self.jobinfo.programs = [fn_exe]

        innodes = {
            self.joboptions["in_mic"]: "--in_mics",
            self.joboptions["in_mov"]: "--in_movies",
            self.joboptions["in_coords"]: "--in_coords",
            self.joboptions["in_part"]: "--in_parts",
            self.joboptions["in_3dref"]: "--in_3Dref",
            self.joboptions["in_mask"]: "--in_mask",
        }

        for i in list(innodes):
            innode_name = i.get_string()
            if len(innode_name) > 0:
                command += [innodes[i], innode_name]

        # Added an output nodes section
        fn_output = self.joboptions["fn_output"].get_string()
        if len(fn_output) > 0:
            newout = self.output_name + fn_output
            output_nodetype = self.joboptions["output_nodetype"].get_string()
            if output_nodetype == "":
                output_nodetype = "{}.{}".format(
                    OUTPUT_NODE_UNKNOWN, fn_output.split(".")[-1]
                )
            self.output_nodes.append(Node(newout, output_nodetype))

        command += ["--o", self.output_name]

        params = [
            "param1",
            "param2",
            "param3",
            "param4",
            "param5",
            "param6",
            "param7",
            "param8",
            "param9",
            "param10",
        ]
        for param in params:
            param_name = self.joboptions[param + "_label"].get_string()
            if len(param_name) > 0:
                command.append("--" + param_name)
                param_val = self.joboptions[param + "_value"].get_string()
                if "'" not in param_val and '"' not in param_val:
                    param_val = param_val.split()
                else:
                    param_val = [param_val]
                if len(param_val) > 0:
                    command.extend(param_val)

        # don't send the thread command if no threads are specified
        if self.joboptions["nr_threads"].get_number() > 1:
            command += ["--j", self.joboptions["nr_threads"].get_string()]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command += self.parse_additional_args()

        commands = [command]
        return commands
