#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from glob import glob
from pipeliner.data_structure import Node


# input nodes
INPUT_NODE_MASK = "Mask3D.mrc"


def solvmask2_node(job):
    """Add the input node for a secondary solvent mask if one is used"""
    splitargs = job.joboptions["other_args"].get_string().split()
    if "--solvent_mask2" in splitargs:
        solvmask2 = splitargs[splitargs.index("--solvent_mask2") + 1]
        job.input_nodes.append(Node(solvmask2, INPUT_NODE_MASK))


def refinement_cleanup(job, do_harsh):
    """Return list of intermediate files/dirs to remove"""

    del_files, del_dirs = [], []

    # get all the data files
    all_datafiles = glob(job.output_name + "run_it*_data.star")
    all_datafiles.sort()
    for df in all_datafiles[:-1]:
        del_files += glob(df.replace("_data.star", "*"))

    # Also clean up maps for PCA movies when doing harsh cleaning
    if job.PROCESS_NAME == "relion.multibody.refine" and do_harsh:
        del_files += glob(job.output_name + "analyse_component*_bin*.mrc")

    del_files = set(del_files)
    del_dirs = set(del_dirs)

    # remove any files or dirs associated with the output nodes
    if "refine3d" in job.PROCESS_NAME:
        # special case if iterations have been manually designated as outnodes
        fns = []
        for outnode in [x.name for x in job.output_nodes]:
            if "run_it" in outnode:
                root = os.path.join(
                    job.output_name, "_".join(os.path.basename(outnode).split("_")[:2])
                )
                fns.append(root)
    else:
        fns = ["_".join(x.name.split("_")[:-1]) for x in job.output_nodes]

    keepfiles = set()
    for fn in fns:
        for f in glob(fn + "*"):
            keepfiles.add(f)

    # remove these files from teh delete list
    for f in keepfiles:
        del_files.discard(f)
        fildir = os.path.dirname(f)
        del_dirs.discard(fildir)

    return list(del_files), list(del_dirs)


def find_current_opt_name(dirname):
    """Get the name of the current optimiser file for a job

    Used for figuring out the last completed iteration when a job
    has been failed or aborted

    Args:
        dirname (str): The job's name

    Returns:
        str: The relative path of the optimiser file

    """
    fn = dirname + "run_it???_optimiser.star"
    fn_opts = glob(fn)

    if len(fn_opts) != 0:
        fn_opts.sort()
        fn_opt = fn_opts[-1]
        return fn_opt
    else:
        print("Job has not even started yet!")


def find_current_refine_iter(dirname):
    """Get the current refinement iteration

     Args:
        dirname (str): The job's name

    Returns:
        int: The current iteration
    """
    opt = find_current_opt_name(dirname)
    if opt is None:
        return 0
    iter_name = os.path.basename(opt).split("_")[1]
    iter_no = int(iter_name.replace("it", ""))
    return iter_no
