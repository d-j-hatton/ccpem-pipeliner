#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from glob import glob

from .relion_job import RelionJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    SAMPLING,
    files_exts,
    EXT_STARFILE,
    EXT_RELION_OPT,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import (
    Node,
    RELION_SUCCESS_FILE,
    INIMODEL_DIR,
    INIMODEL_JOB_NAME,
)
from pipeliner.utils import truncate_number
from pipeliner.jobs.relion.refinement_common import (
    refinement_cleanup,
    find_current_opt_name,
    find_current_refine_iter,
)
from pipeliner.display_tools import make_map_model_thumb_and_display

# input nodes
INPUT_NODE_PARTS = "ParticlesData.star.relion"
OUTPUT_NODE_INIMODEL = "DensityMap.mrc.relion.initialmodel"
OUTPUT_NODE_OPT = "ProcessData.star.relion.optimiser.initialmodel"
OUTPUT_NODE_PARTS = "ParticlesData.star.relion.initialmodel"


class InimodelJob(RelionJob):
    PROCESS_NAME = INIMODEL_JOB_NAME
    OUT_DIR = INIMODEL_DIR

    def __init__(self):
        super(self.__class__, self).__init__()
        self.del_nodes_on_continue = True
        self.jobinfo.programs = ["relion_refine"]
        self.jobinfo.display_name = "RELION initial model generation"

        self.jobinfo.short_desc = "Create a de novo 3D initial model"
        self.jobinfo.long_desc = (
            "Relion 4.0 uses a gradient-driven algorithm to generate a de novo 3D"
            " initial model from the 2D particles. As of release 4.0, this algorithm is"
            " different from the SGD algorithm in the CryoSPARC program. Provided you"
            " have a reasonable distribution of viewing directions, and your data were"
            " good enough to yield detailed class averages in 2D classification , this"
            " algorithm is likely to yield a suitable, low-resolution model that can"
            " subsequently be used for 3D classification or 3D auto-refine"
        )

        self.joboptions["fn_img"] = InputNodeJobOption(
            label="Input images STAR file:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "A STAR file with all images (and their metadata).In SGD, it is very"
                " important that there are particles from enough different"
                " orientations. One only needs a few thousand to 10k particles. When"
                " selecting good 2D classes in the Subset Selection jobtype, use the"
                " option to select a maximum number of particles from each class to"
                " generate more even angular distributions for SGD.\n \n Alternatively,"
                " you may give a Spider/MRC stack of 2D images, but in that case NO"
                " metadata can be included and thus NO CTF correction can be performed,"
                " nor will it be possible to perform noise spectra estimation or"
                " intensity scale corrections in image groups. Therefore, running"
                " RELION with an input stack will in general provide sub-optimal"
                " results and is therefore not recommended!! Use the Preprocessing"
                " procedure to get the input STAR file in a semi-automated manner. Read"
                " the RELION wiki for more information."
            ),
            is_required=True,
        )

        self.joboptions["fn_cont"] = InputNodeJobOption(
            label="Continue from here:",
            node_type="",
            default_value=self.output_name if os.path.isdir(self.output_name) else "",
            directory=".",
            pattern=files_exts("optimiser STAR file", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration from which you want"
                " to continue a previous run. Note that the Output rootname of the"
                " continued run and the rootname of the previous run cannot be the"
                " same. If they are the same, the program will automatically add a"
                " '_ctX' to the output rootname, with X being the iteration from which"
                " one continues the previous run."
            ),
            in_continue=True,
            only_in_continue=True,
        )

        self.joboptions["nr_iter"] = IntJobOption(
            label="Number of VDAM mini-batches:",
            default_value=200,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text=(
                "How many iterations (i.e. mini-batches) to perform with the VDAM"
                " algorithm?"
            ),
        )

        self.joboptions["tau_fudge"] = FloatJobOption(
            label="Regularisation parameter T:",
            default_value=4,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Bayes law strictly determines the relative weight between the"
                " contribution of the experimental data and the prior. However, in"
                " practice one may need to adjust this weight to put slightly more"
                " weight on the experimental data to allow optimal results. Values"
                " greater than 1 for this regularisation parameter (T in the JMB2011"
                " paper) put more weight on the experimental data. Values around 2-4"
                " have been observed to be useful for 3D initial model calculations"
            ),
        )

        self.joboptions["nr_classes"] = IntJobOption(
            label="Number of classes:",
            default_value=1,
            suggested_min=1,
            suggested_max=50,
            step_value=1,
            help_text=(
                "The number of classes (K) for a multi-reference ab initio SGD"
                " refinement. These classes will be made in an unsupervised manner,"
                " starting from a single reference in the initial iterations of the"
                " SGD, and the references will become increasingly dissimilar during"
                " the inbetween iterations."
            ),
        )

        self.joboptions["sym_name"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text=(
                "SGD sometimes works better in C1. If you make an initial model in C1"
                " but want to run Class3D/Refine3D with a higher point group symmetry,"
                " the reference model must be rotated to conform the symmetry"
                " convention. You can do this by the relion_align_symmetry command."
            ),
            is_required=True,
        )

        self.joboptions["do_run_C1"] = BooleanJobOption(
            label="Run in C1 and apply symmetry later? ",
            default_value=True,
            help_text=(
                "If set to Yes, the gradient-driven optimisation is run in C1 and the"
                " symmetry orientation is searched and applied later. If set to No, the"
                " entire optimisation is run in the symmetry point group indicated"
                " above."
            ),
        )

        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Mask diameter (A):",
            default_value=200,
            suggested_min=0,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "The experimental images will be masked with a soft circular mask with"
                " this diameter. Make sure this radius is not set too small because"
                " that may mask away part of the signal! If set to a value larger than"
                " the image size no masking will be performed.\n\n The same diameter"
                " will also be used for a spherical mask of the reference structures if"
                " no user-provided mask is specified."
            ),
            in_continue=True,
        )

        self.joboptions["do_solvent"] = BooleanJobOption(
            label="Flatten and enforce non-negative solvent?",
            default_value=True,
            help_text=(
                "If set to Yes, the job will apply a spherical mask and enforce all"
                " values in the reference to be non-negative."
            ),
        )

        self.joboptions["do_ctf_correction"] = BooleanJobOption(
            label="Do CTF-correction?",
            default_value=True,
            help_text=(
                "If set to Yes, CTFs will be corrected inside the MAP refinement. The"
                " resulting algorithm intrinsically implements the optimal linear, or"
                " Wiener filter. Note that CTF parameters for all images need to be"
                " given in the input STAR file. The command 'relion_refine"
                " --print_metadata_labels' will print a list of all possible metadata"
                " labels for that STAR file. See the RELION Wiki for more details.\n\n"
                " Also make sure that the correct pixel size (in Angstrom) is given"
                " above!)"
            ),
        )

        self.joboptions["ctf_intact_first_peak"] = BooleanJobOption(
            label="Ignore CTFs until first peak?",
            default_value=False,
            help_text=(
                "If set to Yes, then CTF-amplitude correction will only be performed"
                " from the first peak of each CTF onward. This can be useful if the CTF"
                " model is inadequate at the lowest resolution. Still, in general using"
                " higher amplitude contrast on the CTFs (e.g. 10-20%) often yields"
                " better results. Therefore, this option is not generally recommended:"
                " try increasing amplitude contrast (in your input STAR file) first!"
            ),
            deactivate_if=[("do_ctf_correction", "=", False)],
        )

        self.joboptions["sampling"] = MultipleChoiceJobOption(
            label="Initial angular sampling:",
            choices=SAMPLING,
            default_value_index=1,
            help_text=(
                "There are only a few discrete angular samplings possible because we"
                " use the HealPix library to generate the sampling of the first two"
                " Euler angles on the sphere. The samplings are approximate numbers and"
                " vary slightly over the sphere.\n\n For initial model generation at"
                " low resolutions, coarser angular samplings can often be used than in"
                " normal 3D classifications/refinements, e.g. 15 degrees. During the"
                " inbetween and final SGD iterations, the sampling will be adjusted to"
                " the resolution, given the particle size."
            ),
            in_continue=True,
        )

        self.joboptions["offset_range"] = FloatJobOption(
            label="Offset search range (pix):",
            default_value=6,
            suggested_min=0,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Probabilities will be calculated only for translations in a circle"
                " with this radius (in pixels). The center of this circle changes at"
                " every iteration and is placed at the optimal translation for each"
                " image in the previous iteration."
            ),
            in_continue=True,
        )

        self.joboptions["offset_step"] = FloatJobOption(
            label="Offset search step (pix):",
            default_value=2,
            suggested_min=0.1,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Translations will be sampled with this step-size (in pixels)."
                " Translational sampling is also done using the adaptive approach."
                " Therefore, if adaptive=1, the translations will first be evaluated on"
                " a 2x coarser grid."
            ),
            in_continue=True,
        )

        self.get_comp_options()
        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):

        self.command = ["relion_refine"]

        if not self.is_continue:
            self.command += ["--grad", "--denovo_3dref"]

        fn_run = os.path.join(self.output_name, "run")
        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string(
                True,
                "ERROR: empty field for continuation STAR file...",
            )
            pos_it = int(os.path.basename(fn_cont).partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for continuation"
                    " run!",
                )
            self.command += ["--continue", fn_cont]

        elif not self.is_continue:
            fn_img = self.joboptions["fn_img"].get_string(
                True, "ERROR: empty field for input STAR file..."
            )
            self.command += ["--i", fn_img]

        self.command += ["--o", fn_run]

        nr_iter = int(self.joboptions["nr_iter"].get_number())
        self.command += ["--iter", nr_iter]

        fn_out = fn_run + "_it" + "{:03d}".format(nr_iter)
        self.output_nodes.append(Node(fn_out + "_optimiser.star", OUTPUT_NODE_OPT))
        self.output_nodes.append(Node(fn_out + "_data.star", OUTPUT_NODE_PARTS))
        nr_classes = int(self.joboptions["nr_classes"].get_number())

        ## ctf
        if self.joboptions["do_ctf_correction"].get_boolean():
            self.command.append("--ctf")
            if self.joboptions["ctf_intact_first_peak"].get_boolean():
                self.command.append("--ctf_intact_first_peak")

        sym = self.joboptions["sym_name"].get_string()
        self.command += ["--K", truncate_number(nr_classes, 0)]
        if self.joboptions["do_run_C1"].get_boolean() or sym.lower == "c1":
            self.command += ["--sym", "C1"]
            symtag = ""
        else:
            self.command += ["--sym", sym]
            symtag = f".{sym.lower()}sym"

        for iclass in range(nr_classes):
            fn_class = f"{fn_out}_class{iclass+1:03d}.mrc"
            self.output_nodes.append(Node(fn_class, OUTPUT_NODE_INIMODEL + symtag))

        if self.joboptions["do_solvent"].get_boolean():
            self.command.append("--flatten_solvent")
        # zero mask is currently not optional
        self.command.append("--zero_mask")

        ## compute options
        self.add_comp_options()

        ## compute options
        particle_diameter = self.joboptions["particle_diameter"].get_string()
        self.command += ["--particle_diameter", particle_diameter]

        ## sampling
        iover = 1
        self.command += ["--oversampling", str(iover)]
        sampling_opts = SAMPLING
        sampling_opt = self.joboptions["sampling"].get_string()
        sampling = sampling_opts.index(sampling_opt) + 1
        # The sampling given in the GUI will be the oversampled one!
        self.command += ["--healpix_order", str(sampling - iover)]

        ## Offset range
        offset_range = self.joboptions["offset_range"].get_string()
        self.command += ["--offset_range", offset_range]

        ## The sampling given in the GUI will be the oversampled one!
        offset_step = self.joboptions["offset_step"].get_number() * (2**iover)
        self.command += ["--offset_step", str(offset_step)]

        ## Running stuff
        self.command += ["--j", self.joboptions["nr_threads"].get_string()]

        ## GPU-stuffs
        if self.joboptions["use_gpu"].get_boolean():
            gpu_ids = self.joboptions["gpu_ids"].get_string()
            self.command += ["--gpu", gpu_ids]

        ## Other arguments
        other_arguments = self.joboptions["other_args"].get_string()
        if len(other_arguments) > 0:
            self.command += self.parse_additional_args()

        commands = [self.command]

        # if the run was a continuation get the right filename
        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string(
                True,
                "ERROR: empty field for continuation STAR file...",
            )
            pos_it = int(os.path.basename(fn_cont).partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for continuation"
                    " run!",
                )

        fn_model = os.path.join(
            self.output_name, f"run_it{int(nr_iter):03d}_model.star"
        )

        # remove the succss file
        remove_command = [
            "rm",
            "-f",
            os.path.join(self.output_name, RELION_SUCCESS_FILE),
        ]
        commands.append(remove_command)

        # Align with symmetry axes and apply symmetry
        alignsym_command = ["relion_align_symmetry"]
        self.jobinfo.programs.append("relion_align_symmetry")
        alignsym_command += ["--i", fn_model]
        alignsym_command += [
            "--o",
            os.path.join(self.output_name, "initial_model.mrc"),
        ]

        # copied from the relion code, this is is puzzling behavior though
        # only the most populated map is symmetrized, wouldn't it be better to
        # symmetrize all of them to the bring the results in line with if it were
        # run with symmetry from the start?

        fn_sym = self.joboptions["sym_name"].get_string()
        if fn_sym == "":
            print("Warning no symmetry specified; defaulting to C1")
            fn_sym = "c1"

        if self.joboptions["do_run_C1"].get_boolean() and fn_sym.lower() != "c1":
            # apply the desired symmetry
            alignsym_command += ["--sym", fn_sym]
            symtag = f".{fn_sym.lower()}sym"

        elif fn_sym.lower() != "c1":
            # symmetry was applied during refinenemt - just update tag
            alignsym_command += ["--sym", "C1"]
            symtag = f".{fn_sym.lower()}sym"

        else:
            # C1 no symmetry tag
            alignsym_command += ["--sym", "C1"]
            symtag = ""

        alignsym_command += ["--apply_sym", "--select_largest_class"]
        commands.append(alignsym_command)

        # put in the new node
        self.output_nodes.append(
            Node(
                "{}initial_model.mrc".format(self.output_name),
                OUTPUT_NODE_INIMODEL + symtag,
            )
        )

        return commands

    def prepare_clean_up_lists(self, do_harsh=False):
        return refinement_cleanup(self, do_harsh)

    def get_current_output_nodes(self):
        current_opt = find_current_opt_name(self.output_name)
        if current_opt is None:
            return []
        opt_node = Node(current_opt, OUTPUT_NODE_OPT)

        fn_data = current_opt.replace("optimiser", "data")
        data_node = Node(fn_data, OUTPUT_NODE_PARTS)
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_map = fn_root + "class???.mrc"
        fn_maps = glob(fn_map)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = Node(m, OUTPUT_NODE_INIMODEL)
                out_nodes.append(map_node)

        return out_nodes

    # needs to return an OneDep   InitialModel object
    def prepare_onedep_data(self):
        # TODO: write this method
        return []

    def create_results_display(self):
        dispobjs = []

        # if the process is done use the output nodes
        if os.path.isfile(os.path.join(self.output_name, RELION_SUCCESS_FILE)):
            for on in self.output_nodes:
                if on.name.split(".")[-1] == "mrc":
                    dispobjs.append(
                        make_map_model_thumb_and_display(
                            maps=[on.name],
                            outputdir=self.output_name,
                        )
                    )
            return dispobjs

        # otherwise use the current iter
        curr_iter = find_current_refine_iter(self.output_name)
        mapfiles = glob(self.output_name + f"run_it{curr_iter}_class*.mrc")
        for imap in mapfiles:
            dispobjs.append(
                make_map_model_thumb_and_display(
                    maps=[imap],
                    outputdir=self.output_name,
                )
            )

        return dispobjs
