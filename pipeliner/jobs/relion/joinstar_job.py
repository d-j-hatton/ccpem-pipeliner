#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
from .relion_job import RelionJob
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    EXT_STARFILE,
)
from pipeliner.data_structure import (
    JOINSTAR_MOVIES_NAME,
    JOINSTAR_MICS_NAME,
    JOINSTAR_PARTS_NAME,
    JOINSTAR_DIR,
    Node,
)

from pipeliner.onedep_deposition import (
    prepare_EMPIAR_raw_mics,
    prepare_EMPIAR_mics_parts,
)
from pipeliner.display_tools import mini_montage_from_starfile

# input nodes
INPUT_NODE_PARTS = "ParticlesData.star.relion"
INPUT_NODE_MICS = "MicrographsData.star.relion"
INPUT_NODE_MOVIES = "MicrographsMoviesData.star.relion"

# output nodes
OUTPUT_NODE_PARTS = "ParticlesData.star.relion"
OUTPUT_NODE_MICS = "MicrographsData.star.relion"
OUTPUT_NODE_MOVIES = "MicrographsMoviesData.star.relion"


class RelionJoinStarJob(RelionJob):
    OUT_DIR = JOINSTAR_DIR

    def __init__(self):
        self.jobinfo.programs = ["relion_star_handler"]
        self.get_runtab_options(False, False)
        self.jobinfo.long_desc = (
            "Join together starfiles that contain the same type of data. The files must"
            " contain the same metadata columns in the same order to be joined"
            " successfully"
        )

    def make_join_command(self, files, ftype):
        self.command = ["relion_star_handler", "--combine", "--i"]

        filetypes = {
            "particles": ("rlnImageName", OUTPUT_NODE_PARTS),
            "movies": ("rlnMicrographName", OUTPUT_NODE_MOVIES),
            "mics": ("rlnMicrographName", OUTPUT_NODE_MICS),
        }
        usedfiles = []
        for pfile in files:
            fname = pfile.get_string()
            if len(fname) > 0:
                usedfiles.append(pfile)
        if len(usedfiles) > 1:
            self.command.append(
                "{}".format(" ".join([x.get_string() for x in usedfiles]))
            )
            self.command += ["--check_duplicates", filetypes[ftype][0]]
            outputfile = self.output_name + "join_{}.star".format(ftype)
            self.command += ["--o", outputfile]
            self.output_nodes.append(Node(outputfile, filetypes[ftype][1]))
            other_args = self.joboptions["other_args"].get_string()
            if len(other_args) > 0:
                self.command += self.parse_additional_args()

        else:
            raise ValueError(
                "ERROR: Not enough files selected, select at least two to join"
            )


class RelionJoinStarMovies(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MOVIES_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join movie STAR files"

        self.jobinfo.short_desc = "Join multiple movie STAR files into a single file"

        self.joboptions["fn_mov1"] = InputNodeJobOption(
            label="Movie STAR file 1:",
            node_type=INPUT_NODE_MOVIES,
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The first of the micrograph movie STAR files to be combined.",
        )

        self.joboptions["fn_mov2"] = InputNodeJobOption(
            label="Movie STAR file 2:",
            node_type=INPUT_NODE_MOVIES,
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The second of the micrograph movie STAR files to be combined.",
        )

        self.joboptions["fn_mov3"] = InputNodeJobOption(
            label="Movie STAR file 3:",
            node_type=INPUT_NODE_MOVIES,
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The third of the micrograph movie STAR files to be combined.",
        )

        self.joboptions["fn_mov4"] = InputNodeJobOption(
            label="Movie STAR file 4:",
            node_type=INPUT_NODE_MOVIES,
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The fourth of the micrograph movie STAR files to be combined.",
        )

        self.set_joboption_order(["fn_mov1", "fn_mov2", "fn_mov3", "fn_mov4"])

    def get_commands(self):

        fn_mov1 = self.joboptions["fn_mov1"]
        fn_mov2 = self.joboptions["fn_mov2"]
        fn_mov3 = self.joboptions["fn_mov3"]
        fn_mov4 = self.joboptions["fn_mov4"]
        partsfiles = [fn_mov1, fn_mov2, fn_mov3, fn_mov4]
        self.make_join_command(partsfiles, "movies")

        commands = [self.command]
        return commands

    def prepare_onedep_data(self):
        movfile = os.path.join(self.output_name, "join_movies.star")
        return prepare_EMPIAR_raw_mics(movfile)

    def create_results_display(self):
        odir = os.path.join(self.output_name, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="movies",
                column="_rlnMicrographMovies",
                outputdir=odir,
                nimg=4,
            )
        ]


class RelionJoinStarMicrographs(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MICS_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join micrograph STAR files"

        self.jobinfo.short_desc = (
            "Join multiple micrograph STAR files into a single file"
        )

        self.joboptions["fn_mic1"] = InputNodeJobOption(
            label="Micrograph STAR file 1:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The first of the micrograph STAR files to be combined.",
        )
        self.joboptions["fn_mic2"] = InputNodeJobOption(
            label="Micrograph STAR file 2:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The second of the micrograph STAR files to be combined.",
        )
        self.joboptions["fn_mic3"] = InputNodeJobOption(
            label="Micrograph STAR file 3:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The third of the micrograph STAR files to be combined.",
        )
        self.joboptions["fn_mic4"] = InputNodeJobOption(
            label="Micrograph STAR file 4:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The fourth of the micrograph STAR files to be combined.",
        )

        self.set_joboption_order(["fn_mic1", "fn_mic2", "fn_mic3", "fn_mic4"])

    def get_commands(self):
        fn_mic1 = self.joboptions["fn_mic1"]
        fn_mic2 = self.joboptions["fn_mic2"]
        fn_mic3 = self.joboptions["fn_mic3"]
        fn_mic4 = self.joboptions["fn_mic4"]
        partsfiles = [fn_mic1, fn_mic2, fn_mic3, fn_mic4]
        self.make_join_command(partsfiles, "mics")

        commands = [self.command]
        return commands

    # needs to return an EMPIAR correctedmics obj
    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_name, "join_mics.star")
        return prepare_EMPIAR_mics_parts(mpfile, is_parts=False, is_cor_parts=False)

    def create_results_display(self):
        odir = os.path.join(self.output_name, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="micrographs",
                column="_rlnMicrographName",
                outputdir=odir,
            )
        ]


class RelionJoinStarParticles(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_PARTS_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join particle STAR files"

        self.jobinfo.short_desc = "Join multiple particle STAR files into a single file"

        self.joboptions["fn_part1"] = InputNodeJobOption(
            label="Particle STAR file 1:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The first of the particle STAR files to be combined.",
        )

        self.joboptions["fn_part2"] = InputNodeJobOption(
            label="Particle STAR file 2:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The second of the particle STAR files to be combined.",
        )

        self.joboptions["fn_part3"] = InputNodeJobOption(
            label="Particle STAR file 3:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The Third of the particle STAR files to be combined.",
        )

        self.joboptions["fn_part4"] = InputNodeJobOption(
            label="Particle STAR file 4:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The fourth of the particle STAR files to be combined.",
        )

        self.set_joboption_order(["fn_part1", "fn_part2", "fn_part3", "fn_part4"])

    def get_commands(self):
        fn_part1 = self.joboptions["fn_part1"]
        fn_part2 = self.joboptions["fn_part2"]
        fn_part3 = self.joboptions["fn_part3"]
        fn_part4 = self.joboptions["fn_part4"]
        partsfiles = [fn_part1, fn_part2, fn_part3, fn_part4]
        self.make_join_command(partsfiles, "particles")

        commands = [self.command]
        return commands

    # needs to return an EMPIAR Particles obj
    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_name, "join_particles.star")
        return prepare_EMPIAR_mics_parts(mpfile, is_parts=False, is_cor_parts=False)

    def create_results_display(self):
        odir = os.path.join(self.output_name, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="particles",
                column="_rlnImageName",
                outputdir=odir,
                nimg=20,
            )
        ]
