#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from pipeliner.jobs.relion.relion_job import RelionJob
import os
from pipeliner.job_options import (
    InputNodeJobOption,
    FileNameJobOption,
    files_exts,
    EXT_RELION_HALFMAP,
    EXT_MRC_MAP,
    BooleanJobOption,
    FloatJobOption,
)

from pipeliner.data_structure import (
    Node,
    LOCALRES_DIR,
    LOCALRES_OWN_NAME,
    LOCALRES_RESMAP_NAME,
)
from pipeliner.jobstar_reader import JobStar
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)

# input nodes
INPUT_NODE_HALFMAP = "DensityMap.mrc.halfmap"
INPUT_NODE_MASK = "Mask3D.mrc"

# output node
OUTPUT_NODE_LOCRES_OWN = "Image3D.mrc.relion.localresmap"
OUTPUT_NODE_LOCRES_RESMAP = "Image3D.mrc.resmap.localresmap"
OUTPUT_NODE_FILTMAP = "DensityMap.mrc.relion.localresfiltered"
OUTPUT_NODE_LOG = "LogFile.pdf.relion.localres"


class RelionLocalResJob(RelionJob):
    OUT_DIR = LOCALRES_DIR

    def __init__(self):
        self.jobinfo.long_desc = (
            "The estimated resolution from the post-processing program is a global"
            " estimate. However, a single number cannot describe the variations in"
            " resolution that are often observed in reconstructions of macromolecular"
            " complexes"
        )

        self.joboptions["fn_in"] = InputNodeJobOption(
            label="One of the 2 unfiltered half-maps:",
            node_type=INPUT_NODE_HALFMAP,
            default_value="",
            directory="",
            pattern=files_exts("Halfmap file", EXT_RELION_HALFMAP),
            help_text=(
                "Provide one of the two unfiltered half-reconstructions that were"
                " output upon convergence of a 3D auto-refine run."
            ),
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Calibrated pixel size (A)",
            default_value=1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Provide the final, calibrated pixel size in Angstroms. This value may"
                " be different from the pixel-size used thus far, e.g. when you have"
                " recalibrated the pixel size using the fit to a PDB model. The X-axis"
                " of the output FSC plot will use this calibrated value."
            ),
            is_required=True,
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="User-provided solvent mask:",
            node_type=INPUT_NODE_MASK,
            default_value="",
            directory="",
            pattern=files_exts("Mask MRC file", EXT_MRC_MAP),
            help_text=(
                "Provide a mask with values between 0 and 1 around all domains of the"
                " complex. ResMap uses this mask for local resolution calculation."
                " RELION does NOT use this mask for calculation, but makes a histogram"
                " of local resolution within this mask."
            ),
            in_continue=True,
            is_required=True,
        )

    def common_commands(self):
        fn_half1 = self.joboptions["fn_in"].get_string(
            True, "Empty field for input half-map"
        )
        if "half1" not in os.path.basename(fn_half1):
            raise ValueError("Cannot find 'half' substring in the input filename")

        fn_half2 = fn_half1.replace("half1", "half2")
        return (fn_half1, fn_half2)

    def additional_args(self):
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    # To DO: needs to return a OneDep Final3Dreconstruction type
    def prepare_onedep_data(self):
        return RelionJob.prepare_onedep_data(self)


class RelionLocalResResMap(RelionLocalResJob):

    PROCESS_NAME = LOCALRES_RESMAP_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionLocalResJob.__init__(self)
        self.jobinfo.display_name = "RELION ResMap"

        self.jobinfo.short_desc = "Calculate local resolution with ResMap"
        self.jobinfo.long_desc += (
            ". Local resolution is calculated using the program ResMap"
        )
        self.jobinfo.references.append(
            Ref(
                authors=["Kucukelbi A", "Sigworth FJ", "Tagare HD"],
                title="Quantifying the local resolution of cryo-EM density maps.",
                journal="Nature methods",
                year="2014",
                volume="11",
                issue="1",
                pages="63–65",
                doi="10.1038/nmeth.2727",
            )
        )
        self.jobinfo.programs.append("ResMap")

        default_resmap_location = os.getenv("RELION_RESMAP_EXECUTABLE", "")

        self.joboptions["do_resmap_locres"] = BooleanJobOption(
            label="Use ResMap?",
            default_value=True,
            help_text=(
                "If set to Yes, then ResMap will be used for local resolution"
                " estimation."
            ),
        )

        self.joboptions["fn_resmap"] = FileNameJobOption(
            label="ResMap executable:",
            default_value="",
            pattern="ResMap*",
            directory=".",
            help_text=(
                "Location of the ResMap executable. You can control the default of this"
                " field by setting environment variable RELION_RESMAP_EXECUTABLE, or by"
                " editing the first few lines in src/gui_jobwindow.h and recompile the"
                " code. \n \n Note that the ResMap wrapper cannot use MPI."
            ),
            suggestion=default_resmap_location,
            is_required=True,
        )

        self.joboptions["pval"] = FloatJobOption(
            label="P-value:",
            default_value=0.05,
            suggested_min=0.0,
            suggested_max=1.0,
            step_value=0.01,
            help_text=(
                "This value is typically left at 0.05. If you change it, report the"
                " modified value in your paper!"
            ),
            is_required=True,
        )

        self.joboptions["minres"] = FloatJobOption(
            label="Highest resolution (A):",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=10.0,
            step_value=0.1,
            help_text=(
                "ResMaps minRes parameter. By default (0), the program will start at"
                " just above 2x the pixel size"
            ),
            is_required=True,
        )

        self.joboptions["maxres"] = FloatJobOption(
            label="Lowest resolution (A):",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=10.0,
            step_value=0.1,
            help_text=(
                "ResMaps maxRes parameter. By default (0), the program will stop at 4x"
                " the pixel size"
            ),
            is_required=True,
        )

        self.joboptions["stepres"] = FloatJobOption(
            label="Resolution step size (A)",
            default_value=1.0,
            suggested_min=0.1,
            suggested_max=3,
            step_value=0.1,
            help_text="ResMaps stepSize parameter.",
            is_required=True,
        )

        self.make_additional_args()

    def get_commands(self):

        fn_resmap = self.joboptions["fn_resmap"].get_string(
            True,
            "ERROR: please provide an executable for the ResMap program.",
        )
        self.jobinfo.programs = [fn_resmap]

        self.joboptions["fn_mask"].get_string(
            True,
            "ERROR: Please provide an input mask for ResMap local-resolution"
            " estimation.",
        )

        fn_half1, fn_half2 = self.common_commands()

        # make symbolic links to the halfmaps in the output directory
        os.symlink(os.path.abspath(fn_half1), self.output_name + "half1.mrc")
        os.symlink(os.path.abspath(fn_half2), self.output_name + "half2.mrc")

        self.output_nodes.append(
            Node(self.output_name + "half1_resmap.mrc", OUTPUT_NODE_LOCRES_RESMAP)
        )
        self.command = [fn_resmap]
        self.command += [
            "--maskVol=" + self.joboptions["fn_mask"].get_string(),
            "--noguiSplit",
            self.output_name + "half1.mrc",
            self.output_name + "half2.mrc",
            "--vxSize=" + self.joboptions["angpix"].get_string(),
            "--pVal=" + self.joboptions["pval"].get_string(),
            "--minRes=" + self.joboptions["minres"].get_string(),
            "--maxRes=" + self.joboptions["maxres"].get_string(),
            "--stepRes=" + self.joboptions["stepres"].get_string(),
        ]
        self.additional_args()
        commands = [self.command]
        return commands

    # TODO: Need to write a results display function for this, need to see
    #   what the results look like first...


class RelionLocalResOwn(RelionLocalResJob):

    PROCESS_NAME = LOCALRES_OWN_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionLocalResJob.__init__(self)
        self.jobinfo.programs = ["relion_postprocess", "relion_postprocess_mpi"]
        self.jobinfo.display_name = "RELION LocalRes"

        self.jobinfo.short_desc = "Calculate local resolution using RELION"
        self.jobinfo.long_desc += (
            ". Relion uses a post-processing-like procedure with a soft spherical mask"
            " that is moved around the entire map"
        )

        self.joboptions["adhoc_bfac"] = FloatJobOption(
            label="User-provided B-factor:",
            default_value=-100,
            suggested_min=-500,
            suggested_max=0,
            step_value=-25,
            help_text=(
                "Probably, the overall B-factor as was estimated in the postprocess is"
                " a useful value for here. Use negative values for sharpening. Be"
                " careful: if you over-sharpen your map, you may end up interpreting"
                " noise for signal!"
            ),
            is_required=True,
        )

        self.joboptions["fn_mtf"] = FileNameJobOption(
            label="MTF of the detector (STAR file)",
            default_value="",
            pattern="STAR Files (*.star)",
            directory=".",
            help_text=(
                "The MTF of the detector is used to complement the user-provided"
                " B-factor in the sharpening. If you don't have this curve, you can"
                " leave this field empty."
            ),
        )

        self.get_runtab_options()

    def get_commands(self):

        fn_half1 = self.common_commands()[0]

        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            self.command = ["mpirun", "-n", nr_mpi, "relion_postprocess_mpi"]
        else:
            self.command = ["relion_postprocess"]

        self.command += [
            "--locres",
            "--i",
            fn_half1,
            "--o",
            self.output_name + "relion",
            "--angpix",
            self.joboptions["angpix"].get_string(),
        ]

        # these were commented out in the relion code
        # locres_sampling = self.joboptions["locres_sampling"].get_string()
        # command += " --locres_sampling " + locres_sampling
        # randomize_at = self.joboptions["randomize_at"].get_string()
        # command += " --locres_randomize_at " + randomize_at

        self.command += ["--adhoc_bfac", self.joboptions["adhoc_bfac"].get_string()]

        fn_mtf = self.joboptions["fn_mtf"].get_string()
        if len(fn_mtf) > 0:
            self.command += ["--mtf", fn_mtf]

        fn_mask = self.joboptions["fn_mask"].get_string()
        if len(fn_mask) > 0:
            self.command += ["--mask", fn_mask]

        self.output_nodes.append(
            Node(self.output_name + "histogram.pdf", OUTPUT_NODE_LOG)
        )

        self.output_nodes.append(
            Node(
                self.output_name + "relion_locres_filtered.mrc",
                OUTPUT_NODE_FILTMAP,
            )
        )

        self.output_nodes.append(
            Node(
                self.output_name + "relion_locres.mrc",
                OUTPUT_NODE_LOCRES_OWN,
            )
        )

        self.additional_args()
        commands = [self.command]
        return commands

    def gather_metadata(self):
        jobstar = JobStar(os.path.join(self.output_name, "job.star"))
        jobops = jobstar.get_all_options()
        metadata_dict = {}
        metadata_dict["Local res type"] = "Relion"
        metadata_dict["ad hoc b-factor applied"] = jobops["adhoc_bfac"]

        if jobops["fn_mask"] is not None:
            metadata_dict["Mask"] = jobops["fn_mask"]

        if jobops["fn_mtf"] is not None:
            metadata_dict["Mtf file"] = jobops["fn_mtf"]

        runout_file = os.path.join(self.output_name, "run.out")
        with open(runout_file, "r") as runout:
            runout_data = runout.readlines()
        for line in runout_data:
            if "+ randomize phases beyond:" in line:
                metadata_dict["Phases randomized beyond"] = line.split()[-1]

        return metadata_dict

    def create_results_display(self):
        # # make the hisogram
        logstar = os.path.join(self.output_name, "relion_locres_fscs.star")
        runout = os.path.join(self.output_name, "run.out")
        with open(runout, "r") as ro:
            loglines = ro.readlines()
        hbin, hval = [], []
        for line in loglines:
            if line[0] == "[" and len(line.split()) == 3:
                val, count = line.split()[0], line.split()[2]
                if val != "[-INF,":
                    hbin.append(float(val[1:].strip(",")))
                hval.append(float(count))
        hbin.append(hbin[-1] + 0.1)
        hbin = [hbin[0] - 0.1] + hbin
        histo = create_results_display_object(
            "histogram",
            title="Local resolution distribution",
            xlabel="Resolution",
            ylabel="Number of voxels",
            associated_data=[logstar],
            bins=hval,
            bin_edges=hbin,
        )
        mapfile = os.path.join(self.output_name, "relion_locres_filtered.mrc")
        map = make_map_model_thumb_and_display(
            maps=[mapfile],
            outputdir=self.output_name,
            title="Local resolution filtered map",
        )

        return [histo, map]
