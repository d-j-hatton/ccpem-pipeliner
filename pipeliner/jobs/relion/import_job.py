#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from .relion_job import RelionJob
from pipeliner.job_options import (
    FileNameJobOption,
    StringJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import (
    IMPORT_DIR,
    IMPORT_MOVIES_NAME,
    IMPORT_OTHER_NAME,
    Node,
)
from pipeliner.metadata_tools import format_for_metadata
from pipeliner.jobstar_reader import JobStar, RelionStarFile
from pipeliner.job_options import files_exts
from pipeliner.onedep_deposition import prepare_EMPIAR_raw_mics
from pipeliner.display_tools import (
    mini_montage_from_starfile,
    make_map_model_thumb_and_display,
    create_results_display_object,
    mini_montage_from_mrcs_stack,
    make_particle_coords_thumb,
)
from pipeliner.mrc_image_tools import bin_mrc_map

# output nodes (Top-level type, [kwds])
OUTPUT_NODE_MOVIES = ("MicrographMoviesData", ["relion"])
OUTPUT_NODE_MICS = ("MicrographsData", ["relion"])
OUTPUT_NODE_COORDS = ("MicrographsCoords", ["relion"])
OUTPUT_NODE_PARTS = ("ParticlesData", ["relion"])
OUTPUT_NODE_REF2D_MRC = ("Image2DStack", [])
OUTPUT_NODE_REF2D_STAR = ("Images2DData", [])
OUTPUT_NODE_REF3D = ("DensityMap", [])
OUTPUT_NODE_MASK3D = ("Mask3D", [])
OUTPUT_NODE_HALFMAP = ("DensityMap", ["halfmap"])
OUTPUT_NODE_SINGLE_MIC = ("Image2D", [])
OUTPUT_NODE_TOMO = ("Image3D", ["tomogram"])
OUTPUT_NODE_MODEL = ("AtomicCoords", [])
OUTPUT_NODE_MASK2D = ("Mask2D", [])


def make_node_name(nodetype, ext):
    """assemble a node name from output types

    Necessary because there are multiple possible extensions
    for the same node types

    Args:
        nodetype (tuple(str, list)): Top level node name and list of kwds
        ext (str): extension of the file
    Returns:
        string: The full node name (toplevel.ext.kwds)
    """
    ext = "." + ext if ext[0] != "." else ext
    if len(nodetype[1]):
        nn = nodetype[0] + ext + "." + ".".join(nodetype[1])
    else:
        nn = nodetype[0] + ext
    return nn


# these are all the possible node types that can be imported
# except movies, which are handled separately
NODE_TYPES = {
    "micrograph": "2D micrograph (.mrc)",
    "coords_files": "2D/3D particle coordinates (*.box, *_pick.star)",
    "coords_star": "Relion coordinates starfile (.star)",
    "parts_star": "Particles STAR file (.star)",
    "2drefs": "2D references (.star, .mrcs)",
    "mics_star": "Micrographs STAR file (.star)",
    "3dref": "3D reference (.mrc)",
    "3dmask": "3D mask (.mrc)",
    "halfmap": "Unfiltered half-map (unfil.mrc)",
    "atomic_model": "Atomic model (.pdb, .mmcif, .cif)",
    "tomo": "Tomogram (.mrc, .mrcs)",
    "2D mask": "2D mask (.mrc)",
}


class RelionImportMovies(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_MOVIES_NAME

    def __init__(self):
        RelionJob.__init__(self)
        self.jobinfo.programs = ["relion_import"]
        self.jobinfo.display_name = "RELION import micrographs"
        self.jobinfo.short_desc = "Import movies or single-frame micrographs"

        self.jobinfo.long_desc = (
            "Import raw micrographs and create a star file containing the collected"
            " micrographs and their metadata"
        )

        self.joboptions["fn_in_raw"] = FileNameJobOption(
            label="Raw input files:",
            default_value="Micrographs/*.tif",
            pattern="Movie or Image (*.{mrc,mrcs,tif,tiff})",
            directory=".",
            help_text=(
                "Provide a Linux wildcard that selects all raw movies or micrographs to"
                " be imported."
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["is_multiframe"] = BooleanJobOption(
            label="Are these multi-frame movies?",
            default_value=True,
            help_text=(
                "Set to Yes for multi-frame movies, set to No for single-frame"
                " micrographs."
            ),
            in_continue=True,
        )
        self.joboptions["optics_group_name"] = StringJobOption(
            label="Optics group name:",
            default_value="opticsGroup1",
            help_text=(
                "Name of this optics group. Each group of movies/micrographs with"
                " different optics characteristics for CTF refinement should have a"
                " unique name."
            ),
            in_continue=True,
            is_required=True,
            validation_regex="^[a-zA-Z0-9_]+$",
        )
        self.joboptions["fn_mtf"] = FileNameJobOption(
            label="MTF of the detector:",
            default_value="",
            pattern="STAR Files (*.star)",
            directory=".",
            help_text=(
                "As of release-3.1, the MTF of the detector is used in the refinement"
                " stages of refinement. If you know the MTF of your detector, provide"
                " it here. Curves for some well-known detectors may be downloaded from"
                " the RELION Wiki. Also see there for the exact format \n If you do not"
                " know the MTF of your detector and do not want to measure it, then by"
                " leaving this entry empty, you include the MTF of your detector in"
                " your overall estimated B-factor upon sharpening the map. Although"
                " that is probably slightly less accurate, the overall quality of your"
                " map will probably not suffer very much. \n \n Note that when"
                " combining data from different detectors, the differences between"
                " their MTFs can no longer be absorbed in a single B-factor, and"
                " providing the MTF here is important!"
            ),
            in_continue=True,
        )
        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size (Angstrom):",
            default_value=1.4,
            suggested_min=0.5,
            suggested_max=3,
            step_value=0.1,
            help_text="Pixel size in Angstroms. ",
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["kV"] = IntJobOption(
            label="Voltage (kV):",
            default_value=300,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text="Voltage the microscope was operated on (in kV)",
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Cs"] = FloatJobOption(
            label="Spherical aberration (mm):",
            default_value=2.7,
            suggested_min=0,
            suggested_max=8,
            step_value=0.1,
            help_text=(
                "Spherical aberration of the microscope used to collect these images"
                " (in mm). Typical values are 2.7 (FEI Titan & Talos, most JEOL"
                " CRYO-ARM), 2.0 (FEI Polara), 1.4 (some JEOL CRYO-ARM) and 0.01"
                " (microscopes with a Cs corrector)."
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Q0"] = FloatJobOption(
            label="Amplitude contrast:",
            default_value=0.1,
            suggested_min=0,
            suggested_max=0.3,
            step_value=0.01,
            help_text=(
                "Fraction of amplitude contrast. Often values around 10% work better"
                " than theoretically more accurate lower values..."
            ),
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["beamtilt_x"] = FloatJobOption(
            label="Beamtilt in X (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the X-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["beamtilt_y"] = FloatJobOption(
            label="Beamtilt in Y (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the Y-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )

    def get_commands(self):

        self.command = ["relion_import"]

        if self.joboptions["is_multiframe"].get_boolean():

            fn_out = "movies.star"
            node = Node(
                self.output_name + fn_out, make_node_name(OUTPUT_NODE_MOVIES, ".star")
            )
            self.output_nodes.append(node)
            self.command.append("--do_movies")
        else:
            fn_out = "micrographs.star"
            node = Node(
                self.output_name + fn_out, make_node_name(OUTPUT_NODE_MICS, ".star")
            )
            self.output_nodes.append(node)
            self.command.append("--do_micrographs")

        optics_group = self.joboptions["optics_group_name"].get_string()
        if optics_group == "":
            raise ValueError("ERROR: please specify an optics group name.")
        og = optics_group.replace("$$", "")
        og = og.replace("-", "")
        if not og.isalnum():
            raise ValueError(
                "ERROR: an optics group name may contain only numbers, letters and"
                " hyphens(-)."
            )
        self.command += ["--optics_group_name", "{}".format(optics_group)]

        fn_mtf = self.joboptions["fn_mtf"].get_string()
        if len(fn_mtf) > 0:
            self.command += [
                "--optics_group_mtf",
                fn_mtf,
                "--angpix",
                self.joboptions["angpix"].get_string(),
                "--kV",
                self.joboptions["kV"].get_string(),
                "--Cs",
                self.joboptions["Cs"].get_string(),
                "--Q0",
                self.joboptions["Q0"].get_string(),
                "--beamtilt_x",
                self.joboptions["beamtilt_x"].get_string(),
                "--beamtilt_y",
                self.joboptions["beamtilt_y"].get_string(),
            ]

        fn_in = self.joboptions["fn_in_raw"].get_string()
        self.command += [
            "--i",
            "{}".format(fn_in),
            "--odir",
            self.output_name,
            "--ofile",
            fn_out,
        ]

        if self.is_continue:
            self.command.append("--continue")

        commands = [self.command]
        return commands

    def gather_metadata(self):
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata"""
        datafile = RelionStarFile(os.path.join(self.output_name, "movies.star"))
        metadata = {"MovieCount": datafile.count_block("movies")}
        return metadata

    def prepare_onedep_data(self):
        return prepare_EMPIAR_raw_mics(os.path.join(self.output_name, "movies.star"))

    def create_results_display(self):
        movfile = os.path.join(self.output_name, "movies.star")
        dispobj = mini_montage_from_starfile(
            starfile=movfile,
            block="movies",
            column="_rlnMicrographMovieName",
            outputdir=self.output_name,
            nimg=4,
        )
        return [dispobj]


class RelionImportOther(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_OTHER_NAME

    def __init__(self):
        RelionJob.__init__(self)
        self.do_status_check = False
        self.jobinfo.display_name = "RELION import other files"
        self.jobinfo.short_desc = "Import files other than raw micrographs"
        self.jobinfo.programs = ["relion_import"]
        self.jobinfo.long_desc = (
            "Import files so they are visible to the pipeline. This "
            "step is not always absolutly necessary, but will make the "
            "project more well organised"
        )
        self.command = []
        self.joboptions["fn_in_other"] = FileNameJobOption(
            label="Input file:",
            default_value="",
            pattern=files_exts(name="Input File"),
            directory=".",
            help_text=(
                "Select any file(s) to import. \n \n Note that for importing coordinate"
                " files, one has to give a Linux wildcard, where the *-symbol is before"
                " the coordinate-file suffix, e.g. if the micrographs are called"
                " mic1.mrc and the coordinate files mic1.box or mic1_autopick.star, one"
                " HAS to give '*.box' or '*_autopick.star', respectively.\n \n Also"
                " note that micrographs, movies and coordinate files all need to be in"
                " the same directory (with the same rootnames, e.g.mic1 in the example"
                " above) in order to be imported correctly. 3D masks or references can"
                " be imported from anywhere. \n \n Note that movie-particle STAR files"
                " cannot be imported from a previous version of RELION, as the way"
                " movies are handled has changed in RELION-2.0. \n \n For the import of"
                " a particle, 2D references or micrograph STAR file or of a 3D"
                " reference or mask, only a single file can be imported at a time. \n"
                " \n Note that due to a bug in a fltk library, you cannot import from"
                " directories that contain a substring  of the current directory, e.g."
                " dont important from /home/betagal if your current directory is called"
                " /home/betagal_r2. In this case, just change one of the directory"
                " names."
            ),
            is_required=True,
        )
        self.joboptions["node_type"] = MultipleChoiceJobOption(
            label="Node type:",
            choices=[NODE_TYPES[x] for x in NODE_TYPES],
            default_value_index=0,
            help_text="Select the type of Node this is.",
            is_required=True,
        )
        self.joboptions["optics_group_particles"] = StringJobOption(
            label="Rename optics group for particles:",
            default_value="",
            help_text=(
                "Only for the import of a particles STAR file with a single, or no,"
                " optics groups defined: rename the optics group for the imported"
                " particles to this string."
            ),
            validation_regex="^[a-zA-Z0-9_]*$",
        )

    def get_commands(self):
        # TODO: Check that this is handled correctly, should it be
        # updated from relion4 style autopick files? This type of
        # import should be deprecated and replaced
        self.command = ["relion_import"]
        fn_in = self.joboptions["fn_in_other"].get_string()
        node_type = self.joboptions["node_type"].get_string()
        if node_type == NODE_TYPES["coords_files"]:
            fn_out = "coords_suffix" + fn_in.split("*")[-1]
            node = Node(
                self.output_name + fn_out,
                make_node_name(("MicrographsCoords", ["deprecated_format"]), ".star"),
            )
            self.output_nodes.append(node)
            self.command.append("--do_coordinates")

        else:
            fn_out = os.path.basename("/" + fn_in)
            ex = os.path.splitext(fn_in)[1]
            node_translate = {
                NODE_TYPES["parts_star"]: OUTPUT_NODE_PARTS,
                NODE_TYPES["2drefs"]: OUTPUT_NODE_REF2D_MRC
                if ex == ".mrcs"
                else OUTPUT_NODE_REF2D_STAR,
                NODE_TYPES["3dref"]: OUTPUT_NODE_REF3D,
                NODE_TYPES["3dmask"]: OUTPUT_NODE_MASK3D,
                NODE_TYPES["mics_star"]: OUTPUT_NODE_MICS,
                NODE_TYPES["halfmap"]: OUTPUT_NODE_HALFMAP,
                NODE_TYPES["micrograph"]: OUTPUT_NODE_SINGLE_MIC,
                NODE_TYPES["tomo"]: OUTPUT_NODE_TOMO,
                NODE_TYPES["atomic_model"]: OUTPUT_NODE_MODEL,
                NODE_TYPES["coords_star"]: OUTPUT_NODE_COORDS,
                NODE_TYPES["2D mask"]: OUTPUT_NODE_MASK2D,
            }
            mynodetype = make_node_name(node_translate[node_type], ex)
            print(f"**{mynodetype}")
            node = Node(self.output_name + fn_out, mynodetype)
            self.output_nodes.append(node)

            if mynodetype == make_node_name(OUTPUT_NODE_HALFMAP, ".mrc"):
                self.command.append("--do_halfmaps")
            elif mynodetype == make_node_name(OUTPUT_NODE_PARTS, ".star"):
                self.command.append("--do_particles")
                optics_group = self.joboptions["optics_group_particles"].get_string()
                if optics_group != "":
                    self.command += [
                        "--particles_optics_group_name",
                        '"{}"'.format(optics_group),
                    ]
            else:
                self.command.append("--do_other")

        self.command += ["--i", "{}".format(fn_in)]
        self.command += ["--odir", self.output_name]
        self.command += ["--ofile", fn_out]
        if self.is_continue:
            self.command.append("--continue")
        commands = [self.command]
        return commands

    def gather_metadata(self):
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata.  This could be
        expanded later to collect more detailed info about specific types
        of other imports"""

        jobstar = JobStar(os.path.join(self.output_name, "job.star"))
        joboptions = jobstar.get_all_options()
        for i in joboptions:
            joboptions[i] = format_for_metadata(joboptions[i])
        md_dict = {
            "FileName_in": joboptions["fn_in_other"],
            "NodeType": joboptions["node_type"],
        }
        if joboptions.get("optics_group_particles") is not None:
            md_dict["OpticsGroup"] = joboptions["optics_group_particles"]

        return md_dict

    def prepare_clean_up_lists(self, do_harsh=False):
        """There is no cleanup for this job"""
        pre = "Harsh " if do_harsh else ""
        print(pre + "Cleaning up " + self.output_name)
        return [], []

    def create_results_display(self):
        nt = self.joboptions["node_type"].get_string()
        fn_in = self.joboptions["fn_in_other"].get_string()
        fn_out = os.path.join(self.output_name, os.path.basename("/" + fn_in))
        ex = os.path.splitext(fn_in)[1]

        # defineing what kind of display to make for different node types
        minimontage = {
            NODE_TYPES["parts_star"]: ("particles", "_rlnImageName", 30),
            NODE_TYPES["2drefs"]: (None, "_rlnReferenceImage", -1),
            NODE_TYPES["mics_star"]: ("micrographs", "_rlnMicrographName", 2),
        }
        threed = [
            NODE_TYPES["3dref"],
            NODE_TYPES["halfmap"],
            NODE_TYPES["3dmask"],
        ]
        atm_model = [NODE_TYPES["atomic_model"]]
        just_image = [NODE_TYPES["2D mask"], NODE_TYPES["micrograph"]]

        if nt in minimontage and ex == ".star":
            return [
                mini_montage_from_starfile(
                    starfile=fn_out,
                    block=minimontage[nt][0],
                    column=minimontage[nt][1],
                    nimg=minimontage[nt][2],
                    outputdir=self.output_name,
                )
            ]

        elif nt in minimontage and ex == ".mrcs":
            return [
                mini_montage_from_mrcs_stack(
                    mrcs_file=fn_out, outputdir=self.output_name
                )
            ]

        elif nt in threed:
            return [
                make_map_model_thumb_and_display(
                    maps=[fn_out],
                    outputdir=self.output_name,
                )
            ]

        elif nt in atm_model:
            return [
                make_map_model_thumb_and_display(
                    models=[fn_out],
                    outputdir=self.output_name,
                )
            ]

        elif nt == NODE_TYPES["coords_star"]:
            """make an example micrograph"""
            infile = RelionStarFile(fn_out).get_block("coordinate_files")
            mc_data = infile.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])
            themic, coords = mc_data[0]
            return [
                make_particle_coords_thumb(
                    in_mrc=themic, in_coords=coords, out_dir=self.output_name
                )
            ]

        elif nt == NODE_TYPES["coords_files"]:
            return [
                create_results_display_object(
                    "text",
                    title="! Deprecated particle import !",
                    display_data=(
                        "Import successful, but this type of import is not supported"
                        "by Relion4/CCPEM-pipeliner\nThis will be updated in a future"
                        "Pipeliner version"
                    ),
                    associated_data=[
                        os.path.join(self.output_name, "coords_suffix.star")
                    ],
                )
            ]

        elif nt == NODE_TYPES["tomo"]:
            return [
                create_results_display_object(
                    "text",
                    title="No results display for tomograms yet",
                    display_data=(
                        "Fancy 3D viewer for tomograms has not been implemented yet :("
                    ),
                    associated_data=[fn_out],
                )
            ]
        elif nt in just_image:
            thumbpath = os.path.join(self.output_name, "Thumbnails/image.png")
            print(f"**{thumbpath}")
            bin_mrc_map(
                in_map=fn_out,
                dim=640,
                out_name=thumbpath,
            )
            return [
                create_results_display_object(
                    "image",
                    title=f"Imported {nt}",
                    image_path=thumbpath,
                    image_desc=fn_out,
                    associated_data=[fn_out],
                )
            ]

        else:
            return [
                create_results_display_object(
                    "text",
                    title=f"No results display for {nt}",
                    display_data=(
                        f"This nodee type {nt}, currently has no graphical display"
                    ),
                    associated_data=[fn_out],
                )
            ]
