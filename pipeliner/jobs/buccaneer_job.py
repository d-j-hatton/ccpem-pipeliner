#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import json
import os
from typing import List
from pipeliner.pipeliner_job import PipelinerJob, Ref
from pipeliner.job_options import (
    FileNameJobOption,
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import Node
from pipeliner.display_tools import make_map_model_thumb_and_display

# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_SEQ = "Sequence.fasta.protein"
# output nodes
OUTPUT_NODE_MODEL = "AtomCoords.pdb.buccaneer"


class BuccaneerJob(PipelinerJob):
    PROCESS_NAME = "buccaneer.modelbuild"
    OUT_DIR = "Buccaneer"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Buccaneer"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.short_desc = "Automated model building"
        self.jobinfo.long_desc = (
            "Buccaneer performs statistical chain"
            " tracing by identifying connected alpha-carbon positions using a "
            "likelihood-based density target. The target distributions are generated"
            "by a simulation calculation using a known reference structure for which"
            " calculated phases are available. The success of the method is dependent"
            " on the features of the reference structure matching those of the "
            "unsolved, work structure. For almost all cases, a single reference "
            "structure can be used, with modifications automatically applied to "
            "the reference structure to match its features to the work structure."
            " N.B. requires CCP4."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/cbuccaneer.html"
        self.jobinfo.programs = ["ccpem-buccaneer"]
        self.jobinfo.references = [
            Ref(
                authors=["Cowtan K"],
                title=(
                    "The Buccaneer software for automated model building."
                    "1. Tracing protein chains"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2006",
                volume="62",
                issue="Pt 9",
                pages="1002-11",
                doi="10.1107/S0907444906022116",
            ),
            Ref(
                authors=["Hoh SW", "Burnley T", "Cowtan K"],
                title=(
                    "Current approaches for automated model building"
                    " into cryo-EM maps using Buccaneer with CCP-EM."
                ),
                journal="Acta Crystallogr D Struct Biol",
                year="2020",
                volume="76",
                issue="Pt 6",
                pages="531-541",
                doi="10.1107/S2059798320005513",
            ),
        ]

        self.joboptions["job_title"] = StringJobOption(
            label="Job title",
            default_value="",
            help_text=(
                "This is the title of the job.  It can be a long string with spaces"
            ),
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Input map resolution",
            default_value=3,
            suggested_min=1,
            suggested_max=10,
            step_value=0.1,
            help_text="The resolution of the input map",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["input_seq"] = FileNameJobOption(
            label="Input sequence",
            default_value="",
            pattern="Sequence (.fasta)",
            directory=".",
            help_text="The input sequence",
            is_required=True,
        )

        # what is this job option exactly??
        self.joboptions["extend_pdb"] = FileNameJobOption(
            label="Extend existing pdb",
            default_value="",
            pattern="pdb atomic model (.pdb)",
            directory=".",
            help_text="The pdb to be extended",
        )

        self.joboptions["ncycle"] = IntJobOption(
            label="Number of cycles",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text="The number of cycles to perform",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_refmac"] = IntJobOption(
            label="Number of refmac cycles",
            default_value=20,
            suggested_min=1,
            suggested_max=60,
            step_value=1,
            help_text="How many cycles of refmac to run",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_buc1st"] = IntJobOption(
            label="Number of 1st cycle iterations",
            default_value=-1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of internal buccaneer cycles to run on the first "
                "iteration. Default 3"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_bucnth"] = IntJobOption(
            label="Number of subsequent cycle iterations",
            default_value=-1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of internal buccaneer cycles to run on subsequent iterations."
                " Default 2."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["map_sharpen"] = FloatJobOption(
            label="Map sharpening",
            default_value=0.0,
            suggested_min=0,
            suggested_max=-200,
            step_value=1,
            help_text="Sharpening to apply to the map",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncpus"] = IntJobOption(
            label="Number of CPUS to use",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text="Number of CPUS",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["lib_in"] = StringJobOption(
            label="what is this?",
            default_value="",
            help_text="Matt doesn't know what this option is",
            in_continue=True,
        )

        self.joboptions["keywords"] = StringJobOption(
            label="Additional keyword arguments for Buccaneer",
            default_value="",
            help_text="See the Buccaneer documentation",
        )

        self.joboptions["refmac_keywords"] = StringJobOption(
            label="Additional keyword arguments for refmac",
            default_value="",
            help_text="See the refmac documentation",
        )

        self.get_runtab_options(False, False)

    def get_commands(self) -> List[List[str]]:
        args_file = "tmp_buccaneer_args.json"

        # make the input nodes first so if there are failures here
        # the job directory doesn't get made
        input_map = self.joboptions["input_map"].get_string(
            True, "ERROR: input map not found"
        )
        self.input_nodes.append(Node(input_map, INPUT_NODE_MAP))
        input_seq = self.joboptions["input_seq"].get_string(
            True, "ERROR: input sequence not found"
        )
        self.input_nodes.append(Node(input_seq, INPUT_NODE_SEQ))

        # write the args file
        jobops_dict = {}

        # some requirements for parameters
        nulls = ["extend_pdb", "lib_in"]  # values that need None for empty value
        abspaths = ["input_map", "input_seq"]  # does extend pdb need to be here too?

        # relion job options not written to the buccaneer_args json file
        nonwritten = [
            "do_queue",
            "min_dedicated",
            "qsub",
            "qsubscript",
            "queuename",
            "other_args",
        ]

        for jobop in self.joboptions:
            if jobop not in nonwritten:
                val = self.joboptions[jobop].get_string()

                # format the values
                try:
                    val = float(val)
                except ValueError:
                    pass

                # files need to abs paths?
                if jobop in abspaths:
                    val = os.path.abspath(val)

                # some fields need null for empties, some need ""
                if jobop in nulls and val == "":
                    val = None

                jobops_dict[jobop] = val

        with open(args_file, "w") as fp:
            json.dump(jobops_dict, fp)

        # assemble commands
        command = [
            "ccpem-buccaneer",
            "--no-gui",
            "--args",
            args_file,
            "--job_location",
            self.output_name,
        ]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command.extend(self.parse_additional_args())

        commands = [command, ["rm", "tmp_buccaneer_args.json"]]

        self.output_nodes.append(
            Node(self.output_name + "refined1.pdb", OUTPUT_NODE_MODEL)
        )

        # do_makedir is always false because it was created above
        return commands

    def create_results_display(self):
        model = self.output_name + "refined1.pdb"
        map = self.joboptions["input_map"].get_string()
        dispobj = make_map_model_thumb_and_display(
            maps=[map],
            models=[model],
            outputdir=self.output_name,
        )
        return [dispobj]
