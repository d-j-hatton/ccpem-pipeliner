#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
)
from pipeliner.data_structure import Node
from pipeliner.display_tools import create_results_display_object

# input nodes
INPUT_NODE_PARTS = "ParticlesData.star.relion"

# output nodes
OUTPUT_NODE_MAP = "DensityMap.mrc.relion.reconstruct"


class RelionReconstruct(RelionJob):
    PROCESS_NAME = "relion.reconstruct"
    OUT_DIR = "Reconstruct"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "RELION Reconstruct"

        self.jobinfo.short_desc = (
            "Generate a map from a set of particles using relion_reconstruct"
        )
        self.jobinfo.long_desc = (
            "For more information see the help screen by running: 'relion_reconstruct"
            " --h'"
        )
        self.jobinfo.programs = ["relion_reconstruct", "relion_reconstruct_mpi"]

        self.joboptions["input_particles"] = InputNodeJobOption(
            label="Input particles file:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern="Particles star file (.star)",
            help_text=(
                "Input STAR file with the projection images and their orientations"
            ),
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Pixel size in the reconstruction (take from first optics group by"
                " default)"
            ),
            is_required=True,
        )

        self.joboptions["sym"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text="Symmetry to apply IE: C2",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["maxres"] = FloatJobOption(
            label="Maximum resolution to consider:",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Maximum resolution (in Angstrom) to consider in Fourier space (default"
                " Nyquist)"
            ),
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options(mpi=True, threads=False)

    def get_commands(self):

        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            command = ["mpirun", "-n", nr_mpi, "relion_reconstruct_mpi"]
        else:
            command = ["relion_reconstruct"]

        input_file = self.joboptions["input_particles"].get_string()
        command += ["--i", input_file]

        output_file = "{}reconstruction.mrc".format(self.output_name)
        command += ["--o", output_file]
        self.output_nodes.append(Node(output_file, OUTPUT_NODE_MAP))

        max_res = self.joboptions["maxres"].get_number()
        if max_res > 0:
            command += ["--maxres", str(max_res)]

        angpix = self.joboptions["angpix"].get_number()
        if angpix > 0:
            command += ["--angpix", str(angpix)]

        sym = self.joboptions["sym"].get_string()
        sym = "C1" if sym == "" else sym
        command += ["--sym", sym]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        commands = [command]
        return commands

    def create_results_display(self):
        outmap = os.path.join(self.output_name, "reconstruction.mrc")
        return [
            create_results_display_object(
                type="mapmodel",
                title="Reconstructed map",
                associated_data=[outmap],
                maps=[outmap],
            )
        ]
