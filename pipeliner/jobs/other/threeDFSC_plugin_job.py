#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import re
from gemmi import cif
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    StringJobOption,
    InputNodeJobOption,
    BooleanJobOption,
    FloatJobOption,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref

# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_MASK = "Mask3D.mrc"
INPUT_NODE_HALFMAP = "DensityMap.mrc.halfmap"

# output nodes
OUTPUT_NODE_THREEDFSC = "Image3D.mrc.theedfsc.3dfsc"


class ThreeDFSC(PipelinerJob):
    PROCESS_NAME = "threedfsc.map_analysis"
    OUT_DIR = "ThreeDFSC"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "3DFSC"
        self.jobinfo.short_desc = (
            "Calculate the 3-dimensional Fourier Shell Correlation for a reconstruction"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = ["ThreeDFSC_Start.py"]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Tan YZ",
                    "Baldwin PR",
                    "Davis JH",
                    "Williamson JR",
                    "Potter CS",
                    "Carragher B",
                    "Lyumkis D",
                ],
                title=(
                    "Addressing preferred specimen orientation in single-particle"
                    " cryo-EM through tilting"
                ),
                journal="Nature Methods",
                year="2017",
                volume="14",
                issue="8",
                pages="793-796",
                doi="10.1038/nmeth.4347",
            )
        ]
        self.jobinfo.documentation = "https://github.com/nysbc/Anisotropy"
        self.joboptions["fn_halfmap"] = InputNodeJobOption(
            label="Halfmap file:",
            node_type=INPUT_NODE_HALFMAP,
            default_value="",
            directory="",
            pattern="Halfmap MRC file (*.mrc)",
            help_text=(
                "First half map of 3D reconstruction. MRC format. Can be masked or"
                " unmasked. "
            ),
            is_required=True,
        )

        self.joboptions["fn_fullmap"] = InputNodeJobOption(
            label="Map file:",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="MRC 3D map file (*.mrc)",
            help_text=(
                " Full map of 3D reconstruction. MRC format. Can be masked or unmasked,"
                " can be sharpened or unsharpened."
            ),
            is_required=True,
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="Mask file:",
            node_type=INPUT_NODE_MASK,
            default_value="",
            directory="",
            pattern="MRC mask map file (*.mrc)",
            help_text=(
                "(optional) If given, it would be used to mask the half maps during"
                " 3DFSC generation and analysis."
            ),
            in_continue=True,
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Pixel size",
            default_value=1.0,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text="Pixel size of the maps in A",
            is_required=True,
        )

        self.joboptions["dtheta"] = FloatJobOption(
            label="Sampling angle:",
            default_value=-1,
            suggested_min=0,
            suggested_max=90,
            step_value=1,
            help_text=(
                "Angle of cone to be used for 3D FSC sampling in degrees. Default is 20"
                " degrees."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["fsc_cutoff"] = FloatJobOption(
            label="FSC cutoff:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text="FSC cutoff criterion. Default is 0.143",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["sphericity_threshold"] = FloatJobOption(
            label="Sphericity threshold",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Threshold value for 3DFSC volume for calculating sphericity. 0.5 is"
                " default."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="High pass filter:",
            default_value=-1,
            suggested_min=50,
            suggested_max=500,
            step_value=1,
            help_text=(
                "High pass filter for thresholding in Angstrom. Prevents small dips in"
                " directional FSCs at low spatial frequency due to noise from messing"
                " up the thresholding step. Decrease if you see a huge wedge missing"
                " from your thresholded 3DFSC volume. 200 Angstroms is default."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["nr_sphericity_thresholds"] = FloatJobOption(
            label="Number of sphericity thresholds:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Calculate sphericities at different threshold cutoffs to determine"
                " sphericity deviation across spatial frequencies. This can be useful"
                " to evaluate possible effects of overfitting or improperly assigned"
                " orientations. Default is 1"
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU?",
            default_value=False,
            help_text="Use GPU for the calculation?",
            in_continue=True,
        )

        self.joboptions["gpu_id"] = StringJobOption(
            label="GPU ID:",
            default_value="",
            help_text="Specify GPU to use.  Only a single GPU can be used",
            in_continue=True,
            deactivate_if=[("use_gpu", "=", False)],
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        command = ["ThreeDFSC_Start.py"]

        # get the halfmap1
        fn_half1 = self.joboptions["fn_halfmap"].get_string(
            True, "ERROR: No halfmap specified"
        )

        if not os.path.isfile(fn_half1):
            raise ValueError("ERROR: Cannot find halfmap file {}".format(fn_half1))
        if "half1" not in os.path.basename(fn_half1):
            raise ValueError(
                "ERROR: Cannot find 'half' substring in the halfmap input filename"
            )

        # confirm existence of 2nd halfmap
        fn_half2 = fn_half1.replace("_half1", "_half2")
        if not os.path.isfile(fn_half2):
            raise ValueError(
                "ERROR: Could not find corresponding 2nd half map {}".format(fn_half2)
            )

        command += [
            "--halfmap1={}".format(fn_half1),
            "--halfmap2={}".format(fn_half2),
        ]

        # get the full map
        fn_fullmap = self.joboptions["fn_fullmap"].get_string(
            True, "ERROR: No full map specified"
        )

        if not os.path.isfile(fn_fullmap):
            raise ValueError("ERROR: Full map file {} not found".format(fn_fullmap))

        command.append("--fullmap=" + fn_fullmap)

        # get the mask if used
        fn_mask = self.joboptions["fn_mask"].get_string()
        if len(fn_mask) > 0:
            command.append("--mask={}".format(fn_mask))

        apix = self.joboptions["apix"].get_number()

        # get the apix from the model.star file if necessary
        if apix < 0:
            in_dir = os.path.dirname(fn_half1)
            fn_model = os.path.join(in_dir, "run_model.star")
            if not os.path.isfile(fn_model):
                raise ValueError(
                    "ERROR: Cannot file the file {} to automatically get pixel"
                    " size".format(fn_model)
                )
            modelfile = cif.read_file(fn_model)
            genblock = modelfile["model_general"]
            apix_pair = genblock.find_pair("_rlnPixelSize")
            apix = apix_pair[1]

        command.append("--apix=" + str(apix))

        dtheta = self.joboptions["dtheta"].get_number()
        if dtheta > 0:
            command.append("--dthetaInDegrees=" + str(dtheta))

        fsc_cutoff = self.joboptions["fsc_cutoff"].get_number()
        if fsc_cutoff > 0:
            command.append("--FSCCutoff=" + str(fsc_cutoff))

        sphericity_thresh = self.joboptions["sphericity_threshold"].get_number()
        if sphericity_thresh > 0:
            command.append("--ThresholdForSphericity=" + str(sphericity_thresh))

        highpass = self.joboptions["highpass"].get_number()
        if highpass > 0:
            command.append("--HighPassFilter=" + str(highpass))

        nr_sphericity_thresh = int(
            self.joboptions["nr_sphericity_thresholds"].get_number()
        )
        if nr_sphericity_thresh > 1:
            command.append(
                "--numThresholdsForSphericityCalcs=" + str(nr_sphericity_thresh)
            )

        do_gpu = self.joboptions["use_gpu"].get_boolean()
        if do_gpu:
            gpu_id = self.joboptions["gpu_id"].get_string()
            if gpu_id == "":
                raise ValueError("ERROR: A GPU must be specified for ThreeDFSC")
            gpus = re.split(r"\D+", gpu_id)
            if len(gpus) > 1:
                print(
                    "WARNING: ThreeDFSC can only use 1 GPU.  Defaulting to the first"
                    " specified GPU ({})".format(gpus[0])
                )
            command += ["--gpu", "--gpu_id=" + gpus[0]]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command += "X" + self.parse_additional_args()

        # append the extra commands to move the threedfsc outputs
        # to the relion output directory

        move_command = ["mv", "Results_3DFSCOutput/*", self.output_name]
        rm_command = ["rm", "-r", "Results_3DFSCOutput"]

        outfile = os.path.join(self.output_name, "3DFSCOutput.mrc")
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_THREEDFSC))
        commands = [command, move_command, rm_command]

        return commands
