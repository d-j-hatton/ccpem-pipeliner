#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from gemmi import cif

from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)

# input nodes
INPUT_NODE_PARTS = "ParticlesData.star.relion"

# output nodes
OUTPUT_NODE_PS = "Image3D.mrc.cryoef.realspace_powerspectrum"
OUTPUT_NODE_TF = "Image3D.mrc.cryoef.fourierspace_transferfunction"
OUTPUT_NODE_LOG = "LogFile.txt.cryoef"


class CryoEF(PipelinerJob):
    PROCESS_NAME = "cryoef.map_analysis"
    OUT_DIR = "CryoEF"

    def __init__(self):
        super(self.__class__, self).__init__()

        self.jobinfo.display_name = "cryoEF"

        self.jobinfo.short_desc = "Analyse distribution of particle orientations"
        self.jobinfo.long_desc = (
            "Describes the quality of an orientation distribution in terms of providing"
            " uniform resolution in all directions, by a single number - the"
            " efficiency. CryoEF will assist in determining to what extent this affects"
            " the resolution of the 3D reconstruction.The efficiency score, calculated"
            " by cryoEF, measures the ability of the distribution to provide uniform"
            " information and resolution in all directions of the reconstruction,"
            " independent of other factors. This metric allows rapid and rigorous"
            " evaluation of specimen preparation methods, assisting structure"
            " determination to high resolution with minimal data. Also included is an"
            " algorithm for predicting optimal tilt angles for data collection."
        )
        self.jobinfo.programs = ["cryoEF"]
        self.version = "0.1"
        self.job_author = "Matt Iadanza"
        self.jobinfo.references = [
            Ref(
                authors=["Naydenova K", "Russo CJ"],
                title=(
                    "Measuring the effects of particle orientation to improve the"
                    " efficiency of electron cryomicroscopy."
                ),
                journal="Nat. Commun.",
                year="2017",
                volume="8",
                issue="1",
                pages="629",
                doi="10.1038/s41467-017-00782-3.",
            )
        ]
        self.jobinfo.documentation = (
            "https://www.mrc-lmb.cam.ac.uk/crusso/cryoEF/docs.html"
        )

        self.joboptions["input_file"] = InputNodeJobOption(
            label="Input file name:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern=files_exts("Particles file or angles file", [".star", ".txt"]),
            help_text=(
                "The input file can be either a Relion particles star file"
                "or a .txt file with two columns"
            ),
            # automatic node creation off; node type must be determined from file
            is_required=True,
        )

        self.joboptions["b_factor"] = FloatJobOption(
            label="B-factor:",
            default_value=-300,
            suggested_min=-1000,
            suggested_max=0,
            step_value=1,
            help_text="B-factor in Angstroms^2 (optional)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["particle_diameter_ang"] = FloatJobOption(
            label="Particle diameter:",
            default_value=-1,
            suggested_min=10,
            suggested_max=500,
            step_value=1,
            help_text="particle diameter in Angstrom (optional)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["box_size_px"] = IntJobOption(
            label="Box size:",
            default_value=-1,
            suggested_min=20,
            suggested_max=1000,
            step_value=1,
            help_text="Box size in pixels (optional)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution:",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Resolution in Angstrom (optional)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["symmetry"] = StringJobOption(
            label="Symmetry:",
            default_value="",
            help_text="Particle symmetry group (e.g. C1)",
            in_continue=True,
        )

        self.joboptions["angular_acc"] = FloatJobOption(
            label="Angular accuracy:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=2.0,
            step_value=0.1,
            help_text="Angular accuracy in degrees (optional)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["max_tilt_angle"] = FloatJobOption(
            label="Maximum tilt angle:",
            default_value=-1,
            suggested_min=0,
            suggested_max=90,
            step_value=1,
            help_text="Maximum tilt angle in degrees (optional)",
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        command = ["cryoEF"]

        # get input file

        raw_input_file = self.joboptions["input_file"].get_string()
        self.input_nodes.append(Node(raw_input_file, INPUT_NODE_PARTS))

        # convert input an angles file
        in_star = cif.read_file(raw_input_file)
        parts_block = in_star.find_block("particles")
        phi_angs = parts_block.find_loop("_rlnAngleTilt")
        psi_angs = parts_block.find_loop("_rlnAngleRot")
        input_file = "cryoef_angles.dat"
        with open(input_file, "w") as output_file:
            for pair in zip(phi_angs, psi_angs):
                output_file.write("{}\n".format("\t".join(pair)))

        b_factor = self.joboptions["b_factor"].get_number()
        particle_diameter_ang = self.joboptions["particle_diameter_ang"].get_number()
        box_size_px = self.joboptions["box_size_px"].get_number()
        res = self.joboptions["resolution"].get_number()
        sym = self.joboptions["symmetry"].get_string()
        max_tilt = self.joboptions["max_tilt_angle"].get_number()
        angular_acc = self.joboptions["angular_acc"].get_number()
        command += ["-f", os.path.join(self.output_name, "cryoef_angles.dat")]

        if b_factor > 0:
            command += ["-B", str(b_factor)]

        if particle_diameter_ang > 0:
            command += ["-D", str(particle_diameter_ang)]

        if box_size_px > 0:
            command += ["-b", str(int(box_size_px))]

        if res > 0:
            command += ["-r", str(res)]

        if len(sym) > 0:
            command += ["-g", sym]

        if max_tilt > 0:
            command += ["-m", str(max_tilt)]

        if angular_acc > 0:
            command += ["-a", str(angular_acc)]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        self.output_nodes.append(
            Node(
                os.path.join(self.output_name, "cryoef_angles_K.mrc"),
                OUTPUT_NODE_TF,
            )
        )
        self.output_nodes.append(
            Node(
                os.path.join(self.output_name, "cryoef_angles_R.mrc"),
                OUTPUT_NODE_PS,
            )
        )
        self.output_nodes.append(
            Node(
                os.path.join(self.output_name, "cryoef_angles.log"),
                OUTPUT_NODE_LOG,
            )
        )

        commands = [
            [
                "mv",
                "cryoef_angles.dat",
                os.path.join(self.output_name, "cryoef_angles.dat"),
            ],
            command,
        ]

        return commands

    def create_results_display(self):
        logfile = os.path.join(self.output_name, "cryoef_angles.log")
        with open(logfile, "r") as lf:
            loglines = lf.readlines()

        # recommenations table
        headers = ["Tilt angle", "Reccomended particles to collect"]
        recs = []
        for n, line in enumerate(loglines):
            if "Recommended tilt angle:" in line:
                recs.append([line.split()[-2], loglines[n + 1].split()[3] + "x"])
        recs_table = (
            create_results_display_object(
                "table",
                title="Tilted collection recommendations",
                headers=headers,
                table_data=recs,
                associated_data=[logfile],
            )
            if recs
            else None
        )

        # Fourier space completeness map
        fc_map = make_map_model_thumb_and_display(
            outputdir=self.output_name,
            maps=[os.path.join(self.output_name, "cryoef_angles_K.mrc")],
            title="Fourier space information density",
        )

        # Histogram
        datfile = os.path.join(self.output_name, "cryoef_angles_PSFres.dat")
        with open(datfile, "r") as df:
            histodat = [float(x) for x in df.readlines()]

        histo = create_results_display_object(
            "histogram",
            title="Distribution of PSF resolution",
            data_to_bin=histodat,
            xlabel="Resolution",
            ylabel="Number of orientations",
            associated_data=[datfile],
        )

        if recs_table:
            return [recs_table, histo, fc_map]
        else:
            return [histo, fc_map]
