#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref
from pipeliner.jobs.other.model_validation import validation_results
from pipeliner.display_tools import create_results_display_object

# this is a workaround until TEMPy is packaged with the pipeliner
import shutil
import json
import pickle

ccpem_path = shutil.which("ccpem-python")
if ccpem_path:
    try:
        from ccpem_core.tasks.tempy.smoc import smoc_process

        smoc_path = os.path.realpath(smoc_process.__file__)
    except ImportError:  # if ccpem paths are not set in PYTHONPATH
        smoc_path = (
            ccpem_path.split("/bin")[0]
            + "/lib/py2/ccpem/"
            + "src/ccpem_core/tasks/tempy/smoc/smoc_process.py"
        )
    try:
        from ccpem_core.tasks.atomic_model_validation.bfactor_analysis import (
            analyse_bfactors,
        )

        bfactor_path = os.path.realpath(analyse_bfactors.__file__)
    except ImportError:  # if ccpem paths are not set in PYTHONPATH
        bfactor_path = (
            ccpem_path.split("/bin")[0]
            + "/lib/py2/ccpem/"
            + "src/ccpem_core/tasks/atomic_model_validation/"
            "bfactor_analysis/analyse_bfactors.py"
        )

results_path = os.path.realpath(validation_results.__file__)
# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_MODEL = "AtomCoords"
# INPUT_NODE_LIGAND = "LigandDescription"

# output nodes
# OUTPUT_NODE_VALSUMM = "LogFile.txt.model_validation.outliersummary"
OUTPUT_NODE_SMOCMODEL = "AtomCoords.pdb.tempy.bfactor_smoc"
OUTPUT_NODE_SMOCSCORES = "Logfile.csv.tempy.smoc_scores"
OUTPUT_NODE_MOLPROBITY = "Logfile.txt.molprobity.output"
OUTPUT_NODE_MODELVAL_GLOBAL = "Logfile.txt.modelval.glob_output"
OUTPUT_NODE_MODELVAL_LOCAL = "Logfile.txt.modelval.local_output"
OUTPUT_NODE_MODELVAL_CLUSTER = "Logfile.csv.modelval.cluster_output"


class ModelValidate(PipelinerJob):
    PROCESS_NAME = "pipeliner.model_validation.evaluate"
    OUT_DIR = "Model_Validation"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Model Validation"
        self.jobinfo.short_desc = "Atomic model validation"
        self.jobinfo.long_desc = (
            "Validate protein atomic model quality and agreement with map."
            "Reports outliers based on:"
            "Stereochemistry:  (Molprobity),"
            "Local fit of atomic model in density: (TEMPy SMOC),"
        )
        self.jobinfo.programs = ["molprobity.reduce", "molprobity.molprobity"]
        self.version = "0.2"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Williams C et al."],
                title=(
                    "MolProbity: More and better reference data for improved"
                    " all-atom structure validation ."
                ),
                journal="Protein Sci",
                year="2018",
                volume="27",
                issue="1",
                pages="293-315",
                doi="10.1002/pro.3330",
            ),
            Ref(
                authors=["Joseph et al."],
                title=(
                    "Refinement of atomic models in high resolution "
                    "EM reconstructions using Flex-EM and local assessment."
                ),
                journal="Methods",
                year="2016",
                volume="100",
                issue="1",
                pages="42-49",
                doi="10.1016/j.ymeth.2016.03.007",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S205979832101278X"

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=INPUT_NODE_MODEL,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be evaluated",
            is_required=True,
        )

        # self.joboptions["input_ligand"] = InputNodeJobOption(
        #     label="Input ligand",
        #     node_type=INPUT_NODE_LIGAND,
        #     pattern="Ligand definition (.cif)",
        #     default_value="",
        #     directory="",
        #     help_text="The input model",
        # )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map to evaluate the model against",
            is_required=False,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in Angstrom",
            required_if=[("input_map", "!=", "")],
            deactivate_if=[("input_map", "=", "")],
        )

        # self.joboptions["additional_map1"] = InputNodeJobOption(
        #     label="Input additional map 1 (half map 1 or independent map1)",
        #     node_type=INPUT_NODE_MAP,
        #     default_value="",
        #     directory="",
        #     pattern="3D map (.mrc)",
        #     help_text="Additional input map to evaluate the model against",
        #     is_required=False,
        # )
        #
        # self.joboptions["additional_map2"] = InputNodeJobOption(
        #     label="Input additional map 2 (half map 2 or independent map2)",
        #     node_type=INPUT_NODE_MAP,
        #     default_value="",
        #     directory="",
        #     pattern="3D map (.mrc)",
        #     help_text="Additional input map to evaluate the model against",
        #     required_if=[("half_map_refinement", "=", True)],
        # )

        self.joboptions["run_molprobity"] = BooleanJobOption(
            label="Molprobity",
            default_value=True,
            help_text="Run Molprobity geometry evaluation",
        )
        self.joboptions["run_smoc"] = BooleanJobOption(
            label="TEMPy SMOC",
            default_value=True,
            help_text="Run SMOC model-map fit evaluation",
        )

        self.get_runtab_options(mpi=False, threads=False)

    def encode(self):
        return self.__dict__

    def get_commands(self):

        # pipeline dir
        self.pipeline_dir = os.getcwd()
        # make the output directory
        # normally this wouldn't be necessary, but the input file needs to be
        # in the output directory
        os.makedirs(self.output_name)
        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_model = os.path.join(self.pipeline_dir, input_model)

        input_map = self.joboptions["input_map"].get_string(False, "Input file missing")
        self.input_map = os.path.join(self.pipeline_dir, input_map)

        resolution = self.joboptions["resolution"].get_number()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        # bfactor distribution
        commands = []
        if bfactor_path:
            pdb_num = 1  # this needs to be set for multiple model input
            bfact_command = ["ccpem-python", bfactor_path]
            bfact_command += [self.input_model]
            commands += [bfact_command]
        # molprobity
        if run_molprobity:
            # this needs fixing: reduce output pdb is printed in stdout
            # and needs to be saved in a file for molprobity input
            # running the command in a separate shell to redirect output
            reduce_command = ["sh", "-c"]
            if (
                os.path.splitext(self.input_model)[-1].lower() == ".cif"
                or os.path.splitext(self.input_model)[-1].lower() == ".mmcif"
            ):
                reduce_out = "reduce_out.cif"
            else:
                reduce_out = "reduce_out.pdb"
            # reduce_out = os.path.join(self.output_name, reduce_out)
            reduce_run = " ".join(
                [
                    self.jobinfo.programs[0],
                    "-FLIP",
                    "-Quiet",
                    self.input_model,
                    ">",
                    reduce_out,
                ]
            )
            reduce_command += [reduce_run]
            molprobity_command = [self.jobinfo.programs[1], reduce_out]
            molprobity_command += ["output.percentiles=True"]
            commands += [reduce_command, molprobity_command]
            self.molprobity_output = os.path.join(self.output_name, "molprobity.out")
            self.output_nodes.append(
                Node(self.molprobity_output, OUTPUT_NODE_MOLPROBITY)
            )
            self.molprobity_output = os.path.realpath(self.molprobity_output)
        # smoc
        if run_smoc:
            if smoc_path:
                pdb_num = 1  # this needs to be set for multiple model input
                smoc_command = ["ccpem-python", smoc_path]
                dict_smoc_args = {}
                dict_smoc_args["map_path"] = self.input_map
                dict_smoc_args["input_pdbs"] = [self.input_model]
                dict_smoc_args["map_resolution"] = resolution
                dict_smoc_args["dist_or_fragment_selection"] = "dist"
                dict_smoc_args["use_smoc"] = True
                dict_smoc_args["auto_fragment_length"] = True
                # smoc_process.py expects these arguments as well
                dict_smoc_args["rigid_body_path"] = None
                dict_smoc_args["rigid_body_dir"] = None
                dict_smoc_args["job_location"] = None
                smoc_args = os.path.join(self.output_name, "smoc_args.json")
                with open(smoc_args, "w") as s:
                    json.dump(dict_smoc_args, s)
                smoc_command += ["smoc_args.json", "False"]
                commands += [smoc_command]
                modelid = os.path.splitext(os.path.basename(input_model))[0]
                smoc_modelid = modelid + "_" + str(pdb_num)
                # writes out only pdb format, needs to include cif output
                output_modelfile = os.path.join(
                    self.output_name, smoc_modelid + "_smoc.pdb"
                )
                self.smoc_output = os.path.join(
                    self.output_name, smoc_modelid + "_smoc.csv"
                )
                self.output_nodes.append(Node(output_modelfile, OUTPUT_NODE_SMOCMODEL))
                self.output_nodes.append(Node(self.smoc_output, OUTPUT_NODE_SMOCSCORES))
                self.smoc_output = os.path.realpath(self.smoc_output)
                self.smoc_df_output = os.path.join(self.output_name, "smoc_score.csv")
                self.smoc_df_output = os.path.realpath(self.smoc_df_output)
        pipeliner_pkl = os.path.join(self.pipeline_dir, "pipeliner_jobobj.pkl")
        with open(pipeliner_pkl, "wb") as p:
            pickle.dump(self, p, pickle.HIGHEST_PROTOCOL)
        results_command = ["python3", results_path, pipeliner_pkl]
        commands += [results_command]

        glob_output_file = "global_report.txt"
        global_output = os.path.join(self.output_name, glob_output_file)
        self.output_nodes.append(Node(global_output, OUTPUT_NODE_MODELVAL_GLOBAL))

        modelid = os.path.splitext(os.path.basename(input_model))[0]
        local_output = os.path.join(self.output_name, modelid + "_outliersummary.txt")
        self.output_nodes.append(Node(local_output, OUTPUT_NODE_MODELVAL_LOCAL))
        cluster_output = os.path.join(self.output_name, "outlier_clusters.csv")
        self.output_nodes.append(Node(cluster_output, OUTPUT_NODE_MODELVAL_CLUSTER))
        # job run directory
        self.subprocess_cwd = self.output_name

        return commands

    def create_results_display(self):
        # make SMOC plots
        smoc_csvname = os.path.basename(
            self.joboptions["input_model"].get_string()
        ).split(".")[0]
        smoc_csv = os.path.join(self.output_name, smoc_csvname + "_1_smoc.csv")
        with open(smoc_csv, "r") as smoccsv:
            smoc_data = [x.split(",") for x in smoccsv.readlines()]
        smoc_chains = {}
        for line in smoc_data[1:]:
            if line[0] not in smoc_chains:
                smoc_chains[line[0]] = [[line[1]], [line[2]]]
            else:
                smoc_chains[line[0]][0].append(line[1])
                smoc_chains[line[0]][1].append(line[2])

        dos_out = []
        for chain in smoc_chains:
            dos_out.append(
                create_results_display_object(
                    "graph",
                    xvalues=[smoc_chains[chain][0]],
                    yvalues=[smoc_chains[chain][1]],
                    title=f"SMOC score chain {chain}",
                    associated_data=[smoc_csv],
                    data_series_labels=[f"Chain {chain}"],
                    xaxis_label="Residue",
                    yaxis_label="SMOC score",
                )
            )

        # make molprobity table
        mp_csv = os.path.join(self.output_name, "molprobity_table.csv")
        with open(mp_csv, "r") as mpcsv:
            mp_data = [x.split(",") for x in mpcsv.readlines()]

        labels = ["Metric", "Score", "Percentile"]
        molprob_data = []
        for line in mp_data:
            sc_pct = line[1].split("[")
            sc = sc_pct[0].strip(" \n")
            pct = sc_pct[1].strip(" []()%\npercentile:")
            molprob_data.append([line[0], sc, pct])

        dos_out.append(
            create_results_display_object(
                "table",
                title="Molprobity scores",
                headers=labels,
                table_data=molprob_data,
                associated_data=[mp_csv],
            )
        )

        # make clusters
        clusters_file = os.path.join(self.output_name, "outlier_clusters.csv")
        with open(clusters_file, "r") as cf:
            clusters_data = [x.split(",") for x in cf.readlines()]

        clusters, n, reset = [], 0, False
        for line in clusters_data[3:]:
            if line[0] != "-":
                clusters.append([f"{list(smoc_chains)[n]}:{line[0]}"] + line[1:])
                reset = True
            if line[0] == "-" and reset:
                n += 1
                reset = False

        dos_out.append(
            create_results_display_object(
                "table",
                title="Outlier clusters",
                headers=[
                    "Chain:Cluster",
                    "Residue",
                    "SMOC outlier",
                    "Molprobity Outlier",
                ],
                table_data=clusters,
                associated_data=[clusters_file],
            )
        )

        return dos_out
