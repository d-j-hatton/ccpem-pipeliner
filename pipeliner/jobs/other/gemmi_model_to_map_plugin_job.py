#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.display_tools import make_map_model_thumb_and_display

INPUT_NODE_MODEL = "AtomCoords.pdb"
OUTPUT_NODE_MAP = "DensityMap.mrc.simulated"


class GemmiModelToMap(PipelinerJob):
    # XXX decide on name for utils/tools/etc
    PROCESS_NAME = "gemmi.convert.model_to_map"
    OUT_DIR = "GemmiModelToMap"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Gemmi Model to Map"
        self.jobinfo.short_desc = "Convert atomic model to map"
        self.jobinfo.long_desc = (
            "Calculates map from atomic using sfcalc command from Gemmi library."
        )
        self.jobinfo.programs = ["gemmi"]
        self.vers_com = (["gemmi", "-V"], [0])
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        # XXX Todo add ref when available
        # self.jobinfo.references = [
        #     Ref(
        #         authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
        #         title=(
        #             "Cryo-EM single particle structure refinement and map "
        #             "calculation using Servalcat."
        #         ),
        #         journal="Acta Cryst. D",
        #         year="2022",
        #         volume="77",
        #         issue="1",
        #         pages="1282-1291",
        #         doi="10.1107/S2059798321009475",
        #     )
        # ]
        self.jobinfo.documentation = "https://gemmi.readthedocs.io/"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=INPUT_NODE_MODEL,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="Model to generate map from",
            is_required=True,
            create_node=False,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=10.0,
            suggested_min=0.5,
            suggested_max=100,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )
        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):
        # gemmi sfcalc --dmin=4.0 --for=electron --write-map=gemmi.mrc
        #   refined.pdb

        command = [self.jobinfo.programs[0]]

        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_nodes.append(
            Node(self.joboptions["input_model"].get_string(), INPUT_NODE_MODEL)
        )
        resolution = self.joboptions["resolution"].get_number()

        # Set output name
        suffix = "_{0:.1f}.mrc".format(resolution)
        output_map = os.path.basename(str(input_model)).replace(".pdb", suffix)

        command += ["sfcalc", "--for", "electron"]
        command += ["--dmin", str(resolution)]
        command += ["--write-map", str(output_map)]
        # N.B. model has "../../" as Gemmi must be run from job directory
        command += ["../../" + str(input_model)]

        # Set working directory
        self.subprocess_cwd = self.output_name

        self.output_nodes.append(
            Node(os.path.join(self.output_name, output_map), OUTPUT_NODE_MAP)
        )
        commands = [command]
        return commands

    def create_results_display(self):
        thumbdir = os.path.join(self.output_name, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        input_model = self.joboptions["input_model"].get_string()
        output_map = self.output_nodes[0].name
        return [
            make_map_model_thumb_and_display(
                maps=[output_map],
                maps_opacity=[0.5],
                title="Map from model",
                models=[input_model],
                outputdir=self.output_name,
            )
        ]
