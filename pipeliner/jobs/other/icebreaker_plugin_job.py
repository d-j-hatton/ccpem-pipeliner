#!/usr/bin/env python

#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import InputNodeJobOption
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref

# input nodes
INPUT_NODE_MICS = "MicrographsData.star.relion"
INPUT_NODE_GROUPED_MICS = "MicrographsData.star.relion.icebreaker"
INPUT_NODE_PARTS = "ParticlesData.star.relion"


# output nodes
OUTPUT_NODE_GROUPED_MICS = "MicrographsData.star.relion.icebreaker"
OUTPUT_NODE_GROUPED_PARTS = "ParticlesData.star.relion.icebreaker"
OUTPUT_NODE_FLAT_MICS = "MicrographsData.star.relion.icebreaker.flattened"
OUTPUT_NODE_FIVE_FIG = "MicrographsData.csv.relion.icebreaker"

IB_REFS = [
    Ref(
        authors=[
            "Olek, M",
            "Cowtan, K",
            "Webb, D",
            "Chaban, Y",
            "Zhang, P",
        ],
        title=(
            "Icebreaker: Software for High Resolution Single Particle CryoEM With"
            " Nonuniform Ice"
        ),
        journal="Structure",
        year="2021",
        status="In press",
    )
]

IB_DOCS = "https://github.com/DiamondLightSource/python-icebreaker"


class IceBreakerGroupMics(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.micrographs"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super(self.__class__, self).__init__()

        self.jobinfo.display_name = "Icebreaker group micrographs"
        self.jobinfo.short_desc = "Assign ice thickness values to micrographs"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph and"
            " assigns them to groups based on this parameter"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = ["ib_job"]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern="*.star",
            help_text="Star file containing micrographs",
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_job", "--j", threads, "--mode", "group", "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_name]

        outfile = os.path.join(self.output_name, "grouped_micrographs.star")
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_GROUPED_MICS))

        return [coms]


class IceBreakerGroupParts(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.particles"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super(self.__class__, self).__init__()

        self.jobinfo.display_name = "Icebreaker group particles"
        self.jobinfo.short_desc = "Assign ice thickness values to particles"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph and"
            " assigns values to particles based on the micrograph they were extracted"
            " from"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = ["ib_group"]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=INPUT_NODE_GROUPED_MICS,
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped using"
                " Icebreaker"
            ),
            is_required=True,
        )

        self.joboptions["in_parts"] = InputNodeJobOption(
            label="Input particles star file:",
            node_type=INPUT_NODE_PARTS,
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text="Star file containing particles",
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_group", "--j", threads, "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)

        in_parts = self.joboptions["in_parts"].get_string(
            True, "Particles input file not found"
        )
        coms += ["--in_parts", in_parts]

        coms += ["--o", self.output_name]

        outfile = os.path.join(self.output_name, "ib_icegroups.star")
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_GROUPED_PARTS))

        return [coms]


class IceBreakerFlatten(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.enhancecontrast"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Icebreaker contrast enhancement"
        self.jobinfo.short_desc = "Flatten ice gradients to enhance micrograph contrast"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph  and"
            " flattens it based on the K-Means clustering algorithm, thus equalizing"
            " the local contrast"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = ["ib_job"]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=INPUT_NODE_GROUPED_MICS,
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped "
                "using Icebreaker"
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_job", "--j", threads, "--mode", "flatten", "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_name]

        outfile = os.path.join(self.output_name, "flattened_micrographs.star")
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_FLAT_MICS))

        return [coms]


class IceBreakerFiveFigureSummary(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.summary"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = ["ib_5fig"]
        self.jobinfo.display_name = "Icebreaker 5-figure summary"
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS
        self.jobinfo.short_desc = (
            "Calculate five figure summary of the "
            " distriubtion of relative ice thickness"
        )
        self.jobinfo.long_desc = (
            "Ice breaker estimates the relative ice gradient for each micrograph "
            " and produces a five figure summary of the resulting distribution "
        )

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=INPUT_NODE_GROUPED_MICS,
            default_value="",
            directory=".",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped "
                "using Icebreaker"
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_5fig", "--j", threads, "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_name]

        outfile = os.path.join(self.output_name, "five_figs_test.csv")
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_FIVE_FIG))

        return [coms]
