#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil

from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.data_structure import Node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    mini_montage_from_mrcs_stack,
)

# Conversions between GUI labels and Max's argument values
METHODS = {
    "FDR-BY": "BY",
    "FDR-BH": "BH",
    "FWER-Holm": "Holm",
    "FWER-Hochberg": "Hochberg",
}
TEST_PROCS = {
    "Left-sided": "leftSided",
    "Right-sided": "rightSided",
    "Two-sided": "twoSided",
}
# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
# output nodes
OUTPUT_NODE_MAP = "Mask3D.mrc.confidenceMap.fdr_map"
OUTPUT_NODE_PDF = "Logfile.pdf.confidenceMap.noise_window"


class ConfidenceMap(PipelinerJob):
    PROCESS_NAME = "confidencemap.map_analysis"
    OUT_DIR = "ConfidenceMap"

    def __init__(self):
        super(self.__class__, self).__init__()

        # Figure out where FDRcontrol.py is
        # this is a bit of a workaround until it is packaged with the pipeliner
        self.fdr_path = "FDR_NOT_FOUND"
        ccpem_path = shutil.which("ccpem-python")
        if ccpem_path:
            self.fdr_path = ccpem_path.split("/bin")[0] + "/lib/py2/FDRcontrol.py"

        self.jobinfo.display_name = "ConfidenceMap"

        self.jobinfo.short_desc = "Thresholding by FDR control"
        self.jobinfo.long_desc = (
            "Thresholding of cryo-EM density maps by False Discovery Rate " "control."
        )
        self.jobinfo.programs = ["ccpem-python", self.fdr_path]
        self.version = "0.1"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["M. Beckers", "A. J. Jakobi", "C. Sachse"],
                title=(
                    "Thresholding of cryo-EM density maps by False Discovery "
                    "Rate control"
                ),
                journal="IUCrJ.",
                year="2019",
                volume="6",
                issue="1",
                pages="18-33",
                doi="10.1107/S2052252518014434",
            )
        ]
        self.jobinfo.documentation = "https://git.embl.de/mbeckers/FDRthresholding"

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text=("Raw or unmasked input map"),
            is_required=True,
        )

        self.joboptions["window_size"] = IntJobOption(
            label="Window size",
            default_value=-1,
            suggested_min=10,
            suggested_max=50,
            step_value=1,
            help_text="Box size for background noise estimation (in pixels).",
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordx"] = FloatJobOption(
            label="Noise box coordinate X",
            default_value=-1.0,
            help_text=(
                "X coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordy"] = FloatJobOption(
            label="Noise box coordinate Y",
            default_value=-1.0,
            help_text=(
                "Y coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordz"] = FloatJobOption(
            label="Noise box coordinate Z",
            default_value=-1.0,
            help_text=(
                "Z coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Apix",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=20.0,
            step_value=0.1,
            help_text="Apix (voxel size) in Angstrom",
            is_required=False,
        )
        self.joboptions["meanMap"] = InputNodeJobOption(
            label="Mean map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text=("3D map of noise means to be used for FDR control."),
            is_required=False,
        )

        self.joboptions["varianceMap"] = InputNodeJobOption(
            label="Variance map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text=("3D map of noise variance to be used for FDR control."),
            is_required=False,
        )

        self.joboptions["locResMap"] = InputNodeJobOption(
            label="Local resolution map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text=("Input local resolution map (optional)."),
        )

        self.joboptions["method"] = MultipleChoiceJobOption(
            label="Method",
            choices=["FDR-BY", "FDR-BH", "FWER-Holm", "FWER-Hochberg"],
            default_value_index=1,
            help_text=(
                "Method for multiple testing correction. FDR for "
                "False Discovery Rate or FWER for Family-Wise Error "
                "Rate. 'BY' for Benjamini-Yekutieli, 'BH' for "
                "Benjamini-Hochberg."
            ),
        )

        self.joboptions["test_proc"] = MultipleChoiceJobOption(
            label="Test procedure",
            choices=["Left-sided", "Right-sided", "Two-sided"],
            default_value_index=1,
            help_text="Choose between right, left and two-sided testing.",
        )

        self.joboptions["lowPassFilter"] = FloatJobOption(
            label="Low-pass filter to",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text=(
                "Low-pass filter the map at the given resolution prior "
                "to FDR control."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["ecdf"] = BooleanJobOption(
            label="Use ECDF?",
            default_value=False,
            help_text=(
                "Use empirical cumulative distribution function instead"
                " of the standard normal distribution."
            ),
            in_continue=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        command = ["ccpem-python", self.fdr_path]

        # pipeline dir
        pipeline_dir = os.getcwd()

        # get the input map
        input_map = self.joboptions["input_map"].get_string(
            True, "ERROR: No input map specified"
        )
        input_map = os.path.join(pipeline_dir, input_map)

        # run confidence map job in the job dir
        # Set working directory
        self.subprocess_cwd = self.output_name

        # OTHER INPUT PARAMETERS
        window_size = self.joboptions["window_size"].get_number()
        noise_box_coord_x = self.joboptions["noise_box_coordx"].get_number()
        noise_box_coord_y = self.joboptions["noise_box_coordy"].get_number()
        noise_box_coord_z = self.joboptions["noise_box_coordz"].get_number()
        # noise box coord
        noise_box_coord = [noise_box_coord_x, noise_box_coord_y, noise_box_coord_z]
        if (
            noise_box_coord[0] == -1.0
            and noise_box_coord[1] == -1.0
            and noise_box_coord[2] == -1.0
        ):
            noise_box_coord = []
        apix = self.joboptions["apix"].get_number()
        testProc = self.joboptions["test_proc"].get_string()
        method = self.joboptions["method"].get_string()
        lowPassFilter_resolution = self.joboptions["lowPassFilter"].get_number()
        ecdf = self.joboptions["ecdf"].get_boolean()

        # get the mean map
        mean_map = self.joboptions["meanMap"].get_string()
        mean_map = os.path.join(pipeline_dir, mean_map)

        # get the variance map
        variance_map = self.joboptions["varianceMap"].get_string()
        variance_map = os.path.join(pipeline_dir, variance_map)

        # get the locres map
        locres_map = self.joboptions["locResMap"].get_string()
        locres_map = os.path.join(pipeline_dir, locres_map)

        # append output node types
        inp_map_basename = os.path.basename(os.path.splitext(input_map)[0])
        out_confidence_map = os.path.join(
            self.output_name, inp_map_basename + "_confidenceMap.mrc"
        )
        self.output_nodes.append(Node(out_confidence_map, OUTPUT_NODE_MAP))
        diag_image = os.path.join(self.output_name, "diag_image.pdf")
        self.output_nodes.append(Node(diag_image, OUTPUT_NODE_PDF))

        # Required args
        command += [
            "--em_map",
            str(input_map),
            "-method",
            METHODS[method],
            "--testProc",
            TEST_PROCS[testProc],
        ]
        if noise_box_coord is not None and len(noise_box_coord) > 0:
            command.extend(["-noiseBox"] + noise_box_coord)

        if apix is not None and apix != -1:
            command += ["--apix", apix]
        if os.path.isfile(locres_map):
            command += ["--locResMap", locres_map]
        if window_size != -1:
            command += ["--window_size", window_size]
        if os.path.isfile(mean_map):
            command += ["--meanMap", mean_map]
        if os.path.isfile(variance_map):
            command += ["--varianceMap", variance_map]
        if lowPassFilter_resolution is not None and lowPassFilter_resolution != -1:
            command += ["--lowPassFilter", lowPassFilter_resolution]
        if ecdf:
            command.append("-ecdf")

        commands = [command]

        return commands

    def create_results_display(self):
        # Have to do this because create_results_display() is run
        # on a fresh instantiation of the job object which has not
        # had get_commands() run on it. --MGI
        input_map = self.joboptions["input_map"].get_string()
        inp_map_basename = os.path.basename(os.path.splitext(input_map)[0])
        out_confidence_map = os.path.join(
            self.output_name, inp_map_basename + "_confidenceMap.mrc"
        )
        return [
            mini_montage_from_mrcs_stack(
                out_confidence_map,
                "",
                nimg=-1,
                ncols=10,
                montagesize=640,
                title="Confidence map slices",
                cmap="turbo",
            ),
            make_map_model_thumb_and_display(
                maps=[out_confidence_map],
                title="3D Confidence map",
                outputdir=self.output_name,
            ),
        ]
