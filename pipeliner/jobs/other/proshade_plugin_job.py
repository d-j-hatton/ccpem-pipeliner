import os
import json

from pipeliner.pipeliner_job import PipelinerJob, Ref
from pipeliner.job_options import (
    StringJobOption,
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
)
from pipeliner.data_structure import Node
from pipeliner.display_tools import (
    create_results_display_object,
)

# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_COORDS = "AtomCoords.pdb"
OUTPUT_NODE_MAP = "DensityMap.mrc"
OUTPUT_NODE_COORDS = "AtomCoords.pdb"
OUTPUT_NODE_SYMLOG = "LogFile.json.proshade.symmetry"


class ProShadeJob_S(PipelinerJob):
    PROCESS_NAME = "proshade.symmetry"
    OUT_DIR = "ProShade"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["proshade", "-v"], [0])
        self.jobinfo.display_name = "ProShade Symmetry Determination"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Local symmetry detection"
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. These functionalities "
            "accept both, coordinate data as well as map data. Symmetry detection "
            "allows finding the symmetry, the symmetry axes and symmetry group "
            "elements for any input file."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = ["proshade"]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map",
            is_required=True,
        )

        # TODO: write a regex validation for this jobop
        self.joboptions["point_sym"] = StringJobOption(
            label="Expected symmetry",
            default_value="",
            help_text="The symmetry to be preferred, if found. This parameter forces "
            "the symmetry search to output the results for this type of "
            "symmetry, if found. The parameter should have the first letter "
            "C for cyclic, D for dihedral, T for tetrahedral, O for octahedral "
            "or I for icosahedral. C and D must be followed by a "
            "number specifying the required fold, but T, O and I must not.",
            in_continue=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):
        command = ["proshade", "--rvpath", "/xtal/ccpem-20220125/share", "-S"]
        input_file = self.joboptions["input_map"].get_string(True, "Input file missing")
        command += [" -f ", input_file]

        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_name, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    # TODO: check that this function handles the log generated when
    # no symmetry was found - don't know what this looks like and other
    # possible log file formats I may have not anticipated

    def parse_results(self, return_lists=False):
        """Parse the output that went to run.out and return a dict or lists"""
        # parse the logfile from proshade symmetry
        outlog = os.path.join(self.output_name, "run.out")
        with open(outlog, "r") as ol:
            outdata = ol.readlines()
        div_count = 0
        axes, elements, alts = [], [], []
        exclude = ["----", "Symmetry elements table:", "Alternatives:"]
        for line in outdata:
            if not any([x in line for x in exclude]) and line != "\n":
                if div_count == 8:
                    axes.append(line)
                elif div_count == 10:
                    elements.append(line)
                elif div_count == 12:
                    alts.append(line)
                elif div_count > 12:
                    break
            if "--------" in line:
                div_count += 1

        # prepare the data
        outdict = {"Symmetry axes": [], "Symmetry elements": [], "Alternatives": []}
        outax, outelems, outalts = [], [], []

        for ax in axes:
            dat = ax.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Symmetry axes"].append(od)
            outax.append(dat[:5] + [angle] + [dat[-1]])

        for elm in elements:
            dat = elm.split()
            od = {
                "Symmetry Type": dat[0],
                "x": dat[1],
                "y": dat[2],
                "z": dat[3],
                "Ang (deg)": dat[4],
            }
            outdict["Symmetry elements"].append(od)
            outelems.append(dat)
        for alt in alts:
            dat = alt.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Alternatives"].append(od)
            outalts.append(dat[:5] + [angle] + [dat[-1]])
        if return_lists:
            return [outax, outelems, outalts]
        return outdict

    def post_run_actions(self):
        """Read the run.out and create a proper output node"""
        outf = os.path.join(self.output_name, "proshade_symmetry.json")
        outdict = self.parse_results()
        with open(outf, "w") as outfile:
            json.dump(outdict, outfile, indent=4)
        self.output_nodes.append(Node(outfile, OUTPUT_NODE_SYMLOG))

    def create_results_display(self):
        """create a ResultsDisplayTable object for each of the 3 outputs
        and a rvapi display object"""
        # do the three tables
        outf = os.path.join(self.output_name, "proshade_symmetry.json")
        axes, elems, alts = self.parse_results(return_lists=True)

        ax_t = create_results_display_object(
            "table",
            title="Symmetry Axes",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=axes,
            associated_data=[outf],
        )

        elem_t = create_results_display_object(
            "table",
            title="Symmetry Elements",
            headers=[
                "Symmetry Type",
                "x",
                "y",
                "z",
                "Angle",
            ],
            table_data=elems,
            associated_data=[outf],
        )

        alt_t = create_results_display_object(
            "table",
            title="Alternatives",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=alts,
            associated_data=[outf],
        )

        # then the rvapi
        report_dir = os.path.join(self.output_name, "proshade_report")
        rvapi = create_results_display_object("rvapi", rvapi_dir=report_dir)
        return [ax_t, elem_t, alt_t, rvapi]


class ProShadeJob_E(PipelinerJob):
    """CCP-EM gui creates:
    proshade --rvpath /xtal/ccpem-20220125/share -E -f emd_13271.map
    --clearMap emd_13271_reboxed --cellBorderSpace 1.0
    """

    PROCESS_NAME = "proshade.rebox"
    OUT_DIR = "ProShade"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["proshade", "-v"], [0])
        self.jobinfo.display_name = "ProShade Reboxing"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Find smaller box for input map "
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. The map re-boxing "
            "functionality masks the input (map only) and attempts to find smaller "
            "box containing all the mask, thus decreasing the linear processing "
            "time for the map. This functionality is also available for half maps."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = ["proshade"]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["add_space"] = BooleanJobOption(
            label="Add extra space around structure?",
            default_value=False,
            help_text="(Optional) Add extra space around the masked structure",
            in_continue=True,
        )
        self.joboptions["extra_space"] = FloatJobOption(
            label="Extra space around structure",
            default_value=0.0,
            suggested_min=0.0,
            help_text="How much extra space to add (Angstroms)",
            in_continue=True,
            deactivate_if=[("add_space", "=", False)],
            required_if=[("add_space", "=", True)],
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):
        # TODO: We will need to set -rvpath from an env var or add it as a joboption
        # because it will vary
        command = ["proshade", "--rvpath", "/xtal/ccpem-20220125/share", "-E"]
        input_file = self.joboptions["input_map"].get_string(True, "Input file missing")
        command += [" -f ", input_file]
        output_file = os.path.join(self.output_name, "reboxed_map.mrc")
        command += [" --clearMap ", output_file]

        self.output_nodes.append(Node(output_file, OUTPUT_NODE_MAP))

        do_extra_space = self.joboptions["add_space"].get_boolean()
        if do_extra_space:
            alt_arg = self.joboptions["extra_space"].get_number()
            command += ["--cellBorderSpace", alt_arg]
        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_name, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    def create_results_display(self):
        report_dir = os.path.join(self.output_name, "proshade_report")
        return [create_results_display_object("rvapi", rvapi_dir=report_dir)]


class ProShadeJob_O(PipelinerJob):
    """CCP-EM gui creates:
    proshade --rvpath /xtal/ccpem-20220125/share -O -f in1.mrc -f in2.mrc
     --clearMap in2_overlaid.mrc --cellBorderSpace 20.0
    TODO ProShade won't accept mmCIF format coordinate files
    """

    PROCESS_NAME = "proshade.overlay"
    OUT_DIR = "ProShade"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["proshade", "-v"], [0])
        self.jobinfo.display_name = "ProShade Overlay"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Overlay one map/model on another "
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. The overlay "
            "functionality finds the rotation and translation which optimally overlays "
            "(or fits) one structure into another. Each structure can be a "
            "coordinate file or a map."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = ["proshade"]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.joboptions["static_struct"] = InputNodeJobOption(
            label="Static map or coordinates",
            node_type=INPUT_NODE_MAP,
            create_node=False,
            default_value="",
            directory="",
            pattern="3D map or coordinates (*.{mrc,map,pdb})",
            help_text="The first structure which remains static",
            is_required=True,
        )
        self.joboptions["moving_struct"] = InputNodeJobOption(
            label="Moving map or coordinates",
            node_type=INPUT_NODE_MAP,
            create_node=False,
            default_value="",
            directory="",
            pattern="3D map or coordinates (*.{mrc,map,pdb})",
            help_text="The second structure which is moved to match the first",
            is_required=True,
        )

        self.joboptions["add_space"] = BooleanJobOption(
            label="Add extra space around structure?",
            default_value=False,
            help_text=(
                "(Optional) Extra space may be required to accommodate "
                "rotation/translation of second structure"
            ),
            in_continue=True,
        )
        self.joboptions["extra_space"] = FloatJobOption(
            label="Extra space around structure",
            default_value=0.0,
            suggested_min=0.0,
            help_text="How much extra space to add (Angstroms)",
            in_continue=True,
            deactivate_if=[("add_space", "=", False)],
            required_if=[("add_space", "=", True)],
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):
        # TODO: We will need to set -rvpath from an env var or add it as a joboption
        # because it will vary
        command = ["proshade", "--rvpath", "/xtal/ccpem-20220125/share", "-O"]
        input1_file = self.joboptions["static_struct"].get_string(
            True, "Input file missing"
        )
        command += [" -f ", input1_file]
        input2_file = self.joboptions["moving_struct"].get_string(
            True, "Input file missing"
        )
        command += [" -f ", input2_file]

        # based on filename extension, but should do better
        # is there nothing in Gemmi? perhaps test with mrcfile.validate()
        if input1_file.split(".")[-1] == "map" or input1_file.split(".")[-1] == "mrc":
            node_type = INPUT_NODE_MAP
        else:
            node_type = INPUT_NODE_COORDS
        self.input_nodes.append(Node(input1_file, node_type))
        if input2_file.split(".")[-1] == "map" or input2_file.split(".")[-1] == "mrc":
            node_type = INPUT_NODE_MAP
            output_ext = "mrc"
            out_node_type = OUTPUT_NODE_MAP
        else:
            node_type = INPUT_NODE_COORDS
            output_ext = "pdb"
            out_node_type = OUTPUT_NODE_COORDS
        self.input_nodes.append(Node(input2_file, node_type))

        # output type according to second (moved) input file
        output_file = os.path.join(
            self.output_name,
            os.path.basename(input2_file).split(".")[0] + "_overlaid." + output_ext,
        )
        command += [" --clearMap ", output_file]
        self.output_nodes.append(Node(output_file, out_node_type))

        do_extra_space = self.joboptions["add_space"].get_boolean()
        if do_extra_space:
            alt_arg = self.joboptions["extra_space"].get_number()
            command += ["--cellBorderSpace", alt_arg]
        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_name, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    def create_results_display(self):
        report_dir = os.path.join(self.output_name, "proshade_report")
        return [create_results_display_object("rvapi", rvapi_dir=report_dir)]
