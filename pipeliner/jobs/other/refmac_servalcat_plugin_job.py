#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import subprocess
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display


# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_MODEL = "AtomCoords"
INPUT_NODE_LIGAND = "LigandDescription"
INPUT_NODE_MASK3D = "Mask3D.mrc"

# output nodes
OUTPUT_NODE_MODEL = "AtomCoords.cif.refmac_servalcat.refined"
OUTPUT_NODE_DIFFMAP = "DensityMap.mrc.refmac_servalcat.diffmap"


class Refine(PipelinerJob):
    PROCESS_NAME = "refmac_servalcat.atomic_model_refine"
    OUT_DIR = "RefmacServalcat"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["servalcat", "--version"], [])
        self.jobinfo.display_name = "Refmac Servalcat"
        self.jobinfo.short_desc = "Atomic structure refinement"
        self.jobinfo.long_desc = (
            "Macromolecular refinement program. Servalcat is a wrapper for"
            " REFMAC5 that adds map sharpening, symmetry handling, difference"
            " maps and other functions. REFMAC5 is a program designed for"
            " REFinement of MACromolecular structures. It uses maximum"
            " likelihood and some elements of Bayesian statistics."
            " N.B. requires CCP4."
        )
        self.jobinfo.programs = ["servalcat", "gemmi"]
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://github.com/keitaroyam/servalcat"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=INPUT_NODE_MODEL,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be refined",
            is_required=True,
            create_node=False,  # Need to set extension depanding on file
        )
        self.joboptions["input_ligand"] = InputNodeJobOption(
            label="Input ligand",
            node_type=INPUT_NODE_LIGAND,
            pattern="Ligand definition (.cif)",
            default_value="",
            directory="",
            help_text="The input model",
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=None,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )

        self.joboptions["input_half_map1"] = InputNodeJobOption(
            label="Input map 1 (half map 1 or full map)",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["half_map_refinement"] = BooleanJobOption(
            label="Half map refinement",
            default_value=True,
            help_text=(
                "Use half maps for refinement, alternative is to use single full map"
            ),
        )
        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input map to refine the model against",
            required_if=[("half_map_refinement", "=", True)],
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=INPUT_NODE_MASK3D,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input mask required for difference map calculation",
        )
        self.joboptions["masked_refinement"] = BooleanJobOption(
            label="Masked Refinement",
            default_value=True,
            help_text=(
                "Trim map around supplied model file and perform refinement "
                "with resultant sub-volume"
            ),
        )
        self.joboptions["mask_radius"] = FloatJobOption(
            label="Mask radius",
            default_value=3.0,
            help_text="Distance around molecule the map should be cut",
            required_if=[("masked_refinement", "=", True)],
            deactivate_if=[("masked_refinement", "=", False)],
        )
        self.joboptions["b_factor"] = FloatJobOption(
            label="Set model B-factors",
            default_value=3.0,
            help_text="Reset all atomic B-factors in input model to given value",
            is_required=True,
        )
        self.joboptions["n_cycle"] = IntJobOption(
            label="Refmac cycles",
            default_value=8,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of refmac cycles used. User should assess if structure"
                "converges"
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["nucleotide_restraints"] = BooleanJobOption(
            label="Nucleotide restraints",
            default_value=False,
            help_text="Use LIBG to generate restraints for nucleic acids",
        )
        self.joboptions["jelly_body"] = BooleanJobOption(
            label="Jelly body restraints",
            default_value=True,
            help_text="Use jelly body restraints",
        )
        self.joboptions["jelly_body_sigma"] = FloatJobOption(
            label="Jelly body sigma",
            default_value=0.02,
            help_text="Sigma value for jelly body restraints",
            deactivate_if=[("jelly_body", "=", False)],
            required_if=[("jelly_body", "=", True)],
        )
        self.joboptions["jelly_body_dmax"] = FloatJobOption(
            label="Jelly body dmax",
            default_value=4.2,
            help_text="Dmax value for jelly body restraints",
            deactivate_if=[("jelly_body", "=", False)],
            required_if=[("jelly_body", "=", True)],
        )
        self.joboptions["auto_weight"] = BooleanJobOption(
            label="Auto weight",
            default_value=True,
            help_text=(
                "Use Servcalcat to automatically determine the relative "
                "weight of data vs stereochemical restraints"
            ),
        )
        self.joboptions["weight"] = FloatJobOption(
            label="Weight",
            default_value=1.0,
            suggested_min=0.01,
            suggested_max=100,
            step_value=0.01,
            help_text=(
                "Specific relative weight of data vs stereochemical restraints. m"
                "Smaller values result in stricter stereochemical restraints"
            ),
            deactivate_if=[("auto_weight", "=", True)],
            required_if=[("auto_weight", "=", False)],
        )
        # XXX # TODO: add description of local vs global in tooltip
        self.joboptions["auto_symmetry"] = MultipleChoiceJobOption(
            label="Auto symmetry",
            choices=["None", "Local", "Global"],
            default_value_index=1,
            help_text="Automatically detect and apply symmetry (recommended). ",
        )
        self.joboptions["strict_symmetry"] = StringJobOption(
            label="Strict symmetry point group",
            default_value="",
            help_text="Impose strict symmetry using RELION point group convention",
        )
        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        # /Users/tom/code/ccpem_distro/ccpem-20211201/bin/ccpem-python -m servalcat.command_line  # noqa: E501
        # refine_spa --show_refmac_log --model /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/pdb/5me2_a.pdb # noqa: E501
        # --halfmaps /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/map/mrc/3488_run_half1_class001_unfil.mrc /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/map/mrc/3488_run_half2_class001_unfil.mrc # noqa: E501
        # --mask_radius 3.0
        # --ncycle 8
        # --ncsr local
        # --bfactor 40.0
        # --resolution 3.2
        # --jellybody --jellybody_params 0.01 4.2 --output_prefix refined --cross_validation # noqa: E501

        # XXX Todo - should this refer to specific version i.e. explicitly point to
        # CCP-EM distribution?
        command = [self.jobinfo.programs[0]]

        # Get parameters
        input_half_map1 = self.joboptions["input_half_map1"].get_string(
            True, "Input file missing"
        )
        input_half_map2 = self.joboptions["input_half_map2"].get_string(
            True, "Input file missing"
        )
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        nodext = os.path.splitext(input_model)[1]
        self.input_nodes.append(Node(input_model, INPUT_NODE_MODEL + nodext))
        resolution = self.joboptions["resolution"].get_number()
        ncycle = self.joboptions["n_cycle"].get_number()
        mask_radius = self.joboptions["mask_radius"].get_number()
        b_factor = self.joboptions["b_factor"].get_number()
        ncsr = self.joboptions["auto_symmetry"].get_string()
        jelly_body = self.joboptions["jelly_body"].get_boolean()
        jelly_body_sigma = self.joboptions["jelly_body_sigma"].get_number()
        jelly_body_dmax = self.joboptions["jelly_body_dmax"].get_number()

        # Set command parameters
        command += ["refine_spa", "--show_refmac_log", "--output_prefix", "refined"]

        # N.B. all import files must have "../../" as Servalcat must be run from job
        command += [
            "--halfmaps",
            "../../" + str(input_half_map1),
            "../../" + str(input_half_map2),
        ]

        # XXX add use_half_map logic

        # command += ["--map", str(input_map)]
        command += ["--model", "../../" + str(input_model)]
        command += ["--resolution", str(resolution)]
        command += ["--ncycle", str(ncycle)]
        command += ["--mask_radius", str(mask_radius)]
        command += ["--bfactor", str(b_factor)]
        # Servalcat --ncsr option supports "local" or "global" only.  Note lower case.
        if ncsr in ["Local", "Global"]:
            command += ["--ncsr", ncsr.lower()]

        if jelly_body:
            command += [
                "--jellybody",
                "--jellybody_params",
                str(jelly_body_sigma),
                str(jelly_body_dmax),
            ]

        output_file = os.path.join(self.output_name, "refined.pdb")
        self.output_nodes.append(Node(output_file, OUTPUT_NODE_MODEL))

        # Set working directory
        self.subprocess_cwd = self.output_name

        commands = [command]
        return commands

    def post_run_actions(self):
        final_mtz = os.path.join(self.output_name, "diffmap.mtz")
        dmmrc_path = os.path.join(self.output_name, "diffmap.mrc")
        subprocess.call(["gemmi", "sf2map", final_mtz, dmmrc_path])
        self.output_nodes.append(
            Node(os.path.join(self.output_name, "diffmap.mrc"), OUTPUT_NODE_DIFFMAP)
        )

    def create_results_display(self):
        thumbdir = os.path.join(self.output_name, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = os.path.join(self.output_name, "refined.pdb")
        return [
            make_map_model_thumb_and_display(
                maps=[os.path.join(self.output_name, "diffmap.mrc")],
                maps_opacity=[0.5],
                models=[models],
                outputdir=self.output_name,
            )
        ]
