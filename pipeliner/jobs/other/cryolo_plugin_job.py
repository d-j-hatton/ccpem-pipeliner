#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
from gemmi import cif
from glob import glob

from pipeliner.utils import truncate_number, fix_newlines, decompose_pipeline_filename
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref
from pipeliner.star_writer import write
from pipeliner.jobstar_reader import RelionStarFile
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)

# input nodes
INPUT_NODE_MICS = "MicrographsData.star.relion"
INPUT_NODE_JANNIMODEL = "ProcessData.h5.janni.model"
INPUT_NODE_CRYOLOMODEL = "MLModel.h5.cryolo"
# output nodes
OUTPUT_NODE_COORDS = "MicrographsCoords.star.cryolo.autopick"


class CrYOLOAutopick(PipelinerJob):
    PROCESS_NAME = "cryolo.autopick"
    OUT_DIR = "AutoPick"

    def __init__(self):
        super(self.__class__, self).__init__()

        self.jobinfo.display_name = "crYOLO"
        self.jobinfo.short_desc = "Automated particle picking"
        self.jobinfo.long_desc = (
            "Fast and accurate cryo-EM particle picking based on convolutional neural"
            " networks and utilizes the popular You Only Look Once (YOLO) object"
            " detection system."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = ["cryolo_gui.py", "cryolo_predict.py"]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Wagner T",
                    "Merino F",
                    "Stabrin M",
                    "Moriya T",
                    "Antoni C",
                    "Apelbaum A",
                    "Hagel P",
                    "Sitsel O",
                    "Raisch T",
                    "Prumbaum D",
                    "Quentin D",
                    "Roderer D",
                    "Tacke S",
                    "Siebolds B",
                    "Schubert E",
                    "Shaikh TR",
                    "Lill P",
                    "Gatsogiannis C",
                    "Raunser S",
                ],
                title=(
                    "SPHIRE-crYOLO is a fast and accurate fully automated "
                    "particle picker for cryo-EM."
                ),
                journal="Commun. Biol.",
                year="2019",
                volume="2",
                pages="218",
                doi="10.1038/s42003-019-0437-z",
            )
        ]
        self.jobinfo.documentation = "https://cryolo.readthedocs.io/en/stable/"

        self.always_continue_in_schedule = True

        self.joboptions["input_file"] = InputNodeJobOption(
            label="Input micrographs file:",
            node_type=INPUT_NODE_MICS,
            default_value="",
            directory="",
            pattern="Input Micrographs (*.star)",
            help_text=(
                "Input STAR file (preferably with CTF information) with all micrographs"
                " to pick from."
            ),
            is_required=True,
        )

        self.joboptions["model_path"] = InputNodeJobOption(
            label="CrYOLO model path:",
            node_type=INPUT_NODE_CRYOLOMODEL,
            default_value="",
            directory="",
            pattern="Cryolo model (*.h5)",
            help_text="Download a crYOLO model or train your own",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["box_size"] = IntJobOption(
            label="Box size:",
            default_value=128,
            suggested_min=50,
            suggested_max=1000,
            step_value=1,
            help_text="Box size in pixels",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["gpus"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "GPUs should be entered comma separated, unlike relion's notation"
            ),
        )

        self.joboptions["confidence_threshold"] = FloatJobOption(
            label="Confidence threshold",
            default_value=0.3,
            suggested_min=0.01,
            suggested_max=1,
            step_value=0.01,
            help_text="CrYOLO confidence threshold",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["use_JANNI_denoise"] = BooleanJobOption(
            label="Use JANNI denoising?",
            default_value=False,
            help_text="Use JANNI to denoise micrographs.  A model is required",
            in_continue=True,
        )

        self.joboptions["JANNI_model_path"] = InputNodeJobOption(
            label="Path to JANNI noise model:",
            node_type=INPUT_NODE_JANNIMODEL,
            default_value="",
            directory="",
            pattern="JANNI model (*.h5)",
            help_text="Download the model or train your own",
            in_continue=True,
            deactivate_if=[("use_JANNI_denoise", "=", False)],
            required_if=[("use_JANNI_denoise", "=", True)],
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        input_fn = self.joboptions["input_file"].get_string(
            True, "No input file specified"
        )
        if not os.path.isfile(input_fn):
            raise ValueError(f"Input file {input_fn} not found")

        cryolo_model = self.joboptions["model_path"].get_string(
            True, "No crYOLO model specified"
        )
        if not os.path.isfile(cryolo_model):
            raise ValueError(f"crYOLO model file {cryolo_model} not found")

        in_star = cif.read_file(input_fn)
        mics_block = in_star.find_block("micrographs")
        all_mics = mics_block.find_loop("_rlnMicrographName")

        # put symlinks to the mics to pick in a tmp directory
        # if is_continue check which micrographs have already been picked
        if self.is_continue:
            existing_ap_star = os.path.join(self.output_name, "autopick.star")
            ext = RelionStarFile(existing_ap_star)
            mics_block = ext.get_block("coordinate_files")
            done_mics = set(mics_block.find_loop("_rlnMicrographName"))
            # subtract the already fininshed mics from the list of all mics
            all_mics = list(set(all_mics) - done_mics)

        # read autopick.star file if it exists
        # cross reference the autopick.star mics with all_mics remove overlap

        # make the TMP dir and create the symlinks
        tmpdir = "CRYOLO_TMP"
        if os.path.isdir(tmpdir):
            shutil.rmtree(tmpdir)
        os.makedirs(tmpdir)
        for mic in all_mics:
            micpath = os.path.abspath(mic)
            os.symlink(micpath, os.path.join(tmpdir, os.path.basename(mic)))

        # make the  cryolo config file
        box_size = self.joboptions["box_size"].get_number()
        config_command = ["cryolo_gui.py"]
        config_command += [
            "config",
            "{}cryolo_config.json".format(self.output_name),
            truncate_number(box_size, 0),
        ]

        # pick the filtering option
        if not self.joboptions["use_JANNI_denoise"].get_boolean():
            config_command += ["--filter", "LOWPASS", "--low_pass_cutoff", "0.1"]
        else:
            config_command += ["--filter", "JANNI", "--janni_model"]
            jannimodel = self.joboptions["JANNI_model_path"].get_string(
                True, "ERROR: No JANNI model specified"
            )
            config_command.append(jannimodel)
        commands = [config_command]

        # make the crYOLO command
        cryolo_com = [
            "cryolo_predict.py",
            "-c",
            "{}cryolo_config.json".format(self.output_name),
        ]

        cryolo_model = self.joboptions["model_path"].get_string(
            True, "ERROR: No crYOLO model specified"
        )
        cryolo_com += ["-w", cryolo_model]

        cryolo_com += ["-i", f"{tmpdir}/"]

        # check the GPUs entry
        gpus = (
            self.joboptions["gpus"]
            .get_string(True, "ERROR: No GPUs specified")
            .replace(" ", "")
        )
        allowed = "0123456789,"
        if not all(x in allowed for x in gpus):
            raise ValueError("ERROR: GPUs must be specified comma separated")

        cryolo_com += [
            "-g",
            gpus,
            "-o",
            self.output_name,
            "-t",
            str(self.joboptions["confidence_threshold"].get_number()),
        ]

        commands.append(cryolo_com)
        return commands

    def post_run_actions(self):
        """After a crYOLO job has been run assemble the autopick.star file for relion
        this file contains name and coordinate file for each micrograph
        """
        in_star = cif.read_file(self.joboptions["input_file"].get_string())
        mics_block = in_star.find_block("micrographs")
        all_mics = mics_block.find_loop("_rlnMicrographName")

        # make sure that particle files were written
        missing_files = []
        for f in all_mics:
            fn = os.path.basename(f).replace(".mrc", ".star")
            starfile = os.path.join(self.output_name, "STAR/" + fn)
            if not os.path.isfile(starfile):
                missing_files.append(f)
        if len(missing_files) > 0 and len(missing_files) != len(all_mics):
            print(
                "WARNING: No particle coordinate files were produced for the following"
                " files:"
            )
            for f in missing_files:
                print(f" - {f}")

        elif len(missing_files) == len(all_mics):
            raise ValueError("No particle coordinate files were written")

        output_file = "{}autopick.star".format(self.output_name)
        self.output_nodes.append(Node(output_file, OUTPUT_NODE_COORDS))

        d = cif.Document()
        block = d.add_new_block("coordinate_files")
        loop = block.init_loop("", ["_rlnMicrographName", "_rlnMicrographCoordinates"])
        for mic in all_mics:
            coord_fn = os.path.basename(mic).split(".")[0] + ".star"
            coord_file = os.path.join(self.output_name, "STAR/" + coord_fn)
            loop.add_row([mic, coord_file])
        write(d, output_file)

        # fix the newlines in the crYOLO starfiles
        starpath = os.path.join(self.output_name, "STAR")
        starfiles = glob(f"{starpath}/*.star")
        for f in starfiles:
            fix_newlines(f)

        # remove the tmp directory and clean up cryolo dirs
        for yolodir in ("filtered_tmp", "logs"):
            if os.path.isdir(yolodir):
                shutil.move(yolodir, os.path.join(self.output_name, yolodir))
        shutil.rmtree("CRYOLO_TMP")

    def create_results_display(self):
        """count the indivudlal particle starfiles rather that the summary
        star file for on-the-fly updating"""
        # make the histogram
        # get the star files
        instar = RelionStarFile(self.joboptions["fn_input_autopick"].get_string())
        mdat = instar.get_block("micrographs").find(["_rlnMicrographName"])[0][0]
        logsdir = os.path.join(
            self.output_name, os.path.dirname(decompose_pipeline_filename(mdat)[2])
        )
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(RelionStarFile(pf).count_block())
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_name, "autopick.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_name, "autopick.star")
        fb = RelionStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_name)

        return [image, graph]
