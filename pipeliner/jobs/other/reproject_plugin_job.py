#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import Node

# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_ANGLES = "ProcessData.star.relion.eulerangles"

# output nodes
OUTPUT_NODE_PROJS = "Image2DStack.mrcs.relion.projections"
OUTPUT_NODE_STAR = "ProcessData.star.relion.reproject"


class ReprojectPluginJob(RelionJob):
    PROCESS_NAME = "relion.reproject"
    OUT_DIR = "Reproject"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "RELION Reproject"

        self.jobinfo.short_desc = "Generate 2D projections from a 3D map"
        self.jobinfo.long_desc = (
            "Just a reminder: DO NOT use high resolution projections as autopicking"
            " references"
        )
        self.jobinfo.programs = ["relion_project"]

        self.joboptions["map_filename"] = InputNodeJobOption(
            label="Input map mrc file:",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="mrc map files (*.mrc)",
            help_text="The map that will be used for re-projections",
            is_required=True,
        )

        self.joboptions["angles_filename"] = InputNodeJobOption(
            label="Angles file:",
            node_type=INPUT_NODE_ANGLES,
            default_value="",
            directory="",
            pattern="angles star file (*.star)",
            help_text=(
                "This should be a star file with a block called data_angles containing"
                " the following columns: rlnAngleRot, _rlnAngleTilt, and _rlnAnglePsi"
            ),
            deactivate_if=[("nr_uniform", ">", 0)],
        )

        self.joboptions["nr_uniform"] = IntJobOption(
            label="OR: Do this many uniform projections:",
            default_value=-1,
            suggested_min=1,
            suggested_max=180,
            step_value=1,
            help_text="Alternately do this many uniform projections",
            in_continue=True,
            deactivate_if=[("angles_filename", "!=", "")],
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Reference pixel size (A)",
            default_value=1.07,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.01,
            help_text="The pixel size of the reference in Angstroms",
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        command = ["relion_project"]

        map_file = self.joboptions["map_filename"].get_string()
        command += ["--i", map_file]

        nr_uniform = self.joboptions["nr_uniform"].get_number()
        angles_file = self.joboptions["angles_filename"].get_string()
        if angles_file != "":
            command += ["--ang", angles_file]
        elif nr_uniform > 0:
            command += ["--nr_uniform", str(nr_uniform)]
        else:
            raise ValueError(
                "ERROR: An angles file or a number of uniform projections must be"
                " specified"
            )

        apix = self.joboptions["apix"].get_string()
        command += ["--angpix", apix]

        command += ["--o", "{}reproj".format(self.output_name)]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        self.output_nodes.append(
            Node(
                os.path.join(self.output_name, "reproj.mrcs"),
                OUTPUT_NODE_PROJS,
            )
        )

        self.output_nodes.append(
            Node(
                os.path.join(self.output_name, "reproj.star"),
                OUTPUT_NODE_STAR,
            )
        )

        commands = [command]
        return commands
