#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display


# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_MODEL = "AtomCoords"
INPUT_NODE_LIGAND = "LigandDescription"
INPUT_NODE_MASK3D = "Mask3D.mrc"

# output nodes
OUTPUT_NODE_MAP = "DensityMap.mrc.locscale.localsharpen"


class LocalSharpen(PipelinerJob):
    PROCESS_NAME = "locscale.localsharpen"
    OUT_DIR = "LocScale"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["servalcat", "--version"], [])
        self.jobinfo.display_name = "LocScale"
        self.jobinfo.short_desc = "Local sharpening of 3D maps"
        self.jobinfo.long_desc = (
            "LocScale is a reference-based local amplitude scaling tool using"
            " prior model information to improve contrast of cryo-EM density"
            " maps. It can be used as an alternative to other commonly used map"
            " sharpening methods."
        )
        self.jobinfo.programs = ["locscale"]
        self.version = "2.0"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Jakobi A J", "Wilmanns M", "Sachse C"],
                title="Model-based local density sharpening of cryo-EM maps",
                journal="eLife",
                year="2017",
                volume="6",
                issue="1",
                pages="e27131",
                doi="https://doi.org/10.7554/eLife.27131",
            )
        ]
        self.jobinfo.documentation = "https://git.embl.de/jakobi/LocScale/-/wikis/home"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=INPUT_NODE_MODEL,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="Reference PDB with refined B-factors",
            is_required=False,
            create_node=False,  # Need to set extension depanding on file
        )
        self.joboptions["n_nodes"] = IntJobOption(
            label="Nodes",
            default_value=2,
            suggested_min=1,
            suggested_max=8,
            step_value=1,
            help_text=("Number of mpi nodes used for LocScale calculation"),
            in_continue=True,
            is_required=True,
        )
        # # XXX Check if this is required for LocScale2
        # self.joboptions["input_ligand"] = InputNodeJobOption(
        #     label="Input ligand",
        #     node_type=INPUT_NODE_LIGAND,
        #     pattern="Ligand definition (.cif)",
        #     default_value="",
        #     directory="",
        #     help_text="Input ligand dictionary",
        # )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=None,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Unsharpened and unfiltered 3D refined map",
            is_required=True,
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=INPUT_NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input mask required for difference map calculation",
        )
        # XXX check how to set for LocScale
        # self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):

        # Run LocScale using an existing atomic model:
        # locscale run_locscale -em path/to/emmap.mrc -mc path/to/model.pdb -res 3
        #       -v -o model_based_locscale.mrc
        #
        # Run LocScale without atomic model
        # locscale run_locscale -em path/to/emmap.mrc -res 3
        #       -v -o model_based_locscale.mrc

        # Need to add support for mpi
        # command = [
        #     "mpirun",
        #     "-np",
        #     self.joboptions["n_nodes"].get_string(),
        #     self.jobinfo.programs[0],
        #     "run_locscale",
        # ]

        command = [self.jobinfo.programs[0], "run_locscale"]
        # Get parameters and set nodes
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        self.input_nodes.append(Node(input_map, INPUT_NODE_MAP))
        input_model = self.joboptions["input_model"].get_string()
        if input_map != "":
            self.input_nodes.append(Node(input_map, INPUT_NODE_MODEL))

        resolution = self.joboptions["resolution"].get_number()

        # Input Map
        command += ["-em", "../../" + str(input_map)]
        # Input Resolution
        command += ["-res", str(resolution)]
        # Input Model (optional)
        if input_model is not None:
            command += ["-mc", "../../" + str(input_model)]
        # Output map
        output_file = os.path.join(self.output_name, "locscale_output.mrc")
        self.output_nodes.append(Node(output_file, OUTPUT_NODE_MAP))

        # Set working directory
        self.subprocess_cwd = self.output_name

        commands = [command]
        return commands

    def create_results_display(self):
        thumbdir = os.path.join(self.output_name, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        # Get input model if available
        input_model = self.joboptions["input_model"].get_string()
        if input_model == "":
            model = None
        else:
            model = [input_model]
        # Get input and output map
        input_map = self.joboptions["input_map"].get_string()
        output_map = os.path.join(self.output_name, "locscale_output.mrc")

        return [
            make_map_model_thumb_and_display(
                maps=[input_map, output_map],
                maps_opacity=[0.5, 1.0],
                models=model,
                title="LocScale local sharpening",
                outputdir=self.output_name,
            )
        ]
