import os
import copy
import pandas as pd
from collections import OrderedDict
import sys
import pickle
from pipeliner.jobs.other.model_validation.smoc.set_smoc_results import SetSMOCResults
from pipeliner.jobs.other.model_validation.molprobity.set_molprobity_results import (
    SetMolprobityResults,
)

# from pipeliner.jobs.other.model_validation.pi_score.set_piscore_results import SetPIscoreResults
# from pipeliner.jobs.other.model_validation.cablam.set_cablam_results import SetCaBLAMResults
# from pipeliner.jobs.other.model_validation.refmac.set_refmac_results import SetRefmacResults
# from pipeliner.jobs.other.model_validation.global_scores.set_scores_results import SetScoresResults
# from pipeliner.jobs.other.model_validation.dssp.dssp_log_parser import DSSP_LogParser
# from pipeliner.jobs.other.model_validation.jpred.set_jpred_results import SetJpredResults
# from pipeliner.jobs.other.model_validation.bfactor_analysis.analyse_bfactors_results import SetBfactResults
# from cootscript_edit import SetCootScript
import gc
from pipeliner.jobs.other.model_validation.validate_results_settings import (
    residue_summary_table_settings,
)
from pipeliner.jobs.other.model_validation.outlier_utils import cluster_residues

# Resolution label (equivalent to <4SSQ/LL>


class PipelineResultsViewer(object):
    """
    Results viewer for job pipeline
    """

    def __init__(self, pipeliner_pkl):
        with open(pipeliner_pkl, "rb") as p:
            pipeliner_instance = pickle.load(p)
        self.pipeline = pipeliner_instance
        self.pipeline_location = pipeliner_instance.pipeline_dir
        self.job_location = os.path.join(
            pipeliner_instance.pipeline_dir, pipeliner_instance.output_name
        )
        # lists of job processes
        # self.bfactor_process = []
        self.molprobity_process = []
        # self.piscore_process = []
        # self.cablam_process = []
        # self.refmac_process = []
        # self.scores_process = []
        self.smoc_process = []
        # self.fdr_process = []
        # self.fsc_process = []
        # self.diffmap_process = []
        # self.sccc_process = []
        # self.dssp_process = []
        # self.jpred_process = []
        # set coot script
        # cs = SetCootScript(self.job_location)
        dict_local_processes = {}
        self.list_jobs = []
        # list of chains with outliers
        self.list_chains = []
        # list_summary_methods: methods for summary table
        self.list_summary_methods = []
        # dictionary with residue outlier details for each method
        # self.residue_outliers[method][mid][chain][res] = outlier type
        self.residue_outliers = OrderedDict()
        # dictionary with residue coordinates for each model
        self.residue_coordinates = {}
        # output report for global results
        gr = open("global_report.txt", "w")
        run_molprobity = pipeliner_instance.joboptions["run_molprobity"].get_boolean()
        run_smoc = pipeliner_instance.joboptions["run_smoc"].get_boolean()
        self.list_complete = []
        count_total_jobs = 0
        count_finished_jobs = 0
        count_failed_jobs = 0
        dict_num_jobs = {}
        num_local_process_complete = 0
        if run_smoc:
            num_local_process_complete += 1
            count_total_jobs += 1
            dict_num_jobs["SMOC"] = 1  # this needs to be fixed for multi model input
            smoc_output = os.path.join(
                self.job_location, pipeliner_instance.smoc_output
            )
            if os.path.isfile(smoc_output) and os.stat(smoc_output).st_size > 100:
                count_finished_jobs += 1
                self.list_complete.append("SMOC")
                sm = SetSMOCResults(
                    pipeliner_instance,
                    list_chains=self.list_chains,
                    glob_reportfile=gr,
                )
                for process_name in sm.residue_outliers:
                    if len(sm.residue_outliers[process_name]) != 0:
                        self.list_summary_methods.append(process_name)
                        for mapid in sm.residue_outliers[process_name]:
                            self.residue_outliers[process_name] = sm.residue_outliers[
                                process_name
                            ][mapid]

                for mapid in sm.residue_coordinates:
                    self.update_coordinates(sm.residue_coordinates[mapid])

                self.update_list_chains(sm.list_chains)

                gc.collect()
            else:
                count_failed_jobs += 1

        if run_molprobity:
            count_total_jobs += 1
            dict_num_jobs[
                "Molprobity"
            ] = 1  # this needs to be fixed for multi model input
            molprobity_output = os.path.basename(pipeliner_instance.molprobity_output)
            if (
                os.path.isfile(molprobity_output)
                and os.stat(molprobity_output).st_size > 100
            ):
                count_finished_jobs += 1
                mp = SetMolprobityResults(
                    pipeliner_instance,
                    list_chains=self.list_chains,
                    glob_reportfile=gr,
                )

                self.update_list_chains(mp.list_chains)
                if len(mp.residue_outliers) != 0:
                    self.list_summary_methods.append("molprobity")
                    self.residue_outliers["molprobity"] = mp.residue_outliers
                    self.update_coordinates(mp.residue_coordinates)
                self.list_complete.append("Molprobity")
                # collect non-ref/deleted items from the object
                gc.collect()
            else:
                count_failed_jobs += 1
        # set timeout for results
        # timeout = time.time() + 15 * 60

        gr.flush()
        gr.close()

        if not "PI-score" in self.list_complete or len(self.list_complete) > 2:
            # cluster functions class
            self.cl_out = cluster_residues()
            # cluster outliers spatially
            self.cluster_outliers()
            # set outlier summary table
            self.rsumm = residue_summary_table_settings()

            if len(self.list_summary_methods) > 0:
                self.list_summary_methods.insert(0, "residue")
                self.convert_outlierdict_to_dataframe()
                self.get_outlier_summary()

            self.cootset = None
            # self.add_cootdata()

    def update_list_chains(self, list_chains):
        """
        Update list of chains based on outliers from each method
        """
        for chain in list_chains:
            if chain not in self.list_chains:
                self.list_chains.append(chain)

    def get_outlier_summary(self):
        """
        Merge outlier types for each residue
        """
        self.dict_cluster_number = {}
        self.dict_coot_outlier_clusters = {}
        self.cluster_outlier_file = 'outlier_clusters.csv'
        oc = open(self.cluster_outlier_file,'w')
        oc.write(
                "#Molprobity: geometry outliers reported by molprobity.\n"
                "#SMOC: local fit to map outliers reported by TEMPy SMOC.\n"
                    )
        # chains
        count_chain = 0
        for chain in self.list_chains:
            dict_model_outlier = OrderedDict()
            chain_table_flag = False
            # methods
            for met in self.list_summary_methods[1:]:
                #                 for mapid in self.residue_outliers[met]:
                #                     if not mapid in dict_model_outlier:
                #                             dict_model_outlier[mapid] = {}
                #                     if not mapid in self.dict_cluster_number:
                #                             self.dict_cluster_number[mapid] = {}
                for pdbid in self.residue_outliers[met]:
                    if chain not in self.residue_outliers[met][pdbid]:
                        continue
                    if pdbid not in dict_model_outlier:
                        dict_model_outlier[pdbid] = {}
                    if pdbid not in self.dict_cluster_number:
                        self.dict_cluster_number[pdbid] = [1, 1]
                    # add outlier set to dict_model_outlier
                    self.add_new_residue_outlier_set(
                        self.residue_outliers[met][pdbid][chain],
                        dict_model_outlier[pdbid],
                        pdbid,
                        method=met,
                    )
                    if len(dict_model_outlier[pdbid]) > 0:
                        chain_table_flag = True
            count_chain += 1
            if chain_table_flag:
                self.set_summary_tables(dict_model_outlier, chain,
                                        oc, count_chain)
                self.dict_cluster_number[pdbid][1] = 1
                self.set_coot_dict_clusters(dict_model_outlier, chain)

    def set_summary_tables(self, dict_model_outlier, chain,
                           oc, count_chain):
        """
        Set results tables under each chain section
        with list of residues and outlier types
        """
        self.set_summary_table_chain(dict_model_outlier, chain,
                                     oc, count_chain)

    def set_summary_table_chain(self, dict_model_outlier, chain,
                                oc, count_chain):
        """
        Add outlier data to summary table for each chain
        """
        list_method_indices = [
            self.rsumm.data_titles.index(met) for met in self.list_summary_methods
        ]

        #         for mapid in dict_model_outlier:
        for pdbid in dict_model_outlier:
            #                 if mapid == 'nomap':
            tid = pdbid
            #                 else:
            #                     tid = mapid+'_'+pdbid
            dict_outlier = dict_model_outlier[pdbid]
            outlier_res = list(dict_outlier.keys())
            outlier_res.sort()
            data_array = []
            list_clust_sep = []
            if pdbid in self.dict_residue_clusters:
                # check if outliers form clusters
                try:
                    self.cl_out.check_seq_neigh_cluster(
                        outlier_res, self.dict_residue_clusters[pdbid][chain]
                    )
                    self.cl_out.merge_list_clusters(
                        self.dict_residue_clusters[pdbid][chain]
                    )
                    ct_cluster = 0
                    for res_list in self.dict_residue_clusters[pdbid][chain]:
                        flag_add_cluster = 0
                        for res in res_list:
                            if res in dict_outlier:
                                len_row = len(dict_outlier[res])
                                if len_row > 0:
                                    # for each cluster, first row includes cluster ID
                                    if flag_add_cluster == 0:
                                        ct_cluster += 1
                                        if ct_cluster < len(
                                            self.dict_residue_clusters[pdbid][chain]
                                        ):
                                            row_data = [str(ct_cluster)]
                                        # last cluster has individual outliers
                                        elif ct_cluster == len(
                                            self.dict_residue_clusters[pdbid][chain]
                                        ):
                                            row_data = ["-"]
                                        else:
                                            continue
                                        row_data.extend(dict_outlier[res])
                                        data_array.append(row_data)
                                    else:
                                        data_array.append(dict_outlier[res])
                                    flag_add_cluster += 1
                        if flag_add_cluster == 0:
                            continue
                        list_clust_sep.append(len(data_array))
                #                         #cluster separator
                #                         if ct_cluster < len(self.dict_residue_clusters[pdbid][chain]):
                #                             data_array.append([' ']*len_row)
                #                             data_array.append([' ']*len_row)
                except KeyError:
                    for res in outlier_res:
                        data_array.append(dict_outlier[res])
            else:
                for res in outlier_res:
                    data_array.append(dict_outlier[res])

            table_id = "summarytable" + chain + pdbid
            table_name = "Outlier summary table: " + tid
            list_horz_headers = [
                self.rsumm.list_horz_headers[index] for index in list_method_indices
            ]
            list_horz_headers = ["Cluster"] + list_horz_headers
            list_horz_headers_tips = [
                self.rsumm.list_horz_header_tooltips[index]
                for index in list_method_indices
            ]
            list_horz_headers_tips = ["Cluster ID"] + list_horz_headers_tips
            # adjust row span for cluster ID
            #             list_colors = list_cell_colors[:]
            #             while len(list_clust_sep) > len(list_colors):
            #                 list_colors.extend(list_colors)
            textfile = os.path.join(
                self.job_location, pdbid + "_" + chain + "_cluster_outliers.csv"
            )
            self.add_data_to_textfile(textfile, list_horz_headers, data_array,
                                      oc, count_chain)

    def add_data_to_textfile(self, textfile, list_horz_headers, data_array,
                            oc, count_chain):
        # CaBLAM: Backbone geometry outliers reported by molprobity.\n
        # Jpred: Secondary structure prediction from sequence inconsistent with model.\n
        with open(textfile, "w") as c:
            c.write(
                "#Molprobity: geometry outliers reported by molprobity.\n"
                "#SMOC: local fit to map outliers reported by TEMPy SMOC.\n"
                    )
            list_header = []
            ct_str = 0
            for header in list_horz_headers:
                formatted_header = " ".join(header.split("<br>"))
                formatted_header = "_".join(formatted_header.split())
                if ct_str < 2:
                    list_header.append("{}".format(formatted_header))
                else:
                    list_header.append("{}".format(formatted_header))
                ct_str += 1
            c.write(",".join(list_header) + "\n")
            if count_chain == 1:
                oc.write(",".join(list_header) + "\n")
            len_prev = 0
            for row in data_array:
                list_outl = []
                ct_str = 0
                if len_prev != 0 and len_prev - len(row) == 1:  # cluster number missing
                    list_outl.append("{}".format(prev_row[0]))
                    ct_str += 1

                for outl in row:
                    formatted_outl = "*".join(outl.split("<br>"))
                    if ct_str < 2:
                        list_outl.append("{}".format(formatted_outl))
                    else:
                        list_outl.append("{}".format(formatted_outl))
                    ct_str += 1
                c.write(",".join(list_outl) + "\n")
                oc.write(",".join(list_outl) + "\n")
                if len_prev <= len(row):
                    len_prev = len(row)
                    prev_row = row

    def set_df_csv(self, dict_data, data_id):
        df = pd.DataFrame.from_dict(dict_data, orient="index")
        # df1 = df.replace(np.nan,' ')
        df.to_csv(
            os.path.join(self.job_location, data_id + "_outliersummary.txt"),
            sep=",",
        )

    def update_coordinates(self, dict_coordinates):
        """
        Update residue CA coordinates for outliers
        """
        for pdbid in dict_coordinates:
            if not pdbid in self.residue_coordinates:
                self.residue_coordinates[pdbid] = dict_coordinates[pdbid]
            else:
                for chain in dict_coordinates[pdbid]:
                    if not chain in self.residue_coordinates[pdbid]:
                        self.residue_coordinates[pdbid][chain] = dict_coordinates[
                            pdbid
                        ][chain]
                    else:
                        for resnum in dict_coordinates[pdbid][chain]:
                            res_coordinate = dict_coordinates[pdbid][chain][resnum]
                            if len(res_coordinate) != 3:
                                continue
                            if not resnum in self.residue_coordinates[pdbid][chain]:
                                self.residue_coordinates[pdbid][chain][
                                    resnum
                                ] = res_coordinate
                            elif (
                                len(self.residue_coordinates[pdbid][chain][resnum]) != 3
                            ):
                                self.residue_coordinates[pdbid][chain][
                                    resnum
                                ] = res_coordinate

    def cluster_outliers(self):
        """
        Cluster outlier residues based on spatial coordinates
        Uses scipy kdtree
        """
        list_res_clusters = []
        self.dict_residue_clusters = {}
        # get outlier residues from residue_outliers dict
        #         for chain in self.list_chains:
        #             for met in self.list_summary_methods:
        #                 for pdbid in self.residue_outliers[met]:
        #                     if not chain in self.residue_outliers[met][pdbid]: continue
        #                     if not pdbid in self.dict_residue_clusters:
        #                         self.dict_residue_clusters[pdbid] = {}
        #                     #check if coordinate data is available
        #                     if not pdbid in self.residue_coordinates:
        #                         continue
        #                     if not chain in chain in self.residue_coordinates[pdbid]:
        #                         continue
        #                     list_coord = []
        #                     list_res = []
        #                     for res in self.residue_outliers[met][pdbid][chain]:
        #                         list_coord.append(self.residue_outliers[met][pdbid][chain][res])
        #                         list_res.append(str(res))
        #
        #         for mapid in self.residue_coordinates:
        #             if not mapid in self.dict_residue_clusters:
        #                     self.dict_residue_clusters[mapid] = {}
        for pdbid in self.residue_coordinates:
            if not pdbid in self.dict_residue_clusters:
                self.dict_residue_clusters[pdbid] = {}
            for chain in self.residue_coordinates[pdbid]:
                list_coord = []
                list_res = []
                for res in self.residue_coordinates[pdbid][chain]:
                    if str(res) in list_res:
                        continue
                    list_coord.append(self.residue_coordinates[pdbid][chain][res])
                    list_res.append(str(res))  # resid chain

                if len(list_coord) > 0:
                    list_res_clusters = self.cl_out.get_cluster_outliers(
                        list_res, list_coord
                    )
                    list_res_clusters.sort(key=len, reverse=True)

                self.dict_residue_clusters[pdbid][chain] = copy.deepcopy(
                    list_res_clusters
                )

    def set_coot_dict_clusters(self, dict_model_outlier, chain):
        """
        Save outlier clusters in a dictionary for coot fixing
        """
        for pdbid in self.dict_residue_clusters:
            #                 cl_id = mapid+':'+pdbid
            #                 if mapid == 'nomap': cl_id = pdbid
            if pdbid not in self.dict_coot_outlier_clusters:
                self.dict_coot_outlier_clusters[pdbid] = {}
            try:
                dict_outliers = dict_model_outlier[pdbid]
            except KeyError:
                continue
            try:
                list_clusters = self.dict_residue_clusters[pdbid][chain]
            except KeyError:
                continue
            #             try:
            #                 cl_number = self.dict_cluster_number[pdbid][0]
            #             except KeyError: continue
            for l in range(len(list_clusters)):
                if len(list_clusters[l]) == 0:
                    continue
                # sorted by cluster size until individuals
                if l > 0 and len(list_clusters[l - 1]) < len(list_clusters[l]):
                    break
                list_res = list_clusters[l]

                flag_cluster = False
                for res in list_res:
                    try:
                        residue_coordinate = self.residue_coordinates[pdbid][chain][res]
                    except KeyError:
                        continue
                    if res in dict_outliers:
                        # res,molprobity outlier string, cablam outlier string, smoc
                        outlier_types = dict_outliers[res][:]
                        # rename selected outliers with method name
                        # and merge with '\n'
                        for met_index in range(1, len(outlier_types)):
                            # different types within a method
                            split_type = outlier_types[met_index].split("<br>")
                            outlier_types[met_index] = "\n".join(split_type)
                            if outlier_types[met_index] == "-":
                                outlier_types[met_index] = ""
                                continue
                            # add method name to outlier type
                            if self.list_summary_methods[met_index] in [
                                "cablam",
                                "smoc",
                                "fdr",
                                "jpred",
                            ]:
                                outlier_types[met_index] = (
                                    self.list_summary_methods[met_index]
                                    + " "
                                    + outlier_types[met_index]
                                )
                        # remove no outlier methods
                        ct_pop = 0
                        for met_index in range(len(outlier_types)):
                            if met_index > len(outlier_types):
                                break
                            if len(outlier_types[met_index - ct_pop]) == 0:
                                outlier_types.pop(met_index - ct_pop)
                                ct_pop += 1

                        outlier_types_string = "\n".join(
                            outlier_types[1:]
                        )  # 0: 'Residue'

                        flag_cluster = True
                        try:
                            self.dict_coot_outlier_clusters[pdbid]["clusters"].append(
                                (
                                    chain,
                                    res,
                                    self.dict_cluster_number[pdbid][1],
                                    outlier_types_string,
                                    residue_coordinate,
                                )
                            )
                        except KeyError:
                            self.dict_coot_outlier_clusters[pdbid]["clusters"] = [
                                (
                                    chain,
                                    res,
                                    self.dict_cluster_number[pdbid][1],
                                    outlier_types_string,
                                    residue_coordinate,
                                )
                            ]
                if flag_cluster:
                    self.dict_cluster_number[pdbid][0] += 1  # model
                    self.dict_cluster_number[pdbid][1] += 1  # chain
            # break # compute coot data for the first map based outliers only

    def add_cootdata(self):
        """
        Add cluster outliers for fixing in Coot
        """
        if self.cootset is not None:
            for pdbid in self.dict_coot_outlier_clusters:
                self.cootset.add_data_to_cootfile(
                    self.dict_coot_outlier_clusters[pdbid],
                    modelid=pdbid,
                    method="clusters",
                )

    def convert_outlierdict_to_dataframe(self):
        """
        Generate a dataframe of residue outliers
        """
        dict_df = {}
        for met in self.list_summary_methods[1:]:
            #             for mapid in self.residue_outliers[met]:
            #                 self.residue_outliers[met][mapid] = {}
            for pdbid in self.residue_outliers[met]:
                #                 if mapid == 'nomap':
                df_id = pdbid
                #                 else:
                #                     df_id = mapid+':'+pdbid
                if not df_id in dict_df:
                    dict_df[df_id] = {}
                for chain in self.residue_outliers[met][pdbid]:
                    for res in self.residue_outliers[met][pdbid][chain]:
                        outlier_det = self.residue_outliers[met][pdbid][chain][res]
                        if isinstance(outlier_det, list):
                            outlier_string = ":".join(outlier_det)
                        else:
                            outlier_string = outlier_det
                        try:
                            res_key = int(res)
                        except ValueError:
                            res_key = res.strip()

                        try:
                            dict_df[df_id][(chain, res_key)][met] = outlier_string
                        except KeyError:
                            dict_df[df_id][(chain, res_key)] = {}
                            dict_df[df_id][(chain, res_key)][met] = outlier_string
        for df_id in dict_df:
            self.set_df_csv(dict_df[df_id], df_id)

    def add_new_residue_outlier_set(
        self, residue_outliers, dict_outlier, pdbid, method="molprobity"
    ):
        """
        Add outlier set to the dictionary dict_outlier[res][method] = outlier name(s)
        """
        # index for summary table
        met_index = self.list_summary_methods.index(method)
        len_table = len(self.list_summary_methods)
        for res in residue_outliers:
            outlier_det = residue_outliers[res]
            if isinstance(outlier_det, list):
                outlier_string = "<br>".join(outlier_det)
            else:
                outlier_string = outlier_det
            res_key = res.strip()
            #             try:
            #                 res_key = int(res)
            #             except ValueError:
            #                 res_key = res.strip()
            # set outlier dictionary:
            # [res,outlier type1,outlier type 2,..]
            try:
                dict_outlier[res_key][met_index] = outlier_string
            except (KeyError, IndexError) as exc:
                dict_outlier[res_key] = [res]
                dict_outlier[res_key].extend(["-"] * (len_table - 1))
                dict_outlier[res_key][met_index] = outlier_string


def main():
    PipelineResultsViewer(sys.argv[1])


if __name__ == "__main__":
    main()
