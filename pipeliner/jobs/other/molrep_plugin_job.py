#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from pipeliner.pipeliner_job import PipelinerJob, Ref
from pipeliner.job_options import (
    MultipleChoiceJobOption,
    InputNodeJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.data_structure import Node
from pipeliner.display_tools import (
    create_results_display_object,
)

# input nodes
INPUT_NODE_MAP = "DensityMap.mrc"
INPUT_NODE_COORDS = "AtomCoords.pdb"
OUTPUT_NODE_COORDS = "AtomCoords.pdb"


class MolrepJob(PipelinerJob):
    """Molecular replacement with Molrep.
    Covers standard rot fn + trans fn, as well as SAPTF. These represent two methods for
    locating search models in the cryoEM map, but have the same input and output nodes
    and so are considered in the same class.
    """

    PROCESS_NAME = "molrep.fit_model"
    OUT_DIR = "Molrep"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.jobinfo.display_name = "Molrep"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn"
        self.jobinfo.short_desc = "Molecular replacement with Molrep"
        self.jobinfo.long_desc = (
            "Molrep fits one or more atomic models into a cryoEM map by the technique"
            " of Molecular Replacement. The spherically averaged phased translation "
            "function locates the model in the cryoEM map using a spherically averaged"
            " calculated map, then determines its optimal orientation and refines the "
            "position. The rotation translation function is the more traditional "
            "molecular replacement method. The latter is generally faster but less "
            "sensitive."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/molrep.html"
        self.jobinfo.programs = ["molrep", "ccpem-python"]
        self.jobinfo.references = [
            Ref(
                authors=["A.Vagin", "A.Teplyakov"],
                title="Molecular replacement with MOLREP",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2010",
                volume="66",
                issue="1",
                pages="22-25",
                doi="10.1107/S0907444909042589",
            ),
            Ref(
                authors=["A.A.Vagin", "M.N.Isupov"],
                title=(
                    "Spherically averaged phased translation function and"
                    " its application to the search for molecules and fragments"
                    " in electron-density maps"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2001",
                volume="57",
                issue="10",
                pages="1451-1456",
                doi="10.1107/s0907444901012409",
            ),
        ]

        # TODO: need nice names for display, but easy ones for use
        self.joboptions["mode"] = MultipleChoiceJobOption(
            label="Mode",
            choices=["SAPTF", "RTF"],
            default_value_index=0,
            help_text="Choice of search methods to use.",
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=INPUT_NODE_MAP,
            default_value="",
            directory="",
            pattern="3D map (.mrc)",
            help_text="The input cryoEM map into which models will be placed",
            is_required=True,
        )

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=INPUT_NODE_COORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be fitted",
            is_required=True,
        )

        self.joboptions["copies"] = IntJobOption(
            label="Copies to find",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=("Number of copies of search model to find."),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["subunits"] = IntJobOption(
            label="Subunits in search model",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of identical monomers present in search model."
                "Specify for multimers."
            ),
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

    def get_commands(self):
        # CCP-EM gui creates:
        #    molrep -m 5me2_a.pdb -f emd_3488.map -i << eof
        #    _NMON 4
        #    _PRF S
        #    NCSM 1
        #    stick n
        #    eof

        # TODO. This doesn't work yet. Not sure how to format this kind of input.
        # Molrep has alternative syntax with more switches which could be used instead.
        input_model_file = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        input_map_file = self.joboptions["input_map"].get_string(
            True, "Input file missing"
        )
        command = [
            "molrep",
            "-m",
            input_model_file,
            "-f",
            input_map_file,
            "-i <<eof",
            "\\",
        ]

        if self.joboptions["mode"].get_string() == "SAPTF":
            command += ["_PRF", "S", "\\"]
        ncopies = self.joboptions["copies"].get_number()
        command += ["_NMON", str(ncopies), "\\"]
        nsubunits = self.joboptions["subunits"].get_number()
        command += ["NCSM", str(nsubunits), "\\"]
        command += ["stick", "n", "\\"]
        command += ["eof", "\\"]

        fixing_com = ["ccpem-python -m ccpem_core.tasks.molrep.fix_output_pdb"]

        output_file = os.path.join(self.output_name, "molrep.pdb")
        self.output_nodes.append(Node(output_file, OUTPUT_NODE_COORDS))

        # needs to return a list of lists
        final_commands_list = [command, fixing_com]
        return final_commands_list

    def create_results_display(self):
        report_dir = os.path.join(self.output_name, "molrep.doc")
        return [
            create_results_display_object(
                "text", title="Molrep output", display_data=report_dir
            )
        ]
