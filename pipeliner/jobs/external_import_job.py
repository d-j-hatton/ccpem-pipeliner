#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import json
import os
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import FileNameJobOption
from pipeliner.data_structure import Node
from pipeliner.display_tools import create_results_display_object


class ExternalProcessingImportJob(PipelinerJob):
    PROCESS_NAME = "external.processingimport"
    OUT_DIR = "Import"

    def __init__(self):
        super(self.__class__, self).__init__()

        self.jobinfo.display_name = "Import external data"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.short_desc = "Import results from outside the pipeliner"
        self.jobinfo.long_desc = (
            "This job is used to incorporate the results of external processing"
            " where data was removed from the pipeliner, processed with another "
            "program and then re-incorporated into the pipeliner. "
            "A json file with a specific format is required. This can be written"
            " manually or generated with a script.\nThe format for this file is:"
            "\n{\n\t'InputNodes':{\n\t\t'<file_name>': '<node_type>',"
            "\n\t\t'<file_name>': '<node_type>'\n\t},\n\t'OutputNodes':{\n"
            "\t\t'<file_name>': '<node_type>',\n\t\t'<file_name>':\t'<node_type>',"
            "\n\t},\n\t'ProcessingDescription': <processing description>\n}\n\n"
            "The <processing description> can be a simple string IE: 'I did a thing'"
            " or it can be a more complex dict:\n{\n\t'Step 1': 'I did a thing',\n  "
            "\t'Step 2': 'I did another thing',\n\t'Step 3':{\n\t\t'substep a': "
            "'A result',\n\t\t'substep b': 'B result'\n\t}\n}\nSee the pipeliner"
            "documentation for an description of node types"
        )

        self.joboptions["external_import_data"] = FileNameJobOption(
            label="External import json data file:",
            default_value="",
            pattern="*.json",
            directory=".",
            help_text=(
                "This file contains information about the processing steps that"
                " were carried out externally.  See the pipeliner documentation for"
                " the format of the file"
            ),
            is_required=True,
        )

    def get_commands(self):
        # read the input file:
        coms = []
        data_file = self.joboptions["external_import_data"].get_string(
            True, "External processing file not found"
        )
        with open(data_file, "r") as df:
            inputdata = json.load(df)

        for node in inputdata["InputNodes"]:
            self.input_nodes.append(Node(node, inputdata["InputNodes"][node]))

        # make sure no nodes are duplicated:
        # first see if the filenames are unique
        depth = 1
        nodes_dict = {}
        nodes_list = [os.path.basename(x) for x in inputdata["OutputNodes"]]
        if len(set(nodes_list)) == len(nodes_list):
            depth = 0

        # if any of the nodes have same names add directories until all are unique
        if depth > 0:
            max_length = max([len(x.split("/")) for x in inputdata["OutputNodes"]])
            cycle = True
            depth = -1
            while cycle:
                depth += -1
                if -depth > max_length:
                    raise ValueError(
                        "2 or more of the nodes in the external_processing "
                        "have identical names!"
                    )
                nodes_dict = {}
                for node in inputdata["OutputNodes"]:
                    shortname = "/".join(node.split("/")[depth:])
                    nodes_dict[shortname] = (node, inputdata["OutputNodes"][node])

                if len(nodes_dict) != len(inputdata["OutputNodes"]):
                    continue
                cycle = False

        if depth == 0:
            # if all node files names are unique just copy them all in
            for node in inputdata["OutputNodes"]:
                inname = os.path.join(self.output_name, os.path.basename(node))
                coms.append(["cp", os.path.abspath(node), inname])
                self.output_nodes.append(Node(inname, inputdata["OutputNodes"][node]))
        elif depth < 0:
            # if the file names are not unique then a directory structure must be
            # created
            for node in nodes_dict:
                inname = os.path.join(self.output_name, node)
                coms.append(["mkdir", "-p", os.path.dirname(inname)])
                coms.append(["cp", os.path.abspath(nodes_dict[node][0]), inname])
                self.output_nodes.append(Node(inname, nodes_dict[node][1]))

        # copy in the info file
        coms.append(
            [
                "cp",
                data_file,
                os.path.join(self.output_name, "external_processing.json"),
            ]
        )
        return coms

    def gather_metadata(self):
        data_file = os.path.join(self.output_name, "external_processing.json")
        with open(data_file, "r") as df:
            metadata = json.load(df)["ProcessingDescription"]
        return metadata

    def create_results_display(self):
        input_nodes = [[x.name, x.type] for x in self.input_nodes]
        innodes = create_results_display_object(
            "table",
            title="Inputs",
            headers=["File", "NodeType"],
            table_data=input_nodes,
            associated_data=[x[0] for x in input_nodes],
        )
        output_nodes = [[x.name, x.type] for x in self.output_nodes]
        outnodes = create_results_display_object(
            "table",
            title="Outputs",
            headers=["File", "NodeType"],
            table_data=output_nodes,
            associated_data=[x[0] for x in output_nodes],
        )

        data_file = os.path.join(self.output_name, "external_processing.json")
        with open(data_file, "r") as df:
            dat = json.load(df)["ProcessingDescription"]
        pretty_data = str(json.dumps(dat, indent=2))
        p_data = pretty_data.replace("{", "").replace("}", "").replace('"', "")
        proc_desc = create_results_display_object(
            "text",
            title="Processing Info",
            display_data=p_data,
            associated_data=[data_file],
        )

        return [innodes, outnodes, proc_desc]
