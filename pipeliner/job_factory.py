#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""The job factory functions identify the available job types and return
the correct type of job from the job type specified in a parameter file"""

import os
import pkg_resources
from typing import Optional

from pipeliner.data_structure import (
    Process,
    PROC_NUM_2_GENERAL_NAME,
    JOBNAMES_NEED_CONVERSION,
)
from pipeliner.job_options import TRUES, FloatJobOption, IntJobOption
from pipeliner.jobstar_reader import JobStar, get_job_type, convert_proctype
from pipeliner.pipeliner_job import PipelinerJob


def gather_all_jobtypes() -> dict:
    """Assemble a dict of all the available job types

    Returns:
        dict: The job classes dict

        The dict keys are the job process names and it returns the class
        for that specific job type

    Raises:
        ValueError: If a process name is being used by more than one job type
    """
    job_dict = {
        el.name: el.load()
        for el in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs")
    }
    return job_dict


def read_job(filename: str) -> PipelinerJob:
    """
    Reads run.job and job.star files and returns the correct Pipeliner job class

    Args:
        filename (str): The run.job or job.star file

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        ValueError: If the file name entered does not end with run.job or job.star
        RuntimeError: If the input file is in the RELION 3.1 format and conversion fails
        RuntimeError: If the job type specified in the file cannont be found
    """

    if filename.endswith(".job"):
        with open(filename) as job_file:
            job_type_raw = job_file.readline().split("==")[1].strip(" \n")
            # for back compatibility with 3.1 - convert integer job types
            job_type: str = ""
            try:
                job_type = PROC_NUM_2_GENERAL_NAME[int(job_type)].lower()
            except ValueError:
                job_type = job_type_raw.lower()
            # get the job options in case they are needed for conversion
            job_options = {}
            for line in job_file.readlines():
                if "==" in line:
                    ls = line.split("==")
                    job_options[ls[0].strip()] = ls[1].strip()

    elif filename.endswith("job.star"):
        job_options = JobStar(filename).get_all_options()
        job_type = get_job_type(job_options)[0].lower()
    else:
        raise ValueError(f"ERROR: The file {filename} is not a valid Relion job file")

    # Check if the job name is in the lis of jobnames that are ambigious
    # between RELION4 and the pipeliner. Convert the name to the pipeliner
    # standard if necessary

    if job_type in JOBNAMES_NEED_CONVERSION:
        jt_old = job_type
        job_type, success = convert_proctype(job_type, job_options)
        if not success:
            raise RuntimeError(
                f"ERROR: Problem converting job type {jt_old} to the pipeliner"
                f" format:\n{job_type}"
            )
    # get the job object for the specific job type
    try:
        job = new_job_of_type(job_type, job_options)

    except RuntimeError:
        raise RuntimeError(
            f"Cannot find job type {job_type}, used in job file {filename}"
        )
    job.read(filename)

    return job


def convert_relion4_jobtypes_to_pipeliner(type_name, joboptions, jobs_dict):
    """Convert an ambigious Relion4 style job type to the pipeliner type

    Args:
        type_name (str): The current relion4 type for the job
        joboptions (dict): The job's joboptions
        jobs_kdict (dict): The job_factory jobs and classes dict, output from
            gather_jobtypes()

        Returns:
            :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job object for
                the convert pipeler version of the job
    """
    convname, converted = convert_proctype(type_name, joboptions)
    if converted:
        print(f"RELION4 job type {type_name} converted to {convname}")
        job_type = jobs_dict.get(convname)
        if job_type is not None:
            return job_type()


def new_job_of_type(type_name: str, joboptions: Optional[dict] = None) -> PipelinerJob:
    """Creates a new object of the correct PipelinerJob sub-type

    Args:
        type_name (str): The job process name
        joboptions (dict): Dict of the job's joboptions, only necessary if
            converting from a RELION4.0 style jobname

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        RuntimeError: If the job type is not found
    """
    jobs_dict = gather_all_jobtypes()
    job_type = jobs_dict.get(type_name)
    if job_type is not None:
        thejob = job_type()
        thejob.check_for_programs()
        return thejob

    # check if the jobtype is a RELION4 style name
    if joboptions is not None:
        relion4_type = convert_relion4_jobtypes_to_pipeliner(
            type_name,
            joboptions,
            jobs_dict,
        )
        if relion4_type is not None:
            relion4_type.check_for_programs()
            return relion4_type

    raise RuntimeError(f"Unrecognised job type {type_name}")


def job_from_dict(job_input: dict) -> PipelinerJob:
    """Create a job from a dictionary

    The dict must define the job type with a '_rlnJobTypeLabel'
    key.  Any other keys will override the default options for
    that parameter

    Args:
        job_input (dict): The dict containing the params. At minimum it must
            contain `{'_rlnJobTypeLabel': <jobtype>}`

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        ValueError: If the '_rlnJobTypeLabel' key is missing from the dict
        ValueError: If '_rlnJobIsContinue' is in the dict - this function is not for
            creating continuations of jobs
        ValueError: If the specified jobtype is not found
        ValueError: If any of the parameters in the dict are not in the jobtype
            returned
    """
    j_type = job_input.get("_rlnJobTypeLabel")
    if j_type is None:
        raise ValueError(
            "Cannot create a job from this dict; key '_rlnJobTypeLabel'" " is missing"
        )

    try:
        job = new_job_of_type(j_type)
    except RuntimeError:
        raise ValueError(f"Cannot find job type {j_type} to create a new job")

    # set the parameters
    for param in job_input:
        if param not in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            try:
                job.joboptions[param].value = job_input[param]
            except KeyError:
                raise ValueError(
                    f"Parameter {param} not found in jobtype {j_type} "
                    "cannot create a job of this type from this dict"
                )

    # set continuation
    is_continue = job_input.get("_rlnJobIsContinue")
    if is_continue is not None:
        if is_continue is True:
            job.is_continue = True
        elif is_continue.lower() in TRUES:
            job.is_continue = True

    is_tomo = job_input.get("_rlnJobIsTomo")
    if is_tomo is not None:
        if is_tomo is True:
            job.is_tomo = True
        elif is_tomo.lower() in TRUES:
            job.is_tomo = True
    job.check_for_programs()
    return job


def active_job_from_proc(the_proc: Process) -> PipelinerJob:
    """Create an active job from an existing process

    Used when the functions inside a job subclass need to be called on an existing
    job

    Args:
        the_proc (:class:`~pipeliner.data_structure.Process`): The
            process to create a job from

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass
            object for the process

    """
    jobstar = os.path.join(the_proc.name, "job.star")
    if os.path.isfile(jobstar):
        jobops = JobStar(jobstar).get_all_options()
    else:
        raise ValueError(f"job.star file not found for job {the_proc.name}")

    # check if the jobtype is a RELION4 style name
    relion4_type = convert_relion4_jobtypes_to_pipeliner(
        the_proc.type,
        jobops,
        gather_all_jobtypes(),
    )
    if relion4_type is not None:
        the_proc.type = relion4_type.PROCESS_NAME

    # create the new job
    the_job = new_job_of_type(the_proc.type, jobops)
    the_job.check_for_programs()
    if jobops is not None:
        for jo in jobops:
            # try/except to ignoe nonexistant joboptions
            try:
                filljo = the_job.joboptions[jo]
                if isinstance(filljo, FloatJobOption):
                    the_job.joboptions[jo].value = float(jobops[jo])
                elif isinstance(filljo, IntJobOption):
                    # float the number first to deal with strings with decimal points
                    the_job.joboptions[jo].value = int(float(jobops[jo]))
                else:
                    the_job.joboptions[jo].value = jobops[jo]
            except KeyError:
                pass
    the_job.output_name = the_proc.name
    the_job.output_nodes = the_proc.output_nodes
    the_job.input_nodes = the_proc.input_nodes
    return the_job
