#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from typing import NamedTuple, List, Optional, Tuple
from collections.abc import Mapping, Iterable
from copy import deepcopy
from collections import namedtuple
from glob import glob

from pipeliner.utils import decompose_pipeline_filename, date_time_tag
from pipeliner.jobstar_reader import RelionStarFile
from pipeliner.mrc_image_tools import get_mrc_stats, get_tiff_stats
from pipeliner import __version__
from pipeliner.job_factory import active_job_from_proc


class IMAGE(
    namedtuple(
        "movie",
        [
            "file",
            "ext",
            "n_frames",
            "dimx",
            "dimy",
            "dtype",
            "headtype",
            "apix",
            "voltage",
            "spab",
        ],
    )
):
    """Object for storing info about a raw micrograph

    Attributes:
        file (str): The path to the file relative to the project dir
        ext (str): File extension, no '.'
        n_frames (int): Number of frames in the movie (image Z size)
        dimx (int): Image X size in pixels
        dimy (int): Image Y size in pixels
        dtype (string): data type
        headtype (string): header type
        apix (float): Pixel size in angstrom
        voltage (float): Excitation voltage of the microscope in kEv
        spab (float): Spherical aberration of the microscope in mm
    """

    pass


def get_procs_list(terminal_job, pipeline):
    """make a list of all the process objects for the jobs in a project

    Args:
        terminal_job (str): The name of the final job in the workflow used for making
            the deposition
        pipeline (:class:`~pipeliner.project_graph.Pipeline`): The pipeline
            for the project

    Returns:
        list: The :class:`~pipeliner.data_structure.Process` objects for the terminal
            job and its parents, in order by job number

    Raises:
        ValueError: If the terminal job was not found
    """
    term_proc = pipeline.find_process(terminal_job)
    if term_proc is None:
        raise ValueError(f"Process for {terminal_job} not found")
    upstream = pipeline.get_upstream_network(term_proc)
    procs = {term_proc}
    for j in upstream:
        procs.add(j[1])
    proclist = list(procs)
    proclist.sort(key=lambda x: decompose_pipeline_filename(x.name)[1], reverse=True)
    return proclist


def get_imgfile_info(imgfile, blockname, img_block_col):
    """Get information from the starfile containing image info

    Args:
        imgfile (str): The node object for the
            file
        blockname (str): Name of the images block in the starfile
        img_bock_col (str): The name of the column for the images in the image
            data block of the starfile

    Returns:
        dict: Dict with info about the opitcs groups `{og_number:
            (apix, voltage, sphere. ab)}`
        list: A list of full paths (relative to the working dir)
            for all of the images in the file, except in the case of movies
            then the path is relative to import dir
    """
    imgs_sf = RelionStarFile(imgfile)
    # optics groups info
    opticsblock = imgs_sf.get_block("optics")
    og_info = opticsblock.find(
        [
            "_rlnOpticsGroup",
            "_rlnMicrographOriginalPixelSize",
            "_rlnVoltage",
            "_rlnSphericalAberration",
        ]
    )
    og_dict = {}  # {og number: pixelsize}
    for og in og_info:
        og_dict[og[0]] = (float(og[1]), float(og[2]), float(og[3]))

    # get optics group info
    imgsblock = imgs_sf.get_block(blockname)
    all_img = imgsblock.find([img_block_col, "_rlnOpticsGroup"])

    return og_dict, all_img


def prepare_EMPIAR_raw_mics(movfile):
    """Prepare the raw micrographs portion of an EMPIAR deposition

    Returns:
        List[:class:`~pipeliner.onedep_deposition.EmpiarMovieSetType`]: A
            DepositionObject used to create a deposition
    """
    depo_objs = []

    # separate out all the movies and make sure they have the same
    # pixelsize, data type and etc...

    og_dict, movs = get_imgfile_info(movfile, "movies", "_rlnMicrographMovieName")

    # prepend the import dir name to each move file
    dn = os.path.dirname(movfile)
    full_path_movs = [(os.path.join(dn, x[0]), x[1]) for x in movs]

    # make the sets to split upt the movies
    mov_sets = set([os.path.dirname(x[0]) for x in full_path_movs])
    split_movies = {}  # {dir_name: [IMAGE, IMAGE, ...,  IMAGE]}
    for ms in mov_sets:
        split_movies[ms] = []

    # get info about each movie, tiffs and mrcs handled separately
    donegroups = []
    for movie in full_path_movs:
        mov_group = os.path.dirname(movie[0])
        movie_error = False
        # tiffs
        if movie[0].split(".")[-1] in ("tif", "tiff"):
            if mov_group not in donegroups:
                tiffstats = get_tiff_stats(movie[0])
            if len(tiffstats[0]) != 3:
                dimx, dimy = tiffstats[0][0]
                n_frames = len(tiffstats)
                dtype = EMPIAR_TIFF_DATATYPES[tiffstats[0][6].split(": ")[1]]
            # just in case of single frame movies
            else:
                dimx, dimy, n_frames = tiffstats[0][0]
                dtype = EMPIAR_TIFF_DATATYPES[tiffstats[6].split(": ")[1]]
            headtype = "('T3', '')"

        # mrcs
        elif movie[0].split(".")[-1] in ("mrc", "mrcs"):
            if mov_group not in donegroups:
                mstats = get_mrc_stats(movie[0])
            if len(mstats[0]) != 3:
                dimx = mstats[0][0][0]
                dimy = mstats[0][0][1]
                n_frames = len(mstats)
                dtype = EMPIAR_DATATYPES[int(mstats[0][6].split(": ")[0])]
            # in case of single frame movies
            else:
                dimx, dimy, n_frames = mstats[0]
                dtype = EMPIAR_DATATYPES[int(mstats[6].split(": ")[0])]
            headtype = "('T2', '')"
        else:
            print(f"WARNING: Unknown movie type encountered for {movie[0]}")
            movie_error = True

        if not movie_error:
            # make a IMAGE object for this movie
            the_mov = IMAGE(
                file=movie[0],
                ext=movie[0].split(".")[-1],
                n_frames=n_frames,
                dimx=dimx,
                dimy=dimy,
                headtype=headtype,
                dtype=dtype,
                apix=og_dict[movie[1]][0],
                voltage=og_dict[movie[1]][1],
                spab=og_dict[movie[1]][2],
            )
            # assign the movie to its group
            split_movies[mov_group].append(the_mov)
            donegroups.append(mov_group)

    # make the movie imagesets
    for movset in split_movies:
        # check that all movies in this dir have same stats
        nf = ("frame number", set([x.n_frames for x in split_movies[movset]]))
        xs = ("x dimension", set([x.dimx for x in split_movies[movset]]))
        ys = ("y dimension", set([x.dimy for x in split_movies[movset]]))
        ap = ("pixel size", set([x.apix for x in split_movies[movset]]))
        dt = ("data type", set([x.dtype for x in split_movies[movset]]))
        ht = ("header format", set([x.headtype for x in split_movies[movset]]))
        ex = ("file extension", set([x.ext for x in split_movies[movset]]))
        # don't need to check these two
        vt = set([x.voltage for x in split_movies[movset]])
        sa = set([x.spab for x in split_movies[movset]])
        problem = False
        for check in [nf, xs, ys, ap, dt, ht, ex]:
            if len(check[1]) > 1:
                print(
                    f"WARNING: Skipping movies in {movset} because {check[0]}"
                    "is not the same for every movie"
                )
                problem = True
        if problem:
            continue

        # text for spherical abberation and voltage in details field
        spab = str(list(sa)) if len(list(sa)) > 1 else list(sa)[0]
        vlt = str(list(vt)) if len(list(sa)) > 1 else list(sa)[0]

        # prepare the common file path for this movie set
        this_set = [x.file for x in split_movies[movset]]
        reversed_strings = [x[::-1] for x in this_set]
        reversed_lcs = os.path.commonprefix(reversed_strings)
        lcs = reversed_lcs[::-1]
        file_path = f"{os.path.commonprefix(this_set)}*{lcs}"
        deets = (
            f"Voltage {vlt}; Spherical aberration {spab}; Movie data in file: "
            f"{movfile}; Prepared by ccpem-pipeliner vers "
            f"{__version__}"
        )
        depoobj = EmpiarMovieSetType(
            name="Multiframe micrograph movies",
            directory=movset,
            category="('T2', '')",
            header_format=str(list(ht[1])[0]),
            data_format=str(list(dt[1])[0]),
            num_images_or_tilt_series=len(split_movies[movset]),
            frames_per_image=int(list(nf[1])[0]),
            voxel_type=str(list(dt[1])[0]),
            pixel_width=float(list(ap[1])[0]),
            pixel_height=float(list(ap[1])[0]),
            details=deets,
            image_width=int(list(xs[1])[0]),
            image_height=int(list(ys[1])[0]),
            micrographs_file_pattern=file_path,
        )
        depo_objs.append(depoobj)
    return depo_objs


def prepare_EMPIAR_mics_parts(mpfile, is_parts, is_cor_parts):
    """Prepare the micrographs or particles portion of an EMPIAR deposition

    Args:

        mpfile (str): The name of the file containing the micrographs or particles
        name (str): the name for this imageset
        is_part (bool): Is the image set particles? will affect the info in the
            details
    Returns:
        list: A list of deposition objects
    """
    # separate out all the micrographs and make sure they have the same
    # pixelsize, data type and etc...

    imgs_block = "particles" if is_parts else "micrographs"
    img_file_col = "_rlnImageName" if is_parts else "_rlnMicrographName"
    og_dict, full_path_mics = get_imgfile_info(mpfile, imgs_block, img_file_col)

    # make the sets to split up the micrographs
    # strip the particle numbers if necessary
    if is_parts:
        full_path_mics = set([(x[0].split("@")[1], x[1]) for x in full_path_mics])
        mic_sets = set([os.path.dirname(x[0]) for x in full_path_mics])
        header_type = "T2"
        if is_cor_parts:
            category = "T6"
            name = "Per-particle motion corrected particle images"

        else:
            category = "T5"
            name = "Particle images"

    else:
        mic_sets = set([os.path.dirname(x[0]) for x in full_path_mics])
        header_type = "T1"
        category = "T1"
        name = "Corrected micrographs"

    split_micrographs = {}  # {dir_name: [IMAGE, IMAGE, ...,  IMAGE]}
    for ms in mic_sets:
        split_micrographs[ms] = []

    # get info about each micrograph, tiffs and mrcs handled separately
    # TODO: This is way to slow, maybe only do the first of each?
    exp_ext = "mrcs" if is_parts else "mrc"
    donegroups = []
    for micrograph in full_path_mics:
        mic_group = os.path.dirname(micrograph[0])
        micrograph_error = False
        if micrograph[0].split(".")[-1] == exp_ext:
            # the stats are only calculated on the 1st mic in the dir
            # assumes they have the same type and dimensions
            # doing it for every mic is very slow...
            if mic_group not in donegroups:
                mstats = get_mrc_stats(micrograph[0])
            dimx = mstats[0][0]
            dimy = mstats[0][1]
            n_frames = mstats[0][2]
            dtype = EMPIAR_DATATYPES[int(mstats[6].split(":")[0])]
            headtype = f"('{header_type}', '')"
        else:
            print(f"WARNING: Unknown image type encountered for {micrograph[0]}")
            micrograph_error = True

        if not micrograph_error:
            # make a IMAGE object for this micrograph,
            the_mic = IMAGE(
                file=micrograph[0],
                ext=micrograph[0].split(".")[-1],
                n_frames=n_frames,
                dimx=dimx,
                dimy=dimy,
                headtype=headtype,
                dtype=dtype,
                apix=og_dict[micrograph[1]][0],
                voltage=og_dict[micrograph[1]][1],
                spab=og_dict[micrograph[1]][2],
            )
            # assign the micrograph to its group
            split_micrographs[mic_group].append(the_mic)
            donegroups.append(mic_group)

    # make the micrograph imagesets
    depoobjs = []
    for micset in split_micrographs:
        # check that all micrographs in this dir have same stats

        nf = 1  # number of frames is always 1

        xs = ("x dimension", set([x.dimx for x in split_micrographs[micset]]))
        ys = ("y dimension", set([x.dimy for x in split_micrographs[micset]]))
        ap = ("pixel size", set([x.apix for x in split_micrographs[micset]]))
        dt = ("data type", set([x.dtype for x in split_micrographs[micset]]))
        ht = ("header format", set([x.headtype for x in split_micrographs[micset]]))
        # don't need to check these two
        vt = set([x.voltage for x in split_micrographs[micset]])
        sa = set([x.spab for x in split_micrographs[micset]])

        # this checking is currently disabled, because we are not
        # actually checking stats on every image, it's too slow

        # problem = False
        # for check in [xs, ys, ap, dt, ht, ex]:
        #    if len(check[1]) > 1:
        #        print(
        #            f"WARNING: Skipping micrographs in {micset} because {check[0]}"
        #            "is not the same for every micrograph"
        #        )
        #        problem = True
        # if problem:
        #    continue

        # text for spherical abberation and voltage in details field
        spab = str(list(sa)) if len(list(sa)) > 1 else list(sa)[0]
        vlt = str(list(vt)) if len(list(sa)) > 1 else list(sa)[0]

        part_count = RelionStarFile(mpfile).count_block("particles") if is_parts else 0

        # prepare the commont file path for this micrograph set
        this_set = [x.file for x in split_micrographs[micset]]
        reversed_strings = [x[::-1] for x in this_set]
        reversed_lcs = os.path.commonprefix(reversed_strings)
        lcs = reversed_lcs[::-1]
        file_pat = f"{os.path.commonprefix(this_set)}*{lcs}"
        pcount = f"{part_count} total particles; " if is_parts else ""
        deets = (
            f"{pcount}Voltage {vlt}; Spherical aberration {spab}; "
            f"Image data in file: {mpfile}; Prepared by ccpem-"
            f"pipeliner vers {__version__}"
        )
        if is_cor_parts:
            depoobj = EmpiarRefinedParticlesType(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
                picked_particles_file_pattern=mpfile,
            )
        elif is_parts:
            depoobj = EmpiarParticlesType(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
                picked_particles_file_pattern=mpfile,
            )
        else:
            depoobj = EmpiarCorrectedMicsType(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
            )
        depoobjs.append(depoobj)

    return depoobjs


def expand_nt(nt):
    """Expand a series of nested namedtuples into a dictionary

    Args:
        nt (namedtuple): The top-level named tuple

    returns:
        dict: The dict generated from the nested namedtuples
    """

    def recurse(x):
        return map(expand_nt, x)

    def nt_is(x):
        return isinstance(nt, x)

    if nt_is(tuple) and hasattr(nt, "_fields"):
        fields = zip(nt._fields, recurse(nt))
        return dict(fields)
    elif nt_is(Mapping):
        return type(nt)(zip(nt.keys(), recurse(nt.values())))
    elif nt_is(Iterable) and not nt_is(str):
        return type(nt)(recurse(nt))
    else:
        return nt


def merge(source, overrides):
    """Return a new dictionary by merging two dictionaries recursively."""

    result = deepcopy(source)

    for key, value in overrides.items():
        if value is not None:
            if isinstance(value, Mapping):
                result[key] = merge(result.get(key, {}), value)
            elif key != "software_list":
                result[key] = deepcopy(overrides[key])
            elif key == "software_list":
                result[key] = []
                for sware in source[key] + overrides[key]:
                    if sware not in result["software_list"]:
                        result[key].append(sware)
    return result


# TO DO: much more validation can be added to all of these classes

# deposition objects for EMDB
# general
class SoftwareType(NamedTuple):
    _name = "software_type"
    name: Optional[str] = None
    version: Optional[str] = None
    processing_details: Optional[str] = None

    def check(self):
        return self


def software_list_is_valid(software_list):
    sl_types = [type(x) == SoftwareType for x in software_list]
    if not all(sl_types):
        return False
    else:
        return True


class OtherType(NamedTuple):
    _name = "other"
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        return self


# ctf correction
CTF_CORRECTION_SPACE_OPTIONS = ["real", "reciprocal"]


class PhaseReversalType(NamedTuple):
    _name = "phase_reversal"
    anisotropic: Optional[bool] = None
    correction_space: Optional[str] = None

    def check(self):
        if self.correction_space not in CTF_CORRECTION_SPACE_OPTIONS + [None]:
            raise ValueError("Invalid CTF correction space")
        return self


class AmplitudeCorrectionType(NamedTuple):
    _name = "amplitude_correction"
    factor: Optional[float] = None
    correction_space: Optional[str] = None

    def check(self):
        if self.correction_space not in CTF_CORRECTION_SPACE_OPTIONS + [None]:
            raise ValueError("Invalid CTF correction space")
        return self


# TO DO: this one has complete validation, need to do this on all others
# can some of teh validation be done more effeciently with the type hints?
CTF_CORRECTION_OPERATION_OPTIONS = ["multiplication", "division"]


class CtfCorrectionType(NamedTuple):
    _name = "ctf_correction"
    phase_reversal: Optional[PhaseReversalType] = None
    amplitude_correction: Optional[AmplitudeCorrectionType] = None
    correction_operation: Optional[str] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        errors = []
        if self.correction_operation not in CTF_CORRECTION_OPERATION_OPTIONS + [None]:
            errors.append("Invalid CTF correction operation")
        if type(self.phase_reversal) not in (type(None), PhaseReversalType):
            errors.append("phase_reversal must be PhaseReversalType or None")
        if type(self.amplitude_correction) not in (type(None), AmplitudeCorrectionType):
            errors.append(
                "amplitude_correction must be AmplitudeCorrectionType or None"
            )
        if not software_list_is_valid(self.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


# particle picking
class ParticleSelectionType(NamedTuple):
    _name = "particle_selection"
    number_selected: Optional[int] = None
    reference_model: Optional[str] = None
    method: Optional[str] = None
    software: Optional[SoftwareType] = None
    details: Optional[str] = None

    def check(self):
        if type(self.software) not in [type(None), SoftwareType]:
            raise ValueError("software must be SoftwareType object or None")
        return self


# starting model
class RandomConicalTiltType(NamedTuple):
    _name = "random_conical_tilt"
    number_images: Optional[int] = None
    tilt_angle: Optional[float] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("software_list must contain SoftwareType objects")
        return self


class OrthogonalTiltType(NamedTuple):
    _name = "orthogonal_tilt"
    software_list: List[SoftwareType] = list()
    number_images: Optional[int] = None
    tilt_angle1: Optional[float] = None
    tilt_angle2: Optional[float] = None
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("software_list must contain SoftwareType objects")
        return self


# TODO: put regex validation of emdb_id
class EmdbType(NamedTuple):
    _name = "emdb_id"
    emdb_id: Optional[str] = None

    def check(self):
        return self


# TODO: put regex validation of both fields
class PdbChainType(NamedTuple):
    _name = "chain_id_list"
    chain_id: Optional[str] = None
    residue_range: Optional[str] = None

    def check(self):
        return self


class PdbModelType(NamedTuple):
    _name = "pdb_model"
    pdb_id: Optional[str] = None
    chain_id_list: List[PdbChainType] = list()

    def check(self):
        if type(self.chain_id_list) not in (list, None):
            raise ValueError("chain_id_list must be a list or None")
        if type(self.chain_id_list) == list:
            if len(self.chain_id_list) > 0:
                types = [type(x) == PdbChainType for x in self.chain_id_list]
                if not all(types):
                    raise ValueError("chain_id_list must contain PdbChainType objects")
            return self


class InSilicoStartupModelType(NamedTuple):
    _name = 'startup_model type_of_model="insilico"'
    startup_model: Optional[str] = None
    details: Optional[str] = None

    def check(self):
        return self


class OtherStartupModelType(NamedTuple):
    _name = 'startup_model type_of_model="other"'
    startup_model: Optional[str] = None
    details: Optional[str] = None

    def check(self):
        return self


class RandomConicalTiltStartupModelType(NamedTuple):
    _name = 'startup_model type_of_model="random_conical_tilt"'
    startup_model: Optional[RandomConicalTiltType] = None
    details: Optional[str] = None

    def check(self):
        if type(self.startup_model) not in (RandomConicalTiltType, type(None)):
            raise ValueError("startup_model must be RandomConicalTiltType or None")
        return self


class EmdbStartupModelType(NamedTuple):
    _name = 'startup_model type_of_model="emdb_id"'
    startup_model: Optional[EmdbType] = None
    details: Optional[str] = None

    def check(self):
        if type(self.startup_model) not in (EmdbType, type(None)):
            raise ValueError("startup_model must be EmdbType or None")
        return self


class PdbModelStartupModelType(NamedTuple):
    _name = 'startup_model type_of_model="pdb_model"'
    startup_model: Optional[PdbModelType] = None
    details: Optional[str] = None

    def check(self):
        if type(self.startup_model) not in (PdbModelType, type(None)):
            raise ValueError("startup_model must be PdbModelType or None")
        return self


# initial angle assignment / final angle assignment
ANGLE_ASSIGNMENT_METHODS = [
    "ANGULAR RECONSTITUTION",
    "COMMON LINE",
    "NOT APPLICABLE",
    "OTHER",
    "PROJECTION MATCHING",
    "RANDOM ASSIGNMENT",
    "MAXIMUM LIKELIHOOD",
]


class ProjectionMatchingProcessingType(NamedTuple):
    _name = "projection_matching_processing"
    number_reference_projections: Optional[int] = None
    merit_function: Optional[str] = None
    angular_sampling: Optional[float] = None

    def check(self):
        return self


class InitialAngleType(NamedTuple):
    _name = "initial_angle_assignment"
    type: Optional[str] = None
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        errors = []
        if self.type not in ANGLE_ASSIGNMENT_METHODS + [None]:
            errors.append(f"invalid initial angle assignment type: {self.type}")
        if type(self.projection_matching_processing) not in (
            type(None),
            ProjectionMatchingProcessingType,
        ):
            errors.append(
                "projection_matching_processing must be ProjectionMatching"
                "Processing object or None"
            )
        if not software_list_is_valid(self.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        else:
            return self

    # need to check against initial angle types


class FinalAngleType(NamedTuple):
    _name = "final_angle_assignment"
    type: Optional[str] = None
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None
    # need to check against initial angle types

    def check(self):
        errors = []
        if self.type not in ANGLE_ASSIGNMENT_METHODS + [None]:
            errors.append(f"invalid initial angle assignment type: {self.type}")
        if type(self.projection_matching_processing) not in (
            type(None),
            ProjectionMatchingProcessingType,
        ):
            errors.append(
                "projection_matching_processing must be ProjectionMatching"
                "Processing object or None"
            )
        if not software_list_is_valid(self.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        else:
            return self


# final MRA
class FinalMultiReferenceAlignmentType(NamedTuple):
    _name = "final_multi_reference_alignment"
    number_reference_projections: Optional[int] = None
    merit_function: Optional[str] = None
    angular_sampling: Optional[float] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


# final 2D classification
class Final2DClassificationType(NamedTuple):
    _name = "final_two_d_classification"
    number_classes: Optional[int] = None
    average_number_members_per_class: Optional[int] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


# final 3D classification
class Final3DClassificationType(NamedTuple):
    _name = "final_three_d_classification"
    number_classes: Optional[int] = None
    average_number_members_per_class: Optional[int] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


class DimensionsType(NamedTuple):
    _name = "dimensions"
    radius: Optional[float] = None
    width: Optional[float] = None
    height: Optional[float] = None
    depth: Optional[float] = None

    def check(self):
        return self


GEOMETRICAL_SHAPES = [
    "SPHERE",
    "SOFT SPHERE",
    "GAUSSIAN",
    "CIRCLE",
    "RECTANGLE",
    "CYLINDER",
    "OTHER",
]


class BackgroundMaskType(NamedTuple):
    _name = "background_masked"
    geometrical_shape: Optional[str] = None
    dimensions: Optional[DimensionsType] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        errors = []
        if not software_list_is_valid(self.software_list):
            errors.append("Software list must contain SoftwareType objects")
        if type(self.dimensions) not in (type(None), DimensionsType):
            errors.append("dimensions must be DimensionsType or None")
        if self.geometrical_shape not in [None] + GEOMETRICAL_SHAPES:
            errors.append("Invalid entry for geometrical shape")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


class SpatialFilteringType(NamedTuple):
    _name = "spatial_filtering"
    low_frequency_cutoff: Optional[float] = None
    high_frequency_cutoff: Optional[float] = None
    filter_function: Optional[str] = None
    software_list: List[SoftwareType] = list()

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError()
            raise ValueError("Software list must contain SoftwareType objects")
        return self


class SharpeningType(NamedTuple):
    _name = "sharpening"
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("software_list can only contain SoftwareType")
        return self


class BfSharpeningType(NamedTuple):
    _name = "b-factorSharpening"
    brestore: Optional[float] = None
    software_list: List[SoftwareType] = list()
    details: Optional[str] = None

    def check(self):
        if not software_list_is_valid(self.software_list):
            raise ValueError("software_list can only contain SoftwareType")
        return self


class ReconstructionFilteringType(NamedTuple):
    _name = "reconstruction_filtering"
    background_masked: Optional[BackgroundMaskType] = None
    spatial_filtering: Optional[SpatialFilteringType] = None
    sharpening: Optional[SharpeningType] = None
    bfactorSharpening: Optional[BfSharpeningType] = None
    other: Optional[OtherType] = None

    def check(self):
        errors = []
        if type(self.background_masked) not in (BackgroundMaskType, type(None)):
            errors.append("background_masked must be BackgroundMaskedType or None")
        if type(self.sharpening) not in (SharpeningType, type(None)):
            errors.append("sharpening must be SharpeningType or None")
        if type(self.bfactorSharpening) not in (BfSharpeningType, type(None)):
            errors.append("bfactor_sharpening must be SharpeningType or None")
        if type(self.other) not in (OtherType, type(None)):
            errors.append("other must be OtherType or None")
        if type(self.spatial_filtering) not in (SpatialFilteringType, type(None)):
            errors.append("spatial_filtering must be SpatialFilteringType or None")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


class HelicalParamsType(NamedTuple):
    _name = "helical_parameters"
    delta_z: Optional[float] = None
    delta_phi: Optional[float] = None
    axial_symmetry: Optional[str] = None

    def check(self):
        return self


class SymmetryType(NamedTuple):
    _name = "applied_symmetry"
    space_group: Optional[int] = None
    point_group: Optional[str] = None
    helical_parameters: Optional[HelicalParamsType] = None

    def check(self):
        if type(self.helical_parameters) not in (type(None), HelicalParamsType):
            raise ValueError("helical_parameters must be None or HelicalParamsType")
        return self


RECONSTRUCTION_ALGORITHM_TYPES = [
    "ALGEBRAIC (ARTS)",
    "BACK PROJECTION",
    "EXACT BACK PROJECTION",
    "FOURIER SPACE",
    "SIMULTANEOUS ITERATIVE (SIRT]",
]

RESOLUTION_METHODS = [
    "DIFFRACTION PATTERN/LAYERLINES",
    "FSC 0.143 CUT-OFF",
    "FSC 0.33 CUT-OFF",
    "FSC 0.5 CUT-OFF",
    "FSC 1/2 BIT CUT-OFF",
    "FSC 3 SIGMA CUT-OFF",
    "OTHER",
]


class FinalReconstructionType(NamedTuple):
    _name = "final_reconstruction"
    number_of_classes_used: Optional[int] = None
    applied_symmetry: Optional[SymmetryType] = None
    algorithm: Optional[str] = None
    resolution: Optional[float] = None
    resolution_method: Optional[str] = None
    reconstruction_filtering: Optional[ReconstructionFilteringType] = None

    def check(self):
        errors = []
        if type(self.applied_symmetry) not in (SymmetryType, type(None)):
            errors.append("applied_symmetry must be SymmetryType or None")
        if self.resolution_method not in [None] + RESOLUTION_METHODS:
            errors.append("Invalid resolution_method")
        if self.algorithm not in [None] + RECONSTRUCTION_ALGORITHM_TYPES:
            errors.append("Invalid algorithm")
        if type(self.reconstruction_filtering) not in (
            type(None),
            ReconstructionFilteringType,
        ):
            errors.append(
                "reconstruction_filtering must be ReconstructionFilteringType or None"
            )
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


# Deposition objects for EMPIAR

EMPIAR_DATATYPES = {
    0: "('T2', '')",
    1: "('T4', '')",
    2: "(T7, '')",
    3: "('OT', 'transform : complex 16-bit integers')",
    4: "('OT', 'transform : complex 32-bit integers')",
    6: "('T3', '')",
    12: "('OT', '16-bit float')",
}

EMPIAR_TIFF_DATATYPES = {
    "int8": "('T2', '')",
    "int16": "('T4', '')",
    "int32": "(T7, '')",
    "uint16": "('T3', '')",
    "float32": "('OT', '16-bit float')",
}


def empiar_type_is_valid(empobj):
    def empiar_check(empobj, attribute, number):
        """Check that an attribute in epiar format is valid

        Checks values in the form ("T<n>, "") for things like
        EMPIARs, header_format value

        Args:
            empobj (namedtuple): The Empiar DepositionObject
            attribute (str): Name of the attribute to check
            number (int); Max value for the 'T' value, OT (OytherType) will
                always be added as well
        Returns:
            str: The error or None if there is no error
        """
        tvals = ["OT"] + [f"T{x}" for x in range(1, number + 1)]
        value = empobj.getatt(attribute)
        if len(value) != 2:
            return "{attribute} is not in the correct format Tuple[str, str]"
        if value[0] not in tvals:
            return (
                f"{value}[0] is an invalid value for {attribute}. "
                "Value must be in {tvals}"
            )

    errors = []
    for ch in ["header_format", "data_format", "voxel_type"]:
        errors.append(empiar_type_is_valid(empobj, ch, 7))
    errors.append(empiar_check(empobj, "category", 10))

    if not all([x is None for x in errors]):
        raise ValueError()


# name attribute is included as regular attribute for these ones as well as _name
class EmpiarMovieSetType(NamedTuple):
    _name = ("Multiframe micrograph movies",)
    name: str = ("Multiframe micrograph movies",)
    directory: Optional[str] = None
    category: Optional[str] = None
    header_format: Optional[Tuple[str, str]] = None
    data_format: Optional[Tuple[str, str]] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[Tuple[str, str]] = None
    pixel_width: Optional[int] = None
    pixel_height: Optional[int] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None

    def check(self):
        empiar_type_is_valid(self)
        return self


class EmpiarCorrectedMicsType(NamedTuple):
    _name = ("Corrected micrographs",)
    name: str = "Corrected micrographs"
    directory: Optional[str] = None
    category: Optional[Tuple[str, str]] = None
    header_format: Optional[Tuple[str, str]] = None
    data_format: Optional[Tuple[str, str]] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[Tuple[str, str]] = None
    pixel_width: Optional[int] = None
    pixel_height: Optional[int] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None

    def check(self):
        empiar_type_is_valid(self)
        return self


class EmpiarParticlesType(NamedTuple):
    _name = ("Particle images",)
    name: str = "Particle images"
    directory: Optional[str] = None
    category: Optional[Tuple[str, str]] = None
    header_format: Optional[Tuple[str, str]] = None
    data_format: Optional[Tuple[str, str]] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[Tuple[str, str]] = None
    pixel_width: Optional[int] = None
    pixel_height: Optional[int] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None
    picked_particles_file_pattern: Optional[str] = None


class EmpiarRefinedParticlesType(NamedTuple):
    _name = ("Per-particle motion corrected particle images",)
    name: str = "Per-particle motion corrected particle images"
    directory: Optional[str] = None
    category: Optional[Tuple[str, str]] = None
    header_format: Optional[Tuple[str, str]] = None
    data_format: Optional[Tuple[str, str]] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[Tuple[str, str]] = None
    pixel_width: Optional[int] = None
    pixel_height: Optional[int] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None
    picked_particles_file_pattern: Optional[str] = None

    def check(self):
        empiar_type_is_valid(self)
        return self


# method:  work backwards from terminal job
#    for each parent that should contribute run that parents prepare
#     _onedep_info() function (to be written)
#    then prepare json from all using all of the NamedTuples
#    later do one for helical?

# get the terminal job
class OneDepDeposition(object):
    """A class for preparing depositions using the OneDep system

    Will also be used to generate other depositions in the future (EMPIR)
    replacing the cirrent ad hoc EMPIR deposition preparation function

    Attributes:
        terminal_proc (:class:`~pipeliner.data_structure.process`): The process
            that was chosen to make the deposition from
        upstream (List[:class:`~pipeliner.data_structure.process`]): All of the
            processes upstream of the terminal process, in order
        final_depo (dict): A dict that contains all of the info for the final
            deposition
        procs_depobjs (Dict{str:NamedTuple}): Contains all of processes in the upstream
            list and the deposition objects they have produced.

    """

    def __init__(self, pipeline, terminal_job):
        """create a OneDepDeposition object

        Args:
            pipeline (:class:~pipeliner.project_graph.ProjectGraph): The pipeline
                for the current project
            terminal_job (str): The name of the job to create the deposition from

        """
        self.terminal_proc = pipeline.find_process(terminal_job)
        self.upstream = get_procs_list(terminal_job, pipeline)
        self.final_depo = {}
        self.procs_depobjs = {}
        # get all the deposition objects for the processes in the chain
        print("Preparing deposition info")
        for proc in self.upstream:
            job = active_job_from_proc(proc)
            self.procs_depobjs[proc.name] = job.prepare_onedep_data()

    def gather_depdata(self, dep_type, reverse=False):
        """Gather deposition data for a specific toplevel depo object type

        Prioritize later jobs over earlier jobs unless reverse is `True`
        Only takes data from the first job of each type IE: if job001 = ctffind,
        job002 = ctffind, job003 = ctfrefine.

        reverse is false for `CtfCorrectionType` so job003 will form the basis of the
        depo dict and will be updated with info from job002 but job001 will be ignored

        if reverse were true job001 would form the basis of the depo dict, job002 would
        be ignored and it would be updated with data from job003 because it is a
        different type

        reverse is used for initial parameters like particle picking and inital angle
        assignment where early jobs are important

        non-reverse is used for final parameters where the later jobs are the important
        ones

        Args:
            dep_type (class): The type of deposition object
            reverse (bool): Switch the priority, use earlier jobs of that type
                over later ones

        Returns:
            dict: The merged deposition objects as a nested set of dicts
        """
        done_proctypes, dep_objs = [], []
        procs = self.upstream

        # sort the procs depending on precedence set by reverse
        procs.sort(
            key=lambda x: decompose_pipeline_filename(x.name)[1], reverse=reverse
        )

        # get all the depo objects in order, only use the first/last one from any
        # given proc type

        for proc in procs:
            for depobj in self.procs_depobjs[proc.name]:
                if type(depobj) == dep_type and proc.type not in done_proctypes:
                    dep_objs.append(depobj)
                    done_proctypes.append(proc.type.split(".")[1])

        # quit of none of the correct dep objs found
        if len(dep_objs) == 0:
            return

        # convert dep objects to nested dicts
        dds = [expand_nt(x) for x in dep_objs]

        # use the 1st (lowest priority) as the base dict
        depdict = dds[0]

        # update dep dict with increasing priority deposition objects
        for depobj in dds[1:]:
            depdict = merge(depdict, depobj)

        # update the main dict with the deposition dict
        self.final_depo[dep_objs[0]._name] = depdict

    def prepare_pdbemdb_deposition_dict(self, deptype):
        """Prepare the dictionary containing all of the deposition information

        Args:
            deptype (str): The type of deposition to prepare

        Returns:
            dict: The information for the deposition in dict for ready to create a
                json
        """
        # make sure the terminal process has the right sort of deposition objects
        # TO DO: make a similar check for pdb depositions when they are prepared
        termproc_objs = [type(x) for x in self.procs_depobjs[self.terminal_proc.name]]
        if FinalReconstructionType not in termproc_objs and deptype == "emdb":
            raise ValueError(
                f"The job {self.terminal_job} is not of the appropriate type to "
                f"prepare a {deptype} deposition, find a job that returns a final "
                "map such as Refine3D, PostProcess, or LocalRes"
            )

        # {depo type: {depo obj: shoud_it_be_reversed?}}
        depotypes = {
            "emdb": {
                CtfCorrectionType: False,
                ParticleSelectionType: True,
                OtherStartupModelType: False,
                EmdbStartupModelType: False,
                PdbModelStartupModelType: False,
                RandomConicalTiltStartupModelType: False,
                InSilicoStartupModelType: False,
                InitialAngleType: True,
                FinalAngleType: False,
                FinalMultiReferenceAlignmentType: False,
                Final2DClassificationType: False,
                Final3DClassificationType: False,
                FinalReconstructionType: False,
            },
            "pdb": {
                CtfCorrectionType: False,
                ParticleSelectionType: True,
                EmdbStartupModelType: False,
                PdbModelStartupModelType: False,
                RandomConicalTiltStartupModelType: False,
                InSilicoStartupModelType: False,
                InitialAngleType: True,
                FinalAngleType: False,
                FinalMultiReferenceAlignmentType: False,
                Final2DClassificationType: False,
                Final3DClassificationType: False,
                FinalReconstructionType: False,
                # plus more as needed
            },
        }

        # go through the different fields and update the main dict for each
        for dep_field in depotypes[deptype]:
            self.gather_depdata(dep_field, depotypes[deptype][dep_field])

        return self.final_depo

    def prepare_empiar_deposition_dict(
        self, do_rawmic=False, do_cormic=False, do_parts=False, do_corparts=False
    ):
        """Prepare the dictionary containing all of the deposition information

        Args:
            do_rawmic (bool): Include raw micrographs in the deposition
            do_cormic (bool): Include motioncorrected micrographs in the deposition
            do_parts (bool): Include particles in the deposition
            do_cormic (bool): Include per-particle motioncorrected
                particles in the deposition

        Returns:
            dict: The information for the deposition in dict for ready to create a
                json
        """
        if all([not x for x in (do_rawmic, do_cormic, do_parts, do_corparts)]):
            raise ValueError(
                "Nothing to do! Select one or more of the deposition fields"
            )

        # which type of imagesets to make:
        imgsets = {
            EmpiarMovieSetType: do_rawmic,
            EmpiarCorrectedMicsType: do_cormic,
            EmpiarParticlesType: do_parts,
            EmpiarRefinedParticlesType: do_corparts,
        }

        # go through the different imageset types and update the main dict for each
        for dep_field in imgsets:
            if imgsets[dep_field]:
                self.gather_depdata(dep_field)

        # add the extra info to the final depo dict and make a list of
        # files for the archive
        files_patterns = set()
        depdict = {"experiment_type": 3, "imagesets": []}
        for depinfo in self.final_depo:
            depdict["imagesets"].append(self.final_depo[depinfo])

            # get file search strings for archiving
            mfp = self.final_depo[depinfo].get("micrographs_file_pattern")
            ppfp = self.final_depo[depinfo].get("picked_particles_file_pattern")
            for i in (mfp, ppfp):
                if i is not None:
                    files_patterns.add(i)

        # make the archive dir
        depdir = f"{date_time_tag(True)}_EMPIAR_DEPOSITION"
        os.makedirs(depdir)
        print("Preparing deposition files:")
        for fs in files_patterns:
            files = glob(fs)
            fdir = os.path.join(depdir, os.path.dirname(fs))
            if not os.path.isdir(fdir):
                os.makedirs(fdir)
            for f in files:
                linkname = os.path.join(depdir, f)
                os.symlink(f, linkname)

        return (depdict, depdir)
