#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""These utilites are used by the pipeliner for basic tasks such as nice looking
on-screen display, checking file names, and getting directory and file names"""

from datetime import datetime
from pathlib import Path
from typing import List, Tuple, Optional


def touch(filename: str):
    """Create an empty file

    Args:
        filename (str): The name for the file to create
    """
    open(filename, "w").close()


def get_pipeliner_root() -> Path:
    """Get the directory of the main pipeliner module

    Returns:
        path: The path of the pipeliner
    """
    return Path(__file__).parent


def truncate_number(number: float, maxlength: int) -> str:
    """Return a number with no more than x decimal places but no trailing 0s

    This is used to format numbers in the exact same way that RELION does it.
    IE: with maxlength 3; 1.2000 = 1.2, 1.0 = 1, 1.23 = 1.23. RELION commands are happy
    to accept numbers with any number of decimal places or trailing 0s. This
    function is just to maintain continuity between RELION and pipeliner
    commands

    Args:
        number (float): The number to be truncated
        maxlength (int): The maximum number of decimal places
    """

    rounded = round(number, maxlength)
    if int(rounded) == rounded:
        return str(int(rounded))
    return str(rounded)


def decompose_pipeline_filename(fn_in: str) -> Tuple[str, Optional[int], str]:
    """Breaks a job name into usable pieces

    Returns everything before the job number, the job number as an int
    and everything after the job number setup for up to 20 dirs deep.
    The 20 directory limit is from the relion code but no really necessary anymore

    Args:
        fn_in (str): The job or file name to be broken down in the format:
            <jobtype>/jobxxx/<filename>

    Returns:
        tuple: The decomposed file name: (:class:`str`, :class:`int`, :class:`str`)
            `[0]` Everything before 'job' in the file name

            `[1]` The job number

            `[2]` Everything after the job number

    Raises:
        ValueError: If the input file name is more than 20 directories deep
    """

    fn_split = fn_in.split("/")
    # > 20 dirs doesn't really need to be an error any more with the way it
    # is being done now.
    if len(fn_split) > 20:
        raise ValueError(
            "decomposePipelineFileName: BUG or found more than 20"
            " directories deep structure for pipeline filename: " + fn_in
        )
    # return everything before job number, job number, and everything after
    i = 0
    for chunk in fn_split:
        if len(chunk) == 6 and chunk[0:3] == "job":
            fn_jobnr = 0
            try:
                fn_jobnr = int(chunk[3:])
            except ValueError:
                fn_jobnr = 0
            if fn_jobnr:
                fn_pre = "/".join(fn_split[:i])
                fn_post = "/".join(fn_split[i + 1 :])
                return (fn_pre, fn_jobnr, fn_post)
        i += 1
    # return just the file name if it wasn't a pipeline file
    return ("", None, fn_in)


def date_time_tag(compact: bool = False) -> str:
    """Get a current date and time tag

    If can return a compact version or one that is easier to read

    Args:
        compact (bool): Should the returned tag be in the comapct form

    Returns:
        str: The datetime tag

        compact format is: `YYYYMMDDHHMMSS`

        verbose form is: `YYYY-MM-DD HH:MM:SS`
    """
    now = datetime.now()
    if compact:
        date_time = now.strftime("%Y%m%d%H%M%S")
    else:
        date_time = now.strftime("%Y-%m-%d %H:%M:%S")
    return date_time


def check_for_illegal_symbols(
    check_string: str, string_name: str = "input", exclude: str = ""
):
    """Check a text string doesn't have any of the disallowed symbols.

    Illegal symbols are !*?()^/\\#<>&%{}$."' and @.

    Args:
        check_string (str): The string to be checked
        string_name (str): The name of the string being checked; for more informative
            error messages
        exclude (str): Any symbols that are normally in the illegal symbols list but
            should be allowed.
    Raises:
        ValueError: If illegal symbols are present in the test string
    """
    badsym = ""
    for symbol in [
        "!",
        "*",
        "?",
        "(",
        ")",
        "^",
        "/",
        "\\",
        "|",
        "#",
        "<",
        ">",
        "&",
        "%",
        "{",
        "}",
        "$",
        ",",
        '"',
        "'",
        "@",
        ":",
    ]:
        if symbol in check_string and symbol not in exclude:
            badsym += symbol
    if len(badsym) > 0:
        raise ValueError(
            f'ERROR: Symbol(s) {"".join(badsym)} in {string_name}.'
            f'\n{string_name} cannot contain the following symbols: ! * ? / " \\ |'
            " # < > & % ^ ' , ' or $"
        )


def clean_jobname(jobname: str) -> str:
    """Makes sure job names are in the correct format

    Job names must have a trailing slash, cannot begin with a slash,
    and have no illegal characters

    Args:
        jobname (str): The jon name to be checked

    Returns:
        str: The job name, with corrections in necessary

    """
    # fix missing trailing slashes
    if jobname[-1] != "/":
        jobname = jobname + "/"

    # fix leading slashes
    if jobname[0] == "/":
        jobname = jobname[1:]

    # fix double slashes
    if "//" in jobname:
        jobname = "".join(
            jobname[i]
            for i in range(len(jobname))
            if i == 0 or not (jobname[i - 1] == jobname[i] and jobname[i] == "/")
        )

    # return error if illeagl characters present
    check_for_illegal_symbols(jobname, "job name", "/")

    return jobname


def quotate_command_list(commands: List[list]) -> List[list]:
    """Adds quotation marks to commands arguments that need them

    If a command is to be run in terminal some args need to be quotated.  Quotation
    marks are not needed if the command list is run with subprocess.run but they are if
    the command is run as a string in a qsub script or in the terminal

    Any arg that contains a space or the set of characters !*?()^#<>&%{}$@ will be
    quotated

    Args:
        commands (list): The commands are a list of lists. Each item in the main list
            is a single command, which itself is a list of the individual arguments

    Returns:
        list: A correctly quotated command list

        The list is in the same list of lists format
    """
    quote_chars = [
        "!",
        "*",
        "?",
        "(",
        ")",
        "^",
        "#",
        "<",
        ">",
        "&",
        "%",
        "{",
        "}",
        "$",
        "@",
        ":",
    ]
    quoted_commands: List[List[str]] = []
    for n, com in enumerate(commands):
        quoted_commands.append([])
        for arg in com:
            if any(item in arg for item in quote_chars):
                quoted_commands[n].append(f'"{arg}"')
            else:
                quoted_commands[n].append(arg)
    return quoted_commands


def print_nice_columns(
    list_in: List[str], err_msg: str = "ERROR: No items in input list"
):
    """Takes a list of items and makes three columns for nicer on-screen display

    Args:
        list_in (str): The list to display in columns
        err_msg (str): The message to display if the list is empty
    """

    if len(list_in) == 0:
        print(f"\n{err_msg}")
        return

    list_in.sort()
    if len(list_in) <= 10:
        for i in list_in:
            print(i)
        print("\n")
        return

    third = int(len(list_in) / 3)
    chunk1 = list_in[0:third]
    c1 = max([len(x) for x in chunk1]) + 2
    chunk2 = list_in[third : 2 * third]
    c2 = max([len(x) for x in chunk2]) + 2
    chunk3 = list_in[2 * third :]
    c3 = max([len(x) for x in chunk3]) + 2
    comb_data = zip(chunk1, chunk2, chunk3)

    spacer = ["", ""]
    if len(chunk1) < len(chunk3):
        chunk1 += spacer
        chunk2 += spacer
    elif len(chunk3) < len(chunk1):
        chunk3 += spacer

    for row in comb_data:
        print(f"{row[0].ljust(c1)} {row[1].ljust(c2)} {row[2].ljust(c3)}")
    print("\n")


def make_pretty_header(title: str):
    """Make nice looking headers for on-screen display

    Args:
        title (str): The text to put in the header

    Returns:
        str: A nice looking header

        ::

         -=-=-=-=-=-=-=-=-=
         It looks like this
         -=-=-=-=-=-=-=-=-=

    """
    length = max([len(x) for x in title.split("\n")])
    border = "-=" * int(length / 2)
    if length % 2:
        border += "-"
    return f"{border}\n{title}\n{border}"


def wrap_text(text_string: str):
    """Produces <= 55 character wide wrapped text for on-screen display

    Args:
        text_string (str): The text to be displayed
    """
    n = 0
    printed = 0
    text_split = text_string.split(" ")
    while printed <= len(text_split) - 1:
        line_string = ""
        length = 0
        while length < 55 and printed <= len(text_split) - 1:
            line_string += text_split[n] + " "
            length += len(text_split[n])
            if "\n" in text_split[n]:
                length = 0
            printed += 1
            n += 1
        print(line_string)


def fix_newlines(file_path: str):
    """Replace LF+CR new lines in files with LF, because RELION doesn't like them

    Args:
        file_path (str): The file to fix
    """
    # replacement strings
    WINDOWS_LINE_ENDING = b"\r\n"
    UNIX_LINE_ENDING = b"\n"

    with open(file_path, "rb") as open_file:
        content = open_file.read()

    content = content.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING)

    with open(file_path, "wb") as open_file:
        open_file.write(content)


def find_common_string(input_strings: List[str]) -> str:
    """Find the common part of a list of strings starting from the beginning

    Args:
        input_strings (list): List of strings to compare

    Returns:
        str: The common portion of the strings

    Raises:
        ValueError: If input_list is shorter than 2
    """

    common = ""
    for n, character in enumerate(list(input_strings[0])):
        for the_str in input_strings:
            if list(the_str)[n] != character:
                return common
        common += character
    return common


def smart_strip_quotes(in_string: str) -> str:
    """Strip the quotes from a string in a intellegant manner

    Remove leading and ending ' and " but don't remove them internally

    Args:
        in_string (str): The input string

    Returns:
        str: the string with leading and ending quotes removed
    """
    if len(in_string) == 0:
        return ""
    if in_string[0] in ["'", '"']:
        in_string = in_string[1:]
    if in_string[-1] in ["'", '"']:
        in_string = in_string[:-1]
    return in_string
