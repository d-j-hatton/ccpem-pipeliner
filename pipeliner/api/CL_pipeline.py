#!/usr/bin/env python

#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import argparse
from glob import glob
from typing import Optional, List
import sys

from pipeliner.api.manage_project import (
    PipelinerProject,
    convert_pipeline,
    look_for_project,
    get_commands_and_nodes,
)
from pipeliner.api.interactive import interactive_job_create
from pipeliner.utils import (
    check_for_illegal_symbols,
    quotate_command_list,
    print_nice_columns,
    date_time_tag,
    decompose_pipeline_filename,
    make_pretty_header,
    wrap_text,
    get_pipeliner_root,
)
from pipeliner.api.api_utils import (
    write_default_jobstar,
    write_default_runjob,
    validate_starfile,
    get_job_info,
    get_available_jobs,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.data_structure import (
    JOBSTATUS_ABORT,
    JOBSTATUS_SCHED,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_FAIL,
    JOBSTATUS_RUN,
)


def get_arguments():

    parser = argparse.ArgumentParser(description="CCPEM Pipeliner command line utility")
    parser._optionals.title = "Arguments"

    parser.add_argument(
        "--new_project",
        help=(
            "Initialize a new project in this directory."
            " Project name is optional, if none is specified the project will"
            " be called 'default' (recommended)"
        ),
        nargs="?",
        default=None,
        const="default",
        metavar="project name",
    )

    parser.add_argument(
        "--available_jobs",
        help=(
            "Show a list of all available jobs, optionally add a search string"
            " to limit the search, Leave blank to show all available job types"
        ),
        nargs="?",
        default=None,
        const="*ALL*",
        metavar="search string",
    )

    runjob = parser.add_argument_group("Running jobs")
    runjob.add_argument(
        "--run_job",
        help="Create a job using a run.job or job.star file to" " get the parameters",
        nargs="?",
        default=None,
        metavar="run.job or job.star file",
    )

    runjob.add_argument(
        "--overwrite",
        help="Use with --run_job to overwrite a current job rather than making"
        " a new job.  Jobs can only be overwritten with the same job type",
        nargs="?",
        default=None,
        metavar="job name",
    )

    runjob.add_argument(
        "--continue_job",
        help="Continue a job that has been previously run."
        "Edit the job's continue_job.star file to modify parameters "
        "for the continuation",
        nargs="?",
        default=None,
        metavar="job name",
    )

    runjob.add_argument(
        "--create_interactive_job",
        help="Create a job.star file for any job type with an interactive dialog. "
        "Useful for situations where the GUI cannot open",
        action="store_true",
    )

    runjob.add_argument(
        "--print_command",
        help="Read a job.star or run.job file and print the command it"
        " would produce",
        nargs="?",
        default=None,
        metavar="run.job or job.star file",
    )

    runjob.add_argument(
        "--schedule_job",
        help="Add a job to list of scheduled jobs using a run.job or job.star "
        "file to get the parameters.  If followed by a job.star file it will create a"
        " new job if followed by a job name it will schedule a continuation of that "
        "job",
        nargs="?",
        default=None,
        metavar="run.job, job.star file or job name",
    )

    schedjob = parser.add_argument_group("Executing Schedules")

    schedjob.add_argument(
        "--run_schedule",
        help="Create a schedule choosing from the currently scheduled jobs and "
        "run it",
        action="store_true",
    )

    schedjob.add_argument(
        "--name",
        help="(required) Enter a name for the new schedule",
        nargs="?",
        default=None,
    )

    schedjob.add_argument(
        "--jobs",
        help="(required) Enter the jobs that will be run.  Make sure to list the"
        " jobs in the order which they should be run",
        nargs="*",
        default=None,
        metavar="job name",
    )

    schedjob.add_argument(
        "--min_between",
        help="(optional) Wait at least this many minutes between jobs",
        nargs="?",
        default=0,
        metavar="n",
    )
    schedjob.add_argument(
        "--nr_repeats",
        help="(optional) Repeat the schedule this many times",
        nargs="?",
        default=1,
        metavar="1",
    )

    schedjob.add_argument(
        "--wait_sec_after",
        help=(
            "(optional) Wait this many seconds after finishing before starting"
            " the next job"
        ),
        nargs="?",
        default=2,
        metavar="2",
    )

    schedjob.add_argument(
        "--wait_min_before",
        help="(optional) Wait this many minutes before starting the schedule",
        nargs="?",
        default=0,
        metavar="0",
    )

    schedjob.add_argument(
        "--stop_schedule",
        help="(required) Enter a name for the schedule to be stopped",
        nargs="?",
        default=None,
    )

    delete = parser.add_argument_group("Deleting jobs")
    delete.add_argument(
        "--delete_job",
        help="Remove job(s) and put in the trash, deletes the job and all of its child"
        " processes",
        nargs="?",
        metavar="job name",
        default=None,
    )

    undelete = parser.add_argument_group("Undeleting jobs")
    undelete.add_argument(
        "--undelete_job",
        help="Restore a deleted job and any of its deleted parent processes from the "
        "trash",
        nargs="?",
        metavar="job name",
        default=None,
    )

    modify = parser.add_argument_group("Modifying jobs")
    modify.add_argument(
        "--set_alias",
        help="Set the alias of a job",
        nargs=2,
        metavar=("[job name]", "[new alias]"),
        default=None,
    )

    modify.add_argument(
        "--clear_alias",
        help="Clear the alias of a job",
        nargs=1,
        metavar=("[job name]"),
        default=None,
    )

    modify.add_argument(
        "--set_status",
        help="Set the status of a job; choose from 'finished, failed, or aborted",
        nargs=2,
        metavar=("[job name]", "{finished, failed, aborted}"),
        default=None,
    )

    cleanup = parser.add_argument_group("Cleaning Up Job(s)")
    cleanup.add_argument(
        "--cleanup",
        help=(
            "Delete intermediate files from these job(s) to save disk space"
            "; enter ALL to clean up all jobs"
        ),
        nargs="*",
        metavar="job name",
        default=None,
    )
    cleanup.add_argument(
        "--harsh",
        help="Add this argument to --cleanup to delete even more files",
        action="store_true",
    )

    utils = parser.add_argument_group("Utilities")
    utils.add_argument(
        "--validate_starfile",
        help=(
            "Check a star file and make sure it is written in the correct format.  If"
            " errors are found will attempt to fix them"
        ),
        nargs="?",
        metavar="star file",
        default=None,
    )

    utils.add_argument(
        "--convert_pipeline_file",
        help=("Convert a pipeline file from Relion 3.1 to the CCPEM pipeliner format"),
        nargs="?",
        metavar="_pipeline.star file",
        default=None,
    )

    utils.add_argument(
        "--default_jobstar",
        help=(
            "Make a job.star file with the default values for a specific job type "
            "for use with the ccpem-pipeliner"
        ),
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--relionstyle",
        help=(
            "OPTIONAL: Add this argument to --default_jobstar to write job.star files "
            "which are compatible with RELION 4.0.  RELIONstyle job.star files are "
            "fully compatible with the pipeliner, pipeliner job.star files may have"
            "differences that cause bugs in RELION"
        ),
        action="store_true",
    )
    utils.add_argument(
        "--default_runjob",
        help=(
            "Make a _run.job file with the default values for a specific job type."
            " These files can also be used to run jobs and a more human readable"
        ),
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--job_info",
        help=("Get info about a specific job type, including any reference(s)"),
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--empty_trash",
        help="Delete the files in the trash.  THIS CANNOT BE UNDONE!",
        action="store_true",
    )

    analysis = parser.add_argument_group("Project Analysis")

    analysis.add_argument(
        "--job_runtime",
        help=("Measure how long the steps in a job took to complete"),
        nargs="?",
        metavar="job name",
        default=None,
    )

    analysis.add_argument(
        "--metadata_report",
        help=(
            "Prepares a report in .json format for the terminal job and all"
            "of its parent jobs"
        ),
        nargs="?",
        const="nojob",
        metavar="terminal job",
        default=None,
    )
    analysis.add_argument(
        "--draw_flowchart",
        help=(
            "Draw a flowcharts for the pipeline.  If used alone; draws the entire"
            " pipeline, if followed by a job name; draws upstream and downstream"
            " flowcharts for that job.  Saves the output as jobxxx_upstream.png"
            " unless the --interactive argument is also included"
        ),
        nargs="?",
        const="full",
        default=None,
        metavar="job name",
    )
    analysis.add_argument(
        "--upstream",
        help=(
            "[optional] Add this argument to --draw_flowchart to only draw the "
            " upstream flowchart for the specified job"
        ),
        action="store_true",
    )
    analysis.add_argument(
        "--downstream",
        help=(
            "[optional] Add this argument to --draw_flowchart to only draw "
            "the downstream flowchart for the specified job"
        ),
        action="store_true",
    )

    analysis.add_argument(
        "--interactive",
        help=(
            "[optional] Add this argument to --draw_flowchart to show "
            "interactive flowcharts"
        ),
        action="store_true",
    )

    archiving = parser.add_argument_group("Project Archiving")
    archiving.add_argument(
        "--full_archive",
        help=(
            "Create a full archive for a project.  This will contain the entire"
            " job dirs for the stated terminal job and all of its parents"
        ),
        nargs="?",
        default=None,
        metavar="terminal job name",
    )

    archiving.add_argument(
        "--simple_archive",
        help=(
            "Create a simple archive for a project.  This will contain just the"
            " directory structure and parameter files for the stated terminal job and"
            "all of it's parents along with a script to automatically re-run the "
            "project through the terminal job"
        ),
        nargs="?",
        default=None,
        metavar="terminal job name",
    )
    # get the args
    return parser


def main(in_args: Optional[List[str]] = None):
    """Utility for running pipeliner functions from the command-line

    Args:
        in_args (list): The command-line arguments, including "CL_pipeline" at
            position 0
    """
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    # initialize new project function
    def f_new_project():
        """CL_pipeline sub function to create a new project"""
        # look for illegal names...
        check_for_illegal_symbols(args.new_project, "pipeline name")

        if args.new_project == "mini":
            raise ValueError(
                "ERROR: The pipline cannot be named 'mini' this name is used"
                " internally by the pipeliner. "
            )
        # initialize the project
        proj_dict = look_for_project()
        if proj_dict is not None:
            raise ValueError(
                "ERROR: Could not initialize a new project in this directory. A"
                f" pipeline called {proj_dict['pipeline file']} already exists"
            )
        print("Initializing a new project...")
        proj = PipelinerProject(pipeline_name=args.new_project)
        print("Successfully created {}_pipeline.star".format(proj.pipeline_name))
        return True

    # functions that don't require an initialized project

    # validate star file function
    def f_validate_starfile():
        """CL_pipeline sub function to validate a starfile"""
        fn_in = args.validate_starfile
        return validate_starfile(fn_in)

    # print command function
    def f_print_command():
        """CL_pipeline sub function to print command from a job.star file"""
        try:
            initialze_the_project()
        except ValueError:
            pass
        job_file = args.print_command
        commands, innodes, outnodes, warns, progerrs = get_commands_and_nodes(job_file)

        print(
            "\n{}".format(
                make_pretty_header(f"{len(commands[1])} commands to be run: ")
            )
        )
        quotated_commands = quotate_command_list(commands[1])

        for com in quotated_commands:
            print("{}\n".format(" ".join(com)))
        if len(innodes) > 0:
            print(make_pretty_header("{} expected input nodes:".format(len(innodes))))
            for node in innodes:
                print("{}     {}".format(node[0], node[1]))
        if len(outnodes) > 0:
            print(make_pretty_header("{} expected output nodes:".format(len(outnodes))))
            for node in outnodes:
                print("{}     {}".format(node[0], node[1]))
        print("\n")
        if "/job000/" in "".join(com):
            print(
                "WARNING: No pipeliner project present, so command is generated "
                "as `job000`"
            )
        if warns:
            print("WARNING: The following parameter validation warnings were raised:")
            for w in warns:
                print(f"{9 * ' '}{w.raised_by[0].label}: {w.message}")
        if progerrs:
            print("WARNING: Command will not be able to run for the following reasons:")
            for per in progerrs:
                print(f"{9 * ' '}{per}")

    # get job info function
    def f_job_info():
        """CL_pipeline sub function to print the info from a job"""
        info = get_job_info(args.job_info)
        if info is not None:
            print("\n")
            print(
                make_pretty_header(
                    f"CCPEM-pipeliner {args.job_info} job vers {info.version}"
                )
            )
            print(f"{info.short_desc}\n")
            print(f"Job author: {info.job_author}")
            print(f"Program(s) used: {', '.join(info.programs)}")
            print(f"Version info: {info.software_vers}")
            wrap_text(info.long_desc)

            print(f"\nOnline documentation:\n  {info.documentation}\n")

            if len(info.references) > 0:
                print("Reference(s):")
                for ref in info.references:
                    # make sure the references are in the correct format
                    if not isinstance(ref, Ref):
                        print(
                            "  Reference formating error\n  Reference format was "
                            f"{type(ref)}; It should be in the pipeliner Ref format\n"
                            f"  {ref}"
                        )
                        break
                    for line in str(ref).split("\n"):
                        print(f"  {line}")
                    print("\n")
            if info.program_errors:
                print("WARNING: This job is currently not available to run because:")
                for per in info.program_errors:
                    print(f"{' ' * 9}{per}")

    # convert pipeline functions
    def f_convert_pipeline_file():
        """CL_pipeline sub function to convet pipeline from relion 3 to pipeliner"""
        pipeline_file = args.convert_pipeline_file.replace("_pipeline.star", "")
        return convert_pipeline(pipeline_file)

    # default starfile function
    def f_default_jobstar():
        """CL_pipeline sub function to generate a default job.star file"""
        return write_default_jobstar(args.default_jobstar, relionstyle=args.relionstyle)

    # default runjob function
    def f_default_runjob():
        """CL_pipeline sub function to generate a default run.job file"""
        return write_default_runjob(args.default_runjob)

    # available jobs function
    def f_available_jobs():
        """CL_pipeline sub function to print all available jobs"""
        print(make_pretty_header(f"Available jobtypes: {args.available_jobs.lower()}"))
        # get all jobs
        jobs = get_available_jobs(args.available_jobs)
        if False in [x[1] for x in jobs]:
            print(
                "Some jobtypes are not available because the software to run them is "
                "not installed\nThese jobs are indicated in (parentheses)\n"
            )
        show_jobs = [x[0] if x[1] else f"({x[0]})" for x in jobs]
        print_nice_columns(
            show_jobs, f"No job types found with '{args.available_jobs.lower()}'"
        )

    # all other functions require an initialized project
    def initialze_the_project(pipe_name="default"):
        proj_dict = look_for_project()
        if proj_dict is not None:
            proj = PipelinerProject()
            return proj
        else:
            raise ValueError("ERROR: No pipeliner project in this directory")

    # cleanup functions
    def f_cleanup():
        """CL_pipeline sub function to run cleanup on jobs"""
        proj = initialze_the_project()
        if args.cleanup not in [None, ["ALL"]]:
            procs = proj.parse_proclist(args.cleanup)
            proj.run_cleanup(procs, args.harsh)
        elif args.cleanup == ["ALL"]:
            proj.cleanup_all(args.harsh)
            return True
        else:
            print("ERROR: No jobs were specified to clean up")
            return False

    # delete job function
    def f_delete_job():
        """CL_pipeline sub function to delete a job"""
        proj = initialze_the_project()
        proc = proj.parse_procname(args.delete_job)
        proj.delete_job(proc)
        return True

    # undelete job function
    def f_undelete_job():
        """CL_pipeline sub function to undelete a job"""
        proj = initialze_the_project()
        proc = proj.parse_procname(args.undelete_job, search_trash=True)
        proj.undelete_job(proc)
        return True

    # set status function
    def f_set_status():
        """CL_pipeline sub function to set the status of a job"""
        proj = initialze_the_project()
        job, new_status = args.set_status
        status_cases = {
            "aborted": JOBSTATUS_ABORT,
            "scheduled": JOBSTATUS_SCHED,
            "success": JOBSTATUS_SUCCESS,
            "failed": JOBSTATUS_FAIL,
            "running": JOBSTATUS_RUN,
        }

        found = 0
        for stat in status_cases:
            if new_status.lower() in stat:
                new_status = status_cases[stat]
                found += 1
        if found != 1:
            raise ValueError(
                f"ERROR: The job status must be in {list(status_cases)}"
                f" The status entered was `{new_status}`"
            )

        job = proj.parse_procname(job)
        proj.update_job_status(job, new_status)
        return True

    # set alias function
    def f_set_alias():
        """CL_pipeline sub function to set the alias of a job"""
        proj = initialze_the_project()
        job, new_alias = args.set_alias
        job = proj.parse_procname(job)
        proj.set_alias(job, new_alias)
        return True

    # clear alias function
    def f_clear_alias():
        """CL_pipeline sub function to clear the alias of a job"""
        proj = initialze_the_project()
        job = args.clear_alias[0]
        job = proj.parse_procname(job)
        proj.set_alias(job, None)
        return True

    # run job function
    def f_run_job():
        """CL_pipeline sub function to run a new job"""
        proj = initialze_the_project()
        overwrite = None
        target = None
        if args.overwrite is not None:
            target = proj.parse_procname(args.overwrite, search_trash=False)
            overwrite = target
        job_name = proj.run_job(args.run_job, overwrite)
        print(make_pretty_header(f"Ran job as: {job_name}"))
        return True

    # continue a job
    def f_continue_job():
        """CL_pipeline sub function to continue a job"""
        proj = initialze_the_project()
        job_name = proj.parse_procname(args.continue_job)
        proj.continue_job(job_name)
        print("Continuing " + job_name)
        return True

    # schedule job function
    def f_schedule_job():
        """CL_pipeline sub function to schedule a job"""
        proj = initialze_the_project()
        # if a starfile is given it is a new job
        if ".star" in args.schedule_job:
            job_name = proj.schedule_job(args.schedule_job)
        else:
            job_name = proj.parse_procname(args.schedule_job, search_trash=False)
            proj.schedule_continue_job(job_name)

        print(make_pretty_header(f"Scheduled job as: {job_name}"))
        return True

    # create and run schedule functions
    def f_run_schedule():
        """CL_pipeline sub function to run scheduled jobs"""
        proj = initialze_the_project()
        for req_arg in [
            "--name",
            "--jobs",
        ]:
            if req_arg not in in_args:
                sys.exit(f"\nERROR: Required argument {req_arg} is missing")
        job_ids = proj.parse_proclist(args.jobs)
        proj.run_schedule(
            args.name,
            job_ids,
            int(args.nr_repeats),
            int(args.min_between),
            int(args.wait_min_before),
            int(args.wait_sec_after),
        )
        return True

    # stop schedule function
    def f_stop_schedule():
        """CL_pipeline sub function to stop a running schedule"""
        proj = initialze_the_project()
        # allow for entry of the full file name too
        sched_prefix = f"RUNNING_PIPELINER_{proj.pipeline_name}_"
        sched_name = args.stop_schedule.replace(sched_prefix, "")
        proj.stop_schedule(sched_name)

    # create interactive job functions
    def f_create_interactive_job():
        """CL_pipeline sub function to create a job file interactively"""
        interactive_job_create()
        return True

    # empty the trash
    def f_empty_trash():
        """CL_pipeline sub function to empty the trash"""
        proj = initialze_the_project()
        trash_files = glob("Trash/*/*/*")
        if len(trash_files) > 0:
            print(f"Do want to delete {len(trash_files)} files in the trash?")
            doit = input("THIS CANNOT BE UNDONE! (Y/N): ")
            if doit in ["Y", "y", "Yes", "YES"]:
                proj.empty_trash()
                return True
            else:
                print("Exiting without emptying trash")
                return False
        else:
            print("WARNING: No files found in Trash/")
            return False

    # draw the flowcharts
    def f_draw_flowchart():
        """CL_pipeline sub function to draw project flowcharts"""
        proj = initialze_the_project()
        job = args.draw_flowchart
        do_upstream = args.upstream
        do_downstream = args.downstream
        show = args.interactive
        save = False if args.interactive else True

        if job == "full":
            if do_upstream or do_downstream:
                print(
                    "ERROR: Asked to draw upstream or downstream flowcharts but "
                    "no job was specified"
                )
                return [False, False, False]
            else:
                proj.draw_flowcharts(do_full=True, save=save, show=show)
                return [False, False, True]

        else:
            job = proj.parse_procname(args.draw_flowchart)
            if not do_upstream and not do_downstream:
                do_upstream, do_downstream = True, True
            proj.draw_flowcharts(
                job,
                do_upstream,
                do_downstream,
                save=save,
                show=show,
            )
            return [do_upstream, do_downstream, False]

    # simple archive
    def f_simple_archive():
        """CL_pipeline sub function to create a simple archive"""
        proj = initialze_the_project()
        print(
            f"\n{make_pretty_header('Preparing simple archive')}\n"
            "This archive will allow re-running of all jobs leading up to"
            f" and including {args.simple_archive}.\nFiles not produced"
            "by these jobs (IE:raw data, queue submission script templates, or "
            "external executables) will\nnot be included in the archive.\nResults"
            " from re-running the archive may not be identical to the original due"
            " to stochasitc steps in processing\nor differences in the systems they"
            " are run on.\n\nTo produce an archive with exact fidelity to the "
            "original use the '--full_archive' option"
        )
        jobname = proj.parse_procname(args.simple_archive)
        message = proj.create_archive(jobname, full=False, tar=True)
        print(message)

    # full archive
    def f_full_archive():
        """CL_pipeline sub function to create a full archive"""
        proj = initialze_the_project()
        jobname = proj.parse_procname(args.full_archive)
        message = proj.create_archive(jobname, full=True, tar=True)
        print(message)

    # job runtime
    def f_job_runtime():
        """CL pipeliner sub function to get time to run jobs"""
        proj = initialze_the_project()
        jobname = proj.parse_procname(args.job_runtime)
        time_data = proj.get_job_runtime(jobname)
        print(make_pretty_header(f"{args.job_runtime} running time analysis"))
        print(f"Job had {len(time_data[3])} commands - job status is: {time_data[4]}")
        print(f"Command{25 * ' '}real{11* ' '}user{11* ' '}sys")
        for com in time_data[3]:
            pipepath = str(get_pipeliner_root()) + "/"
            trim_com = com.replace(pipepath, "")
            if len(trim_com) > 25:
                short_com = trim_com[5:30] + "..."
            else:
                short_com = trim_com[5:] + (17 - len(com)) * " "
            if time_data[3][com][0]:
                print(
                    f"{short_com}    {time_data[3][com][0]:<15f}"
                    f"{time_data[3][com][1]:<15f}{time_data[3][com][2]:<15f}"
                )
        print(
            f"{22 * ' '}TOTAL:    {time_data[0]:<15f}{time_data[1]:<15f}"
            f"{time_data[2]:<15f}"
        )

    # metadata report
    def f_metadata_report():
        """CL_pipeline sub function to generate a project metadata report"""
        proj = initialze_the_project()
        if args.metadata_report == "nojob":
            print(
                "\nERROR: Specify a terminal job for the report"
                "\nUSAGE: CL_pipeline --metadata_report <job_name>"
            )
            return
        jobname = proj.parse_procname(args.metadata_report)
        print(f"\n preparing metadata report for {jobname} and parent jobs...")
        jobno = decompose_pipeline_filename(jobname)[1]
        metadata_outfile = f"{date_time_tag(compact=True)}_job{jobno:03d}.json"
        proj.get_network_metadata(jobname, metadata_outfile)
        print(f"Wrote metadata report to {metadata_outfile}")

    # make sure only one function is run at a time
    # args that need values
    funct2run = None
    argcount = 0
    for arg in (
        (args.run_job, f_run_job),
        (args.delete_job, f_delete_job),
        (args.undelete_job, f_undelete_job),
        (args.set_status, f_set_status),
        (args.set_alias, f_set_alias),
        (args.clear_alias, f_clear_alias),
        (args.cleanup, f_cleanup),
        (args.schedule_job, f_schedule_job),
        (args.print_command, f_print_command),
        (args.validate_starfile, f_validate_starfile),
        (args.draw_flowchart, f_draw_flowchart),
        (args.continue_job, f_continue_job),
        (args.stop_schedule, f_stop_schedule),
        (args.new_project, f_new_project),
        (args.convert_pipeline_file, f_convert_pipeline_file),
        (args.default_jobstar, f_default_jobstar),
        (args.default_runjob, f_default_runjob),
        (args.job_info, f_job_info),
        (args.available_jobs, f_available_jobs),
        (args.simple_archive, f_simple_archive),
        (args.full_archive, f_full_archive),
        (args.metadata_report, f_metadata_report),
        (args.job_runtime, f_job_runtime),
    ):
        if arg[0] is not None:
            argcount += 1
            funct2run = arg[1]

    # true/false args
    for arg in (
        (args.create_interactive_job, f_create_interactive_job),
        (args.run_schedule, f_run_schedule),
        (args.empty_trash, f_empty_trash),
    ):
        if arg[0]:
            argcount += 1
            funct2run = arg[1]

    if argcount > 1:
        raise ValueError("ERROR: please only select one type of function at a time")
    elif argcount == 0:
        parser.print_help()

    # run the appropriate function
    if funct2run is not None:
        funct2run()


if __name__ == "__main__":
    main()
