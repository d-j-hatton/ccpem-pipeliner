import platform
import os
import json


_settings_json = "ccpem-pipeliner-settings.json"


def get_pipeliner_settings_path():
    """Get ccpem-pipeline settings from platform specific standard location"""
    settings_directory = "ccpem-pipeliner"
    user_platform = platform.system()
    # Linux
    if user_platform == "Linux":
        path = os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.config"))
    # Mac
    elif user_platform == "Darwin":
        path = os.path.expanduser("~/Library/Application Support/")

    # Check write permissions, ask user to set directory if not ok
    if not os.access(path, os.W_OK):
        # If path if found / doesn't have necessary write permissions throw error
        # and quit
        message = (
            "CCP-EM needs to create a settings directory and "
            "the default does not have write permissions:\n"
            "\n{0}".format(path)
        )
        print(message)
    # Create settings directory
    else:
        path = os.path.join(path, settings_directory)
        if not os.path.exists(path):
            os.makedirs(path)
        return path


def create_settings_json():
    """Create settings json"""
    path = os.path.join(get_pipeliner_settings_path(), _settings_json)
    settings = {"path_to_source_files": []}
    with open(path, "w", encoding="utf-8") as f:
        json.dump(settings, f, ensure_ascii=False, indent=4)


def load_settings_json():
    """Load settings json and return settings dictionary"""
    path = os.path.join(get_pipeliner_settings_path(), _settings_json)
    # Check settings json exists, create if not
    if not os.path.exists(path):
        create_settings_json()
    with open(path, "r") as j:
        settings = json.loads(j.read())
    return settings
