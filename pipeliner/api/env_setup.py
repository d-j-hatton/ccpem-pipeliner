#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import glob
from subprocess import Popen, PIPE
import platform
from pipeliner.api import user_settings


def source_env(script):
    """
    Source bash script and update python enviroment
    """
    # https://gist.github.com/mammadori/3891614
    pipe = Popen(". %s; env" % script, stdout=PIPE, shell=True)
    data = pipe.communicate()[0]
    data = data.decode("utf-8")
    env_dict = {}
    dat_list = data.splitlines()
    # very specific fix for when there is a `}` newline
    # may need to be generalized
    for n, line in enumerate(dat_list):
        if line == "}":
            continue
        try:
            nd = dat_list[n + 1]
            if nd == "}":
                line = line + "  }"
        except IndexError:
            pass
        key, val = line.split("=")[0], "=".join(line.split("=")[1:])
        env_dict[key] = val
    os.environ.update(env_dict)


def set_scripts_to_source():
    # Auto load CCP4 if present
    settings = user_settings.load_settings_json()
    scripts = settings["path_to_source_files"]
    missing_ccp4 = True
    for script in scripts:
        if "ccp4" in script:
            missing_ccp4 = False
    if missing_ccp4 and platform.system() == "Darwin":
        # Find most recently install CCP4 by finding most recently modified directory
        files = list(filter(lambda f: "ccp4" in f, glob.glob("/Applications/" + "*")))
        files.sort(key=lambda x: os.path.getmtime(x))
        ccp4_directory = files[-1]
        ccp4_setup = os.path.join(ccp4_directory, "bin/ccp4.setup-sh")
        if os.path.exists(ccp4_setup):
            scripts.append(ccp4_setup)
    return scripts


def set_python_environment(scripts=None):
    """Find source script locations from settings and update python environment"""
    if scripts is None:
        scripts = set_scripts_to_source()

    for script in scripts:
        source_env(script=script)
