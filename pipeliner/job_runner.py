#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


from datetime import datetime, timedelta
from glob import glob
import os
import shutil
import subprocess
import time
import json
from typing import List, Optional, Tuple

from pipeliner.project_graph import ProjectGraph, update_jobinfo_file
from pipeliner import job_factory
from pipeliner.utils import date_time_tag, decompose_pipeline_filename, touch
from pipeliner.api.api_utils import job_success
from pipeliner.data_structure import (
    Process,
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    ABORT_TRIGGER,
    RELION_ABORT_FILE,
    RELION_SUCCESS_FILE,
    RELION_FAIL_FILE,
    JOBSTATUS_RUN,
    JOBSTATUS_SCHED,
    JOBSTATUS_FAIL,
    STARFILE_READS_OFF,
)
from pipeliner.display_tools import ResultsDisplayPending
from pipeliner.pipeliner_job import PipelinerJob

OUTPUT_NODE_MICS = "MicrographsData.star.relion"
OUTPUT_NODE_PARTS = "ParticlesData.star.relion"


class JobRunner(object):
    """The JobRunner object handles running jobs (who would have thought?)

    Attributes:
        graph (:class:`~pipeliner.project_graph.ProjectGraph`): The pipeline for the
            project
    """

    def __init__(self, project_name: str = "default"):
        """Create a JobRunner object

        Args:
            project_name (str): The name of the project
        """
        self.graph = ProjectGraph(project_name)
        if os.path.isfile(project_name + "_pipeline.star"):
            self.graph.read()

    def get_commandline_job(
        self,
        thisjob: PipelinerJob,
        current_proc: Optional[Process],
        is_main_continue: bool,
        is_scheduled: bool,
        do_makedir: bool,
        overwrite: bool = False,
        subsequent_scheduled: bool = False,
    ) -> list:
        """Assemble all of the commands necessary to run a job

        Args:
            thisjob (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job that is being run
            current_proc (:class:`~pipeliner.data_structure.Process`):
                The existing process object for the current job, if the job to be run is
                a continuation or overwrite job, otherwise None
            is_main_continue (bool): Is the job to be run a continuation?
            is_scheduled (bool): Has the job to be run already been scheduled?
            do_makedir (bool): Should a directory for the job be made if necessary?
            overwrite (bool): Is the job to be run overwriting a previous job?
            subsequent_scheduled (bool): Is the job to be run a subsequent iteration
                of a job that has already been run in a currently running schedule?

        Returns:
            list: [[[Actual, command], [to, be, run]], [[the, Job, commands]]] If the
                job is being submitted to a queue `[0]` will be the qsub command and
                `[1]` will the be the actual job commands.  For local jobs they will
                be identical

            This is a list of lists, Each sublist is holds the arguments for a single
            command.

        Raises:
            ValueError: If an attempt is made to overwrite or contiune a job that
                doesn't exist
            RuntimeError: If no commands are generated
        """

        original_jobtype, target_job = None, None
        if (overwrite or subsequent_scheduled) and current_proc is None:
            raise ValueError(
                "ERROR: The job runner is trying to overwrite/continue a job"
                "that does not exist."
            )
        # if it was overwrite or a repeat of a scheduled job
        # get info on the job being overwritten/continues

        if current_proc is not None:
            original_jobtype, target_job = decompose_pipeline_filename(
                current_proc.name
            )[:2]

        if not overwrite and not subsequent_scheduled:
            target_job = self.graph.job_counter

        # set the name of the job - new jobs have no name
        if (
            (is_main_continue or is_scheduled or overwrite)
            and current_proc
            and current_proc in self.graph.process_list
        ):
            name = current_proc.name
        else:
            name = ""

        # get the command for the job
        thisjob.is_continue = is_main_continue

        if target_job is None:
            raise ValueError(f"No target job number was found for {current_proc}")

        outputname = thisjob.initialise_pipeline(name, thisjob.OUT_DIR, target_job)
        thisjob.output_name = outputname
        command_list = thisjob.get_commands()
        final_command_list = thisjob.prepare_final_command(
            outputname, command_list, do_makedir
        )

        if overwrite:
            # check the job is the same type as the one it replaces
            thisjob_type = decompose_pipeline_filename(thisjob.output_name)[0]
            if thisjob_type != original_jobtype:
                raise ValueError(
                    "ERROR: A job can only be overwritten by the same job type.\n"
                    f"Attempted to overwrite a {original_jobtype} job with a"
                    f" {thisjob_type} job"
                )
        # make sure a command was returned
        if len(final_command_list) == 0:
            raise RuntimeError("\n\nERROR: nothing to do...")

        return final_command_list

    def prepare_job_to_run(
        self,
        job: PipelinerJob,
        current_proc: Optional[Process],
        is_main_continue: bool,
        is_scheduled: bool,
        overwrite_current: bool = False,
        subsequent_scheduled: bool = False,
    ) -> Tuple[List[list], bool]:
        """Do the setup for running a job

        This includes:
        - Removing the original files if overwriting
        - Removing any control files

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job that is being run
            current_proc (:class:`~pipeliner.data_structure.Process`):
                The existing process object for the current job, if the job to be run is
                a continuation or overwrite job, otherwise None
            is_main_continue (bool): Is the job to be run a continuation?
            is_scheduled (bool): Has the job to be run already been scheduled?
            do_makedir (bool): Should a directory for the job be made if necessary?
            overwrite (bool): Is the job to be run overwriting a previous job?
            subsequent_scheduled (bool): Is the job to be run a subsequent iteration
                of a job that has already been run in a currently running schedule?

        Returns:
            tuple: The commands (lists of lists) and if the job is overwriting
            (``bool``)

        """
        # validate the joboptions
        jo_valdat = job.validate_joboptions()
        jo_errs = list(filter(lambda x: x.type == "error", jo_valdat))
        if len(jo_errs) > 0:
            disp_errs = [f"{str(x.raised_by)[1:-1]}:{x.message}" for x in jo_errs]
            jo_format = ", ".join(disp_errs)
            raise ValueError(
                "Job cannot run/schedule job because of the following errors in the "
                f"joboptions:{jo_format}"
            )

        do_makedir = True

        if overwrite_current:
            is_main_continue = False
            do_makedir = False

        # if overwriting remove the original files
        if overwrite_current and current_proc:

            # check for children...
            children = self.graph.find_immediate_child_processes(current_proc)

            if len(children) > 0:
                # make get a place to archive the current run
                kiddies = [x.name for x in children]
                timestamp = date_time_tag(compact=True)
                archive_dir = (
                    "." + timestamp + "." + current_proc.name[:-1].replace("/", ".")
                )

                # copy the current directory contents into the archive
                shutil.copytree(current_proc.name, archive_dir)

                # make notes on the children
                overwrote_children = False
                for child in children:
                    if os.path.isdir(child.name):
                        childfile = os.path.join(
                            child.name, "PARENT_OVERWRITTEN_" + timestamp
                        )
                        with open(childfile, "w") as make_note:
                            make_note.write(archive_dir)
                        overwrote_children = True
                if overwrote_children:
                    print(
                        "\nWARNING: The process designated for overwriting has"
                        f" {len(children)} child processes: {','.join(kiddies)}\n"
                        "The original run of this process has been archived"
                        f" as {archive_dir}\nPARENT_OVERWRITTEN_{timestamp} markers "
                        "have been written to all child processes\n"
                    )

        if current_proc:
            # remove any control files:
            control_files = [
                ABORT_TRIGGER,
                ABORT_FILE,
                SUCCESS_FILE,
                FAIL_FILE,
                RELION_ABORT_FILE,
                RELION_SUCCESS_FILE,
                RELION_FAIL_FILE,
            ]
            # remove overwrite notices
            parent_overwrite_files = glob(job.output_name + "PARENT_OVERWRITTEN_*")
            control_files += parent_overwrite_files

            for cf in control_files:
                cf_name = os.path.join(current_proc.name, cf)
                if os.path.isfile(cf_name):
                    os.remove(cf_name)

        final_command_list = self.get_commandline_job(
            job,
            current_proc,
            is_main_continue,
            is_scheduled,
            do_makedir,
            overwrite_current,
            subsequent_scheduled,
        )

        allow_overwrite = is_main_continue or is_scheduled or overwrite_current

        return (final_command_list, allow_overwrite)

    def add_job_to_pipeline(
        self, job: PipelinerJob, status: str, allow_overwrite: bool
    ) -> Process:
        """Add the job to the pipeline and create its temp nodes files

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job that is being run
            status (str): The status of the new job either `Running` or `Scheduled`
            allow_overwrite (bool): Is the job to be run overwriting a previous job?
        Returns:
            :class:`~pipeliner.data_structure.Process`: The
            job that is being run

        """
        # lock the pipeline before editing
        self.graph.create_lock()
        current_proc = self.graph.add_job(job, status, allow_overwrite)
        self.graph.touch_temp_node_files(current_proc)
        self.graph.write()  # TODO: reduce coupling, graph should write itself
        return current_proc

    def schedule_job(
        self,
        job: PipelinerJob,
        current_proc: Optional[Process],
        is_main_continue: bool,
        overwrite_current: bool = False,
        subsequent_scheduled: bool = False,
        comment: Optional[str] = None,
    ) -> Process:
        """Schedule a job, add to the pipeline with scheduled status

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job that is being run
            current_proc (:class:`~pipeliner.data_structure.Process`):
                The existing process object for the current job, if the job to be run is
                a continuation or overwrite job, otherwise None
            is_main_continue (bool): Is the job to be run a continuation?
            is_scheduled (bool): Has the job to be run already been scheduled?
            overwrite_current (bool): Is the job to be run overwriting a previous job?
            subsequent_scheduled (bool): Is the job to be run a subsequent iteration
                of a job that has already been run in a currently running schedule?

        Returns:
            :class:`~pipeliner.data_structure.Process`: The
            job that is being run
        """
        # check that the required executables are available to run the job
        execs = job.jobinfo.programs
        missing = []
        for prog in execs:
            if shutil.which(prog) is None:
                missing.append(prog)
        if len(missing) > 0:
            raise RuntimeError(
                f"The executable(s) {' '.join(missing)} are necessary"
                " for running this job and were not found."
            )

        # validate the joboptions, don't check for files because they may have
        # not been created yet
        joboption_valdata = job.validate_joboptions()
        jo_errs = list(filter(lambda x: x.type == "error", joboption_valdata))
        if len(jo_errs) > 0:
            disp_errs = [f"{str(x.raised_by)[1:-1]}:{x.message}" for x in jo_errs]
            jo_format = ", \n".join(disp_errs)
            raise ValueError(
                "Job cannot run because of the following errors in the "
                f"joboptions:\n {jo_format}"
            )

        allow_overwrite = self.prepare_job_to_run(
            job,
            current_proc,
            is_main_continue,
            False,
            overwrite_current,
            subsequent_scheduled,
        )[1]
        current_proc = self.add_job_to_pipeline(job, JOBSTATUS_SCHED, allow_overwrite)
        action = "Scheduled"
        if is_main_continue:
            action += " continuation"
        if overwrite_current:
            action += " overwrite last run"
        update_jobinfo_file(
            current_proc,
            action,
            comment,
            None,
        )

        return current_proc

    def wait_for_queued_job_completion(self, outdir: str):
        """Wait for a job that has been sent to the queue to finish

        Args:
            outdir (str): The job's output directory
        """
        print("Waiting for queued job to finish...")
        while not os.path.isfile(os.path.join(outdir, SUCCESS_FILE)):
            failed = os.path.isfile(os.path.join(outdir, FAIL_FILE))
            aborted = os.path.isfile(os.path.join(outdir, ABORT_FILE))
            if failed:
                print(f"WARNING: queued job {outdir} failed")
                return
            if aborted:
                print(f"WARNING: queued job {outdir} was aborted")
                return
            time.sleep(10)

    def run_job(
        self,
        job: PipelinerJob,
        current_proc: Optional[Process],
        is_main_continue: bool,
        is_scheduled: bool,
        overwrite_current: bool = False,
        subsequent_scheduled: bool = False,
        wait_for_queued: bool = True,
        comment: Optional[str] = None,
    ) -> Process:
        """Run a job, add to the pipeline with running status and execute its commmands

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job that is being run
            current_proc (:class:`~pipeliner.data_structure.Process`):
                The existing process object for the current job, if the job to be run is
                a continuation or overwrite job, otherwise None
            is_main_continue (bool): Is the job to be run a continuation?
            is_scheduled (bool): Has the job to be run already been scheduled?
            overwrite_current (bool): Is the job to be run overwriting a previous job?
            subsequent_scheduled (bool): Is the job to be run a subsequent iteration
                of a job that has already been run in a currently running schedule?
            wait_for_queued (bool): If this job is sent to the queue should the
                pipeliner wait for it to finish before continuing on?
            comment (str): Comments to add to the job's jobinfo file
        Returns:
            :class:`~pipeliner.data_structure.Process`: The
            job that is being run
        """
        # check that the required executables are available to run the job
        execs = job.jobinfo.programs
        missing = []
        for prog in execs:
            if shutil.which(prog) is None:
                missing.append(prog)
        if len(missing) > 0:
            raise RuntimeError(
                f"The executable(s) {' '.join(missing)} are necessary"
                f"for running job {job.PROCESS_NAME} and were not found."
            )

        # validate the input files - make sure they exist
        jo_fe = job.validate_input_files()
        if len(jo_fe) > 0:
            file_errors = [f"{str(x.raised_by)[1:-1]}:{x.message}" for x in jo_fe]
            jo_format = ", \n".join(file_errors)
            raise ValueError(
                "Job cannot run because of the following errors in the "
                f"joboptions: \n{jo_format}"
            )

        final_command_list, allow_overwrite = self.prepare_job_to_run(
            job,
            current_proc,
            is_main_continue,
            is_scheduled,
            overwrite_current,
            subsequent_scheduled,
        )
        current_proc = self.add_job_to_pipeline(job, JOBSTATUS_RUN, allow_overwrite)

        # For continuation of relion_refine jobs, remove the
        # original output nodes from the list
        if is_main_continue and job.del_nodes_on_continue:
            self.graph.delete_temp_node_files(current_proc)
            if current_proc:
                for node in current_proc.output_nodes:
                    self.graph.node_list.remove(node)

        # set the job type - write the jobstar and runjob files
        job.type = job.output_name.split("/")[0]
        job.write_runjob(job.output_name)
        job.write_jobstar(job.output_name)
        job.write_jobstar(".gui_" + job.PROCESS_NAME.replace(".", "_"))

        # write the continuation file - for continuing the job later
        continue_name = os.path.join(job.output_name, "continue_")
        job.write_jobstar(continue_name, is_continue=True)

        # back up pipeline
        fn_pipe = self.graph.get_pipeline_filename()
        if os.path.isfile(fn_pipe):
            shutil.copy(fn_pipe, job.output_name)

        # run the job commands
        notefile_path = os.path.join(job.output_name, "note.txt")
        for n, com in enumerate(final_command_list[0]):
            print(
                "Executing {} ({}/{}): ".format(
                    job.output_name, n + 1, len(final_command_list[0])
                ),
                " ".join(com),
            )

            # TO DO: This file is written to keep consistent with Relion, but is not
            # used for anything - it could be removed
            with open(notefile_path, "a") as notefile:
                notefile.write(" ".join(com))

            # create the output and error files
            err_file = open(os.path.join(job.output_name, "run.err"), "a")
            out_file = open(os.path.join(job.output_name, "run.out"), "a")

            subprocess.run(
                com, stdout=out_file, stderr=err_file, cwd=job.subprocess_cwd
            )
            err_file.close()
            out_file.close()

            # if the job was submitted to queue wait for it to finish before moving on
            queue_jo = job.joboptions.get("do_queue", False)
            is_queued = queue_jo.get_boolean() if queue_jo else False
            if is_queued and wait_for_queued:
                self.wait_for_queued_job_completion(job.output_name)

        # perform the jobs's post run actions
        # mark the job failed if post_run actions not finished successfully
        try:
            job.post_run_actions()
            # re-add the process in case new nodes were added
            current_proc = self.add_job_to_pipeline(job, JOBSTATUS_RUN, True)

        except Exception as e:
            success_marker = os.path.join(current_proc.name, SUCCESS_FILE)
            if os.path.isfile(success_marker):
                os.remove(success_marker)
            touch(os.path.join(job.output_name, FAIL_FILE))
            current_proc = self.add_job_to_pipeline(job, JOBSTATUS_FAIL, True)

            print(
                f"WARNING: post_run_actions for {job.output_name} raised an error:\n"
                f"{str(e)}"
            )

        action = "Run"
        if is_scheduled:
            action += " scheduled"
        if is_main_continue:
            action += " continuation"
        if overwrite_current:
            action += " overwrite last run"

        update_jobinfo_file(
            current_proc,
            action,
            comment,
            final_command_list,
        )

        if not os.path.isfile(STARFILE_READS_OFF):
            # collect the metadata from this run and make the display objects
            # if not waiting for the job to finish make a placeholder
            if is_queued and not wait_for_queued:
                # placeholder metadat file for queued jobs that pipeliner doesn't wait
                # to finish this file will be updated by project_graph.check_completion
                md_file = (
                    f"{job.output_name}.WAITING_FOR_{date_time_tag(compact=True)}"
                    "_job_metadata.json"
                )
                # if not wait for queued just ignore making the display object
                # it will get created the first time it is viewed
            else:
                md_file = (
                    f"{job.output_name}{date_time_tag(compact=True)}_job_metadata.json"
                )

                # write the display objects for the GUI
                try:
                    display_objs = job.create_results_display()
                except Exception as e:
                    display_objs = [ResultsDisplayPending(reason=str(e))]

                if len(display_objs) > 0:
                    n = 1
                    for dob in display_objs:
                        dob.write_displayobj_file(job.output_name, n)
                        n += 1

            # write the metadata file (or placeholder)
            try:
                metadata_dict = job.gather_metadata()
                with open(md_file, "w") as metadata_out:
                    metadata_out.write(json.dumps(metadata_dict))
            except Exception as e:
                md_error = {
                    "No metadata collected": f"Error running job.gather_metadata: {e}"
                }
                with open(md_file, "w") as metadata_out:
                    metadata_out.write(json.dumps(md_error))
        else:
            # if metadata and results generation are disabled, write some explanations
            why = (
                "This is usually done because of memory issues caused by reading "
                "large starfiles.  To re-enable this feature delete the file "
                f"{STARFILE_READS_OFF}"
            )
            md_file = (
                f"{job.output_name}{date_time_tag(compact=True)}_job_metadata.json"
            )
            md_disabled = {
                "No metadata collected": "This project has metadata collection"
                f" disabled. {why}"
            }
            with open(md_file, "w") as mdf:
                mdf.write(json.dumps(md_disabled))

            results_file = os.path.join(
                job.output_name, ".results_display_text_001.json"
            )
            results_disabled = {
                "title": "Result display disabled",
                "display_data": "Results display has been disabled for this project. "
                f"{why}",
                "associated_data": [],
            }
            with open(results_file, "w") as rf:
                rf.write(json.dumps(results_disabled))

        # finally update the status of all jobs
        self.graph.check_process_completion()

        return current_proc

    def write_to_sched_log(self, message: str, logfile: str):
        """For real time updating of the schedule log

        Args:
            message (str): The message to display and write to the log file
            sl_name (str): Name of the log file
        """

        with open(logfile, "a+") as schedlog:
            schedlog.write(message)

    def schedule_fail(self, message: str, sl_name: str, sched_lock: str):
        """Run when a schedule fails

        Write to the log and then delete the schedule lock file

        Args:
            message (str): The message to display and write to the log file
            sl_name (str): Name of the log file
            sched_lock (str): Name of the lock file

        Returns:
            str: An error message determined by why the schedule failed
        """
        self.write_to_sched_log("\n+ " + date_time_tag() + "\n", sl_name)
        self.write_to_sched_log(message, sl_name)
        os.remove(sched_lock)
        print(message)

    def run_scheduled_jobs(
        self,
        fn_sched: str,
        job_ids: Optional[List[str]] = None,
        nr_repeat: int = 1,
        minutes_wait: int = 0,
        minutes_wait_before: int = 0,
        seconds_wait_after: int = 0,
    ):

        """Run the jobs in a schedule

        Args:
            fn_sched (str): The name to be assigned to the schedule
            job_ids (list): A list of :class:`str` job names
            nr_repeat (int): Number of times to repeat the entire schedule
            minutes_wait (int): Minimum time to wait between jobs in minutes.  If this
                has been passed whilst the job is running the next job will start
                immediately
            minutes_wait_before (int): Wait this amount of time before initially
                starting to run the schedule
            seconds_wait_after (int): Wait this many seconds before starting each job
                this wait always occurs, even if the minimum time between jobs has
                already be surpassed

        Raises:
            ValueError: If a schedule lock file exists with the selected schedule name,
                suggesting another schedule with the same name is already running
            ValueError: (through :meth:`~pipeliner.job_runner.JobRunner.schedule_fail`)
                If the job directory for a scheduled job could not be found
            ValueError: (through :meth:`~pipeliner.job_runner.JobRunner.schedule_fail`)
                If a job.star file is not found in one of the directories of a job to
                be run
            ValueError: (through :meth:`~pipeliner.job_runner.JobRunner.schedule_fail`)
                If an input node for a job cannot be found
            ValueError: (through :meth:`~pipeliner.job_runner.JobRunner.schedule_fail`)
                : If a job in the schedule
                fails
        """

        # make sure there are jobs to run
        if not job_ids or len(job_ids) == 0:
            raise ValueError("\nERROR: run_scheduled_jobs: Nothing to do...")

        # make the schedule lock file
        sched_lock = "RUNNING_PIPELINER_{}_{}".format(self.graph.name, fn_sched)

        if os.path.isfile(sched_lock):
            raise ValueError(
                f"ERROR: a file called {sched_lock} already exists. \n This "
                "implies another set of scheduled jobs with this name is already "
                "running. \n Cancelling job execution..."
            )

        # touch the schedule lockfile
        open(sched_lock, "w").close()

        # write the PID to schedule control file
        proc_pid = os.getpid()
        with open(sched_lock, "a") as runfile:
            runfile.write("RELION_SCHEDULE: {}\n".format(proc_pid))

        # prepare the logfile
        sl_name = "pipeline_{}.log".format(fn_sched)
        self.write_to_sched_log("\n" + "+" * 35, sl_name)
        self.write_to_sched_log(
            "\nStarting a new scheduler execution called schedule1\n"
            "The scheduled jobs are:",
            sl_name,
        )

        for jobname in job_ids:
            self.write_to_sched_log("\n- " + jobname, sl_name)

        if nr_repeat > 1:
            self.write_to_sched_log(
                f"\nWill execute the scheduled jobs {nr_repeat} times\nWill wait"
                f" until at  least {minutes_wait} minute(s) have passed between each "
                "repeat",
                sl_name,
            )
        self.write_to_sched_log(
            f"\nRUNNING_PIPELINER_{fn_sched} will be used to control the "
            "schedule\n" + "+" * 35,
            sl_name,
        )

        if minutes_wait_before > 0:
            time.sleep(minutes_wait_before * 60)

        for repeat in range(int(nr_repeat)):
            repeat_start = datetime.now()
            self.write_to_sched_log("\n+ " + date_time_tag(), sl_name)
            self.write_to_sched_log(
                f"\n-- Starting repeat {repeat + 1}/{nr_repeat}",
                sl_name,
            )

            for job in job_ids:
                self.write_to_sched_log("\n+ " + date_time_tag(), sl_name)
                self.write_to_sched_log("\n---- Executing " + job, sl_name)
                current_proc = self.graph.find_process(job)
                if current_proc is None:
                    self.schedule_fail(
                        f"ERROR: Cannot find process with name/alias: {job}"
                        " schedule terminated",
                        sl_name,
                        sched_lock,
                    )
                    return
                # use the job.star files, only use run.job if not available
                job_file = os.path.join(job, "job.star")
                if not os.path.isfile(job_file):
                    job_file = os.path.join(job, "run.job")

                try:
                    myjob = job_factory.read_job(job_file)
                except ValueError as e:
                    print(e)
                    self.schedule_fail(
                        f"ERROR: there was an error reading job: {current_proc.name}"
                        " schedule terminated",
                        sl_name,
                        sched_lock,
                    )
                    return
                # if certain jobs are repeats they should always be continued
                if myjob.always_continue_in_schedule and repeat >= 1:
                    myjob.is_continue = True
                # check for input nodes before executing job
                for curr_node in current_proc.input_nodes:
                    node_job = os.path.dirname(curr_node.name)
                    waiting_for_node = 0
                    while self.graph.find_node(curr_node.name) is None:
                        print(
                            f"\n\n WARNING: node {curr_node.name} does not exist, "
                            "waiting 1 second...",
                        )
                        time.sleep(1)
                        waiting_for_node += 1
                        if waiting_for_node > 600:
                            self.schedule_fail(
                                f"ERROR: Waited 10 minutes for node {curr_node.name}"
                                f" to appear after job {node_job} finished "
                                "successfully, but it never did. Could there be a "
                                "file system issue?",
                                sl_name,
                                sched_lock,
                            )
                            return
                # write the current job to lock file, in case it needs to be cancelled
                with open(sched_lock, "a") as sched_lockfile:
                    sched_lockfile.write(current_proc.name + "\n")

                # read the pipeline in case other jobs have changed it
                self.graph.read(do_lock=False)
                self.graph.check_process_completion()

                # run the job - marked as a subsequent run of a schedule
                self.run_job(
                    myjob,
                    current_proc,
                    myjob.is_continue,
                    True,
                    False,
                    True,
                )

                success = job_success(myjob.output_name, 5)
                if not success:
                    self.schedule_fail(
                        f"Schedule terminated because process {myjob.output_name}"
                        " failed",
                        sl_name,
                        sched_lock,
                    )
                    return
                time.sleep(seconds_wait_after)

            repeat_finish = datetime.now()
            wait_time = timedelta(minutes=minutes_wait)
            while repeat_finish < repeat_start + wait_time and repeat != nr_repeat - 1:
                time.sleep(1)
                repeat_finish = datetime.now()

        self.write_to_sched_log("\n+ " + date_time_tag(), sl_name)
        self.write_to_sched_log(
            f"\n+ performed all requested repeats in scheduler {fn_sched}. Stopping "
            "pipeliner now ...\n" + "+" * 35,
            sl_name,
        )
        os.remove(sched_lock)
