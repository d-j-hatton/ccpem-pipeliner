#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""
---------------------------
Star file reading utilities
---------------------------

Tools for reading and modifying star files
"""
import os
import time
import shutil
from gemmi import cif
from collections import OrderedDict
from typing import Tuple
from pipeliner.job_options import TRUES
from pipeliner.star_writer import write_jobstar, write
from pipeliner.utils import (
    decompose_pipeline_filename,
    fix_newlines,
    smart_strip_quotes,
)
from pipeliner.star_keys import (
    MOVIES_NODE_NAME,
    MICS_NODE_NAME,
    PARTS_NODE_NAME,
)
from pipeliner.data_structure import (
    PROC_NUM_2_GENERAL_NAME,
    STATUS2LABEL,
    AUTOPICK_DIR,
    AUTOPICK_LOG_NAME,
    AUTOPICK_REF3D_NAME,
    AUTOPICK_REF2D_NAME,
    AUTOPICK_LOG_HELICAL_NAME,
    AUTOPICK_REF3D_HELICAL_NAME,
    AUTOPICK_REF2D_HELICAL_NAME,
    AUTOPICK_TOPAZ_NAME,
    AUTOPICK_TOPAZ_HELICAL_NAME,
    AUTOPICK_TOPAZ_TRAIN_NAME,
    CLASS2D_DIR,
    CLASS2D_PARTICLE_NAME,
    CLASS2D_HELICAL_NAME_EM,
    CLASS2D_PARTICLE_NAME_EM,
    CLASS2D_HELICAL_NAME_VDAM,
    CLASS2D_PARTICLE_NAME_VDAM,
    CLASS3D_DIR,
    CLASS3D_HELICAL_NAME,
    CLASS3D_PARTICLE_NAME,
    CTFFIND_DIR,
    CTFFIND_GCTF_NAME,
    CTFFIND_CTFFIND4_NAME,
    CTFREFINE_REFINE_NAME,
    CTFREFINE_DIR,
    EXTRACT_DIR,
    EXTRACT_HELICAL_NAME,
    EXTRACT_PARTICLE_NAME,
    IMPORT_DIR,
    IMPORT_MOVIES_NAME,
    IMPORT_OTHER_NAME,
    INIMODEL_DIR,
    INIMODEL_JOB_NAME,
    LOCALRES_DIR,
    MANUALPICK_DIR,
    MANUALPICK_JOB_NAME,
    MASKCREATE_DIR,
    MASKCREATE_JOB_NAME,
    BAYESPOLISH_DIR,
    BAYESPOLISH_POLISH_NAME,
    BAYESPOLISH_TRAIN_NAME,
    POSTPROCESS_DIR,
    POSTPROCESS_JOB_NAME,
    REFINE3D_DIR,
    REFINE3D_HELICAL_NAME,
    REFINE3D_PARTICLE_NAME,
    LOCALRES_RESMAP_NAME,
    LOCALRES_OWN_NAME,
    SELECT_ONVALUE_NAME,
    SELECT_DISCARD_NAME,
    SELECT_SPLIT_NAME,
    SELECT_INTERACTIVE_NAME,
    SELECT_DIR,
    MOTIONCORR_DIR,
    MOTIONCORR_OWN_NAME,
    MOTIONCORR_MOTIONCORR2_NAME,
    MANUALPICK_HELICAL_NAME,
    CTFREFINE_ANISO_NAME,
    NODE_TYPE2LABEL,
    NODE_INT2NODELABEL,
    NODELABEL_MICSDATA,
    NODELABEL_LOG,
    NODELABEL_MICSCOORDS,
    NODELABEL_DENSITYMAP,
    NODELABEL_PROCDATA,
    NODELABEL_PARTSDATA,
    EXTERNAL_DIR,
    NODELABEL_MOVIESDATA,
    NODELABEL_MASK3D,
    JOINSTAR_DIR,
    MULTIBODY_DIR,
    SUBTRACT_DIR,
    AUTOPICK_RELIONSTYLE_NAME,
    CTFFIND_RELIONSTYLE_NAME,
    IMPORT_RELIONSTYLE_NAME,
    LOCALRES_RELIONSTYLE_NAME,
    MOTIONCORR_RELIONSTYLE_NAME,
    SELECT_RELIONSTYLE_NAME,
    SUBTRACT_RELIONSTYLE_NAME,
    SUBTRACT_JOB_NAME,
    SUBTRACT_REVERT_NAME,
    JOINSTAR_RELIONSTYLE_NAME,
    JOINSTAR_PARTS_NAME,
    JOINSTAR_MOVIES_NAME,
    JOINSTAR_MICS_NAME,
    MULTIBODY_REFINE_NAME,
    MULTIBODY_RELIONSTYLE_NAME,
    SELECT_AUTO2D_NAME,
)

from pipeliner.star_keys import (
    PROCESS_BLOCK,
    PROCESS_PREFIX,
    PROCESS_SUFFIXES,
    PROCESS_SUFFIXES_OLD,
    NODE_BLOCK,
    NODE_PREFIX,
    NODE_SUFFIXES,
    NODE_SUFFIXES_OLD,
    INPUT_EDGE_BLOCK,
    INPUT_EDGE_PREFIX,
    INPUT_EDGE_SUFFIXES,
    OUTPUT_EDGE_BLOCK,
    OUTPUT_EDGE_PREFIX,
    OUTPUT_EDGE_SUFFIXES,
)

datalabels = (
    "data_",
    "data_pipeline_general",
    "data_pipeline_processes",
    "data_pipeline_nodes",
    "data_pipeline_input_edges",
    "data_pipeline_output_edges",
    "data_job",
    "data_joboptions_values",
    "data_joboptions_full_definition",
    "data_guinier",
    "data_fsc",
    "data_general",
    "data_optics",
    "data_micrographs",
    "data_particles",
    "data_movies",
    "data_schedule_general",
    "data_schedule_bools",
    "data_schedule_floats",
    "data_schedule_strings",
    "data_schedule_operators",
    "data_schedule_jobs",
    "data_schedule_edges",
    "data_output_nodes",
    "data_filament_splines",
    "data_coordinates",
    "data_model_general",
    "data_model_class",
    "data_class_averages",
)
reserved_words = (
    "stop_",
    "data_",
    "save_",
    "global_",
    "loop_",
)


def modify_jobstar(fn_template: str, params_to_change: dict, out_fn: str) -> str:
    """Edits the a template job.star file and saves a copy

    Args:
        fn_template (str): Path to the template star file to be modified
        params_to_change (dict): The Parameters to change in the template in the format
            {param_name: new_value}
        out_fn (str): Name for the output file.  If the same as the template files it
            will be overwritten

    Returns:
        str: The name of the output file
    """
    if not isinstance(params_to_change, dict):
        raise ValueError("ERROR: modify_jobstar() requires a dict as the 2nd input")

    # get the template and make joboptions dict
    the_template = JobStar(fn_template)
    joboptions = the_template.get_all_options()

    # change any necessary parameters
    for changed_opt in params_to_change:
        joboptions[changed_opt] = params_to_change[changed_opt]

    # make sure the output ends in _job.star
    if not out_fn.endswith("_job.star") and os.path.basename(out_fn) != "job.star":
        out_fn = out_fn.replace(".star", "") + "_job.star"

    write_jobstar(joboptions, out_fn)
    return out_fn


def get_job_type(job_options_dict: dict) -> Tuple[str, str]:
    """Get the job type from a job.star file

    Includes back compatibility with the old naming convention in RELION
    3.1 files (_rlnJobType vs _rlnJobTypeLabel)

    Args:
        job_options_dict (dict): A dict of the parameters from a job.star file in
            the format ``{param_name: param_value}``

    Returns:
        tuple: (job_type (:class:`str`), job_type_label (:class:`str`))

        The job type is the pipeliner format string job type. The job_type_label
        is the label used in the star file to designate the job type, which is
        different between RELION 3.x and RELION 4.0/Pipeliner
    """
    # new style ID
    job_type = job_options_dict.get("_rlnJobTypeLabel")
    job_type_label = "_rlnJobTypeLabel"
    if job_type is None:
        job_type_label = "_rlnJobType"

        # for back compatibility with 3.1
        try:
            jt_str = job_options_dict.get("_rlnJobType")
            if jt_str:
                job_type = int(jt_str.strip())
                gen_job_type = PROC_NUM_2_GENERAL_NAME[
                    job_type
                ]  # for back compatibility
                job_type = convert_proctype(gen_job_type, job_options_dict)[0]

        # for back compatibility with mixed files - old style label - new style ID
        except ValueError:
            jt_str = job_options_dict.get("_rlnJobType")
            if not jt_str:
                raise ValueError(f"_rlnJobType not found in {job_options_dict}")
            job_type = jt_str.strip()

    return (job_type, job_type_label)


def count_movs_mics_parts(node):
    """Count the number of objects in a movies, mics or particles starfile

    Args:
        node (:class:`~pipeliner.data_structure.Node`): The `Node` object
            for the star file

    Returns:
        int: Count of object if the file contained movies, micrographs, or particles
        returns ``None`` if the file is not found or it does not contain the correct
        data type
    """
    nodetype = node.type.split(".")[0]
    nodefile = node.name
    if os.path.isfile(node.name):
        data_labels = {
            MOVIES_NODE_NAME: "movies",
            MICS_NODE_NAME: "micrographs",
            PARTS_NODE_NAME: "particles",
        }
        if nodetype in data_labels:
            nodefile = RelionStarFile(node.name)
            if nodetype in data_labels:
                return nodefile.count_block(data_labels[nodetype])
            else:
                return None
    return None


class JobStar:
    """A class for star files that define a pipeliner job parameters

    Attributes:
        data (:class:`gemmi.cif.Cif`): A gemmi cif object containing the data from
            the job.star file
        pipeline_info (:class:`gemmi.cif.Block`): The cif data block for the `job`
            section of the file
        options (:class:`gemmi.cif.Block`): The cif data block for the 'options'
            section of the file
    """

    def __init__(self, fn_in):
        # workaround for star files with reserved words being used in data
        infile = StarfileCheck(fn_in)
        infile.check_reserved_words()

        if infile.been_corrected:
            # the file might take some time to write... wait for it
            itime = 0
            while not os.path.isfile(infile.tmp_name):
                time.sleep(1)
                itime += 1
                if itime > 30:
                    raise ValueError(
                        f"ERROR: Waitied 30 sec for the file {infile.tmp_name} "
                        "to write but it hasn't. Are there disk access issues?"
                    )
            fn_in = infile.tmp_name

        self.data = cif.read_file(fn_in)

        if infile.been_corrected:
            os.remove(infile.tmp_name)

        self.pipeline_info = self.data.find_block("job")
        self.options = self.data.find_block("joboptions_values").find_loop(
            "_rlnJobOptionVariable"
        )
        self.values = self.data.find_block("joboptions_values").find_loop(
            "_rlnJobOptionValue"
        )

    def count_blocks(self):
        """Count the number of blocks in the file

        Returns:
            int: The number of blocks
        """

        return len(self.data)

    def count_pipeline(self):
        """Count the number of items in the pipeline section of the file

        Returns:
            int: The number of items
        """

        return len(list(self.pipeline_info))

    def count_jobopt(self):
        """Count the number of items in the job options section of the file

        Returns:
            int: The number of items
        """
        return len(list(self.options))

    def get_all_options(self, against=None):
        """Get a dict of all the parameters in the file

        Args:
            against (iterable): Only return joboptions that are present in
                this list, if `None` returns all the job options
        Returns:
            dict: The parameters dict in the format {param_name: value}
        """
        against = [] if against is None else against
        joboptions = {}
        job_options = list(zip(list(self.options), list(self.values)))

        for item in self.pipeline_info:
            if item.pair is not None:
                job_options.append((item.pair[0], item.pair[1]))
        for item in job_options:
            if item in against or len(against) == 0:
                joboptions[smart_strip_quotes(item[0])] = smart_strip_quotes(item[1])
        return joboptions

    def get_block(self, block_name):
        """Get a specific block from the file

        Args:
            block_name (str): The name of the block to retrieve

        Returns:
            :class:`gemmi.cif.Block`: The desired block
        """

        the_block = self.data.find_block(block_name)
        return the_block

    def get_jobtype(self):
        options = self.get_all_options()
        jobtype = get_job_type(options)[0]
        return jobtype

    def get_continue_status(self):
        options = self.get_all_options()
        if options["_rlnJobIsContinue"].lower() in TRUES:
            return True
        return False


class BodyFile:
    """A star file that lists the bodies in a multibody refinement

    Attributes:
        data (:class:`gemmi.cif.Cif`): A gemmi cif object containing the data from
        bodies (:class:`gemmi.cif.Block`): The block that contains info about the
            bodies
    """

    def __init__(self, fn_in):
        bodyfile = StarfileCheck(fn_in)
        if bodyfile.been_corrected:
            fn_in = bodyfile.tmp_name

        self.data = cif.read_file(fn_in)

        if bodyfile.been_corrected and bodyfile.remove_temp:
            os.remove(bodyfile.tmp_name)

        self.bodies = self.data.sole_block()

    def count_bodies(self):
        """Count the number of bodies in the BodyFile

        Returns:
            int: The count
        """

        bodycount = len(self.bodies.find_loop("_rlnBodyMaskName"))
        return bodycount


class ExportStar:
    """Class for a star file that lists exported jobs

    *This functionality is from RELION 3.1 and will probably be deprecated*

    Attributes:
        data (:class:`gemmi.cif.Cif`): A gemmi cif object containing the data
            from the star file
        jobs (:class:`gemmi.cif.Block`): The block that contains info about the
            exported jobs
    """

    def __init__(self, fn_in):
        """Create an ExportStar object

        Args:
            fn_in (str): Path to the star file
        """

        exportfile = StarfileCheck(fn_in)
        if exportfile.been_corrected:
            fn_in = exportfile.tmp_name

        self.data = cif.read_file(fn_in)

        if exportfile.been_corrected and exportfile.remove_temp:
            os.remove(exportfile.tmp_name)

        self.jobs = self.data.sole_block()


class OutputNodeStar:
    """A starfile that lists output nodes for exported jobs

    *This functionality is from RELION 3.1 and will probably be deprecated*


    Attributes:
        data (:class:`gemmi.cif.Cif`): A gemmi cif object containing the data from
            the star file
        jobs (:class:`gemmi.cif.Block`): The block that contains info about the
            exported nodes
    """

    def __init__(self, fn_in):
        """Create an OutputNodeStar object

        Args:
            fn_in (str): The path to the starfile

        """
        on_file = StarfileCheck(fn_in)
        self.fn_in = fn_in
        self.data = cif.read_file(fn_in)

        if on_file.been_corrected:
            os.remove(on_file.tmp_name)

    def get_output_nodes(self):
        """Get a list of output node names and types from an OutputNodeStar

        *This functionality is from RELION 3.1 and will probably be deprecated*

        Returns:
            list: [[node 1 name, node 1 type], ...,  [node n name, node n type]]
        """
        on_block = self.data.find_block("output_nodes")

        # check the version
        is_new_format = bool(len(on_block.find_loop(NODE_PREFIX + NODE_SUFFIXES[-1])))

        # convert to new format if necessary
        if not is_new_format:
            print(
                "WARNING: Nodes file {} is a deprecated version"
                ", converting...".format(self.fn_in)
            )
            oldstyle_table = on_block.find(NODE_PREFIX, NODE_SUFFIXES_OLD)
            converted = [[x[0], NODE_TYPE2LABEL[int(x[1])]] for x in oldstyle_table]
            nodes_table = converted
        else:
            nodes_table = on_block.find(
                ["_rlnPipeLineNodeNameLabel", "_rlnPipeLineNodeTypeLabel"]
            )

        return nodes_table


class RelionStarFile(object):
    """A general class for starfiles that contain data, such as particles files

    The input file is checked for reserved word errors when initialising a
    RelionStarFile object

    Attributes:
        data (:class:`gemmi.cif.Cif`): A gemmi cif object containing the data from
            the star file
    """

    def __init__(self, fn_in):
        """Create a RelionStarFile object

        Args:
            fn_in (str): The starfile to read
        """
        # Check that the file does not have any reserved work errors
        infile = StarfileCheck(fn_in)
        infile.check_reserved_words()
        if infile.been_corrected:
            fn_in = infile.tmp_name
        # read the starfile
        self.data = cif.read_file(fn_in)

        # remove the temp file if it was corrected
        if infile.been_corrected and infile.remove_temp:
            os.remove(infile.tmp_name)

    def get_block(self, blockname=None):
        """Get a block from the star file

        Args:
            blockname (str): The name of the block to get.  Use `None`
                if the file has a single unnamed block


        Returns:
            (:class:`gemmi.cif.Block`): The desired block
        """
        if blockname is not None:
            block = self.data.find_block(blockname)
        else:
            block = self.data.sole_block()
        return block

    def count_block(self, blockname=None):
        """Count the number of items in a block that only contains a single loop

        This is the format in most relion data star files

        Args:
            blockname (str): The name of the block to count

        Returns:
            int: The count
        """
        block = self.get_block(blockname)
        item_count = int(str(block[0].loop).split()[1])
        return item_count


class StarfileCheck:
    """A class for checking the validity of star files and converting them

    Checks for reserved word errors which can be common in star files written by
    some versions of RELION. If the starfile was written by RELION 3.x converts it
    to the pipeliner format

    Attributes:
        version (str): Info from the star file's #version line what program wrote it
            and what version number it is
        fn_in (str): The path to the star file.
        cifdoc (:class:`gemmi.cif.Cif`): The cif doc containing the data for the file
        has_been_corrected (bool): If the file has been corrected
        has_been_converted (bool): If the file has been converted from RELION 3.1 to
            RELION 4.0/Pipeliner format
        is_pipeline (bool): If the file is a pipeline star file

    """

    def __init__(self, fn_in=None, cifdoc=None, is_pipeline=False):
        """Create a StarfileCheck object

        Args:
            fn_in (str): The path to the star file.
            cifdoc (:class:gemmi.cif.Cif): The cif doc containing the data for the file
            is_pipeline (bool): If the file is a pipeline star file

        Raises:
            ValueError: If neither a cif doc nor file name is specified
            ValueError: If the file is pipeline file but its version could not be
                determined
        """
        self.version = "undefined"
        self.fn_in = fn_in
        self.cifdoc = cifdoc
        self.been_corrected = False
        self.been_converted = False
        self.is_pipeline = is_pipeline

        # don't need reserved words check on a cif doc because it's alreday been read
        if self.fn_in is not None:
            # make sure line endings are compatible with Relion
            fix_newlines(fn_in)
            self.check_reserved_words()
            try:
                self.cifdoc = cif.read_file(self.fn_in)
            except RuntimeError as e:
                raise ValueError(f"ERROR: Invalid starfile format: {e}")

        if self.is_pipeline:
            self.pipeline_version = self.check_pipeline_version()
            if self.pipeline_version == "relion3":
                self.convert_relion3_1_pipeline()
            if self.pipeline_version == "undefined":
                raise ValueError(
                    "ERROR: The pipeline file version could not be "
                    "determined, it may contain errors"
                )

        if self.cifdoc is None and self.fn_in is None:
            raise ValueError(
                "ERROR: StarfileCheck class requires a filename OR Gemmi cif"
                " object as an input"
            )

    def check_reserved_words(self):
        """Make sure the starfile doesn't contain any illegally used reserved words

        Only operates on a file only as trying to read in a cif doc with reserved
        word errors raises a parse error.  Overwrites the original file if it is
        corrected.  The old file is saved as ``<filename>.old``
        """

        with open(self.fn_in) as thefile:
            f = thefile.readlines()
            for line in f[:4]:
                if "# version" in line:
                    self.version = line.replace("#", "")

        # go line by line and look for reserved words used badly
        nline = 0
        for line in f:
            if (
                line.rstrip() != "loop_"
                and not any(label in line.rstrip() for label in datalabels)
                and line[0] != "#"
            ):
                line_split = line.split()
                for entry in line_split:
                    # quotate them if necessary
                    for word in reserved_words:
                        if entry.startswith(word):
                            print(f[nline], f[nline].replace(entry, '"' + entry + '"'))
                            f[nline] = f[nline].replace(entry, '"' + entry + '"')
                            self.been_corrected = True
            nline += 1
            # if the file has been corrected write a tmp file with the corrections
            if self.been_corrected:
                oldname = self.fn_in + ".old"
                print(f"The file has been overwritten and original saved as {oldname}")
                shutil.move(self.fn_in, oldname)

                with open(self.fn_in, "w") as out_file:
                    for line in f:
                        out_file.write(line)
                print(
                    "WARNING: Reserved words errors were found in the star "
                    "file and have been corrected"
                )

    def check_pipeline_version(self):
        """See if a pipeline is the RELION 3.x or the new RELION 4.0/pipeliner style

        Returns:
            str: The pipeline version `relion3`, `relion4`, or `undefined`

        Raises:
            ValueError: If the pipeline has features of both RELION 3.x and
                RELION 4.0/Pipeliner versions
        """

        # read node block
        proc_block = self.cifdoc.find_block(PROCESS_BLOCK)

        # only check the process block for new - if there are no processes
        # there will be no nodes
        if proc_block is not None:
            # check if the new style node labels are present
            new_proc_count = 0
            old_proc_count = 0
            for label in PROCESS_SUFFIXES[-2:]:
                check_vers = len(proc_block.find_loop(PROCESS_PREFIX + label))
                new_proc_count += check_vers
            check_vers = 0
            for label in PROCESS_SUFFIXES_OLD[-2:]:
                check_vers = len(proc_block.find_loop(PROCESS_PREFIX + label))
                old_proc_count += check_vers
            if new_proc_count * old_proc_count != 0:
                raise ValueError("ERROR: The pipeline was in an unrecognised format")
            elif old_proc_count > new_proc_count:
                return "relion3"
            elif new_proc_count > old_proc_count:
                return "relion4"
            else:
                return "undefined"

    def convert_relion3_1_pipeline(self, display_warning=True):
        """Convert a pipeline from RELION 3.x to RELION 4.0/Pipeliner format

        Backs up the original file as <filename>.old

        Args:
            display_warning (bool): Should a warning be displayed on screen?
        """

        if display_warning:
            print(
                "WARNING: The pipeliner appears to be a older version of the "
                "pipeline.star format\nAttempting to convert it..."
            )

        errors = []
        # read node and process blocks
        if self.fn_in is not None:
            self.cifdoc = cif.read_file(self.fn_in)

        node_block = self.cifdoc.find_block(NODE_BLOCK)
        proc_block = self.cifdoc.find_block(PROCESS_BLOCK)
        in_edge_block = self.cifdoc.find_block(INPUT_EDGE_BLOCK)
        out_edge_block = self.cifdoc.find_block(OUTPUT_EDGE_BLOCK)

        # convert the node and process blocks
        blocktypes = {
            node_block: (NODE_PREFIX, NODE_SUFFIXES, NODE_SUFFIXES_OLD),
            proc_block: (PROCESS_PREFIX, PROCESS_SUFFIXES, PROCESS_SUFFIXES_OLD),
            in_edge_block: (
                INPUT_EDGE_PREFIX,
                INPUT_EDGE_SUFFIXES,
                INPUT_EDGE_SUFFIXES,
            ),
            out_edge_block: (
                OUTPUT_EDGE_PREFIX,
                OUTPUT_EDGE_SUFFIXES,
                OUTPUT_EDGE_SUFFIXES,
            ),
        }
        for block in blocktypes:
            if block is not None:
                prefix = blocktypes[block][0]
                suffixes = blocktypes[block][1]
                suffixes_old = blocktypes[block][2]

                oldstyle_table = list(block.find(prefix, suffixes_old))
                new_table = []
                # conversions for nodes
                if prefix == NODE_PREFIX:
                    # replace the node types with labels
                    new_table = []
                    for node in oldstyle_table:
                        nodename = convert_oldstyle_names(node[0])
                        if not nodename[1]:
                            errors.append(node[0], nodename[0])
                        new_table.append(convert_pipeline_node(nodename, node[1]))

                # conversions for processes
                if prefix == PROCESS_PREFIX:
                    for proc in oldstyle_table:
                        try:
                            conv_type = convert_proctype(proc[0])
                        except Exception as e:
                            newtype = "UNKNOWN"
                            conv_type = (
                                f"Error determining process type: {e}",
                                False,
                            )
                        if conv_type[1]:
                            newtype = conv_type[0]

                        elif not conv_type[1]:
                            if PROC_NUM_2_GENERAL_NAME[int(proc[2])] == "Select":
                                conv_type = ("relion.select.interactive", True)
                                newtype = conv_type[0]
                            else:
                                newtype = (
                                    PROC_NUM_2_GENERAL_NAME[int(proc[2])]
                                    + ".AMBIGUOUS_TYPE"
                                )

                        if not conv_type[1]:
                            errors.append([proc[0], conv_type[0]])
                        new_table.append(
                            [proc[0], proc[1], newtype, STATUS2LABEL[int(proc[3])]]
                        )

                if prefix in (OUTPUT_EDGE_PREFIX, INPUT_EDGE_PREFIX):
                    new_table = [
                        (convert_oldstyle_names(x[0]), convert_oldstyle_names(x[1]))
                        for x in oldstyle_table
                    ]

                # replace the loop with the converted
                new_loop = block.init_loop(prefix, suffixes)
                for line in new_table:
                    new_loop.add_row(line)

        self.pipeline_version = "relion4"
        self.been_converted = True

        if len(errors) != 0:
            print(
                "WARNING:  Errors encountered with the "
                "following nodes/processes:\n{}\nThey have been "
                "marked as UNKNOWN or .AMBIGUOUS".format(
                    "\n".join([x[0] + " : " + x[1] for x in errors])
                )
            )

        # backup the old pipeline file and write a new one
        if self.fn_in is not None:
            print(
                "...pipeline was converted.  The original file has been backed up as "
                f"{self.fn_in}.old"
            )
            shutil.move(self.fn_in, self.fn_in + ".old")
            write(self.cifdoc, self.fn_in)


def bool_jobop(jobop):
    """Checks if a value should be interpreted as True

    Returns:
        bool: `True` if the value is true, otherwise `False`
    """
    if str(jobop).lower() in TRUES:
        return True
    return False


def convert_coordinates_job(fn_in):
    """Convert RELION 3.1 style coordinates files to RELION 4 style

    RELION 3.x has an empty file called coords_suffix_xxx.star RELION 4.0/pipeliner
    has a starfile with two columns for micrograph name and coords file.  If errors are
    encountered with the conversion it sets the node type  for that file as
    UNSUPPORTED_deprecated_format

    Args:
        fn_in (str): Path to the RELION 3.x style file

    Returns:
        str: The new file name

    """
    try:
        job_dir, the_file = os.path.split(fn_in)
        suffix = the_file.split(".")[0].replace("coords_suffix_", "")
        jobstar = JobStar(os.path.join(job_dir, "job.star"))
        jobops = jobstar.get_all_options()
        input_file = jobops.get("fn_in")
        if input_file is None:
            input_file = jobops.get("fn_input_autopick")
        if input_file is None:
            # this means it is an extract file
            input_file = jobops.get("star_mics")
            if jobops["do_reextract"] == "Yes":
                suffix = "reextract"
            else:
                suffix = "helical_segments"

        new_coord_file = cif.Document()
        coords_block = new_coord_file.add_new_block("coordinate_files")
        coords_loop = coords_block.init_loop(
            "_", ["rlnMicrographName", "rlnMicrographCoordinates"]
        )
        micsdata = cif.read(input_file)
        mics_block = micsdata.find_block("micrographs")
        mics = mics_block.find_loop("_rlnMicrographName")
        new_file_name = "{}/{}.star".format(job_dir, suffix)

        for mic in mics:
            mic_path = decompose_pipeline_filename(mic[0])[2].split(".")[0]
            coord_path = job_dir + mic_path + "_" + suffix + ".star"
            coords_loop.add_row([mic[0], coord_path])
            write(new_coord_file, new_file_name)
        return new_file_name

    except Exception:
        print(
            f"WARNING: There was an error converting the file {fn_in} from RELION 3.1 "
            "to RELION 4.0 format.\nThis node has been marked 'UNSUPPORTED_depr"
            "ecated_format' and will not be usable."
        )
        return None


def convert_oldstyle_names(fn_in):
    """Converts model and particle nodes from RELION 3.x to RELION 4 names

    The model files is RELION 3.x have been merged in to the _optimiser files in
    RELION 4. The particle files have moved from a system where the name and location
    of the file define the particles to the particles being explicitly defined in the
    file itself.

    Args:
        fn_in (str): The path to the file to convert

    Returns:
        str: The name of that file's node in RELION 4
    """

    nodename = fn_in
    if "_model.star" in fn_in:
        nodename = fn_in.replace("_model.star", "_optimiser.star")
    elif "coords_suffix_" in fn_in:
        new_nodename = convert_coordinates_job(fn_in)
        if new_nodename is not None:
            nodename = new_nodename
        else:
            nodename = fn_in
    return nodename


def get_joboption(dict_name, label, jobops):
    """Get a job option value from muliple dictionary types

    Return a job option where the jobops dict might contain
    the dictionary entries (from jobstar file) or the labels
    (from runjob file)

    Args:
        dict_name (str): The joboption key IE: from a job.star file
        label (str): The joboption label IE: from a run.job file
        jobops (dict): The dict of joboptions with `{dict_name or label: value}`

    Returns:
        str: The job option value
    """
    jo = jobops.get(dict_name)
    if jo is None:
        jo = jobops.get(label)
    return jo


def convert_proctype(jobname, jobops=None):
    """Convert a process from the RELION to pipeliner style

    The pipeliner has much more specific job types than RELION 3.x/4.0
    so conversion is necessary

    Args:
        jobname (str): The name of the job
        jobops (dict): Dict of joboptions with `{dict_name or label: value}`

    Returns:
        tuple: job name or error and if it was successful:
        (:class:`str`, :class:`bool`)
    """
    # A unique conversion is required for each process type
    jobtype = jobname.split("/")[0]
    need_to_read_jobstar = [
        AUTOPICK_DIR,
        CLASS2D_DIR,
        CLASS3D_DIR,
        EXTRACT_DIR,
        IMPORT_DIR,
        CTFFIND_DIR,
        LOCALRES_DIR,
        MOTIONCORR_DIR,
        BAYESPOLISH_DIR,
        REFINE3D_DIR,
        SELECT_DIR,
        MANUALPICK_DIR,
        CTFREFINE_DIR,
    ]

    # some jobs need to check the jobfile params to determine type
    if jobtype in need_to_read_jobstar and jobops is None:

        jobfile = os.path.join(jobname, "job.star")
        try:
            jobdata = JobStar(jobfile)
        except FileNotFoundError:
            return ("Can't find/read job.star file", False)
        jobops = jobdata.get_all_options()

    # Autopick jobs
    if jobtype in [AUTOPICK_DIR, AUTOPICK_RELIONSTYLE_NAME]:
        log = get_joboption("do_log", "OR: use Laplacian-of-Gaussian?", jobops)
        ref3d = get_joboption("do_ref3d", "OR: provide a 3D reference?", jobops)
        topaz_pick = get_joboption("do_topaz_pick", "", jobops)
        topaz_train = get_joboption("do_topaz_train", "", jobops)
        helical = get_joboption(
            "do_pick_helical_segments", "Pick 2D helical segments?", jobops
        )

        do_log = bool_jobop(log)
        do_ref3d = bool_jobop(ref3d)
        do_topaz_pick = bool_jobop(topaz_pick)
        do_topaz_train = bool_jobop(topaz_train)
        do_helical = bool_jobop(helical)
        if do_log and do_ref3d:
            return (
                "Both LoG and Ref3D options were selected, can't determine job type",
                False,
            )
        elif do_log:
            return (
                (AUTOPICK_LOG_HELICAL_NAME, True)
                if do_helical
                else (AUTOPICK_LOG_NAME, True)
            )
        elif do_ref3d:
            return (
                (AUTOPICK_REF3D_HELICAL_NAME, True)
                if do_helical
                else (AUTOPICK_REF3D_NAME, True)
            )
        elif do_topaz_pick:
            return (
                (AUTOPICK_TOPAZ_HELICAL_NAME, True)
                if do_helical
                else (AUTOPICK_TOPAZ_NAME, True)
            )

        elif do_topaz_train:
            return (AUTOPICK_TOPAZ_TRAIN_NAME, True)

        else:
            return (
                (AUTOPICK_REF2D_HELICAL_NAME, True)
                if do_helical
                else (AUTOPICK_REF2D_NAME, True)
            )

    # Class 2D
    elif jobtype in [CLASS2D_DIR, CLASS2D_PARTICLE_NAME]:
        helical_jo = get_joboption("do_helix", "Classify 2D helical segments?", jobops)
        grad = get_joboption("nr_iter_grad", "Number of VDAM mini-batches:", jobops)

        # if it is already in the pipeliner naming convention
        if helical_jo is None:
            if grad is None:
                return (CLASS2D_PARTICLE_NAME_EM, True)
            else:
                return (CLASS2D_PARTICLE_NAME_VDAM, True)
        do_helical = bool_jobop(helical_jo)
        if do_helical:
            if grad is None:
                return (CLASS2D_HELICAL_NAME_EM, True)
            else:
                return (CLASS2D_HELICAL_NAME_VDAM, True)
        else:
            if grad is None:
                return (CLASS2D_PARTICLE_NAME_EM, True)
            else:
                return (CLASS2D_PARTICLE_NAME_VDAM, True)

    # Class 3D
    elif jobtype in [CLASS3D_DIR, CLASS3D_PARTICLE_NAME]:
        helical_jo = get_joboption("do_helix", "Do helical reconstruction?", jobops)
        # if it is already in the pipeliner naming convention
        if helical_jo is None:
            return (CLASS3D_PARTICLE_NAME, True)
        do_helical = bool_jobop(helical_jo)
        if do_helical:
            return (CLASS3D_HELICAL_NAME, True)
        else:
            return (CLASS3D_PARTICLE_NAME, True)

    # CtfFind
    elif jobtype in [CTFFIND_DIR, CTFFIND_RELIONSTYLE_NAME]:
        gctf_jo = get_joboption("use_gctf", "Use Gctf instead?", jobops)

        if gctf_jo is None:
            return (CTFFIND_CTFFIND4_NAME, True)

        gctf = bool_jobop(gctf_jo)
        if gctf:
            return (CTFFIND_GCTF_NAME, True)
        else:
            return (CTFFIND_CTFFIND4_NAME, True)

    # CtfRefine
    elif jobtype in [CTFREFINE_DIR, CTFREFINE_REFINE_NAME]:
        aniso_jo = get_joboption(
            "do_aniso_mag", "Estimate (anisotropic) magnification?", jobops
        )
        # if it is already in the pipeliner naming convention
        if aniso_jo is None:
            return (CTFREFINE_REFINE_NAME, True)
        aniso = bool_jobop(aniso_jo)
        if aniso:
            return (CTFREFINE_ANISO_NAME, True)
        else:
            return (CTFREFINE_REFINE_NAME, True)

    # Extract
    elif jobtype in [EXTRACT_DIR, EXTRACT_PARTICLE_NAME]:
        helical_jo = get_joboption(
            "do_extract_helix", "Extract helical segments?", jobops
        )

        # if it is already in the pipeliner naming convention
        if helical_jo is None:
            return (EXTRACT_PARTICLE_NAME, True)
        do_helical = bool_jobop(helical_jo)
        if do_helical:
            return (EXTRACT_HELICAL_NAME, True)
        else:
            return (EXTRACT_PARTICLE_NAME, True)

    # Import
    elif jobtype in [IMPORT_DIR, IMPORT_RELIONSTYLE_NAME]:
        raw_jo = get_joboption("do_raw", "Import raw movies/micrographs?", jobops)
        other_jo = get_joboption("do_other", "Import other node types?", jobops)
        do_raw = bool_jobop(raw_jo)
        do_other = bool_jobop(other_jo)

        if do_raw and do_other:
            return (
                "Both do_raw and do_other were selected, can't determine jobtype",
                False,
            )
        elif do_raw:
            return (IMPORT_MOVIES_NAME, True)
        elif do_other:
            return (IMPORT_OTHER_NAME, True)
        else:
            return (
                "Neither do_raw nor do_other were selected, can't determine jobtype",
                False,
            )

    # Initial Model
    elif jobtype == INIMODEL_DIR:
        return (INIMODEL_JOB_NAME, True)

    # Local Res
    elif jobtype in [LOCALRES_DIR, LOCALRES_RELIONSTYLE_NAME]:
        resmap_jo = get_joboption("do_resmap_locres", "Use ResMap?", jobops)
        relion_jo = get_joboption("do_relion_locres", "Use Relion?", jobops)
        if resmap_jo is None or relion_jo is None:
            return ("Could not determine type of LocalRes Job", False)
        resmap = bool_jobop(resmap_jo)
        relionlocal = bool_jobop(relion_jo)
        if resmap and relionlocal:
            return (
                "Both RESMAP and RELION localres selected, can't determine job type",
                False,
            )
        elif resmap:
            return (LOCALRES_RESMAP_NAME, True)
        elif relionlocal:
            return (LOCALRES_OWN_NAME, True)
        else:
            return (
                "Neither RESMAP nor RELION localres selected, can't determine job type",
                False,
            )

    # ManualPick
    elif jobtype in [MANUALPICK_DIR, MANUALPICK_JOB_NAME]:
        helix_jo = get_joboption(
            "do_starend", "Pick start-end coordinates helices?", jobops
        )
        # if already in pipeliner naming convention
        if helix_jo is None:
            return (MANUALPICK_JOB_NAME, True)
        helix = bool_jobop(helix_jo)
        if helix:
            return (MANUALPICK_HELICAL_NAME, True)
        else:
            return (MANUALPICK_JOB_NAME, True)

    # MaskCreate
    elif jobtype == MASKCREATE_DIR:
        return (MASKCREATE_JOB_NAME, True)

    elif jobtype in [BAYESPOLISH_DIR, BAYESPOLISH_POLISH_NAME]:
        train_jo = get_joboption("do_param_optim", "Train optimal parameters?", jobops)
        polish_jo = get_joboption("do_polish", "Perform particle polishing?", jobops)

        # if already in pipeliner naming convention
        if train_jo is None:
            return (BAYESPOLISH_POLISH_NAME, True)
        train = bool_jobop(train_jo)
        polish = bool_jobop(polish_jo)
        if train and polish:
            return (
                "Both training and polishing selected, can't determine job type",
                False,
            )
        elif polish:
            return (BAYESPOLISH_POLISH_NAME, True)
        elif train:
            return (BAYESPOLISH_TRAIN_NAME, True)
        else:
            return (
                "Neither training nor polishing selected, can't determine job type",
                False,
            )
    # MotionCorr
    elif jobtype in [MOTIONCORR_DIR, MOTIONCORR_RELIONSTYLE_NAME]:
        own = get_joboption(
            "do_own_motioncor", "Use RELION's own implementation?", jobops
        )
        if own is None:
            return ("Ambiguous MotionCorr Job type", False)
        do_own = bool_jobop(own)
        if do_own:
            return (MOTIONCORR_OWN_NAME, True)
        return (MOTIONCORR_MOTIONCORR2_NAME, True)

    # PostProcess
    elif jobtype == POSTPROCESS_DIR:
        return (POSTPROCESS_JOB_NAME, True)

    # Refine3D
    elif jobtype in [REFINE3D_DIR, REFINE3D_PARTICLE_NAME]:
        helical_jo = get_joboption("do_helix", "Do helical reconstruction?", jobops)
        if helical_jo is None:
            return (REFINE3D_PARTICLE_NAME, True)
        helical = bool_jobop(helical_jo)
        if helical:
            return (REFINE3D_HELICAL_NAME, True)
        return (REFINE3D_PARTICLE_NAME, True)

    # Select
    elif jobtype in [SELECT_DIR, SELECT_RELIONSTYLE_NAME]:
        dups_jo = get_joboption(
            "do_remove_duplicates",
            "OR: remove duplicates?",
            jobops,
        )
        discard_jo = get_joboption(
            "do_discard",
            "OR: select on image statistics?",
            jobops,
        )
        split_jo = get_joboption(
            "do_split",
            "OR: split into subsets?",
            jobops,
        )
        on_vals_jo = get_joboption(
            "do_select_values",
            "Select based on metadata values?",
            jobops,
        )
        autoranker = get_joboption(
            "do_class_ranker",
            "Automatically select 2D classes?",
            jobops,
        )

        for opt in dups_jo, discard_jo, split_jo, on_vals_jo, autoranker:
            if opt is None:
                return (
                    "Ambiguous Select type job.  Options: {}, {}, {}, {}, "
                    "or {}".format(
                        SELECT_DISCARD_NAME,
                        SELECT_INTERACTIVE_NAME,
                        SELECT_ONVALUE_NAME,
                        SELECT_SPLIT_NAME,
                        SELECT_AUTO2D_NAME,
                    ),
                    False,
                )
        dups = bool_jobop(dups_jo)
        discard = bool_jobop(discard_jo)
        split = bool_jobop(split_jo)
        on_vals = bool_jobop(on_vals_jo)
        do_autoranker = bool_jobop(autoranker)

        count = 0
        for opt in [dups, discard, split, on_vals, do_autoranker]:
            if opt:
                count += 1
        if count > 1:
            return ("Multiple functions were selected, can't determine job type", False)
        elif on_vals:
            return (SELECT_ONVALUE_NAME, True)
        elif discard:
            return (SELECT_DISCARD_NAME, True)
        elif split:
            return (SELECT_SPLIT_NAME, True)
        elif do_autoranker:
            return (SELECT_AUTO2D_NAME, True)
        else:
            return (SELECT_INTERACTIVE_NAME, True)

    # Subtract
    elif jobtype in [SUBTRACT_DIR, SUBTRACT_RELIONSTYLE_NAME]:
        fliplabel_jo = get_joboption(
            "do_fliplabel", "OR revert to original particles?", jobops
        )
        # if already in pipeliner format
        if fliplabel_jo is None:
            return (SUBTRACT_JOB_NAME, True)
        fliplabel = bool_jobop(fliplabel_jo)
        if fliplabel:
            return (SUBTRACT_REVERT_NAME, True)
        return (SUBTRACT_JOB_NAME, True)

    # JoinStar
    elif jobtype in [JOINSTAR_DIR, JOINSTAR_RELIONSTYLE_NAME]:
        do_parts = get_joboption("do_part", "Combine particle STAR files?", jobops)
        do_movies = get_joboption("do_mov", "Combine movie STAR files?", jobops)
        do_mics = get_joboption("do_mic", "Combine micrograph STAR files?", jobops)

        if do_parts is None and do_movies is None and do_mics is None:
            return ("No type of file to join was selected", False)
        count = 0
        parts = bool_jobop(do_parts)
        if parts:
            jtype = JOINSTAR_PARTS_NAME
            count += 1
        movies = bool_jobop(do_movies)
        if movies:
            jtype = JOINSTAR_MOVIES_NAME
            count += 1
        mics = bool_jobop(do_mics)
        if mics:
            jtype = JOINSTAR_MICS_NAME
            count += 1
        if count > 1:
            return ("Multiple types of files were selected to join", False)
        else:
            return (jtype, True)

    # MultiBody
    elif jobtype in [MULTIBODY_DIR, MULTIBODY_RELIONSTYLE_NAME]:
        analysis = get_joboption("do_analyse", "Run flexibility analysis?", jobops)

        if analysis is None:
            return "Ambiguous MultiBody Job type"

        do_analysis = bool_jobop(analysis)
        if do_analysis:
            print(
                "\nWARNING: The ccpem-pipeliner has separated the MultiBody Refine"
                " job from the flexibility analysis. This job will only run "
                "the refinement\n Run or schedule an additional "
                "relion.multibody.flexanalysis job after this job has finished\n"
            )
        return (MULTIBODY_REFINE_NAME, True)

    elif jobtype in [BAYESPOLISH_DIR, BAYESPOLISH_POLISH_NAME]:
        train = get_joboption("do_param_optim", "Train optimal parameters?", jobops)
        if train is not None:
            do_train = bool_jobop(train)
        if do_train:
            return (BAYESPOLISH_TRAIN_NAME, True)
        return (BAYESPOLISH_POLISH_NAME, True)

    else:
        return ("Unrecognised job type", False)


def convert_pipeline_node(name, oldtype):
    """Convert node names from a RELION 3.1 to the RELION 4.0 formats

    Every job type has a specific method for converting its own node types

    Args:
        name (str): The node name
        oldtype (str): The original node type

    Returns:
        tuple: job name or error and if it was successful:
        (:class:`str`, :class:`bool`)
    """

    jobtype = name.split("/")[0]
    filename = name.split("/")[-1]
    jobname = os.path.dirname(name)
    filename_conv = {}
    use_gen_name = False

    try:
        node_genname = NODE_INT2NODELABEL[int(oldtype)]
    except KeyError:
        return (f"Cannot Identify node type: {oldtype}", False)

    try:
        ext = "." + filename.split(".")[-1]
    except IndexError:
        ext = ""

    # Autopick
    if jobtype == AUTOPICK_DIR:
        filename_conv = {
            "logfile.pdf": ".pdf.relion.autopick",
            "autopick.star": ".star.relion.autopick",
            "coords_suffix_autopick.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # Class 2D
    # TODO: Needs helical
    elif jobtype == CLASS2D_DIR:
        # this one has to be done on filetype because the
        # iteration number will be different for individual jobs
        use_gen_name = True
        is_halfmap = ""
        filename_conv = {
            NODELABEL_PROCDATA: ".star.relion.optimiser.class2d",
            NODELABEL_PARTSDATA: ".star.relion.class2d",
        }

    # Class 3D
    # TODO: Needs helical
    elif jobtype == CLASS3D_DIR:
        is_halfmap = "Half" if ".mrc" in filename and "half" in filename else ""
        use_gen_name = True
        filename_conv = {
            NODELABEL_PROCDATA: ".star.optimiser.relion.class3d",
            NODELABEL_PARTSDATA: ".star.relion.class3d",
            NODELABEL_DENSITYMAP: ".mrc.relion.class3d",
            NODELABEL_DENSITYMAP + "Half": ".mrc.relion.class3d.halfmap",
        }

    # CtfFind
    elif jobtype == CTFFIND_DIR:
        filename_conv = {
            "micrographs_ctf.star": ".star.relion.ctf",
            "logfile.pdf": ".pdf.relion.ctffind",
        }

    # CtfRefine
    elif jobtype == CTFREFINE_DIR:
        jobops = JobStar(os.path.join(jobname, "job.star")).get_all_options()

        aniso = bool_jobop(jobops.get("do_aniso_mag"))
        if aniso:
            filename_conv = {
                "particles_ctf_refine.star": ".star.relion.anisomagrefine",
                "logfile.pdf": ".pdf.relion.ctfrefine",
            }
        else:
            filename_conv = {
                "particles_ctf_refine.star": ".star.relion.ctfrefine",
                "logfile.pdf": ".pdf.relion.ctfrefine",
            }

    # External
    if jobtype == EXTERNAL_DIR:
        return "UnknownNodeType" + ext

    # Extract
    elif jobtype == EXTRACT_DIR:
        # need to check on the naming of the coords files first
        filename_conv = {
            "particles.star": ".star.relion",
            "reextract.star": ".star.relion",
            "helical_segments": ".star.relion",
            "coords_suffix_extract.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # Import
    elif jobtype == IMPORT_DIR:
        is_halfmap = "Half" if ".mrc" in filename and "half" in filename else ""
        use_gen_name = True
        filename_conv = {
            NODELABEL_MICSDATA: ".star.relion",
            NODELABEL_MOVIESDATA: ".star.relion",
            NODELABEL_MICSCOORDS: ".star.relion",
            NODELABEL_DENSITYMAP + "Half": ".mrc.halfmap",
            NODELABEL_DENSITYMAP: ".mrc",
            NODELABEL_MASK3D: ".mrc",
        }

    # Initial Model
    elif jobtype == INIMODEL_DIR:
        use_gen_name = True
        is_halfmap = ""
        filename_conv = {
            NODELABEL_DENSITYMAP: ".mrc.relion.initialmodel",
            NODELABEL_PARTSDATA: ".star.relion.initialmodel",
            NODELABEL_PROCDATA: ".star.relion.optimser.initialmodel",
        }

    # JoinStar
    elif jobtype == JOINSTAR_DIR:
        return [node_genname + ".star.relion"]

    # LocalRes
    elif jobtype == LOCALRES_DIR:
        # different conversion here just done by filename
        filename_conv = {
            "histogram.pdf": ".pdf.relion.localres",
            "relion_locres_filtered.mrc": ".mrc.relion.localresfiltered",
            "relion_locres.mrc": ".mrc.relion.localres",
            "half1_resmap.mrc": ".mrc.resmap.localres",
        }

    # ManualPick
    elif jobtype == MANUALPICK_DIR:
        # different conversion here just done by filename
        filename_conv = {
            "manualpick.star": ".star.relion.manualpick",
            "micrographs_selected.star": ".star.relion",
            "coords_suffix_manualpick.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # MaskCreate
    elif jobtype == MASKCREATE_DIR:
        return [name, node_genname + ".mrc.relion"]

    # MotionCorr
    elif jobtype == MOTIONCORR_DIR:
        filename_conv = {
            "corrected_micrographs.star": ".star.relion.motioncorr",
            "logfile.pdf": ".pdf.relion.motioncorr",
        }

    # MultiBody
    elif jobtype == MULTIBODY_DIR:
        is_halfmap = "Half" if ".mrc" in filename and "half" in filename else ""
        use_gen_name = True
        filename_conv = {
            NODELABEL_DENSITYMAP + "Half": ".mrc.relion.halfmap.multibody",
            # this isn't used yet but might be added in
            NODELABEL_DENSITYMAP: ".mrc.relion.multibody",
            NODELABEL_PARTSDATA: ".star.relion.multibody.eigenselection",
            NODELABEL_LOG: ".pdf.relion.multibody.flexanalyse",
        }

    # Polish
    elif jobtype == BAYESPOLISH_DIR:
        filename_conv = {
            "shiny.star": ".star.relion.polished",
            "opt_params_all_groups.txt": ".txt.relion.polishparams",
            "logfile.pdf": ".pdf.relion.polish",
        }

    # PostProcess
    elif jobtype == POSTPROCESS_DIR:
        filename_conv = {
            "postprocess.mrc": ".mrc.relion.postprocessed",
            "postprocess_masked.mrc": ".mrc.relion.postprocessed.masked",
            "logfile.pdf": ".pdf.relion.postprocess",
            "postprocess.star": ".star.relion.postprocess",
        }

    # Refine3D
    # TODO: needs to check for helical
    elif jobtype == REFINE3D_DIR:
        filename_conv = {
            "run_class001.mrc": ".mrc.relion.refine3d",
            "run_data.star": ".star.relion.refine3d",
            "run_half1_class001_unfil.mrc": ".mrc.relion.refine3d.halfmap",
        }

    # Select
    elif jobtype == SELECT_DIR:
        if "class_averages" in name:
            return [name, node_genname + ".star.relion.classaverages"]
        return [name, node_genname + ".star.relion"]

    # subtract
    elif jobtype == SUBTRACT_DIR:
        filename_conv = {
            "particles_subtracted.star": ".star.relion.subtracted",
            "original.star": ".star.relion.subtractreverted",
        }

    # for nodes that use a relion 3.1 node type, but have no associated job
    if use_gen_name:
        try:
            return [name, node_genname + filename_conv[node_genname + is_halfmap]]
        except KeyError:
            return [name, "UnknownNodeType" + ext]

    else:
        try:
            return [name, node_genname + filename_conv[filename]]
        except KeyError:
            return [name, "UnknownNodeType" + ext]


def star_loop_as_list(starfile, block, columns=None):
    """Returns a set of columns from a starfile loop as a list

    Args:
        starfile (str): The file to read from
        block (str): The name of the block to get the data from
        columns (list): Names of the columns to get, if None then all
            columns are returned

    Returns:
        tuple: (:class:`list`, :class:`list`) [0] The names of the columns in order,
        [c1, c2, c3]. [1] The rows of the column(s) as a list of lists
        [[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3]]

    Raises:
        ValueError: If the specified block is not found
        ValueError: If the specified block does not contain a loop
        ValueError: If any of the specified columns are not found
    """
    sf = cif.read(starfile)
    fblock = sf.find_block(block)
    if fblock is None:
        raise ValueError(f"Block '{block}' not found")
    loop = fblock[0].loop
    if loop is None:
        raise ValueError(
            f"Block '{block}' does not contain a loop; Use star_pairs_as_dict()"
            " instead"
        )
    cols = loop.tags[0:] if columns is None else columns
    for col in cols:
        if col not in loop.tags[0:]:
            raise ValueError(f"Column '{col}' not found in block '{block}'")
    data = []
    for row in fblock.find(cols):
        data.append([x for x in row])
    return cols, data


def star_pairs_as_dict(starfile, block):
    """Returns paired values from a starfile as a dict

    Args:
        starfile (str): The file to read from
        block (str): The name of the block to get the data from

    Returns:
        dict: `{parameter: value}`

    Raises:
        ValueError: If the specified block is not found
        ValueError: If the specified block is a loop and not a pair-value
    """
    sf = cif.read(starfile)
    fblock = sf.find_block(block)
    if fblock is None:
        raise ValueError(f"Block '{block}' not found")
    out_dict = {}
    if fblock[0].pair is None:
        raise ValueError(
            f"Block '{block}' does not contain item-value pairs. "
            "Try using star_loop_as_list() instead"
        )
    for item in fblock:
        out_dict[item.pair[0]] = item.pair[1]
    return out_dict


def compare_starfiles(starfile1, starfile2):
    """See if two starfiles contain the same information

    Direct comparison can be difficult because the starfile columns or blocks
    can be in different orders

    Args:
        starfile1 (str): Name of the first file
        starfile2 (str): Name of the second file

    Returns:
        tuple: (:class:`bool`, :class:`bool`) [0] True if the starfile structures
        are identical (Names of blocks and columns) [1] True if the data in
        the two files are identical
    """
    sf1, sf2 = cif.read(starfile1), cif.read(starfile2)
    # check the blocks
    sf1_blocks = set([x.name for x in sf1])
    sf2_blocks = set([x.name for x in sf2])
    if sf1_blocks != sf2_blocks:
        return False, False

    # check loops/pairs within blocks
    for block in sf1_blocks:
        try:
            l1 = set(star_loop_as_list(starfile1, block)[0])
            l2 = set(star_loop_as_list(starfile2, block)[0])

        except ValueError:
            l1 = set(star_pairs_as_dict(starfile1, block))
            l2 = set(star_pairs_as_dict(starfile2, block))
        if l1 != l2:
            return False, False

    # check the data
    for block in sf1_blocks:
        try:
            l1_lists = star_loop_as_list(starfile1, block)[1]
            l2_lists = star_loop_as_list(starfile2, block)[1]
            l1_sets = [set(x) for x in l1_lists]
            l2_sets = [set(x) for x in l2_lists]
            for i in l1_sets:
                if i not in l2_sets:
                    return True, False
            for i in l2_sets:
                if i not in l1_sets:
                    return True, False

        except ValueError:
            d1 = star_pairs_as_dict(starfile1, block)
            d2 = star_pairs_as_dict(starfile2, block)
            l1 = set([d1[x] for x in d1])
            l2 = set([d2[x] for x in d2])
            if l1 != l2:
                return True, False

    return True, True


def convert_relion20_datafile(
    starfile,
    dtype,
    outname=None,
    boxsize=None,
    og_name="convertedOpticsGroup1",
    og_num=1,
):

    """Convert a relion 2.x format starfile to relion 3.x/4.x format

    Adds on an optics groups block and renames the main data block with
    the relion 3/x/4.x format. Fixes te descrepency between how Relion 2.x
    and Relion 3.x/4.x define pixel size. Adds missing fields to the data
    block.

    Args:
        starfile (str): The name of the file to process
        outname (str): The name of the output file, 'converted_<starfile>.star'
            by default.
        dtype(str): The type of file, must be in (movies, micrographs,
            ctf_micrographs, particles)
        boxsize (int): The box size of particles in pix.  This is required for
            particles
        og_name (str): The name for the optics group that will be created for the data
            in the file
        og_num (int): Optics group number for the optics group that
            will be created for the data in the file
    """

    data = cif.read_file(starfile).sole_block()

    # get the parameters for the optics group of the correct data file type
    # default values - if None then it is required to be in the original file
    parts_cols = OrderedDict(
        [
            ("_rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", None),
            ("_rlnVoltage", None),
            ("_rlnSphericalAberration", None),
            ("_rlnAmplitudeContrast", None),
            ("_rlnImagePixelSize", None),
            ("_rlnImageSize", str(boxsize)),
            ("_rlnImageDimensionality", "2"),
        ]
    )
    mics_cols = OrderedDict(
        [
            ("rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", None),
            ("_rlnVoltage", None),
            ("_rlnSphericalAberration", None),
            ("_rlnAmplitudeContrast", None),
            ("_rlnMicrographPixelSize", None),
        ]
    )
    movs_cols = OrderedDict(
        [
            ("_rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", None),
            ("_rlnVoltage", None),
            ("_rlnSphericalAberration", None),
            ("_rlnAmplitudeContrast", None),
        ]
    )

    if dtype == "particles":
        og_cols = parts_cols
    elif dtype == "micrographs":
        og_cols = mics_cols
    elif dtype == "movies":
        og_cols = movs_cols
    else:
        raise ValueError(
            "Invalid Relion data file type specified. Choose from particles,"
            " micrographs, or movies"
        )

    # columns present in Relion 2.x files but not 3.x
    r2x_og_cols = ["_rlnDetectorPixelSize", "_rlnMagnification"]

    # find all the OG columns in the input file
    # replace the values in og_cols if they were present in the input file
    all_og_cols = list(og_cols) + r2x_og_cols
    found_cols = [data.find_loop(col) for col in all_og_cols]
    num_ogs = []
    for col in found_cols:
        if str(col) != "<gemmi.cif.Column nil>":
            col_name = str(col).split()[1]
            dat = [x for x in col]
            num_ogs = len(set(dat))
            if num_ogs > 1:
                raise ValueError(
                    "File has multiple optics groups - currently not setup to convert"
                    " these files"
                )
            og_cols[col_name] = dat[0]

    # calculate the image pixel size from the detector pixel size and magnification
    # Relion 2.x files shouldn't have a _rlnImagePixelSize column
    dps = og_cols["_rlnDetectorPixelSize"]
    mag = og_cols["_rlnMagnification"]
    apix_i = og_cols.get("rlnImagePixelSize")
    apix_m = og_cols.get("rlnMicrographPixelSize")

    if None in [dps, mag] and (apix_m is None and apix_i is None):
        raise ValueError("Pixel size information missing from starfile")
    apix = str(float(dps) / float(mag) * 10000)
    if dtype == "particles" and apix_i is None:
        og_cols["_rlnImagePixelSize"] = apix
    elif dtype == "micrographs" and apix_m is None:
        og_cols["_rlnMicrographPixelSize"] = apix

    og_cols["_rlnMicrographOriginalPixelSize"] = apix

    print(
        "WARNING: Using this tool on binned data may cause issues\n"
        "Relion 2.x does not save binning information in its data star files "
        "so this tool will produced converted files where the binned pixel size"
        "is treated as the original pixel size."
    )

    # handle boxsize for particles files
    if dtype == "particles":
        if og_cols["_rlnImageSize"] is None:
            raise ValueError(
                "Box size not specified, you must specify box size for files containing"
                " particles"
            )

    # make "rlnOpticsGroupName" block and populate it
    outdoc = cif.Document()
    og_block = outdoc.add_new_block("optics")
    og_loop = og_block.init_loop("", list(og_cols))
    og_row = []
    for col in og_cols:
        val = og_cols[col]
        if val is None:
            raise ValueError(f"Required value {col} was not founnd")

        og_row.append(val)
    og_loop.add_row(og_row)

    # get the columns necessary for the data block
    data_loop = data[0].loop
    all_dcols = data_loop.tags[0:]
    data_cols = list(set(all_dcols) - set(all_og_cols))

    # make main data block and populate
    out_data_block = outdoc.add_new_block(dtype)
    out_data_loop = out_data_block.init_loop("", data_cols + ["_rlnOpticsGroup"])

    # populate the data rows
    data_table = data.find(data_cols)
    for row in data_table:
        row_data = [x for x in row]
        row_data.append(og_cols["_rlnOpticsGroup"])
        out_data_loop.add_row(row_data)

    if outname is None:
        outname = f"{os.path.basename(starfile).split('.')[0]}_converted.star"
    elif not outname.endswith(".star"):
        outname += ".star"
    write(outdoc, outname)
