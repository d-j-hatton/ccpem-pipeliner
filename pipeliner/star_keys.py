#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""
Keys for field names in RELION pipeline STAR files
"""

# General
GENERAL_BLOCK = "pipeline_general"
JOB_COUNTER = "_rlnPipeLineJobCounter"

# Processes
PROCESS_BLOCK = "pipeline_processes"
PROCESS_PREFIX = "_rlnPipeLineProcess"
PROCESS_SUFFIXES = ["Name", "Alias", "TypeLabel", "StatusLabel"]
PROCESS_SUFFIXES_OLD = ["Name", "Alias", "Type", "Status"]

# Nodes
NODE_BLOCK = "pipeline_nodes"
NODE_PREFIX = "_rlnPipeLineNode"
NODE_SUFFIXES = ["Name", "TypeLabel"]
NODE_SUFFIXES_OLD = ["Name", "Type"]
# Input edges
INPUT_EDGE_BLOCK = "pipeline_input_edges"
INPUT_EDGE_PREFIX = "_rlnPipeLineEdge"
INPUT_EDGE_SUFFIXES = ["FromNode", "Process"]

# Output edges
OUTPUT_EDGE_BLOCK = "pipeline_output_edges"
OUTPUT_EDGE_PREFIX = "_rlnPipeLineEdge"
OUTPUT_EDGE_SUFFIXES = ["Process", "ToNode"]

# used to define general nodetypes
MOVIES_NODE_NAME = "MicrographMoviesData"
MICS_NODE_NAME = "MicrographsData"
PARTS_NODE_NAME = "ParticlesData"
