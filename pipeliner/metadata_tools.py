#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import json
from glob import glob
from typing import Any, Dict, Optional, Union
from pipeliner.data_structure import JOBSTATUS_SUCCESS, Process
from pipeliner.job_factory import new_job_of_type, active_job_from_proc
from pipeliner.utils import smart_strip_quotes
from pipeliner.job_options import (
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    JobOption,
)

# from pipeliner.project_graph import ProjectGraph


def format_for_metadata(joboption: JobOption) -> Optional[Union[str, float, int, bool]]:
    """Format data from relion starfiles for JSON.

    Changes 'Yes'/'No" to True/False, None for blank vals and removes any
    quotation marks

    Args:
        joboption (:class:`pipeliner.job_options.JobOption`): The joboption
            to format

    Returns:
        str: A properly formatted :class:`string`, :class:`float` or
            :class:`int` or :class:`bool`

    Raises:
        ValueError: If a boolean job option doesn't have a value compatible
            with a class:bool"
    """
    if isinstance(joboption, FloatJobOption):
        return joboption.get_number()
    elif isinstance(joboption, IntJobOption):
        return joboption.get_number()
    elif isinstance(joboption, BooleanJobOption):
        return joboption.get_boolean()
    else:
        val = smart_strip_quotes(joboption.get_string())
        if val == "":
            return None  # TODO: why do we use None here rather than just returning ""?
        return val


def get_job_metadata(this_job: Process) -> dict:
    """Run a job's metadata gathering method

    Args:
        this_job (:class:`pipeliner.data_structure.Process`): The `Process` object
            for the job to gather metadata from

    Returns:
        dict: Metadata dict for the job
        Returns ``None`` if the job has no metadata gathering function
    """
    if this_job.status != JOBSTATUS_SUCCESS:
        return {
            "No metadata": f"job {this_job.name} has not finished"
            "successfully, you can only gather metadata from successfully "
            "finished jobs"
        }
    try:
        the_job = active_job_from_proc(this_job)

        # job optioons are cleaned in case a RELION style job star is used
        # instead of a pipeliner style one
        md_jobops = {"JobType": the_job.PROCESS_NAME, "Continued": the_job.is_continue}

        for opt in the_job.joboptions:
            md_jobops[opt] = format_for_metadata(the_job.joboptions[opt])

        the_job.output_name = this_job.name
        metadata = {"run_parameters": md_jobops, "results": the_job.gather_metadata()}
        # add in the logfile and outputnode info to the results metadata

        metadata["results"]["OutputFiles"] = [x.name for x in this_job.output_nodes]

        logfiles = [
            os.path.join(this_job.name, "run.out"),
            os.path.join(this_job.name, "run.err"),
        ]
        if metadata["results"].get("LogFiles") is None:
            metadata["results"]["LogFiles"] = logfiles
        else:
            metadata["results"]["LogFiles"] += logfiles

        # TODO: do the validation tasks here:
        # - check if the schema exist
        # - if not, create them
        # - read the schema
        # validate the created dict against the schema

        return metadata
    except Exception as e:
        return {"metadata collection error": str(e)}


def get_metadata_chain(
    pipeline, this_job: Process, output_file: Optional[str] = None, full: bool = False
) -> dict:
    """Get the metadata for a job and all its upstream jobs

    Metadata is gathered by each individual job's gather_metadata
    function

    Args:
        pipeline (:class:`~pipeliner.project_graph.ProjectGraph`): The current pipeline
        this_job(:class:`~pipeliner.data_structure.Process`):  The `Process`
            object for the terminal job
        output_file (str): The name of a file to write the results to; if ``None``
            no file will be written
        full (bool): Should the metadata report also contain information about
            continuations and multiple runs of the the jobs or just the current one
    Returns:
        dict: The metadata dictionary
    """
    metadata_dict: Dict[str, Any] = {}
    tracked_job: Dict[str, Any] = {}
    jobs_metadata = {}
    job_network = []

    tracked_job["Terminal job"] = this_job.name
    upstream = pipeline.get_upstream_network(this_job)
    upstream_procs = [os.path.dirname(x[0].name) + "/" for x in upstream]
    tracked_job["Number of parent jobs"] = len(upstream_procs)

    jobs_metadata[this_job.name] = get_job_metadata(this_job)

    if full:
        prev_runs = glob(f"{this_job.name}*job_metadata.json")
        prev_runs.sort()
        if len(prev_runs) > 1:
            for pr in prev_runs[:-1]:
                pjob_name = os.path.basename(pr).split("_")[0]
                pjob = f"{this_job.name} previous run {pjob_name}"
                with open(pr, "r") as prdata:
                    jobs_metadata[pjob] = json.loads(prdata.read())

    for job in upstream_procs:
        jobproc = pipeline.find_process(job)
        jobs_metadata[job] = get_job_metadata(jobproc)
        if full:
            prev_runs = glob(f"{jobproc.name}*job_metadata.json")
            prev_runs.sort()
            if len(prev_runs) > 1:
                for pr in prev_runs[:-1]:
                    pjob_name = os.path.basename(pr).split("_")[0]
                    pjob = f"{jobproc.name} previous run {pjob_name}"
                    with open(pr, "r") as prdata:
                        jobs_metadata[pjob] = json.loads(prdata.read())

    for edge in upstream:
        job_network.append([edge[1].name, edge[2].name, edge[0].name])

    metadata_dict["CCPEM pipeliner job metadata trace"] = tracked_job
    metadata_dict["Jobs metadata"] = jobs_metadata
    metadata_dict["Jobs network edges"] = job_network
    if output_file is not None:
        if output_file[-5:] != (".json"):
            output_file += ".json"
        with open(output_file, "w") as outfile:
            json.dump(metadata_dict, outfile, indent=4)
    return metadata_dict


def make_job_parameters_schema(job_type: str) -> dict:
    """
    Write the json schema for the running parameters of a job

    The metadata schema have two parts: the running parameters part is generated
    automatically and the results part is written by the user

    Args:
        job_type (str): The job type to write the schema for

    Returns:
        dict: The json schema ready to be dumped to json file
    """
    the_job = new_job_of_type(job_type)
    scheme_dict: dict = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": f"PARMS: {job_type}",
        "description": f"{the_job.jobinfo.short_desc}",
        "type": "object",
        "properties": {},
    }

    for jo in the_job.joboptions:
        jo_types = {
            "STRING": "string",
            "MULTIPLECHOICE": "string",
            "FILENAME": "string",
            "INPUTNODE": "string",
            "FLOAT": "number",
            "INT": "integer",
            "BOOLEAN": "boolean",
        }
        scheme_dict["properties"][jo] = {
            "description": the_job.joboptions[jo].label,
            "type": jo_types[the_job.joboptions[jo].joboption_type],
        }
    return scheme_dict


def make_default_results_schema(job_type: str) -> dict:
    """
    Write the json schema for the results of a job if one was not provided

    The metadata schema have two parts: the running parameters part is generated
    automatically and the results part is written by the user.  If no schema was
    provided for the results this function is used to write a blank plackholder
    one

    Args:
        job_type (str): The job type to write the schema for

    Returns:
        dict: The json schema ready to be dumped to json file
    """
    the_job = new_job_of_type(job_type)
    scheme_dict = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": f"RESULTS: {job_type}",
        "description": f"{the_job.jobinfo.short_desc}",
        "type": "object",
        "properties": {
            "OutputFiles": {
                "description": "Output nodes added to the pipeline",
                "type": "array",
                "Items": {"type": "string"},
            },
            "LogFiles": {
                "description": "log files produced by the job",
                "type": "array",
                "items": {"type": "string"},
            },
        },
    }

    return scheme_dict


def get_reference_list(pipeline, terminal_job: Process) -> dict:
    """Prepares a list of every piece of software used to generate a termianl job

    Args:
        pipeline (:class:`~pipeliner.project_graph.ProjectGraph`): The current pipeline
        terminal_job (:class:`~pipeliner.data_structure.Process`):  The `Process`
            object for the terminal job
    Returns:
        dict: The reference list{job_name: ([programs, used], [references])}
    """

    run_jobs = {}
    upstream = pipeline.get_upstream_network(terminal_job)
    upstream_procs = [os.path.dirname(x[0].name) + "/" for x in upstream]

    for job in upstream_procs:
        jobproc = pipeline.find_process(job)
        jobinfo = new_job_of_type(jobproc.type).jobinfo
        run_jobs[jobproc.name] = jobinfo.programs, jobinfo.references

    return run_jobs
