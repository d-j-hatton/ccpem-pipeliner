#!/usr/bin/env python
import sys
import os
import napari
import argparse
import mrcfile
import numpy as np
import warnings
from pipeliner.jobstar_reader import RelionStarFile
from pipeliner import star_writer
from magicgui import magicgui
from random import shuffle
from gemmi import cif
from matplotlib.image import imsave
from qtpy.QtWidgets import QGraphicsOpacityEffect


class Class2DSelectSession(object):
    """Object that contains the picking session info

    Attributes:
        stack_name (str): Name of the mrcs file containing the class average images

    """

    def __init__(self, stackfile, modelfile, datafile, outdir):
        self.stack_name = stackfile
        self.model_name = modelfile
        self.data_name = datafile
        self.out = outdir
        self.classes = []
        self.selected_classes = set()
        self.total_parts = -1


class ClassAverage(object):
    """Object to contain a single class average

    Attributes:
        class_number (int): The class number, corresponds to the image index in
            the mrc stack
        image_data (np.array): The actual image
        column_data {dict}: The info contained in the class averages star file
            {column_name: data}
        array_center (list): The center point of the image in the current montage
            image
        array_position (list): The indices of the four corners of the image in the
            montage image
        particle_count (int):  The number of particles associated with this class
            average
    """

    def __init__(self, class_number, column_data, part_count):
        self.class_number = class_number
        self.image_data = None
        self.column_data = column_data
        self.array_center = [None, None]
        self.array_position = [None, None, None, None]
        self.particle_count = part_count


def get_arguments():
    """Get all the args from the command line

    Returns:
         parser: The parser object
    """
    parser = argparse.ArgumentParser(description="CCPEM Pipeliner Particle Picker")
    parser._optionals.title = "Arguments"

    parser.add_argument(
        "--stack",
        help="File containing the stack of class averages",
        nargs="?",
        default=None,
        const=None,
        metavar=".mrcs file",
    )
    parser.add_argument(
        "--model",
        help=(
            "2D classification class_model_starfile, run_itxxx_model.star "
            "(RELION 3.x) or run_itxxx_optimiser.star (RELION 4.x)"
        ),
        nargs="?",
        default=None,
        const=None,
        metavar="model/optimiser file",
    )
    parser.add_argument(
        "--data",
        help=(
            "2D classification data file. " "Usually Class2D/jobxxx/run_itxxx_data.star"
        ),
        nargs="?",
        default=None,
        const=None,
        metavar="data file",
    )
    parser.add_argument(
        "--o",
        help="output directory name.  Will be created if it doesn't exist",
        nargs="?",
        default=None,
        const=None,
        metavar="directory name",
    )
    return parser


def enable_widget(widget, enabled=True):
    widget.setEnabled(enabled)
    op = QGraphicsOpacityEffect()
    op.setOpacity(1 if enabled else 0.5)
    widget.setGraphicsEffect(op)


def main(in_args=None):

    # get the args
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    for arg in args.__dict__:
        if args.__dict__[arg] is None:
            parser.print_help()
            print(f"ERROR: Missing argument --{arg}")
            return

    # setup the viewer
    viewer = napari.Viewer(ndisplay=2)
    viewer.grid.enabled = False
    viewer.window.qt_viewer.dockLayerList.setVisible(False)
    viewer.window.qt_viewer.dockLayerControls.setVisible(False)
    buttons_to_hide = ["newLabelsButton", "newShapesButton", "newPointsButton"]
    qctrl = viewer.window.qt_viewer.layerButtons
    for wdg in buttons_to_hide:
        getattr(qctrl, wdg).setVisible(False)
    more_buttons_to_hide = [
        "rollDimsButton",
        "transposeDimsButton",
        "gridViewButton",
        "ndisplayButton",
        "consoleButton",
    ]
    qctrl = viewer.window.qt_viewer.viewerButtons
    for wdg in more_buttons_to_hide:
        getattr(qctrl, wdg).setVisible(False)

    # start the session
    session = Class2DSelectSession(args.stack, args.model, args.data, args.o)
    # read the data file now for particle counting
    parts_file = RelionStarFile(session.data_name)
    # do only the parts block
    parts_block = parts_file.get_block("particles")
    session.total_parts = parts_file.count_block("particles")
    class_loop = parts_block.find_loop("_rlnClassNumber")
    class_pcounts = {}
    for pline in class_loop:
        cl = int(pline)
        if cl not in class_pcounts:
            class_pcounts[cl] = 1
        else:
            class_pcounts[cl] += 1

    @magicgui(
        auto_call=True,
        nsel={"widget_type": "Label", "label": "classes:"},
        nparts={"widget_type": "Label", "label": "particles:"},
    )
    def sel_counter(nsel=f"0/{len(session.classes)}", nparts="0  0.00%"):
        """Displays the numbe of picked classes"""
        pass

    def update_sel_counter():
        """Update the values displayed by the sel counter widget"""
        sel_counter.nsel.value = (
            f"{len(session.selected_classes)}/{len(session.classes)}"
        )
        part_count = 0
        for cl in session.classes:
            if cl.class_number in session.selected_classes:
                part_count += cl.particle_count
        pct = round((float(part_count) / float(session.total_parts)) * 100, 2)
        sel_counter.nparts.value = f"{part_count}  ~{pct}%"

    # make all the class objects from the model file
    classes_model = RelionStarFile(args.model)
    stardata_block = classes_model.get_block("model_classes")
    block_count = classes_model.count_block("model_classes")
    loop = stardata_block.find_loop_item("_rlnReferenceImage").loop
    all_tags = loop.tags[0:]
    metadata = stardata_block.find(all_tags)

    # create a class object for each class and put in the session classlist
    for cl in metadata:
        class_no = int(cl["_rlnReferenceImage"].split("@")[0])
        data_dict = {}
        for tag in all_tags:
            try:
                data_dict[tag] = float(cl[tag])
            except ValueError:
                data_dict[tag] = str(cl[tag])
        session.classes.append(
            ClassAverage(class_no, data_dict, class_pcounts.get(class_no, 0))
        )

    # get the images data
    with mrcfile.open(session.stack_name) as mrcdata:
        class_images = np.array(mrcdata.data, dtype="float32")
    if class_images.shape[0] != block_count:
        raise ValueError(
            f"ERROR: There are {class_images.shape[0]} images in the image stack"
            f" but there are {block_count} classes in the model file, these counts"
            f"need to match!\nImage stack: {session.stack_name}\nModel file: "
            f"{session.model_name}"
        )

    # calculate the number of rows and create target array
    class_size = class_images.shape[1]
    n_rows = class_images.shape[0] // 10
    final_row = class_images.shape[0] % 10 != 0
    target_array = np.zeros(
        [(n_rows + int(final_row)) * class_images.shape[1], 10 * class_images.shape[2]]
    )

    # get a list of the individual images and normalize them
    individual_imgs = [class_images[n, :, :] for n in range(class_images.shape[0])]
    normed_imgs = []
    for n, x in enumerate(individual_imgs):
        normed = np.nan_to_num((x - np.min(x)) / np.ptp(x))
        normed_imgs.append(normed)
        # add the image data to the classaverage object
        session.classes[n].image_data = normed
        session.classes[n].class_number = n + 1

    def make_image_montage(images_list):
        """Make a single array with tiled images

        Args:
            images_list (list): List of  np.arrays containing image data

        Returns:
            array: The images tiles in an array of shape [x, 10]
        """
        xpos, ypos = 0, 0
        for n, img in enumerate(images_list):
            ystart = ypos * class_size
            yend = (ypos + 1) * class_size
            xstart = xpos * class_size
            xend = (xpos + 1) * class_size
            xpos += 1
            class_center = [(ystart + yend) / 2, (xstart + xend) / 2]
            session.classes[n].array_center = class_center
            session.classes[n].array_position = [ystart, yend, xstart, xend]
            if xpos % 10 == 0:
                xpos = 0
                ypos += 1
            target_array[ystart:yend, xstart:xend] = img
        return target_array

    # create initial montage image and empty selection overlay
    target_array = make_image_montage(normed_imgs)
    # suppress annoying napari scale warning
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        montage_image = viewer.add_image(target_array, name="unsorted")
        overlay_image = viewer.add_image(
            np.zeros(target_array.shape),
            name="selected",
            colormap="red",
            blending="additive",
        )

    def update_overlay():
        """Update the overlay showing the selected classes"""
        # reset the selected classes overlay
        print("UPDATE")
        overlay_image.data = np.zeros(target_array.shape)
        for selcl in session.classes:
            if selcl.class_number in session.selected_classes:
                cpos = selcl.array_position
                overlay_image.data[cpos[0] : cpos[1], cpos[2] : cpos[3]] = np.full(
                    [class_size, class_size], 1
                )
        overlay_image.visible = False
        overlay_image.visible = True
        if len(session.selected_classes) > 0:
            enable_widget(save_button.native)

    # look for existing selected classes and load them
    efile = os.path.join(session.out, "selected_class_averages.star")
    if os.path.isfile(efile):
        print(f"Importing {efile}")
        clavs_efile = RelionStarFile(efile)

        # do the optics block
        clavs_block = clavs_efile.get_block("class_averages")
        clavs_loop = clavs_block.find_loop("_rlnReferenceImage")
        for e_clav in clavs_loop:
            print(f"  - {e_clav}")
            clno = int(e_clav.split("@")[0])
            session.selected_classes.add(clno)
        update_overlay()
        update_sel_counter()

    @magicgui(
        call_button="sort classes",
        sort_param={"choices": list(session.classes[0].column_data) + ["RANDOM"]},
        order={
            "widget_type": "RadioButtons",
            "choices": [("Ascending", False), ("Descending", True)],
        },
        sorted_by={"widget_type": "Label", "label": "sorted by:"},
    )
    def sort_classes_button(
        sort_param="_rlnReferenceImage",
        order=True,
        sorted_by=f"class number\n{len(session.classes)} classes",
    ):
        # sort the images and make a new montage
        if sort_param == "RANDOM":
            shuffle(session.classes)
        else:
            session.classes.sort(key=lambda x: x.column_data[sort_param], reverse=order)
        montage_image.data = make_image_montage([x.image_data for x in session.classes])

        # display info on widget
        if sort_param not in ("_rlnReferenceImage", "RANDOM"):
            clmin = min([x.column_data[sort_param] for x in session.classes])
            clmax = max([x.column_data[sort_param] for x in session.classes])
            sort_banner = f"{sort_param.replace('_rln', '')}\nmin: {clmin} max: {clmax}"
        elif sort_param == "_rlnReferenceImage":
            sort_banner = f"class number\n{len(session.classes)} classes"
        else:
            sort_banner = f"RANDOM!! \n{len(session.classes)} classes"
        sort_classes_button.sorted_by.value = sort_banner
        update_overlay()

    @viewer.mouse_drag_callbacks.append
    def update_selected_layer(layer, event):
        # return the class with its center closest to the pick point
        cpos = None
        for classavg in session.classes:
            xd = abs(classavg.array_center[1] - montage_image.position[1])
            yd = abs(classavg.array_center[0] - montage_image.position[0])
            cpos = None
            if xd < 0.5 * class_size and yd < 0.5 * class_size:
                cpos = classavg.array_position
                if classavg.class_number not in session.selected_classes:
                    session.selected_classes.add(classavg.class_number)
                    select_array = np.full([class_size, class_size], 1)
                else:
                    session.selected_classes.remove(classavg.class_number)
                    select_array = np.zeros([class_size, class_size])
                break
        if cpos is None:
            return
        # add the class region to the overlay
        overlay_image.data[cpos[0] : cpos[1], cpos[2] : cpos[3]] = select_array
        update_sel_counter()
        update_overlay()

    @magicgui(call_button="select random classes", layout="horizontal")
    def select_random(number: float = len(session.classes) * 0.25):
        if number > len(session.classes):
            warnings.warn("Too many classes!")
            return
        session.selected_classes.clear()
        shuffed = session.classes.copy()
        shuffle(shuffed)
        session.selected_classes = {x.class_number for x in shuffed[: int(number)]}
        update_overlay()
        update_sel_counter()

    @magicgui(call_button="clear selection")
    def clear_selection():
        """Clear all selected classes"""
        session.selected_classes.clear()
        overlay_image.data = np.zeros(target_array.shape)
        enable_widget(save_button.native, False)
        update_sel_counter()

    @magicgui(call_button="save selected particles")
    def save_button():
        # get the ClassAverage objects for selected classes
        clavs_to_write = []
        for clav in session.classes:
            if clav.class_number in session.selected_classes:
                clavs_to_write.append(clav)

        # save a star file with info about the selected class averages
        clav_starfile = cif.Document()
        clav_block = clav_starfile.add_new_block("class_averages")
        columns = list(clavs_to_write[0].column_data)
        clav_block.init_loop("", columns)
        clav_loop = clav_block.find_loop(columns[0]).get_loop()

        for clav in clavs_to_write:
            clav_loop.add_row([str(clav.column_data[x]) for x in clav.column_data])
        outfile = os.path.join(session.out, "selected_class_averages.star")
        star_writer.write(clav_starfile, outfile)
        print(f"wrote starfile {outfile} with {len(session.selected_classes)} classes")

        # save a starfile with the particles from the selected classes
        # save a starfile with the particles from the selected classes
        # do the optics block
        optics_block = parts_file.get_block("optics")
        optics_loop = optics_block.find_loop("_rlnOpticsGroupName").get_loop()
        optics_tags = optics_loop.tags[0:]
        optics_data = optics_block.find(optics_tags)

        # do the parts block
        parts_loop = parts_block.find_loop("_rlnCoordinateX").get_loop()
        parts_tags = parts_loop.tags[0:]
        parts_data = parts_block.find(parts_tags)

        # write file
        parts_starfile = cif.Document()

        write_opt_block = parts_starfile.add_new_block("optics")
        write_opt_block.init_loop("", optics_tags)
        write_opt_loop = write_opt_block.find_loop(optics_tags[0]).get_loop()
        for opt_row in optics_data:
            write_opt_loop.add_row(opt_row)

        write_parts_block = parts_starfile.add_new_block("particles")
        write_parts_block.init_loop("", parts_tags)
        write_parts_loop = write_parts_block.find_loop(parts_tags[0]).get_loop()
        pcount = 0
        for parts_row in parts_data:
            if int(parts_row["_rlnClassNumber"]) in session.selected_classes:
                write_parts_loop.add_row(parts_row)
                pcount += 1
        out_file = os.path.join(session.out, "selected_particles.star")
        star_writer.write(parts_starfile, out_file)
        print(f"wrote {out_file} with {pcount} particles")

        # save a mrc stack of the selected class averages
        out_data = np.stack(
            [class_images[n - 1, :, :] for n in session.selected_classes]
        )
        out_file = os.path.join(session.out, "selected_class_averages.mrcs")
        out_mrc = mrcfile.new(out_file, overwrite=True)
        out_mrc.set_data(out_data)
        print(f"wrote stack {out_file} with {len(session.selected_classes)} classes")

        # save a montage image of the selected class averages attempts to
        # make it as even as possible IE 9 imgs = [3,3,3], 10 imgs = [5,5],
        # 11 imgs = [4, 4, 3]
        nsel = len(session.selected_classes)
        rangemax = 6 if nsel > 5 else nsel
        rows = {}
        if nsel > 4:
            for n_per_row in range(3, rangemax):
                rows[n_per_row] = nsel % n_per_row
            even = []
            for x in rows:
                if rows[x] == 0:
                    even.append(x)
            if bool(len(even)):
                nprow = max(even)
            else:
                nprow = max(rows, key=rows.get)

            sumrows = 0
            tiles = []
            while sumrows + nprow <= nsel:
                subtile = []
                chunk = clavs_to_write[sumrows : sumrows + nprow]
                for cl in chunk:
                    subtile.append(cl.image_data)
                tiles.append(np.hstack(subtile))
                sumrows += nprow
            if sumrows < nsel:
                subtile = []
                chunk = clavs_to_write[sumrows:]
                for cl in chunk:
                    subtile.append(cl.image_data)
                hsub = np.hstack(subtile)
                padded = np.zeros(tiles[0].shape)
                padded[: hsub.shape[0], : hsub.shape[1]] = hsub
                tiles.append(padded)

            out_array = np.vstack(tiles)
        else:
            out_array = np.hstack([cl.image_data for cl in clavs_to_write])

        out_name = os.path.join(session.out, "selected_classes.png")
        imsave(out_name, out_array, cmap="gray")
        print(f"wrote {out_name} montage with {len(session.selected_classes)} classes ")

    viewer.window.add_dock_widget(sort_classes_button, name="sort class averages")
    viewer.window.add_dock_widget(sel_counter, name="selection info")
    viewer.window.add_dock_widget(select_random, name="random sample")
    viewer.window.add_dock_widget(clear_selection, name="clear selection")
    viewer.window.add_dock_widget(save_button, name="save results")
    enable_widget(save_button.native, False)
    napari.run(max_loop_level=2)


if __name__ == "__main__":
    main()
