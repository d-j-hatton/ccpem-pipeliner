#!/usr/bin/env python
import sys
import os
import argparse
from pipeliner.job_factory import new_job_of_type
from pipeliner import star_writer
from pipeliner.jobstar_reader import JobStar
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (
    QWidget,
    QLineEdit,
    QLabel,
    QPushButton,
    QScrollArea,
    QApplication,
    QHBoxLayout,
    QMainWindow,
    QCheckBox,
    QMessageBox,
    QDoubleSpinBox,
    QFileDialog,
    QGridLayout,
    QComboBox,
)
from PyQt5.QtCore import Qt


def get_arguments():

    parser = argparse.ArgumentParser(
        description="CCPEM-EM Pipeliner Quick Asses job editor"
    )
    parser._optionals.title = "Arguments"

    parser.add_argument(
        "--jobtype",
        "-j",
        help=("What kind of job to edit"),
        nargs="?",
        metavar="job type",
        required=True,
    )
    parser.add_argument(
        "-s",
        "--skip",
        nargs="+",
        help="Parameters to skip (because they are set elsewhere)",
    )

    parser.add_argument(
        "--output",
        "-o",
        help="What to name the output file",
        nargs="?",
        required=True,
    )
    return parser


class JoboptionWidget(object):
    def __init__(self, joboption):
        self.jo = joboption

    def show_helpbox(self):
        """Display the help for a joboption as a message box"""
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(self.jo.label)
        msg.setInformativeText(self.jo.help_text)
        msg.setWindowTitle("Help")
        msg.exec()

    def make_int_slider(self):
        """Make a slide for an integer joboption"""
        label_widg = QLabel(self.jo.label)
        val_widg = QDoubleSpinBox()
        val_widg.setRange(-1, 10000000000)
        val_widg.setDecimals(0)
        val_widg.setSingleStep(self.jo.step_value)
        val_widg.setValue(float(self.jo.value))
        helpbut = QPushButton("?")
        return [label_widg, val_widg, helpbut, val_widg]

    def make_float_slider(self):
        """Make a slider for a float joboption"""
        label_widg = QLabel(self.jo.label)
        val_widg = QDoubleSpinBox()
        val_widg.setRange(-0.00001, 10000000000)
        val_widg.setDecimals(4)
        val_widg.setSingleStep(self.jo.step_value)
        val_widg.setValue(float(self.jo.value))
        helpbut = QPushButton("?")
        return [label_widg, val_widg, helpbut, val_widg]

    def make_boolean(self):
        """Make a checkbox for a boolean joboption"""
        label_widg = QLabel(self.jo.label)
        checkbox_widg = QCheckBox()
        if self.jo.value == "Yes":
            checkbox_widg.setCheckState(Qt.Checked)
        helpbut = QPushButton("?")
        return [label_widg, checkbox_widg, helpbut, checkbox_widg]

    def make_filename(self):
        """Make a file selector for a file input joboption"""

        def get_file(widg, jo, fn_sel):
            sel_file = QFileDialog.getOpenFileName(
                widg,
                "Select file",
                jo.directory,
                jo.pattern,
            )
            fn_sel.setText(sel_file[0])

        fn_widg = QWidget()
        layout = QHBoxLayout()
        fn_sel = QLineEdit()
        fn_sel.setText(self.jo.value)
        layout.addWidget(fn_sel)
        sel_button = QPushButton("Select")
        sel_button.clicked.connect(lambda: get_file(fn_widg, self.jo, fn_sel))
        layout.addWidget(sel_button)

        fn_widg.setLayout(layout)

        helpbut = QPushButton("?")
        label_widg = QLabel(self.jo.label)
        return [label_widg, fn_widg, helpbut, fn_sel]

    def make_textbox(self):
        """Make a text input for a text joboption"""
        label_widg = QLabel(self.jo.label)
        fn_widg = QLineEdit()
        fn_widg.setText(self.jo.value)
        helpbut = QPushButton("?")
        return [label_widg, fn_widg, helpbut, fn_widg]

    def make_pulldown(self):
        """Make a pulldown for a 'radio' job option"""
        label_widg = QLabel(self.jo.label)
        pd_widg = QComboBox()
        pd_widg.addItems(self.jo.choices)
        helpbut = QPushButton("?")
        return [label_widg, pd_widg, helpbut, pd_widg]


class JobGui(QWidget):
    """The main object for the job GUI"""

    def __init__(self, jobtype, output_file):
        super().__init__()
        self.layout = QGridLayout()
        self.job = new_job_of_type(jobtype)
        # read the existing file and update if necessary
        if os.path.isfile(output_file):
            prev = JobStar(output_file).get_all_options()
            for jo in self.job.joboptions:
                if prev.get(jo) is not None:
                    self.job.joboptions[jo].value = prev[jo]
        self.jobop_widgets = {}
        self.jobtype = jobtype


class MainWindow(QMainWindow):
    def __init__(self, jobtype, output_file, skip_params):
        super().__init__()
        self.jobtype = jobtype
        self.output_file = output_file
        self.skip_params = skip_params
        self.initUI()

    def initUI(self):
        jobgui = JobGui(self.jobtype, self.output_file)
        jobop_helps = {}
        jos = jobgui.job.joboptions
        for jo in jos:
            jobobj = JoboptionWidget(jobgui.job.joboptions[jo])

            if jos[jo].joboption_type == "INT_SLIDER":
                jobgui.jobop_widgets[jo] = jobobj.make_int_slider()
                jobop_helps[jo] = jobobj.show_helpbox

            if jos[jo].joboption_type == "FLOAT_SLIDER":
                jobgui.jobop_widgets[jo] = jobobj.make_float_slider()
                jobop_helps[jo] = jobobj.show_helpbox

            elif jos[jo].joboption_type == "BOOLEAN":
                jobgui.jobop_widgets[jo] = JoboptionWidget(
                    jobgui.job.joboptions[jo]
                ).make_boolean()
                jobop_helps[jo] = jobobj.show_helpbox

            elif jos[jo].joboption_type == "FILENAME":
                jobgui.jobop_widgets[jo] = JoboptionWidget(
                    jobgui.job.joboptions[jo]
                ).make_filename()
                jobop_helps[jo] = jobobj.show_helpbox

            # TODO: the same as filename for now, The input node joboption type should
            # probably be removed and replaced with FILENAME type.
            elif jos[jo].joboption_type == "INPUTNODE":
                jobgui.jobop_widgets[jo] = JoboptionWidget(
                    jobgui.job.joboptions[jo]
                ).make_filename()
                jobop_helps[jo] = jobobj.show_helpbox

            elif jos[jo].joboption_type == "TEXTBOX":
                jobgui.jobop_widgets[jo] = JoboptionWidget(
                    jobgui.job.joboptions[jo]
                ).make_textbox()
                jobop_helps[jo] = jobobj.show_helpbox

            elif jos[jo].joboption_type == "RADIO":
                jobgui.jobop_widgets[jo] = JoboptionWidget(
                    jobgui.job.joboptions[jo]
                ).make_pulldown()
                jobop_helps[jo] = jobobj.show_helpbox

        gcount = 2

        for jo_name in jobgui.job.joboptions:
            if jo_name not in self.skip_params:
                label, disp, helpbut = jobgui.jobop_widgets.get(jo_name)[:3]
                jobgui.layout.addWidget(label, gcount, 0)
                jobgui.layout.addWidget(disp, gcount, 1)
                jobgui.layout.addWidget(helpbut, gcount, 2)
                helpbut.clicked.connect(jobop_helps[jo_name])
                gcount += 1

        main_label = QLabel(f" Edit {self.jobtype} job parameters")
        main_label.setFont(QFont("Arial", 20))
        jobgui.layout.addWidget(main_label, 0, 0, 1, 3, alignment=Qt.AlignCenter)

        def save_jobstar():
            jobop_vals = {"_rlnJobTypeLabel": jobgui.jobtype, "_rlnJobIsContinue": "0"}
            for jobop in jobgui.job.joboptions:
                the_widg = jobgui.jobop_widgets[jobop][3]
                if type(the_widg) == QCheckBox:
                    bool_val = the_widg.isChecked()
                    jobop_vals[jobop] = "Yes" if bool_val else "No"
                elif type(the_widg) == QComboBox:
                    jobop_vals[jobop] = str(the_widg.currentText())
                else:
                    jobop_vals[jobop] = jobgui.jobop_widgets[jobop][3].text()
            # write the file
            out_filename = self.output_file
            star_writer.write_jobstar(jobop_vals, out_filename)
            sys.exit(f"Saved {out_filename}")

        def goodbye():
            sys.exit()

        jobstar_button = QPushButton("Save and Exit")
        jobstar_button.clicked.connect(save_jobstar)

        quit_button = QPushButton("Cancel")
        quit_button.clicked.connect(goodbye)

        jobgui.layout.addWidget(quit_button, gcount + 1, 2)
        jobgui.layout.addWidget(jobstar_button, gcount + 1, 1)
        jobgui.setLayout(jobgui.layout)

        self.scroll = (
            QScrollArea()
        )  # Scroll Area which contains the widgets, set as the centralWidget
        self.widget = jobgui  # Widget that contains the collection of Vertical Box

        # Scroll Area Properties
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.setCentralWidget(self.scroll)

        self.setGeometry(600, 100, 900, 800)
        self.setWindowTitle("Make job.star file")
        self.show()

        return


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    skip_params = [] if args.skip is None else args.skip

    app = QApplication(sys.argv)
    MainWindow(args.jobtype, args.output, skip_params)
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
