#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from pipeliner.display_tools import ResultsDisplayPending


"""The ProjectGraph handles all the management of the pipeline keeping track
of which jobs have been run, their statuses, and what files are input and outputs
to the various jobs"""

from glob import glob
import os
import shutil
import stat
import sys
import time
import json
import subprocess
from gemmi import cif
from typing import Optional, List, Tuple

from pipeliner import star_keys
from pipeliner import star_writer
from pipeliner.jobstar_reader import (
    OutputNodeStar,
    ExportStar,
    StarfileCheck,
    modify_jobstar,
)
from pipeliner.metadata_tools import get_job_metadata
from pipeliner.job_factory import new_job_of_type, read_job, active_job_from_proc
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.data_structure import (
    Process,
    Node,
    NODES_DIR,
    ABORT_FILE,
    FAIL_FILE,
    SUCCESS_FILE,
    JOBSTATUS_SCHED,
    JOBSTATUS_RUN,
    JOBSTATUS_ABORT,
    JOBSTATUS_FAIL,
    JOBSTATUS_SUCCESS,
    PROJECT_FILE,
    JOBINFO_FILE,
    CLEANUP_LOG,
)
from pipeliner.utils import (
    check_for_illegal_symbols,
    date_time_tag,
    decompose_pipeline_filename,
    touch,
)
from pipeliner.display_tools import (
    ResultsDisplayMontage,
    ResultsDisplayImage,
    ResultsDisplayGraph,
    ResultsDisplayHistogram,
    ResultsDisplayText,
    ResultsDisplayMapModel,
    ResultsDisplayTable,
    ResultsDisplayRvapi,
)


def update_jobinfo_file(
    current_proc: Process,
    action: str,
    comment: Optional[str] = None,
    command_list: Optional[list] = None,
):
    """Update the file in the jobdir that stores info about the job

    Args:
        current_proc (:class:`~pipeliner.data_structure.Process`): Process object
            for the job being operated on
        action (str): what action was performed on the job IE: Ran, Scheduled,
            Cleaned up
        comment (str): Comment to append to the job's comments list
        command_list (list): Commands that were run. Generally `None` if action
            was any other than Run or Schedule
    """
    # read current jobinfo file if it exists and update
    jinfo = os.path.join(current_proc.name, JOBINFO_FILE)
    jobinfo: dict = {
        "job directory": current_proc.name,
        "comments": [comment] if comment is not None else [],
        "rank": None,
        "created": date_time_tag(),
        "history": {},
        "command history": {},
    }
    if os.path.isfile(jinfo):
        try:
            with open(jinfo) as ji:
                jobinfo = json.load(ji)
            if comment is not None:
                jobinfo["comments"].append(comment)
        except json.decoder.JSONDecodeError:
            pass
    dtt = date_time_tag()
    hist_memo = [action]

    # don't accidentially overwrite previous history.
    hcheck = jobinfo["history"].get(dtt)
    if hcheck:
        dtt += ".1"

    # update the history
    jobinfo["history"][dtt] = " ".join(hist_memo)
    if command_list is not None:
        jobinfo["command history"][dtt] = [" ".join(x) for x in command_list[1]]
    with open(jinfo, "w") as ji:
        json.dump(jobinfo, ji, indent=2)


class ProjectGraph(object):
    """The main ProjectGraph object is used for manipulating the pipeline

    Attributes:
        name (str): The name of the pipeline. `_pipeline.star` is added to the name to
            generate the pipeline file name
        node_list (list): A :class:`~pipeliner.data_structure.Node` object for
            every file that is an input or output for a job in the project
        process_list (list): A :class:`~pipeliner.data_structure.Process` object for
            each job in the project
        job_counter (int): The number of the *next* job in the project IE: If there are
            10 jobs in a project job_counter is 11
        do_read_only (bool): If this current instatiation of the object should be able
            to make changes to the pipeline
    """

    def __init__(self, name: str = "default", do_read_only: bool = False):
        """Create a ProjectGraph object
        Args:
            name (str): The name for the project. It is best to let it default to
                "default"
            do_ready_only (bool): Should this instance be read only?

        """
        self.name = name
        self.node_list: List[Node] = []
        self.process_list: List[Process] = []

        # TODO correct this to 0, it's currently at 1 for compatibility reasons
        # For now, think of this as the number of the next job. But in the long run it's
        # probably less confusing to count the number of jobs already in the pipeline.
        self.job_counter = 1
        self.do_read_only = do_read_only

    def clear(self):
        """Clear the node and process lists and set the job counter to 1"""
        self.node_list = []
        self.process_list = []
        self.job_counter = 1

    def set_name(self, name: str, new_lock: bool = True, overwrite: bool = False):
        """Change the name of the pipeline file

        Unlocks the old pipeline when the new one is created.  This should really not
        be used as there is no reason to be changing the name of the pipeline

        Args:
            name (str): The new name
            new_lock (bool): Should the new pipeline be locked?
            overwrite (bool): Should the old pipeline be removed?

        Raises:
            ValueError: If an attempt is made to change the name to on that already
                exists
        """
        old_name = self.name

        # write a copy of the new pipeline
        if not os.path.isfile(name + "_pipeline.star"):
            # write it out in the old style to create it
            self.name = name
            self.write(lockfile_expected=False)
            self.read()
            # get rid of the old lock file
            old_lockfile = ".relion_lock/lock_" + old_name + "_pipeline.star"
            new_lockfile = ".relion_lock/lock_" + name + "_pipeline.star"
            if new_lock:
                if os.path.isfile(old_lockfile):
                    shutil.move(old_lockfile, new_lockfile)
                else:
                    self.create_lock()

            if os.path.isfile(old_lockfile):
                os.remove(old_lockfile)

        elif not overwrite:
            self.remove_lock()
            raise ValueError(
                "ERROR: Can't change pipeline name because pipeline file "
                f"{name}_pipeline.star already exists"
            )

    def create_lock(self, wait_time: int = 2):
        """Lock the pipeline

        Creates a directory called .relion_lock and the file
        .relion_lock/lock_<pipeline_name>_pipeline.star.  The pipeline
        cannot be edited when this file is present.

        Args:
            wait_time (int): The number of minutes to continue trying to make
                the lock directory/file if a problem is encountered.
        Raises:
            RuntimeError: If a permission error is encountered trying to create or
                read the .relion_lock directory
            RuntimeError: If the lock file has not appeared after <wait_time> minutes

        """
        dir_lock = ".relion_lock"
        fn_lock = ".relion_lock/lock_" + self.name + "_pipeline.star"

        try:
            os.mkdir(dir_lock)
        except PermissionError as ex:
            raise RuntimeError(
                f"ERROR: ProjectGraph.read() cannot create a lock directory {dir_lock}."
                " You don't have write permission to this project. If you want to"
                " look at other's project directory (but run nothing there), please"
                " start RELION with --readonly"
            ) from ex
        except FileExistsError:
            pass
        iwait = 0
        touch(fn_lock)
        if not os.path.isfile(fn_lock):
            print("WARNING: Having trouble writing lock file")
            sys.stdout.write("Still trying...")
            sys.stdout.flush()
        while not os.path.isfile(fn_lock):
            # If the lock exists: wait 3 seconds and try again
            # First time round, print a warning message
            time.sleep(3)
            iwait += 3
            sys.stdout.write(".")
            sys.stdout.flush()
            if iwait > wait_time * 60:  # faster for testing
                raise RuntimeError(
                    f"ERROR: ProjectGraph.read() has waited for {wait_time} minutes for"
                    " lock file to appear, but it didn't. This should not happen. Is"
                    " something wrong with the disk access?"
                )

    def check_lock(self, lock_expected: bool, wait_time: int = 2):
        """Checks to see if a lock file exists for the pipeline


        Args:
            lock_expected (bool): Is a lock file expected to exist?
            wait_time (int): The number of minutes to continue trying whilst waiting
                for a lock file to appear/disappear based of if one is expected

        Raises:
            RuntimeError: If no lock is expected, but a lock file is still found after
                waiting <wait_time> minutes for it to disappear
            RuntimeError: If a lock file is expected but not found after
                waiting <wait_time> minutes for it to appear
        """
        fn_lock = ".relion_lock/lock_" + self.name + "_pipeline.star"
        if not lock_expected and os.path.isfile(fn_lock):
            print(
                f"\nWARNING: trying to read pipeline.star, but directory {fn_lock}"
                " exists (which protects against simultaneous writing by"
                " multiple instances of the GUI)"
            )
            sys.stdout.write("Waiting...")
            iwait = 0
            while os.path.isfile(fn_lock):
                # If the lock exists: wait 3 seconds and try again
                # First time round, print a warning message
                time.sleep(3)
                iwait += 3
                sys.stdout.write(".")
                sys.stdout.flush()
                if iwait > wait_time * 60:  # faster for testing
                    raise RuntimeError(
                        f"ERROR: ProjectGraph.read() has waited for {wait_time} "
                        " minutes for the lock directory to disappear. You may "
                        f"want to manually remove the file: {fn_lock}"
                    )

        if lock_expected and not os.path.isfile(fn_lock):
            print(
                f"\nWARNING: was expecting a file called {fn_lock} but it isn't"
                f" there. Will wait for {wait_time} minutes to see if it"
                " appears"
            )
            sys.stdout.write("Waiting...")
            iwait = 0
            while not os.path.isfile(fn_lock):
                # If the lock exists: wait 3 seconds and try again
                # First time round, print a warning message

                time.sleep(3)
                iwait += 3
                sys.stdout.write(".")
                sys.stdout.flush()
                if iwait > wait_time * 60:
                    raise RuntimeError(
                        f"\nERROR: ProjectGraph.read() has waited for {wait_time}"
                        f" minutes for lock file {fn_lock} to appear, but it doesn't."
                        " This should not happen. Is something wrong with disk access?"
                    )

    def remove_lock(self):
        """Remove the lock file for the pipeline"""
        dir_lock = ".relion_lock"
        fn_lock = ".relion_lock/lock_" + self.name + "_pipeline.star"

        if not os.path.isfile(fn_lock):
            print(
                f"ERROR: PipeLine::write was expecting a file called {fn_lock} but it "
                "is no longer there."
            )

        try:
            os.remove(fn_lock)
        except Exception as e:
            print(
                "ERROR: PipelineGraph.write() reported error in removing file"
                f" {fn_lock}\n{e}"
            )

        try:
            os.rmdir(dir_lock)
        except Exception as e:
            print(
                "ERROR: PipelineGraph.write() reported error in removing directory "
                f"{dir_lock}:\n{e}"
            )

    def update_lock_message(self, lock_message: str):
        """Updates the contents of the lockfile for the pipeline

        This enables the user to see which process has locked the pipeline
        """
        fn_lock = ".relion_lock/lock_" + self.name + "_pipeline.star"
        with open(fn_lock, "w") as lockfile:
            lockfile.write(lock_message)

    def get_pipeline_filename(self) -> str:
        """Get the name of the pipeline file

        Returns:
            str: The name of the pipeline file usually 'default_pipeline.star'
        """
        return self.name + "_pipeline.star"

    def is_empty(self) -> bool:
        """Tests of the pipeline is empty

        Returns:
            bool: ``True`` if there are no jobs in the pipeline
        """
        if self.job_counter <= 1:
            return True
        return False

    def find_node(self, name: str) -> Optional[Node]:
        """Retrieve the :class:`~pipeliner.data_structure.Node` object for a file

        Args:
            name (str): The name of the file to get the node for

        Returns:
            :class:`~pipeliner.data_structure.Node`: The files's `Node` object.
            ``None`` if the file is not found.
        """
        found = None
        for node in self.node_list:
            if node.name == name:
                found = node
        return found

    def find_process(self, name_or_alias: str) -> Optional[Process]:
        """Retrieve the :class:`~pipeliner.data_structure.Process` object for a job
        in the pipeline

        Args:
            name_or_alias (str): The job name or its alias

        Returns:
            :class:`pipeliner.data_structre.Process`: The job's `Process`

        Raises:
            RuntimeError: If multiple processes with the same name are found
        """
        found = [
            x
            for x in self.process_list
            if (x.name == name_or_alias or x.alias == name_or_alias)
        ]
        if len(found) > 1:
            raise RuntimeError(
                "ERROR: find_process() found multiple processes with the name/alias "
                f"{name_or_alias}.  This should not happen!"
            )
        try:
            return found[0]
        except IndexError:
            return None

    def add_node(self, node: Node, touch_if_not_exists: bool = False) -> Optional[Node]:
        """Add a :class:`~pipeliner.data_structure.Node` to the pipeline

        A node is only added if it doesn't already exist in the
        pipeline, so if a node is used as an input the keywords
        from the process that wrote the node will overrule any
        added in the node's definition from the process that used
        it as input

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to add
            touch_if_not_exists (bool): If the file for the node does not exist
                should it be created?

        Returns:
            :class:`pipeline.data_structure.Node`: The Node that was added.
            If this node already existed, returns the existing copy

        Raises:
            RuntimeError: If the node name is empty

        """
        if node.name == "":
            raise RuntimeError(
                "Pipeline::addNode: Adding an emtpy nodename. "
                "Did you fill in all Node names correctly?"
            )
        fn_node = node.name
        for proc in self.process_list:
            if proc.alias is not None and proc.alias in fn_node:
                fn_node = proc.name + fn_node.replace(proc.alias, "")
                node.name = fn_node
                break

        found = None
        for n in self.node_list:
            if n.name == node.name:
                found = n
                break
        if found is None:
            self.node_list.append(node)
            found = node
        self.touch_temp_node_file(node, touch_if_not_exists)
        return found

    def get_node_name(self, node: Node) -> str:
        """Get the relative path of a node file with its alias if it exists

        This returns the relative path (which is the same as the file name)
        unless the job that created the node has an alias in which case it
        returns the file path with the alias instead of <jobtype>/jobxxx/

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to get the name for

        Returns:
            str: The relative path of the file to node points to
            with the job's alias if applicable
        """
        if node.output_from_process is not None:
            fn_alias = node.output_from_process.alias
        else:
            fn_alias = None

        if fn_alias is not None:
            # make sure alias ends with a slash
            if fn_alias[-1] != "/":
                fn_alias += "/"
            fn_post = decompose_pipeline_filename(node.name)[2]
            fnt = fn_alias + fn_post
        else:
            fnt = node.name
        return fnt

    def touch_temp_node_file(self, node: Node, touch_if_not_exists: bool) -> bool:
        """Create a placeholder file for a node that will be created later

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to create the
                file for
            touch_if_not_exists (bool): Should the file be created if it does not
                already exist

        Returns:
            bool: ``True`` if a file was created or it already existed
            ``False`` if no file was created.

        """
        if self.do_read_only:
            return False
        fnt = self.get_node_name(node)
        if os.path.isfile(node.name) or touch_if_not_exists:
            fn_type = str(node.type).split(".")[0] + "/"
            mynode = NODES_DIR + fn_type + fnt
            mydir = os.path.dirname(mynode)
            if not os.path.isdir(mydir):
                os.makedirs(mydir)
            touch(mynode)
            return True
        return False

    def touch_temp_node_files(self, process: Process) -> bool:
        """Create placeholde files for all nodes in a
        :class:`~pipeliner.data_structure.Process`

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The Process to create
                the files for

        Returns:
            bool: ``True`` if files were created, otherwise ``False``
        """
        if self.do_read_only:
            return False
        touch_if_not_exists = False
        if process.status == JOBSTATUS_SCHED:
            touch_if_not_exists = True

        for node in process.output_nodes:
            self.touch_temp_node_file(node, touch_if_not_exists)
        return True

    def delete_temp_node_file(self, node: Node) -> bool:
        """Remove files associated with a :class:`~pipeliner.data_structure.Node`

        Also removes the directory if it is empty

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to remove the
                file for

        Returns:
            bool: ``True`` if files were deleted, Otherwise ``False``
        """
        if self.do_read_only:
            return False
        fnt = self.get_node_name(node)
        # remove the node
        if os.path.isfile(node.name):
            fn_type = str(node.type).split(".")[0] + "/"
            mynode = NODES_DIR + fn_type + fnt
            if os.path.isfile(mynode):
                os.remove(mynode)
            # also remove the directory if empty
            mydir = os.path.dirname(mynode)
            if os.path.isdir(mydir):
                if len(os.listdir(mydir)) == 0:
                    shutil.rmtree(mydir)
            return True
        return False

    def delete_temp_node_files(self, process: Process) -> bool:
        """Delete all the files for the nodes in a specific
        :class:`~pipeliner.data_structure.Process`

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The Process to create
                the files for

        Returns:
            bool: ``True`` if files were removed, otherwise ``False``
        """
        if self.do_read_only:
            return False
        did_del = False
        for node in process.output_nodes:
            did_del = self.delete_temp_node_file(node)
        if did_del:
            return True
        return False

    def add_new_input_edge(self, node: Node, process: Process):
        """Add a :class:`~pipeliner.data_structure.Node` to a
        :class:`~pipeliner.data_structure.Process` as in input

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to add
            process (:class:`~pipeliner.data_structure.Process`): The Process to add
                the Node to
        """
        # 1. Check whether Node with that name already exists in the Node list
        newnode = self.add_node(node)  # if new its added to the list

        # 2. Set the input_for_process in the inputForProcessList of this Node but only
        # if it doesn't yet exist
        found = None
        if newnode:
            for proc in newnode.input_for_processes_list:
                if proc == process:
                    found = proc
                    break
        if found is None and newnode:
            newnode.input_for_processes_list.append(process)
            process.input_nodes.append(newnode)
        for proc in self.process_list:
            if newnode and newnode is not node:
                # Previously unobserved node. Check whether it came from an old process.
                nodename = newnode.name
                if proc.name in nodename:
                    proc.output_nodes.append(newnode)
                    newnode.output_from_process = proc
                    break

    def add_new_output_edge(self, process: Process, node: Node, mini: bool = False):
        """Add a :class:`~pipeliner.data_structure.Node` to a
        :class:`~pipeliner.data_structure.Process` as in output

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to add
            process (:class:`~pipeliner.data_structure.Process`): The Process to add
                the Node to
        """

        # 1. Check whether Node with that name already exists in the node_list
        # Touch .Nodes entries even if they don't exist for scheduled jobs
        touch_if_not_exist = process.status == JOBSTATUS_SCHED

        # 2. Set the output_from_process of this Node
        node.output_from_process = process
        newnode = self.add_node(node, touch_if_not_exist)

        # 3. Only for new Nodes, add this Node to the outputNodeList of myProcess
        if not mini:
            if newnode == node:
                process.output_nodes.append(newnode)

    def add_new_process(
        self, process: Process, do_overwrite: bool
    ) -> Optional[Process]:
        """Add a :class:`~pipeliner.data_structure.Process` to the pipeline

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The `Process` to add
            do_overwrite (bool): If the process already exists should it be
                overwritten?

        Returns:
            (:class:`~pipeliner.data_structure.Process`): The `Process` that was added
            or the existing `Process` if it already existed

        Raises:
            RuntimeError: If the `Process` already exists and overwrite is ``False``
        """

        found = None
        for proc in self.process_list:
            if proc.name == process.name:
                found = proc
                proc.status = process.status
                break
        if found is None:
            self.process_list.append(process)
            self.job_counter += 1
        elif not do_overwrite:
            raise RuntimeError(
                "project_graph.add_new_process(): trying to add existing Process "
                "to the pipeline while overwriting is not allowed."
            )
        return found

    def add_job(self, job: PipelinerJob, as_status: str, do_overwrite: bool) -> Process:
        """Add a job to the pipeline

        Adds the :class:`~pipeliner.data_structure.Process` for the job, a
        :class:`~pipeliner.data_structure.Node` for each of its
        input and output files, and writes a mini-pipeline containing
        just that job

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job to add.
            as_status (str): The status of the job in the pipeline
            do_overwrite (bool): If the job already exists, should it be overwritten?

        Returns:
            :class:`~pipeliner.data_structure.Process`: The `Process` for the new job
        """
        # Also write a mini-pipeline in the output directory
        mini_pipeline = ProjectGraph(name="mini")
        mini_pipeline.set_name(job.output_name + "job", new_lock=False, overwrite=True)

        # Add process to the processList of the pipeline
        process = Process(job.output_name, job.PROCESS_NAME, as_status)
        newprocess = self.add_new_process(process, do_overwrite)
        mini_pipeline.add_new_process(process, True)
        if newprocess is not None:
            process = newprocess
        # Add all input nodes
        for node in job.input_nodes:
            self.add_new_input_edge(node, process)
            mini_pipeline.add_new_input_edge(node, mini_pipeline.process_list[0])

        # Add all output nodes
        for node in job.output_nodes:
            self.add_new_output_edge(process, node)
            mini_pipeline.add_new_output_edge(
                mini_pipeline.process_list[0], node, mini=True
            )
        # write the mini-pipeline
        mini_pipeline.write(lockfile_expected=False)
        return process

    def read(self, do_lock: bool = False, lock_wait: int = 2) -> StarfileCheck:
        """Read the pipeline

        Args:
            do_lock (bool): Should the pipeline be locked upon reading
            lock_wait (int): If the pipeline is unable to be locked continue
                trying for this many minutes

        Returns:
            :class:`pipeliner.jobstar_reader.StarfileCheck`: For the pipeline file

        Raises:
            AttributeError: If the pipeline file is not found
        """
        # check for a lock - create on is it doesn't exist
        if do_lock and not self.do_read_only:
            self.check_lock(False, wait_time=lock_wait)
            self.create_lock(wait_time=lock_wait)

        pipeline_fname = self.get_pipeline_filename()
        if not os.path.isfile(pipeline_fname):
            raise AttributeError(f"ERROR: Pipeline file {pipeline_fname} not found")

        self.clear()

        # do validation checks on the pipeline
        pipeline_file = StarfileCheck(pipeline_fname, is_pipeline=True)
        doc = pipeline_file.cifdoc

        node_suffixes = star_keys.NODE_SUFFIXES
        proc_suffixes = star_keys.PROCESS_SUFFIXES

        # read jobCounter
        job_count = int(
            doc.find_block(star_keys.GENERAL_BLOCK).find_value(star_keys.JOB_COUNTER)
        )
        self.job_counter = int(job_count)

        # read node and process blocks
        node_block = doc.find_block(star_keys.NODE_BLOCK)
        proc_block = doc.find_block(star_keys.PROCESS_BLOCK)
        if node_block is not None:
            node_table = node_block.find(star_keys.NODE_PREFIX, node_suffixes)

            # write the nodelist
            for node_row in node_table:
                self.node_list.append(Node(*node_row))

        # get the processes
        if proc_block is not None:
            process_table = proc_block.find(star_keys.PROCESS_PREFIX, proc_suffixes)
            # write the process list
            for process_row in process_table:
                name = process_row[0]
                ptype = process_row[2]  # updated
                status = process_row[3]  # updated
                alias = process_row[1]
                if alias == "None":
                    alias = None
                self.process_list.append(Process(name, ptype, status, alias))

        # read input edges
        in_edge_block = doc.find_block(star_keys.INPUT_EDGE_BLOCK)
        if in_edge_block is not None:
            input_edge_table = in_edge_block.find(
                star_keys.INPUT_EDGE_PREFIX, star_keys.INPUT_EDGE_SUFFIXES
            )
            for input_edge_row in input_edge_table:
                node = self.find_node(input_edge_row[0])
                process = self.find_process(input_edge_row[1])
                if node is None:
                    print(
                        "WARNING: creating input edge failed - cannot find "
                        f"input node {input_edge_row[0]} for process with "
                        f"name {input_edge_row[1]}"
                    )

                elif process is None:
                    print(
                        "WARNING: creating input edge failed - cannot find parent "
                        f"process with name {input_edge_row[1]} for node "
                        f"{input_edge_row[0]}:"
                    )
                else:
                    node.input_for_processes_list.append(process)
                    process.input_nodes.append(node)

        # set out edges
        out_edge_block = doc.find_block(star_keys.OUTPUT_EDGE_BLOCK)
        if out_edge_block is not None:
            output_edge_table = out_edge_block.find(
                star_keys.OUTPUT_EDGE_PREFIX, star_keys.OUTPUT_EDGE_SUFFIXES
            )
            for output_edge_row in output_edge_table:
                process = self.find_process(output_edge_row[0])
                node = self.find_node(output_edge_row[1])
                if node is None:
                    print(
                        "WARNING: creating out edge failed - cannot find child "
                        f"node {output_edge_row[1]} from process with name "
                        f"{output_edge_row[0]}"
                    )
                elif process is None:
                    print(
                        "WARNING: creating output edge failed - cannot find parent "
                        f"process with name {output_edge_row[0]} for node "
                        f"{output_edge_row[1]}:"
                    )
                else:
                    process.output_nodes.append(node)
                    node.output_from_process = process

        # for testing only
        return pipeline_file

    def write(
        self,
        edges: Optional[Tuple[List[Tuple[str, str]], List[Tuple[str, str]]]] = None,
        lockfile_expected: bool = True,
        lock_wait: int = 2,
    ):
        """Write the pipeline file from the `ProjectGraph` object

        If the 'ProjectGraph' is locked it will be unlocked after writing is finished

        Args:
            edges (tuple): The pipeline edges as

                [0] A list of tuples (process name, input file)

                [1] A list of tuples (process name, output file)

                If this is left as ``None`` the edges of the current pipeline are used
            lockfile_expected (bool): Does the pipeline expect to be locked?
            lock_wait (int): If the locking status of the pipeline is not as expected
                 wait this many minutes for it to resolve
        """
        # Write will always recieve an new stye pipeline from read
        # pipeline_fname = self.get_pipeline_filename()

        node_suffixes = star_keys.NODE_SUFFIXES
        proc_suffixes = star_keys.PROCESS_SUFFIXES

        # make sure the lock file exists as expected
        self.check_lock(lockfile_expected, lock_wait)

        # Prepare a new Gemmi CIF document to hold the output document structure
        doc = cif.Document()

        # Add general block with job counter
        general_block = doc.add_new_block(star_keys.GENERAL_BLOCK)
        general_block.set_pair(star_keys.JOB_COUNTER, str(self.job_counter))

        # Add processes, while keeping track of input and output edges
        if edges is None:
            input_edges, output_edges = self.get_pipeline_edges()
        else:
            input_edges, output_edges = edges[0], edges[1]
        if len(self.process_list) > 0:
            process_block = doc.add_new_block(star_keys.PROCESS_BLOCK)
            process_loop = process_block.init_loop(
                star_keys.PROCESS_PREFIX, proc_suffixes
            )
            for process in self.process_list:
                process_loop.add_row(
                    [
                        process.name,
                        str(process.alias),
                        str(process.type),
                        str(process.status),
                    ]
                )

        # Add nodes
        # TODO: check consistency of input and output edges?
        if len(self.node_list) > 0:
            node_block = doc.add_new_block(star_keys.NODE_BLOCK)
            node_loop = node_block.init_loop(star_keys.NODE_PREFIX, node_suffixes)
            for node in self.node_list:
                node_loop.add_row([node.name, str(node.type)])

        # Add input edges
        if len(input_edges) > 0:
            input_edge_block = doc.add_new_block(star_keys.INPUT_EDGE_BLOCK)
            input_edge_loop = input_edge_block.init_loop(
                star_keys.INPUT_EDGE_PREFIX, star_keys.INPUT_EDGE_SUFFIXES
            )
            for input_edge in input_edges:
                input_edge_loop.add_row(input_edge)

        # Add output edges
        if len(output_edges) > 0:
            output_edge_block = doc.add_new_block(star_keys.OUTPUT_EDGE_BLOCK)
            output_edge_loop = output_edge_block.init_loop(
                star_keys.OUTPUT_EDGE_PREFIX, star_keys.OUTPUT_EDGE_SUFFIXES
            )
            for output_edge in output_edges:
                output_edge_loop.add_row(output_edge)

        # Write the file
        pipeline_fname = self.get_pipeline_filename()
        star_writer.write(doc, pipeline_fname)

        # remove the lock file
        if lockfile_expected:
            self.remove_lock()

    def find_immediate_child_processes(self, process: Process) -> list:
        """Find just the immediate child processes of a process

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The process to
                find children for

        Returns:
            list: The `Process` object for each job connected to the input `Process`
        """
        children = []
        parent_proc = process.name
        for proc in self.process_list:
            innodes = [os.path.dirname(x.name) + "/" for x in proc.input_nodes]
            if parent_proc in innodes:
                children.append(proc)
        return children

    def remake_node_directory(self):
        """Erase and rewrite RELION's .Nodes directory"""
        if self.do_read_only:
            return False

        # clear existing directory
        if os.path.isdir(NODES_DIR):
            shutil.rmtree(NODES_DIR)
        os.makedirs(NODES_DIR)

        # remake nodes
        for node in self.node_list:
            myproc = node.output_from_process
            if myproc not in self.process_list:
                touch_if_not_exists = False
            else:
                touch_if_not_exists = bool(myproc.status == JOBSTATUS_SCHED)

            self.touch_temp_node_file(node, touch_if_not_exists)

        # set permissions
        mode_777 = stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO
        for root, dirs, files in os.walk(NODES_DIR):
            for file in files:
                path = os.path.join(root, file)
                os.chmod(path, mode_777)

    def get_output_nodes_from_starfile(self, process: Process):
        """Get nodes from an exported job

        Reads a RELION_OUTPUT_NODES.star file created when a job is exported
        and adds these nodes to a `Process`

        *This function is RELION-specific and will probably be deprecated*

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The process to
                add the nodes to.  It will look in the process's directory for
                the RELION_OUTPUT_NODES.star file

        """
        outnodes = os.path.join(process.name, "RELION_OUTPUT_NODES.star")

        if os.path.isfile(outnodes):
            on_file = OutputNodeStar(outnodes)
            nodes = on_file.get_output_nodes()
            for fnode in nodes:
                # check if node is already on the list and create if not
                if not self.find_node(fnode[0]):
                    node_to_add = Node(fnode[0], fnode[1])
                    self.add_new_output_edge(process, node_to_add)

    def check_process_completion(self) -> bool:
        """Check to see if a any processes have finished running, update their status

        Returns:
            bool: ``True`` if any statuses have been updated, else ``False``

        """

        self.read(do_lock=True)

        finished, failed, aborted = [], [], []
        for proc in self.process_list:
            if proc.status == JOBSTATUS_RUN:
                if os.path.isfile(os.path.join(proc.name, SUCCESS_FILE)):
                    finished.append(proc)
                if os.path.isfile(os.path.join(proc.name, FAIL_FILE)):
                    failed.append(proc)
                if os.path.isfile(os.path.join(proc.name, ABORT_FILE)):
                    aborted.append(proc)

        # gather the changes
        lock_message = ""
        changes = len(finished) + len(failed) + len(aborted)
        if changes:
            outcomes = {
                "successfully finished:\n": finished,
                "failed with an error:\n": failed,
                "been aborted:\n": aborted,
            }
            msg = "check_process_completion: the following jobs have "
            for outcome in outcomes:
                if len(outcomes[outcome]) > 0:
                    lock_message += msg + outcome
                    for proc in outcomes[outcome]:
                        lock_message += proc.name + "\n"

        # only do a read/write cycle if some processes' statuses have changed
        if changes:
            self.update_lock_message(lock_message)
        else:
            self.remove_lock()
            return False

        # set the statuses of the newly finished processes
        for proc in finished:
            proc.status = JOBSTATUS_SUCCESS

            # check if there was an output nodes starfile
            self.get_output_nodes_from_starfile(proc)

            # make any output nodes
            if len(proc.output_nodes) == 0:
                print("ERROR: The output nodes list for job" f" {proc.name} is empty")

            for node in proc.output_nodes:
                if os.path.isfile(node.name):
                    self.touch_temp_node_file(node, False)
                else:
                    print(
                        f"WARNING: output node {node.name} does not exist"
                        f" while job {proc.name} should have finished "
                        "You can manually mark this job as failed"
                        " to suppress this message."
                    )
            update_jobinfo_file(proc, f"Job ended; {JOBSTATUS_SUCCESS}")

        for proc in failed:
            proc.status = JOBSTATUS_FAIL
            update_jobinfo_file(proc, f"Job ended; {JOBSTATUS_FAIL}")

        for proc in aborted:
            proc.status = JOBSTATUS_ABORT
            update_jobinfo_file(proc, f"Job ended; {JOBSTATUS_ABORT}")

        # write and remove changed status
        self.write()
        return True

    def update_status(self, the_proc: Process, new_status: str):
        """Mark a job finished

        The job can be marked as "Succeeded", "Failed", or "Aborted", "Running" or
        "Scheduled"

        Args:
            the_proc (:class:`~pipeliner.data_structure.Process`): The `Process` to
                update the status for
            new_status (str): The new status for the job

        Returns:
            bool: Was the status changed?

        Raises:
            ValueError: If the new_status is not in the approved list
            ValueError: If a job with any other status than 'Running' is marked
                'Aborted'
            ValueError: If a job's updated status is the same as it's current status
        """
        statuses = {
            JOBSTATUS_RUN: None,
            JOBSTATUS_SCHED: None,
            JOBSTATUS_ABORT: ABORT_FILE,
            JOBSTATUS_FAIL: FAIL_FILE,
            JOBSTATUS_SUCCESS: SUCCESS_FILE,
        }
        stats = list(statuses)
        if new_status not in (stats):
            raise ValueError(f"ERROR: New status must be one of {stats}")

        # read in the existing pipeline - get the process
        self.read(do_lock=True)
        _proc = self.find_process(the_proc.name)
        if _proc:
            the_proc = _proc

        lock_message = f"Updating status of {the_proc.name} to {new_status}"
        self.update_lock_message(lock_message)

        # error if trying to abort a job that is not running
        if new_status == JOBSTATUS_ABORT and the_proc.status != JOBSTATUS_RUN:
            self.remove_lock()
            raise ValueError(
                "ERROR: Only currently running job can be marked as aborted"
            )

        # error if trying to change to current status
        if new_status == the_proc.status:
            self.remove_lock()
            raise ValueError(f"Status of {the_proc.name} is already {new_status}")

        # remove any existing control files
        for control_file in [
            FAIL_FILE,
            ABORT_FILE,
            SUCCESS_FILE,
        ]:
            control_file_path = os.path.join(the_proc.name, control_file)
            if os.path.isfile(control_file_path):
                os.remove(control_file_path)

        the_proc.status = new_status
        _status = statuses[new_status]
        if _status is not None:
            touch(os.path.join(the_proc.name, _status))

        # check for output nodes star file
        self.get_output_nodes_from_starfile(the_proc)

        if new_status == JOBSTATUS_SUCCESS:
            # create a dummy job to get any intermediate outputnodes
            d_job = active_job_from_proc(the_proc)
            new_output_nodes = d_job.get_current_output_nodes()

            # Add any new output nodes
            for node in new_output_nodes:
                node.output_from_process = the_proc
                self.add_node(node)
                self.add_new_output_edge(the_proc, node)
                the_proc.output_nodes.append(node)

        # remove any of the output nodes if the files don't exist
        # removes nonexistent terminal nodes if the process had been scheduled
        for outnode in the_proc.output_nodes:
            if not os.path.isfile(outnode.name):
                the_proc.output_nodes.remove(outnode)

        self.write()

        # update the jobinfo file
        update_jobinfo_file(the_proc, f"Status manually set to {new_status}")

        # finally see if the metadata file for the job needs to be updated
        # this only occurs when jobs runner doesn't wait for a queued job to finish
        waiting4md_files = glob(f"{the_proc.name}.WAITING_FOR*")
        if len(waiting4md_files) == 0:
            return True
        elif len(waiting4md_files) > 1:
            print(
                f"Warning {len(waiting4md_files)} placeholder metadata files"
                "were found.\nThis suggests multiple copies of this job were"
                "running simultaneously. BAD!!\nMetadata will only be saved for"
                " the most recent"
            )

        # write the metadata file
        metadata_dict = get_job_metadata(the_proc)
        md_file = waiting4md_files[-1].replace("WAITING_FOR_", "")
        with open(md_file, "w") as md_out:
            json.dump(metadata_dict, md_out, indent=4)

        # remove the placeholders
        for f in waiting4md_files:
            os.remove(f)

    def get_pipeline_edges(
        self, delete_nodes: Optional[List[Node]] = None
    ) -> Tuple[List[Tuple[str, str]], List[Tuple[str, str]]]:
        """Find the connections between jobs

        Get the connections between jobs and nodes for the pipeline
        if any nodes have been deleted their corresponding edges need
        to be removed

        Args:
            delete_nodes (list): List of :class:`~pipeliner.data_structure.Node`
                objects to be deleted from the pipeline

        Returns:
            tuple: ([input edges], [output edges])
            input edges is a list of tuples (process name, input file)
            output edges is a list of tuples (process name, output file)
        """
        input_edges = []
        output_edges = []
        delete_nodes = [] if delete_nodes is None else delete_nodes
        for process in self.process_list:
            for node in process.input_nodes:
                # added 2nd condition to prevent duplication of nodes
                if (
                    node not in delete_nodes
                    and (node.name, process.name) not in input_edges
                ):
                    input_edges.append((node.name, process.name))
            for node in process.output_nodes:
                # added 2nd condition to prevent duplication of nodes
                if (
                    node not in delete_nodes
                    and (process.name, node.name) not in output_edges
                ):
                    output_edges.append((process.name, node.name))

        return (input_edges, output_edges)

    # TODO: Try to make a project graph functions operate on processes rather
    # than job names, fix this for this function later
    def delete_job(self, this_job: Process):
        """Remove a job from the pipeline

        Args:
            this_job (:class:`~pipeliner.data_structure.Process`:): The job to remove

        """
        self.read(do_lock=True)
        # get the processes and nodes to delete
        job_proc = self.find_process(this_job.name)

        # update the lock message
        lock_message = f"Deleting process {this_job} and child jobs"
        self.update_lock_message(lock_message)

        # make the list of jobs to delete
        if job_proc:
            del_processes = [job_proc]
            del_nodes = job_proc.output_nodes

        for proc in self.process_list:
            del_this_proc = False
            for node in proc.input_nodes:
                if node in del_nodes and proc not in del_processes:
                    del_processes.append(proc)
                    del_this_proc = True
            if del_this_proc:
                for onode in proc.output_nodes:
                    if onode not in del_nodes:
                        del_nodes.append(onode)

        # move the processes to the trash
        for delproc in del_processes:
            print("Deleting: " + delproc.name)
            proctype = delproc.outdir
            trashdir = "Trash/" + proctype + "/"
            if not os.path.isdir(trashdir):
                os.makedirs(trashdir)
            shutil.move(delproc.name[:-1] + "/", trashdir)

            # remove any aliases
            alias = delproc.alias
            if alias is not None:
                os.unlink(alias[:-1])

            ## remove the process from the process list..
            self.process_list.remove(delproc)

        # nodes are already deleted with the processes
        for node in del_nodes:
            self.node_list.remove(node)

        # remove the nodes directory(s)
        self.remake_node_directory()

        # update the pipeline file
        edges = self.get_pipeline_edges(del_nodes)
        self.write(edges=edges)

    def delete_node(self, node: Node):
        """Remove a node from the pipeline

        Also removes any edges that contain this node

        Args:
            node (:class:`~pipeliner.data_structure.Node`): The node to remove
        """

        self.read(do_lock=True)
        delnode = self.find_node(node.name)
        if delnode:
            self.node_list.remove(delnode)
            edges = self.get_pipeline_edges([delnode])
            os.remove(delnode.name)
        self.write(lockfile_expected=True, edges=edges)

    def set_job_alias(self, process: Process, new_alias: Optional[str]) -> bool:
        """Set a job's alias

        Sets the alias in the pipeline and creates the ailas directory,
        which is a symlink to the original directory

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The `Process` to
                make an alias for
            new_alias  (str): The new alias for the job

        Returns:
            bool: ``True`` if the new alias was successfully set

        Raises:
            ValueError: If the :class:`~pipeliner.data_structure.Process` is not found
            ValueError: If the new alias is 'None', which is not allowed
            ValueError: If the new alias is shorter than 2 characters
            ValueError: If the new alias begins with job, which would cause problems
            ValueError: If the new alias is not unique

        """
        # make sure job is vaild
        self.read()
        fn_pre, fn_jobnr = decompose_pipeline_filename(process.name)[0:2]
        if fn_pre == "" or fn_jobnr == "":
            raise ValueError("ERROR: invalid pipeline process" " name: " + process.name)

        if new_alias is not None and len(new_alias) == 0:
            new_alias = None

        if new_alias is not None:
            # replace any spaces
            if " " in new_alias:
                new_alias = new_alias.replace(" ", "_")
                print(
                    "WARNING: No spaces are permitted in aliases. New alias"
                    " changed to " + new_alias
                )

            # check the alias name rules
            if new_alias == "None":
                raise ValueError("ERROR: New alias cannot be 'None'")

            if len(new_alias) < 2:
                raise ValueError(
                    "ERROR: Alias cannot be fewer than two characters"
                    ", please provide another one"
                )

            if new_alias[:3] == "job":
                raise ValueError(
                    "ERROR: Alias cannot begin with 'job', please provide another one"
                )
            check_for_illegal_symbols(new_alias, "alias")

            # add the trailing slash
            new_alias = new_alias + "/"
            for proc in self.process_list:
                if proc.alias == fn_pre + "/" + new_alias:
                    raise ValueError("Alias is not unique, please provide another one")

        # read in the pipeline
        self.read(do_lock=True)
        # find the process again because its ID has changed
        new_process = self.find_process(process.name)
        if not new_process:
            raise ValueError(
                f"Process {process.name} cannot be found again after ID change"
            )

        # update the lock message
        lock_message = f"Updating alias for {new_process.name} to {new_alias}"
        self.update_lock_message(lock_message)

        # remove the existing .Nodes entry
        self.delete_temp_node_files(new_process)

        # unlink the alias
        if new_process.alias is not None:
            if os.path.isdir(new_process.alias):
                os.remove(new_process.alias[:-1])

        if new_alias is None:
            new_process.alias = None
        else:
            # set alias in pipline
            _alias = fn_pre + "/" + new_alias
            new_process.alias = _alias
            # make new symlink - relative the the jobdir
            if not os.path.isdir(".Nodes"):
                os.makedirs(".Nodes")
            os.symlink(os.path.relpath(new_process.name[:-1], ".Nodes"), _alias[:-1])

        # remake new nodes entry
        self.touch_temp_node_files(new_process)

        # rewrite the pipeline
        self.write()
        return True

    def undelete_job(self, del_job: str):
        """Get job out of the trash and restore it to the pipeline

        Also restores any alias the job may have had as long as it does not
        confilct wit the current aliases

        Args:
            del_job (str): The name of the deleted job
        """
        # reread the pipeline
        self.read(do_lock=True)
        trashed_procs = [del_job]
        restore_procs = list()

        # get current aliases to make sure none are duplicated
        curr_proclist_aliases = [x.alias for x in self.process_list]

        for del_proc in trashed_procs:
            del_proc_dir = os.path.join("Trash", del_proc)

            # find other process that inputed in and may have also been deleted
            if del_proc not in self.process_list and os.path.isdir(del_proc_dir):
                del_proc_pipe = ProjectGraph(name=del_proc_dir + "job")
                del_proc_pipe.read()
                the_process = del_proc_pipe.process_list[0]
                for node in the_process.input_nodes:
                    fn_pre, fn_jobnr = decompose_pipeline_filename(node.name)[0:2]
                    inproc = fn_pre + f"/job{fn_jobnr:03d}/"
                    if inproc not in trashed_procs:
                        trashed_procs.append(inproc)

                    # restore the alias if it exists - check for duplication
                    the_alias = the_process.alias
                    if the_alias not in curr_proclist_aliases:
                        the_process.alias = the_alias
                    elif the_alias is not None:
                        print(
                            f"WARNING: Cannot restore alias {the_alias} for "
                            f"{the_process.name} because this would create a duplicate "
                            "alias"
                        )
                        the_process.alias = None
                restore_procs.append(the_process)

        print("The following processes will be restored:")
        lock_message = "The following processes will be restored:\n"
        # reverse so they write in the right order
        restore_procs.reverse()
        for rproc in restore_procs:
            # put the process back in the pipeline
            print(rproc.name, " alias: ", rproc.alias)
            lock_message += rproc.name + " alias: " + str(rproc.alias) + "\n"
            self.process_list.append(rproc)
            # put the nodes back in the pipeline
            for onode in rproc.output_nodes:
                if onode.name not in self.node_list:
                    self.node_list.append(onode)
            # restore the files
            proc_pre = decompose_pipeline_filename(rproc.name)[0]
            if not os.path.isdir(proc_pre):
                os.makedirs(proc_pre)
            shutil.move("Trash/" + rproc.name, proc_pre)
            # restore the alias if it exists
            if rproc.alias is not None:
                os.symlink(rproc.name[:-1], rproc.alias[:-1])

        self.update_lock_message(lock_message)
        self.write()
        self.remake_node_directory()

    def clean_up_job(self, process: Process, do_harsh: bool) -> bool:
        """Cleans up a job by deleting intermediate files

        Gets a list of files to delete from the specific job's cleanup function.
        First checks that none of the files that are slated for deletion are
        on the Node list or are of a few specific types that RELION needs.  Then
        moves all the intermediate files to the trash

        There are two tiers of clean up 'harsh' and normal.  Each job defines what
        files are cleaned up by each cleanup type

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The `Process` of
                the job to clean up
            do_harsh (bool): Should harsh cleaning be performed?

        Returns:
            bool: ``True`` if cleaning was performed
            ``False`` if the specified job had no clean up method or it was protected
            from harsh cleaning
        """

        def write_cleanuplog(message: str):
            """Write the message to the clean up log file

            Args:
                message (str): The message to write to the logfile
            """

            with open(CLEANUP_LOG, "a") as clup:
                clup.write(f"{message}\n")

        if process.status != "Succeeded":
            write_cleanuplog("you can only clean up finished jobs ... ")
            return False

        # start the log entry
        cluptype = "Harsh cleanup" if do_harsh else "Cleanup"
        write_cleanuplog(f"{date_time_tag()}: {cluptype} up job {process.name}")

        # first make sure jobname is in pipeliner format
        jobfile = os.path.join(process.name, "job.star")
        reread_job = read_job(jobfile)
        the_job = new_job_of_type(reread_job.PROCESS_NAME)
        the_job.output_name = process.name
        the_job.output_nodes = process.output_nodes
        protected = os.path.isfile(os.path.join(process.name, "NO_HARSH_CLEAN"))
        if do_harsh and protected:
            write_cleanuplog(
                f"WARNING: {process.name} was not harsh cleaned because it "
                "has been protected.\nTo remove the protection delete the file "
                f"{process.name}/NO_HARSH_CLEAN"
            )
            update_jobinfo_file(
                process, f"{cluptype} requested; not performed; job protected"
            )
            return False
        try:
            del_files, del_dirs = the_job.prepare_clean_up_lists(do_harsh)
        except AttributeError:
            write_cleanuplog(f"WARNING: Jobtype {process.type} has no cleanup method")
            update_jobinfo_file(
                process,
                f"{cluptype} requested; not performed; job has no cleanup method",
            )
            return False
        if len(del_files) + len(del_dirs) == 0:
            write_cleanuplog(" - No files or dirs to clean up")
            update_jobinfo_file(process, f"{cluptype}; 0 files, 0 dirs removed")
            return False

        # Check none of the files slated for deletion are input or output nodes
        keeper_dirs = []
        keeper_nodes = [x.name for x in self.node_list]
        # make sure none of the files removed are output nodes
        for f in del_files:
            if f in keeper_nodes:
                del_files.remove(f)
                if os.path.dirname(f) not in keeper_dirs:
                    keeper_dirs.append(os.path.dirname(f))

        for a_dir in del_dirs:
            if a_dir in keeper_dirs:
                del_dirs.remove(a_dir)

        # Then move everything to the trash:
        fcount = 0
        for ddir in del_dirs:
            fcount += len(glob(f"{ddir}/*"))
            shutil.move(ddir, os.path.join("Trash/", ddir))

        for dfile in del_files:
            trash_name = "Trash/" + dfile
            trash_dir = os.path.dirname(trash_name)
            if not os.path.isdir(trash_dir):
                os.makedirs(trash_dir)
            if not os.path.isfile(os.path.join(trash_dir, os.path.basename(dfile))):
                shutil.move(dfile, trash_dir)
        fcount += len(del_files)

        final_stat = (
            f"{cluptype}; {fcount} file(s), {len(del_dirs)} directory(s) removed"
        )
        write_cleanuplog(final_stat)
        update_jobinfo_file(
            process,
            final_stat,
        )
        return True

    def cleanup_all_jobs(self, do_harsh: bool) -> int:
        """Clean up all jobs in the project

        Args:
            do_harsh (bool): Should harsh cleaning be performed?

        Returns:
            int: The number of jobs that were successfully cleaned
        """
        finished_jobs = list()
        for job in self.process_list:
            if job.status == "Succeeded":
                finished_jobs.append(job)

        cleancount = 0
        for job in finished_jobs:
            cleaned = self.clean_up_job(job, do_harsh)
            if cleaned:
                cleancount += 1

        return cleancount

    def replace_files_for_import_export_of_sched_jobs(
        self,
        fn_in_dir: str,
        fn_out_dir: str,
        find_pattern: str,
        replace_pattern: str,
    ):
        """Updates the content of files in jobs that are to be exported

        *This function is part of the RELION import/export system which
        is not used by the pipeliner and will probably be removed*

        Args:
            fn_in_dir (str): The input directory
            fn_out_dir (str): The output directory
            find_pattern (str): The text to replace
            replace_pattern (str): The text to replace it with
        """
        os.makedirs(fn_out_dir)
        my_files = ["run.job", "note.txt", "job_pipeline.star"]
        for the_file in my_files:
            fn = os.path.join(fn_in_dir, the_file)
            if os.path.isfile(fn):
                with open(fn) as f:
                    data = f.read()
                data = data.replace(find_pattern, replace_pattern)
                outfile = os.path.join(fn_out_dir, the_file)
                with open(outfile, "w") as out_file:
                    out_file.write(data)
            if the_file == "job_pipeline.star":
                # check the pipeline and convert if necessary
                StarfileCheck(outfile, is_pipeline=True)

    def export_all_scheduled_jobs(self, mydir: str) -> bool:
        """Exports all scheduled jobs in the pipeline

        *This function is part of the RELION import/export system which
        is not used by the pipeliner and will probably be removed*

        Args:
            mydir (str): The name of the export directory to be created.
                It will be written as ExportJobs/<mydir>

        Returns:
            bool: Were any jobs exported?
        """
        if mydir[-1] != "/":
            mydir += "/"

        export_star = cif.Document()
        export_star_name = "ExportJobs/" + mydir + "exported.star"
        block = export_star.add_new_block("")
        loop = block.init_loop("_", ["rlnPipeLineProcessName"])

        iexp = 0
        self.read(do_lock=False)
        for proc in self.process_list:
            if proc.status == JOBSTATUS_SCHED:
                iexp += 1
                if str(proc.alias) != "None":
                    print(
                        "WARNING: aliases are not allowed on Scheduled jobs that are"
                        f" to be exported!\n- Removing the alias {proc.alias} from "
                        f"{proc.name} for the export"
                    )
                    proc.alias = None
                expname = proc.name.split("/")[0] + f"/exp{iexp:03d}/"
                find_pattern = proc.name
                proc.name = expname
                loop.add_row([proc.name])
                self.replace_files_for_import_export_of_sched_jobs(
                    find_pattern,
                    "ExportJobs/" + mydir + expname,
                    find_pattern,
                    expname,
                )
        if iexp > 0:
            expdir = os.path.join("ExportJobs/", mydir)
            if not os.path.isdir(expdir):
                os.makedirs(expdir)
            star_writer.write(export_star, export_star_name)
            return True

        print("WARNING: There were no scheduled jobs found to export.")
        return False

    def import_jobs(self, fn_export: str):
        """Import a previously exported job

        *This function is part of the RELION import/export system which
        is not used by the pipeliner and will probably be removed*

        Args:
            fn_export (str): The name of the file created by an export job
        """
        self.read(do_lock=True)

        fn_export_dir = os.path.dirname(fn_export)
        imported_jobs = ExportStar(fn_export)
        ijobs = list(imported_jobs.jobs.find_loop("_rlnPipeLineProcessName"))
        for ijob in ijobs:
            ijob_pipe = ProjectGraph(name=fn_export_dir + "/" + ijob + "job")
            ijob_pipe.read(do_lock=False)
            for iproc in ijob_pipe.process_list:
                new_name = iproc.outdir + f"/job{self.job_counter:03d}/"
                self.job_counter += 1
                self.replace_files_for_import_export_of_sched_jobs(
                    fn_export_dir + "/" + iproc.name,
                    new_name,
                    iproc.name,
                    new_name,
                )

                for node in ijob_pipe.node_list:
                    node.name = node.name.replace(iproc.name, new_name)
                    if self.find_node(node.name) not in self.node_list:
                        self.node_list.append(node)
                for proc in ijob_pipe.process_list:
                    proc.name = new_name
                    self.process_list.append(proc)
        self.write()

    def get_downstream_network(
        self, process: Process
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Gets data for drawing a network downstream from process

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The process to trace

        Returns:
            list: Contains tuple for each edge (node, parent process, chile process)
            [:class:`~pipeliner.data_structure.Node`, :class:`~pipeliner.data_
            structure.Process`, :class:`~pipeliner.data_structure.Process`].
            Each edge describes one file in the network

        """
        self.read()
        new_process = self.find_process(process.name)
        if not new_process:
            raise ValueError(f"Process {process.name} not found after ID change")
        associated_procs = [new_process]
        print("Drawing downstream process graph for " + new_process.name)
        # find the children
        edges_list: list = []
        for proc in self.process_list:
            for node in proc.input_nodes:
                if (
                    node.output_from_process in associated_procs
                    and proc not in associated_procs
                ):
                    associated_procs.append(proc)

                    ds_node = (node, node.output_from_process, proc)
                    if ds_node not in edges_list:
                        edges_list.append(ds_node)

        # now do it again backwards to find the parents
        for proc in reversed(self.process_list):
            for node in proc.input_nodes:
                if node.output_from_process in associated_procs:
                    # get the node type from its source and assign color
                    actual = node.output_from_process.output_nodes.index(node)
                    color_node = node.output_from_process.output_nodes[actual]

                    ds_edge = (node, color_node.output_from_process, proc)

                    if ds_edge not in edges_list:
                        edges_list.append(ds_edge)

                    if proc not in associated_procs:
                        associated_procs.append(proc)
        return edges_list

    def get_upstream_network(
        self, process: Process
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Gets data for drawing a network upstream from process

        Args:
            process (:class:`~pipeliner.data_structure.Process`): The process to trace

        Returns:
            list: Contains tuple for each edge (node, parent process, child process)
            [:class:`~pipeliner.data_structure.Node`, :class:`~pipeliner.data_
            structure.Process`, :class:`~pipeliner.data_structure.Process`].
            Each edge describes one file in the network

        """
        self.read()
        new_process = self.find_process(process.name)
        associated_procs = [new_process]
        # find the children
        edges_list: list = []
        # get all the nodes that contributed to the job
        con_node_list: List[Node] = []
        cn_count2, cn_count1 = 1, 0
        while cn_count2 > cn_count1:
            cn_count1 = len(con_node_list)
            for proc in associated_procs:
                if proc:
                    for node in proc.input_nodes:
                        if node not in con_node_list:
                            con_node_list.append(node)
                        if (
                            node.output_from_process not in associated_procs
                            and node.output_from_process is not None
                        ):
                            associated_procs.append(node.output_from_process)
            cn_count2 = len(con_node_list)

        # get the processes that produced them
        for node in con_node_list:
            for into_proc in node.input_for_processes_list:
                if into_proc in associated_procs:
                    edges_list.append((node, node.output_from_process, into_proc))

        return edges_list

    def get_whole_project_network(
        self,
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Get the edges and nodes for the entire project

        Returns:
            list: Edges [nod type, parent_job, child_job, extra info]
            set: The names of all nodes
        """
        # find the children
        self.read()
        edges_list = []
        # get all the nodes
        con_node_list: List[Node] = []
        cn_count2, cn_count1 = 1, 0
        while cn_count2 > cn_count1:
            cn_count1 = len(con_node_list)
            for proc in reversed(self.process_list):
                for node in proc.input_nodes:
                    if node not in con_node_list:
                        con_node_list.append(node)
            cn_count2 = len(con_node_list)

        # get the processes that produced them
        done_procs = []
        for node in con_node_list:
            for into_proc in node.input_for_processes_list:
                edges_list.append((node, node.output_from_process, into_proc))
                done_procs += [node.output_from_process, into_proc]

        # catch any missed processes where nodes don't connect to other processes
        for proc in self.process_list:
            if proc not in done_procs:
                edges_list.append((None, None, proc))

        return edges_list

    def prepare_archive(
        self, process: Process, do_full: bool = False, tar: bool = True
    ) -> str:
        """Create a archive for a job

        There are two levels of archive:

        - A full archive copies the full job directories for the terminal job
          and all of its parents
        - A simple archive just recreates the directory structure, saves the
          parameter files for each job, and writes a script to re-run the project


        Args:
            process (:class:`~pipeliner.data_structure.Process`): The `Process`
                object for the terminal job in the project
            do_full (bool): Should a full archive be created?
            tar (bool): Should the archive be compressed after creation?
        Returns:
            str: A completion message

            It says the archive was created successfully and gives the name of the file
            created or describes any errors encountered

        """
        # get the archive name
        archive_name = date_time_tag(compact=True) + "_project_archive"
        archive_type = "simple" if not do_full else "full"
        # make sets of jobs, their jobstar files, and inout_nodes
        edges_list = self.get_upstream_network(process)
        jobs, inputs, outputs = set(), set(), set()
        for edge in edges_list:
            if edge[1] is not None:
                jobs.add(edge[1].name)
                for innode in edge[1].input_nodes:
                    inputs.add(innode.name)
                for outnode in edge[1].output_nodes:
                    outputs.add(outnode.name)
            # deal with if input file was not product of a node
            elif edge[1] is None and edge[1] is not None:
                jobs.add(os.path.dirname(edge[0].name))

        # add the terminal job's name and nodes
        jobs.add(process.name)
        for inp in process.input_nodes:
            inputs.add(inp.name)
        for outp in process.output_nodes:
            outputs.add(outp.name)

        # check that all the jobs have completed and get the output files

        for job in jobs:
            job_proc = self.find_process(job)
            if job_proc is None:
                return (
                    "ERROR: An archive could not be created because job "
                    f"{job} does not have an associated pipeliner Process"
                    " This is usually the result of manually marking a job "
                    "'Failed' after using it's output(s) for other jobs"
                )
            if job_proc.status in [JOBSTATUS_RUN, JOBSTATUS_SCHED]:
                err_msg = (
                    f"ERROR: Job {job} is still marked as running or "
                    "scheduled! Archives cannot be created unless all jobs "
                    "have terminated\nNo archive created."
                )
                return err_msg
            if not do_full and job_proc.type == "external.processingimport":
                return (
                    f"ERROR: The job {job} contains external processing steps "
                    "that will not be preserved by a simple archive.\nA full archive"
                    " is more appropriate for this project.\nNo archive created."
                )

        # make the archive directory
        os.makedirs(archive_name)

        # make the archive directory
        for job in jobs:
            archive_jobdir = os.path.join(archive_name, job)
            if do_full:
                shutil.copytree(job, archive_jobdir)
            else:
                os.makedirs(archive_jobdir)
                jobfile = os.path.join(job, "job.star")
                modify_jobstar(
                    jobfile,
                    {"_rlnJobIsContinue": "0"},
                    os.path.join(archive_jobdir, "job.star"),
                )

            # remake the aliases if necessary
            job_proc = self.find_process(job)
            if job_proc:
                if job_proc.alias is not None:
                    new_alias = os.path.join(archive_name, job_proc.alias)
                    os.symlink(
                        os.path.relpath(archive_jobdir, archive_name), new_alias[:-1]
                    )
                update_jobinfo_file(job_proc, f"Included job in {archive_type} archive")

        # put the pipeline in the archive
        shutil.copy(self.name + "_pipeline.star", archive_name)

        # put the project file into the directory
        shutil.copy(PROJECT_FILE, archive_name)

        # write the run script if making a simple archive
        if not do_full:
            # check for missing files
            missing_files = inputs - outputs

            for f in missing_files:
                mdir = os.path.join(archive_name, os.path.dirname(f))
                if not os.path.isdir(mdir):
                    os.makedirs(mdir)
                shutil.copy(f, mdir)
                print(
                    f"WARNING: The file {f} is an input to one or more jobs but is "
                    "not a product of any step in the archived pipeline."
                    "\nIt has been included in the archive, but you may want to check"
                    " the results."
                )
            # make the archive and script
            jobs_list = list(jobs)
            jobs_list.sort(key=lambda x: decompose_pipeline_filename(x)[1] or 0)
            with open(os.path.join(archive_name, "run_project.py"), "w") as script:
                script.write("#!/usr/bin/env python\n")
                script.write(
                    "from pipeliner.api.manage_project import PipelinerProject\n\n"
                )
                script.write("proj = PipelinerProject()\n")
                for job in jobs_list:
                    jobstar = os.path.join(job, "job.star")
                    script.write(f'proj.run_job("{jobstar}", overwrite="{job}")\n')
                    job_proc = self.find_process(job)
                    if job_proc and job_proc.alias is not None:
                        new_alias = job_proc.alias[:-1].split("/")[-1]
                        script.write(f'proj.set_alias("{job}", "{new_alias}")\n')

        if tar:
            subprocess.run(["tar", "-zcvf", f"{archive_name}.tar.gz", archive_name])
            shutil.rmtree(archive_name)
            archive_name += ".tar.gz"

        message = f"Created {archive_type} archive {archive_name}"
        update_jobinfo_file(process, message)
        return message

    def create_process_display_objs(self, proc):
        """Create the ResultsDisplay objects for a process and save them

        Args:
            Proc (:class:`~pipeliner.data_structure.Process`): The process to
                operate on

        Returns:
            list: The DisplayObjects for that process
        """
        job = active_job_from_proc(proc)

        # find and remove current display object files
        dispfile = os.path.join(proc.name, ".results_display")
        dispfiles = glob(dispfile + "*.json")
        for f in dispfiles:
            os.remove(f)

        # create new display objects
        dispobjs = job.create_results_display()
        n = 1
        for dob in dispobjs:
            dob.write_displayobj_file(job.output_name, n)
            n += 1
        return dispobjs

    def get_process_results_display(self, proc, forceupdate=False):
        """Get the ResultsDisplay objects for a process

        Atttempts to be as effecient as possible, uses already existing
        files if they are found

        Args:
            forceupdate (bool): Force an update even if it thinks one is
                not necessary
        """
        display_types = {
            "montage": ResultsDisplayMontage,
            "image": ResultsDisplayImage,
            "graph": ResultsDisplayGraph,
            "histogram": ResultsDisplayHistogram,
            "pending": ResultsDisplayPending,
            "text": ResultsDisplayText,
            "mapmodel": ResultsDisplayMapModel,
            "table": ResultsDisplayTable,
            "rvapi": ResultsDisplayRvapi,
        }
        # if the process is running create a new list of DisplayObjects
        if proc.status == JOBSTATUS_RUN:
            try:
                job = active_job_from_proc(proc)
                return job.create_results_display()
            except Exception as e:
                return [ResultsDisplayPending(reason=str(e))]

        # if the job is finished check for existing DisplayObjctes
        elif proc.status == JOBSTATUS_SUCCESS:
            try:
                # find current display object files
                dispfile = os.path.join(proc.name, ".results_display")
                dispfiles = glob(dispfile + "*.json")

                # use the existing files if possible
                if len(dispfiles) > 0:
                    dobjs = []
                    for df in dispfiles:
                        dtype = df.split("_")[-2]
                        # if a pending job is found rerun the whole thing
                        if dtype == "pending" or forceupdate:
                            for f in dispfiles:
                                os.remove(f)
                            return self.create_process_display_objs(proc)

                        # otherwise just read the file
                        with open(df, "r") as dfile:
                            dobjs.append(display_types[dtype](**json.load(dfile)))
                    return dobjs

                # otherwise write new ones and create files for later
                else:
                    return self.create_process_display_objs(proc)
            except Exception as e:
                return [ResultsDisplayPending(reason=str(e))]

        else:
            return [
                ResultsDisplayPending(
                    message=f"No results to display because job is {proc.status}",
                    reason="Job failed or aborted",
                )
            ]
