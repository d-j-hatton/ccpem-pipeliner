#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import math
import operator
import subprocess
import shutil

from collections import OrderedDict
from gemmi import cif
from typing import Optional, List, Union, Tuple

from pipeliner.jobstar_reader import (
    JobStar,
    get_job_type,
)
from pipeliner.star_writer import write
from pipeliner.job_options import (
    FileNameJobOption,
    StringJobOption,
    TRUES,
    BooleanJobOption,
    IntJobOption,
    InputNodeJobOption,
    FloatJobOption,
    JobOptionValidationResult,
)
from pipeliner.data_structure import Node
from pipeliner.utils import (
    truncate_number,
    touch,
    quotate_command_list,
    smart_strip_quotes,
    get_pipeliner_root,
)
from pipeliner.display_tools import create_results_display_object

OUTPUT_NODE_RUNOUT = "LogFile.txt"


class Ref(object):
    """
    Class to hold metadata about a citation or reference, typically a journal article.



    Attributes:
        authors (list): The authors of the reference.
        title (str): The reference's title.
        journal (str): The journal.
        year (str): The year of publication.
        volume (str): The volume number.
        issue (str): The issue number.
        pages (str): The page numbers.
        doi (str): The reference's Digital Object Identifier.
        other_metadata (dict): Other metadata as needed. Gathered from kwargs
    """

    def __init__(
        self,
        authors: Optional[Union[str, List[str]]] = None,
        title: str = "",
        journal: str = "",
        year: str = "",
        volume: str = "",
        issue: str = "",
        pages: str = "",
        doi: str = "",
        **kwargs,
    ):
        """
        Create a new Ref object.

        All arguments are optional. Use additional keyword arguments to store other
        metadata if necessary.

        Args:
            authors (list of str): The authors of the reference.
            title (str): The reference's title.
            journal (str): The journal.
            year (str): The year of publication.
            volume (str): The volume number.
            issue (str): The issue number.
            pages (str): The page numbers.
            doi (str): The reference's Digital Object Identifier.
            **kwargs (str): Other metadata as needed.
        """
        # Ensure self.authors is stored as a list
        if authors is not None:
            if isinstance(authors, str):
                self.authors = [authors]
            else:
                self.authors = list(authors)
        else:
            self.authors = []

        self.title = title
        self.journal = journal
        self.year = year
        self.volume = volume
        self.issue = issue
        self.pages = pages
        self.doi = doi
        self.other_metadata = kwargs

    def __str__(self):
        """Prints out the reference in the pipeliner's display format

        Format is:
            <authors>
            <title>
            <journal>(<year>) <vol>(<issue>) doi: <doi>
            <other metadata key 1>: <other_metadata value 1>
            ...
            <other metadata key n>: <other_metadata value n>

        Returns:
            str: The formatted reference
        """
        thestring = []
        if 0 < len(self.authors) < 5:
            thestring.append(f"{', '.join(self.authors)}")
        elif len(self.authors) > 5:
            thestring.append(f"{self.authors[0]} et al.")
        if self.title != "":
            thestring.append(f"\n{self.title}")

        journal = f"{self.journal} " if self.journal != "" else ""
        year = f"({self.year})" if self.year != "" else ""
        vol = f" {self.volume}" if self.volume != "" else ""
        issue = f"({self.issue})" if self.issue != "" else ""
        pages = f":{self.pages}" if self.pages != "" else ""
        doi = f" doi: {self.doi}" if self.doi != "" else ""
        thestring.append(f"\n{journal}{year}{vol}{issue}{pages}{doi}")
        if len(self.other_metadata) > 0:
            for addarg in self.other_metadata:
                thestring.append(f"\n{addarg}: {self.other_metadata[addarg]}")

        return "".join(thestring)


class JobInfo(object):
    """
    Class for storing info about jobs.

    This is used to generate documentation for the job within the
    pipeliner

    Attributes:
        display_name (str): A user-friendly name to describe the job in a GUI, this
            should not include the software used, because that info is pulled from
            the job type
        version (str): The version number of the pipeliner job
        software_vers (str): The version of the outside executables that will be
            used
        job_author (str): Who wrote the pipeliner job
        short_desc (str): A one line "title" for the job
        long_desc (str): A detained description about what the job does
        documentation (str): A URL for online documentation
        programs (list): A list of 3rd party software used by the job.  These
            are used by the pipeliner to determine if the job can be run so
            they need too be the names of all executables the job might call.
            If any program on this list cannot be found with `which` then the
            job will be marked as unable to run.
        references (list): A list of
            :class:`~pipeliner.pipeliner_job.Ref` objects
        software_version (str): The version of the software that will be called
            when the job is executed
        program_errors (List[str]): List of any errors encountered in checking
            the programs needed to run the job
    """

    def __init__(
        self,
        display_name: str = "Pipeliner job",
        version: str = "0.0",
        job_author: Optional[str] = None,
        short_desc: str = "No short description for this job",
        long_desc: str = "No long description for this job",
        documentation: str = "No online documentation available",
        programs: Optional[list] = None,
        references: Optional[list] = None,
        # todo: make software vers check each program separately and contain
        #  a list of errors like program_errors does
        software_vers: str = "No version info available",
    ):
        """Create a JobInfo object

        Args:
            display_name (str): A user-friendly name to describe the job in a GUI, this
                should not include the software used, because that info is pulled from
                the job type
            version (str): The job version number
            job_author (str): Who wrote the job for the pipeliner
            short_desc (str): A one line description of what the job does
            long_desc (str):  A longer description with background info
            deocumentation (str): Address for on-line documentation
            programs (list): A list of programs used by the job
            reference(list): A list of :class:Ref objects for the job's for the
                main papers describing the software used
            software_vers (str): Info about the version of the third-party software
                that will be run
            availability (str): "Job is available to run" is default, otherwise
                a string telling why the job is not available

        """
        self.display_name = display_name
        self.version = version
        self.job_author = job_author
        self.short_desc = short_desc
        self.long_desc = long_desc
        self.documentation = documentation
        self.programs = programs if programs is not None else []
        self.references = references if references is not None else []
        self.software_vers = software_vers
        self.program_errors: List[str] = []


class PipelinerJob(object):
    """
    Super-class for job objects.

    Each job type has its own sub-class.

    WARNING: do not instantiate this class directly, use the factory functions in this
    module.

    Attributes:
        jobinfo (:class:`~pipeliner.pipeliner_job.JobInfo`): Contains
            information about the job such as references
        output_name (str): The path of the output directory created by this job
        alias (str): the alias for the job if one has been assigned
        is_continue (bool): If this job is a continuation of an older job or a new one
        input_nodes (list): A list of :class:`~pipeliner.data_structure.Node` objects
            for each file used as in input
        output_nodes (list): A list of :class:`~pipeliner.data_structure.Node` objects
            for files produced by the job
        joboptions (dict): A dict of :class:`~pipeliner.job_options.JobOption` objects
            specifying the parameters for the job
        final_commands (list): A list of commands to be run by the job.  Each item is a
            list of arguments
        is_mpi (bool): Does the job use multi-threading?
        is_tomo (bool): Is the job a tomography job?
        vers_com (tuple(list, list)): `[0]` The command to run to which will print
            the program's version info to the `STDOUT`, `[1]` The lines from the stdout
            to display, if empty all lines will be displayed
    """

    PROCESS_NAME = ""
    OUT_DIR = ""

    def __init__(self):
        """Creates a PipelinerJob object

        Raises:
            NotImplementedError: If this class is instantiated directly
        """
        if type(self) == PipelinerJob:
            raise NotImplementedError(
                "Cannot create PipelinerJob objects directly - use a sub-class"
            )

        self.jobinfo = JobInfo()
        self.output_name = ""
        self.alias = None
        self.is_continue = False
        self.input_nodes: List[Node] = []
        self.output_nodes: List[Node] = []
        self.joboptions = {}
        self.final_commands: List[List[List[str]]] = []
        self.is_mpi = False
        self.is_tomo = False
        self.type = ""
        self.subprocess_cwd = None
        self.vers_com: Tuple[List[str], List[int]] = ([], [])

        # If the job is run multiple times ina schedule
        # should every subsequent run always be a continue?
        self.always_continue_in_schedule = False

        # if the job is continued should the previous output
        # nodes be deleted first?
        self.del_nodes_on_continue = False

    def clear(self):
        """Clear all attributes of the job"""

        self.output_name = ""
        self.alias = None
        self.input_nodes.clear()
        self.output_nodes.clear()
        self.is_continue = False

    def set_option(self, line: str):
        """Sets a value in the joboptions dict from a run.job file

        Args:
            line (str): A line from a run.job file

        Raises:
            RuntimeError: If the line does not contain '=='
            RuntimeError: If the value of the line does not match any of the
                joboptions keys
        """

        if len(line) == 0:
            return
        if "==" not in line:
            raise RuntimeError("No '==' entry on JobOption line: {}".format(line))
        key, val = line.split(" == ")
        found = None
        for k, v in self.joboptions.items():
            if v.label == key:
                found = k
                break
        if found is None:
            raise RuntimeError("Job does not contain label: {}".format(key))
        self.joboptions[found].set_string(val)

    def read(self, filename: str):
        """Reads parameters from a run.job or job.star file

        Args:
            filename (str): The file to read.  Can be a run.job or job.star file

        Raises:
            ValueError: If the file is a job.star file and job option from the
                :class:`~pipeliner.pipeliner_job.PipelinerJob` is missing
                from the input file
        """
        if filename.endswith(".job"):
            # make a dict of params from the file {joboption_label: joboption_value}
            with open(filename) as job_file:
                job_lines = job_file.readlines()
            job_lines_split = [x.partition("==") for x in job_lines]
            j_param = {key.strip(): val.strip() for key, _, val in job_lines_split}

            # overwrite joboption values in the job with those from the file
            self.is_continue = True if j_param.get("is_continue") == "true" else False
            self.is_tomo = True if j_param.get("is_tomo") == "true" else False

            for joboption in self.joboptions.values():
                try:
                    joboption.value = j_param[joboption.label]

                # if a joboption is missing from the file just use the default
                except KeyError:
                    print(
                        f"WARNING: {filename}; no value for `{joboption.label}` "
                        f"using default value: {joboption.default_value}"
                    )
        elif filename.endswith("job.star"):
            job_options = JobStar(filename).get_all_options()
            job_type_label = get_job_type(job_options)[1]

            self.is_continue = job_options["_rlnJobIsContinue"].lower() in TRUES
            del job_options[job_type_label]
            del job_options["_rlnJobIsContinue"]
            del job_options["_rlnJobIsTomo"]
            for joboption in self.joboptions:
                # overwrite joboption values with those from the file
                try:
                    if type(job_options[joboption]) is str:
                        job_options[joboption] = smart_strip_quotes(
                            job_options[joboption]
                        )
                    self.joboptions[joboption].value = job_options[joboption]

                # if a joboption is missing from the file just use the defaule
                except KeyError:
                    print(
                        f"WARNING: {filename}; no value for `{joboption.label}` "
                        f"using default value: {joboption.default_value}"
                    )

    def write_runjob(self, fn: Optional[str] = None):
        """Writes a run.job file

        Args:
            fn (str): The name of the file to write. Defaults to the file the pipeliner
                uses for storing GUI parameters.  A directory can be entered also and it
                will add on the file name 'run.job'

        """
        hidden_name = f".gui_{self.PROCESS_NAME.replace('.','_')}"
        filename = hidden_name if fn is None else fn
        if filename != hidden_name and not filename.endswith("run.job"):
            filename += "run.job"
        with open(filename, "w+") as f:
            f.write("job_type == {}\n".format(self.PROCESS_NAME))
            cont_val = "true" if self.is_continue else "false"
            f.write(f"is_continue == {cont_val}\n")
            tomo_val = "true" if self.is_tomo else "false"
            f.write(f"is_tomo == {tomo_val}\n")
            for key, val in self.joboptions.items():
                f.write(val.label + " == " + str(val.value) + "\n")

    def default_params_dict(self) -> dict:
        """Get a dict with the job's parameters and default values

        Returns:
            dict: All of the job's parameters {parameter: default value}
        """
        params_dict = {}
        for jo in self.joboptions:
            params_dict[jo] = self.joboptions[jo].default_value
        return params_dict

    def write_jobstar(
        self,
        output_dir: str,
        output_fn: str = "job.star",
        is_continue: bool = False,
    ):
        """Write a job.star file.

        Args:
            output_fn (str): The name of the file to write. Defaults to job.star
            is_contine (bool): Is the file for a continuation of a previously run job?
                If so only the parameters that can be changed on continuation are
                written.  Overrules is_continue attribute of the job
        """
        filename = output_dir + output_fn
        jobstar = cif.Document()

        job_block = jobstar.add_new_block("job")
        job_block.set_pair("_rlnJobTypeLabel", str(self.PROCESS_NAME))
        continue_val = "1" if is_continue else str(int(self.is_continue))
        job_block.set_pair("_rlnJobIsContinue", continue_val)
        tomo_val = "1" if self.is_tomo else "0"
        job_block.set_pair("_rlnJobIsTomo", tomo_val)

        jobop_block = jobstar.add_new_block("joboptions_values")
        jobop_loop = jobop_block.init_loop(
            "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
        )

        rows = []
        for option in self.joboptions:
            if not is_continue:
                rows.append(
                    [
                        cif.quote(option),
                        cif.quote(str(self.joboptions[option].value)),
                    ]
                )
            else:
                if self.joboptions[option].in_continue:
                    rows.append(
                        [
                            cif.quote(option),
                            cif.quote(str(self.joboptions[option].value)),
                        ]
                    )
        rows.sort(key=lambda x: x[0])
        for row in rows:
            jobop_loop.add_row(row)

        write(jobstar, filename)

    def check_for_programs(self):
        """
        Update the jobinfo with if the required programs are available
        """
        missing = []
        for prog in self.jobinfo.programs:
            if shutil.which(prog) is None:
                missing.append(f"{prog} not found in system path")
        self.jobinfo.program_errors = missing

    def initialise_pipeline(
        self,
        outputname: str,
        defaultname: str,
        job_counter: int,
    ) -> str:
        """Gets the pipeline ready to add a new job

        Sets the output name and clears the input and output nodes

        Args:
            outputname (str): Where the job should write its results.  If blank it
                is set to the next job number based on the job counter
            defaultname (str): The name of the job type
            job_counter (int): The number that job will get

        Returns:
            str: The output name
        """
        self.output_nodes = []
        self.input_nodes = []

        if outputname == "":  # for continue jobs, use the same output name
            outputname = "{}/job{:03}/".format(defaultname, job_counter)
        self.output_name = outputname
        return outputname

    def save_job_submission_script(
        self, output_script: str, outputname: str, commands: list, nmpi: int
    ) -> str:
        """Writes a submission script for jobs submitted to a queue

        Args:
            output_script (str): The name for the script to be written
            output_name (str): The job's output name
            commands (list): The job's commands. In a list of lists format
            nmpi (int): The number of MPI used by the job.  Should be 1 if the job is
                not multi-threaded

        Returns:
            str: The name of the submission script that was written

        Raises:
            ValueError: If no submission script template was specified in the job's
                joboptions
            ValueError: If the submission script template is not found
            RuntimeError: If the output script could not be written
        """
        ## error checking
        fn_qsub = self.joboptions["qsubscript"].get_string(
            True, "ERROR: no submission script template specified"
        )
        if not os.path.isfile(fn_qsub):
            raise ValueError("Error reading template submission script in: " + fn_qsub)
        try:
            touch(output_script)
        except Exception as ex:
            raise RuntimeError(
                "Error writing to job submission script in: " + outputname
            ) from ex

        nthr = self.joboptions.get("nr_threads", 1)
        if nthr != 1:
            nthr = int(nthr.get_number())

        ncores = nmpi * nthr
        ndedi = self.joboptions["min_dedicated"].get_number()
        fnodes = ncores / ndedi
        nnodes = math.ceil(fnodes)
        # should this be written to the run.err as well?
        # should we set ndedi to ncores if ncores < ndedi?
        if fnodes % 1 != 0:
            print(
                " Warning! You're using {} MPI processes with {} threads each "
                "(i.e. {} cores), while asking for {} nodes with {} cores.\n"
                "It is more efficient to make the number of cores (i.e. mpi*threads)"
                " a multiple of the minimum number of dedicated cores per"
                " node ".format(
                    truncate_number(nmpi, 0),
                    truncate_number(nthr, 0),
                    truncate_number(ncores, 0),
                    truncate_number(nnodes, 0),
                    truncate_number(ndedi, 0),
                )
            )

        queuename = self.joboptions["queuename"].get_string()
        qsub_extra_1 = self.joboptions.get("qsub_extra_1")
        qsub_extra_2 = self.joboptions.get("qsub_extra_2")
        qsub_extra_3 = self.joboptions.get("qsub_extra_3")
        qsub_extra_4 = self.joboptions.get("qsub_extra_4")
        extras = {}
        n = 1
        for extra in (qsub_extra_1, qsub_extra_2, qsub_extra_3, qsub_extra_4):
            qsubvar = "qsub_extra_" + str(n)
            if extra is not None:
                extras[qsubvar] = extra.get_string()
            else:
                extras[qsubvar] = None
            n += 1

        quoted_commands = quotate_command_list(commands)

        replacements = {
            "XXXmpinodesXXX": str(int(nmpi)),
            "XXXthreadsXXX": str(int(nthr)),
            "XXXcoresXXX": str(int(ncores)),
            "XXXdedicatedXXX": str(int(ndedi)),
            "XXXnameXXX": outputname,
            "XXXerrfileXXX": outputname + "run.err",
            "XXXoutfileXXX": outputname + "run.out",
            "XXXqueueXXX": queuename,
            "XXXextra1XXX": extras["qsub_extra_1"],
            "XXXextra2XXX": extras["qsub_extra_2"],
            "XXXextra3XXX": extras["qsub_extra_3"],
            "XXXextra4XXX": extras["qsub_extra_4"],
            "XXXcommandXXX": "\n".join([" ".join(x) for x in quoted_commands]),
        }

        with open(fn_qsub) as template_data:
            template = template_data.readlines()

        with open(output_script, "w") as outscript:
            for line in template:
                for sub in replacements:
                    if sub in line:
                        line = line.replace(sub, replacements[sub])
                outscript.write(line)
        return fn_qsub

    def prepare_final_command(
        self,
        outputname: str,
        commands: list,
        do_makedir: bool,
        ignore_queue: bool = False,
    ) -> list:
        """Assemble commands to be run for a job

        The commands are in a lists of lists format.  Each item in the main list is
        a single command and composed of a list of the arguments for that command.

        An additional command to run the check completion script is added to the
        commands list.

        Decides if a queue submission script is needed. If so it is written and the
        commands list is changed to the queue submission command

        Args:
            outputname (str): The job's output directory
            commands (list): The commands to run as a list of lists
            do_makedir (bool): Should the output directory be created if it doesn't
                already exist?
            ignore_queue (bool): Do not make a submission script, even if the job is
                sent to the queue, used for generating commands for display

        Returns:
            list: [[[Actual, command], [to, be, run]], [[the, Job, commands]]] If the
                job is being submitted to a queue `[0]` will be the qsub command and
                `[1]` will the be the actual job commands.  For local jobs they will
                be identical
        """
        # first make all of the input nodes automatically
        self.create_input_nodes()

        # make sure the commands list is all strings
        commands = [[str(x) for x in i] for i in commands]

        # add time -p to the beginnning of every command
        for n, com in enumerate(commands):
            if com[:2] != ["time", "-p"]:
                commands[n] = ["time", "-p"] + com

        # Create output directory
        if do_makedir:
            job_dir = os.path.dirname(outputname)
            if not os.path.isdir(job_dir) and len(job_dir) > 0:
                os.makedirs(job_dir)

        # check for MPI
        nr_mpi_jo = self.joboptions.get("nr_mpi")
        if nr_mpi_jo is not None:
            nr_mpi = int(nr_mpi_jo.get_number())
        else:
            nr_mpi = 0
        self.is_mpi = True if nr_mpi > 1 else False

        # run the job locally or submit to queue
        do_queue = self.joboptions.get("do_queue")
        if do_queue is not None and not ignore_queue:
            do_queue = do_queue.get_boolean()
        else:
            do_queue = False

        # if there are no specified output nodes use the run.out logfile
        if len(self.output_nodes) == 0:
            run_of = os.path.join(self.output_name, "run.out")
            self.output_nodes.append(Node(run_of, OUTPUT_NODE_RUNOUT))

        # perform the status check to see if the job finished successfully
        outfile_names = [
            x.name.replace(self.output_name, "") for x in self.output_nodes
        ]
        cc_path = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")
        ccp = self.output_name if self.subprocess_cwd is None else ""
        if len(outfile_names) > 0:
            commands.append(["time", "-p", cc_path, ccp, *outfile_names])
        if do_queue:
            output_script = outputname + "run_submit.script"

            PipelinerJob.save_job_submission_script(
                self, output_script, outputname, commands, nr_mpi
            )

            qsub = self.joboptions["qsub"].get_string()
            self.final_commands = [[[qsub, output_script]]]

        else:
            # check for allowed number of mpis
            try:
                warn = int(os.environ["PIPELINER_ERROR_LOCAL_MPI"])
                if nr_mpi > warn:
                    errmsg = (
                        "ERROR: you're submitting a local job with {} parallel MPI"
                        " processes. Only {} are allowed by the"
                        " PIPELINER_ERROR_LOCAL_MPI environment variable. This might"
                        " cause the job to fail.".format(nr_mpi, warn)
                    )
                    print(errmsg)
                    with open(os.path.join(outputname, "run.err"), "a") as errfile:
                        errfile.write(errmsg)

            except KeyError:
                pass
            self.final_commands = [commands]

        # add the 2nd copy of the job commands
        self.final_commands.append(commands)
        return self.final_commands

    def get_runtab_options(self, mpi: bool = True, threads: bool = True):
        """Get the options found in the Run tab of the GUI,
        which are common to for all jobtypes

        Adds entries to the joboptions dict for queueing, MPI, threading, and
        additional arguments. This method should be used when initialising a
        `PipelinerJob` subclass

        Args:
            mpi (bool): Should MPI options be included?
            threads (bool): Should multi-threading options be included

        """

        mpimax = int(os.environ.get("PIPELINER_MPI_MAX", 1))
        if mpi:
            self.joboptions["nr_mpi"] = IntJobOption(
                label="Number of MPI procs:",
                default_value=mpimax,
                suggested_min=1,
                suggested_max=64,
                step_value=1,
                help_text=(
                    "Number of MPI nodes to use in parallel. When set to 1, MPI will"
                    " not be used. The maximum can be set through the environment"
                    " variable PIPELINER_MPI_MAX."
                ),
                in_continue=True,
            )

        if threads:
            self.joboptions["nr_threads"] = IntJobOption(
                label="Number of threads:",
                default_value=1,
                suggested_min=1,
                suggested_max=16,
                step_value=1,
                help_text=(
                    "Number of shared-memory (POSIX) threads to use in"
                    " parallel. When set to 1, no multi-threading will "
                    "be used. The maximum can be set through the environment"
                    " variable PIPELINER_THREAD_MAX."
                ),
                in_continue=True,
            )

        self.make_queue_options()
        self.make_additional_args()

    def make_additional_args(self):
        """Get the additional arguments job option"""
        self.joboptions["other_args"] = StringJobOption(
            label="Additional arguments:",
            default_value="",
            help_text="",
            in_continue=True,
        )

    def make_queue_options(self):
        """Get options related to queueing and queue submission,
        which are common to for all jobtypes"""

        do_queue_default = os.environ.get("PIPELINER_QUEUE_USE", False)
        queue_default = True if do_queue_default in TRUES else False

        self.joboptions["do_queue"] = BooleanJobOption(
            label="Submit to queue?",
            default_value=queue_default,
            help_text=(
                "If set to Yes, the job will be submit to a queue,"
                " otherwise the job will be executed locally. Note "
                "that only MPI jobs may be sent to a queue. The default "
                "can be set through the environment variable PIPELINER_QUEUE_USE."
            ),
            in_continue=True,
        )

        queue_name_default = os.environ.get("PIPELINER_QUEUE_NAME", "openmpi")
        self.joboptions["queuename"] = StringJobOption(
            label="Queue name:",
            default_value=queue_name_default,
            help_text=(
                "Name of the queue to which to submit the job. The"
                " default name can be set through the environment"
                " variable PIPELINER_QUEUE_NAME."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
        )

        qsub_default = os.environ.get("PIPELINER_QSUB_COMMAND", "qsub")
        self.joboptions["qsub"] = StringJobOption(
            label="Queue submit command:",
            default_value=qsub_default,
            help_text=(
                "Name of the command used to submit scripts to the queue,"
                " e.g. qsub or bsub. Note that the person who installed PIPELINER"
                " should have made a custom script for your cluster/queue setup."
                " Check this is the case (or create your own script"
                " if you have trouble submitting jobs. The default "
                "command can be set through the environment variable "
                "PIPELINER_QSUB_COMMAND."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
        )
        qsub_template_default = os.environ.get(
            "PIPELINER_QSUB_TEMPLATE",
            "",
        )

        self.joboptions["qsubscript"] = FileNameJobOption(
            label="Standard submission script:",
            default_value="",
            pattern="*",
            directory=".",
            help_text=(
                "The template for your standard queue job submission script. Its"
                " default location may be changed by setting the environment variable"
                " PIPELINER_QSUB_TEMPLATE. In the template script a number of variables"
                " will be replaced: \nXXXcommandXXX = relion command + arguments;"
                " \nXXXqueueXXX = The queue name;\nXXXmpinodesXXX = The number of MPI"
                " nodes;\nXXXthreadsXXX = The number of threads;\nXXXcoresXXX ="
                " XXXmpinodesXXX * XXXthreadsXXX;\nXXXdedicatedXXX = The minimum number"
                " of dedicated cores on each node;\nXXXnodesXXX = The number of"
                " requested nodes = CEIL(XXXcoresXXX / XXXdedicatedXXX);\nIf these"
                " options are not enough for your standard jobs, you may define a"
                " user-specified number of extra variables: XXXextra1XXX, XXXextra2XXX,"
                " etc. The number of extra variables is controlled through the"
                " environment variable PIPELINER_QSUB_EXTRA_COUNT. Their help text is"
                " set by the environment variables PIPELINER_QSUB_EXTRA1,"
                " PIPELINER_QSUB_EXTRA2, etc For example, setenv"
                " PIPELINER_QSUB_EXTRA_COUNT 1, together with setenv"
                " PIPELINER_QSUB_EXTRA1 'Max number of hours in queue' will result in"
                " an additional (text) ein the GUI Any variables XXXextra1XXX in the"
                " template script will be replaced by the corresponding value.Likewise,"
                " default values for the extra entries can be set through environment"
                " variables PIPELINER_QSUB_EXTRA1_DEFAULT,"
                " PIPELINER_QSUB_EXTRA2_DEFAULT, etc. But note that (unlike all other"
                " entries in the GUI) the extra values are not remembered from one run"
                " to the other."
            ),
            in_continue=True,
            suggestion=qsub_template_default,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
        )

        min_cores_default = os.environ.get("PIPELINER_MINIMUM_DEDICATED", 1)
        self.joboptions["min_dedicated"] = IntJobOption(
            label="Minimum dedicated cores per node:",
            default_value=min_cores_default,
            suggested_min=1,
            suggested_max=64,
            step_value=1,
            help_text=(
                "Minimum number of dedicated cores that need to be requested "
                "on each node. This is useful to force the queue to fill up entire "
                "nodes of a given size. The default can be set through the environment"
                " variable PIPELINER_MINIMUM_DEDICATED."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
        )

        self.get_extra_options()

    def get_extra_options(self):
        """Get user specified extra queue submission options"""
        if "PIPELINER_QSUB_EXTRA_COUNT" in os.environ:
            n_extra = int(os.environ["PIPELINER_QSUB_EXTRA_COUNT"])
        else:
            n_extra = 0
        if n_extra > 0:
            for extranum in range(1, n_extra + 1):
                if "PIPELINER_QSUB_EXTRA{}".format(str(extranum)) in os.environ:
                    extra_id = "PIPELINER_QSUB_EXTRA{}".format(extranum)
                    extra_name = os.environ[extra_id]
                else:
                    raise ValueError(
                        "environment variable PIPELINER_QSUB_EXTRA{} missing".format(
                            str(extranum)
                        )
                    )

                extra_default = os.environ.get("{}_DEFAULT".format(extra_id), "")
                extra_help = os.environ.get("{}_HELP".format(extra_id), "")

                self.joboptions["qsub_extra_{}".format(extranum)] = StringJobOption(
                    label=extra_name,
                    default_value=extra_default,
                    help_text=extra_help,
                    in_continue=True,
                )

    def gather_metadata(self):
        """
        Placeholder function for metadata gathering

        Each job class should define this individually

        Returns:
            dict: A place holder "No metadata available" and the reason why

        """

        return {"No metadata": "No metadata available for this job type"}

    def prepare_clean_up_lists(self, do_harsh: bool = False):
        """
        Placeholder function for preparation of list of files to cleanup

        Each job class should define this individually

        Args:
            do_harsh (bool): Should a harsh cleanup be performed

        Returns:
            tuple: Two empty lists ``([files, to, delete], [dirs, to, delete])``
        """
        if do_harsh:
            return ([], [])

        return ([], [])

    def get_commands(self):
        raise NotImplementedError(
            "The get_commands method must be overwritten in subclasses of PipelinerJob"
        )

    def post_run_actions(self):
        """
        Placeholder function for actions to do after the job has finished

        Each job class should define this individually.  This is used for job where
        somthing needs to be done after the job has completed, such as jobs where the
        number/names of output nodes is not known until the job has finished
        """

        pass

    def get_current_output_nodes(self) -> list:
        """
        Get the current output nodes if the job was stopped prematurely

        For most jobs there will not be any but for jobs with many iterations
        the most recent interation can be used of teh job is aborted or failed
        and then later marked as successful

        Args:
            new_status (str): The new status - what actions are performed will be
                dependent on this

        Returns:
            list: of :class:`~pipeliner.data_structure.Node` objects
        """

        return []

    def parameter_validation(self) -> List[JobOptionValidationResult]:
        """Advanced validation of job parameters

        This is a placeholder function for additional validation to be done by
        individual job subtypes, such as comparing JobOption values IE:
        JobOption A must be > JobOption B

        returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        return []

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        """Make sure all of the joboptions meet their validation criteria

        Returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects

        """

        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]
            err = jo.validate()
            if err is not None:
                errors.append(err)

        adval = self.parameter_validation()
        adval = adval if adval is not None else []
        if len(adval) > 0:
            errors.extend(adval)

        dynamic_req_errors = self.validate_dynamically_required_joboptions()
        if len(dynamic_req_errors) > 0:
            errors.extend(dynamic_req_errors)

        return errors

    def validate_dynamically_required_joboptions(self):
        """Validate joboptions that only become required in relation to others

        For example if job option A is True, job option B is now required

        Returns:
            list: :class:`pipeliner.job_options.JobOptionValidationResult`:
                for any errors found
        """
        ops = {
            "=": operator.eq,
            "!=": operator.ne,
            ">": operator.gt,
            ">=": operator.ge,
            "<": operator.lt,
            "<=": operator.le,
        }

        errors = []
        for jo in self.joboptions:
            jobop = self.joboptions[jo]
            if not jobop.required_if:
                continue

            jobop_type = type(jobop)
            if jobop_type == BooleanJobOption:
                jo_val = jobop.get_boolean()
            elif jobop_type in [FloatJobOption, IntJobOption]:
                jo_val = jobop.get_number()
            else:
                jo_val = jobop.get_string()

            for req in jobop.required_if:
                parent, op, exp_value = req
                parent_jo = self.joboptions[parent]
                pjo_type = type(parent_jo)
                if pjo_type == BooleanJobOption:
                    compare_val = parent_jo.get_boolean()
                elif pjo_type in [FloatJobOption, IntJobOption]:
                    compare_val = parent_jo.get_number()
                else:
                    compare_val = parent_jo.get_string()
                is_now_req = ops[op](compare_val, exp_value)

                if is_now_req and jo_val in [None, ""]:
                    msg = (
                        f"Because {parent_jo.label} {op} {exp_value} {jobop.label} "
                        "is required"
                    )
                    errors.append(
                        JobOptionValidationResult("error", [jobop, parent_jo], msg)
                    )
        return errors

    def validate_input_files(self) -> list:
        """Check that files specified as inputs actually exist

        Returns:
            list: A list of :class:`pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]

            if jo.joboption_type in ["FILENAME", "INPUTNODE"]:
                err = jo.check_file()
                if err is not None:
                    errors.append(err)

        return errors

    def prepare_onedep_data(self) -> list:
        """Placeholder for function to return deposition data objects

        The specific list returned should be defined by each jobtype

        Returns:
            list: The deposition object(s) returned by the specific job.  These
                need to be of the types defined in `pipeliner.onedep_deposition`
        """

        return []

    def parse_additional_args(self):
        """Parse the additional arguments job option and return a list

        Returns:
            list: A list ready to append to the command.  Quotated strings are preserved
                as quoted strings all others are split into individual items
        """
        other_arguments = self.joboptions["other_args"].get_string()
        oo_coms = []
        if len(other_arguments) > 0:
            oo = other_arguments.replace("'", '"')
            oosplit = oo.split('"')
            for n, addarg in enumerate(oosplit):
                if bool(n % 2):
                    oo_coms.append(f'"{addarg}"')
                else:
                    oo_coms.extend(addarg.split())
        return oo_coms

    def create_input_nodes(self):
        """Automatically add the job's input nodes to it's nodelist"""
        curr_in_nodes = [(x.name, x.type) for x in self.input_nodes]

        for jo in self.joboptions:
            jop = self.joboptions[jo]
            if type(jop) in [InputNodeJobOption, FileNameJobOption]:
                fname = jop.get_string()
                ntype = jop.node_type
                create_node = getattr(jop, "create_node", True)
            else:
                continue

            node_needed = fname not in ["", None] and ntype not in ["", None]
            node_exists = (fname, ntype) in curr_in_nodes

            if node_needed and not node_exists and create_node:
                self.input_nodes.append(Node(fname, ntype))

    def create_results_display(self) -> list:
        """This function creates the objects to be displayed by the GUI

        Placeholder for individual jobs to have their own

        Returns:
            list: a :class:`~pipeliner.display_tools.ResultsDisplayText` saying
                there is no specific method for this job
        """

        return [
            create_results_display_object(
                "text",
                title=("This job does not have a specific results display function."),
                display_data=f"It produced {len(self.output_nodes)} output nodes",
                associated_data=[x.name for x in self.output_nodes],
            )
        ]

    def get_job_vers(self):
        """Get the current version of the software available in this system

        Returns:
            str: The version info, or `No version info available` if no version
                command was specified
        """
        if self.vers_com[0]:
            try:
                versproc = subprocess.run(self.vers_com[0], capture_output=True)
                vers_raw = versproc.stdout.decode().split("\n")
                if self.vers_com[1]:

                    vers_info = "; ".join([vers_raw[x] for x in self.vers_com[1]])
                else:
                    vers_info = "; ".join(vers_raw)
                return vers_info
            except FileNotFoundError:
                return "No version info available"
        return "No version info available"

    def set_joboption_order(self, new_order=List[str]):
        """Replace the joboptions dict with an ordered dict

        Use this to set the order the joboptions will appear in the GUI. If a joboption
        is not specified in the list it will be tagged on to the end of the list.


        Args:
            new_order (list[str]): A list of joboption keys, in the order they should
                appear

        Raises:
            ValueError: If a nonexistent joboption is specified
        """
        # error checking
        jo_keys = list(self.joboptions)
        bad = []
        for njo in new_order:
            if njo not in jo_keys:
                bad.append(njo)
        if bad:
            raise ValueError(f"{bad} not in self.joboptions dict")

        # make the ordered dict
        ordered_jos = [self.joboptions[x] for x in new_order]
        ordered_jodict = OrderedDict(zip(new_order, ordered_jos))

        # add any not included in the list
        for jokey in jo_keys:
            if jokey not in ordered_jodict:
                ordered_jodict[jokey] = self.joboptions[jokey]

        self.joboptions = ordered_jodict
