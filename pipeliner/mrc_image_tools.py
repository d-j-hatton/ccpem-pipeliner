#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import mrcfile
import os
import numpy as np
from PIL import Image
from matplotlib.pyplot import get_cmap
from typing import Any, Iterable, List, Optional, Tuple


def read_tiff(intiff):
    """Read multi-image tiff file, e.g. tiff stacks
    Args:
        intiff (str): The name of the file
    Returns:
        array (array): Numpy array
    """
    img = Image.open(intiff)
    images = []
    for i in range(img.n_frames):
        img.seek(i)
        images.append(np.array(img))
    img.close()
    return np.array(images)


def get_mrc_dims(inmrc: str) -> Tuple[int, int, int]:
    """Get the shape of an mrc file

    Args:
        inmrc (str): The name of the file
    Returns:
        list: (int,int,int) x,y,x size in pixels

    """
    try:
        with mrcfile.open(inmrc) as mrc:
            z, y, x = mrc.data.shape
    except ValueError:
        with mrcfile.open(inmrc) as mrc:
            y, x = mrc.data.shape
        z = 1

    return (x, y, z)


def get_tiff_dims(intiff: str) -> Tuple[int, int, int]:
    img = read_tiff(intiff)
    if len(img.shape) == 3:
        z, y, x = img.shape
    else:
        z = 1
        y, x = img.shape

    return (x, y, z)


def get_tiff_stats(intiff: str) -> list:
    final_stats: list = []
    img = read_tiff(intiff)
    if len(img.shape) == 3:
        z, y, x = img.shape
        stack = True
    else:
        z = 1
        y, x = img.shape
        stack = False

    dtype = img.dtype
    modes = {
        "int8": ("8-bit signed integer (range -128 to 127)"),
        "int16": ("16-bit signed integer"),
        "int32": ("32-bit signed real"),
        "uint16": ("16-bit unsigned integer"),
        "float16": ("16-bit float (IEEE754)"),
    }
    typedesc = modes.get(str(dtype), "Unknown data type")
    dtype_desc = f"{typedesc}: {dtype}"

    if not stack:
        final_stats.append((x, y, z))
        final_stats.append(np.min(img))
        final_stats.append(np.max(img))
        final_stats.append(np.mean(img))
        final_stats.append(np.std(img))
        final_stats.append(None)
        final_stats.append(dtype_desc)

    elif stack:
        for frame in range(z):
            sub_stats: list = []
            sub = img[frame, :, :]
            sub_stats.append((sub.shape[1], sub.shape[0]))
            sub_stats.append(np.min(sub))
            sub_stats.append(np.max(sub))
            sub_stats.append(np.mean(sub))
            sub_stats.append(np.std(sub))
            sub_stats.append(None)
            sub_stats.append(dtype_desc)
            final_stats.append((sub_stats))

    return final_stats


def get_mrc_stats(inmrc: str, stack: bool = False) -> list:
    """Get statistics on a mrc file


    Args:
        inmrc (str): The name of the file
        stack (bool): Calculate stats for each individual
            frame rather than the image as a whole
    Returns:
        tuple: ((imgsize x, y, z), min, max, mean, std,
            (voxelsize x, y, z), 'mode: mode description)') or a
            tuple of with a tuple for each image in stack mode
    """
    dims = get_mrc_dims(inmrc)
    final_stats: list = []

    modes = {
        0: ("8-bit signed integer (range -128 to 127)", np.int8),
        1: ("16-bit signed integer", np.int16),
        2: ("32-bit signed real", np.float32),
        3: ("transform : complex 16-bit integers", np.int16),
        4: ("transform : complex 32-bit reals", np.float32),
        6: ("16-bit unsigned integer", np.int16),
        12: ("16-bit float (IEEE754)", np.float16),
    }

    with mrcfile.mmap(inmrc) as mrc:
        data = mrc.data
        mode = int(mrc.header["mode"])
        modes_list = modes.get(mode)
        if modes_list is None:
            raise ValueError(f"Unknown mode (data type): {mode}")
        mode_desc = modes_list[0]
        modedisp = f"{mode}: {mode_desc}"
        conv = modes[mode][1]
        apix = (
            conv(mrc.voxel_size["x"]),
            conv(mrc.voxel_size["y"]),
            conv(mrc.voxel_size["z"]),
        )

    if not stack:
        final_stats.append(dims)
        final_stats.append(conv(np.min(data)))
        final_stats.append(conv(np.max(data)))
        final_stats.append(conv(np.mean(data)))
        final_stats.append(conv(np.std(data)))
        final_stats.append(apix)
        final_stats.append(modedisp)

    elif stack:
        for frame in range(dims[2]):
            sub_stats: list = []
            sub = data[frame, :, :]
            sub_stats.append((sub.shape[1], sub.shape[0]))
            sub_stats.append(conv(np.min(sub)))
            sub_stats.append(conv(np.max(sub)))
            sub_stats.append(conv(np.mean(sub)))
            sub_stats.append(conv(np.std(sub)))
            sub_stats.append((apix[0], apix[1]))
            sub_stats.append(modedisp)
            final_stats.append((sub_stats))

    return final_stats


def stack_mrcs(input_files: List[str], out: str = "stacked.mrcs") -> Tuple[str, int]:
    """Create a stack from mrc files

    Args:
        input_files (list): The files to be stacked, in order
        output_name (str): The name to be given to the output file

    Returns:
        tuple: ('output file name', number of files stacked)
    """
    # make sure file name ends with .mrcs and not .mrc
    if not out.endswith("s"):
        out += "s"
    if not out.endswith(".mrcs"):
        out += ".mrcs"

    # make the output directory if necessary
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "" and not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    imgs, shapes, apixs, errors = [], [], [], []
    for in_file in input_files:
        with mrcfile.open(in_file) as mrc:
            imgs.append(mrc.data)
            shapes.append(mrc.data.shape)
            apixs.append(mrc.voxel_size)
    if not all(x == shapes[0] for x in shapes):
        errors.append("All images must have the same dimensions.")
    if not all(x == apixs[0] for x in apixs):
        errors.append("All images must have the same voxel size.")
    if len(errors) > 0:
        raise ValueError("".join(errors))

    with mrcfile.new(out, np.stack(imgs)) as newmrc:
        newmrc.voxel_size = apixs[0]

    return (out, len(input_files))


def unstack_mrcs(
    input_file: str, imgs: Optional[Iterable[int]] = None, out: str = "unstack"
) -> Tuple[str, int]:
    """Create individial images from a mrc stack

    Args:
        input_file (str): Name of the input file
        imgs (list): indexes of the images to unstack, will do all if None
            use proper numbering starting with 0
        out (str): base name of the output, if a directory is included it will be
            created if necessary

    Returns:
        tuple: ('format of the output', number of files created)
    """
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "" and not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    with mrcfile.mmap(input_file) as stack:
        if imgs is None:
            imgs = range(stack.data.shape[0])
        if max(imgs) >= stack.data.shape[0]:
            raise ValueError("Requested frames not in the stack")
        zeros = len(str(len(list(imgs))))
        apix = stack.voxel_size
        for n, img in enumerate(imgs):
            the_slice = stack.data[img, :, :]
            sp = "" if out[-1] == "/" else "_"
            name = f"{out}{sp}{n:0{zeros}d}.mrc"
            with mrcfile.new(name) as nf:
                nf.set_data(the_slice)
                nf.voxel_size = apix

    return (f"{out}{sp}{'%' * zeros}.mrc", len(list(imgs)))


def substack_mrcs(
    input_stack: str, imgs: Optional[Iterable[Any]], out: str = "substack.mrcs"
) -> Tuple[str, int]:
    """Create a substack from a mrc stack

    Args:
        input_file (str): Name of the input file
        imgs (list): Which images to unstack, will do all if None
        output_name (str): Name of the output file to be written

    Returns:
        tuple: ('name of output stack', number of frames new stack)
    """
    # make sure file name ends with .mrcs and not .mrc
    if not out.endswith("s"):
        out += "s"
    if not out.endswith(".mrcs"):
        out += ".mrcs"

    # make the output directory if necessary
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "" and not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    frames = []
    with mrcfile.mmap(input_stack) as instack:
        if imgs is None:
            imgs = range(instack.data.shape[0])
        if max(imgs) >= instack.data.shape[0]:
            raise ValueError("Requested frames not in the stack")

        apix = instack.voxel_size
        for fr in imgs:
            frames.append(instack.data[fr, :, :])

    with mrcfile.new(out, np.stack(frames)) as newmrc:
        newmrc.voxel_size = apix

    return (out, len(list(imgs)))


def bin_and_norm_array(in_array, newsize):
    """Bin and normalize an array

    If the array is 3D it is first summed along the z axis

    Args:
        in_array (array): The numpy array to be binned
        newsize (int): The new size, must be square

    Returns:
        array: The binned and normalized array

    Raises:
        ValueError: If the array is not 2D or 3D

    """
    # If 3D array merge along zaxis
    if len(in_array.shape) == 3:
        raw = np.sum(in_array, axis=0)
    elif len(in_array.shape) == 2:
        raw = in_array
    else:
        raise ValueError(f"Array is {len(in_array.shape)}D; must be 2d or 3D")

    # bin array
    binned = bin_array_to_size(raw, newsize)

    # remove outliers
    binmean = np.mean(binned)
    # have to lower precision of std calculation to prevent overflow errors
    std_cutoff = 3.0 * np.std(binned, dtype=np.float32)
    outlier_h = binmean + std_cutoff
    outlier_l = binmean - std_cutoff
    binned_a = binned.copy()
    binned_a[binned_a > outlier_h] = binmean
    binned_a[binned_a < outlier_l] = binmean

    # normalize  from 0-255
    normed = np.nan_to_num((binned_a - np.min(binned_a)) / np.ptp(binned_a))
    intarray = np.full(normed.shape, 255)
    normed = normed * intarray
    return normed


def mrc_thumbnail(in_mrc, new_size, outname=None):
    """Write a thumbnail image from an mrc file

    Args:
        in_mrc (str): Name of input file
        new_size (int): The size to make the thumbnail axis 0 of the image will be
            shrunk to this size and axix 1 shrunk proportionally
        outname (str): Name of the output file

    Returns:
        array: The binned and normalized (and z-summed if applicable) array
    """
    with mrcfile.open(in_mrc) as mrc:
        raw = mrc.data
    normed = bin_and_norm_array(raw, new_size)

    if outname is not None:
        outimg = Image.fromarray(normed)
        outimg = outimg.convert("RGB")
        outimg.save(outname)

    return normed


def tiff_thumbnail(in_tiff, new_size, outname=None):
    """Write a thumbnail image from an mrc file

    Args:
        in_mrc (str): Name of input file
        new_size (int): The size to make the thumbnail axis 0 of the image will be
            shrunk to this size and axix 1 shrunk proportionally
        outname (str): Name of the output file, if noe

    Returns:
        array: The binned and normalized (and z-summed if applicable) array
    """
    raw = read_tiff(in_tiff)
    normed = bin_and_norm_array(raw, new_size)

    if outname is not None:
        outimg = Image.fromarray(normed)
        outimg = outimg.convert("RGB")
        outimg.save(outname)

    return normed


def bin_axis(data, axis, bstep, binsize):
    """Bin a numpy array along one axis

    Args:
        data (array): The array to bin
        axis (int): The axis to bin along
        bstep (float): Number of points between each bin
        binsize (float): Size of each bin

    Returns:
        array: The array binned along the selected axis
    """
    data = np.array(data)
    dims = np.array(data.shape)
    argdims = np.arange(data.ndim)
    argdims[0], argdims[axis] = argdims[axis], argdims[0]
    data = data.transpose(argdims)
    data = [
        np.mean(
            np.take(data, np.arange(int(i * bstep), int(i * bstep + binsize)), 0), 0
        )
        for i in np.arange(dims[axis] // bstep)
    ]
    data = np.array(data).transpose(argdims)
    return data


def bin_array_to_size(in_array, dim):
    """Bin an array to a specific size

    Bins an array so axis 0 is the specified size

    Args:
        in_array: The array to bin
        dim (int): The size to bin axis 0 to.  Axis 1 will be binned
            proportionally.
    Returns:
        array: The binned array
    """
    binx = float(in_array.shape[0]) / dim
    if binx <= 1:
        return in_array

    if len(in_array.shape) == 2:
        return bin_axis(bin_axis(in_array, 0, binx, binx), 1, binx, binx)

    elif len(in_array.shape) == 3:
        ox, oy, oz = [float(x) for x in in_array.shape]
        if not ox == oy == oz:
            raise ValueError("Only works on 3D arrays where all dimensions are equal")
        in_cube = ox * oy * oz
        new_cube = dim**3
        binx = (in_cube / new_cube) ** (1.0 / 3)

        return bin_axis(
            bin_axis(bin_axis(in_array, 0, binx, binx), 1, binx, binx), 2, binx, binx
        )


def threed_array_to_montage(raw, ncols, boxsize, outname=None, cmap=None):
    """Make a single array with tiled images

    Args:
        raw (array): 3D input array
        ncols (int): Number of columns desired in the montage
        boxsize (int): Desired size of each panel in px
        cmap (str): colormap to apply if any

    Returns:
        array: The images tiles in an array of shape [x, ncols]
    """

    # calculate the number of rows and create target array
    n_rows = raw.shape[0] // ncols
    final_row = raw.shape[0] % ncols != 0
    boxsize = boxsize if boxsize < raw.shape[1] else raw.shape[1]

    # get a list of the individual images and normalize them
    if boxsize < raw.shape[1]:
        individual_imgs = [
            bin_array_to_size(raw[n, :, :], boxsize) for n in range(raw.shape[0])
        ]
    else:
        individual_imgs = [raw[n, :, :] for n in range(raw.shape[0])]

    normed_imgs = []
    if not cmap:
        for x in individual_imgs:
            normed = np.nan_to_num((x - np.min(x)) / np.ptp(x))
            intarray = np.full(normed.shape, 255)
            normed = normed * intarray
            normed_imgs.append(normed)
    if cmap:
        for x in individual_imgs:
            tocolour = (x - np.min(x)) / np.ptp(x)
            cm = get_cmap(cmap)
            coloured = cm(tocolour)
            img = (coloured[:, :, :3] * 255).astype(np.uint8)
            normed_imgs.append(img)

    # adjust in case the box size was shrunk by 1 px in binnng
    bindim = normed_imgs[0].shape
    if not cmap:
        target_array = np.zeros(
            [(n_rows + int(final_row)) * bindim[0], ncols * bindim[1]], dtype=np.int8
        )
    else:
        target_array = np.zeros(
            [(n_rows + int(final_row)) * bindim[0], ncols * bindim[1], 3],
            dtype=np.uint8,
        )
    xpos, ypos = 0, 0
    for img in normed_imgs:
        ystart = ypos * bindim[0]
        yend = (ypos + 1) * bindim[0]
        xstart = xpos * bindim[1]
        xend = (xpos + 1) * bindim[1]
        xpos += 1
        if xpos % ncols == 0:
            xpos = 0
            ypos += 1
        target_array[ystart:yend, xstart:xend] = img

    if outname is not None:
        outimg = Image.fromarray(target_array)
        outimg = outimg.convert("RGB")
        outimg.save(outname)

    return target_array


def bin_mrc_map(in_map, dim, out_name):
    """Bin a 2D or 3D mrc

    Args:
        in_map (str): Path to the map
        dim (int): Size of desired map, edge length in pixels
            assumes map is cubic
        out_name (str): Path to output file, will convert to png
            if that extension is specified

    Raises:
        ValueError: If the output name does not end with .mrc or .png

    """
    # read the old map
    with mrcfile.open(in_map) as mrc:
        raw = mrc.data
        old_voxel = float(mrc.voxel_size.item(0)[0])
        old_dim = float(raw.shape[0])

    # bin the data
    binned = bin_array_to_size(
        raw,
        dim,
    )

    # calculate the new voxel size
    new_dim = float(binned.shape[0])
    scale = old_dim / new_dim
    new_voxel = old_voxel * scale

    # write the new map
    ext = os.path.splitext(in_map)[1]
    outdir = os.path.dirname(out_name)
    if not os.path.isdir(outdir):
        print(f"*****MAKEOD {outdir}")
        os.makedirs(outdir)
    if ext == ".mrc":
        with mrcfile.new(out_name, overwrite=True) as mrc:
            mrc.set_data(binned)
            mrc.voxel_size = new_voxel
    elif ext == ".png":
        out_img = Image.fromarray(binned)
        out_img = out_img.convert("RGB")
        out_img.save(out_name)
    else:
        raise ValueError("Output name must end with .png or .mrc")


def central_slice_3D_mrcs(in_mrcs, output_dir, out_size, thick=10):
    """Make central slices of an 3D mrc

    Writes a mrc stack with the x, y, and z slices for each map in order

    Args:
        in_mrcs (list): Paths to the input files, should be cubic dimensions
        output_dir (str): Path to the output dir
        out_size (int): Desired size of the output array in px
        thick (int): number of pixels to pad the slice by on either side

    Returns:
        str: The path of the output file
    """
    stacklist = []
    for in_mrc in in_mrcs:
        with mrcfile.open(in_mrc) as the_mrc:
            dat = the_mrc.data
        cent = int(dat.shape[0] * 0.5)
        xd = np.sum(dat[:, :, cent - thick : cent + thick], axis=2)
        yd = np.sum(dat[:, cent - thick : cent + thick, :], axis=1)
        zd = np.sum(dat[cent - thick : cent + thick, :, :], axis=0)
        xdd, ydd, zdd = (
            xd.copy().clip(min=0),
            yd.copy().clip(min=0),
            zd.copy().clip(min=0),
        )

        xslice = bin_and_norm_array(xdd, out_size)
        yslice = bin_and_norm_array(ydd, out_size)
        zslice = bin_and_norm_array(zdd, out_size)
        stacklist.extend([xslice, yslice, zslice])

    outstack = np.stack(stacklist).astype(np.int8)
    out_name = os.path.basename(in_mrc).split(".")[0] + "_slices.mrcs"
    outfile = os.path.join(output_dir, out_name)
    with mrcfile.new(outfile, overwrite=True) as outmrcs:
        outmrcs.set_data(outstack)
    return outfile
