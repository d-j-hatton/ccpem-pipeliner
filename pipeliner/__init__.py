from pkg_resources import get_distribution
from pipeliner.api import env_setup

__version__ = get_distribution("pipeliner").version

# Set python environment for project - set user specified bash scripts
try:
    env_setup.set_python_environment()
except Exception:
    print("WARNING: unable to set pipeliner python environment")
