#!/usr/bin/env python
import sys
import os
import shutil
import subprocess
from itertools import product
from glob import glob
from time import sleep
from typing import List
from pipeliner.jobstar_reader import JobStar
from pipeliner.jobstar_reader import modify_jobstar
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_options import TRUES
from pipeliner.data_structure import JOBSTATUS_SUCCESS, JOBSTATUS_FAIL


class BenchMarkRun(object):
    def __init__(self, homedir: str):
        self.homedir = homedir
        self.templates = glob("benchmark_templates/*_job.star")
        self.templates.sort(key=lambda x: os.path.basename(x))
        self.do_parallel = True
        for template in self.templates:
            tem_js = JobStar(template)
            ops = tem_js.get_all_options()
            for opt in ops:
                if opt == "do_queue" and ops[opt].lower() not in TRUES:
                    self.do_parallel = False
                    break
        self.template_dicts: dict = {}
        self.jobtypes: List[str] = []
        self.jobnames: List[str] = []

    def read_template_file(self, filename: str):
        tem_js = JobStar(filename)
        options = tem_js.get_all_options()
        test_params = {}
        for param in options:
            val = options[param]
            if len(val) >= 2:
                if val[0] == "[" and val[-1] == "]":
                    test_params[param] = [str(x) for x in val[1:-1].split(",")]
        combos, param_list = [], []
        if len(test_params) == 0:
            return []
        for pgroup in test_params:
            combos.append(test_params[pgroup])
            param_list.append(pgroup)
            param_combos = list(product(*combos))
        final_combos = []
        for combo in param_combos:
            combo_dict = {}
            for n, p in enumerate(param_list):
                combo_dict[p] = combo[n]
            final_combos.append(combo_dict)
        self.template_dicts[filename] = final_combos
        self.jobtypes.append(options["_rlnJobTypeLabel"])

    def make_template_permutations(self):
        all_tems = [self.template_dicts[tem] for tem in self.templates]
        self.all_tem_combos = [x for x in product(*all_tems)]
        self.runs = [f"bmrk{x:03d}" for x in range(len(self.all_tem_combos))]

        # make the test dirs
        for n, dirname in enumerate(self.runs):
            os.makedirs(dirname)
            datafiles = glob("benchmark_data/*")
            for f in datafiles:
                slink = os.path.join(dirname, os.path.basename(f))
                os.symlink(os.path.abspath(f), slink)
            for m, template in enumerate(self.templates):
                outfile = os.path.join(dirname, os.path.basename(template))
                modify_jobstar(template, self.all_tem_combos[n][m], outfile)
                sys.stdout.write(".")
                sys.stdout.flush()
        sys.stdout.write("\n")
        sys.stdout.flush()

    def setup_bmrk_schedule(self):
        for bmrk in self.runs:
            os.chdir(bmrk)
            proj = PipelinerProject()
            for job in self.templates:
                jobname = proj.schedule_job(os.path.basename(job))
                if jobname not in self.jobnames:
                    self.jobnames.append(jobname)
                sys.stdout.write(".")
                sys.stdout.flush()
            os.chdir(self.homedir)

    def run_schedule_parallel(self, bmname: str):
        os.chdir(bmname)
        jobs = [str(x) for x in range(1, len(self.templates) + 1)]
        subprocess.Popen(
            [
                "CL_pipeline",
                "--run_schedule",
                "--name",
                bmname,
                "--jobs",
                *jobs,
            ]
        )
        os.chdir(self.homedir)

    def run_schedule_sequential(self, bmname: str):
        os.chdir(bmname)
        jobs = [str(x) for x in range(1, len(self.templates) + 1)]
        proj = PipelinerProject()
        jobs_run = proj.parse_proclist(jobs)
        proj.run_schedule(bmname, jobs_run)
        os.chdir(self.homedir)


def main():
    """Utility for running pipeliner functions from the command-line"""

    # check and clean up
    if not os.path.isdir("benchmark_templates"):
        raise ValueError("benchmark_templates directory not found")
    olddirs = glob("bmrk???")
    for od in olddirs:
        shutil.rmtree(od)

    sesh = BenchMarkRun(os.getcwd())

    # read in the template files
    for tf in sesh.templates:
        sesh.read_template_file(tf)

    # calculate the permutations for all templates and write files
    sys.stdout.write("BMRK: Building template permutations")
    sesh.make_template_permutations()

    # setup the schedules
    sys.stdout.write("BMRK: Setting up schedules")
    sys.stdout.flush()
    sesh.setup_bmrk_schedule()
    sys.stdout.write("\n")
    sys.stdout.flush()

    # decide if to do parallel runs
    print(
        f"BMRK: {len(sesh.runs)} projects will be run with {len(sesh.templates)}"
        " jobs each"
    )
    if sesh.do_parallel:
        print(
            "BMRK: This benchmark run appears to be suitable for parallel execution"
            "\nBMRK: No computationally intensive jobs will be run locally"
        )
        do_p = input("BMRK: Run the jobs in parallel? (Y/N) ")
        if do_p.lower() not in ["yes", "y"]:
            sesh.do_parallel = False

    if not sesh.do_parallel:
        print(
            "BMRK: If computationally intensive jobs are run locally "
            "parallel execution is not possible"
            "\nBMRK: The jobs will be run in series, this may take a VERY long time"
        )
        contin = input("BMRK: Continue running? (Y/N) ")
        if contin.lower() not in ["yes", "y"]:
            print("BMRK: User exit")
            return

    # run the schedules
    print("BMRK: Running benchmark jobs")
    for bmname in sesh.runs:
        if sesh.do_parallel:
            sesh.run_schedule_parallel(bmname)
        else:
            sesh.run_schedule_sequential(bmname)

    # check that all the schedules have finished if running in parallel
    if sesh.do_parallel:
        # give it five sec for the first schedule to start running
        sleep(5)

        sched_files = [f"{x}/RUNNING_PIPELINER_default_{x}" for x in sesh.runs]
        files_check = [os.path.isfile(x) for x in sched_files]
        while any(files_check):
            sleep(1)
            files_check = [os.path.isfile(x) for x in sched_files]

    print("BMRK: All benchmarking jobs finished")

    # get the run times
    runtimes = {}
    for bmrkrun in sesh.runs:
        runtimes[bmrkrun] = {}
        os.chdir(bmrkrun)
        proj = PipelinerProject()
        all_jobs = proj.pipeline.process_list
        for job in all_jobs:
            runtimes[bmrkrun][job.name] = proj.get_job_runtime(job.name)

        os.chdir(sesh.homedir)

    # files to write the results
    results_file = open("benchmark_summary.csv", "w")
    results_file.write(
        "Benchmark,JobName,JobStatus,JobType,ParameterVariations,TimeReal,"
        "TimeUser,TimeSys\n"
    )
    full_results_file = open("benchmark_full_results.csv", "w")
    full_results_file.write(
        "Benchmark,JobName,JobStatus,JobType,ParameterVariations,TimeReal,"
        "TimeUser,TimeSys,CommandRun\n"
    )

    # get the results, format and write to the files
    for n, bmrk in enumerate(sesh.runs):
        bmrk_total = [0, 0, 0]
        was_success = True
        for m, job in enumerate(sesh.jobnames):
            results = (
                bmrk,
                sesh.jobnames[m],
                runtimes[bmrk][job][4],
                sesh.jobtypes[m],
                sesh.all_tem_combos[n],
                runtimes[bmrk][job][0],
                runtimes[bmrk][job][1],
                runtimes[bmrk][job][2],
            )
            results_file.write(
                ",".join([str(x).replace(",", " ") for x in results]) + "\n"
            )
            for command in runtimes[bmrk][job][3]:
                comtime = runtimes[bmrk][job][3][command]
                basic_info = ",".join([str(x).replace(",", " ") for x in results[:5]])
                full_results_file.write(
                    f"{basic_info},{comtime[0]},{comtime[1]},{comtime[2]},{command}\n"
                )

            # note success/failure of main job
            if runtimes[bmrk][job][4] != JOBSTATUS_SUCCESS:
                was_success = False
            # see if any run times measurements raised errors
            for m in range(3):
                if runtimes[bmrk][job][m] is not None:
                    bmrk_total[m] += runtimes[bmrk][job][m]
                else:
                    bmrk_total[m] = "Job ended with error"
                    break

        results_file.write(
            f"{bmrk},TOTAL:,,,,{bmrk_total[0]},{bmrk_total[1]},{bmrk_total[2]}\n"
        )
        print(
            f"{bmrk}    {JOBSTATUS_SUCCESS if was_success else JOBSTATUS_FAIL}"
            f"    {round(bmrk_total[0], 2):.2f}    {sesh.all_tem_combos[n]}"
        )

    results_file.close()
    full_results_file.close()


if __name__ == "__main__":
    main()
