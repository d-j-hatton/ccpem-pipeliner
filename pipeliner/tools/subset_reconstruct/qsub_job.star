# This is a special job.star file to set queue and running parameters 
# for the CCPEM-pipeliner subset-reconstruct script

# Set these parameters for running the refinement jobs. 
# These jobs cannot be run with GPU so set the submission
# script and number of MPIs and threads accordingly

data_joboptions_values
loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_queue'	    Yes
'min_dedicated'     1 
'nr_mpi'            5 
'nr_pool'           3 
'nr_threads'        1 
'scratch_dir'       "" 
qsub         	    sbatch 
qsubscript          "/home/vol07/scarf957/sub_scripts/slurm_template.sh" 
queuename           scarf
'other_args'        ""


data_job	# do not modify these parameters

_rlnJobTypeLabel    relion.refine3d
_rlnJobIsContinue    0