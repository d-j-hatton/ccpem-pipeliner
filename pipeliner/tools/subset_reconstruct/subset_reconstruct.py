#!/usr/bin/env python

import argparse
import sys
import shutil
import os
import matplotlib.pyplot as plt
import numpy as np

from math import log, sqrt
from time import sleep
from pipeliner.jobstar_reader import RelionStarFile, JobStar
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.utils import make_pretty_header, get_pipeliner_root
from pipeliner.job_options import FALSES, TRUES
from pipeliner.data_structure import ABORT_FILE, SUCCESS_FILE, FAIL_FILE
from pipeliner.api.api_utils import job_success


class SsRec(object):
    def __init__(self):
        self.h1_particles = None
        self.h2_particles = None
        self.combined_parts = None
        self.reconstruct_job = None
        self.post_job = None


class SsRecAnalysis(object):
    def __init__(self):
        self.particle_counts = []
        self.resos = []


def job_succeeded(job_name):
    if not job_success(job_name):
        print(f"SSREC: ERROR: job {job_name} failed")
        sys.exit("SSREC: Aborting...")


def get_arguments():

    parser = argparse.ArgumentParser(description="CCPEM Pipeliner bfactor Tool")
    parser._optionals.title = "Arguments"

    parser.add_argument(
        "--post_job",
        help=("A Relion postprocess job"),
        nargs="?",
        metavar="job name",
    )

    parser.add_argument(
        "--iter",
        help="Number of reconstructions to produce",
        nargs="?",
        metavar="int",
    )

    parser.add_argument(
        "--shrink",
        help="Percentage by which to shrink the dataset on each iteration",
        nargs="?",
        metavar="int",
    )

    parser.add_argument(
        "--n_parallel_jobs",
        help="Max number of jobs to run in parallel, if not specified all jobs"
        " will be run simultaneously",
        nargs="?",
        const=-1,
        metavar="int",
    )
    parser.add_argument(
        "--analysis_only",
        help="If the program has already been run, just display the results",
        action="store_true",
    )

    return parser


class SubsetReconstruct(object):
    def __init__(self, post_job, iters, shrink, max_parallel):
        self.post_job = post_job
        post_jos = JobStar(os.path.join(post_job, "job.star")).get_all_options()
        self.refine_job = os.path.dirname(post_jos["fn_in"])
        self.data_file = os.path.join(self.refine_job, "run_data.star")
        self.particle_count = RelionStarFile(self.data_file).count_block("particles")
        self.proj = PipelinerProject()
        self.subset1 = None
        self.subset2 = None
        self.test_subsets = {}
        self.iters = int(iters)
        shrink = float(shrink)
        self.shrink = (100 - shrink) / 100 if shrink > 1 else 1 - shrink
        qs_params = JobStar("qsub_job.star").get_all_options()
        self.running_jobs = set()
        self.do_parallel = int(max_parallel)
        if self.do_parallel < 1:
            self.do_parallel = self.iters + 1
        if qs_params["do_queue"] in FALSES:
            print(
                "SSREC: WARNING: Parallel jobs are not being sent to a queue.\n"
                "SSREC: This may be VERY resource intensive and might crash\n"
            )
            really_do_it = input("SSREC: Continue? (Y/N) ")
            if really_do_it.lower() not in TRUES:
                sys.exit("SSREC: Goodbye")

        # get the refinement params
        refine_jobstar = os.path.join(self.refine_job, "job.star")
        self.refine_params = JobStar(refine_jobstar).get_all_options()

        # set up the qsub parameters
        for opt in qs_params:
            self.refine_params[opt] = qs_params[opt]

        # skip the alignment
        self.refine_params["other_args"] += " --skip_align"
        self.refine_params["use_gpu"] = "No"

    def run_subset_jobs(self):
        print("SSREC: Preparing halfsets")
        self.subset1 = self.proj.run_job(
            {
                "_rlnJobTypeLabel": "relion.select.onvalue",
                "fn_data": self.data_file,
                "select_label": "rlnRandomSubset",
                "select_maxval": 1.1,
                "select_minval": 0.9,
            }
        )
        job_succeeded(self.subset1)

        self.subset2 = self.proj.run_job(
            {
                "_rlnJobTypeLabel": "relion.select.onvalue",
                "fn_data": self.data_file,
                "select_label": "rlnRandomSubset",
                "select_maxval": 2.1,
                "select_minval": 1.9,
            }
        )
        job_succeeded(self.subset2)

        self.proj.set_alias(self.subset1, "subset_1")
        self.proj.set_alias(self.subset2, "subset_2")

    def get_test_subsets(self):
        self.test_subsets[self.particle_count] = SsRec()
        pc = self.particle_count
        i = 0
        while i < self.iters:
            pc = int(pc * self.shrink)
            self.test_subsets[pc] = SsRec()
            i += 1

    def prepare_test_subsets(self):
        print("SSREC: Preparing reconstruction subsets")
        data_file_h1 = os.path.join(self.subset1, "particles.star")
        data_file_h2 = os.path.join(self.subset2, "particles.star")

        for i in self.test_subsets:
            ss_h1_job = self.proj.run_job(
                {
                    "_rlnJobTypeLabel": "relion.select.split",
                    "split_size": int(i * 0.5),
                    "nr_split": 1,
                    "do_random": "True",
                    "fn_data": data_file_h1,
                }
            )
            job_succeeded(ss_h1_job)

            self.proj.set_alias(ss_h1_job, f"{i:07d}_half1")
            data_file_h1 = os.path.join(ss_h1_job, "particles_split1.star")
            self.test_subsets[i].h1_particles = data_file_h1

            ss_h2_job = self.proj.run_job(
                {
                    "_rlnJobTypeLabel": "relion.select.split",
                    "split_size": int(i * 0.5),
                    "nr_split": 1,
                    "do_random": "True",
                    "fn_data": data_file_h2,
                }
            )
            job_succeeded(ss_h2_job)

            self.proj.set_alias(ss_h2_job, f"{i:07d}_half2")
            data_file_h2 = os.path.join(ss_h2_job, "particles_split1.star")
            self.test_subsets[i].h2_particles = data_file_h2

        print("SSREC: Finished Preparing reconstruction subsets")
        print(f"SSREC: Made {len(self.test_subsets)} subsets")
        print(f"SSREC: Largest subset: {max(self.test_subsets)} particles")
        print(f"SSREC: Smallest subset: {min(self.test_subsets)} particles")

    def recombine_sets(self):
        print("SSREC: Preparing combined test sets")
        for i in self.test_subsets:
            comb_job = self.proj.run_job(
                {
                    "_rlnJobTypeLabel": "relion.joinstar.particles",
                    "fn_part1": self.test_subsets[i].h1_particles,
                    "fn_part2": self.test_subsets[i].h2_particles,
                }
            )
            self.test_subsets[i].combined_parts = os.path.join(
                comb_job, "join_particles.star"
            )

    def check_recs_finished(self):
        current = list(self.running_jobs)
        for job in current:
            checks = [
                os.path.isfile(f"{job}{x}")
                for x in (SUCCESS_FILE, FAIL_FILE, ABORT_FILE)
            ]
            if any(checks):
                self.running_jobs.remove(job)
                print(f"SSREC: {job} finished")
        self.proj.pipeline.check_process_completion()
        sleep(1)

    def do_reconstructions(self):
        print(f"SSREC: Starting {len(self.test_subsets)} reconstructions")
        print(f"SSREC: Will perform {self.do_parallel} parallel reconstructions")
        for i in self.test_subsets:

            # check of the max number of jobs are running
            while len(self.running_jobs) == self.do_parallel:
                self.check_recs_finished()

            # run the next job
            self.refine_params["fn_img"] = self.test_subsets[i].combined_parts

            job_name = f"Refine3D/job{self.proj.pipeline.job_counter:03d}/"
            self.running_jobs.add(job_name)
            self.test_subsets[i].reconstruct_job = job_name
            self.proj.run_job(self.refine_params, wait_for_queued=False)
            self.proj.set_alias(job_name, f"{i:07d}")

            # check of the max number of jobs are running
            while len(self.running_jobs) == self.do_parallel:
                self.check_recs_finished()
        while len(self.running_jobs) > 0:
            self.check_recs_finished()

    def do_postprocessing(self):
        print("SSREC: Doing postprocessing jobs")
        post_jobstar = os.path.join(self.post_job, "job.star")
        post_params = JobStar(post_jobstar).get_all_options()
        for i in self.test_subsets:
            job = self.test_subsets[i]
            rec_job = os.path.join(job.reconstruct_job, "run_half1_class001_unfil.mrc")
            post_params["fn_in"] = rec_job
            post_job = self.proj.run_job(post_params)
            self.proj.set_alias(post_job, f"{i:07d}")
            self.test_subsets[i].post_job = post_job

        while len(self.running_jobs) > 0:
            self.check_recs_finished()
        print("SSREC: PostProcessing jobs finished")

    def do_fscs(self):
        for i in self.test_subsets:
            ppjob = self.test_subsets[i].post_job
            fsc_file = os.path.join(ppjob, "postprocess_fsc.dat")
            with open("fsc_files.txt", "a") as fscf_out:
                fscf_out.write(f"{i} {fsc_file}\n")


def get_fsc_values(fsc_file):
    try:
        with open(fsc_file, "r") as fscdat:
            fsc_data = fscdat.readlines()
        resos, fscs = [], []
        for line in fsc_data:
            sline = line.split()
            resos.append(float(sline[0]))
            fscs.append(float(sline[1]))
        return [resos, fscs]
    except FileNotFoundError:
        return [None, None]


def get_fsc_cutoffs(fsc_values):
    point5 = None
    point143 = None
    if fsc_values is None:
        return "Error", "Error"
    for line in fsc_values:
        sline = line.split()
        if float(sline[1]) <= 0.5 and point5 is None:
            point5 = float(sline[0])
        if float(sline[1]) <= 0.143 and point143 is None:
            point143 = float(sline[0])
    if point5 is None:
        point5 = 0
    if point143 is None:
        point143 = 0
    return point5, point143


def display_results():
    analysis_sesh = SsRecAnalysis()
    print(
        f"{'SSREC: Particle Number':>22s}  {'FSC(0.143)':>10s}  "
        f"{'FSC(0.5)':>10s}  {'b-factor':>10s}"
    )
    with open("fsc_files.txt", "r") as fscdat:
        fsc_files = fscdat.readlines()
    for f in fsc_files:
        fs = f.split()
        try:
            with open(fs[1], "r") as fscf:
                fsc_vals = fscf.readlines()
        except FileNotFoundError:
            fsc_vals = None
        point5, point143 = get_fsc_cutoffs(fsc_vals)

        pp_log = os.path.join(os.path.dirname(fs[1]), "run.out")
        bfact = None
        try:
            with open(pp_log, "r") as pp_runout:
                pp_lines = pp_runout.readlines()
                for line in pp_lines:
                    linesplit = line.split()
                    if linesplit[0:3] == ["+", "apply", "b-factor"]:
                        bfact = linesplit[-1]
        except FileNotFoundError:
            bfact = "n/a"
        if bfact is None:
            bfact = "n/a"

        if "Error" not in (point5, point143):
            print(
                f"SSREC{fs[0]:>17s}  {1/point143:>10f}  {1/point5:>10f}  {bfact:>10s}"
            )
            analysis_sesh.particle_counts.append(int(fs[0]))
            analysis_sesh.resos.append(1 / point143)
        else:
            print(f"SSREC{fs[0]:>17s}  {point143:>10s}  {point5:>10s}  {bfact:>10s}")
    return analysis_sesh


def do_bf_fit(a_s, n):
    parts = a_s.particle_counts[:-n] if n > 0 else a_s.particle_counts
    resos = a_s.resos[:-n] if n > 0 else a_s.resos
    log_all_parts = [log(x) for x in parts]
    reso_sq = [1 / (x * x) for x in resos]
    coefs, stats = np.polyfit(log_all_parts, reso_sq, 1, full=True)[:2]
    sst = sum([(x - np.mean(reso_sq)) ** 2 for x in reso_sq])
    slope, intercept = coefs
    bfactor = 2.0 / slope
    nparts = len(log_all_parts)
    return (
        log_all_parts,
        reso_sq,
        slope,
        intercept,
        bfactor,
        nparts,
        1 - (stats[0] / sst),
    )


def make_plots(analysis_sesh):
    r = 0
    n = 0
    print("SSREC: Finding best fit")
    print(f"SSREC: {'n points':>8s}  {'R^2':<6s}")
    while r < 0.95 and n <= len(analysis_sesh.particle_counts) - 3:
        log_all_parts, reso_sq, slope, intercept, bfactor, nparts, r = do_bf_fit(
            analysis_sesh, n
        )
        print(f"SSREC: {len(log_all_parts):>8d}  {round(r, 4)}")
        n += 1
    if n <= len(analysis_sesh.particle_counts) - 3:
        print(f"SSREC: Fit is good enough using {len(log_all_parts)} points")
    else:
        new_n = len(analysis_sesh.particle_counts) - 4
        print("SSREC: No set of points had R^2 > 0.95 using the last three points")
        log_all_parts, reso_sq, slope, intercept, bfactor, nparts, r = do_bf_fit(
            analysis_sesh, new_n
        )

    print(f"SSREC: B-factor fit R = {r}")
    print(f"SSREC: Estimated b-factor from {nparts} points is {bfactor:.2f}")
    print(
        f"SSREC: Estimated resolution = 1 / Sqrt(2 / "
        f"{bfactor:.3f} * ln(#Particles) + {intercept:.3f})\n"
        "SSREC: IF this trend holds, you will get:"
    )

    pred_parts, pred_res = [], []
    for x in (1, 1.5, 2, 4, 8, 10, 20):
        cnp = int(max(analysis_sesh.particle_counts) * x)
        pred_parts.append(cnp)
        res = 1 / sqrt(slope * log(cnp) + intercept)
        pred_res.append(res)
        print(
            f"SSREC:    {res:.1f} \u212B from {cnp} particles ({x}x the current "
            "particle number)"
        )

    # make the plot
    ax1 = plt.subplot2grid([2, 3], (0, 0))
    ax2 = plt.subplot2grid([2, 3], (1, 0))
    ax3 = plt.subplot2grid([2, 3], (0, 1), rowspan=2, colspan=2)

    # fig, ax = plt.subplots(2)
    cuts5, cuts143 = [], []
    with open("fsc_files.txt", "r") as fscdat:
        fsc_files = fscdat.readlines()
    part_nos = []
    for f in fsc_files:
        fs = f.split()
        resos, corrs = get_fsc_values(fs[1])
        try:
            with open(fs[1], "r") as fscf:
                fsc_vals = fscf.readlines()
        except FileNotFoundError:
            fsc_vals = None

        if None not in (resos, corrs):
            resos = [1 / x for x in resos]
            ax3.plot(resos, corrs, label=fs[0])
            point5, point143 = get_fsc_cutoffs(fsc_vals)
            cuts5.append(point5)
            cuts143.append(point143)
            part_nos.append(int(f[0]))

    ax3.set_xlim(0, 20)
    ax3.invert_xaxis()
    ax3.hlines(0.143, 0, 20, linestyles="dashed", color="grey")
    ax3.hlines(0.5, 0, 20, linestyles="dashed", color="grey")
    ax3.set_title("Fsc by particle number")
    ax3.set_xlabel("resolution")
    ax3.set_ylabel("fsc")
    ax3.legend()

    # make the plot
    ax1.scatter(log_all_parts, reso_sq)
    ys = [slope * x + intercept for x in log_all_parts]
    ax1.plot(log_all_parts, ys, color="orange")
    ax1.set_xlabel("ln(particles)")
    ax1.set_ylabel("1/resolution^2")
    ax1.set_title("b-factor fit")

    ax2.plot(pred_parts, pred_res, color="red", label="predicted")
    ax2.plot(analysis_sesh.particle_counts, analysis_sesh.resos, label="actual")
    ax2.set_ylabel("resolution (A)")
    ax2.set_xlabel("particle number")
    ax2.legend()

    plt.tight_layout()
    plt.savefig("results_bfactor.png")
    print("SSREC: Output graphs written to: results_bfactor.png\n" "SSREC: Finished!")


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    for arg in (args.iter, args.shrink, args.post_job):
        if arg is None and not args.analysis_only:
            parser.print_help()

    print(make_pretty_header("CCPEM-pipeliner subset reconstruction tool"))

    # find the qsub params
    if not os.path.isfile("qsub_job.star"):
        print("SSREC: ERROR: File qsub_job.star is missing!")
        print(
            "SSREC: This file is required for setting the running parameters for"
            "jobs run by this program"
        )
        print("SSREC: The file has been copied in as qusub_job.star.edit")
        print(
            "SSREC: Edit it, rename to remove the .edit extension and then "
            "re-run the program"
        )
        qsub_file = os.path.join(
            get_pipeliner_root(), "tools/subset_reconstruct/qsub_job.star"
        )
        shutil.copy(qsub_file, "qsub_job.star.edit")
        sys.exit("SSREC: Quitting...")

    if not args.analysis_only:
        if not os.path.isdir(args.post_job):
            print(f"SSREC: ERROR: Directory {args.post_job} is missing!")
            sys.exit("SSREC: Quitting...")

    if not args.analysis_only:
        sesh = SubsetReconstruct(
            args.post_job,
            args.iter,
            args.shrink,
            args.n_parallel_jobs,
        )
        sesh.run_subset_jobs()
        sesh.get_test_subsets()
        sesh.prepare_test_subsets()
        sesh.recombine_sets()
        sesh.do_reconstructions()
        sesh.do_postprocessing()
        sesh.do_fscs()

    analysis_sesh = display_results()
    make_plots(analysis_sesh)


if __name__ == "__main__":
    main()
