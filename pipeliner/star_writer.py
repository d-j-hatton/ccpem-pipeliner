#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""
--------------------------
Starfile writing utilities
--------------------------

Writes star files in the same style as RELION
"""
from gemmi import cif
from typing import IO
from pipeliner import __version__

COMMENT_LINE = f"Relion version 4.0 / CCP-EM_pipeliner vers {__version__}"


def write(doc, filename: str, commentline: str = COMMENT_LINE):
    """Write a Gemmi CIF document to a RELION-style STAR file.

    Args:
        doc (:class:`gemmi.cif.Cif`): The data to write out
        filename (str): The name of the file to write the data to
        commentline (str): The comment line to put in the written star file
    """
    with open(filename, "w") as star_file:
        write_to_stream(doc, star_file, commentline)


def write_to_stream(doc, out_stream: IO[str], commentline: str = COMMENT_LINE):
    """Write a Gemmi CIF document to an output stream using RELION's output style.

    Args:
        doc (:class:`gemmi.cif.Cif`): The data to write out
        out_stream (str): The name of the file to write the data to
        commentline (str): The comment line to put in the written star file
    """
    for block in doc:
        if commentline is not None:
            out_stream.write("\n# " + commentline + "\n")
        out_stream.write("\ndata_{}\n\n".format(block.name))
        for item in block:
            if item.pair:
                out_stream.write("{}    {}\n".format(*item.pair))
            elif item.loop:
                out_stream.write("loop_ \n")
                loop = item.loop
                for ii, tag in enumerate(loop.tags, start=1):
                    out_stream.write("{} #{} \n".format(tag, ii))
                for row in range(loop.length()):
                    for col in range(loop.width()):
                        value = loop.val(row, col)
                        # Annoyingly, RELION's output uses 12-char wide fields except
                        # if value is "None"
                        if value == "None":
                            out_stream.write("{:>10} ".format(value))
                        # Need to quote empty strings
                        # (can normally use gemmi.cif.quote() for this but not if we
                        # want strict RELION-style output)
                        elif value == "":
                            out_stream.write("{:>12} ".format('""'))
                        else:
                            out_stream.write("{:>12} ".format(value))
                    out_stream.write("\n")
            out_stream.write(" \n")


def write_jobstar(in_dict: dict, out_fn: str):
    """Write a job.star file from a dictionary of options

    Args:
        in_dict (dict): Dict of job option keys and values
        out_fn (str): Name of the file to write to

    """
    jobstar = cif.Document()

    # make the job block - don't know why I have to separate these
    # two functions but Gemmi seg faults if you don't....
    job_block = jobstar.add_new_block("job")

    for param in in_dict:
        val = in_dict[param]
        # get the two that go to the job block
        if param in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            job_block.set_pair(param, val)

    # make the options block
    jobop_block = jobstar.add_new_block("joboptions_values")
    jobop_loop = jobop_block.init_loop(
        "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
    )

    for param in in_dict:
        val = in_dict[param]
        # get the two that go to the job block
        if param not in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            jobop_loop.add_row([cif.quote(param), cif.quote(val)])

    write(jobstar, out_fn)
