#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner.jobs.relion.subtract_job import (
    INPUT_NODE_MASK,
    INPUT_NODE_PARTS,
    INPUT_NODE_OPTIMISER,
    OUTPUT_NODE_PARTS_SUB,
    OUTPUT_NODE_PARTS,
)


class SubtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_subtract_commands(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_relionstyle_name(self):
        """Check correct interpretation of ambiguous jobname"""
        general_get_command_test(
            self,
            "Subtract",
            "subtract_relionstyle.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_diff_parts(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract_diff_parts.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --data myother_particles.star"
                " --o Subtract/job004/ --new_box 420 --pipeline_control Subtract/job00"
                "4/"
            ],
        )

    def test_get_subtract_commands_revert(self):
        os.mkdir("Subtract")
        os.mkdir("Subtract/job004")
        general_get_command_test(
            self,
            "Subtract",
            "subtract_revert.job",
            4,
            {"these_were_original.star": OUTPUT_NODE_PARTS},
            {"original.star": OUTPUT_NODE_PARTS},
            [
                "relion_particle_subtract --revert these_were_original.star"
                " --o Subtract/job004/ --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_revert_relionstyle_name(self):
        """Test conversion of relion style ambiguous job name"""
        os.mkdir("Subtract")
        os.mkdir("Subtract/job004")
        general_get_command_test(
            self,
            "Subtract",
            "subtract_revert_relionstyle_job.star",
            4,
            {"Refine3D/job010/run_data.star": OUTPUT_NODE_PARTS},
            {"original.star": OUTPUT_NODE_PARTS},
            [
                "relion_particle_subtract --revert Refine3D/job010/run_data.star"
                " --o Subtract/job004/ --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_centonmask(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract_centonmask.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ "
                "--recenter_on_mask --new_box 420 --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_centoncoords(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract_centoncoords.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ "
                "--center_x 101 --center_y 102 --center_z 103 --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_additional_arg(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract_addarg.job",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
                "myother_particles.star": INPUT_NODE_PARTS,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_additional_arg_jobstar(self):
        general_get_command_test(
            self,
            "Subtract",
            "subtract_addarg_job.star",
            4,
            {
                "Refine3D/job500/run_it017_optimiser.star": INPUT_NODE_OPTIMISER,
                "MaskCreate/job501/mask.mrc": INPUT_NODE_MASK,
            },
            {"particles_subtracted.star": OUTPUT_NODE_PARTS_SUB},
            [
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job004/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
