#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.jobs.relion.maskcreate_job import (
    INPUT_NODE_MAP,
    OUTPUT_NODE_MASK,
)


class MaskCreateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_mask_create_commands(self):
        general_get_command_test(
            self,
            "MaskCreate",
            "maskcreate.job",
            4,
            {"Import/job001/emd_3488.mrc": INPUT_NODE_MAP},
            {"mask.mrc": OUTPUT_NODE_MASK},
            [
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job004/mask.mrc --lowpass 15 --angpix -1 "
                "--ini_threshold 0.005 --extend_inimask 0 --width_soft_edge 6 --j 12 "
                "--pipeline_control MaskCreate/job004/"
            ],
        )

    def test_get_mask_create_commands_with_jobstar(self):
        general_get_command_test(
            self,
            "MaskCreate",
            "maskcreate_job.star",
            4,
            {"Import/job001/emd_3488.mrc": INPUT_NODE_MAP},
            {"mask.mrc": OUTPUT_NODE_MASK},
            [
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job004/mask.mrc --lowpass 15 --angpix -1 --ini_threshold "
                "0.005 --extend_inimask 0 --width_soft_edge 6 --j 12 "
                "--pipeline_control MaskCreate/job004/"
            ],
        )

    def test_get_mask_create_commands_helical(self):
        general_get_command_test(
            self,
            "MaskCreate",
            "maskcreate_helical.job",
            5,
            {"Import/job001/emd_3488.mrc": INPUT_NODE_MAP},
            {"mask.mrc": OUTPUT_NODE_MASK},
            [
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job005/mask.mrc --lowpass 15 --angpix -1 --ini_threshold "
                "0.005 --extend_inimask 0 --width_soft_edge 6 --helix --z_percentage"
                " 0.3 --j 12 --pipeline_control MaskCreate/job005/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_maskcreate_generate_results(self):
        get_relion_tutorial_data(["MaskCreate", "Refine3D"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("MaskCreate/job020/")
        dispobjs = pipeline.get_process_results_display(proc)
        print(dispobjs[0].__dict__)
        assert dispobjs[0].__dict__ == {
            "maps": [
                "MaskCreate/job020/Thumbnails/mask.mrc",
                "MaskCreate/job020/Thumbnails/run_class001.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "models": [],
            "title": "Created mask overlaid on input map",
            "maps_data": "Threshold: 0.005; Extend: 0 px; Soft edge: 6 px",
            "models_data": "",
            "associated_data": [
                "MaskCreate/job020/mask.mrc",
                "Refine3D/job019/run_class001.mrc",
            ],
        }


if __name__ == "__main__":
    unittest.main()
