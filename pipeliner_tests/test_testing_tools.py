#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests.generic_tests import check_for_relion
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner_tests import test_data
from pipeliner.jobs.relion.class2D_job import (
    INPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_OPT,
)


class TestingToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_test(self):
        job_running_test(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "emd_3488_mask.mrc")),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_get_command_class2D_scratch_comb_em(self):
        job_generate_commands_test(
            os.path.join(self.test_data, "JobFiles/Class2D/class2D_scratch_comb.job"),
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job999/run "
                "--scratch_dir Fake_scratch_dir --pool 30 --pad 2 --ctf "
                "--iter 25 --tau2_fudge 2 --particle_diameter 200 --K 50 "
                "--flatten_solvent --zero_mask --center_classes --oversampling 1"
                " --psi_step 12.0"
                " --offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job999/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
