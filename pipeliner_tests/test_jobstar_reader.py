#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
from pipeliner.jobstar_reader import (
    JobStar,
    BodyFile,
    OutputNodeStar,
    RelionStarFile,
    modify_jobstar,
    StarfileCheck,
    get_job_type,
    star_loop_as_list,
    star_pairs_as_dict,
    compare_starfiles,
    convert_relion20_datafile,
)
from pipeliner.star_writer import COMMENT_LINE
import os
import tempfile
import shutil
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    read_pipeline,
    make_conversion_file_structure,
)
from pipeliner.jobs.relion import motioncorr_job, ctffind_job


class JobStarReaderTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories and get example file.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_reader(self):
        """make sure all the blocks are as expected"""
        jobdata = JobStar(
            os.path.join(self.test_data, "JobFiles/MaskCreate/maskcreate_job.star")
        )
        assert jobdata.count_blocks() == 2, jobdata.count_blocks()
        assert jobdata.count_pipeline() == 3, jobdata.count_pipeline()
        assert jobdata.count_jobopt() == 15, jobdata.count_jobopt()

    def test_return_option_pairs(self):
        """return the job options and values"""
        jobdata = JobStar(
            os.path.join(self.test_data, "JobFiles/MaskCreate/maskcreate_job.star")
        )
        options = jobdata.get_all_options()
        assert len(options) == 18, len(options)

    def test_read_bodyfile(self):
        """Test reading a body file for multibody refinement"""
        bildfile_data = BodyFile(
            os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star")
        )
        assert bildfile_data.count_bodies() == 3

    def test_correcting_reserved_words_changed(self):
        """Check reading a motioncorr file where the data labels
        have been changed to avoid using reserved words"""
        jobdata = JobStar(
            os.path.join(self.test_data, "StarFiles/motioncorr_changed.star")
        )
        jovals = jobdata.data.find_block("joboptions_values")
        joloop = jovals.find_loop("_rlnJobOptionVariable")
        assert list(joloop) == [
            "bfactor",
            "bin_factor",
            "do_dose_weighting",
            "do_own_motioncor",
            "do_queue",
            "dose_per_frame",
            "first_frame_sum",
            "fn_defect",
            "fn_gain_ref",
            "fn_motioncor2_exe",
            "gain_flip",
            "gain_rot",
            "gpu_ids",
            "group_for_ps",
            "group_frames",
            "input_star_mics",
            "last_frame_sum",
            "min_dedicated",
            "nr_mpi",
            "nr_threads",
            "other_args",
            "other_motioncor2_args",
            "patch_x",
            "patch_y",
            "pre_exposure",
            "qsub",
            "qsubscript",
            "noDW_save",
            "ps_save",
            "queuename",
        ]

    def test_correcting_reserved_words_quotated(self):
        """Check read a files that contains reserved words as labels
        but they have been quotated"""
        jobdata = JobStar(
            os.path.join(self.test_data, "StarFiles/motioncorr_quotated.star")
        )
        jovals = jobdata.data.find_block("joboptions_values")
        joloop = jovals.find_loop("_rlnJobOptionVariable")
        assert list(joloop) == [
            "bfactor",
            "bin_factor",
            "do_dose_weighting",
            "do_own_motioncor",
            "do_queue",
            "dose_per_frame",
            "first_frame_sum",
            "fn_defect",
            "fn_gain_ref",
            "fn_motioncor2_exe",
            "gain_flip",
            "gain_rot",
            "gpu_ids",
            "group_for_ps",
            "group_frames",
            "input_star_mics",
            "last_frame_sum",
            "min_dedicated",
            "nr_mpi",
            "nr_threads",
            "other_args",
            "other_motioncor2_args",
            "patch_x",
            "patch_y",
            "pre_exposure",
            "qsub",
            "qsubscript",
            '"save_noDW"',
            '"save_ps"',
            "queuename",
        ]

    def test_correcting_reserved_words_not_quotated(self):
        """Test reading a file where reserved words are used for data labels
        make sure the correction runs as expected"""

        test_file = os.path.join(
            self.test_data, "StarFiles/motioncorr_not_quotated.star"
        )

        shutil.copy(test_file, "motioncorr_not_quoted.star")
        jobdata = JobStar(test_file)
        jovals = jobdata.data.find_block("joboptions_values")
        joloop = jovals.find_loop("_rlnJobOptionVariable")
        assert list(joloop) == [
            "bfactor",
            "bin_factor",
            "do_dose_weighting",
            "do_own_motioncor",
            "do_queue",
            "dose_per_frame",
            "first_frame_sum",
            "fn_defect",
            "fn_gain_ref",
            "fn_motioncor2_exe",
            "gain_flip",
            "gain_rot",
            "gpu_ids",
            "group_for_ps",
            "group_frames",
            "input_star_mics",
            "last_frame_sum",
            "min_dedicated",
            "nr_mpi",
            "nr_threads",
            "other_args",
            "other_motioncor2_args",
            "patch_x",
            "patch_y",
            "pre_exposure",
            "qsub",
            "qsubscript",
            '"save_noDW"',
            '"save_ps"',
            "queuename",
        ]

    def test_reading_outputnodes_star(self):
        """Test reading a file containing output nodes"""
        outnodes_file = os.path.join(
            self.test_data, "StarFiles/RELION_OUTPUT_NODES.star"
        )
        shutil.copy(outnodes_file, "outnodes.star")
        on_star = OutputNodeStar("outnodes.star")
        outnodes = on_star.get_output_nodes()
        assert outnodes[0][0] == "MotionCorr/job002/corrected_micrographs.star"
        assert outnodes[0][1] == motioncorr_job.OUTPUT_NODE_MICS
        assert outnodes[1][0] == "MotionCorr/job002/logfile.pdf"
        assert outnodes[1][1] == motioncorr_job.OUTPUT_NODE_LOG
        assert outnodes[2][0] == "CtfFind/job003/micrographs_ctf.star"
        assert outnodes[2][1] == ctffind_job.OUTPUT_NODE_MICS
        assert outnodes[3][0] == "CtfFind/job003/logfile.pdf"
        assert outnodes[3][1] == ctffind_job.OUTPUT_NODE_LOG

    def test_read_starfile_file(self):
        # get a starfile
        starfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(starfile, "particles.star")

        # verifjy file was read
        the_file = RelionStarFile("particles.star")
        assert str(vars(the_file)) == (
            "{'data': <gemmi.cif.Document with 2 blocks (optics, particles)>}"
        )

    def test_get_starfile_datablock(self):
        # get a starfile
        starfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(starfile, "particles.star")

        # verifjy file was read
        the_file = RelionStarFile("particles.star")
        parts_block = the_file.get_block("particles")
        for i in parts_block:
            count = int(str(i.loop).split()[1])
        assert count == 1158, count

    def test_count_block_function(self):
        # get a particles starfile
        starfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(starfile, "particles.star")

        # verify the block count
        the_file = RelionStarFile("particles.star")
        parts_count = the_file.count_block("particles")
        assert parts_count == 1158, parts_count

        # get a micrographs starfile
        starfile = os.path.join(self.test_data, "micrographs_ctf.star")
        shutil.copy(starfile, "micrographs.star")

        # verify the block count
        the_file = RelionStarFile("micrographs.star")
        parts_count = the_file.count_block("micrographs")
        assert parts_count == 24, parts_count

        # get a movies starfile
        starfile = os.path.join(self.test_data, "movies.star")
        shutil.copy(starfile, "movies.star")

        # verify the block count
        the_file = RelionStarFile("movies.star")
        parts_count = the_file.count_block("movies")
        assert parts_count == 24, parts_count

    def test_counting_block_micrograhs(self):
        """count the number of micrographs is a micrographs_cft.star"""
        f = os.path.join(self.test_data, "micrographs_ctf.star")
        shutil.copy(f, "micrographs.star")
        mics_file = RelionStarFile("micrographs.star")
        count = mics_file.count_block("micrographs")
        assert count == 24, count

    def test_counting_block_movies(self):
        """count the number of micrographs is a micrographs_cft.star"""
        f = os.path.join(self.test_data, "movies.star")
        shutil.copy(f, "movies.star")
        mics_file = RelionStarFile("movies.star")
        count = mics_file.count_block("movies")
        assert count == 24, count

    def test_modify_jobstar_new_file(self):
        """modify a jobstar, change several params and write a new file"""
        testfile = os.path.join(self.test_data, "JobFiles/Extract/extract_job.star")
        shutil.copy(testfile, "jobstar_file.star")
        original = read_pipeline("jobstar_file.star")
        modify_jobstar(
            "jobstar_file.star",
            {"bg_diameter": "100", "black_dust": "420"},
            "new_file_job.star",
        )
        written = read_pipeline("new_file_job.star")

        for line in written:
            if line != ["#"] + COMMENT_LINE.split() and len(line) > 1:
                if line[0] not in ["bg_diameter", "black_dust"]:
                    assert line in original, line
                if line[0] == "bg_diameter":
                    assert line[1] == "100"
                if line[1] == "black_dust":
                    assert line[1] == "420"
        for line in original:
            if line[:2] != ["#", "version"] and len(line) > 1:
                if line[0] not in ["bg_diameter", "black_dust"]:
                    assert line in written, line

    def test_modify_jobstar_overwrite_file(self):
        """modify a jobstar, change several params and overwrite the existing file"""
        testfile = os.path.join(self.test_data, "JobFiles/Extract/extract_job.star")
        shutil.copy(testfile, "jobstar_file_job.star")
        original = read_pipeline("jobstar_file_job.star")

        modify_jobstar(
            "jobstar_file_job.star",
            {"bg_diameter": "100", "black_dust": "420"},
            "jobstar_file_job.star",
        )
        written = read_pipeline("jobstar_file_job.star")
        for line in written:
            if line != ["#"] + COMMENT_LINE.split() and len(line) > 1:
                if line[0] not in ["bg_diameter", "black_dust"]:
                    assert line in original, line
                if line[0] == "bg_diameter":
                    assert line[1] == "100"
                if line[1] == "black_dust":
                    assert line[1] == "420"
        for line in original:
            if line[:2] != ["#", "version"] and len(line) > 1:
                if line[0] not in ["bg_diameter", "black_dust"]:
                    assert line in written, line

    def test_make_conversion_file_structure(self):
        make_conversion_file_structure()
        created_dirs = {
            "AutoPick": ["job006", "job010", "job011"],
            "Class2D": ["job008", "job013"],
            "Class3D": ["job016"],
            "CtfFind": ["job003"],
            "CtfRefine": ["job022", "job023", "job024"],
            "Extract": ["job007", "job012", "job018"],
            "Import": ["job001"],
            "InitialModel": ["job015"],
            "LocalRes": ["job031"],
            "ManualPick": ["job004"],
            "MaskCreate": ["job020"],
            "MotionCorr": ["job002"],
            "Polish": ["job027", "job028"],
            "PostProcess": ["job021", "job026", "job030"],
            "Refine3D": ["job019", "job025", "job029"],
            "Select": ["job005", "job009", "job014", "job017"],
        }
        for dr in created_dirs:
            for jobdir in created_dirs[dr]:
                the_dir = os.path.join(dr, jobdir)
                assert os.path.isdir(the_dir)

    def test_pipeline_conversion(self):
        """Convert a pipeline from the RELION 3.1 to RELION 4.0 format
        Currently broken until coordinate updating is fixed"""
        make_conversion_file_structure()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003//micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        shutil.copy(pipeline, "default_pipeline.star")
        StarfileCheck(fn_in="default_pipeline.star", cifdoc=None, is_pipeline=True)
        expipe = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        expected = read_pipeline(expipe)
        wrotepipe = read_pipeline("default_pipeline.star")

        # check lines were written as expected
        for line in wrotepipe:
            if line[0:3] != ["#", "Relion", "version"]:
                assert line in expected, line
        for line in expected:
            if line[0:2] != ["#", "version"]:
                assert line in wrotepipe, line

        # check backup was written
        original = read_pipeline(pipeline)
        backup = read_pipeline("default_pipeline.star.old")
        assert backup == original

    def test_pipeline_conversion_cant_update_coords(self):
        """Convert a pipeline from the RELION 3.1 to RELION 4.0 format
        with errors because the coordinate job files were not available
        for converting the coordinate files"""

        make_conversion_file_structure()
        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        shutil.copy(pipeline, "default_pipeline.star")
        StarfileCheck(fn_in="default_pipeline.star", cifdoc=None, is_pipeline=True)
        expipe = os.path.join(
            self.test_data, "Pipelines/converted_relion31_coordserrors_pipeline.star"
        )
        expected = read_pipeline(expipe)
        wrotepipe = read_pipeline("default_pipeline.star")

        # check lines were written as expected
        for line in wrotepipe:
            if line[0:3] != ["#", "Relion", "version"]:
                assert line in expected, line
        for line in expected:
            if line[0:2] != ["#", "version"]:
                assert line in wrotepipe, line

        # check backup was written
        original = read_pipeline(pipeline)
        backup = read_pipeline("default_pipeline.star.old")
        assert backup == original

    def test_get_job_type(self):
        """Test that the get_jobtype handles RELION3.1 style
        process ids"""
        test_file = os.path.join(
            self.test_data, "JobFiles/Tutorial_Pipeline/Import_job001_job.star"
        )
        shutil.copy(test_file, os.path.join(self.test_dir, "test_job.star"))
        test_jobstar = JobStar("test_job.star")
        jobopts = test_jobstar.get_all_options()
        jobtype = get_job_type(jobopts)
        assert jobtype[0] == "relion.import.movies"

    def test_get_job_type_from_JobStar_object(self):
        """Get a RELION 3.1 style job type from a JobStar object"""
        test_file = os.path.join(
            self.test_data, "JobFiles/Tutorial_Pipeline/Import_job001_job.star"
        )
        js = JobStar(test_file)
        assert js.get_jobtype() == "relion.import.movies", js.get_jobtype()

    def test_get_continue_status_from_JobStar_object_no_continue(self):
        """When continue is False"""
        test_file = os.path.join(
            self.test_data, "JobFiles/Tutorial_Pipeline/Import_job001_job.star"
        )
        js = JobStar(test_file)
        assert not js.get_continue_status()

    def test_get_continue_status_from_JobStar_object_continue(self):
        """When continue is True"""
        test_file = os.path.join(
            self.test_data, "JobFiles/Class2D/class2D_continue_job.star"
        )
        js = JobStar(test_file)
        assert js.get_continue_status()

    def test_star_loop_as_list_all_single_row(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        found = star_loop_as_list(test_file, "optics")
        expected = (
            [
                "_rlnOpticsGroupName",
                "_rlnOpticsGroup",
                "_rlnMtfFileName",
                "_rlnMicrographOriginalPixelSize",
                "_rlnVoltage",
                "_rlnSphericalAberration",
                "_rlnAmplitudeContrast",
                "_rlnMicrographPixelSize",
            ],
            [
                [
                    "opticsGroup1",
                    "1",
                    "mtf_k2_200kV.star",
                    "0.885000",
                    "200.000000",
                    "1.400000",
                    "0.100000",
                    "0.885000",
                ]
            ],
        )
        assert found == expected

    def test_star_loop_as_list_all_multi_rows(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        found = star_loop_as_list(test_file, "micrographs")
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
            "_rlnOpticsGroup",
            "_rlnAccumMotionTotal",
            "_rlnAccumMotionEarly",
            "_rlnAccumMotionLate",
        ]

        assert found[0] == columns
        assert len(found[1]) == 24
        for i in found[1]:
            assert len(i) == 7

    def test_star_loop_as_list_specific_multi_rows(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        found = star_loop_as_list(test_file, "micrographs", columns)
        assert found[0] == columns
        assert len(found[1]) == 24
        for i in found[1]:
            assert len(i) == 3

    def test_star_loop_as_list_block_error(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        with self.assertRaises(ValueError):
            star_loop_as_list(test_file, "bad", columns)

    def test_star_loop_as_list_column_error(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        columns = ["bad_column"]
        with self.assertRaises(ValueError):
            star_loop_as_list(test_file, "micrographs", columns)

    def test_star_loop_as_list_wrong_blocktype_error(self):
        test_file = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        with self.assertRaises(ValueError):
            star_loop_as_list(test_file, "job")

    def test_star_pairs_as_dict(self):
        test_file = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        actual = star_pairs_as_dict(test_file, "job")
        expected = {
            "_rlnJobType": "relion.Import.Movies",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
        }
        assert actual == expected

    def test_star_pairs_as_dict_error_not_valuepair(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        with self.assertRaises(ValueError):
            star_pairs_as_dict(test_file, "micrographs")

    def test_compare_starfiles_identical(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = test_file
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert data

    def test_compare_starfiles_blocks_different(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        struct, data = compare_starfiles(test_file, test_file2)
        assert not struct
        assert not data

    def test_compare_starfiles_data_different_loop(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = os.path.join(self.test_data, "corrected_micrographs2.star")
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert not data

    def test_compare_starfiles_data_different_pair(self):
        test_file = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        test_file2 = os.path.join(self.test_data, "JobFiles/Import/import_alt_job.star")
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert not data

    def test_compare_starfiles_data_same_order_different_pair(self):
        test_file = os.path.join(self.test_data, "JobFiles/Import/import_alt_job.star")
        test_file2 = os.path.join(
            self.test_data, "JobFiles/Import/import_alt2_job.star"
        )
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert data

    def test_compare_starfiles_data_same_order_different_loop(self):
        test_file = os.path.join(self.test_data, "JobFiles/Import/import_alt2_job.star")
        test_file2 = os.path.join(
            self.test_data, "JobFiles/Import/import_alt3_job.star"
        )
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert data

    def test_convert_relion20_particles_file(self):
        test_star = os.path.join(self.test_data, "relion20_parts.star")
        ref_star = os.path.join(self.test_data, "20starfile_converted.star")

        shutil.copy(test_star, "20starfile.star")
        convert_relion20_datafile("20starfile.star", "particles", boxsize=256)
        struct, data = compare_starfiles(ref_star, "20starfile_converted.star")
        assert struct, data


if __name__ == "__main__":
    unittest.main()
