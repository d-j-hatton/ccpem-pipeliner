#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.metadata_tools import (
    get_job_metadata,
    make_job_parameters_schema,
    make_default_results_schema,
    get_metadata_chain,
    get_reference_list,
)
from pipeliner_tests import test_data
from pipeliner.utils import touch, get_pipeliner_root
from pipeliner.project_graph import ProjectGraph
from pipeliner.api.api_utils import get_available_jobs
from pipeliner.job_factory import new_job_of_type


class MetadataToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def copy_in_tutorial_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

    def make_import_filestructure(self):
        os.makedirs("Import/job001")
        movie_file = os.path.join(self.test_data, "movies.star")
        shutil.copy(movie_file, "Import/job001/movies.star")
        jobstar = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        shutil.copy(jobstar, "Import/job001/job.star")

    def make_motioncorr_filestructure(self, jobfile):
        os.makedirs("MotionCorr/job002")
        movie_file = os.path.join(self.test_data, "corrected_micrographs.star")
        shutil.copy(movie_file, "MotionCorr/job002/corrected_micrographs.star")
        jobstar = os.path.join(self.test_data, "JobFiles/" + jobfile)
        shutil.copy(jobstar, "MotionCorr/job002/job.star")

    def make_refine3D_filestructure(self):
        os.makedirs("Refine3D/job021/")
        # make a realistic file structure
        file_suffixes = [
            "_data.star",
            "_half1_model.star",
            "half2_model.star",
            "_model.star",
        ]
        for f in file_suffixes:
            for n in range(15):
                touch("Refine3D/job021/run_it{:03d}{}".format(n, f))

        data_files = [
            "run_data.star",
            "run_it014_half1_data.star",
            "run_it014_half2_data.star",
        ]
        for f in data_files:
            shutil.copy(
                os.path.join(self.test_data, "particles.star"),
                "Refine3D/job021/{}".format(f),
            )

        model_files = [
            "run_it014_half1_model.star",
            "run_it014_half2_model.star",
        ]
        for f in model_files:
            shutil.copy(
                os.path.join(self.test_data, "run_it020_half1_model.star"),
                "Refine3D/job021/{}".format(f),
            )

        shutil.copy(
            os.path.join(self.test_data, "run_model.star"),
            "Refine3D/job021/run_model.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3d_job.star"),
            "Refine3D/job021/job.star",
        )

    def check_metadata(self, exp_json, metadata_dict):
        """Check that the expected file and the dictionary that
        was produced are equivilent"""
        if os.path.isfile(str(exp_json)):
            with open(exp_json, "r") as expfile:
                expected_md = json.loads(expfile.read())
        else:
            expected_md = exp_json
        for i in metadata_dict:
            try:
                for j in metadata_dict[i]:
                    if type(metadata_dict[i]) == dict:

                        info = (
                            f"{i} {j}\n{[metadata_dict[i][j]]}\n{[expected_md[i][j]]}"
                        )
                        assert metadata_dict[i][j] == expected_md[i][j], info
            except TypeError:
                assert metadata_dict[i] == exp_json[i]

        for i in expected_md:
            try:
                for j in expected_md[i]:
                    if type(metadata_dict[i]) == dict:
                        info = f"{i} {j}\n{metadata_dict[i][j]}\n{expected_md[i][j]}"
                        assert metadata_dict[i][j] == expected_md[i][j], (i, j, info)
            except TypeError:
                assert metadata_dict[i] == exp_json[i]

    def test_params_scheme_test(self):
        for job in [x[0] for x in get_available_jobs()]:
            scheme_file = os.path.join(
                get_pipeliner_root(),
                f"metadata_schema/parameters/params_{job.replace('.', '_')}.json",
            )
            gen_scheme = make_job_parameters_schema(job)
            try:
                with open(scheme_file, "r") as scheme_template:
                    exp_scheme = json.loads(scheme_template.read())
            except FileNotFoundError:
                print(gen_scheme)
            assert gen_scheme == exp_scheme, f"{job}\n{gen_scheme}\n{exp_scheme}"

    def test_make_default_schema(self):
        for job in [x[0] for x in get_available_jobs()]:
            default_schema = make_default_results_schema(job)
            the_job = new_job_of_type(job)
        exp_dict = {
            "$schema": "http://json-schema.org/draft-04/schema#",
            "title": f"RESULTS: {job}",
            "description": f"{the_job.jobinfo.short_desc}",
            "type": "object",
            "properties": {
                "OutputFiles": {
                    "description": "Output nodes added to the pipeline",
                    "type": "array",
                    "Items": {"type": "string"},
                },
                "LogFiles": {
                    "description": "log files produced by the job",
                    "type": "array",
                    "items": {"type": "string"},
                },
            },
        }
        assert default_schema == exp_dict, f"{job}\n{default_schema}\n{exp_dict}"

    def test_for_metadata_schema_file(self):
        for job in [x[0] for x in get_available_jobs()]:
            j = new_job_of_type(job)
            jname = j.PROCESS_NAME.replace(".", "_")
            default_schema = {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "title": f"RESULTS: {j.PROCESS_NAME}",
                "description": f"{j.jobinfo.short_desc}",
                "type": "object",
                "properties": {
                    "run parameters": {
                        "description": "Complete parameters used for running the job",
                        "$ref": f"parameters/params_{jname}.json",
                    },
                    "results": {
                        "description": "Data on the results of the job",
                        "$ref": f"results/results_{jname}.json",
                    },
                },
            }
            schema_file = os.path.join(
                get_pipeliner_root(), f"metadata_schema/{jname}.json"
            )
            assert os.path.isfile(schema_file), print(default_schema, schema_file)

    def metadata_report_test(self, setup_function, jobname, exp_md_file, show=False):
        """Run a metadata report test for jobs in the tutorial pipeline"""
        self.copy_in_tutorial_pipeline()
        setup_function()
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)

        test_proc = pipeline.find_process(jobname)
        metadata_dict = get_job_metadata(test_proc)
        exp_json = os.path.join(self.test_data, f"Metadata/{str(exp_md_file)}")
        if not os.path.isfile(str(exp_json)):
            exp_json = exp_md_file
        if show:
            print(metadata_dict)
        self.check_metadata(exp_json, metadata_dict)

    def test_get_metadata_refine3D(self):
        self.metadata_report_test(
            self.make_refine3D_filestructure,
            "Refine3D/job021/",
            "refine3D.json",
        )

    def test_get_metadata_refine3D_error_jobstar_missing(self):
        """No metadata returned if the job.star file is missing"""
        self.copy_in_tutorial_pipeline()
        self.make_refine3D_filestructure()
        # remove the jobstar file
        os.remove(os.path.join(self.test_dir, "Refine3D/job021/job.star"))
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        ref3d_proc = pipeline.find_process("Refine3D/job021/")
        metadata_dict = get_job_metadata(ref3d_proc)
        err = "job.star file not found for job Refine3D/job021/"
        exp_dict = {"metadata collection error": err}
        self.check_metadata(exp_dict, metadata_dict)

    def test_get_import_metadata_movies(self):
        self.metadata_report_test(
            self.make_import_filestructure,
            "Import/job001/",
            "import_movies.json",
        )

    def test_get_import_metadata_movies_relion4_style_name(self):
        """Getting metedata when the job has an abigious name and needs to be
        converted"""
        self.copy_in_tutorial_pipeline()
        self.make_import_filestructure()
        # overwrite with the RELION4 style jobstar
        import_jobstar = os.path.join(
            self.test_data, "JobFiles/Import/import_relionstyle_job.star"
        )
        shutil.copy(import_jobstar, "Import/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        import_proc = pipeline.find_process("Import/job001/")
        metadata_dict = get_job_metadata(import_proc)
        exp_json = os.path.join(self.test_data, "Metadata/import_relionstyle.json")
        self.check_metadata(exp_json, metadata_dict)

    def test_get_motioncorr_metadata_own(self):
        self.copy_in_tutorial_pipeline()
        self.make_motioncorr_filestructure("MotionCorr/motioncorr_own_job.star")
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        mocorr_proc = pipeline.find_process("MotionCorr/job002/")
        metadata_dict = get_job_metadata(mocorr_proc)
        exp_json = os.path.join(self.test_data, "Metadata/motioncorr_own.json")
        self.check_metadata(exp_json, metadata_dict)

    def test_get_motioncorr_metadata_mocorr2(self):
        self.copy_in_tutorial_pipeline()
        self.make_motioncorr_filestructure(
            "Default_JobStar/default_relion_motioncorr_motioncorr2_job.star"
        )
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        mocorr_proc = pipeline.find_process("MotionCorr/job002/")
        # set the type correctly rather than using a completely different pipeline
        mocorr_proc.type = "relion.motioncorr.motioncorr2"
        metadata_dict = get_job_metadata(mocorr_proc)
        exp_json = os.path.join(self.test_data, "Metadata/motioncorr_mocorr2.json")
        self.check_metadata(exp_json, metadata_dict)

    def test_get_metadata_from_backtrace_two_jobs(self):
        self.copy_in_tutorial_pipeline()
        self.make_import_filestructure()
        self.make_motioncorr_filestructure("MotionCorr/motioncorr_own_job.star")
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        mocorr_proc = pipeline.find_process("MotionCorr/job002/")
        get_metadata_chain(pipeline, mocorr_proc, "job_metadata.json")
        with open("job_metadata.json", "r") as out_file:
            written = json.loads(out_file.read())
        exp_json = os.path.join(self.test_data, "Metadata/two_jobs.json")
        self.check_metadata(exp_json, written)

    def test_get_metadata_from_backtrace_two_jobs_with_continuations(self):
        self.copy_in_tutorial_pipeline()
        self.make_import_filestructure()

        # make a file for a previous run of the inport job
        prevfiles = ["20180912102500", "20210915162069"]
        mdjson = os.path.join(self.test_data, "Metadata/import_movies.json")
        for thedate in prevfiles:
            prevfile = f"Import/job001/{thedate}_job_metadata.json"
            shutil.copy(mdjson, prevfile)

        self.make_motioncorr_filestructure("MotionCorr/motioncorr_own_job.star")
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        mocorr_proc = pipeline.find_process("MotionCorr/job002/")
        get_metadata_chain(pipeline, mocorr_proc, "job_metadata.json", True)
        with open("job_metadata.json", "r") as out_file:
            written = json.loads(out_file.read())
        exp_json = os.path.join(self.test_data, "Metadata/two_jobs_with_previous.json")
        self.check_metadata(exp_json, written)

    def test_get_references(self):
        self.copy_in_tutorial_pipeline()
        pipeline = ProjectGraph()
        pipeline.read(do_lock=False)
        refs_list = get_reference_list(pipeline, pipeline.process_list[3])

        exp_data = {
            "CtfFind/job003/": (
                ["relion_run_ctffind", "relion_run_ctffind_mpi"],
                ["New tools ", "RELION: im", "Gctf: Real"],
            ),
            "MotionCorr/job002/": (
                ["relion_run_motioncorr", "relion_run_motioncorr_mpi"],
                ["New tools ", "RELION: im", "MotionCor2"],
            ),
            "Import/job001/": (
                ["relion_import"],
                ["New tools ", "RELION: im"],
            ),
        }

        for r in refs_list:
            test = (refs_list[r][0], [x.title[:10] for x in refs_list[r][1]])
            assert exp_data[r] == test, (r, test, exp_data[r])

    def setup_ctf_filestructure(self):
        os.makedirs("CtfFind/job003")
        ctf_file = os.path.join(self.test_data, "micrographs_ctf.star")
        shutil.copy(ctf_file, "CtfFind/job003/micrographs_ctf.star")
        jobstar = os.path.join(self.test_data, "JobFiles/CtfFind/ctffind_job.star")
        shutil.copy(jobstar, "CtfFind/job003/job.star")

    def test_ctffind_metadata(self):
        self.metadata_report_test(
            self.setup_ctf_filestructure,
            "CtfFind/job003/",
            "ctffind_job.json",
        )

    def setup_autopick_filestructure(self):
        ap_dir = "AutoPick/job006"
        os.makedirs(f"{ap_dir}/Movies")
        shutil.copy(os.path.join(self.test_data, "autopick.star"), ap_dir)
        for n in range(1, 5):
            shutil.copy(
                os.path.join(self.test_data, f"micrograph{n:03d}_autopick.star"),
                os.path.join(ap_dir, "Movies"),
            )
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/AutoPick/autopick_job.star"),
            os.path.join(ap_dir, "job.star"),
        )

    def test_autopick_metadata(self):
        self.metadata_report_test(
            self.setup_autopick_filestructure,
            "AutoPick/job006/",
            "autopick_md.json",
        )

    def setup_class2d_file_structure(self):
        cl2d_dir = "Class2D/job008"
        os.makedirs(cl2d_dir)
        for t in ["model", "data"]:
            shutil.copy(
                os.path.join(self.test_data, f"class2d_{t}.star"),
                os.path.join(cl2d_dir, f"run_it025_{t}.star"),
            )
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Class2D/class2D_job.star"),
            os.path.join(cl2d_dir, "job.star"),
        )

    def test_class2d_metadata(self):
        self.metadata_report_test(
            self.setup_class2d_file_structure,
            "Class2D/job008/",
            "class2d_md.json",
        )


if __name__ == "__main__":
    unittest.main()
