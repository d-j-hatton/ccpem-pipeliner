#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from glob import glob
from pipeliner import pipeliner_job
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    touch,
    general_get_command_test,
)
from pipeliner.jobs.buccaneer_job import (
    INPUT_NODE_MAP,
    OUTPUT_NODE_MODEL,
    INPUT_NODE_SEQ,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.jobstar_reader import JobStar
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE
from pipeliner.scripts import check_completion


class PipelinerJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_pipeliner_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            pipeliner_job.PipelinerJob()

    def test_get_buccaneer_plugin_commands(self):
        os.makedirs("sequences")
        touch("sequences/5ni1.fasta")
        os.makedirs("PostProcess/job010/")
        touch("PostProcess/job010/postprocess_masked.mrc")
        general_get_command_test(
            self,
            "Buccaneer",
            "buccaneer_job.star",
            1502,
            {
                "PostProcess/job010/postprocess_masked.mrc": INPUT_NODE_MAP,
                "sequences/5ni1.fasta": INPUT_NODE_SEQ,
            },
            {"refined1.pdb": OUTPUT_NODE_MODEL},
            [
                "ccpem-buccaneer --no-gui --args tmp_buccaneer_args.json"
                " --job_location Buccaneer/job1502/",
                "rm tmp_buccaneer_args.json",
            ],
        )

        # make sure the json written was as expected:
        with open("tmp_buccaneer_args.json", "r") as json_data:
            actual = json.loads(json_data.read())
        expected = {
            "job_title": "RELION pipeline test with EMD-3488",
            "input_map": os.path.abspath("PostProcess/job010/postprocess_masked.mrc"),
            "resolution": 3.3,
            "input_seq": os.path.abspath("sequences/5ni1.fasta"),
            "extend_pdb": None,
            "ncycle": 1,
            "ncycle_refmac": 20,
            "ncycle_buc1st": 1,
            "ncycle_bucnth": 1,
            "map_sharpen": 0.0,
            "ncpus": 1,
            "lib_in": None,
            "keywords": "",
            "refmac_keywords": "",
        }
        for line in actual:
            assert actual[line] == expected[line], (line, actual[line], expected[line])

    def test_get_default_params_import_movies(self):
        job = new_job_of_type("relion.import.movies")
        params_dict = job.default_params_dict()
        expected_params = {
            "fn_in_raw": "Micrographs/*.tif",
            "is_multiframe": "Yes",
            "optics_group_name": "opticsGroup1",
            "fn_mtf": "",
            "angpix": 1.4,
            "kV": 300,
            "Cs": 2.7,
            "Q0": 0.1,
            "beamtilt_x": 0.0,
            "beamtilt_y": 0.0,
        }
        assert params_dict == expected_params

    def test_get_default_params_all_jobs(self):
        defaults_dir = os.path.join(self.test_data, "JobFiles/Default_JobStar")
        default_files = glob(f"{defaults_dir}/*.star")

        # parameters not to check because they are set by env vars or not in the
        # joboptions dict
        params_to_skip = [
            "_rlnJobTypeLabel",
            "_rlnJobIsContinue",
            "_rlnJobIsTomo",
            "scratch_dir",
            "fn_gctf_exe",
            "fn_motioncor2_exe",
            "qsubscript",
            "do_queue",
            "queuename",
            "qsubscript",
            "qsub",
            "fn_ctffind_exe",
            "fn_resmap",
            "fn_topaz_exec",
        ]

        for f in default_files:
            options = JobStar(f).get_all_options()
            jobtype = options["_rlnJobTypeLabel"]
            new_job = new_job_of_type(jobtype)
            params_dict = new_job.default_params_dict()
            for param in options:
                if param not in params_to_skip:
                    info = (jobtype, param, str(params_dict[param]), options[param])
                    assert str(params_dict[param]) == options[param], info
            for param in params_dict:
                if param not in params_to_skip:
                    info = (jobtype, param, str(params_dict[param]), options[param])
                    assert str(params_dict[param]) == options[param], info

    def test_joboption_validation_with_errors(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].suggested_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "Required parameter empty", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            (
                "warning",
                "Value outside suggested range < 100.0",
                "Pixel size (Angstrom):",
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_warnings(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].hard_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "Required parameter empty", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            ("error", "Value cannot be less than 100.0", "Pixel size (Angstrom):"),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_adv_parameter_errors(self):
        job = new_job_of_type("relion.autopick.log")
        job.joboptions["fn_input_autopick"].value = "test.star"
        job.joboptions["log_diam_min"].value = 100
        job.joboptions["log_diam_max"].value = 99
        job.joboptions["do_queue"].value = "No"

        errors = job.validate_joboptions()

        exp_errors = [
            (
                "error",
                "Max diameter of LoG filter (log_diam_max) must be > min diameter "
                "(log_diam_min).",
                [
                    "Min. diameter for LoG filter (A)",
                    "Max. diameter for LoG filter (A)",
                ],
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, [x.label for x in i.raised_by])
            formatted_errors.append(error)

        for err in formatted_errors:
            assert err in exp_errors

        for err in exp_errors:
            assert err in formatted_errors, err

    def test_parse_additional_args_even_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions[
            "other_args"
        ].value = '--1 test --2 test2 --3 "This is complicated"'
        assert job.parse_additional_args() == [
            "--1",
            "test",
            "--2",
            "test2",
            "--3",
            '"This is complicated"',
        ]

    def test_dynamic_requirement_of_joboptions(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["do_queue"].value = "Yes"
        job.joboptions["qsubscript"].value = ""

        errors = job.validate_dynamically_required_joboptions()

        assert errors[0].message == (
            "Because Submit to queue? = True Standard submission script: is required"
        )
        assert [x.label for x in errors[0].raised_by] == [
            "Standard submission script:",
            "Submit to queue?",
        ]
        assert errors[0].type == "error"

    def test_parse_additional_args_starts_with_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions[
            "other_args"
        ].value = '"1 test" --2 test2 --3 "This is complicated"'

        assert job.parse_additional_args() == [
            '"1 test"',
            "--2",
            "test2",
            "--3",
            '"This is complicated"',
        ]

    def test_parse_additional_args_starts_no_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["other_args"].value = "1 test --2 test2 --3 This is simple"

        assert job.parse_additional_args() == [
            "1",
            "test",
            "--2",
            "test2",
            "--3",
            "This",
            "is",
            "simple",
        ]

    def test_reordering_joboptions_complete_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        assert list(job.joboptions) != new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == new_order

    def test_reordering_joboptions_partial_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        full_new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        assert list(job.joboptions) != full_new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == full_new_order

    def test_reordering_joboptions_error_in_not_present(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_img",
            "fn_ref",
            "Badbadbad",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        with self.assertRaises(ValueError):
            job.set_joboption_order(new_order)

    def test_job_withno_outputs_adds_logfile_in_post(self):
        jobdir = "PostProcess/job001/"
        job = new_job_of_type("relion.postprocess")
        job.output_name = jobdir
        job.output_nodes = []
        assert len(job.output_nodes) == 0
        assert not os.path.isfile(SUCCESS_FILE)

        job.prepare_final_command(jobdir, ["fake", "commands"], True)
        job.output_name = jobdir
        oj = os.path.join(jobdir, "run.out")
        touch(oj)

        check_completion.main([jobdir, "run.out"])
        success = os.path.join(jobdir, SUCCESS_FILE)

        assert len(job.output_nodes) == 1
        assert job.output_nodes[0].name == oj, job.output_nodes[0].name
        assert job.output_nodes[0].type == pipeliner_job.OUTPUT_NODE_RUNOUT
        assert os.path.isfile(success)

    def test_job_withno_outputs_fails_if_log_not_written(self):
        jobdir = "PostProcess/job001/"
        job = new_job_of_type("relion.postprocess")
        job.output_name = jobdir
        job.output_nodes = []
        ffile = os.path.join(jobdir, FAIL_FILE)

        assert len(job.output_nodes) == 0
        assert not os.path.isfile(ffile)
        job.output_name = jobdir
        job.prepare_final_command(jobdir, ["fake", "commands"], True)
        oj = os.path.join(jobdir, "run.out")
        check_completion.main([jobdir, "run.out"])

        assert len(job.output_nodes) == 1
        assert job.output_nodes[0].name == oj
        assert job.output_nodes[0].type == pipeliner_job.OUTPUT_NODE_RUNOUT
        assert os.path.isfile(ffile)


if __name__ == "__main__":
    unittest.main()
