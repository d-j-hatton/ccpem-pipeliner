#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.jobs.relion.localres_job import (
    INPUT_NODE_HALFMAP,
    INPUT_NODE_MASK,
    OUTPUT_NODE_LOCRES_OWN,
    OUTPUT_NODE_LOCRES_RESMAP,
    OUTPUT_NODE_LOG,
    OUTPUT_NODE_FILTMAP,
)


class LocalResTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        # make some dirs for testing sym link making
        os.mkdir("Refine3D")
        os.mkdir("Refine3D/job029")
        open("Refine3D/job029/run_half1_class001_unfil.mrc", "a").close()
        open("Refine3D/job029/run_half1_class002_unfil.mrc", "a").close()
        os.mkdir("LocalRes")
        os.mkdir("LocalRes/job014")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_relion_localres(self):
        general_get_command_test(
            self,
            "LocalRes",
            "localres_relion.job",
            14,
            {"Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP},
            {
                "histogram.pdf": OUTPUT_NODE_LOG,
                "relion_locres_filtered.mrc": OUTPUT_NODE_FILTMAP,
                "relion_locres.mrc": OUTPUT_NODE_LOCRES_OWN,
            },
            [
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_jobstar(self):
        general_get_command_test(
            self,
            "LocalRes",
            "localres_relion_job.star",
            14,
            {"Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP},
            {
                "histogram.pdf": OUTPUT_NODE_LOG,
                "relion_locres_filtered.mrc": OUTPUT_NODE_FILTMAP,
                "relion_locres.mrc": OUTPUT_NODE_LOCRES_OWN,
            },
            [
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_mtf(self):
        general_get_command_test(
            self,
            "LocalRes",
            "localres_relion_mtf.job",
            14,
            {"Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP},
            {
                "histogram.pdf": OUTPUT_NODE_LOG,
                "relion_locres_filtered.mrc": OUTPUT_NODE_FILTMAP,
                "relion_locres.mrc": OUTPUT_NODE_LOCRES_OWN,
            },
            [
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_mtf_relionstyle_name(self):
        """Test Automatic concersion of ambiguous relion style job name"""
        general_get_command_test(
            self,
            "LocalRes",
            "localres_relion_mtf_relionstyle.job",
            14,
            {"Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP},
            {
                "histogram.pdf": OUTPUT_NODE_LOG,
                "relion_locres_filtered.mrc": OUTPUT_NODE_FILTMAP,
                "relion_locres.mrc": OUTPUT_NODE_LOCRES_OWN,
            },
            [
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_nompi(self):
        general_get_command_test(
            self,
            "LocalRes",
            "localres_relion_nompi.job",
            14,
            {"Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP},
            {
                "histogram.pdf": OUTPUT_NODE_LOG,
                "relion_locres_filtered.mrc": OUTPUT_NODE_FILTMAP,
                "relion_locres.mrc": OUTPUT_NODE_LOCRES_OWN,
            },
            [
                "relion_postprocess --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_resmap_localres(self):
        general_get_command_test(
            self,
            "LocalRes",
            "localres_resmap.job",
            14,
            {
                "Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP,
                "MaskCreate/job400/mask.mrc": INPUT_NODE_MASK,
            },
            {"half1_resmap.mrc": OUTPUT_NODE_LOCRES_RESMAP},
            [
                "/public/EM/ResMap/ResMap-1.1.4-linux64 "
                "--maskVol=MaskCreate/job400/mask.mrc --noguiSplit "
                "LocalRes/job014/half1.mrc LocalRes/job014/half2.mrc --vxSize=1.244 "
                "--pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1"
            ],
        )
        # check that the symlinks were made correctly
        assert os.path.islink("LocalRes/job014/half1.mrc")
        assert os.path.islink("LocalRes/job014/half2.mrc")

    def test_resmap_localres_relionstyle_jobname(self):
        """test automatic conversion of ambiguous relion style jobname"""
        general_get_command_test(
            self,
            "LocalRes",
            "localres_resmap_relionstyle.job",
            14,
            {
                "Refine3D/job029/run_half1_class001_unfil.mrc": INPUT_NODE_HALFMAP,
                "MaskCreate/job400/mask.mrc": INPUT_NODE_MASK,
            },
            {"half1_resmap.mrc": OUTPUT_NODE_LOCRES_RESMAP},
            [
                "/public/EM/ResMap/ResMap-1.1.4-linux64 "
                "--maskVol=MaskCreate/job400/mask.mrc --noguiSplit "
                "LocalRes/job014/half1.mrc LocalRes/job014/half2.mrc --vxSize=1.244 "
                "--pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1"
            ],
        )
        # check that the symlinks were made correctly
        assert os.path.islink("LocalRes/job014/half1.mrc")
        assert os.path.islink("LocalRes/job014/half2.mrc")

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_locres_relion(self):
        get_relion_tutorial_data("LocalRes")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("LocalRes/job031/")
        dispobjs = pipeline.get_process_results_display(proc)
        histfile = os.path.join(self.test_data, "ResultsFiles/local_res.json")
        with open(histfile, "r") as hf:
            exp_hist = json.load(hf)
        exp_map = {
            "maps": ["LocalRes/job031/Thumbnails/relion_locres_filtered.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Local resolution filtered map",
            "maps_data": "LocalRes/job031/relion_locres_filtered.mrc",
            "models_data": "",
            "associated_data": ["LocalRes/job031/relion_locres_filtered.mrc"],
        }
        assert dispobjs[0].__dict__ == exp_hist
        assert dispobjs[1].__dict__ == exp_map


if __name__ == "__main__":
    unittest.main()
