#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    get_relion_tutorial_data,
    tutorial_data_available,
)
from pipeliner.jobs.relion.initialmodel_job import (
    INPUT_NODE_PARTS,
    OUTPUT_NODE_INIMODEL,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_OPT,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.data_structure import RELION_SUCCESS_FILE


class InitialModelTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ["RELION_SCRATCH_DIR"] = ""

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_inimodel(self):
        """Test with run.job file, default params"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel.job",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL,
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_jobstar(self):
        """Test with job.star file default params"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_job.star",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL,
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_3classes_c1(self):
        """Three classes with c1 symmetry"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_3classes.job",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "run_it200_class002.mrc": OUTPUT_NODE_INIMODEL,
                "run_it200_class003.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL,
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_continue(self):
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_continue.job",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it300_optimiser.star": OUTPUT_NODE_OPT,
                "run_it300_data.star": OUTPUT_NODE_PARTS,
                "run_it300_class001.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL,
            },
            [
                "relion_refine --continue InitialModel/job012/run_it200_optimiser.star"
                " --o InitialModel/job012/run --iter 300 --ctf --K 1 --sym C1"
                " --flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i "
                "InitialModel/job012/run_it300_model.star --o "
                "InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_finesampling(self):
        """Test with job.star file finer sampling"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_finesampling.job",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL,
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 5 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1(self):
        """single class non-c1 symmetry, symmetry applied after refinement"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_c4_job.star",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym(self):
        """single class non-c1 symmetry, symmetry applied during refinement"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_c4_inrefine_job.star",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
                "initial_model.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1_multiclass(self):
        """single class non-C1 symmetry, symmetry applied after refinement with
        multiple classes"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_c4_multiclass_job.star",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL,
                "run_it200_class002.mrc": OUTPUT_NODE_INIMODEL,
                "run_it200_class003.mrc": OUTPUT_NODE_INIMODEL,
                "initial_model.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_multiclass(self):
        """single class non-C1 symmetry, symmetry applied during refinement with
        multiple classes"""
        general_get_command_test(
            self,
            "InitialModel",
            "initialmodel_c4_inrefine_multiclass_job.star",
            12,
            {"Select/job014/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
                "run_it200_class001.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
                "run_it200_class002.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
                "run_it200_class003.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
                "initial_model.mrc": OUTPUT_NODE_INIMODEL + ".c4sym",
            },
            [
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_inimod(self):
        get_relion_tutorial_data("InitialModel")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("InitialModel/job015/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = {
            "maps": ["InitialModel/job015/Thumbnails/initial_model.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: InitialModel/job015/initial_model.mrc",
            "maps_data": "InitialModel/job015/initial_model.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/initial_model.mrc"],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_inimod_not_finished(self):
        get_relion_tutorial_data("InitialModel")
        os.remove(f"InitialModel/job015/{RELION_SUCCESS_FILE}")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("InitialModel/job015/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = {
            "maps": ["InitialModel/job015/Thumbnails/run_it100_class001.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: InitialModel/job015/run_it100_class001.mrc",
            "maps_data": "InitialModel/job015/run_it100_class001.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/run_it100_class001.mrc"],
        }
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
