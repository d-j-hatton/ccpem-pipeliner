#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import subprocess

from pipeliner import job_factory
from pipeliner_tests import test_data, plugins_tests
from pipeliner.utils import get_pipeliner_root

CC_PATH = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


class QsubJobsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.plugin_test_data = os.path.dirname(plugins_tests.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # Change the environment variables to the defaults
        # in case user has already set theirs
        q_envvars = {
            "PIPELINER_QSUB_COMMAND": "qsub",
            "PIPELINER_QUEUE_NAME": "openmpi",
        }
        self.old_envvars = {}
        for var in q_envvars:
            if os.environ.get(var) is not None:
                self.old_envvars[var] = os.environ.get(var)
                os.environ[var] = q_envvars[var]

    def tearDown(self):
        os.chdir(self._orig_dir)
        for envvar in (
            "PIPELINER_QSUB_EXTRA_COUNT",
            "PIPELINER_QSUB_EXTRA1",
            "PIPELINER_QSUB_EXTRA1_DEFAULT",
            "PIPELINER_QSUB_EXTRA1_HELP",
            "PIPELINER_QSUB_EXTRA2",
            "PIPELINER_QSUB_EXTRA2_DEFAULT",
            "PIPELINER_QSUB_EXTRA2_HELP",
            "PIPELINER_QSUB_EXTRA3",
            "PIPELINER_QSUB_EXTRA3_DEFAULT",
            "PIPELINER_QSUB_EXTRA3_HELP",
            "PIPELINER_QSUB_EXTRA4",
            "PIPELINER_QSUB_EXTRA4_DEFAULT",
            "PIPELINER_QSUB_EXTRA4_HELP",
        ):
            if envvar in os.environ:
                del os.environ[envvar]

        # restore the queue sub related env vars
        for var in self.old_envvars:
            os.environ[var] = self.old_envvars[var]

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_env_vars_defaults(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_defaults.job")
        )

        assert job.joboptions["qsub_extra_1"].value == "101"
        assert job.joboptions["qsub_extra_2"].value == "202"
        assert job.joboptions["qsub_extra_3"].value == "303"
        assert job.joboptions["qsub_extra_4"].value == "404"

    def test_get_env_vars_2extras(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "2",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_2extras.job")
        )

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"

    def test_get_env_vars_4extras(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_4extras.job")
        )

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

    def test_qsub_write_script_refine3D(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        subprocess.call(["cp", scriptfile, "."])

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_4extras.job")
        )

        outputname = "Refine3D/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        command = job.prepare_final_command(outputname, commandlist, True)
        expected_command = "qsub Refine3D/job001/run_submit.script".split()

        assert command[0][0] == expected_command, (command[0][0], expected_command)
        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_ref3D.sh"
        )
        out_script = os.path.join(self.test_dir, "Refine3D/job001/run_submit.script")

        with open(expected_script) as expected:
            expected = expected.readlines()
        expected_update = [
            x.replace("XXXCHECK_COMMAND_PATHXXX", CC_PATH) for x in expected
        ]

        with open(out_script) as wrote:
            wrote = wrote.readlines()
        for line in zip(wrote, expected_update):
            assert line[0] == line[1], (line[0], line[1])

    def test_qsub_write_script_refine3D_1mpi(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        subprocess.call(["cp", scriptfile, "."])

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_4extras_1mpi.job")
        )
        outputname = "Refine3D/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        command = job.prepare_final_command(outputname, commandlist, True)

        expected_command = "qsub Refine3D/job001/run_submit.script".split()

        assert command[0][0] == expected_command, (command[0][0], expected_command)
        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_ref3D_1mpi.sh"
        )
        out_script = os.path.join(self.test_dir, "Refine3D/job001/run_submit.script")

        with open(expected_script) as expected:
            expected = expected.readlines()
        expected_update = [
            x.replace("XXXCHECK_COMMAND_PATHXXX", CC_PATH) for x in expected
        ]
        with open(out_script) as wrote:
            wrote = wrote.readlines()
        for line in zip(wrote, expected_update):
            assert line[0] == line[1], (line[0], line[1])

    def test_qsub_write_script_autopick(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        subprocess.call(["cp", scriptfile, "."])

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
        )

        outputname = "AutoPick/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        command = job.prepare_final_command(outputname, commandlist, True)

        expected_command = "qsub AutoPick/job001/run_submit.script".split()

        assert command[0][0] == expected_command, (command[0][0], expected_command)
        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_autopick_2dref.sh"
        )
        out_script = os.path.join(self.test_dir, "AutoPick/job001/run_submit.script")

        with open(expected_script) as expected:
            expected = expected.readlines()
        expected_update = [
            x.replace("XXXCHECK_COMMAND_PATHXXX", CC_PATH) for x in expected
        ]

        with open(out_script) as wrote:
            wrote = wrote.readlines()

        for line in zip(expected_update, wrote):
            assert line[0] == line[1], (line[0], line[1])

    def test_qsub_write_script_multibody(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        subprocess.call(["cp", scriptfile, "."])
        bodyfile = os.path.join(self.test_data + "/JobFiles/MultiBody/bodyfile.star")
        subprocess.call(["cp", bodyfile, "."])

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/multibody_sep_eigen_qsub.job")
        )
        outputname = "MultiBody/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        command = job.prepare_final_command(outputname, commandlist, True)

        expected_command = "qsub MultiBody/job001/run_submit.script".split()

        assert command[0][0] == expected_command, (command[0][0], expected_command)
        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_multibody.sh"
        )
        out_script = os.path.join(self.test_dir, "MultiBody/job001/run_submit.script")

        with open(expected_script) as expected:
            expected = expected.readlines()
        expected_update = [
            x.replace("XXXCHECK_COMMAND_PATHXXX", CC_PATH) for x in expected
        ]
        with open(out_script) as wrote:
            wrote = wrote.readlines()
        for line in zip(wrote, expected_update):
            assert line[0] == line[1], (line[0], line[1])

    def test_qsub_no_template_specified(self):
        with self.assertRaises(ValueError):
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Qsub/qsub_notemplate.job")
            )
            outputname = "AutoPick/job003/"
            job.output_name = outputname
            commandlist = job.get_commands()
            job.prepare_final_command(outputname, commandlist, False)

    def test_qsub_template_missing(self):
        with self.assertRaises(ValueError):
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
            )
            outputname = "AutoPick/job003/"
            job.output_name = outputname
            commandlist = job.get_commands()
            job.prepare_final_command(outputname, commandlist, False)

    def test_qsub_cant_write_output_script(self):
        with self.assertRaises(RuntimeError):
            scriptfile = os.path.join(self.test_data + "/submission_script.sh")
            subprocess.call(["cp", scriptfile, "."])
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
            )
            outputname = "AutoPick/job003/"
            job.output_name = outputname
            commandlist = job.get_commands()
            job.prepare_final_command(outputname, commandlist, False)

    def test_qsub_write_script_plugin(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        subprocess.call(["cp", scriptfile, "."])

        job = job_factory.read_job(
            os.path.join(
                self.plugin_test_data,
                "test_data/Reproject/project_plugin_queue_job.star",
            )
        )

        outputname = "Reproject/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        command = job.prepare_final_command(outputname, commandlist, True)

        expected_command = "qsub Reproject/job001/run_submit.script".split()

        assert command[0][0] == expected_command, (command[0][0], expected_command)

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_plugin.sh"
        )
        actual_script = os.path.join(
            self.test_dir, "Reproject/job001/run_submit.script"
        )

        with open(expected_script) as expected_file:
            expected_lines = expected_file.readlines()
        expected_update = [
            x.replace("XXXCHECK_COMMAND_PATHXXX", CC_PATH) for x in expected_lines
        ]

        with open(actual_script) as actual_file:
            actual_lines = actual_file.readlines()

        for line in zip(expected_update, actual_lines):
            assert line[0] == line[1], (line[0], line[1])


if __name__ == "__main__":
    unittest.main()
