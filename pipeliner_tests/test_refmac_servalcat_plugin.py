#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("servalcat") is None else False

plugin_present = generic_tests.check_for_plugin("refmac_servalcat_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class RefmacServalcatTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="refmac-servalcat")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_hb_refinement_halfmaps(self):
        proc = job_running_test(
            os.path.join(self.test_data, "JobFiles/Refmac_Servalcat/refine.job"),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            [
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        print()
        assert dispobjs[0].__dict__ == {
            "maps": ["RefmacServalcat/job998/Thumbnails/diffmap.mrc"],
            "maps_opacity": [0.5],
            "models": ["RefmacServalcat/job998/refined.pdb"],
            "title": "Overlaid map: RefmacServalcat/job998/diffmap.mrc model: "
            "RefmacServalcat/job998/refined.pdb",
            "models_data": "RefmacServalcat/job998/refined.pdb",
            "maps_data": "RefmacServalcat/job998/diffmap.mrc",
            "associated_data": [
                "RefmacServalcat/job998/diffmap.mrc",
                "RefmacServalcat/job998/refined.pdb",
            ],
        }


if __name__ == "__main__":
    unittest.main()
