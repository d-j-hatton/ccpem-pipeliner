#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.jobs.relion.ctfrefine_job import (
    INPUT_NODE_PARTS,
    INPUT_NODE_POST,
    OUTPUT_NODE_LOG,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS_ANISO,
)


class CtfRefineTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_aniso(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS_ANISO,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_aniso --kmin_mag 29 --j 8 --pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_aniso_relionstyle_name(self):
        """Test automatic conversion of relion style ambiguous job name"""
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_anisomag_relionstyle.job",
            18,
            {
                "CtfRefine/job022/particles_ctf_refine.star": INPUT_NODE_PARTS,
                "PostProcess/job021/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS_ANISO,
            },
            [
                "relion_ctf_refine --i CtfRefine/job022/particles_ctf_refine.star"
                " --f PostProcess/job021/postprocess.star --o CtfRefine/job018/"
                " --fit_aniso --kmin_mag 30 --j 12 --pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_aniso_jobstar(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_job.star",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS_ANISO,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_aniso --kmin_mag 29 --j 8 --pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_no_options_error(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self, "CtfRefine", "ctfrefine_nooptions_error.job", 18, 2, 2, ""
            )

    def test_get_command_ctfrefine_defocus(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_defocus.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_defocus --kmin_defocus 29 --fit_mode fmfff --j 8 "
                "--pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_ctfrefine_defocus_astig(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_defocus_astig.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_defocus --kmin_defocus 29 --fit_mode fmmff --j 8"
                " --pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_ctfrefine_focus_astig_bfact(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_defocus_astig_bf.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_defocus --kmin_defocus 29 --fit_mode fppfp --j 8 "
                "--pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_ctfrefine_focus_astig_bfact_phase(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_defocus_astig_bf_ps.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_defocus --kmin_defocus 29 --fit_mode mppfp --j 8 "
                "--pipeline_control CtfRefine/job018/"
            ],
        )

    def test_get_command_ctfrefine_no_ctf_params(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "CtfRefine",
                "ctfrefine_no_ctf_params_error.job",
                18,
                2,
                2,
                "",
            )

    def test_get_command_ctfrefine_continue(self):
        general_get_command_test(
            self,
            "CtfRefine",
            "ctfrefine_continue.job",
            18,
            {
                "Refine3D/job600/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job700/postprocess.star": INPUT_NODE_POST,
            },
            {
                "logfile.pdf": OUTPUT_NODE_LOG,
                "particles_ctf_refine.star": OUTPUT_NODE_PARTS_ANISO,
            },
            [
                "mpirun -n 16 relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job018/"
                " --fit_aniso --kmin_mag 29 --only_do_unfinished --j 8 "
                "--pipeline_control CtfRefine/job018/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctfrefine_generate_display_data_abberation_corr(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(["CtfRefine"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("CtfRefine/job022/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = {
            "title": "CTF refinement graphical ouputs",
            "xvalues": [0, 1, 0, 1, 0, 1, 0, 1],
            "yvalues": [3, 3, 2, 2, 1, 1, 0, 0],
            "labels": [
                "aberr_delta-phase_iter-fit_optics-group_1_N-4",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4_residual",
                "aberr_delta-phase_per-pixel_optics-group_1",
                "beamtilt_delta-phase_iter-fit_optics-group_1_N-3",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3_residual",
                "beamtilt_delta-phase_per-pixel_optics-group_1",
            ],
            "associated_data": [
                "CtfRefine/job022/aberr_delta-phase_iter-fit_optics-group_1_N-4.mrc",
                "CtfRefine/job022/aberr_delta-phase_lin-fit_optics-group_1_N-4.mrc",
                "CtfRefine/job022/aberr_delta-phase_lin-fit_optics-group_1_N-4"
                "_residual.mrc",
                "CtfRefine/job022/aberr_delta-phase_per-pixel_optics-group_1.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_iter-fit_optics-group_1_N-3.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_lin-fit_optics-group_1_N-3.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_lin-fit_optics-group_1_N-3"
                "_residual.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_per-pixel_optics-group_1.mrc",
            ],
            "img": "CtfRefine/job022/Thumbnails/montage_f0.png",
        }

        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctfrefine_generate_display_data_anisomag_corr(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(["CtfRefine"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("CtfRefine/job023/")
        dispobjs = pipeline.get_process_results_display(proc)

        expected = {
            "title": "CTF refinement graphical ouputs",
            "xvalues": [0, 1, 0, 1],
            "yvalues": [1, 1, 0, 0],
            "labels": [
                "mag_disp_x_fit_optics-group_1",
                "mag_disp_x_optics-group_1",
                "mag_disp_y_fit_optics-group_1",
                "mag_disp_y_optics-group_1",
            ],
            "associated_data": [
                "CtfRefine/job023/mag_disp_x_fit_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_x_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_y_fit_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_y_optics-group_1.mrc",
            ],
            "img": "CtfRefine/job023/Thumbnails/montage_f0.png",
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctfrefine_generate_display_data_per_particle(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(["CtfRefine"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("CtfRefine/job024/")
        dispobjs = pipeline.get_process_results_display(proc)

        expected = {
            "title": "Ctf refinements performed",
            "headers": ["Refinement type", "Basis"],
            "table_data": [
                ["defocus", "Per-particle"],
                ["astigmatism", "Per-micrograph"],
                ["phase shift", "Not performed"],
                ["b-factor", "Not performed"],
            ],
            "associated_data": ["CtfRefine/job024/particles_ctf_refine.star"],
        }

        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
