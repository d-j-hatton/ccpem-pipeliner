#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import unittest
import shutil
import os
import tempfile
from pipeliner.api import env_setup
from pipeliner_tests import test_data


class PipelinerEnvSetupTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_env_setup(self):
        # Remove variable in case set using pop in case not present
        env_var = "CCPEMLOC"
        os.environ.pop(env_var, None)
        assert env_var not in os.environ
        # Load test bash script and reset enviromant variable
        test_script = os.path.join(self.test_data, "test_env.sh")
        assert os.path.exists(test_script)
        env_setup.set_python_environment(scripts=[test_script])
        assert env_var in os.environ
        assert os.environ["CCPEMLOC"] == "playing.burden.lavished"
        assert os.environ["CCPEMLOC"] != "r92"


if __name__ == "__main__":
    unittest.main()
