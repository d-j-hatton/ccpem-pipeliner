#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests

from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("locscale") is None else False
plugin_present = generic_tests.check_for_plugin("locscale_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class LocScaleTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="locscale")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_model_based_locscale(self):
        """
        Fast implementation test with small, low resolution map (1ake).
        """
        proc = job_running_test(
            os.path.join(
                self.test_data, "JobFiles/LocScale/locscale_localsharpen_job.star"
            ),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "1ake_10A_fitted.pdb")),
            ],
            [
                "run.out",
                "run.err",
                "locscale_output.mrc",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dic = {
            "maps": [
                "LocScale/job998/Thumbnails/1ake_10A.mrc",
                "LocScale/job998/Thumbnails/locscale_output.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "models": ["Import/job002/1ake_10A_fitted.pdb"],
            "title": "LocScale local sharpening",
            "models_data": "Import/job002/1ake_10A_fitted.pdb",
            "maps_data": (
                "Import/job001/1ake_10A.mrc, LocScale/job998/locscale_output.mrc"
            ),
            "associated_data": [
                "Import/job001/1ake_10A.mrc",
                "LocScale/job998/locscale_output.mrc",
                "Import/job002/1ake_10A_fitted.pdb",
            ],
        }
        for key in test_dic.keys():
            assert test_dic[key] == dispobjs[0].__dict__[key]


if __name__ == "__main__":
    unittest.main()
