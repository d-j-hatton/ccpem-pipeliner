#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
"""Utility functions to help with running tests."""

import os
import shutil
import time
import subprocess
import warnings
from contextlib import contextmanager
from glob import glob
from pathlib import Path

from pipeliner.job_runner import JobRunner
from pipeliner import job_factory
from pipeliner.data_structure import SUCCESS_FILE, RELION_GENERAL_PROCS
from pipeliner.utils import get_pipeliner_root, touch
from pipeliner_tests import test_data


test_data_dir = os.path.dirname(test_data.__file__)
CCPATH = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


def get_pipeliner_tests_root() -> Path:
    return Path(__file__).parent


def print_coms(commands, expected_commands):
    for line in zip(commands, expected_commands):
        print("\n" + line[0])
        print(line[1])
        compare = ""
        for i in range(len(line[0])):
            try:
                if line[0][i] == line[1][i]:
                    compare += " "
                else:
                    compare += "*"
            except IndexError:
                pass
        print(compare)


def general_get_command_test(
    self,
    jobtype,
    jobfile,
    jobnumber,
    input_nodes,
    output_nodes,
    expected_commands,
    show_coms=False,
    show_inputnodes=False,
    show_outputnodes=False,
    jobfiles_dir="JobFiles/{}/{}",
    job_out_dir=None,
    qsub=False,
):
    """Tests that executing the get command function returns the expected
    commands and input and output nodes. Input and output nodes and entered
    as a dict {NodeName:Nodetype}.  The full path is necessary for input nodes
    but only the file name for output nodes as it is assumed they are in the
    output directory."""

    jobfile_path = jobfiles_dir.format(jobtype, jobfile)
    job = job_factory.read_job(os.path.join(self.test_data, jobfile_path))
    if job_out_dir is None:
        outputname = jobtype + "/job{:03d}/".format(jobnumber)
    else:
        outputname = job_out_dir + "/job{:03d}/".format(jobnumber)
    job.output_name = outputname
    commandlist = job.get_commands()
    commands = job.prepare_final_command(outputname, commandlist, False)[0]

    command_strings = [" ".join(x) for x in commands]

    if job.subprocess_cwd:
        check_comp_command = [f"{CCPATH}  {' '.join(list(output_nodes))}"]
    else:
        check_comp_command = [f"{CCPATH} {outputname} {' '.join(list(output_nodes))}"]

    if not qsub and len(output_nodes) > 0:
        expected_commands = expected_commands + check_comp_command

    # add the time -p to every command
    if not qsub:
        for n, com in enumerate(expected_commands):
            if not com.startswith("time -p"):
                expected_commands[n] = "time -p " + com

    jobout = job_out_dir if job_out_dir is not None else jobtype

    assert command_strings == expected_commands, print_coms(
        command_strings, expected_commands
    )

    def print_inputnodes(expected):
        print("\nINPUT NODES: ({}/{})".format(len(job.input_nodes), len(expected)))
        for i in job.input_nodes:
            print(i.name)

    def print_outputnodes(expected):
        print("\nOUTPUT NODES: ({}/{})".format(len(job.output_nodes), len(expected)))
        for i in job.output_nodes:
            print(i.name)

    # make sure the expected nodes have been created
    expected_in_nodes = input_nodes
    expected_out_nodes = [
        jobout + "/job{:03d}/".format(jobnumber) + x for x in output_nodes
    ]
    actual_in_nodes = [x.name for x in job.input_nodes]
    actual_out_nodes = [x.name for x in job.output_nodes]

    if show_coms:
        print_coms([" ".join(x) for x in commands], expected_commands)
    if show_inputnodes:
        print_inputnodes(input_nodes)
    if show_outputnodes:
        print_outputnodes(output_nodes)

    for node in expected_in_nodes:
        assert node in actual_in_nodes, node
    for node in expected_out_nodes:
        assert node in actual_out_nodes, node

    # make sure no extra nodes have been produced
    for node in actual_in_nodes:
        assert node in expected_in_nodes, "EXTRA NODE: " + node
    for node in actual_out_nodes:
        assert node in expected_out_nodes, "EXTRA NODE: " + node

    # make sure all of the nodes are of the correct type
    for node in job.input_nodes:
        assert node.type == input_nodes[node.name], (
            node.name,
            "node error expect/act",
            input_nodes[node.name],
            node.type,
        )
    # using the expected list here, but it has already been checked
    for node in job.output_nodes:
        node_name = node.name.replace(job.output_name, "")
        assert node.type == output_nodes[node_name], (
            node_name,
            "node error expect/act",
            output_nodes[node_name],
            node.type,
        )

    return job


# TODO: This test assumes input files are in pipeliner_tests/test_data
#   pipeliner_tests.job_testing_tools.job_running_test() is a better
#   more robust function to use in most instances


def running_job(
    self,
    test_jobfile,
    job_dir_type,
    job_no,
    input_files,  # [(directory,file), ... (directory,file)]
    expected_outfiles,  # (file, file, ... file)
    sleep_time,  # to let the job finish - why is this needed?
    success=True,
    print_run=False,
    print_err=False,
    show_contents=False,
    jobfiles_dir="JobFiles/{}/{}",
    overwrite=False,
    target_job=None,
    n_metadata_files_expected=1,
):
    # Prepare the directory structure as if previous jobs have been run:
    for infile in input_files:
        import_dir = infile[0]
        if not os.path.isdir(import_dir):
            os.makedirs(import_dir)
        assert os.path.isfile(os.path.join(self.test_data, infile[1])), infile[1]
        shutil.copy(
            os.path.join(self.test_data, infile[1]),
            import_dir,
        )

    pipeline = JobRunner()
    pipeline.graph.job_counter = job_no
    jobfile_path = jobfiles_dir.format(job_dir_type, test_jobfile)
    job = job_factory.read_job(os.path.join(self.test_data, jobfile_path))
    job_dir = job_dir_type + "/job00{}/".format(job_no)

    extra_files = [
        "job.star",
        "note.txt",
        "job_pipeline.star",
        "run.out",
        "run.err",
    ]

    if success:
        extra_files.append(SUCCESS_FILE)

    job_run = pipeline.run_job(job, target_job, False, False, overwrite, False)

    # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
    time.sleep(sleep_time)
    if print_run:
        subprocess.run(["cat", os.path.join(job_dir, "run.out")])
    if print_err:
        subprocess.run(["cat", os.path.join(job_dir, "run.err")])

    if show_contents:
        subprocess.run(["ls", "-al"])
        subprocess.run(["ls", "-al", job_dir])
        subprocess.run(["cat", "default_pipeline.star"])
        subprocess.run(["cat", "{}/job.star".format(job_dir)])
        subprocess.run(["cat", "{}/run.out".format(job_dir)])
        subprocess.run(["cat", "{}/job_pipeline.star".format(job_dir)])

        # test for output files
    assert os.path.isdir(job_dir)
    for outfile in expected_outfiles:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for outfile in extra_files:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    assert os.path.isfile(".gui_" + job.PROCESS_NAME.replace(".", "_") + "job.star")

    # look for the metadata file
    md_files = glob(f"{job_dir}/*job_metadata.json")
    assert len(md_files) == n_metadata_files_expected, md_files

    return job_run


def make_shortpipe_filestructure(procs_to_make):
    """Builds the entire file structure for test_data/Pipelines/short_full_pipeline.star
    input a list of processes or 'all' to make the entire pipeline"""

    # only making the first few .Nodes dirs for testing deletion later
    nodes_files = [
        ".Nodes/0/Import/job001/movies.star",
        ".Nodes/13/MotionCorr/job002/logfile.pdf",
        ".Nodes/1/MotionCorr/job002/corrected_micrographs.star",
        ".Nodes/13/CtfFind/job003/logfile.pdf",
        ".Nodes/1/CtfFind/job003/micrographs_ctf.star",
    ]

    nodes_dirs = [
        ".Nodes/0/Import/job001",
        ".Nodes/13/MotionCorr/job002",
        ".Nodes/1/MotionCorr/job002",
        ".Nodes/13/CtfFind/job003",
        ".Nodes/1/CtfFind/job003",
    ]

    common_files = [
        "default_pipeline.star",
        "job_pipeline.star",
        "run.job",
        "note.txt",
        SUCCESS_FILE,
        "run.err",
        "run.out",
    ]

    loop_procs = {
        "MotionCorr": [
            "_Fractions0-Patch-FitCoeff.log",
            "_Fractions0-Patch-Frame.log",
            "_Fractions0-Patch-Full.log",
            "_Fractions0-Patch-Patch.log",
            "_Fractions.com",
            "_Fractions.err",
            "_Fractions.mrc",
            "_Fractions.out",
            "_Fractions_shifts.eps",
            "_Fractions.star",
        ],
        "CtfFind": [
            "_Fractions.mrc",
            "_Fractions.pow",
            "_Fractions.ctf",
            "_Fractions.gctf.log",
        ],
        "AutoPick": ["_Fractions_autopick.star", "_Fractions_autopick.spi"],
        "Extract": ["_Fractions.mrcs", "_Fractions_extract.star"],
        "Class2D": [
            "_data.star",
            "_model.star",
            "_classes.mrcs",
            "_optimiser.star",
            "_sampling.star",
        ],
        "Class3D": [
            "_class001.mrc",
            "_class001_angdist.bild",
            "_class002.mrc",
            "_class002_angdist.bild",
            "_class003.mrc",
            "_class003_angdist.bild",
            "_sampling.star",
            "_data.star",
            "_model.star",
            "_optimiser.star",
        ],
        "Refine3D": [
            "_half1_class001.mrc",
            "_half1_class001_angdist.bild",
            "_half2_class001.mrc",
            "_half2_class001_angdist.bild",
            "_half2_model.star",
            "_half1_model.star",
            "_sampling.star",
            "_data.star",
            "_optimiser.star",
        ],
        "InitialModel": [
            "_data.star",
            "_class001.mrc",
            "_grad001.mrc",
            "_class002.mrc",
            "_grad002.mrc",
            "_sampling.star",
            "_class001_data.star",
            "_class002_data.star",
            "_model.star",
            "_optimiser.star",
        ],
        # need to add optimiser files here
        "MultiBody": [
            "_data.star",
            "_half1_body001_angdist.bild",
            "_half1_body001.mrc",
            "_half1_body002_angdist.bild",
            "_half1_body002.mrc",
            "_half1_model.star",
            "_half2_body001_angdist.bild",
            "_half2_body001.mrc",
            "_half2_body002_angdist.bild",
            "_half2_body002.mrc",
            "_half2_model.star",
        ],
        "CtfRefine": [
            "_fit.star",
            "_fit.eps",
            "_wAcc.mrc",
            "_xyAcc_real.mrc",
            "_xyAcc_imag.mrc",
        ],
        "Polish": [
            "_FCC_cc.mrc",
            "_FCC_w0.mrc",
            "_FCC_w1.mrc",
            "_shiny.mrcs",
            "_shiny.star",
            "_tracks.eps",
            "_tracks.star",
        ],
        "Subtract": ["star", "_opticsgroup1.mrcs", "_opticsgroup2.mrcs"],
    }

    single_files = {
        "MotionCorr": [
            "corrected_micrographs_all_rlnAccumMotionEarly.eps",
            "corrected_micrographs_all_rlnAccumMotionLate.eps",
            "corrected_micrographs_all_rlnAccumMotionTotal.eps",
            "corrected_micrographs_hist_rlnAccumMotionEarly.eps",
            "corrected_micrographs_hist_rlnAccumMotionLate.eps",
            "corrected_micrographs_hist_rlnAccumMotionTotal.eps",
            "corrected_micrographs.star",
            "logfile.pdf",
            "logfile.pdf.lst",
        ],
        "CtfFind": [
            "gctf0.err",
            "gctf0.out",
            "gctf1.err",
            "gctf1.out",
            "micrographs_ctf_all_rlnCtfAstigmatism.eps",
            "micrographs_ctf_all_rlnCtfFigureOfMerit.eps",
            "micrographs_ctf_all_rlnDefocusAngle.eps",
            "micrographs_ctf_all_rlnDefocusU.eps",
            "micrographs_ctf_hist_rlnCtfAstigmatism.eps",
            "micrographs_ctf_hist_rlnCtfFigureOfMerit.eps",
            "micrographs_ctf_hist_rlnDefocusAngle.eps",
            "micrographs_ctf_hist_rlnDefocusU.eps",
            "micrographs_ctf.star",
            "logfile.pdf",
        ],
        "Select": ["backup_selection.star", "particles.star", "class_averages.star"],
        "Refine3D": [
            "run_class001.mrc",
            "run_class001_angdist.bild",
            "run_model.star",
            "run_sampling.star",
            "run_class001_half1_unfil.mrc",
            "run_class001_half2_unfil.mrc",
            "run_optimiser.star",
        ],
        "MultiBody": [
            "run_bodies.bild",
            "run_body001_angdist.bild",
            "run_body001_mask.mrc",
            "run_body001.mrc",
            "run_body002_angdist.bild",
            "run_body002_mask.mrc",
            "run_body002.mrc",
            "run_data.star",
            "run_half1_body001_unfil.mrc",
            "run_half1_body002_unfil.mrc",
            "run_half2_body001_unfil.mrc",
            "run_half2_body002_unfil.mrc",
            "run_optimiser.star",
        ],
        "CtfRefine": [
            "aberr_delta-phase_iter-fit_optics-group_1_N-4.mrc",
            "aberr_delta-phase_lin-fit_optics-group_1_N-4.mrc",
            "aberr_delta-phase_lin-fit_optics-group_1_N-4_residual.mrc",
            "aberr_delta-phase_per-pixel_optics-group_1.mrc",
            "asymmetric_aberrations_optics-group_1.eps",
            "beamtilt_delta-phase_iter-fit_optics-group_1_N-3.mrc",
            "beamtilt_delta-phase_lin-fit_optics-group_1_N-3.mrc",
            "beamtilt_delta-phase_lin-fit_optics-group_1_N-3_residual.mrc",
            "beamtilt_delta-phase_per-pixel_optics-group_1.mrc]",
            "particles_ctf_refine.star",
        ],
        "MaskCreate": ["mask.mrc.old", "mask.mrc"],
        "Polish": ["bfactors.eps", "bfactors.star", "scalefactors.eps", "shiny.star"],
        "PostProcess": [
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "postprocess.mrc",
            "postprocess.star",
            "logfile.pdf",
        ],
        "LocalRes": [
            "relion_locres.mrc",
            "relion_locres_filtered.mrc",
            "relion_locres_fscs.star]",
        ],
    }

    jobstar_files = {
        "Import/job001/": "import_movies_job.star",
        "MotionCorr/job002/": "motioncorr_own_job.star",
        "CtfFind/job003/": "ctffind_ctffind4_job.star",
        "AutoPick/job004/": "autopick_log_job.star",
        "Extract/job005/": "extract_job.star",
        "Class2D/job006/": "class2d_em_job.star",
        "Select/job007/": "select_interactive_job.star",
        "InitialModel/job008/": "initialmodel_job.star",
        "Class3D/job009/": "class3d_job.star",
        "Refine3D/job010/": "refine3d_job.star",
        "MultiBody/job011/": "multibody_refine_job.star",
        "CtfRefine/job012/": "ctfrefine_job.star",
        "MaskCreate/job013/": "maskcreate_job.star",
        "Polish/job014/": "polish_job.star",
        "JoinStar/job015/": "joinstar_micrographs_job.star",
        "Subtract/job016/": "subtract_job.star",
        "PostProcess/job017/": "postprocess_job.star",
        "External/job018/": "external_job.star",
        "LocalRes/job019/": "localres_own_job.star",
    }

    def make_files(jobdir, filelist, out_list, common=common_files):
        os.makedirs(jobdir)

        for node_dir in nodes_dirs:
            os.makedirs(node_dir, exist_ok=True)

        for f in nodes_files:
            touch(f)
        for f in common:
            touch(os.path.join(jobdir, f))

        for f in filelist:
            ff = os.path.join(jobdir, f)
            touch(ff)
            out_list.append(ff)
        # copy in an actual job.star file needed for cleanuo
        jobstar_dir = os.path.join(test_data_dir, "JobFiles/Default_JobStar/")
        jobstar_name = "default_relion_" + jobstar_files[jobdir]
        jobstar_file = os.path.join(jobstar_dir, jobstar_name)
        shutil.copy(jobstar_file, os.path.join(jobdir, "job.star"))
        out_list.append(os.path.join(jobdir, "job.star"))

    def make_loopfiles(writedir, file_list, n, fn, out=False):
        if not os.path.isdir(writedir):
            os.makedirs(writedir)

        for i in range(1, n + 1):
            for f in file_list:
                ff = writedir + "/" + fn.replace("*", "{:03d}".format(i)) + f
                touch(ff)
                if out:
                    out.append(ff)

    outfiles = {}
    for proc in RELION_GENERAL_PROCS:  # PROCS will be deprecated
        if proc in procs_to_make or procs_to_make == "all":
            if proc == "Import":
                jobdir = proc + "/job001/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, ["movies.star"], outfiles[proc])

            if proc == "MotionCorr":
                jobdir = proc + "/job002/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
                raw_dir = jobdir + "Raw_data"
                make_loopfiles(
                    raw_dir,
                    loop_procs[proc],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "CtfFind":
                jobdir = proc + "/job003/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
                raw_data = jobdir + "Raw_data"
                make_loopfiles(
                    raw_data,
                    loop_procs["CtfFind"],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "AutoPick":
                jobdir = proc + "/job004/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(
                    jobdir,
                    ["coords_suffix_autopick.star", "logfile.pdf"],
                    outfiles[proc],
                )
                raw_dir = jobdir + "Raw_data"
                make_loopfiles(
                    raw_dir,
                    loop_procs[proc],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "Extract":
                jobdir = proc + "/job005/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, ["particles.star"], outfiles[proc])
                raw_dir = jobdir + "Raw_data"
                make_loopfiles(
                    raw_dir,
                    loop_procs[proc],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "Class2D":
                jobdir = proc + "/job006/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, [], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1],
                    loop_procs[proc],
                    25,
                    "run_it*",
                    outfiles[proc],
                )

            if proc == "Select":
                jobdir = proc + "/job007/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])

            if proc == "InitialModel":
                jobdir = proc + "/job008/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, [], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1],
                    loop_procs[proc],
                    150,
                    "run_it*",
                    outfiles[proc],
                )

            if proc == "Class3D":
                jobdir = proc + "/job009/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, [], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1],
                    loop_procs[proc],
                    25,
                    "run_it*",
                    outfiles[proc],
                )

            if proc == "Refine3D":
                jobdir = proc + "/job010/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1],
                    loop_procs[proc],
                    16,
                    "run_it*",
                    outfiles[proc],
                )

            if proc == "MultiBody":
                jobdir = proc + "/job011/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1],
                    loop_procs[proc],
                    12,
                    "run_it*",
                    outfiles[proc],
                )
                for i in range(0, 6):
                    h = "analyse_component{:03d}_histogram.eps".format(i)
                    hh = os.path.join(jobdir, h)
                    touch(hh)
                    outfiles[proc].append(hh)

                    for j in range(1, 11):
                        f = "analyse_component{:03d}_bin{:03d}.mrc".format(i, j)
                        ff = os.path.join(jobdir, f)
                        touch(ff)
                        outfiles[proc].append(ff)

            if proc == "CtfRefine":
                jobdir = proc + "/job012/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files["CtfRefine"], outfiles[proc])
                raw_data = jobdir + "Raw_data"
                make_loopfiles(
                    raw_data,
                    loop_procs[proc],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "MaskCreate":
                jobdir = proc + "/job013/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])

            if proc == "Polish":
                jobdir = proc + "/job014/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
                raw_dir = jobdir + "Raw_data"
                make_loopfiles(
                    raw_dir,
                    loop_procs[proc],
                    100,
                    "FoilHole_62762_*",
                    outfiles[proc],
                )

            if proc == "JoinStar":
                jobdir = proc + "/job015/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, ["join_particles.star"], outfiles[proc])

            if proc == "Subtract":
                jobdir = proc + "/job016/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, ["particles_subtract.star"], outfiles[proc])
                part_dir = jobdir + "Particles"
                make_loopfiles(
                    part_dir,
                    loop_procs[proc],
                    10,
                    "subtracted_rank*",
                    outfiles[proc],
                )
            if proc == "PostProcess":
                jobdir = proc + "/job017/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])

            if proc == "External":
                jobdir = proc + "/job018/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, [], outfiles[proc])
                make_loopfiles(
                    jobdir[:-1], [".file"], 10, "External_file_*", outfiles[proc]
                )
            if proc == "LocalRes":
                jobdir = proc + "/job019/"
                outfiles[proc] = [jobdir + x for x in common_files]
                make_files(jobdir, single_files[proc], outfiles[proc])
    return outfiles


def check_for_plugin(plugin_file):
    """Check to see if a plugin is in the active plugins folder
    Used to skip plugin tests if the plugin in not installed"""

    plugin_dir = os.path.join(get_pipeliner_root(), "jobs/other")
    plugins = glob(plugin_dir + "/*_job.py")
    plugfiles = [os.path.basename(x) for x in plugins]
    if plugin_file in plugfiles:
        return True
    else:
        return False


def do_slow_tests():
    """Include slow-running tests if PIPELINER_TEST_SLOW is set"""
    return True if os.environ.get("PIPELINER_TEST_SLOW", False) else False


def do_interactive_tests():
    """Include tests with user interaction if PIPELINER_TEST_INTERACTIVE is set"""
    return True if os.environ.get("PIPELINER_TEST_INTERACTIVE", False) else False


def read_pipeline(pipeline_file):
    """Reads pipeline into a list where each line is a list of the entries on that line
    used for comparing pipeline outputs without having to worry about spaces or
    carriage returns, removes quotes on the entry names so all lines can be compared
    equally"""

    with open(pipeline_file) as pipeline:
        pipe_data = pipeline.readlines()
    split_data = [x.split() for x in pipe_data]
    final_data = []
    # if the first column has any quotes remove them
    for line in split_data:
        if len(line) >= 1:
            newline = []
            for i in line:
                if i[0] in ["'", '"']:
                    i = i[1:]
                if i[-1] in ["'", '"']:
                    i = i[:-1]
                newline.append(i)
            final_data.append(newline)
        else:
            final_data.append(line)
    return final_data


def make_conversion_file_structure():
    """Make all the jobfiles and dirs for testing pipeline conversion
    Each process needs a directory with a job.star file in it"""
    test_data = get_pipeliner_tests_root()
    files_path = os.path.join(test_data, "test_data/JobFiles/Tutorial_Pipeline/")
    jobfiles = glob(files_path + "*_job.star")
    for f in jobfiles:
        fsplit = os.path.basename(f).split("_")
        target_dir = fsplit[0] + "/" + fsplit[1]
        os.makedirs(target_dir)
        shutil.copy(f, os.path.join(target_dir, "job.star"))


def check_for_relion(version_required=3):
    """Check to see if Relion is available for tests
    Returns true if the Relion version is >=  the specified
    version."""

    try:
        check = subprocess.run(["relion", "--version"], stdout=subprocess.PIPE)
    except FileNotFoundError:
        return False

    vers_info = check.stdout.decode().split()
    if vers_info[0:2] != ["RELION", "version:"]:
        return False
    vers = int(vers_info[2].split(".")[0])
    if vers >= version_required:
        return True
    return False


@contextmanager
def expected_warning(type_, msg="", nwarn=1):
    """Simple context manager to catch a single warning (or multiple copies of the same
    warning) and check that it contains a given message.

    Args:
        type_ (type): the type of the warning, for example RuntimeWarning
        msg (str): optional string to check for in the warning message
        nwarn (bool): number of warnings expected
    """
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("default")
        yield
        print([x.__dict__ for x in w])
        assert len(w) == nwarn, len(w)
        assert issubclass(w[-1].category, type_), (w[-1].category, type_, str(w[-1]))
        assert msg in str(w[-1].message), (str(w[-1].message), msg)


def tutorial_data_available():
    """Check if the relion tutorial data is available

    Returns:
        bool: Was the env var found and does the dir it points to
            exist
    """
    tutorialdir = os.environ.get("PIPELINER_TUTORIAL_DATA")
    if tutorialdir is not None:
        return os.path.isdir(tutorialdir)
    else:
        return False


def get_relion_tutorial_data(dirs=None):
    """Copy in parts of the relion tutorial so it doesn't need
    to be included with the pipeliner

    Args:
        dirs (list): Which dirs to copy in, if `None` all will be done
    """

    dirs = [] if dirs is None else dirs
    if not tutorial_data_available():
        raise ValueError("Set the path to tutorial data with $PIPELINER_TUTORIAL_DATA")
    tutorialdir = os.environ.get("PIPELINER_TUTORIAL_DATA")
    if tutorialdir[-1] != "/":
        tutorialdir += "/"
    for d in os.walk(tutorialdir):
        dname = d[0].replace(tutorialdir, "")
        docopy = dname.split("/")[0] in dirs or dirs == []
        if not os.path.isdir(dname) and dname != "" and docopy:
            os.makedirs(dname)
        if len(d[2]) > 0 and docopy:
            for f in d[2]:
                fn = os.path.join(d[0], f)
                if os.path.isfile(fn) and f[0] != ".":
                    ln = fn.replace(tutorialdir, "")
                    os.symlink(fn, ln)
    # always copy in the default pipeline
    if os.path.islink("default_pipeline.star"):
        os.unlink("default_pipeline.star")
    shutil.copy(
        os.path.join(tutorialdir, "default_pipeline.star"), "default_pipeline.star"
    )
