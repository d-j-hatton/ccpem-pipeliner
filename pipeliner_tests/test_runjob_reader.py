#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
from pipeliner.runjob_reader import RunJobFile
import os
import tempfile
import shutil
from pipeliner_tests import test_data
from pipeliner.star_writer import COMMENT_LINE


class RunJobReaderTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories and get example file.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_all_options(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        the_rj = RunJobFile(testfile)
        options = the_rj.get_all_options()
        expected_dict = {
            "fn_in_other": "emd_3488.mrc",
            "node_type": "3D reference (.mrc)",
            "optics_group_particles": "",
            "_rlnJobTypeLabel": "relion.import.other",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
        }
        assert options == expected_dict

    def test_write_job_star_default_name(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        the_rj.convert_to_jobstar()
        exp_file = os.path.join(self.test_data, "JobFiles/Import/import_map_job.star")
        with open(exp_file, "r") as exp:
            expected = [x.split() for x in exp.readlines()]
        with open("test_job.star", "r") as wrt:
            written = [x.split() for x in wrt.readlines()]
        for i in expected:
            if len(i) > 0:
                if i[0] != "#":
                    assert i in written, i
        for i in written:
            if i != ["#"] + COMMENT_LINE.split():
                assert i in expected, i

    def test_write_job_star_custom_name(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        the_rj.convert_to_jobstar("conv_job.star")
        exp_file = os.path.join(self.test_data, "JobFiles/Import/import_map_job.star")
        with open(exp_file, "r") as exp:
            expected = [x.split() for x in exp.readlines()]
        with open("conv_job.star", "r") as wrt:
            written = [x.split() for x in wrt.readlines()]
        for i in expected:
            if len(i) > 0:
                if i[0] != "#":
                    assert i in written, i
        for i in written:
            if i != ["#"] + COMMENT_LINE.split():
                assert i in expected, i

    def test_write_job_star_custom_name_with_error(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        with self.assertRaises(ValueError):
            the_rj.convert_to_jobstar("conv.star")


if __name__ == "__main__":
    unittest.main()
