#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import unittest
import shutil
import os
import tempfile
from pipeliner.api import user_settings
from pipeliner_tests import test_data


class PipelinerUserSettingsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_create_settings(self):
        settings = user_settings.load_settings_json()
        assert settings is not None
        assert type(settings) is dict
        assert "path_to_source_files" in settings.keys()

    # def test_ccp4_progs_found(self):


if __name__ == "__main__":
    unittest.main()
