#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import general_get_command_test, running_job
from pipeliner.utils import touch
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_factory import active_job_from_proc


class ExternalProcessingImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_all_nodes_are_unique(self):
        """The basic test all imported outputs are unique so they just need
        to be copied in to the directory"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_simple.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        touch("processed_map.mrc")
        touch("logfile.log")
        mapout = os.path.abspath("processed_map.mrc")
        logout = os.path.abspath("logfile.log")
        general_get_command_test(
            self,
            "Import",
            "external_import_job.star",
            1,
            {
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.relion.refine3d",
            },
            {
                "processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "logfile.log": "LogFile.txt.externalprog",
            },
            [
                f"cp {mapout} Import/job001/processed" "_map.mrc",
                f"cp {logout} Import/job001/logfile.log",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_single_dir(self):
        """The imported nodes have the same name and a 1-deep directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth1.json"
        )
        shutil.copy(importfile, "test_external_import.json")

        os.makedirs("step1")
        os.makedirs("step2")
        touch("step1/processed_map.mrc")
        touch("step2/processed_map.mrc")
        mapout = os.path.abspath("step1/processed_map.mrc")
        logout = os.path.abspath("step2/processed_map.mrc")

        general_get_command_test(
            self,
            "Import",
            "external_import_job.star",
            1,
            {
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.relion.refine3d",
            },
            {
                "step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "step2/processed_map.mrc": "LogFile.txt.externalprog",
            },
            [
                "mkdir -p Import/job001/step1",
                f"cp {mapout} Import/job001/ste" "p1/processed_map.mrc",
                "mkdir -p Import/job001/step2",
                f"cp {logout} Import/job001/ste" "p2/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_longer_dir(self):
        """The imported nodes have the same name and a deeper directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth2.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1")
        os.makedirs("test2/step1")
        touch("test1/step1/processed_map.mrc")
        touch("test2/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("test2/step1/processed_map.mrc")
        general_get_command_test(
            self,
            "Import",
            "external_import_job.star",
            1,
            {
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.relion.refine3d",
            },
            {
                "test1/step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "test2/step1/processed_map.mrc": "LogFile.txt.externalprog",
            },
            [
                "mkdir -p Import/job001/test1/step1",
                f"cp {mapout} Import/job0" "01/test1/step1/processed_map.mrc",
                "mkdir -p Import/job001/test2/step1",
                f"cp {logout} Import/job0" "01/test2/step1/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_variable_dir(self):
        """The imported nodes have the same name and a deeper directory structure
        with varying lengths"""
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_depth_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1/")
        os.makedirs("silly/test1/step1")
        touch("test1/step1/processed_map.mrc")
        touch("silly/test1/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("silly/test1/step1/processed_map.mrc")
        general_get_command_test(
            self,
            "Import",
            "external_import_job.star",
            1,
            {
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.relion.refine3d",
            },
            {
                "test1/step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "silly/test1/step1/processed_map.mrc": "LogFile.txt.externalprog",
            },
            [
                "mkdir -p Import/job001/test1/step1",
                f"cp {mapout} Import/job0" "01/test1/step1/processed_map.mrc",
                "mkdir -p Import/job001/silly/test1/step1",
                f"cp {logout} Import/job001/silly/test1/step1/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_running(self):
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            self,
            "external_import_job.star",
            "Import",
            2,
            [
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job001", "particles.star"),
                ("SomeOtherProgram", "logfile.log"),
                ("SomeOtherProgram", "emd_3488_mask.mrc"),
            ],
            ["emd_3488_mask.mrc", "logfile.log"],
            1,
        )

    def test_running_multidir(self):
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_live_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            self,
            "external_import_job.star",
            "Import",
            2,
            [
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job002", "particles.star"),
                ("SomeOtherProgram/test1/", "emd_3488_mask.mrc"),
                ("SomeOtherProgram/test2/", "emd_3488_mask.mrc"),
            ],
            ["test1/emd_3488_mask.mrc", "test2/emd_3488_mask.mrc"],
            1,
        )

    def test_results_display(self):
        # setup and run the job
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            self,
            "external_import_job.star",
            "Import",
            2,
            [
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job001", "particles.star"),
                ("SomeOtherProgram", "logfile.log"),
                ("SomeOtherProgram", "emd_3488_mask.mrc"),
            ],
            ["emd_3488_mask.mrc", "logfile.log"],
            1,
        )
        proj = PipelinerProject()
        proc = proj.pipeline.process_list[0]
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        expected = [
            {
                "title": "Inputs",
                "headers": ["File", "NodeType"],
                "table_data": [
                    ["Refine3D/job001/emd_3488.mrc", "DensityMap.mrc.relion.refine3d"],
                    ["Refine3D/job001/particles.star", "ParticlesData.relion.refine3d"],
                ],
                "associated_data": [
                    "Refine3D/job001/emd_3488.mrc",
                    "Refine3D/job001/particles.star",
                ],
            },
            {
                "title": "Outputs",
                "headers": ["File", "NodeType"],
                "table_data": [
                    [
                        "Import/job002/emd_3488_mask.mrc",
                        "DensityMap.mrc.externalprog.fixed",
                    ],
                    ["Import/job002/logfile.log", "LogFile.txt.externalprog"],
                ],
                "associated_data": [
                    "Import/job002/emd_3488_mask.mrc",
                    "Import/job002/logfile.log",
                ],
            },
            {
                "title": "Processing Info",
                "display_data": (
                    "\n  Step 1: I did a thing,\n  Step 2: I did another thing,\n  "
                    "Step 3: \n    substep a: A result,\n    substep b: B result\n  "
                    "\n"
                ),
                "associated_data": ["Import/job002/external_processing.json"],
            },
        ]
        for n, do in enumerate(dispobjs):
            assert do.__dict__ == expected[n], [do.__dict__, expected[n]]


if __name__ == "__main__":
    unittest.main()
