#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import shutil
import os
import tempfile

from pipeliner.utils import (
    truncate_number,
    clean_jobname,
    smart_strip_quotes,
)
from pipeliner.mrc_image_tools import get_mrc_dims
from pipeliner_tests import test_data


class PipelinerUtilsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_truncate_number_seven_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 7)
        assert trunc == "1.6666667", trunc

    def test_truncate_number_two_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 2)
        assert trunc == "1.67", trunc

    def test_truncate_number_seven_places_trailing_zeros(self):
        x = 2.0
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_two_places_with_two_places_input(self):
        x = 0.75
        trunc = truncate_number(x, 2)
        assert trunc == "0.75", trunc

    def test_truncate_number_with_int(self):
        x = 2
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_with_large_int(self):
        x = 1234567890
        trunc = truncate_number(x, 7)
        assert trunc == "1234567890", trunc

    def test_truncate_number_with_near_multiple_of_ten(self):
        x = 1234567890.1
        trunc = truncate_number(x, 0)
        assert trunc == "1234567890", trunc

    def test_truncate_number_zero_places_rounding_up(self):
        x = 1.89
        trunc = truncate_number(x, 0)
        assert trunc == "2", trunc

    def test_truncate_number_zero_places_rounding_down(self):
        x = 1.324
        trunc = truncate_number(x, 0)
        assert trunc == "1", trunc

    def test_truncate_negative_number_zero_places_rounding_down(self):
        x = -1.324
        trunc = truncate_number(x, 0)
        assert trunc == "-1", trunc

    def test_truncate_negative_number_zero_places_rounding_up(self):
        x = -509.87
        trunc = truncate_number(x, 0)
        assert trunc == "-510", trunc

    def test_clean_jobname_with_trailing_slash(self):
        jobname = "Refine/job012/"
        cleaned = clean_jobname(jobname)
        assert cleaned == jobname

    def test_clean_jobname_without_trailing_slash(self):
        jobname = "Refine/job012"
        cleaned = clean_jobname(jobname)
        assert cleaned == "Refine/job012/"

    def test_clean_jobname_trailing_slash(self):
        jobname = clean_jobname("testjob/job003")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_leading_slash(self):
        jobname = clean_jobname("/testjob/job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_double_slash(self):
        jobname = clean_jobname("testjob//job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_multiple_problems(self):
        jobname = clean_jobname("/testjob////////////job003")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_illegal_character(self):
        with self.assertRaises(ValueError):
            clean_jobname("testjob/job@@3/")

    def test_get_mrc_dims_3d(self):
        dims = get_mrc_dims(os.path.join(self.test_data, "emd_3488.mrc"))
        assert dims == (100, 100, 100)

    def test_smart_strip_quotes_single(self):
        test_str = '"test"'
        result = smart_strip_quotes(test_str)
        assert result == "test"

    def test_smart_strip_quotes_double(self):
        test_str = "'test'"
        result = smart_strip_quotes(test_str)
        assert result == "test"

    def test_smart_strip_quotes_with_internal(self):
        test_str = '''"test with 'internal quotes'"'''
        result = smart_strip_quotes(test_str)
        assert result == "test with 'internal quotes'"


if __name__ == "__main__":
    unittest.main()
