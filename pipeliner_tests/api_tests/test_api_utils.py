#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner.api.api_utils import (
    write_default_jobstar,
    write_default_runjob,
    job_success,
)
from pipeliner.jobstar_reader import JobStar
from pipeliner.utils import touch
from pipeliner_tests.api_tests import test_data
from pipeliner.data_structure import ABORT_FILE, SUCCESS_FILE, FAIL_FILE
from pipeliner.api.api_utils import get_available_jobs


class APIUtilsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.pipeliner_test_data = os.path.join(
            os.path.abspath(os.path.join(__file__, "../..")),
            "test_data",
        )
        self.test_data = os.path.dirname(test_data.__file__)
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def general_default_writing_test(self, jobtype):
        write_default_jobstar(jobtype)

        jobtype_name = jobtype.replace(".", "_").lower()
        out_file = f"{jobtype_name}_job.star"
        exp_file = os.path.join(
            self.pipeliner_test_data,
            f"JobFiles/Default_JobStar/default_{jobtype_name}_job.star",
        )
        wrote = JobStar(out_file).get_all_options()
        try:
            expected = JobStar(exp_file).get_all_options()
        except FileNotFoundError:
            return f"File not found {exp_file}"

        special_lines = [
            "fn_motioncor2_exe",
            "fn_ctffind_exe",
            "fn_gctf_exe",
            "scratch_dir",
            "fn_topaz_exec",
            "fn_resmap",
        ]
        for jobop in wrote:
            if wrote[jobop] != expected[jobop] and jobop not in special_lines:
                return jobtype, jobop, wrote[jobop], expected[jobop]
        for jobop in expected:
            if wrote[jobop] != expected[jobop] and jobop not in special_lines:
                return jobtype, jobop, wrote[jobop], expected[jobop]

        return None

    def test_all_default_jobstar_writing(self):
        errors = []
        for job in [x[0] for x in get_available_jobs()]:
            err = self.general_default_writing_test(job)
            if err:
                errors.append(err)
        if errors:
            for i in errors:
                print(i)
            raise AssertionError("Errors in written files")

    def test_all_default_runjob_writing(self):
        errors = []
        for job in [x[0] for x in get_available_jobs()]:
            anyerr = self.general_default_writing_runjob(job)
            if anyerr:
                errors.append(anyerr)
        if errors:
            for i in errors:
                print("****\n", i)
            raise AssertionError("Errors in written files")

    def general_default_writing_runjob(self, jobtype):
        write_default_runjob(jobtype)
        jobtype_name = jobtype.replace(".", "_").lower()
        out_file = "{}_run.job".format(jobtype_name)

        template_file = "JobFiles/Default_RunJob/{}_run.job".format(jobtype_name)

        expected_file = os.path.join(self.pipeliner_test_data, template_file)
        with open(expected_file, "r") as expected:
            expected_lines = [
                x.replace(" ", "").split("==") for x in expected.readlines()
            ]
        with open(out_file, "r") as written_file:
            lines = written_file.readlines()
        written_lines = [x.replace(" ", "").split("==") for x in lines]
        actout = "".join(lines)

        # these are lines that will be different based of the
        # environment vars so they are skipped for now
        special_lines = [
            "Copyparticlestoscratchdirectory:",
            "MOTIONCOR2executable:",
            "Gctfexecutable:",
            "Standardsubmissionscript:",
            "Submittoqueue?",
            "Queuename:",
            "",
            "Queuesubmitcommand:",
            "CTFFIND-4.1executable:",
            "ResMapexecutable:",
            "Topazexecutable:",
        ]

        for line in written_lines:
            if line[0] not in special_lines and line != "":
                if line not in expected_lines:
                    return actout
        for line in expected_lines:
            if line[0] not in special_lines and line != "":
                if line not in expected_lines:
                    return actout
        return None

    def test_checking_job_success_succeeded(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        touch(os.path.join(job_name, SUCCESS_FILE))
        assert job_success(job_name)

    def test_checking_job_success_failed(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        touch(os.path.join(job_name, FAIL_FILE))
        assert not job_success(job_name)

    def test_checking_job_success_failed_with_error(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        touch(os.path.join(job_name, FAIL_FILE))
        with self.assertRaises(RuntimeError):
            job_success(job_name, raise_error=True)

    def test_checking_job_success_aborted(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        touch(os.path.join(job_name, ABORT_FILE))
        assert not job_success(job_name)

    def test_checking_job_success_aborted_with_error(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        touch(os.path.join(job_name, ABORT_FILE))
        with self.assertRaises(RuntimeError):
            job_success(job_name, raise_error=True)

    def test_checking_job_success_nofile(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        assert not job_success(job_name, search_time=0.1)

    def test_checking_job_success_nofile_with_error(self):
        job_name = "TestJob/job001"
        os.makedirs(job_name)
        with self.assertRaises(RuntimeError):
            job_success(job_name, raise_error=True, search_time=0.1)


if __name__ == "__main__":
    unittest.main()
