#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import time
import sys
import json
from glob import glob

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    make_conversion_file_structure,
    do_slow_tests,
    do_interactive_tests,
    check_for_relion,
)
from pipeliner_tests.generic_tests import (
    make_shortpipe_filestructure,
    read_pipeline,
)
from pipeliner.utils import touch, get_pipeliner_root
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    MOTIONCORR_OWN_NAME,
    CTFFIND_CTFFIND4_NAME,
    POSTPROCESS_JOB_NAME,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    JOBSTATUS_SCHED,
    PROJECT_FILE,
)
from pipeliner.jobstar_reader import modify_jobstar, JobStar
from pipeliner.api import CL_pipeline
from pipeliner.jobs.relion import (
    postprocess_job,
    motioncorr_job,
    ctffind_job,
    import_job,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.star_writer import COMMENT_LINE

do_flowchart_tests = True
try:
    import pipeliner.flowchart_illustration  # noqa: F401  # ignore unused import
except ImportError:
    do_flowchart_tests = False

do_full = do_slow_tests()
do_interactive = do_interactive_tests()
has_relion = check_for_relion()


class CL_PipelineTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.pipeliner_test_data = os.path.join(
            os.path.abspath(os.path.join(__file__, "../..")),
            "test_data",
        )
        self.old_stdin = sys.stdin
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        sys.stdin = self.old_stdin

    def read_pipeline(self, pipeline_name="default"):
        """Return the contents of the pipline as a string, for comparisons"""
        with open("{}_pipeline.star".format(pipeline_name)) as pipe:
            pipe_data = [x.split() for x in pipe.readlines()]
        return pipe_data

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def run_postprocess_job(self):
        """copy in the necessary files and run a postprocess job"""

        # get the files
        assert not os.path.isfile("default_pipeline.star")

        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )

        halfmap_import_dir = "HalfMaps"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Mask"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        # intialize a new project
        CL_pipeline.main(["--new_project"])

        # run the job
        CL_pipeline.main(["--run_job", jobfile])

        # wait for it to finish
        time.sleep(2)

    def schedule_job(self):
        """schedule a job to be used in later tests"""
        # get the files
        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )

        halfmap_import_dir = "HalfMaps"
        if not os.path.isdir(halfmap_import_dir):
            os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Mask"
        if not os.path.isdir(mask_import_dir):
            os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        # intialize a new project the first time
        if not os.path.isfile("default_pipeline.star"):
            CL_pipeline.main(["--new_project"])

        # schedule the job
        CL_pipeline.main(["--schedule_job", jobfile])

    def test_initialize_project(self):
        # there should not be a default_pipeline files
        assert not os.path.isfile("default_pipeline.star")

        # initialize a new project
        CL_pipeline.main(["--new_project"])

        # check the pipeline is written as expected
        assert os.path.isfile("default_pipeline.star")
        lines = [
            ["data_pipeline_general"],
            ["_rlnPipeLineJobCounter", "1"],
        ]
        pipe_data = self.read_pipeline()
        for line in lines:
            assert line in pipe_data, line
        with open(PROJECT_FILE) as project_file:
            project_name = json.load(project_file)["project name"]
        assert project_name == "New project", project_name

    def test_initialize_project_nondefault_name(self):
        # there should not be a default_pipeline files
        assert not os.path.isfile("fancyfancy_pipeline.star")

        # initialize a new project
        CL_pipeline.main(["--new_project", "fancyfancy"])

        # check the pipeline is written as expected
        assert os.path.isfile("fancyfancy_pipeline.star")
        lines = [
            ["data_pipeline_general"],
            ["_rlnPipeLineJobCounter", "1"],
        ]
        pipe_data = self.read_pipeline(pipeline_name="fancyfancy")
        for line in lines:
            if "# version" not in line:
                assert line in pipe_data, line

        with open(PROJECT_FILE) as project_file:
            proj_info = json.load(project_file)

        assert proj_info["pipeline file"] == "fancyfancy_pipeline.star"

    def test_initialize_project_name_error_illegal_symbol(self):
        """using an illegal symbol in pipeline name should raise error"""

        # initialize a new project with a bad name
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--new_project", "BAD!!"])

    def test_initialize_project_name_illegal_name(self):
        """calling th pipeline 'mini' should raise error"""

        # initialize a new project with a bad name
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--new_project", "mini"])

    def test_initializing_with_existing_project_fails(self):
        # make the directory already contain a pipeline files
        touch("default_pipeline.star")
        assert os.path.isfile("default_pipeline.star")

        # initializing a new project should should raise error
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--new_project"])

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_run_job(self):
        # run a postprocess job
        self.run_postprocess_job()

        # check the expected files are produced
        pp_files = [
            SUCCESS_FILE,
            "logfile.pdf",
            "logfile.pdf.lst",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in pp_files:
            assert os.path.isfile(os.path.join("PostProcess/job001", f)), f

        # check the expected lines are added to the pipeline
        pipe_lines = [
            ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                import_job.make_node_name(import_job.OUTPUT_NODE_HALFMAP, ".mrc"),
            ],
            ["PostProcess/job001/postprocess.mrc", postprocess_job.OUTPUT_NODE_MAP],
            [
                "PostProcess/job001/postprocess_masked.mrc",
                postprocess_job.OUTPUT_NODE_MASKED_MAP,
            ],
            ["PostProcess/job001/postprocess.star", postprocess_job.OUTPUT_NODE_POST],
            ["PostProcess/job001/logfile.pdf", postprocess_job.OUTPUT_NODE_LOG],
            ["Mask/emd_3488_mask.mrc", "PostProcess/job001/"],
            ["HalfMaps/3488_run_half1_class001_unfil.mrc", "PostProcess/job001/"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess.mrc"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess_masked.mrc"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess.star"],
            ["PostProcess/job001/", "PostProcess/job001/logfile.pdf"],
            ["_rlnPipeLineJobCounter", "2"],
        ]

        pipe_data = self.read_pipeline()
        for line in pipe_lines:
            assert line in pipe_data, line

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_run_job_time_analysis(self):
        # run a postprocess job
        self.run_postprocess_job()

        CL_pipeline.main(["--job_runtime", "PostProcess/job001"])

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_set_alias(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # change the alias
        CL_pipeline.main(["--set_alias", "PostProcess/job001/", "new_alias"])

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        new_ln = [
            "PostProcess/job001/",
            "PostProcess/new_alias/",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        old_ln = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        assert new_ln in pipe_data, new_ln
        assert old_ln not in pipe_data, old_ln
        return (new_ln, old_ln)

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_set_alias_and_clear(self):
        # run the previous test to make a job with an alias
        new_ln, old_ln = self.test_set_alias()

        # clear the alias
        CL_pipeline.main(["--clear_alias", "PostProcess/job001/"])

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        assert new_ln not in pipe_data, new_ln
        assert old_ln in pipe_data, old_ln

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_set_status_to_failed_short_entry(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        CL_pipeline.main(["--set_status", "PostProcess/job001/", "fail"])
        assert os.path.isfile("PostProcess/job001/" + FAIL_FILE)

    def test_set_status_to_failed_bad_entry(self):
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--set_status", "PostProcess/job001/", "not valid"])

    def test_set_status_to_failed_ambigous_entry(self):
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--set_status", "PostProcess/job001/", "s"])

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_set_status_to_failed_and_revert(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        CL_pipeline.main(["--set_status", "PostProcess/job001/", "failed"])
        assert os.path.isfile("PostProcess/job001/" + FAIL_FILE)

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        fail_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_FAIL,
        ]
        assert fail_line in pipe_data

        # change the status back to finished
        CL_pipeline.main(["--set_status", "PostProcess/job001/", "success"])
        assert os.path.isfile("PostProcess/job001/" + SUCCESS_FILE)
        assert not os.path.isfile("PostProcess/job001/" + FAIL_FILE)

        # check the pipeline has been updated
        fin_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        pipe_data = self.read_pipeline()
        assert fin_line in pipe_data

    def test_set_status_from_failed_to_aborted(self):
        """Can't make any type of status except running into aborted"""
        # run the postprocess job to make the files
        self.run_postprocess_job()
        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        CL_pipeline.main(["--set_status", "PostProcess/job001/", "failed"])
        assert os.path.isfile("PostProcess/job001/" + FAIL_FILE)

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        fail_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_FAIL,
        ]
        assert fail_line in pipe_data

        # set the status to aborted
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--set_status", "PostProcess/job001/", "aborted"])

    def test_deleting_job(self):
        # make the necessary files
        make_shortpipe_filestructure(["Import", "MotionCorr", "CtfFind"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_3jobs_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # count the files
        mocorr_file_count = len(os.listdir("MotionCorr/job002"))
        mocorr_rawdata_file_count = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count = len(os.listdir("CtfFind/job003"))

        # make sure the expected lines are in the pipeline
        pipe_data = self.read_pipeline()
        inc_lines = [
            ["MotionCorr/job002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/", "None", CTFFIND_CTFFIND4_NAME, JOBSTATUS_SUCCESS],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                motioncorr_job.OUTPUT_NODE_MICS,
            ],
            ["MotionCorr/job002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
            ["CtfFind/job003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
            ["CtfFind/job003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
            ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/job003/"],
            ["MotionCorr/job002/", "MotionCorr/job002/corrected_micrographs.star"],
            ["MotionCorr/job002/", "MotionCorr/job002/logfile.pdf"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
        ]
        for line in inc_lines:
            assert line in pipe_data, line

        # delete the MotionCorr job, child CtfFind job should go with it
        CL_pipeline.main(["--delete_job", "MotionCorr/job002/"])

        # check the lines are gone from the pipeline
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line not in pipe_data, line

        # make sure the files have been moved to the trash
        mocorr_trash_count = len(os.listdir("Trash/MotionCorr/job002"))
        mocorr_rawdata_trash_count = len(os.listdir("Trash/MotionCorr/job002/Raw_data"))
        ctffind_trash_count = len(os.listdir("Trash/CtfFind/job003"))
        assert mocorr_trash_count == mocorr_file_count
        assert mocorr_rawdata_trash_count == mocorr_rawdata_file_count
        assert ctffind_trash_count == ctffind_file_count

        # the job directories shoudl be gone
        assert not os.path.isdir("MotionCorr/job002")
        assert not os.path.isdir("CtfFind/job003")

    def test_delete_then_undelete(self):
        # make all of the necessary files
        make_shortpipe_filestructure(["Import", "MotionCorr", "CtfFind"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_3jobs_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/minipipe_mocorr_pipeline.star"),
            os.path.join(self.test_dir, "MotionCorr/job002/job_pipeline.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/minipipe_ctffind_pipeline.star"),
            os.path.join(self.test_dir, "CtfFind/job003/job_pipeline.star"),
        )

        # count the files
        mocorr_file_count = len(os.listdir("MotionCorr/job002"))

        mocorr_rawdata_file_count = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count = len(os.listdir("CtfFind/job003"))

        # read the default pipeline and check that the expected lines are there
        pipe_data = self.read_pipeline()
        inc_lines = [
            ["MotionCorr/job002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/", "None", CTFFIND_CTFFIND4_NAME, JOBSTATUS_SUCCESS],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                motioncorr_job.OUTPUT_NODE_MICS,
            ],
            ["MotionCorr/job002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
            ["CtfFind/job003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
            ["CtfFind/job003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
            ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/job003/"],
            ["MotionCorr/job002/", "MotionCorr/job002/corrected_micrographs.star"],
            ["MotionCorr/job002/", "MotionCorr/job002/logfile.pdf"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
        ]

        for line in inc_lines:
            assert line in pipe_data, [line]
        # delete the mocorr job along with 1 child
        CL_pipeline.main(["--delete_job", "MotionCorr/job002/"])

        # make sure the files have been moved to the trash
        assert not os.path.isdir("MotionCorr/job002")
        assert not os.path.isdir("CtfFind/job003")
        mocorr_trash_count = len(os.listdir("Trash/MotionCorr/job002"))
        mocorr_rawdata_trash_count = len(os.listdir("Trash/MotionCorr/job002/Raw_data"))
        ctffind_trash_count = len(os.listdir("Trash/CtfFind/job003"))
        assert mocorr_trash_count == mocorr_file_count
        assert mocorr_rawdata_trash_count == mocorr_rawdata_file_count
        assert ctffind_trash_count == ctffind_file_count

        # reread the pipeline and make sure the lines are gone
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line not in pipe_data, line

        # undelete the ctffind job - which should include 1 parent
        CL_pipeline.main(["--undelete_job", "CtfFind/job003/"])

        # check that the lines are back in the default pipeline
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line in pipe_data, line

        # count the files - make sure they have been moved back from the trash...
        assert not os.path.isdir("Trash/MotionCorr/job002")
        assert not os.path.isdir("Trash/CtfFind/job003")

        # ...and are back in the right place
        mocorr_file_count2 = len(os.listdir("MotionCorr/job002"))
        mocorr_rawdata_file_count2 = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count2 = len(os.listdir("CtfFind/job003"))

        assert mocorr_file_count2 == mocorr_file_count
        assert mocorr_rawdata_file_count2 == mocorr_rawdata_file_count
        assert ctffind_file_count2 == ctffind_file_count

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_schedule_job(self):
        job = "001"
        self.schedule_job()
        # check the expected lines are in the pipeline
        expected_lines = [
            [
                "PostProcess/job{}/".format(job),
                "None",
                POSTPROCESS_JOB_NAME,
                "Scheduled",
            ],
            [
                "Mask/emd_3488_mask.mrc",
                import_job.make_node_name(import_job.OUTPUT_NODE_MASK3D, ".mrc"),
            ],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                import_job.make_node_name(import_job.OUTPUT_NODE_HALFMAP, ".mrc"),
            ],
            [
                "PostProcess/job{}/postprocess.mrc".format(job),
                postprocess_job.OUTPUT_NODE_MAP,
            ],
            [
                "PostProcess/job{}/postprocess_masked.mrc".format(job),
                postprocess_job.OUTPUT_NODE_MASKED_MAP,
            ],
            [
                "PostProcess/job{}/postprocess.star".format(job),
                postprocess_job.OUTPUT_NODE_POST,
            ],
            [
                "PostProcess/job{}/logfile.pdf".format(job),
                postprocess_job.OUTPUT_NODE_LOG,
            ],
            ["Mask/emd_3488_mask.mrc", "PostProcess/job{}/".format(job)],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job{}/".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess.mrc".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess_masked.mrc".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess.star".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/logfile.pdf".format(job),
            ],
        ]

        pipe_data = self.read_pipeline()
        for line in expected_lines:
            assert line in pipe_data, line

        # check that the .Nodes files have been made
        expected_files = [
            ".Nodes/{}/PostProcess/job{}/postprocess.mrc".format(
                postprocess_job.OUTPUT_NODE_MAP.split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/postprocess_masked.mrc".format(
                postprocess_job.OUTPUT_NODE_MASKED_MAP.split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/logfile.pdf".format(
                postprocess_job.OUTPUT_NODE_LOG.split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/postprocess.star".format(
                postprocess_job.OUTPUT_NODE_POST.split(".")[0],
                job,
            ),
        ]

        for f in expected_files:
            assert os.path.isfile(f), f

    @unittest.skipUnless(
        do_full and has_relion, "Slow test: Only runs in full unittest"
    )
    def test_running_schedule(self):
        """Make a schedule with two jobs and run it 3 times"""

        # schedule two postprocess jobs
        self.schedule_job()
        self.schedule_job()

        # run the schedule 3 times
        CL_pipeline.main(
            [
                "--run_schedule",
                "--name",
                "schedule1",
                "--jobs",
                "PostProcess/job001/",
                "PostProcess/job002/",
                "--nr_repeats",
                "3",
                "--min_between",
                "0",
                "--wait_min_before",
                "0",
                "--wait_sec_after",
                "1",
            ]
        )

        # check the expected files are produced
        pp_files = [
            SUCCESS_FILE,
            "logfile.pdf",
            "logfile.pdf.lst",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in pp_files:
            for job in ["001", "002"]:
                assert os.path.isfile(os.path.join("PostProcess/job" + job, f)), f

        # check the jobs ran 3x and schedule log was written properly
        with open("pipeline_schedule1.log") as logfile:
            log_data = logfile.readlines()
        job001_count = 0
        job002_count = 0
        for line in log_data:
            if "---- Executing PostProcess/job001/" in line:
                job001_count += 1
            if "---- Executing PostProcess/job002/" in line:
                job002_count += 1
        assert job001_count == 3
        assert job002_count == 3

        # TODO: check the mini_pipeline is as expected

        # TODO: Add test the 2nd and 3rd runs were continues

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_running_schedule_fails_when_schedule_lock_present(self):
        """Make a schedule with two jobs but it fails because the schedule lock
        file is present"""

        # schedule two postprocess jobs
        self.schedule_job()
        self.schedule_job()
        touch("RUNNING_PIPELINER_default_schedule1")
        # run the schedule 3 times
        with self.assertRaises(ValueError):
            CL_pipeline.main(
                [
                    "--run_schedule",
                    "--name",
                    "schedule1",
                    "--jobs",
                    "PostProcess/job001/",
                    "PostProcess/job002/",
                    "--nr_repeats",
                    "3",
                    "--min_between",
                    "0",
                    "--wait_min_before",
                    "0",
                    "--wait_sec_after",
                    "1",
                ]
            )

    def test_cleanup_single_job(self):
        # create the files
        procname = "Class3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = make_shortpipe_filestructure([procname])
        files = glob("Class3D/job009/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("Class3D/job009/run_it*")
        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        CL_pipeline.main(["--cleanup", "Class3D/job009/"])

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Class3D/job009/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    @unittest.skipUnless(do_full, "Slow test only run in full unittest")
    def test_cleanup_multiple_jobs(self):
        # create the files
        procnames = ["Class3D", "InitialModel"]
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = make_shortpipe_filestructure(procnames)

        # list of files that should be deleted
        df1 = "Class3D/job009/run_it*"
        df2 = "InitialModel/job008/run_it*"
        del_files = glob(df1) + glob(df2)

        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
            "InitialModel/job008/run_it150_data.star",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_sampling.star",
            "InitialModel/job008/run_it150_model.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_grad001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_grad002.mrc",
        ]:
            del_files.remove(f)

        # do the cleanup
        CL_pipeline.main(["--cleanup", "Class3D/job009/", "InitialModel/job008/"])

        # sort the files
        removed, kept = [], []
        for procname in procnames:
            for f in outfiles[procname]:
                if f in del_files:
                    removed.append(f)
                else:
                    kept.append(f)

        # check they are all in the right place
        files = glob("Class3D/job009/*") + glob("InitialModel/job008/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_single_job_harsh(self):
        procname = "Polish"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = make_shortpipe_filestructure([procname])

        files = glob("Polish/job014/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = []
        for ext in [
            "*_FCC_cc.mrc",
            "*_FCC_w0.mrc",
            "*_FCC_w1.mrc",
            "*shiny.star",
            "*shiny.mrcs",
        ]:
            del_files += glob("Polish/job014/Raw_data/" + ext)

        # do the cleanup
        CL_pipeline.main(["--cleanup", "Polish/job014/", "--harsh"])

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Polish/job014/**/*", recursive=True)
        trash = glob("Trash/**/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    @unittest.skipUnless(do_full, "Slow test only run in full unittest")
    def test_cleanup_alljobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        make_shortpipe_filestructure("all")

        del_files = {
            "Import": [],
            "MotionCorr": [
                "/job002/Raw_data/*.com",
                "/job002/Raw_data/*.err",
                "/job002/Raw_data/*.out",
                "/job002/Raw_data/*.log",
            ],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*_extract.star"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
            ],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        CL_pipeline.main(["--cleanup", "ALL"])

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob("Trash/*/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert "Trash/" + f in trash, "Trash/" + f

    @unittest.skipUnless(do_full, "Slow test only run in full unittest")
    def test_cleanup_alljobs_harsh(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        make_shortpipe_filestructure("all")

        del_files = {
            "Import": [],
            "MotionCorr": ["/job002/Raw_data/*"],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*", "/job011/analyse_component*_bin*.mrc"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
                "/job014/Raw_data/*shiny.star",
                "/job014/Raw_data/*shiny.mrcs",
            ],
            "JoinStar": [],
            "Subtract": ["/job016/subtracted_*"],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        # run the cleanup
        CL_pipeline.main(["--cleanup", "ALL", "--harsh"])
        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob("Trash/*/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert "Trash/" + f in trash, "Trash/" + f

    def test_validate_starfile(self):
        """run validate on a good statfile"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_changed.star"),
            self.test_dir,
        )
        CL_pipeline.main(["--validate_starfile", "motioncorr_changed.star"])

    def test_validate_starfile_reserved_word(self):
        """run validate on a starfile with a reserved word"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"),
            self.test_dir,
        )
        CL_pipeline.main(["--validate_starfile", "motioncorr_not_quotated_fail.star"])
        badlines = [
            ["save_noDW", "No"],
            ["save_ps", "Yes"],
        ]
        goodlines = [
            ['"save_noDW"', "No"],
            ['"save_ps"', "Yes"],
        ]
        with open("motioncorr_not_quotated_fail.star") as fixed:
            fixed_data = [x.split() for x in fixed.readlines()]
        for line in badlines:
            assert line not in fixed_data, line
        for line in goodlines:
            assert line in fixed_data, line

    def test_validate_starfile_reserved_word_different_dir(self):
        """run validate on a starfile with a reserved word not located
        in the directory that the program was run from, make sure it writes in
        the correct place"""
        os.makedirs(os.path.join(self.test_dir, "1/2/3"))
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"),
            os.path.join(self.test_dir, "1/2/3"),
        )
        CL_pipeline.main(
            ["--validate_starfile", "1/2/3/motioncorr_not_quotated_fail.star"]
        )
        badlines = [
            ["save_noDW", "No"],
            ["save_ps", "Yes"],
        ]
        goodlines = [
            ['"save_noDW"', "No"],
            ['"save_ps"', "Yes"],
        ]
        with open("1/2/3/motioncorr_not_quotated_fail.star") as fixed:
            fixed_data = [x.split() for x in fixed.readlines()]
        for line in badlines:
            assert line not in fixed_data, line
        for line in goodlines:
            assert line in fixed_data, line

    def test_validate_starfile_unfixable(self):
        """run validate on a good starfile"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/invalid_starfile.star"),
            self.test_dir,
        )
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--validate_starfile", "invalid_starfile.star"])

    def test_deleting_file_then_empty_trash(self):
        self.test_deleting_job()
        nr_trash_files = len(glob("Trash/*/*/*"))
        assert nr_trash_files == 41
        with open("Yes", "w") as doit:
            doit.write("Yes")
        yes = open("Yes")
        sys.stdin = yes
        CL_pipeline.main(["--empty_trash"])
        nr_trash_files = len(glob("Trash/*/*/*"))
        assert nr_trash_files == 0
        yes.close()

    def test_empty_trash_error_nofiles(self):
        nr_trash_files = len(glob("Trash/*/*/*"))
        assert nr_trash_files == 0
        CL_pipeline.main(["--new_project"])
        assert not CL_pipeline.main(["--empty_trash"])

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_upstream(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )
        # make it like CL relion has already been used
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "Refine3D/job010/", "--upstream"])
        assert os.path.isfile("Refine3D_job010_parent.png")

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_downstream(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "Refine3D/job010/", "--downstream"])
        assert os.path.isfile("Refine3D_job010_child.png")

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_full(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart"])
        assert os.path.isfile("full_project_flowchart.png")

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_up_and_down(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "Refine3D/job010/"])

    @unittest.skipUnless(
        do_interactive and do_flowchart_tests,
        "Requires interaction: only run in full unittest",
    )
    def test_drawing_flowchart_upstream_interactive(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )
        # make it like CL relion has already been used
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(
            ["--draw_flowchart", "Refine3D/job010/", "--upstream", "--interactive"]
        )

    @unittest.skipUnless(
        do_interactive and do_flowchart_tests,
        "Requires interaction: only run in full unittest",
    )
    def test_drawing_flowchart_downstream_interactive(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(
            ["--draw_flowchart", "Refine3D/job010/", "--downstream", "--interactive"]
        )

    @unittest.skipUnless(
        do_interactive and do_flowchart_tests,
        "Requires interaction: only run in full unittest",
    )
    def test_drawing_flowchart_full_interactive(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "--interactive"])

    @unittest.skipUnless(
        do_interactive and do_flowchart_tests,
        "Requires interaction: only run in full unittest",
    )
    def test_drawing_flowchart_up_and_down_interactive(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "Refine3D/job010/", "--interactive"])

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_error_upstream_but_no_job(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "--upstream"])

    @unittest.skipUnless(
        do_flowchart_tests, "requires flowchart drawing to be installed"
    )
    def test_drawing_flowchart_error_downstream_but_no_job(self):
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/short_full_pipeline.star"
            ),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        CL_pipeline.main(["--draw_flowchart", "--upstream"])

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_continuing_job_basic(self):
        """Make sure continuation creates the job.star.ct000 file"""
        # run a postprocess job
        self.run_postprocess_job()
        # continue that job
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 2, count

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_continuing_job_multiple(self):
        """Make sure multiple continuations increment the job.star.ctxxx file"""
        # run a postprocess job
        self.run_postprocess_job()
        # continue that job 3x
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)

        # make sure the files are there
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        assert os.path.isfile("PostProcess/job001/job.star.ct001")
        assert os.path.isfile("PostProcess/job001/job.star.ct002")

        # make sure it actually ran 4x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 4, count

    @unittest.skipUnless(do_full and has_relion, "Slow test: only run in full unittest")
    def test_continuing_job_continue_file_is_missing(self):
        """If the continue_job.star file is missing fall back to the
        orginial job.star"""
        # run a postprocess job
        self.run_postprocess_job()
        # continue that job
        os.remove("PostProcess/job001/continue_job.star")
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 2, count

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_continuing_job_both_files_missing(self):
        """If the continue_job.star and the job.star files are
        missing the continue can't run"""
        # run a postprocess job
        self.run_postprocess_job()
        # continue that job
        os.remove("PostProcess/job001/continue_job.star")
        os.remove("PostProcess/job001/job.star")
        with self.assertRaises(ValueError):
            CL_pipeline.main(["--continue_job", "PostProcess/job001"])

    @unittest.skipUnless(do_full and has_relion, "Slow test: only run in full unittest")
    def test_continuing_job_parameters_are_set_correctly(self):
        """Make sure continuation subs in the values from the continue
        as expected"""
        # run a postprocess job
        self.run_postprocess_job()

        # read the command that was executed
        with open("PostProcess/job001/note.txt") as notefile:
            note1 = notefile.read()

        # edit the continue_file
        contfile = "PostProcess/job001/continue_job.star"
        modify_jobstar(contfile, {"angpix": "2.55"}, contfile)

        # continue that job
        CL_pipeline.main(["--continue_job", "PostProcess/job001"])
        time.sleep(1)
        assert os.path.isfile("PostProcess/job001/job.star.ct000")

        # read the new command
        with open("PostProcess/job001/note.txt") as notefile:
            note2 = notefile.read()

        # make sure the commands are different
        assert "--angpix 1.244" in note1
        assert "--angpix 2.55" in note2

        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()

        # make sure it was run with the new parameter
        resolution_lines = []
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                resolution_lines.append(line)
        assert len(resolution_lines) == 2
        assert resolution_lines[0] != resolution_lines[1]

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_scheduling_continue_job(self):
        """Make sure scheduling continuation of a job
        works as expected"""
        # run a postprocess job
        self.run_postprocess_job()

        line = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS]
        pipeline = self.read_pipeline()

        assert line in pipeline, line
        # schedule a continuation of that job
        CL_pipeline.main(["--schedule_job", "PostProcess/job001"])

        line = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED]
        pipeline = self.read_pipeline()
        assert line in pipeline

    def test_convert_pipeline(self):
        """Convert an old style pipeline to a new style one"""
        # copy in the pipeline - old version
        make_conversion_file_structure()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003//micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/relion31_tutorial_pipeline.star"
            ),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        # copy in the pipeline - new version
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/converted_relion31_pipeline.star"
            ),
            self.test_dir,
        )
        CL_pipeline.main(["--convert_pipeline_file", "default_pipeline.star"])

        # get the pipeline data
        written = read_pipeline("default_pipeline.star")
        new_style = read_pipeline("converted_relion31_pipeline.star")
        # compare the two
        for line in written:
            if line != ["#"] + COMMENT_LINE.split():
                assert line in new_style, line
        for line in new_style:
            if line[0:2] != ["#", "version"]:
                assert line in written, line

    def test_convert_pipeline_convert_error(self):
        """Convert error raised when pipeline is already new style"""
        # copy in the pipeline - new version
        shutil.copy(
            os.path.join(
                self.pipeliner_test_data, "Pipelines/converted_relion31_pipeline.star"
            ),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        assert not CL_pipeline.main(
            ["--convert_pipeline_file", "default_pipeline.star"]
        )

    def test_stop_schedule_GUI_style(self):
        """if the schedule is made by the GUI the RUNNING_ file is empty
        and should be deleted"""
        # make the RUNNING_ file
        touch("RUNNING_PIPELINER_default_empty")
        assert os.path.isfile("RUNNING_PIPELINER_default_empty")

        # stop the schedule
        CL_pipeline.main(["--new_project"])
        CL_pipeline.main(["--stop_schedule", "empty"])

        # file should be gone
        assert not os.path.isfile("RUNNING_PIPELINER_default_empty")

    def test_stop_schedule_from_API_style(self):
        """if the schedule is made by the API the RUNNING_ file
        just has a list of jobs"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data[0] == "PostProcess/job001/\n"

        # initialize the project and copy in the pipeline
        CL_pipeline.main(["--new_project"])
        shutil.copy(
            os.path.join(self.pipeliner_test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        CL_pipeline.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_FILE)

        # pipeline should be updated
        with open("default_pipeline.star") as f:
            pipeline_data = [x.split() for x in f.readlines()]
        newline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_ABORT]
        oldline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_RUN]

        assert newline in pipeline_data, newline
        assert oldline not in pipeline_data, oldline

    def test_stop_schedule_from_API_style_pipeliner_custom_name(self):
        """if the schedule is made by the API the RUNNING_ file
        just has a list of jobs.  Check this works if the pipeline
        name is not default"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_fancypants_api")
        with open("RUNNING_PIPELINER_fancypants_api", "w") as f:
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_fancypants_api")
        with open("RUNNING_PIPELINER_fancypants_api") as f:
            runfile_data = f.readlines()
        assert runfile_data[0] == "PostProcess/job001/\n"

        # initialize the project and copy in the pipeline
        CL_pipeline.main(["--new_project", "fancypants"])
        shutil.copy(
            os.path.join(self.pipeliner_test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "fancypants_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        CL_pipeline.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_fancypants_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_FILE)

        # pipeline should be updated
        with open("fancypants_pipeline.star") as f:
            pipeline_data = [x.split() for x in f.readlines()]
        newline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_ABORT]
        oldline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_RUN]

        assert newline in pipeline_data, newline
        assert oldline not in pipeline_data, oldline

    def test_stop_schedule_from_CL_style(self):
        """if the schedule is made by CL_pipeline the RUNNING_ file
        has a PID and a list of jobs, the schedule process needs to
        be killed as well"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("CL_RELION_SCHEDULE\n")
            f.write("9999999999\n")
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data == [
            "CL_RELION_SCHEDULE\n",
            "9999999999\n",
            "PostProcess/job001/\n",
        ]

        # initialize the project and copy in the pipeline
        CL_pipeline.main(["--new_project"])
        shutil.copy(
            os.path.join(self.pipeliner_test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        CL_pipeline.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_FILE)

        # pipeline should be updated
        with open("default_pipeline.star") as f:
            pipeline_data = [x.split() for x in f.readlines()]
        newline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_ABORT]
        oldline = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_RUN]

        assert newline in pipeline_data, newline
        assert oldline not in pipeline_data, oldline

    def test_write_default_starfile(self):
        """No need to test for all types as this is done in api_utils
        tests"""
        CL_pipeline.main(["--default_jobstar", "relion.import.movies"])
        assert os.path.isfile("relion_import_movies_job.star")

    def test_write_default_runjob(self):
        """No need to test for all types as this is done in api_utils
        tests"""
        CL_pipeline.main(["--default_runjob", "relion.import.movies"])
        assert os.path.isfile("relion_import_movies_run.job")

    def test_default_writing_relionstyle_jobstar(self):
        CL_pipeline.main(["--default_jobstar", "relion.import", "--relionstyle"])
        wrote = JobStar("relion_import_job.star").get_all_options()

        template_file = os.path.join(
            get_pipeliner_root(),
            "jobs/relion/relion_jobstars/default_relion_import_job.star",
        )
        exp_data = JobStar(template_file).get_all_options()

        # add in joboptions set by envvars
        dj = new_job_of_type("dummyjob")
        for jobop in dj.joboptions:
            if jobop in exp_data:
                exp_data[jobop] = dj.joboptions[jobop].get_string()
        for jobop in exp_data:
            assert exp_data[jobop] == wrote[jobop], (
                jobop,
                exp_data[jobop],
                wrote[jobop],
            )
        for jobop in wrote:
            assert exp_data[jobop] == wrote[jobop], (
                jobop,
                exp_data[jobop],
                wrote[jobop],
            )


if __name__ == "__main__":
    unittest.main()
