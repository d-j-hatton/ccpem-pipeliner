
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      30
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/ Import/movies/            relion.import.movies            Succeeded 
MotionCorr/job002/ MotionCorr/own/            relion.motioncorr.own            Succeeded 
CtfFind/job003/ CtfFind/gctf/            relion.ctffind.gctf            Succeeded 
ManualPick/job004/ ManualPick/illustrate_only/            relion.manualpick            Succeeded 
Select/job005/ Select/5mics/            relion.Select.interactive           Succeeded 
AutoPick/job006/ AutoPick/LoG_based/            relion.autopick.log            Succeeded 
Extract/job007/ Extract/LoG_based/            relion.extract            Succeeded 
Class2D/job008/ Class2D/LoG_based/            relion.class2d.em            Succeeded
Select/job009/ Select/templates4autopick/            relion.Select.interactive            Succeeded
AutoPick/job010/ AutoPick/optimise_params/            relion.autopick.ref2d            Succeeded 
AutoPick/job011/ AutoPick/template_based/            relion.autopick.ref2d            Succeeded 
Extract/job012/ Extract/template_based/            relion.extract            Succeeded 
Select/job014/ Select/after_sort/            relion.Select.interactive            Succeeded 
Class2D/job015/ Class2D/after_sorting/            relion.class2d.em            Succeeded 
Select/job016/ Select/class2d_aftersort/            relion.select.onvalue           Succeeded 
InitialModel/job017/ InitialModel/symC1/           relion.initialmodel           Succeeded 
Class3D/job018/ Class3D/first_exhaustive/            relion.class3d            Succeeded 
Select/job019/ Select/class3d_first_exhaustive/            relion.select.interactive            Succeeded 
Extract/job020/ Extract/best3dclass_bigbox/            relion.extract.reextract            Succeeded 
Refine3D/job021/ Refine3D/first3dref/           relion.refine3d            Succeeded 
MaskCreate/job022/ MaskCreate/first3dref/           relion.maskcreate            Succeeded 
PostProcess/job023/ PostProcess/first3dref/           relion.postprocess            Succeeded 
CtfRefine/job024/       None           relion.ctfrefine            Succeeded 
Polish/job025/ Polish/train/           relion.polish.train            Succeeded 
Polish/job026/ Polish/polish/          relion.polish            Succeeded 
Refine3D/job027/ Refine3D/polished/           relion.refine3d            Succeeded 
PostProcess/job028/ PostProcess/polished/           relion.postprocess            Succeeded 
LocalRes/job029/ LocalRes/polished/           relion.localRes.resmap            Succeeded 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star             		relion.MovieStar
MotionCorr/job002/corrected_micrographs.star            relion.MicrographStar 
MotionCorr/job002/logfile.pdf           relion.PdfLogfile 
CtfFind/job003/micrographs_ctf.star            relion.MicrographStar 
CtfFind/job003/logfile.pdf           relion.PdfLogfile 
ManualPick/job004/coords_suffix_manualpick.star            relion.CoordinateStar 
ManualPick/job004/micrographs_selected.star            relion.MicrographStar 
Select/job005/micrographs_selected.star            relion.MicrographStar 
AutoPick/job006/coords_suffix_autopick.star            relion.CoordinateStar 
AutoPick/job006/logfile.pdf           relion.PdfLogfile 
Extract/job007/particles.star            relion.ParticleStar 
Class2D/job008/run_it025_data.star            relion.ParticleStar 
Class2D/job008/run_it025_optimiser.star            relion.OptimiserStar 
Select/job009/particles.star            relion.ParticleStar 
Select/job009/class_averages.star            relion.ReferenceStar 
AutoPick/job010/coords_suffix_autopick.star            relion.CoordinateStar 
AutoPick/job010/logfile.pdf           relion.PdfLogfile 
AutoPick/job011/coords_suffix_autopick.star            relion.CoordinateStar 
AutoPick/job011/logfile.pdf           relion.PdfLogfile 
Extract/job012/particles.star            relion.ParticleStar 
Select/job014/particles.star            relion.ParticleStar 
Class2D/job015/run_it025_data.star            relion.ParticleStar 
Class2D/job015/run_it025_optimiser.star            relion.OptimiserStar 
Select/job016/particles.star            relion.ParticleStar 
Select/job016/class_averages.star            relion.ReferenceStar 
InitialModel/job017/run_it150_data.star            relion.ParticleStar 
InitialModel/job017/run_it150_model.star            relion.OptimiserStar 
InitialModel/job017/run_it150_class001.mrc            relion.DensityMap 
InitialModel/job017/run_it150_class001_symD2.mrc            relion.DensityMap 
Class3D/job018/run_it025_data.star            relion.ParticleStar 
Class3D/job018/run_it025_optimiser.star            relion.OptimiserStar 
Class3D/job018/run_it025_class001.mrc            relion.DensityMap 
Class3D/job018/run_it025_class002.mrc            relion.DensityMap 
Class3D/job018/run_it025_class003.mrc            relion.DensityMap
Class3D/job018/run_it025_class004.mrc            relion.DensityMap
Select/job019/particles.star            relion.ParticleStar 
Class3D/job018/run_it025_class001_box256.mrc            relion.DensityMap 
Extract/job020/particles.star            relion.ParticleStar 
Extract/job020/coords_suffix_extract.star            relion.CoordinateStar 
Refine3D/job021/run_data.star            relion.ParticleStar 
Refine3D/job021/run_half1_class001_unfil.mrc           relion.HalfMap 
Refine3D/job021/run_class001.mrc            relion.DensityMap 
MaskCreate/job022/mask.mrc            relion.Mask 
PostProcess/job023/postprocess.mrc           relion.DensityMap 
PostProcess/job023/postprocess_masked.mrc           relion.DensityMap 
PostProcess/job023/logfile.pdf           relion.PdfLogfile 
PostProcess/job023/postprocess.star           relion.PostprocessStar 
CtfRefine/job024/logfile.pdf           relion.PdfLogfile 
CtfRefine/job024/particles_ctf_refine.star            relion.ParticleStar 
Polish/job025/opt_params.txt           relion.PolishParams 
Polish/job026/logfile.pdf           relion.PdfLogfile 
Polish/job026/shiny.star            relion.ParticleStar
Refine3D/job027/run_data.star            relion.ParticleStar 
Refine3D/job027/run_half1_class001_unfil.mrc           relion.HalfMap 
Refine3D/job027/run_class001.mrc            relion.DensityMap 
PostProcess/job028/postprocess.mrc           relion.DensityMap 
PostProcess/job028/postprocess_masked.mrc           relion.DensityMap 
PostProcess/job028/logfile.pdf           relion.PdfLogfile 
PostProcess/job028/postprocess.star           relion.PostprocessStar 
LocalRes/job029/relion_locres_filtered.mrc           relion.DensityMap 
LocalRes/job029/relion_locres.mrc           relion.LocalResolutionMap 
LocalRes/job029/flowchart.pdf           relion.PdfLogfile 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
ManualPick/job004/coords_suffix_manualpick.star Select/job005/ 
Select/job005/micrographs_selected.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/coords_suffix_autopick.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 
Class2D/job008/run_it025_optimiser.star Select/job009/ 
Select/job005/micrographs_selected.star AutoPick/job010/ 
Select/job009/class_averages.star AutoPick/job010/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job011/ 
Select/job009/class_averages.star AutoPick/job011/ 
CtfFind/job003/micrographs_ctf.star Extract/job012/ 
AutoPick/job011/coords_suffix_autopick.star Extract/job012/ 
Extract/job012/particles.star Select/job014/
Select/job014/particles.star Class2D/job015/ 
Class2D/job015/run_it025_optimiser.star Select/job016/ 
Select/job016/particles.star InitialModel/job017/ 
Select/job016/particles.star Class3D/job018/ 
InitialModel/job017/run_it150_class001_symD2.mrc Class3D/job018/ 
Class3D/job018/run_it025_optimiser.star Select/job019/ 
CtfFind/job003/micrographs_ctf.star Extract/job020/ 
Select/job019/particles.star Extract/job020/ 
Extract/job020/particles.star Refine3D/job021/ 
Class3D/job018/run_it025_class001_box256.mrc Refine3D/job021/ 
Refine3D/job021/run_class001.mrc MaskCreate/job022/ 
MaskCreate/job022/mask.mrc PostProcess/job023/ 
Refine3D/job021/run_half1_class001_unfil.mrc PostProcess/job023/ 
Refine3D/job021/run_data.star CtfRefine/job024/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job025/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job026/ 
Polish/job026/shiny.star Refine3D/job027/ 
Refine3D/job021/run_class001.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc LocalRes/job029/ 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/coords_suffix_manualpick.star 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
Select/job005/ Select/job005/micrographs_selected.star 
AutoPick/job006/ AutoPick/job006/coords_suffix_autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
Class2D/job008/ Class2D/job008/run_it025_data.star 
Class2D/job008/ Class2D/job008/run_it025_optimiser.star 
Select/job009/ Select/job009/particles.star 
Select/job009/ Select/job009/class_averages.star 
AutoPick/job010/ AutoPick/job010/coords_suffix_autopick.star 
AutoPick/job010/ AutoPick/job010/logfile.pdf 
AutoPick/job011/ AutoPick/job011/coords_suffix_autopick.star 
AutoPick/job011/ AutoPick/job011/logfile.pdf 
Extract/job012/ Extract/job012/particles.star 
Select/job014/ Select/job014/particles.star 
Class2D/job015/ Class2D/job015/run_it025_data.star 
Class2D/job015/ Class2D/job015/run_it025_optimiser.star 
Select/job016/ Select/job016/particles.star 
Select/job016/ Select/job016/class_averages.star 
InitialModel/job017/ InitialModel/job017/run_it150_data.star 
InitialModel/job017/ InitialModel/job017/run_it150_model.star 
InitialModel/job017/ InitialModel/job017/run_it150_class001.mrc 
InitialModel/job017/ InitialModel/job017/run_it150_class001_symD2.mrc 
Class3D/job018/ Class3D/job018/run_it025_data.star 
Class3D/job018/ Class3D/job018/run_it025_optimiser.star 
Class3D/job018/ Class3D/job018/run_it025_class001.mrc 
Class3D/job018/ Class3D/job018/run_it025_class002.mrc 
Class3D/job018/ Class3D/job018/run_it025_class003.mrc 
Class3D/job018/ Class3D/job018/run_it025_class004.mrc 
Class3D/job018/ Class3D/job018/run_it025_class001_box256.mrc 
Select/job019/ Select/job019/particles.star 
Extract/job020/ Extract/job020/particles.star 
Extract/job020/ Extract/job020/coords_suffix_extract.star 
Refine3D/job021/ Refine3D/job021/run_data.star 
Refine3D/job021/ Refine3D/job021/run_half1_class001_unfil.mrc 
Refine3D/job021/ Refine3D/job021/run_class001.mrc 
MaskCreate/job022/ MaskCreate/job022/mask.mrc 
PostProcess/job023/ PostProcess/job023/postprocess.mrc 
PostProcess/job023/ PostProcess/job023/postprocess_masked.mrc 
PostProcess/job023/ PostProcess/job023/logfile.pdf 
PostProcess/job023/ PostProcess/job023/postprocess.star 
CtfRefine/job024/ CtfRefine/job024/logfile.pdf 
CtfRefine/job024/ CtfRefine/job024/particles_ctf_refine.star 
Polish/job025/ Polish/job025/opt_params.txt 
Polish/job026/ Polish/job026/logfile.pdf 
Polish/job026/ Polish/job026/shiny.star 
Refine3D/job027/ Refine3D/job027/run_data.star 
Refine3D/job027/ Refine3D/job027/run_half1_class001_unfil.mrc 
Refine3D/job027/ Refine3D/job027/run_class001.mrc 
PostProcess/job028/ PostProcess/job028/postprocess.mrc 
PostProcess/job028/ PostProcess/job028/postprocess_masked.mrc 
PostProcess/job028/ PostProcess/job028/logfile.pdf 
PostProcess/job028/ PostProcess/job028/postprocess.star 
LocalRes/job029/ LocalRes/job029/relion_locres_filtered.mrc 
LocalRes/job029/ LocalRes/job029/relion_locres.mrc 
LocalRes/job029/ LocalRes/job029/flowchart.pdf 
 
