
# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnImagePixelSize #8 
_rlnImageSize #9 
_rlnImageDimensionality #10 
_rlnDetectorPixelSize #11 
_rlnMagnification #12 
convertedOpticsGroup1            1           "" 1.215601562261156   300.000000     2.000000     0.070000 1.215601562261156        128ls            2     5.000000 41131.898438 
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_particles

loop_ 
_rlnRandomSubset #1 
_rlnAnglePsi #2 
_rlnMicrographName #3 
_rlnAngleRot #4 
_rlnClassNumber #5 
_rlnCtfFigureOfMerit #6 
_rlnDefocusAngle #7 
_rlnDefocusV #8 
_rlnAngleTilt #9 
_rlnAutopickFigureOfMerit #10 
_rlnMaxValueProbDistribution #11 
_rlnOriginY #12 
_rlnGroupNumber #13 
_rlnDefocusU #14 
_rlnOriginX #15 
_rlnNormCorrection #16 
_rlnCoordinateY #17 
_rlnCoordinateX #18 
_rlnImageName #19 
_rlnLogLikeliContribution #20 
_rlnNrOfSignificantSamples #21 
_rlnOpticsGroup #22 
           1   -38.988060 images/stack_101016-2_0001_2x_SumCorr.mrc    20.246157            2     0.121563    13.390000 20835.980469    94.604259     1.173185     1.000000    -7.797830            1 21562.509766    -3.797830     0.509441  1362.000000  2759.000000 1@sClass12456-merge.mrcs 1.355668e+05            1            1 
           2   128.584219 images/stack_101016-2_0001_2x_SumCorr.mrc    11.450013            1     0.121563    13.390000 20835.980469    99.182877     0.678425     1.000000     9.266188            1 21562.509766     1.825188     0.504943  1420.000000  3387.000000 2@sClass12456-merge.mrcs 1.359507e+05            1            1 
           1    48.829958 images/stack_101016-2_0001_2x_SumCorr.mrc    23.496992            1     0.121563    13.390000 20835.980469    89.054355     0.868732     1.000000     3.861632            1 21562.509766    -7.138368     0.509666  1521.000000   391.000000 3@sClass12456-merge.mrcs 1.359287e+05            1            1 
 
