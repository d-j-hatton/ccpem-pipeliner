#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.jobs.relion.manualpick_job import (
    INPUT_NODE_MICS,
    OUTPUT_NODE_COORDS,
    OUTPUT_NODE_MICS,
    OUTPUT_NODE_COORDS_HELIX,
)


class ManualPickTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_basic(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick.job",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --pipeline_control ManualPick/job004/"
            ],
        )

    def test_get_command_helcial(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_helical.job",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS_HELIX,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --do_startend --pipeline_control ManualPick/job004/"
            ],
        )

    def test_get_command_helcial_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_helical_relionstyle.job",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS_HELIX,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --do_startend --pipeline_control ManualPick/job004/"
            ],
        )

    def test_get_command_jobstar(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_job.star",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --pipeline_control ManualPick/job004/"
            ],
        )

    def test_get_command_redblue(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_color.job",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --color_label rlnParticleSelectZScore --blue 0 --red 2 "
                "--color_star fake_colorfile.star --pipeline_control "
                "ManualPick/job004/"
            ],
        )

    def test_get_command_topaz_denoise(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_topaz_job.star",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 "
                "--topaz_denoise --topaz_exe public/EM/TOPAZ/topaz "
                "--particle_diameter 200 --pipeline_control ManualPick/job004/"
            ],
        )

    def test_get_command_topaz_denoise_with_FOM(self):
        general_get_command_test(
            self,
            "ManualPick",
            "manualpick_topaz_fom_job.star",
            4,
            {"CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS},
            {
                "micrographs_selected.star": OUTPUT_NODE_MICS,
                "manualpick.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job004/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job004/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 "
                "--topaz_denoise --topaz_exe public/EM/TOPAZ/topaz "
                "--particle_diameter 200 --minimum_pick_fom 0.1 --pipeline_control"
                " ManualPick/job004/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_manpick_generate_display_data(self):
        get_relion_tutorial_data(["ManualPick", "CtfFind", "MotionCorr"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ManualPick/job004/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "image_path": "ManualPick/job004/Thumbnails/picked_coords.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 11 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "ManualPick/job004/Movies/20170629_00021_frameImage_manualpick.star",
            ],
        }

        assert dispobjs[1].__dict__ == {
            "title": "11 picked particles",
            "bins": [1],
            "bin_edges": [10.5, 11.5],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["ManualPick/job004/manualpick.star"],
        }


if __name__ == "__main__":
    unittest.main()
