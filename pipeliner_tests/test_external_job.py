#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.relion.external_job import (
    INPUT_NODE_MOVIES,
    INPUT_NODE_MICS,
    INPUT_NODE_PARTS,
    INPUT_NODE_COORDS,
    INPUT_NODE_REF3D,
    INPUT_NODE_MASK,
)
from pipeliner.pipeliner_job import OUTPUT_NODE_RUNOUT


class ExternalJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_external_movies(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_movies.job",
            19,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_parts(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_parts.job",
            19,
            {"Import/job001/particles.star": INPUT_NODE_PARTS},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_parts Import/job001/particles.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_3dref(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_3dref.job",
            19,
            {"Import/job001/3dref.mrc": INPUT_NODE_REF3D},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_coords(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_coords.job",
            19,
            {"Import/job001/coords.star": INPUT_NODE_COORDS},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_coords Import/job001/coords.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_mask(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_mask.job",
            19,
            {"Import/job001/mask.mrc": INPUT_NODE_MASK},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_mask Import/job001/mask.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_allin(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_allin.job",
            19,
            {
                "Import/job001/mics.star": INPUT_NODE_MICS,
                "Import/job001/movies.star": INPUT_NODE_MOVIES,
                "Import/job001/coords.star": INPUT_NODE_COORDS,
                "Import/job001/parts.star": INPUT_NODE_PARTS,
                "Import/job001/3dref.mrc": INPUT_NODE_REF3D,
            },
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_mics Import/job001/mics.star"
                " --in_movies Import/job001/movies.star --in_coords "
                "Import/job001/coords.star --in_parts Import/job001/parts.star"
                " --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_allparams(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_all_params.job",
            19,
            {"Import/job001/3dref.mrc": INPUT_NODE_REF3D},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
                " --test_param2 2002 --test_param3 3003 --test_param4 4004"
                " --test_param5 5005 --test_param6 6006 --test_param7 7007"
                " --test_param8 8008 --test_param9 9009 --test_param10 1010 "
                "--j 16"
            ],
        )

    def test_get_command_external_allparams_jobstar(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_job.star",
            19,
            {"Import/job001/3dref.mrc": INPUT_NODE_REF3D},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
                " --test_param2 2002 --test_param3 3003 --test_param4 4004"
                " --test_param5 5005 --test_param6 6006 --test_param7 7007"
                " --test_param8 8008 --test_param9 9009 --test_param10 1010 --j "
                "16"
            ],
        )

    def test_get_command_external_parts_output(self):
        job = generic_tests.general_get_command_test(
            self,
            "External",
            "external_parts_out.job",
            19,
            {"Import/job001/particles.star": INPUT_NODE_PARTS},
            {"parts_out.star": "ParticlesData.star"},
            [
                "/path/to/external/here.exe --in_parts Import/job001/particles.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/parts_out.star"
        assert outnode.type == "ParticlesData.star"

    def test_get_command_external_mask_output(self):
        job = generic_tests.general_get_command_test(
            self,
            "External",
            "external_mask_out.job",
            19,
            {"Import/job001/mask.mrc": INPUT_NODE_MASK},
            {"mask_out.mrc": INPUT_NODE_MASK},
            [
                "/path/to/external/here.exe --in_mask Import/job001/mask.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/mask_out.mrc"
        assert outnode.type == INPUT_NODE_MASK

    def test_get_command_external_3dref_output(self):
        job = generic_tests.general_get_command_test(
            self,
            "External",
            "external_3dref_out.job",
            19,
            {"Import/job001/3dref.mrc": INPUT_NODE_REF3D},
            {"3dref_out.mrc": INPUT_NODE_REF3D},
            [
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/3dref_out.mrc"
        assert outnode.type == INPUT_NODE_REF3D

    def test_get_command_external_other_output(self):
        job = generic_tests.general_get_command_test(
            self,
            "External",
            "external_otherout.job",
            19,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {"strange_file.xyz": "StrangeNodeType.xyz"},
            [
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/strange_file.xyz"
        assert outnode.type == "StrangeNodeType.xyz"

    def test_get_command_external_other_output_no_type_entered(self):
        job = generic_tests.general_get_command_test(
            self,
            "External",
            "external_noout.job",
            19,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {"strange_file.xyz": "UnknownNodeType.xyz"},
            [
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/strange_file.xyz"
        assert outnode.type == "UnknownNodeType.xyz"

    def test_get_command_external_3dref_nothreads(self):
        generic_tests.general_get_command_test(
            self,
            "External",
            "external_3dref_nothreads.job",
            19,
            {"Import/job001/3dref.mrc": INPUT_NODE_REF3D},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
            ],
        )


if __name__ == "__main__":
    unittest.main()
