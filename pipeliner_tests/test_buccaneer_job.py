#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner.utils import touch
from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner.jobs.buccaneer_job import (
    INPUT_NODE_MAP,
    INPUT_NODE_SEQ,
    OUTPUT_NODE_MODEL,
)
from pipeliner.api.api_utils import write_default_jobstar
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc


class BuccaneerJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_buccaneer_plugin_commands(self):
        os.makedirs("sequences")
        touch("sequences/5ni1.fasta")
        os.makedirs("PostProcess/job010/")
        touch("PostProcess/job010/postprocess_masked.mrc")
        general_get_command_test(
            self,
            "Buccaneer",
            "buccaneer_job.star",
            1502,
            {
                "PostProcess/job010/postprocess_masked.mrc": INPUT_NODE_MAP,
                "sequences/5ni1.fasta": INPUT_NODE_SEQ,
            },
            {"refined1.pdb": OUTPUT_NODE_MODEL},
            [
                "ccpem-buccaneer --no-gui --args tmp_buccaneer_args.json"
                " --job_location Buccaneer/job1502/",
                "rm tmp_buccaneer_args.json",
            ],
        )

        # make sure the json written was as expected:
        with open("tmp_buccaneer_args.json", "r") as json_data:
            actual = json.loads(json_data.read())
        expected = {
            "job_title": "RELION pipeline test with EMD-3488",
            "input_map": os.path.abspath("PostProcess/job010/postprocess_masked.mrc"),
            "resolution": 3.3,
            "input_seq": os.path.abspath("sequences/5ni1.fasta"),
            "extend_pdb": None,
            "ncycle": 1,
            "ncycle_refmac": 20,
            "ncycle_buc1st": 1,
            "ncycle_bucnth": 1,
            "map_sharpen": 0.0,
            "ncpus": 1,
            "lib_in": None,
            "keywords": "",
            "refmac_keywords": "",
        }
        for line in actual:
            assert actual[line] == expected[line], (line, actual[line], expected[line])

    def test_make_results_display(self):
        # make it look like abuccaneer job has run
        os.makedirs("ModelBuild/job001")
        touch("ModelBuild/job001/refined1.pdb")
        write_default_jobstar("buccaneer.modelbuild", "ModelBuild/job001/job.star")
        pipelinefile = os.path.join(self.test_data, "Pipelines/single_buccaneer.star")
        shutil.copy(pipelinefile, "default_pipeline.star")
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")

        # set up the pipeline and get the job
        pipeline = ProjectGraph()
        pipeline.read()
        buc_proc = pipeline.find_process("ModelBuild/job001/")
        buc_job = active_job_from_proc(buc_proc)
        buc_job.joboptions["input_map"].value = "map1.mrc"

        dispobjs = buc_job.create_results_display()
        print(dispobjs[0].__dict__)
        assert dispobjs[0].__dict__ == {
            "maps": ["ModelBuild/job001/Thumbnails/map1.mrc"],
            "maps_opacity": [0.5],
            "models": ["ModelBuild/job001/refined1.pdb"],
            "title": "Overlaid map: map1.mrc model: ModelBuild/job001/refined1.pdb",
            "maps_data": "map1.mrc",
            "models_data": "ModelBuild/job001/refined1.pdb",
            "associated_data": ["map1.mrc", "ModelBuild/job001/refined1.pdb"],
        }
        assert os.path.isfile("ModelBuild/job001/Thumbnails/map1.mrc")


if __name__ == "__main__":
    unittest.main()
