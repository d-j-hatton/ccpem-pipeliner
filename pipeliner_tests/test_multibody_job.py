#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.relion.multibody_job import (
    INPUT_NODE_OPTIMISER_REFINE3D,
    INPUT_NODE_OPTIMISER_MULTIBODY,
    OUTPUT_NODE_LOG,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_HALFMAP,
)


class MultiBodyTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star"),
            self.test_dir,
        )
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ["RELION_SCRATCH_DIR"] = ""

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_multibody_basic(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody.job",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_basic_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted"""
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_relionstyle_job.star",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_flex_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted
        A warning shoulf be raised because the flexibility analysis will not
        be performed"""
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_relionstyle_analysis_job.star",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_jobstar(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_job.star",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_reconstruct_subtracted(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_rec_sub.job",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_no_flexanalysis(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_noflex.job",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_separate_oneigen(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_sep_eigen.job",
            13,
            {"MultiBody/job012/run_model.star": INPUT_NODE_OPTIMISER_MULTIBODY},
            {
                "analyse_eval1_select_min-15_max10.star": OUTPUT_NODE_PARTS,
                "analyse_logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data"
                ".star --bodies bodyfile.star --o MultiBody/job013/analyse --do_maps"
                " --k 3 --select_eigenvalue 1 --select_eigenvalue_min -15.0"
                " --select_eigenvalue_max 10.0 --write_pca_projections "
                "--pipeline_control MultiBody/job013/"
            ],
        )

    def test_get_command_multibody_separate_oneigen_small(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_sep_eigen_small.job",
            13,
            {"MultiBody/job012/run_model.star": INPUT_NODE_OPTIMISER_MULTIBODY},
            {
                "analyse_eval1_select_min-0p25_max0p5.star": OUTPUT_NODE_PARTS,
                "analyse_logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data.star"
                " --bodies bodyfile.star --o MultiBody/job013/analyse --do_maps --k 3"
                " --select_eigenvalue 1 --select_eigenvalue_min -0.25"
                " --select_eigenvalue_max 0.5 --write_pca_projections "
                "--pipeline_control MultiBody/job013/"
            ],
        )

    def test_get_command_multibody_different_sampling(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_diff_samp.job",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 2 --auto_local_healpix_order 2 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_noMPI(self):
        generic_tests.general_get_command_test(
            self,
            "MultiBody",
            "multibody_noMPI.job",
            12,
            {"Refine3D/job400/run_it025_optimiser.star": INPUT_NODE_OPTIMISER_REFINE3D},
            {
                "run_half1_body001_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body002_unfil.mrc": OUTPUT_NODE_HALFMAP,
                "run_half1_body003_unfil.mrc": OUTPUT_NODE_HALFMAP,
            },
            [
                "relion_refine "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 16 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control"
                " MultiBody/job012/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
