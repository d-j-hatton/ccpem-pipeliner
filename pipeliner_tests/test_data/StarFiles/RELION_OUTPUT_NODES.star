
# version 30001 / CCP-EM_pipeliner

data_output_nodes

loop_ 
_rlnPipeLineNodeNameLabel #1 
_rlnPipeLineNodeTypeLabel #2 
MotionCorr/job002/corrected_micrographs.star            MicrographsData.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf           LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star            MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf           LogFile.pdf.relion.ctffind 
SomeOtherJob/job400/extrafile.star		UnknownNodeType.star