# version 30001

data_job

_rlnJobTypeLabel                        relion.joinstar.particles
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  do_queue         No 
  fn_part1   parts_file1.star 
  fn_part2 	 parts_file2.star 
  fn_part3   parts_file3.star      
  fn_part4   parts_file4.star       
min_dedicated          1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 