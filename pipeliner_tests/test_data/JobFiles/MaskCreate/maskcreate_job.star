
# version 30001 / CCP-EM_pipeliner / 202010-devel

data_job

_rlnJobTypeLabel              relion.maskcreate
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo				0

# version 30001 / CCP-EM_pipeliner / 202010-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
     'fn_in' 'Import/job001/emd_3488.mrc' 
'lowpass_filter'           15 
      angpix           -1 
'inimask_threshold'        0.005 
'extend_inimask'            0 
'width_mask_edge'            6 
  'do_helix'           No 
'helical_z_percentage'           30 
'nr_threads'           12 
  'do_queue'           No 
   queuename      openmpi 
        qsub         qsub 
  qsubscript '' 
'min_dedicated'           24 
'other_args'           '' 
 
