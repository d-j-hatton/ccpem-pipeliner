
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    locscale.localsharpen
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
 'input_map'           'Import/job001/1ake_10A.mrc' 
'input_mask'           '' 
'input_model'           'Import/job002/1ake_10A_fitted.pdb' 
  resolution       5.0
  n_nodes           1
 
