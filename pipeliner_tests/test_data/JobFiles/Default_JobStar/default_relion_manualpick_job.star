# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.manualpick
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
 'black_val'            0 
'blue_value'          0.0 
'color_label' rlnParticleSelectZScore 
  'do_color'           No 
'do_fom_threshold'           No 
  'do_queue'           No 
'do_topaz_denoise'           No 
  'fn_color'           '' 
     'fn_in'           '' 
'fn_topaz_exec' public/EM/TOPAZ/topaz 
'min_dedicated'            1 
'minimum_pick_fom'            0 
'other_args'           '' 
 'red_value'          2.0 
'sigma_contrast'            3 
 'white_val'            0 
      angpix           -1 
    diameter          100 
    highpass           -1 
     lowpass           20 
    micscale          0.2 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
 
 