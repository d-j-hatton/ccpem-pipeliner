# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel   relion.autopick.ref2d
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'angpix_ref'           -1 
'do_ctf_autopick'          Yes 
'do_ignore_first_ctfpeak_autopick'           No 
'do_invert_refs'          Yes 
  'do_queue'           No 
'do_read_fom_maps'           No 
'do_write_fom_maps'           No 
'fn_input_autopick'           '' 
'fn_refs_autopick'           '' 
   'gpu_ids'           '' 
'maxstddevnoise_autopick'          1.1 
'min_dedicated'            1 
'minavgnoise_autopick'       -999.0 
'mindist_autopick'          100 
    'nr_mpi'            1 
'other_args'           '' 
'psi_sampling_autopick'            5 
'threshold_autopick'         0.05 
   'use_gpu'           No 
      angpix           -1 
    highpass           -1 
     lowpass           20 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      shrink            0 
