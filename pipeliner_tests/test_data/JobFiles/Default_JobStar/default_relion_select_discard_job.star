# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.select.discard
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'discard_label' rlnImageName 
'discard_sigma'            4 
  'do_queue'           No 
'do_regroup'           No 
   'fn_data'           '' 
    'fn_mic'           '' 
  'fn_model'           '' 
'image_angpix'           -1 
'min_dedicated'            1 
 'nr_groups'            1 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 