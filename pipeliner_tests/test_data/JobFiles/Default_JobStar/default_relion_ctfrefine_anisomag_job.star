# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.ctfrefine.anisomag
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
   'fn_data'           '' 
   'fn_post'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'nr_threads'            1 
'other_args'           '' 
      minres           30 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
 