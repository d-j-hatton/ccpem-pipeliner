# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.localres.resmap
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_resmap_locres'          Yes 
     'fn_in'           '' 
   'fn_mask'           '' 
 'fn_resmap'       None 
'other_args'           '' 
      angpix            1 
      maxres          0.0 
      minres          0.0 
        pval         0.05 
     stepres          1.0 