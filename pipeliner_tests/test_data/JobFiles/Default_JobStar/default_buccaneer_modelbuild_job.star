
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    buccaneer.modelbuild
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'extend_pdb'           '' 
 'input_map'           '' 
 'input_seq'           '' 
 'job_title'           '' 
    'lib_in'           '' 
'map_sharpen'          0.0 
'min_dedicated'            1 
'ncycle_buc1st'           -1 
'ncycle_bucnth'           -1 
'ncycle_refmac'           20 
'other_args'           '' 
'refmac_keywords'           '' 
    keywords           '' 
       ncpus            1 
      ncycle            1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
  resolution            3 
 
