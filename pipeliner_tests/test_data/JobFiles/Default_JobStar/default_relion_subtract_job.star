# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.subtract
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'center_x'            0 
  'center_y'            0 
  'center_z'            0 
'do_center_mask'          Yes 
'do_center_xyz'           No 
   'do_data'           No 
  'do_queue'           No 
   'fn_data'           '' 
   'fn_mask'           '' 
    'fn_opt'           '' 
'min_dedicated'            1 
   'new_box'           -1 
    'nr_mpi'            1 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 