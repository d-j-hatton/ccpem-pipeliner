# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.multibody.flexanalysis
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
 'do_select'           No 
'eigenval_max'        999.0 
'eigenval_min'       -999.0 
 'fn_bodies'           '' 
'input_model'           '' 
'min_dedicated'            1 
 'nr_movies'            3 
'other_args'           '' 
'select_eigenval'            1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 