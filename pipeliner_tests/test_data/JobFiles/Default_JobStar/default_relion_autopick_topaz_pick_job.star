# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel   relion.autopick.topaz.pick
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'do_read_fom_maps'           No 
'do_write_fom_maps'           No 
'fn_input_autopick'           '' 
'fn_topaz_exec' '' 
   'gpu_ids'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'other_args'           '' 
'topaz_model'           '' 
'topaz_nr_particles'           -1 
'topaz_other_args'           '' 
'topaz_particle_diameter'           -1 
   'use_gpu'           No 
      angpix           -1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      shrink            0 
