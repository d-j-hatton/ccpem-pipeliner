
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    threedfsc.map_analysis
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'fn_fullmap'           '' 
'fn_halfmap'           '' 
   'fn_mask'           '' 
'fsc_cutoff'           -1 
    'gpu_id'           '' 
'min_dedicated'            1 
'nr_sphericity_thresholds'           -1 
'other_args'           '' 
'sphericity_threshold'           -1 
   'use_gpu'           No 
        apix          1.0 
      dtheta           -1 
    highpass           -1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
