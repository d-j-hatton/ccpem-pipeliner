
# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.autopick.log.helical
 
_rlnJobIsContinue    0

_rlnJobIsTomo                           0


# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_amyloid'           No 
  'do_queue'           No 
'do_read_fom_maps'           No 
'do_write_fom_maps'           No 
'fn_input_autopick'           '' 
'helical_nr_asu'            1 
'helical_rise'           -1 
'helical_tube_kappa_max'          0.1 
'helical_tube_length_min'           -1 
'helical_tube_outer_diameter'          200 
'log_adjust_thr'            0 
'log_diam_max'          250 
'log_diam_min'          200 
'log_invert'           No 
'log_maxres'           20 
'log_upper_thr'        999.99 
'min_dedicated'            1 
    'nr_mpi'            1 
'other_args'           '' 
      angpix           -1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      shrink            0 
 