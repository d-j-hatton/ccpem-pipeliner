# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.class2d.em.helical
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'allow_coarser'           No 
'ctf_intact_first_peak'           No 
'do_bimodal_psi'          Yes 
 'do_center'          Yes 
'do_combine_thru_disc'           No 
'do_ctf_correction'          Yes 
'fn_cont' ''
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
'do_restrict_xoff'          Yes 
'do_zero_mask'          Yes 
'dont_skip_align'          Yes 
   'fn_cont' '' 
    'fn_img'           '' 
   'gpu_ids'           '' 
'helical_rise'         4.75 
'helical_tube_outer_diameter'          200 
'highres_limit'           -1 
'min_dedicated'            1 
'nr_classes'            1 
'nr_iter_em'           25 
    'nr_mpi'            1 
   'nr_pool'            3 
'nr_threads'            1 
'offset_range'            5 
'offset_step'            1 
'other_args'           '' 
'particle_diameter'          200 
'psi_sampling'          6.0 
 'range_psi'            6 
'scratch_dir'           '' 
 'tau_fudge'            2 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
 