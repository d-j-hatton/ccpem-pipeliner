
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    cryolo.autopick
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'JANNI_model_path'           '' 
  'box_size'          128 
'confidence_threshold'          0.3 
  'do_queue'           No 
'input_file'           '' 
'min_dedicated'            1 
'model_path'           '' 
'other_args'           '' 
'use_JANNI_denoise'           No 
        gpus           '' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
