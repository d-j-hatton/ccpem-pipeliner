
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    icebreaker.micrograph_analysis.particles
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
   'in_mics'           '' 
  'in_parts'           '' 
'min_dedicated'            1 
'nr_threads'            1 
'other_args'           '' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
