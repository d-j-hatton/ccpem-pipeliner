# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel               relion.ctfrefine
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
   'fn_data'           '' 
   'fn_post'           '' 
      minres           30 
    'do_ctf'          Yes 
'do_defocus'           No 
  'do_astig'           No 
'do_bfactor'           No 
  'do_phase'           No 
   'do_tilt'           No 
'do_trefoil'           No 
'do_4thorder'           No 
    'nr_mpi'            1 
'nr_threads'            1 
  'do_queue'           No 
   queuename      openmpi 
        qsub         qsub 
  qsubscript '' 
'min_dedicated'            1 
'other_args'           '' 
 