# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.polish
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_own_params'           No 
  'do_queue'           No 
'extract_size'           -1 
'first_frame'          1
   'fn_data' 'Enter particle data file here' 
    'fn_mic' 'Enter micrographs file here' 
   'fn_post' 'Enter PostProcess star file here' 
'last_frame'         -1
'min_dedicated'            1 
    'nr_mpi'            1 
'nr_threads'            1 
'opt_params'           '' 
'other_args'           '' 
 'sigma_acc'            2 
 'sigma_div'         5000 
 'sigma_vel'          0.2 
      maxres           -1 
      minres           20 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
     rescale           -1 