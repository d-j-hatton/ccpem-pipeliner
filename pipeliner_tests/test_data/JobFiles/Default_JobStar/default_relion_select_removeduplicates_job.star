# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.select.removeduplicates
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'duplicate_threshold'           30 
   'fn_data'           '' 
'image_angpix'           -1 
'min_dedicated'            1 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 