
# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.autopick.ref3d.helical
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'angpix_ref'           -1 
'do_amyloid'           No 
  'do_queue'           No 
'do_read_fom_maps'           No 
'do_write_fom_maps'           No 
'fn_input_autopick'           '' 
'fn_ref3d_autopick'           '' 
   'gpu_ids'           '' 
'helical_nr_asu'            1 
'helical_rise'           -1 
'helical_tube_kappa_max'          0.1 
'helical_tube_length_min'           -1 
'helical_tube_outer_diameter'          200 
'maxstddevnoise_autopick'          1.1 
'min_dedicated'            1 
'minavgnoise_autopick'       -999.0 
'mindist_autopick'          100 
    'nr_mpi'            1 
'other_args'           '' 
'psi_sampling_autopick'            5 
'ref3d_sampling' '30 degrees' 
'ref3d_symmetry'           C1 
'threshold_autopick'         0.05 
   'use_gpu'           No 
      angpix           -1 
    highpass           -1 
     lowpass           20 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      shrink            0 
 