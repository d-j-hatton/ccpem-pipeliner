# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.class3d.helical.multiref
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'allow_coarser'           No 
'ctf_intact_first_peak'           No 
'do_apply_helical_symmetry'          Yes 
'do_combine_thru_disc'           No 
'do_ctf_correction'          Yes 
'do_local_ang_searches'           No 
'do_local_search_helical_symmetry'           No 
   'do_pad1'           No 
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
'do_zero_mask'          Yes 
'dont_skip_align'          Yes 
   'fn_cont' '' 
    'fn_img'           '' 
   'fn_mask'           '' 
    'fn_ref' 'reference/file/number1, reference/file/number2, reference/file/number3' 
   'gpu_ids'           '' 
'helical_nr_asu'            1 
'helical_range_distance'         -1.0 
'helical_rise_inistep'            0 
'helical_rise_initial'            0 
'helical_rise_max'            0 
'helical_rise_min'            0 
'helical_tube_inner_diameter'           -1 
'helical_tube_outer_diameter'           -1 
'helical_twist_inistep'            0 
'helical_twist_initial'            0 
'helical_twist_max'            0 
'helical_twist_min'            0 
'helical_z_percentage'         30.0 
'highres_limit'           -1 
  'ini_high'           60 
'keep_tilt_prior_fixed'          Yes 
'min_dedicated'            1 
   'nr_iter'           25 
   'nr_pool'            3 
'nr_threads'            1 
'offset_range'            5 
'offset_step'            1 
'other_args'           '' 
'particle_diameter'          200 
 'range_psi'           10 
 'range_rot'           -1 
'range_tilt'           15 
'ref_correct_greyscale'           No 
'scratch_dir'       None 
'sigma_angles'          5.0 
'skip_gridding'          Yes 
  'sym_name'           C1 
 'tau_fudge'            4 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
    sampling '7.5 degrees' 