# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.extract.helical
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'bg_diameter'           -1 
'black_dust'           -1 
'coords_suffix'           '' 
'do_cut_into_segments'          Yes 
'do_extract_helical_tubes'          Yes 
'do_float16'          Yes 
'do_fom_threshold'           No 
 'do_invert'          Yes 
   'do_norm'          Yes 
  'do_queue'           No 
'do_rescale'           No 
'extract_size'          128 
'helical_bimodal_angular_priors'          Yes 
'helical_nr_asu'            1 
'helical_rise'            1 
'helical_tube_outer_diameter'          200 
'min_dedicated'            1 
'minimum_pick_fom'            0 
    'nr_mpi'            1 
'other_args'           '' 
 'star_mics'           '' 
'white_dust'           -1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
     rescale          128 