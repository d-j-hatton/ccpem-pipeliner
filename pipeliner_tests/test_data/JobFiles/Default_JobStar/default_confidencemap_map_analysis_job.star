
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    confidencemap.map_analysis
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
 'input_map'           '' 
'min_dedicated'            1 
'noise_box_coordx'         -1.0 
'noise_box_coordy'         -1.0 
'noise_box_coordz'         -1.0 
'other_args'           '' 
 'test_proc'  Right-sided 
'window_size'           -1 
        apix           -1 
        ecdf           No 
   locResMap           '' 
lowPassFilter           -1 
     meanMap           '' 
      method       FDR-BH 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 varianceMap           '' 
 
