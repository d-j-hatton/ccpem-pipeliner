# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.joinstar.micrographs
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
   'fn_mic1'           '' 
   'fn_mic2'           '' 
   'fn_mic3'           '' 
   'fn_mic4'           '' 
'min_dedicated'            1 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 