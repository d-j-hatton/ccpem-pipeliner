
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    relion.reconstruct
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'input_particles'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'other_args'           '' 
      angpix           -1 
      maxres           -1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
         sym           C1 
 
