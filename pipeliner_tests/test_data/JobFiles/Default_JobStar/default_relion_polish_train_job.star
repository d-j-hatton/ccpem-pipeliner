# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.polish.train
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
 'eval_frac'          0.5 
'first_frame'          1
   'fn_data' 'Enter particle data file here' 
    'fn_mic' 'Enter micrographs file here' 
   'fn_post' 'Enter PostProcess star file here' 
'last_frame'         -1
'min_dedicated'            1 
    'nr_mpi'            1 
'nr_threads'            1 
'optim_min_part'        10000 
'other_args'           '' 
 'sigma_acc'            2 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
 