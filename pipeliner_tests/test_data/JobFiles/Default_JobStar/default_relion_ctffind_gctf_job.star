# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.ctffind.gctf
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
   'ctf_win'           -1 
    'do_EPA'           No 
'do_ignore_ctffind_params'          Yes 
'do_phaseshift'           No 
  'do_queue'           No 
'fn_gctf_exe' 'Enter path to GCTF executable' 
   'gpu_ids'           '' 
'input_star_mics'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'other_args'           '' 
'other_gctf_args'           '' 
 'phase_max'          180 
 'phase_min'            0 
'phase_step'           10 
  'use_noDW'           No 
         box          512 
        dast          100 
       dfmax        50000 
       dfmin         5000 
      dfstep          500 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      resmax            5 
      resmin           30 