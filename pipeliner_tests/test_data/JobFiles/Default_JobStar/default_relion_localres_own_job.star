# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.localres.own
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'adhoc_bfac'         -100 
  'do_queue'           No 
     'fn_in'           '' 
   'fn_mask'           '' 
    'fn_mtf'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'nr_threads'            1 
'other_args'           '' 
      angpix            1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 