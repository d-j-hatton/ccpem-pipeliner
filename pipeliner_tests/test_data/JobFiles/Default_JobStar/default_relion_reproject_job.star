
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    relion.reproject
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'angles_filename'           '' 
  'do_queue'           No 
'map_filename'           '' 
'min_dedicated'            1 
'nr_uniform'           -1 
'other_args'           '' 
        apix         1.07 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
