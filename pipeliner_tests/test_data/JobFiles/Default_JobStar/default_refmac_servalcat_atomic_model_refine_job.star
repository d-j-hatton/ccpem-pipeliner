
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    refmac_servalcat.atomic_model_refine
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'auto_symmetry'        Local 
'auto_weight'          Yes 
  'b_factor'          3.0 
  'do_queue'           No 
'half_map_refinement'          Yes 
'input_half_map1'           '' 
'input_half_map2'           '' 
'input_ligand'           '' 
'input_mask'           '' 
'input_model'           '' 
'jelly_body'          Yes 
'jelly_body_dmax'          4.2 
'jelly_body_sigma'         0.02 
'mask_radius'          3.0 
'masked_refinement'          Yes 
'min_dedicated'            1 
   'n_cycle'            8 
'nucleotide_restraints'           No 
'other_args'           '' 
'strict_symmetry'           '' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
  resolution       None 
      weight          1.0 
 
