# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel              relion.maskcreate
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
     'fn_in'           '' 
'lowpass_filter'           10 
      angpix           -1 
'inimask_threshold'         0.02 
'extend_inimask'            3 
'width_mask_edge'            3 
  'do_helix'           No 
'helical_z_percentage'         30.0 
'nr_threads'            1 
  'do_queue'           No 
   queuename      openmpi 
        qsub         qsub 
  qsubscript '' 
'min_dedicated'            1 
'other_args'           '' 
 