
# version 30001

data_job

_rlnJobType                            15
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
adhoc_bfac      -1000 
    angpix      1.244 
autob_lowres         10 
do_adhoc_bfac         No 
do_auto_bfac        Yes 
    do_mtf         No 
  do_queue         No 
do_skip_fsc_weighting         No 
     fn_in Refine3D/job025/run_half1_class001_unfil.mrc 
   fn_mask MaskCreate/job020/mask.mrc 
    fn_mtf         "" 
  low_pass          5 
min_dedicated         24 
mtf_angpix          1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
 