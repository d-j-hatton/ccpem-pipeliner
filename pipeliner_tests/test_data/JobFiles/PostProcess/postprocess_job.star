
# version 30001 / CCP-EM_pipeliner / 202010-devel

data_job

_rlnJobTypeLabel             relion.postprocess
 
_rlnJobIsContinue                       0

_rlnJobIsTomo				0

# version 30001 / CCP-EM_pipeliner / 202010-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
     'fn_in' 'Import/job001/3488_run_half1_class001_unfil.mrc' 
   'fn_mask' 'Import/job002/emd_3488_mask.mrc' 
      angpix        1.244 
'do_auto_bfac'           No 
'autob_lowres'           10 
'do_adhoc_bfac'          Yes 
'adhoc_bfac'        -1000 
    'fn_mtf'           '' 
'mtf_angpix'            1 
'do_skip_fsc_weighting'           No 
  'low_pass'            5 
  'do_queue'           No 
   queuename      openmpi 
        qsub         qsub 
  qsubscript '' 
'min_dedicated'           24 
'other_args'           '' 
 
