# version 30001 / CCP-EM_pipeliner / 202010-devel

data_pipeline_general

_rlnPipeLineJobCounter                       4
 

# version 30001 / CCP-EM_pipeliner / 202010-devel

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/       None       relion.import.movies    Succeeded 
MotionCorr/job002/       None   relion.motioncorr.own    Succeeded 
CtfFind/job003/       None      relion.ctffind.ctffind4    Succeeded 
CtfFind/job004/       None      relion.ctffind.ctffind4    Succeeded 
CtfFind/job005/       None      relion.ctffind.ctffind4    Succeeded 
 

# version 30001 / CCP-EM_pipeliner / 202010-devel

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star MicrographMoviesData.star.relion 
MotionCorr/job002/corrected_micrographs.star MicrographsData.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf LogFile.pdf.relion.ctffind 
CtfFind/job004/micrographs_ctf.star MicrographsData.star.relion.ctf 
CtfFind/job004/logfile.pdf LogFile.pdf.relion.ctffind 
CtfFind/job005/micrographs_ctf.star MicrographsData.star.relion.ctf 
CtfFind/job005/logfile.pdf LogFile.pdf.relion.ctffind  

# version 30001 / CCP-EM_pipeliner / 202010-devel

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job004/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job005/ 


# version 30001 / CCP-EM_pipeliner / 202010-devel

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
CtfFind/job004/ CtfFind/job004/micrographs_ctf.star 
CtfFind/job004/ CtfFind/job004/logfile.pdf 
CtfFind/job005/ CtfFind/job005/micrographs_ctf.star 
CtfFind/job005/ CtfFind/job005/logfile.pdf 
