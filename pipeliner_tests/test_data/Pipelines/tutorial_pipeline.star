
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      30
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/ Import/movies/            relion.import.movies            Succeeded 
MotionCorr/job002/ MotionCorr/own/            relion.motioncorr.own            Succeeded 
CtfFind/job003/ CtfFind/gctf/            relion.ctffind.gctf            Succeeded 
ManualPick/job004/ ManualPick/illustrate_only/            relion.manualpick            Succeeded 
Select/job005/ Select/5mics/            relion.Select.interactive           Succeeded 
AutoPick/job006/ AutoPick/LoG_based/            relion.autopick.log            Succeeded 
Extract/job007/ Extract/LoG_based/            relion.extract            Succeeded 
Class2D/job008/ Class2D/LoG_based/            relion.class2d.em            Succeeded
Select/job009/ Select/templates4autopick/            relion.Select.interactive            Succeeded
AutoPick/job010/ AutoPick/optimise_params/            relion.autopick.ref2d            Succeeded 
AutoPick/job011/ AutoPick/template_based/            relion.autopick.ref2d            Succeeded 
Extract/job012/ Extract/template_based/            relion.extract            Succeeded 
Select/job014/ Select/after_sort/            relion.Select.interactive            Succeeded 
Class2D/job015/ Class2D/after_sorting/            relion.class2d.em            Succeeded 
Select/job016/ Select/class2d_aftersort/            relion.select.onvalue           Succeeded 
InitialModel/job017/ InitialModel/symC1/           relion.initialmodel           Succeeded 
Class3D/job018/ Class3D/first_exhaustive/            relion.class3d            Succeeded 
Select/job019/ Select/class3d_first_exhaustive/            relion.select.interactive            Succeeded 
Extract/job020/ Extract/best3dclass_bigbox/            relion.extract.reextract            Succeeded 
Refine3D/job021/ Refine3D/first3dref/           relion.refine3d            Succeeded 
MaskCreate/job022/ MaskCreate/first3dref/           relion.maskcreate            Succeeded 
PostProcess/job023/ PostProcess/first3dref/           relion.postprocess            Succeeded 
CtfRefine/job024/       None           relion.ctfrefine            Succeeded 
Polish/job025/ Polish/train/           relion.polish.train            Succeeded 
Polish/job026/ Polish/polish/          relion.polish            Succeeded 
Refine3D/job027/ Refine3D/polished/           relion.refine3d            Succeeded 
PostProcess/job028/ PostProcess/polished/           relion.postprocess            Succeeded 
LocalRes/job029/ LocalRes/polished/           relion.localRes.resmap            Succeeded 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star             		MicrographMoviesData.star.relion
MotionCorr/job002/corrected_micrographs.star            MicrographsData.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf           LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star            MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf           LogFile.pdf.relion.ctffind 
ManualPick/job004/coords_suffix_manualpick.star            MicrographsCoords.star.relion.manualpick 
ManualPick/job004/micrographs_selected.star            MicrographsData.star.relion 
Select/job005/micrographs_selected.star            MicrographsData.star.relion
AutoPick/job006/coords_suffix_autopick.star            MicrographsCoords.star.relion.autopick 
AutoPick/job006/logfile.pdf           LogFile.pdf.relion.autopick 
Extract/job007/particles.star            ParticlesData.star.relion 
Class2D/job008/run_it025_data.star            ParticlesData.star.relion.class2d.em
Class2D/job008/run_it025_optimiser.star            ProcessData.star.relion.optimiser.class2d 
Select/job009/particles.star            ParticlesData.star.relion.class2d.em 
Select/job009/class_averages.star            ImagesData.star.relion.classaverages 
AutoPick/job010/coords_suffix_autopick.star            MicroraphsCoords.star.relion.autopick 
AutoPick/job010/logfile.pdf           LogFile.pdf.relion.autopick 
AutoPick/job011/coords_suffix_autopick.star            MicrographsCoords.star.relion.autopick 
AutoPick/job011/logfile.pdf           LogFile.pdf.relion.autopick 
Extract/job012/particles.star            ParticlesData.star.relion 
Select/job014/particles.star            ParticlesData.star.relion 
Class2D/job015/run_it025_data.star            ParticlesData.star.relion.class2d.em 
Class2D/job015/run_it025_optimiser.star            ProcessData.star.relion.optimiser.class2d 
Select/job016/particles.star            ParticlesData.star.relion 
Select/job016/class_averages.star            ImagesData.star.relion.classaverages 
InitialModel/job017/run_it150_data.star            ParticlesData.star.relion.initialmodel 
InitialModel/job017/run_it150_model.star            ProcessData.star.relion.optimiser.initialmodel 
InitialModel/job017/run_it150_class001.mrc            DensityMap.mrc.relion.initialmodel 
InitialModel/job017/run_it150_class001_symD2.mrc      DensityMap.mrc.relion.initialmodel 
Class3D/job018/run_it025_data.star            ParticlesData.star.relion.cless3d 
Class3D/job018/run_it025_optimiser.star            ProcessData.star.relion.optimiser.class3d 
Class3D/job018/run_it025_class001.mrc            DensityMap.mrc.relion.class3d
Class3D/job018/run_it025_class002.mrc            DensityMap.mrc.relion.class3d 
Class3D/job018/run_it025_class003.mrc            DensityMap.mrc.relion.class3d
Class3D/job018/run_it025_class004.mrc            DensityMap.mrc.relion.class3d
Select/job019/particles.star            ParticlesData.star.relion 
Class3D/job018/run_it025_class001_box256.mrc            DensityMap.mrc.relion.class3d 
Extract/job020/particles.star            ParticlesData.star.relion 
Extract/job020/coords_suffix_extract.star            MicrographsCoords.star.relion 
Refine3D/job021/run_data.star            ParticlesData.star.relion.refine3d 
Refine3D/job021/run_half1_class001_unfil.mrc           DensityMap.mrc.relion.halfmap.refine3d 
Refine3D/job021/run_class001.mrc            DensityMap.mrc.relion.refine3d 
MaskCreate/job022/mask.mrc            Mask3D.mrc.relion
PostProcess/job023/postprocess.mrc           DensityMap.mrc.relion.postprocessed 
PostProcess/job023/postprocess_masked.mrc           DensityMap.mrc.relion.postprocess.masked 
PostProcess/job023/logfile.pdf           LogFile.pdf.relion.postprocess 
PostProcess/job023/postprocess.star           ProcessData.pdf.relion.postprocess 
CtfRefine/job024/logfile.pdf           LogFile.pdf.relion.ctfrefine 
CtfRefine/job024/particles_ctf_refine.star            ParticlesData.star.relion.ctfrefine 
Polish/job025/opt_params.txt           ProcessData.txt.relion.polishparams 
Polish/job026/logfile.pdf           LogFile.pdf.relion.polish 
Polish/job026/shiny.star            ParticlesData.star.relion.polish
Refine3D/job027/run_data.star            ParticlesData.star.relion.refine3d 
Refine3D/job027/run_half1_class001_unfil.mrc           DensityMap.mrc.relion.halfmap.refine3d 
Refine3D/job027/run_class001.mrc            DensityMap.mrc.relion.refine3d 
PostProcess/job028/postprocess.mrc           DensityMap.mrc.relion.postprocessed 
PostProcess/job028/postprocess_masked.mrc           DensityMap.mrc.relion.postprocessed.masked
PostProcess/job028/logfile.pdf           LogFile.pdf.relion.postprocess
PostProcess/job028/postprocess.star           ProcessData.star.relion.postprocess
LocalRes/job029/relion_locres_filtered.mrc           DensityMap.mrc.relion.localresfilt
LocalRes/job029/relion_locres.mrc           Image3D.mrc.relion.localresmap 
LocalRes/job029/flowchart.pdf           LogFile.pdf.relion.localres 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
ManualPick/job004/coords_suffix_manualpick.star Select/job005/ 
Select/job005/micrographs_selected.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/coords_suffix_autopick.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 
Class2D/job008/run_it025_optimiser.star Select/job009/ 
Select/job005/micrographs_selected.star AutoPick/job010/ 
Select/job009/class_averages.star AutoPick/job010/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job011/ 
Select/job009/class_averages.star AutoPick/job011/ 
CtfFind/job003/micrographs_ctf.star Extract/job012/ 
AutoPick/job011/coords_suffix_autopick.star Extract/job012/ 
Extract/job012/particles.star Select/job014/
Select/job014/particles.star Class2D/job015/ 
Class2D/job015/run_it025_optimiser.star Select/job016/ 
Select/job016/particles.star InitialModel/job017/ 
Select/job016/particles.star Class3D/job018/ 
InitialModel/job017/run_it150_class001_symD2.mrc Class3D/job018/ 
Class3D/job018/run_it025_optimiser.star Select/job019/ 
CtfFind/job003/micrographs_ctf.star Extract/job020/ 
Select/job019/particles.star Extract/job020/ 
Extract/job020/particles.star Refine3D/job021/ 
Class3D/job018/run_it025_class001_box256.mrc Refine3D/job021/ 
Refine3D/job021/run_class001.mrc MaskCreate/job022/ 
MaskCreate/job022/mask.mrc PostProcess/job023/ 
Refine3D/job021/run_half1_class001_unfil.mrc PostProcess/job023/ 
Refine3D/job021/run_data.star CtfRefine/job024/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job025/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job026/ 
Polish/job026/shiny.star Refine3D/job027/ 
Refine3D/job021/run_class001.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc LocalRes/job029/ 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/coords_suffix_manualpick.star 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
Select/job005/ Select/job005/micrographs_selected.star 
AutoPick/job006/ AutoPick/job006/coords_suffix_autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
Class2D/job008/ Class2D/job008/run_it025_data.star 
Class2D/job008/ Class2D/job008/run_it025_optimiser.star 
Select/job009/ Select/job009/particles.star 
Select/job009/ Select/job009/class_averages.star 
AutoPick/job010/ AutoPick/job010/coords_suffix_autopick.star 
AutoPick/job010/ AutoPick/job010/logfile.pdf 
AutoPick/job011/ AutoPick/job011/coords_suffix_autopick.star 
AutoPick/job011/ AutoPick/job011/logfile.pdf 
Extract/job012/ Extract/job012/particles.star 
Select/job014/ Select/job014/particles.star 
Class2D/job015/ Class2D/job015/run_it025_data.star 
Class2D/job015/ Class2D/job015/run_it025_optimiser.star 
Select/job016/ Select/job016/particles.star 
Select/job016/ Select/job016/class_averages.star 
InitialModel/job017/ InitialModel/job017/run_it150_data.star 
InitialModel/job017/ InitialModel/job017/run_it150_model.star 
InitialModel/job017/ InitialModel/job017/run_it150_class001.mrc 
InitialModel/job017/ InitialModel/job017/run_it150_class001_symD2.mrc 
Class3D/job018/ Class3D/job018/run_it025_data.star 
Class3D/job018/ Class3D/job018/run_it025_optimiser.star 
Class3D/job018/ Class3D/job018/run_it025_class001.mrc 
Class3D/job018/ Class3D/job018/run_it025_class002.mrc 
Class3D/job018/ Class3D/job018/run_it025_class003.mrc 
Class3D/job018/ Class3D/job018/run_it025_class004.mrc 
Class3D/job018/ Class3D/job018/run_it025_class001_box256.mrc 
Select/job019/ Select/job019/particles.star 
Extract/job020/ Extract/job020/particles.star 
Extract/job020/ Extract/job020/coords_suffix_extract.star 
Refine3D/job021/ Refine3D/job021/run_data.star 
Refine3D/job021/ Refine3D/job021/run_half1_class001_unfil.mrc 
Refine3D/job021/ Refine3D/job021/run_class001.mrc 
MaskCreate/job022/ MaskCreate/job022/mask.mrc 
PostProcess/job023/ PostProcess/job023/postprocess.mrc 
PostProcess/job023/ PostProcess/job023/postprocess_masked.mrc 
PostProcess/job023/ PostProcess/job023/logfile.pdf 
PostProcess/job023/ PostProcess/job023/postprocess.star 
CtfRefine/job024/ CtfRefine/job024/logfile.pdf 
CtfRefine/job024/ CtfRefine/job024/particles_ctf_refine.star 
Polish/job025/ Polish/job025/opt_params.txt 
Polish/job026/ Polish/job026/logfile.pdf 
Polish/job026/ Polish/job026/shiny.star 
Refine3D/job027/ Refine3D/job027/run_data.star 
Refine3D/job027/ Refine3D/job027/run_half1_class001_unfil.mrc 
Refine3D/job027/ Refine3D/job027/run_class001.mrc 
PostProcess/job028/ PostProcess/job028/postprocess.mrc 
PostProcess/job028/ PostProcess/job028/postprocess_masked.mrc 
PostProcess/job028/ PostProcess/job028/logfile.pdf 
PostProcess/job028/ PostProcess/job028/postprocess.star 
LocalRes/job029/ LocalRes/job029/relion_locres_filtered.mrc 
LocalRes/job029/ LocalRes/job029/relion_locres.mrc 
LocalRes/job029/ LocalRes/job029/flowchart.pdf 
 
