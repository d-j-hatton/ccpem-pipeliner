
# version 30001

data_pipeline_general

_rlnPipeLineJobCounter                      32
 

# version 30001

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/       None relion.import  Succeeded 
MotionCorr/job002/       None relion.motioncorr  Succeeded 
CtfFind/job003/       None relion.ctffind  Succeeded 
ManualPick/job004/       None relion.manualpick  Succeeded 
Select/job005/       None relion.select  Succeeded 
AutoPick/job006/       None relion.autopick  Succeeded 
Extract/job007/       None relion.extract  Succeeded 
Class2D/job008/       None relion.class2d  Succeeded 
Select/job009/       None relion.select  Succeeded 
AutoPick/job010/       None relion.autopick  Succeeded 
AutoPick/job011/       None relion.autopick  Succeeded 
Extract/job012/       None relion.extract  Succeeded 
Class2D/job013/       None relion.class2d  Succeeded 
Select/job014/       None relion.select  Succeeded 
InitialModel/job015/       None relion.initialmodel  Succeeded 
Class3D/job016/       None relion.class3d  Succeeded 
Select/job017/       None relion.select  Succeeded 
Extract/job018/       None relion.extract  Succeeded 
Refine3D/job019/       None relion.refine3d  Succeeded 
MaskCreate/job020/       None relion.maskcreate  Succeeded 
PostProcess/job021/       None relion.postprocess  Succeeded 
CtfRefine/job022/       None relion.ctfrefine  Succeeded 
CtfRefine/job023/       None relion.ctfrefine  Succeeded 
CtfRefine/job024/       None relion.ctfrefine  Succeeded 
Refine3D/job025/       None relion.refine3d  Succeeded 
PostProcess/job026/       None relion.postprocess  Succeeded 
Polish/job027/       None relion.polish  Succeeded 
Polish/job028/       None relion.polish  Succeeded 
Refine3D/job029/       None relion.refine3d  Succeeded 
PostProcess/job030/       None relion.postprocess  Succeeded 
LocalRes/job031/       None relion.localres  Succeeded 
 

# version 30001

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star MicrographMoviesData.star.relion 
MotionCorr/job002/corrected_micrographs.star MicrographsData.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf LogFile.pdf.relion.ctffind 
ManualPick/job004/micrographs_selected.star MicrographsData.star.relion 
ManualPick/job004/manualpick.star MicrographsCoords.star.relion.manualpick 
Select/job005/micrographs_split1.star MicrographsData.star.relion 
Select/job005/micrographs_split2.star MicrographsData.star.relion 
Select/job005/micrographs_split3.star MicrographsData.star.relion 
AutoPick/job006/autopick.star MicrographsCoords.star.relion.autopick 
AutoPick/job006/logfile.pdf LogFile.pdf.relion.autopick 
Extract/job007/particles.star ParticlesData.star.relion 
Class2D/job008/run_it025_data.star ParticlesData.star.relion.class2d 
Class2D/job008/run_it025_optimiser.star ProcessData.star.relion.optimiser.class2d 
Select/job009/particles.star ParticlesData.star.relion 
Select/job009/class_averages.star ImagesData.star.relion.classaverages 
Select/job009/rank_optimiser.star ProcessData.star.relion.optimiser.autoselect 
AutoPick/job010/input_training_coords.star MicrographsCoords.star.relion 
AutoPick/job011/autopick.star MicrographsCoords.star.relion.autopick 
AutoPick/job011/logfile.pdf LogFile.pdf.relion.autopick 
Extract/job012/particles.star ParticlesData.star.relion 
Class2D/job013/run_it100_data.star ParticlesData.star.relion.class2d 
Class2D/job013/run_it100_optimiser.star ProcessData.star.relion.optimiser.class2d 
Select/job014/particles.star ParticlesData.star.relion 
Select/job014/class_averages.star ImagesData.star.relion.classaverages 
Select/job014/rank_optimiser.star ProcessData.star.relion.optimiser.autoselect 
InitialModel/job015/initial_model.mrc DensityMap.mrc.relion.initialmodel 
Class3D/job016/run_it025_data.star ParticlesData.star.relion.class3d 
Class3D/job016/run_it025_optimiser.star ProcessData.star.relion.optimiser.class3d 
Class3D/job016/run_it025_class001.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class002.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class003.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class004.mrc DensityMap.mrc.relion.class3d 
Select/job017/particles.star ParticlesData.star.relion 
Extract/job018/extractpick.star MicrographsCoords.star.relion.reextract 
Extract/job018/particles.star ParticlesData.star.relion 
Extract/job018/reextract.star MicrographsCoords.star.relion.reextract 
Class3D/job016/run_it025_class004_box256.mrc DensityMap.mrc 
Refine3D/job019/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job019/run_optimiser.star ProcessData.star.relion.optimiser.refine3d 
Refine3D/job019/run_half1_class001_unfil.mrc DensityMap.mrc.relion.halfmap.refine3d 
Refine3D/job019/run_class001.mrc DensityMap.mrc.relion.refine3d 
MaskCreate/job020/mask.mrc Mask3D.mrc.relion 
PostProcess/job021/postprocess.mrc DensityMap.mrc.relion.postprocess 
PostProcess/job021/postprocess_masked.mrc DensityMap.mrc.relion.postprocess.masked 
PostProcess/job021/logfile.pdf LogFile.pdf.relion.postprocess 
PostProcess/job021/postprocess.star ProcessData.star.relion.postprocess 
CtfRefine/job022/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job022/particles_ctf_refine.star ParticlesData.star.relion.ctfrefine 
CtfRefine/job023/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job023/particles_ctf_refine.star ParticlesData.star.relion.anisomagrefine 
CtfRefine/job024/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job024/particles_ctf_refine.star ParticlesData.star.relion.ctfrefine 
Refine3D/job025/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job025/run_optimiser.star ProcessData.star.relion.optimiser.refine3d 
Refine3D/job025/run_half1_class001_unfil.mrc DensityMap.mrc.relion.halfmap.refine3d 
Refine3D/job025/run_class001.mrc DensityMap.mrc.relion.refine3d 
PostProcess/job026/postprocess.mrc DensityMap.mrc.relion.postprocess 
PostProcess/job026/postprocess_masked.mrc DensityMap.mrc.relion.postprocess.masked 
PostProcess/job026/logfile.pdf LogFile.pdf.relion.postprocess 
PostProcess/job026/postprocess.star ProcessData.star.relion.postprocess 
Polish/job027/opt_params_all_groups.txt ProcessData.txt.relion.polish.params 
Polish/job028/logfile.pdf LogFile.pdf.relion.polish 
Polish/job028/shiny.star ParticlesData.star.relion.polished 
Refine3D/job029/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job029/run_optimiser.star ProcessData.star.relion.optimiser.refine3d 
Refine3D/job029/run_half1_class001_unfil.mrc DensityMap.mrc.relion.halfmap.refine3d 
Refine3D/job029/run_class001.mrc DensityMap.mrc.relion.refine3d 
PostProcess/job030/postprocess.mrc DensityMap.mrc.relion.postprocess 
PostProcess/job030/postprocess_masked.mrc DensityMap.mrc.relion.postprocess.masked 
PostProcess/job030/logfile.pdf LogFile.pdf.relion.postprocess 
PostProcess/job030/postprocess.star ProcessData.star.relion.postprocess 
LocalRes/job031/histogram.pdf LogFile.pdf.relion.localres 
LocalRes/job031/relion_locres_filtered.mrc DensityMap.mrc.relion.localresfiltered 
LocalRes/job031/relion_locres.mrc Image3D.mrc.resmap.localresmap 
 

# version 30001

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
CtfFind/job003/micrographs_ctf.star Select/job005/ 
Select/job005/micrographs_split1.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/autopick.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 
Class2D/job008/run_it025_optimiser.star Select/job009/ 
Select/job005/micrographs_split1.star AutoPick/job010/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job011/ 
CtfFind/job003/micrographs_ctf.star Extract/job012/ 
AutoPick/job011/autopick.star Extract/job012/ 
Extract/job012/particles.star Class2D/job013/ 
Class2D/job013/run_it100_optimiser.star Select/job014/ 
Select/job014/particles.star InitialModel/job015/ 
Select/job014/particles.star Class3D/job016/ 
InitialModel/job015/initial_model.mrc Class3D/job016/ 
Class3D/job016/run_it025_optimiser.star Select/job017/ 
CtfFind/job003/micrographs_ctf.star Extract/job018/ 
Select/job017/particles.star Extract/job018/ 
Extract/job018/particles.star Refine3D/job019/ 
Class3D/job016/run_it025_class004_box256.mrc Refine3D/job019/ 
Refine3D/job019/run_class001.mrc MaskCreate/job020/ 
MaskCreate/job020/mask.mrc PostProcess/job021/ 
Refine3D/job019/run_half1_class001_unfil.mrc PostProcess/job021/ 
Refine3D/job019/run_data.star CtfRefine/job022/ 
CtfRefine/job022/particles_ctf_refine.star CtfRefine/job023/ 
CtfRefine/job023/particles_ctf_refine.star CtfRefine/job024/ 
CtfRefine/job024/particles_ctf_refine.star Refine3D/job025/ 
Refine3D/job019/run_class001.mrc Refine3D/job025/ 
MaskCreate/job020/mask.mrc PostProcess/job026/ 
Refine3D/job025/run_half1_class001_unfil.mrc PostProcess/job026/ 
Refine3D/job025/run_data.star Polish/job027/ 
Refine3D/job025/run_data.star Polish/job028/ 
Polish/job028/shiny.star Refine3D/job029/ 
Refine3D/job025/run_half1_class001_unfil.mrc Refine3D/job029/ 
MaskCreate/job020/mask.mrc Refine3D/job029/ 
MaskCreate/job020/mask.mrc PostProcess/job030/ 
Refine3D/job029/run_half1_class001_unfil.mrc PostProcess/job030/ 
Refine3D/job029/run_half1_class001_unfil.mrc LocalRes/job031/ 
 

# version 30001

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
ManualPick/job004/ ManualPick/job004/manualpick.star 
Select/job005/ Select/job005/micrographs_split1.star 
Select/job005/ Select/job005/micrographs_split2.star 
Select/job005/ Select/job005/micrographs_split3.star 
AutoPick/job006/ AutoPick/job006/autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
Class2D/job008/ Class2D/job008/run_it025_data.star 
Class2D/job008/ Class2D/job008/run_it025_optimiser.star 
Select/job009/ Select/job009/particles.star 
Select/job009/ Select/job009/class_averages.star 
Select/job009/ Select/job009/rank_optimiser.star 
AutoPick/job010/ AutoPick/job010/input_training_coords.star 
AutoPick/job011/ AutoPick/job011/autopick.star 
AutoPick/job011/ AutoPick/job011/logfile.pdf 
Extract/job012/ Extract/job012/particles.star 
Class2D/job013/ Class2D/job013/run_it100_data.star 
Class2D/job013/ Class2D/job013/run_it100_optimiser.star 
Select/job014/ Select/job014/particles.star 
Select/job014/ Select/job014/class_averages.star 
Select/job014/ Select/job014/rank_optimiser.star 
InitialModel/job015/ InitialModel/job015/initial_model.mrc 
Class3D/job016/ Class3D/job016/run_it025_data.star 
Class3D/job016/ Class3D/job016/run_it025_optimiser.star 
Class3D/job016/ Class3D/job016/run_it025_class001.mrc 
Class3D/job016/ Class3D/job016/run_it025_class002.mrc 
Class3D/job016/ Class3D/job016/run_it025_class003.mrc 
Class3D/job016/ Class3D/job016/run_it025_class004.mrc 
Class3D/job016/ Class3D/job016/run_it025_class004_box256.mrc 
Select/job017/ Select/job017/particles.star 
Extract/job018/ Extract/job018/extractpick.star 
Extract/job018/ Extract/job018/particles.star 
Extract/job018/ Extract/job018/reextract.star 
Refine3D/job019/ Refine3D/job019/run_data.star 
Refine3D/job019/ Refine3D/job019/run_optimiser.star 
Refine3D/job019/ Refine3D/job019/run_half1_class001_unfil.mrc 
Refine3D/job019/ Refine3D/job019/run_class001.mrc 
MaskCreate/job020/ MaskCreate/job020/mask.mrc 
PostProcess/job021/ PostProcess/job021/postprocess.mrc 
PostProcess/job021/ PostProcess/job021/postprocess_masked.mrc 
PostProcess/job021/ PostProcess/job021/logfile.pdf 
PostProcess/job021/ PostProcess/job021/postprocess.star 
CtfRefine/job022/ CtfRefine/job022/logfile.pdf 
CtfRefine/job022/ CtfRefine/job022/particles_ctf_refine.star 
CtfRefine/job023/ CtfRefine/job023/logfile.pdf 
CtfRefine/job023/ CtfRefine/job023/particles_ctf_refine.star 
CtfRefine/job024/ CtfRefine/job024/logfile.pdf 
CtfRefine/job024/ CtfRefine/job024/particles_ctf_refine.star 
Refine3D/job025/ Refine3D/job025/run_data.star 
Refine3D/job025/ Refine3D/job025/run_optimiser.star 
Refine3D/job025/ Refine3D/job025/run_half1_class001_unfil.mrc 
Refine3D/job025/ Refine3D/job025/run_class001.mrc 
PostProcess/job026/ PostProcess/job026/postprocess.mrc 
PostProcess/job026/ PostProcess/job026/postprocess_masked.mrc 
PostProcess/job026/ PostProcess/job026/logfile.pdf 
PostProcess/job026/ PostProcess/job026/postprocess.star 
Polish/job027/ Polish/job027/opt_params_all_groups.txt 
Polish/job028/ Polish/job028/logfile.pdf 
Polish/job028/ Polish/job028/shiny.star 
Refine3D/job029/ Refine3D/job029/run_data.star 
Refine3D/job029/ Refine3D/job029/run_optimiser.star 
Refine3D/job029/ Refine3D/job029/run_half1_class001_unfil.mrc 
Refine3D/job029/ Refine3D/job029/run_class001.mrc 
PostProcess/job030/ PostProcess/job030/postprocess.mrc 
PostProcess/job030/ PostProcess/job030/postprocess_masked.mrc 
PostProcess/job030/ PostProcess/job030/logfile.pdf 
PostProcess/job030/ PostProcess/job030/postprocess.star 
LocalRes/job031/ LocalRes/job031/histogram.pdf 
LocalRes/job031/ LocalRes/job031/relion_locres_filtered.mrc 
LocalRes/job031/ LocalRes/job031/relion_locres.mrc 
 
