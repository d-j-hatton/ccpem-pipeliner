#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("slicendice") is None else False
plugin_present = generic_tests.check_for_plugin("slicendice_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class SliceNDiceTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="slicendice")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_slice_only(self):
        proc = job_running_test(
            os.path.join(self.test_data, "JobFiles/SliceNDice/slicendice.job"),
            [
                (
                    "Import/job001",
                    os.path.join(
                        self.test_data, "open_fold_7U5C_5_model_1_relaxed_2.pdb"
                    ),
                ),
            ],
            [
                "run.out",
                "run.err",
                "slicendice_0/input.pdb",
                "slicendice_0/slicendice.log",
                (
                    "slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                ),
                (
                    "slicendice_0/split_3/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_2.pdb"
                ),
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dic = {
            "maps": [],
            "maps_opacity": [],
            "models": [
                (
                    "SliceNDice/job998/slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                )
            ],
            "title": "SliceNDice Split 1",
            "models_data": (
                "SliceNDice/job998/slicendice_0/split_1/"
                "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
            ),
            "maps_data": "",
            "associated_data": [
                (
                    "SliceNDice/job998/slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                )
            ],
        }
        for key in test_dic.keys():
            assert test_dic[key] == dispobjs[-1].__dict__[key]

    def test_slice_and_dice(self):
        """
        Add when dice functionality is available in CCP4 distribution
        """
        pass


if __name__ == "__main__":
    unittest.main()
