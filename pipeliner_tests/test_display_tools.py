#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import unittest
import tempfile
import shutil
import json

from pipeliner.utils import touch
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    get_relion_tutorial_data,
    tutorial_data_available,
    expected_warning,
)
from pipeliner.display_tools import (
    get_ordered_classes_arrays,
    ResultsDisplayMontage,
    ResultsDisplayImage,
    ResultsDisplayHistogram,
    ResultsDisplayTable,
    ResultsDisplayMapModel,
    graph_from_starfile_cols,
    histogram_from_starfile_col,
    mrc_thumbnail,
    tiff_thumbnail,
    mini_montage_from_many_files,
    mini_montage_from_starfile,
    make_map_model_thumb_and_display,
    make_particle_coords_thumb,
    mini_montage_from_mrcs_stack,
)


class DisplayToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_image_resultsdisplay_obj(self):
        rdo = ResultsDisplayImage(
            title="myimage",
            image_desc="Test description",
            image_path="test.png",
            associated_data=["data_file"],
        )
        assert rdo.title == "myimage"
        assert rdo.image_desc == "Test description"
        assert rdo.image_path == "test.png"
        assert rdo.associated_data == ["data_file"]

    def test_map_model_maps_models_opacities(self):
        mmo = ResultsDisplayMapModel(
            title="test mm display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            maps_opacity=[0.175, 0.2],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "maps_opacity": [0.175, 0.2],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        }

        assert mmo.__dict__ == expected

    def test_map_model_maps_models(self):
        mmo = ResultsDisplayMapModel(
            title="test mm display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "maps_opacity": [0.5, 0.5],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        }

        assert mmo.__dict__ == expected

    def test_map_model_maps_only(self):
        mmo = ResultsDisplayMapModel(
            title="test mm display",
            maps=["map1.mrc", "map2.mrc"],
            maps_opacity=[0.175, 0.2],
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "maps_opacity": [0.175, 0.2],
            "models": [],
            "title": "test mm display",
            "maps_data": "here are the maps",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        }

        assert mmo.__dict__ == expected

    def test_map_model_one_map_opacity_is_1(self):
        mmo = ResultsDisplayMapModel(
            title="test mm display",
            maps=["map1.mrc"],
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        assert mmo.__dict__ == {
            "maps": ["map1.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "test mm display",
            "maps_data": "here are the maps",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        }

    def test_map_model_models_only(self):
        mmo = ResultsDisplayMapModel(
            title="test mm display",
            models=["model1.pdb", "model2.pdb"],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": [],
            "maps_opacity": [],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        }

        assert mmo.__dict__ == expected

    def test_map_model_no_maps_or_models(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMapModel(
                title="test mm display",
                models_data="here are the models",
                maps_data="here are the maps",
                associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            )

    def test_map_model_no_maps_opacities_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMapModel(
                title="test mm display",
                maps=["map1.mrc", "map2.mrc"],
                maps_opacity=[1],
                models_data="here are the models",
                maps_data="here are the maps",
                associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            )

    def test_create_map_mode_thumbs(self):
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        touch("model1.pdb")
        touch("model2.pdb")
        dispobj = make_map_model_thumb_and_display(
            title="Test map model display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            assoc_data=["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
            outputdir="",
        )
        assert os.path.isfile("Thumbnails/map1.mrc")
        assert os.path.isfile("Thumbnails/map2.mrc")
        assert dispobj.__dict__ == {
            "maps": ["Thumbnails/map1.mrc", "Thumbnails/map2.mrc"],
            "maps_opacity": [0.5, 0.5],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "Test map model display",
            "maps_data": "map1.mrc, map2.mrc",
            "models_data": "model1.pdb, model2.pdb",
            "associated_data": ["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
        }

    def test_histo(self):
        rdo = ResultsDisplayHistogram(
            title="mytitle",
            data_to_bin=[1] * 10 + [2] * 15 + [3] * 20 + [4] * 2 + [5] * 100,
            xlabel="number",
            ylabel="count",
            associated_data=["mydata.star"],
        )

        assert rdo.title == "mytitle"
        assert list(rdo.bins) == [10, 15, 0, 20, 2, 100]
        expround_bins = ["1.0", "1.7", "2.3", "3.0", "3.7", "4.3", "5.0"]
        assert [f"{x:.01f}" for x in list(rdo.bin_edges)] == expround_bins
        assert rdo.xlabel == "number"
        assert rdo.ylabel == "count"
        assert rdo.associated_data == ["mydata.star"]

    def test_histo_write_and_read(self):
        rdo = ResultsDisplayHistogram(
            title="mytitle",
            data_to_bin=[1] * 10 + [2] * 15 + [3] * 20 + [4] * 2 + [5] * 100,
            xlabel="number",
            ylabel="count",
            associated_data=["mydata.star"],
        )

        rdo.write_displayobj_file("", 1)
        with open(".results_display_histogram_001.json", "r") as of:
            wrote = json.load(of)
        rdo2 = ResultsDisplayHistogram(**wrote)
        assert rdo.__dict__ == rdo2.__dict__

    def test_montage_cant_instantiate_if_counts_dont_match_no_labels(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMontage(
                associated_data=["fake_file.txt"],
                title="Hereistitle",
                xvalues=[1, 2, 3, 4, 5, 6],
                yvalues=[1, 2, 3, 4],
                img="myimg.png",
            )

    def test_montage_cant_instantiate_if_counts_dont_match_with_labels(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMontage(
                associated_data=["fake_file.txt"],
                title="Hereistitle",
                xvalues=[1, 2, 3, 4, 5, 6],
                yvalues=[1, 2, 3, 4, 5],
                img="my_image.png",
            )

    def test_montage_write_dispobj_json(self):
        ddata = ResultsDisplayMontage(
            associated_data=["fake_file.txt"],
            title="Hereistitle",
            xvalues=[1, 2, 3, 4, 5, 6],
            yvalues=[1, 2, 3, 4, 5, 6],
            labels=[1, 2, 3, 4, 5, 6],
            img="test.png",
        )
        ddata.write_displayobj_file("", 1)
        with open(".results_display_montage_001.json", "r") as f:
            ddict = json.load(f)
        exp_dict = {
            "title": "Hereistitle",
            "xvalues": [1, 2, 3, 4, 5, 6],
            "yvalues": [1, 2, 3, 4, 5, 6],
            "labels": [1, 2, 3, 4, 5, 6],
            "associated_data": ["fake_file.txt"],
            "img": "test.png",
        }
        assert ddict == exp_dict

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_prepare_class2d_montage_no_parts_counting(self):
        get_relion_tutorial_data("Class2D")
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(mf, 10, 50, "mfile.png")
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 10
        expy = sum([[n] * 10 for n in reversed(range(10))], [])
        explabels = ["Class 61; 2.61%", "Class 59; 0.00%"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-1] == explabels[1]
        assert len(ddata.labels) == 100
        assert ddata.img == "mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_prepare_class2d_montage_no_parts_counting_uneven_rows(self):
        get_relion_tutorial_data("Class2D")
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(mf, 11, 50, "mfile.png")
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 10
        expy = sum([[n] * 11 for n in reversed(range(10))], [])
        explabels = ["Class 61; 2.61%", "Class 59; 0.00%"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [None] * 10
        assert ddata.img == "mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_prepare_class2d_montage_parts_counting_uneven_rows(self):
        get_relion_tutorial_data("Class2D")
        mf = "Class2D/job013/run_it100_model.star"
        dat = "Class2D/job013/run_it100_data.star"
        ddata = get_ordered_classes_arrays(mf, 11, 50, "mfile.png", dat)
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 10
        expy = sum([[n] * 11 for n in reversed(range(10))], [])
        explabels = ["Class 61; 2.61%; 239 particles", "Class 59; 0.00%; 0 particles"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [None] * 10
        assert ddata.img == "mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf, dat]
        assert ddata.title == "2D class averages"

    def test_graph_from_starfile_single_data_series_no_x_or_labels(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = graph_from_starfile_cols(
            title="test graph",
            starfile="data.star",
            block="particles",
            ycols=["_rlnDefocusU"],
            data_series_labels=["Defocus"],
        )
        dataobj.title == "test graph",
        assert len(dataobj.xvalues) == 1
        assert len(dataobj.xvalues[0]) == 2377
        assert dataobj.xaxis_label == "Count"
        assert len(dataobj.yvalues) == 1
        assert len(dataobj.yvalues[0]) == 2377
        assert dataobj.yaxis_label == "_rlnDefocusU"
        assert dataobj.data_series_labels == ["Defocus"]
        assert dataobj.associated_data == ["data.star"]

    def test_graph_from_starfile_multiple_data_sets(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = graph_from_starfile_cols(
            title="test graph",
            starfile="data.star",
            block="particles",
            ycols=["_rlnDefocusU", "_rlnDefocusV"],
            xcols=["_rlnDefocusV", "_rlnDefocusU"],
            data_series_labels=["Series1", "Series2"],
        )

        assert dataobj.title == "test graph"
        assert len(dataobj.xvalues) == 2
        assert len(dataobj.xvalues[0]) == 2377
        assert len(dataobj.xvalues[1]) == 2377
        assert dataobj.xaxis_label == "_rlnDefocusV"
        assert len(dataobj.yvalues) == 2
        assert len(dataobj.yvalues[0]) == 2377
        assert len(dataobj.yvalues[1]) == 2377
        assert dataobj.yaxis_label == "_rlnDefocusU"
        assert dataobj.data_series_labels == ["Series1", "Series2"]
        assert dataobj.associated_data == ["data.star"]

    def test_histp_from_starfile_minimal(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = histogram_from_starfile_col(
            title="my histo",
            starfile="data.star",
            block="particles",
            data_col="_rlnClassNumber",
        )
        hfile = os.path.join(self.test_data, "ResultsFiles/histo_from_star1.json")
        with open(hfile, "r") as hf:
            expected = json.load(hf)
        assert dataobj.__dict__ == expected

    def test_histp_from_starfile_with_labels(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = histogram_from_starfile_col(
            title="my histo",
            starfile="data.star",
            block="particles",
            data_col="_rlnClassNumber",
            xlabel="Classy class",
            ylabel="How many is there be?",
            assoc_data=["completely_different.file", "file2.txt"],
        )
        hfile = os.path.join(self.test_data, "ResultsFiles/histo_from_star2.json")
        with open(hfile, "r") as hf:
            expected = json.load(hf)
        assert dataobj.__dict__ == expected

    def test_ResultsDisplayTable(self):
        rdtab = ResultsDisplayTable(
            title="my table",
            headers=["col1", "col2", "col3"],
            table_data=[[1, 1, 1], [2, 2, 2], [3, 3, 3]],
            associated_data=["file1.txt"],
        )
        assert rdtab.__dict__ == {
            "title": "my table",
            "headers": ["col1", "col2", "col3"],
            "table_data": [[1, 1, 1], [2, 2, 2], [3, 3, 3]],
            "associated_data": ["file1.txt"],
        }

    def test_ResultsDisplayTable_error_header_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayTable(
                title="my table",
                headers=["col1", "col2"],
                table_data=[[1, 1, 1], [2, 2, 2], [3, 3, 3]],
                associated_data=["file1.txt"],
            )

    def test_ResultsDisplayTable_error_data_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayTable(
                title="my table",
                headers=["col1", "col2", "col3"],
                table_data=[[1, 1, 1], [2, 2, 2], [3, 3]],
                associated_data=["file1.txt"],
            )

    def test_merge_bin_imagestack_mrcs(self):
        instack = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        merged_n_binned = mrc_thumbnail(instack, 10, "test.png")

        assert merged_n_binned.shape == (9, 9)
        assert os.path.isfile("test.png")

    def test_merge_bin_imagestack_tiff(self):
        instack = os.path.join(self.test_data, "tiff_stack.tiff")
        merged_n_binned = tiff_thumbnail(instack, 40, "test.png")
        assert merged_n_binned.shape == (40, 40)
        assert os.path.isfile("test.png")

    def test_mini_montage_tifs(self):
        os.makedirs("Thumbs")
        tifffile = os.path.join(self.test_data, "tiff_stack.tiff")
        for n in range(10):
            shutil.copy(tifffile, f"image{n:03d}.tiff")
        imgs = ["image000.tiff", "image001.tiff", "image002.tiff", "image003.tiff"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f0.png")
        expected = {
            "title": "test",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f0.png",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_tifs_not_enough_imgs(self):
        os.makedirs("Thumbs")
        tifffile = os.path.join(self.test_data, "tiff_stack.tiff")
        for n in range(5):
            shutil.copy(tifffile, f"image{n:03d}.tiff")
        imgs = ["image000.tiff", "image001.tiff", "image002.tiff", "image003.tiff"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f0.png")
        expected = {
            "title": "test",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f0.png",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_mrcs(self):
        os.makedirs("Thumbs")
        mrcfile = os.path.join(self.test_data, "single.mrc")
        for n in range(10):
            shutil.copy(mrcfile, f"image{n:03d}.mrc")
        imgs = ["image000.mrc", "image001.mrc", "image002.mrc", "image003.mrc"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f0.png")
        expected = {
            "title": "test",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f0.png",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_mrcs_not_enough_imgs(self):
        os.makedirs("Thumbs")
        mrcfile = os.path.join(self.test_data, "single.mrc")
        for n in range(4):
            shutil.copy(mrcfile, f"image{n:03d}.mrc")
        imgs = ["image000.mrc", "image001.mrc", "image002.mrc", "image003.mrc"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f0.png")
        expected = {
            "title": "test",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f0.png",
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_single_map(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "emd_3488.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["emd_3488.mrc"],
            outputdir="",
            thumbsize=50,
            maps_data="test",
        )
        expected = {
            "maps": ["Thumbnails/emd_3488.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: emd_3488.mrc",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["emd_3488.mrc"],
        }
        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_maps_noopacity(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc", "map2.mrc"],
            outputdir="",
            thumbsize=50,
            maps_data="test",
        )
        expected = {
            "maps": ["Thumbnails/map1.mrc", "Thumbnails/map2.mrc"],
            "maps_opacity": [0.5, 0.5],
            "models": [],
            "title": "Overlaid maps",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mrc"],
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_models_opacity(self):
        touch("model1.pdb")
        touch("model2.pdb")
        dispobj = make_map_model_thumb_and_display(
            models=["model1.pdb", "model2.pdb"],
            outputdir="",
            thumbsize=50,
        )
        expected = {
            "maps": [],
            "maps_opacity": [],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "Overlaid models",
            "maps_data": "",
            "models_data": "model1.pdb, model2.pdb",
            "associated_data": ["model1.pdb", "model2.pdb"],
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_maps_opacity(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc", "map2.mrc"],
            maps_opacity=[1.0, 0.5],
            outputdir="",
            thumbsize=50,
            maps_data="test",
        )
        expected = {
            "maps": ["Thumbnails/map1.mrc", "Thumbnails/map2.mrc"],
            "maps_opacity": [1.0, 0.5],
            "models": [],
            "title": "Overlaid maps",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mrc"],
        }
        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_1map_1model(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        touch("model1.pdb")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc"],
            models=["model1.pdb"],
            outputdir="",
            thumbsize=50,
            maps_data="test",
        )
        expected = {
            "maps": ["Thumbnails/map1.mrc"],
            "maps_opacity": [0.5],
            "models": ["model1.pdb"],
            "title": "Overlaid map: map1.mrc model: model1.pdb",
            "maps_data": "test",
            "models_data": "model1.pdb",
            "associated_data": ["map1.mrc", "model1.pdb"],
        }

        assert dispobj.__dict__ == expected

    def test_mini_montage_from_stafile_movies_file(self):
        os.makedirs("Movies")
        movfile = os.path.join(self.test_data, "movies_mrcs.star")
        shutil.copy(movfile, "movies.star")
        mrcfile = os.path.join(self.test_data, "single_100.mrc")
        for n in range(1, 25):
            shutil.copy(mrcfile, f"Movies/20170629_{n:05d}_frameImage.mrcs")

        with expected_warning(RuntimeWarning, "true_divide", nwarn=2):
            dispobj = mini_montage_from_starfile(
                "movies.star",
                "movies",
                "_rlnMicrographMovieName",
                "",
                nimg=7,
                montagesize=640,
            )

        files = [f"Movies/20170629_{n:05d}_frameImage.mrcs" for n in range(1, 8)]
        expected = {
            "title": "movies.star; 7/24 images",
            "xvalues": [0, 1, 2, 3, 4, 5, 6],
            "yvalues": [0, 0, 0, 0, 0, 0, 0],
            "labels": files,
            "associated_data": ["movies.star"],
            "img": "Thumbnails/montage_f100.png",
        }
        assert dispobj.__dict__ == expected, (dispobj.__dict__, expected)

    def test_mini_montage_from_starfile_particles_file(self):
        partsdir = "Extract/job007/Movies/"
        os.makedirs(partsdir)
        partsfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(partsfile, "particles.star")
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        partsdest = "Extract/job007/Movies/20170629_00021_frameImage.mrcs"
        shutil.copy(mrcfile, partsdest)
        dispobj = mini_montage_from_starfile(
            "particles.star",
            "particles",
            "_rlnImageName",
            "",
            nimg=50,
            montagesize=640,
        )

        ns = range(1, 51).__reversed__()
        labs = [f"{n:06d}@{partsdir}20170629_00021_frameImage.mrcs" for n in ns]
        labs.reverse()
        expected = {
            "title": "particles.star; 50/2377 images",
            "xvalues": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 5,
            "yvalues": [4] * 10 + [3] * 10 + [2] * 10 + [1] * 10 + [0] * 10,
            "labels": labs,
            "associated_data": ["particles.star"],
            "img": "Thumbnails/montage_s0.png",
        }

        assert dispobj.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_s0.png")

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_make_particle_coords_thumb(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(["MotionCorr", "AutoPick"])
        mc_file = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        coords = "Autopick/job006/Movies/20170629_00021_frameImage_autopick.star"
        dispobj = make_particle_coords_thumb(mc_file, coords, "")
        expected = {
            "title": "Example picked particles",
            "image_path": "Thumbnails/picked_coords.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc: "
            "242 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "Autopick/job006/Movies/20170629_00021_frameImage_autopick.star",
            ],
        }
        assert dispobj.__dict__ == expected
        assert os.path.isfile(dispobj.image_path)

    def test_make_mini_motage_from_mrcs(self):
        mrcs = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcs, os.path.join(self.test_dir, "img.mrcs"))
        dispobj = mini_montage_from_mrcs_stack(
            outputdir="",
            mrcs_file="img.mrcs",
            nimg=11,
        )
        expected = {
            "title": "",
            "xvalues": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "yvalues": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            "labels": [
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ],
            "associated_data": ["img.mrcs"],
            "img": "Thumbnails/montage_m0.png",
        }
        assert dispobj.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_m0.png")


if __name__ == "__main__":
    unittest.main()
