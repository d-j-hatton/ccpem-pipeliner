import shutil
import os
import time
import subprocess
from glob import glob

from pipeliner import job_factory
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE
from pipeliner.job_runner import JobRunner
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import print_coms
from pipeliner.utils import get_pipeliner_root

PIPELINER_TEST_DATA = os.path.dirname(test_data.__file__)
cc_path = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


def job_running_test(
    test_jobfile,
    input_files,
    expected_outfiles,
    sleep_time=1,
    success=True,
    overwrite_job=None,
    n_metadata_files_expected=1,
    print_run=False,
    print_err=False,
    show_contents=False,
):
    """Runs a job and checks that the expected outputs are produced

    Args:
        test_jobfile (str): The name of the file containing the running parameters
            for the job. A run.job or job.star file
        input_files (list):  A list of tuples for input files to the job (Directory
            the files should be in for the test, path to the file to copy)
        expected_outfiles (list): List of expected output files.  Only needs the
            path past the output dir of the job IE: `Import/job001/movies/test.mrc`
            should be `movies/test.mrc`
        sleep_time (int): Time to wait before checking for the job's outputs, only
            necessary when large files are written and it's a bit slow
        success (bool): Do you expect the job to be successful
        overwrite_job (str): Overwrite this existing job when running the job.  If
            `None` creates the job as job001
        n_metadata_files_expected (int): Expected number of pipeliner metadata files
            to be produced by the job, almost always 1
        print_run (bool): Display the contents of the run.job file, for debugging
        print_err (bool): Display the contents of the run.err file, for debugging
        show_contents (bool): Show the contents of the output directory, for debugging

    """

    # Prepare the directory structure as if previous jobs have been run:
    for infile in input_files:
        import_dir = infile[0]
        if not os.path.isdir(import_dir):
            os.makedirs(import_dir)
        shutil.copy(infile[1], import_dir)

    pipeline = JobRunner()
    pipeline.graph.job_counter = 998
    job = job_factory.read_job(test_jobfile)
    job_dir = os.path.join(job.OUT_DIR, "job998/")

    extra_files = [
        "job.star",
        "note.txt",
        "job_pipeline.star",
        "run.out",
        "run.err",
    ]

    if success:
        extra_files.append(SUCCESS_FILE)
    else:
        extra_files.append(FAIL_FILE)

    overwrite = False if overwrite_job is None else True
    job_run = pipeline.run_job(job, overwrite_job, False, False, overwrite, False)

    # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
    time.sleep(sleep_time)
    if print_run:
        subprocess.run(["cat", os.path.join(job_dir, "run.out")])
    if print_err:
        subprocess.run(["cat", os.path.join(job_dir, "run.err")])

    if show_contents:
        subprocess.run(["ls", "-al"])
        subprocess.run(["ls", "-al", job_dir])
        subprocess.run(["cat", "default_pipeline.star"])
        subprocess.run(["cat", "{}/job.star".format(job_dir)])
        subprocess.run(["cat", "{}/run.out".format(job_dir)])
        subprocess.run(["cat", "{}/job_pipeline.star".format(job_dir)])

        # test for output files
    assert os.path.isdir(job_dir)
    for outfile in expected_outfiles:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for outfile in extra_files:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    assert os.path.isfile(".gui_" + job.PROCESS_NAME.replace(".", "_") + "job.star")

    # look for the metadata file
    md_files = glob(f"{job_dir}/*job_metadata.json")
    assert len(md_files) == n_metadata_files_expected, md_files

    return job_run


def job_generate_commands_test(
    jobfile,
    input_nodes,
    output_nodes,
    expected_commands,
    show_coms=False,
    show_inputnodes=False,
    show_outputnodes=False,
    qsub=False,
):
    """Test that executing the get command function returns the expected
    commands and input and output nodes.

    If the commands don't match, a comparison of the expected and actual
    commands will be displayed

    The pipelienr's check completion command will automatically be added,
    as well as some of the wrapping that the pipeliner performs

    Args:
        jobfile (str): The file containing the running parameters for the job.
            job.star or run.job format
        input_nodes (dict): The input nodes expected to be created by the job
            `{node file path: nodetype}`
        output_nodes (dict): The output nodes excpected to be created by the job
            Only needs the path past the output dir of the job IE:
            `Import/job001/movies/test.mrc` should be `movies/test.mrc`.
            {node file, nodetype}
        expected commands (list): The commands the job is expected to generate
            each command string should be one item in the list
        show_coms (bool): Show the commands that were generated, for debugging
        show_inputnodes (bool): Show the input nodes that would be created, for
            debugging
        show_outputnodes (bool): Show the output nodes that would be created,
            for debugging
        qsub (bool): Is the job being submitted to a queue?
    """

    job = job_factory.read_job(jobfile)

    outputname = os.path.join(job.OUT_DIR, "job999/")
    job.output_name = outputname
    commandlist = job.get_commands()
    commands = job.prepare_final_command(outputname, commandlist, False)[0]

    command_strings = [" ".join(x) for x in commands]

    if job.subprocess_cwd:
        check_comp_command = [f"{cc_path} {' '.join(list(output_nodes))}"]
    else:
        check_comp_command = [f"{cc_path} {outputname} {' '.join(list(output_nodes))}"]

    if not qsub and len(output_nodes) > 0:
        expected_commands = expected_commands + check_comp_command

    # add the time -p to every command
    if not qsub:
        for n, com in enumerate(expected_commands):
            if not com.startswith("time -p"):
                expected_commands[n] = "time -p " + com

    assert command_strings == expected_commands, print_coms(
        command_strings, expected_commands
    )

    def print_inputnodes(expected):
        print("\nINPUT NODES: ({}/{})".format(len(job.input_nodes), len(expected)))
        for i in job.input_nodes:
            print(i.name)

    def print_outputnodes(expected):
        print("\nOUTPUT NODES: ({}/{})".format(len(job.output_nodes), len(expected)))
        for i in job.output_nodes:
            print(i.name)

    # make sure the expected nodes have been created
    expected_out_nodes = [
        os.path.join(job.OUT_DIR, f"job999/{x}") for x in output_nodes
    ]
    actual_in_nodes = [x.name for x in job.input_nodes]
    actual_out_nodes = [x.name for x in job.output_nodes]

    if show_coms:
        print_coms([" ".join(x) for x in commands], expected_commands)
    if show_inputnodes:
        print_inputnodes(input_nodes)
    if show_outputnodes:
        print_outputnodes(output_nodes)

    for node in input_nodes:
        assert node in actual_in_nodes, node
    for node in expected_out_nodes:
        assert node in actual_out_nodes, node

    # make sure no extra nodes have been produced
    for node in actual_in_nodes:
        assert node in input_nodes, "EXTRA NODE: " + node
    for node in actual_out_nodes:
        assert node in expected_out_nodes, "EXTRA NODE: " + node

    # make sure all of the nodes are of the correct type
    for node in job.input_nodes:
        assert node.type == input_nodes[node.name], (
            node.name,
            "node error expect/act",
            input_nodes[node.name],
            node.type,
        )
    # using the expected list here, but it has already been checked
    for node in job.output_nodes:
        node_name = node.name.replace(job.output_name, "")
        assert node.type == output_nodes[node_name], (
            node_name,
            "node error expect/act",
            output_nodes[node_name],
            node.type,
        )
