#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import tempfile
import shutil

from pipeliner.job_options import (
    files_exts,
    IntJobOption,
    FloatJobOption,
    FileNameJobOption,
    StringJobOption,
    InputNodeJobOption,
    MultipleChoiceJobOption,
    BooleanJobOption,
    JobOptionValidationResult,
)
from pipeliner_tests import test_data
from pipeliner.utils import touch


class JobRunnerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_files_extensions_single(self):
        name = "Test file type"
        exts = [".mrc"]
        result = files_exts(name, exts)
        assert result == "Test file type (*.mrc)"

    def test_files_extensions_multiple(self):
        name = "Test file types"
        exts = [".mrc", "_unfil.mrcs", ".txt"]
        result = files_exts(name, exts)
        assert result == "Test file types (*.mrc *_unfil.mrcs *.txt)"

    def test_files_extensions_single_exact(self):
        name = "Exact file type"
        exts = ["myfile.mrc"]
        result = files_exts(name, exts, True)
        assert result == "Exact file type (myfile.mrc)"

    def test_files_extensions_multiple_exact(self):
        name = "Exact file types"
        exts = ["myfile.mrc", "f_unfil.mrcs", "text.txt"]
        result = files_exts(name, exts, True)
        assert result == "Exact file types (myfile.mrc f_unfil.mrcs text.txt)"

    def test_validate_int_jo_noerror(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
        )
        int_jo.value = 1
        assert int_jo.validate() is None

    def test_validate_int_jo_is_required(self):
        int_jo = IntJobOption(
            label="Test int JobOption", default_value=0, is_required=True
        )
        int_jo.value = None
        valobj = int_jo.validate()
        ermsg = "Required parameter empty"
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == IntJobOption

    def test_validate_int_jo_below_hard_min(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        int_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = int_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == IntJobOption

    def test_validate_int_jo_above_hard_max(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        int_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = int_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == IntJobOption

    def test_validate_float_jo_noerror(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
        )
        float_jo.value = 1
        assert float_jo.validate() is None

    def test_validate_float_jo_is_required(self):
        float_jo = FloatJobOption(
            label="Test float JobOption", default_value=0, is_required=True
        )
        float_jo.value = None
        ermsg = "Required parameter empty"
        valobj = float_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FloatJobOption

    def test_validate_float_jo_below_hard_min(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        float_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = float_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FloatJobOption

    def test_validate_float_jo_above_hard_max(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        float_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = float_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FloatJobOption

    def test_validate_string_jo_noerror(self):
        float_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        assert float_jo.validate() is None

    def test_validate_string_jo_not_required(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        string_jo.value = ""
        assert string_jo.validate() is None

    def test_validate_string_joboption_regexmatches(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "test string 123"
        assert string_jo.validate() is None

    def test_validate_string_joboption_regex_nomatch(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "NONONO string 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = string_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == StringJobOption

    def test_validate_filename_jo_noerror(self):
        float_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
        )
        assert float_jo.validate() is None

    def test_validate_filename_jo_not_required(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
        )
        filename_jo.value = ""
        assert filename_jo.validate() is None

    def test_validate_filename_joboption_regexmatches(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "test filename 123"
        assert filename_jo.validate() is None

    def test_validate_filename_joboption_regex_nomatch(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "NONONO filename 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = filename_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FileNameJobOption

    def test_filename_exists_validation_allOK(self):
        """File actually exists"""

        test_file = os.path.join(self.test_dir, "test.txt")
        touch(test_file)
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        assert filename_jo.check_file() is None

    def test_filename_exists_validation_file_missing_checkfile_finds(self):
        """File missing check_file() chould catch it"""

        test_file = os.path.join(self.test_dir, "test.txt")
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        ermsg = f"File {test_file} not found"
        valobj = filename_jo.check_file()
        assert type(valobj) == JobOptionValidationResult, valobj
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FileNameJobOption

    def test_filename_exists_validation_file_missing_validate_doesntfind(self):
        """File missing validate() should not catch it"""

        test_file = os.path.join(self.test_dir, "test.txt")
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        assert filename_jo.validate() is None

    def test_filename_exists_validation_file_inPATH(self):
        """File is an exe in the $PATH shouldn't fail validation"""

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="ls",
            is_required=True,
        )
        filename_jo.value = "ls"
        assert filename_jo.check_file() is None

    def test_filename_exists_validation_files_wildcard(self):
        """File is a list to a search string"""

        os.makedirs("test")
        for n in range(5):
            touch(f"test/file_{n}.txt")

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="test/*.txt",
            is_required=True,
        )
        filename_jo.value = "test/*.txt"
        assert filename_jo.check_file() is None

    def test_filename_exists_validation_files_wildcard_none_found(self):
        """File is a list to a search string, but no files were found"""

        os.makedirs("test")
        for n in range(5):
            touch(f"test/file_{n}.txt")

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="test/*.mrc",
            is_required=True,
        )
        filename_jo.value = "test/*.mrc"
        ermsg = "File test/*.mrc not found"
        valobj = filename_jo.check_file()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == FileNameJobOption

    def test_validate_inputnode_jo_noerror(self):
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            node_type="test.node",
        )
        assert float_jo.validate() is None

    def test_validate_inputnode_jo_not_required(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            node_type="test.node",
        )
        inputnode_jo.value = ""
        assert inputnode_jo.validate() is None

    def test_validate_inputnode_joboption_regexmatches(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "test inputnode 123"
        assert inputnode_jo.validate() is None

    def test_validate_inputnode_joboption_regex_nomatch(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "NONONO inputnode 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = inputnode_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == InputNodeJobOption

    def test_validate_multichoice_jo_noerror(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        assert mc_jo.validate() is None

    def test_validate_multichoice_bad_option(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        mc_jo.value = "mbili"
        ermsg = "mbili not in list of accepted values"
        valobj = mc_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == MultipleChoiceJobOption

    def test_validate_multichoice_jo_noval_required(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
            is_required=True,
        )
        mc_jo.value = ""
        ermsg = "Required parameter empty"
        valobj = mc_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == MultipleChoiceJobOption

    def test_validate_bool_joboption_noerror(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        assert bool_jo.validate() is None

    def test_validate_bool_joboption_noval(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = None
        ermsg = "Required parameter empty"
        valobj = bool_jo.validate()
        assert type(valobj) == JobOptionValidationResult
        assert valobj.type == "error"
        assert valobj.message == ermsg
        assert type(valobj.raised_by[0]) == BooleanJobOption


if __name__ == "__main__":
    unittest.main()
