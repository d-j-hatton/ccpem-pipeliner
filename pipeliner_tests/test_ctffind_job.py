#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    get_relion_tutorial_data,
    tutorial_data_available,
)

from pipeliner.jobs.relion.ctffind_job import (
    INPUT_NODE_MICS,
    OUTPUT_NODE_MICS,
    OUTPUT_NODE_LOG,
)

from pipeliner.project_graph import ProjectGraph


class CtfFindTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_ctffind_env_var(self):
        # see if the user already has an env var for ctffind
        old_ctffind_env_var = os.environ.get("RELION_CTFFIND_EXECUTABLE")

        # read job
        job = job_factory.read_job(
            os.path.join(
                self.test_data, "JobFiles/CtfFind/ctffind_noexe_ctffind4_run.job"
            )
        )

        # ctffind var should be eithr default or user set
        assert job.joboptions["fn_ctffind_exe"].default_value in (
            "",
            old_ctffind_env_var,
        ), job.joboptions["fn_ctffind_exe"].default_value

        # set the variable
        os.environ["RELION_CTFFIND_EXECUTABLE"] = os.path.join(
            self.test_data, "fake_ctffind.exe"
        )
        # read the job
        job = job_factory.read_job(
            os.path.join(
                self.test_data, "JobFiles/CtfFind/ctffind_noexe_ctffind4_run.job"
            )
        )
        # check it was read
        assert job.joboptions["fn_ctffind_exe"].default_value == os.path.join(
            self.test_data, "fake_ctffind.exe"
        )
        # restore the old var (if any)
        if old_ctffind_env_var is not None:
            os.environ["RELION_CTFFIND_EXECUTABLE"] = old_ctffind_env_var
        else:
            del os.environ["RELION_CTFFIND_EXECUTABLE"]

    def test_get_command_ctffind(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_ctffind_run.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_ctffind_run_relionstyle.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_jobstar(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_job.star",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_continue_jobstar(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_continue_job.star",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job003/"
            ],
        )

    def test_is_continue_works(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_continue_run.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_gctf(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_gctf_run.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                " /public/EM/Gctf/bin/Gctf --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_gctf_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_gctf_run_relionstyle.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                " /public/EM/Gctf/bin/Gctf --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_nonmpi(self):
        general_get_command_test(
            self,
            "CtfFind",
            "ctffind_nompi_run.job",
            3,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "micrographs_ctf.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_ctffind --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --ctffind_exe /public/EM/ctffind/ctffind.exe"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_nofile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "CtfFind",
                "ctffind_ctffind_nofile.job",
                3,
                1,
                2,
                " ",
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctffind_generate_display_data(self):
        get_relion_tutorial_data(["CtfFind", "MotionCorr"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("CtfFind/job003/")
        dispobjs = pipeline.get_process_results_display(proc)

        expected = [
            {
                "title": "Defocus",
                "bins": [4, 7, 6, 4, 3],
                "bin_edges": [
                    0.813312,
                    0.9096186,
                    1.0059252,
                    1.1022318,
                    1.1985384,
                    1.294845,
                ],
                "xlabel": "Defocus (uM)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
            },
            {
                "title": "Astigmatism",
                "bins": [1, 0, 0, 0, 0, 0, 2, 8, 7, 6],
                "bin_edges": [
                    51.68,
                    54.569,
                    57.458,
                    60.346999999999994,
                    63.236,
                    66.125,
                    69.014,
                    71.90299999999999,
                    74.792,
                    77.681,
                    80.57,
                ],
                "xlabel": "Astigmatism (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
            },
            {
                "title": "Resolution of good Thon ring fit",
                "bins": [8, 6, 5, 2, 2, 0, 0, 0, 0, 0, 0, 1],
                "bin_edges": [
                    3.0,
                    3.15,
                    3.3,
                    3.45,
                    3.6,
                    3.75,
                    3.9,
                    4.05,
                    4.2,
                    4.35,
                    4.5,
                    4.65,
                    4.8,
                ],
                "xlabel": "Resolution (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
            },
        ]

        for i in dispobjs:
            assert i.__dict__ in expected

        dicts = [x.__dict__ for x in dispobjs]
        for i in expected:
            assert i in dicts

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctffind_generate_display_data_nologfiles(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        tdata = os.getenv("PIPELINER_TUTORIAL_DATA")
        get_relion_tutorial_data(["MotionCorr"])
        os.makedirs("CtfFind/job003/Movies")
        shutil.copy(
            os.path.join(tdata, "CtfFind/job003/job.star"), "CtfFind/job003/job.star"
        )
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("CtfFind/job003/")
        dispobjs = pipeline.get_process_results_display(proc)

        assert dispobjs[0].__dict__ == {
            "title": "Results pending...",
            "message": "Error creating results display",
            "reason": "Error creating ResultsDisplayHistogram: No data to operate on",
        }


if __name__ == "__main__":
    unittest.main()
