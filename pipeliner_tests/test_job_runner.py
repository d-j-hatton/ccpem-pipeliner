#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import time
import json
import re
from glob import glob

from pipeliner.job_runner import JobRunner, job_factory
from pipeliner.jobs.relion import select_job
from pipeliner_tests import test_data, generic_tests
from pipeliner.data_structure import (
    RELION_SUCCESS_FILE,
    SUCCESS_FILE,
    JOBINFO_FILE,
    STARFILE_READS_OFF,
)
from pipeliner.jobs.relion.import_job import (
    OUTPUT_NODE_REF3D,
    OUTPUT_NODE_MASK3D,
    make_node_name,
)
from pipeliner_tests.generic_tests import read_pipeline, check_for_relion
from pipeliner.utils import get_pipeliner_root, touch

do_full = generic_tests.do_slow_tests()
cc_path = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


@unittest.skipUnless(check_for_relion(), "Relion needed for test")
class JobRunnerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def wait_for_file(self, path, time_in_seconds=1):
        """Wait for a file to appear after a job"""
        step = 0.1
        total = 0
        while not os.path.isfile(path):
            time.sleep(step)
            total += step
            assert total < time_in_seconds, (
                f"File {path} does not exist after " f"{time_in_seconds} seconds"
            )

    def test_running_fails_file_validation(self):

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        with self.assertRaises(ValueError):
            pipeline.run_job(job, None, False, False, False, False)

    def test_running_import_mask_job(self):
        # Copy the mask file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False, False, False)
        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(0.1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488_mask.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488_mask.mrc"
        assert project_graph.node_list[0].type == make_node_name(
            OUTPUT_NODE_MASK3D, ".mrc"
        )
        assert os.path.isfile(".gui_relion_import_otherjob.star")

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

    def test_running_import_map_job(self):
        # Copy the map file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False, False, False)

        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488.mrc"
        assert project_graph.node_list[0].type == make_node_name(
            OUTPUT_NODE_REF3D, ".mrc"
        )

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

    def test_running_import_map_job_with_comments(self):
        # Copy the map file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(
            job, None, False, False, False, False, comment="Here is a comment"
        )

        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488.mrc"
        assert project_graph.node_list[0].type == make_node_name(
            OUTPUT_NODE_REF3D, ".mrc"
        )

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

        # check the jobinfo file
        with open(f"Import/job001/{JOBINFO_FILE}", "r") as ji:
            actual_ji = json.load(ji)
        assert actual_ji["job directory"] == "Import/job001/"
        assert actual_ji["comments"] == ["Here is a comment"]
        assert actual_ji["rank"] is None
        ts = list(actual_ji["history"])[0]
        assert re.match(r"\d+-\d+-\d+ \d+:\d+:\d+", ts)
        assert actual_ji["history"][ts] == "Run"
        com = [
            "time -p relion_import --do_other --i emd_3488.mrc --odir Import/job001/"
            " --ofile emd_3488.mrc --pipeline_control Import/job001/",
            f"time -p {cc_path} Import/job001/" " emd_3488.mrc",
        ]
        assert actual_ji["command history"][ts] == com

    def test_running_postprocess_job(self):
        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        pipeline = JobRunner()
        pipeline.graph.job_counter = 3
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
        )

        job_dir = "PostProcess/job003/"

        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False)

        # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))
        assert os.path.isfile(os.path.join(job_dir, RELION_SUCCESS_FILE))
        assert os.path.isfile(os.path.join(job_dir, SUCCESS_FILE))
        assert os.path.isfile(".gui_relion_postprocessjob.star")

        # look for the metadata file
        md_files = glob("PostProcess/job003/*job_metadata.json")
        assert len(md_files) == 1
        os.system(f"cat PostProcess/job003/{JOBINFO_FILE}")

    def test_generic_with_PP(self):
        generic_tests.running_job(
            self,
            "postprocess.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

    def test_running_maskcreate_job(self):
        generic_tests.running_job(
            self,
            "maskcreate.job",
            "MaskCreate",
            2,
            [("Import/job001", "emd_3488.mrc")],
            ("run.out", "run.err", "mask.mrc"),
            5,
        )

    def test_postprocess_adhocbf(self):
        generic_tests.running_job(
            self,
            "postprocess_adhocbf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

    def test_postprocess_autobf(self):
        generic_tests.running_job(
            self,
            "postprocess_autobf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

    @unittest.skipUnless(do_full, "Slow test: Only runs in full unittest")
    def test_split_job(self):
        """Select jobs that split star files in defined size groups generate
        an unknown number of output files so they are a special case where
        output nodes are generated after the job runs"""

        # first run a job to create the dirs
        generic_tests.running_job(
            self,
            "select_parts_split_runtest.job",
            "Select",
            3,
            [("Extract/job001", "particles.star")],
            (
                "run.out",
                "run.err",
                "particles_split1.star",
                "particles_split2.star",
                "particles_split3.star",
                "particles_split4.star",
                "particles_split5.star",
                "particles_split6.star",
                "particles_split7.star",
                "particles_split8.star",
                "particles_split9.star",
                "particles_split10.star",
                "particles_split11.star",
                "particles_split12.star",
            ),
            1,
        )
        os.system("ls -al Select/job003/*")
        os.system("cat Select/job003/RELION_OUTPUT_NODES.star")

        partnode = select_job.OUTPUT_NODE_PARTS
        exp_lines = [
            ["Select/job003/", "Select/job003/particles_split1.star"],
            ["Select/job003/", "Select/job003/particles_split10.star"],
            ["Select/job003/", "Select/job003/particles_split11.star"],
            ["Select/job003/", "Select/job003/particles_split12.star"],
            ["Select/job003/", "Select/job003/particles_split2.star"],
            ["Select/job003/", "Select/job003/particles_split3.star"],
            ["Select/job003/", "Select/job003/particles_split4.star"],
            ["Select/job003/", "Select/job003/particles_split5.star"],
            ["Select/job003/", "Select/job003/particles_split6.star"],
            ["Select/job003/", "Select/job003/particles_split7.star"],
            ["Select/job003/", "Select/job003/particles_split8.star"],
            ["Select/job003/", "Select/job003/particles_split9.star"],
            ["Select/job003/particles_split1.star", partnode],
            ["Select/job003/particles_split10.star", partnode],
            ["Select/job003/particles_split11.star", partnode],
            ["Select/job003/particles_split12.star", partnode],
            ["Select/job003/particles_split2.star", partnode],
            ["Select/job003/particles_split3.star", partnode],
            ["Select/job003/particles_split4.star", partnode],
            ["Select/job003/particles_split5.star", partnode],
            ["Select/job003/particles_split6.star", partnode],
            ["Select/job003/particles_split7.star", partnode],
            ["Select/job003/particles_split8.star", partnode],
            ["Select/job003/particles_split9.star", partnode],
        ]
        lines = read_pipeline("default_pipeline.star")
        for i in lines:
            print(i)
        for line in exp_lines:
            assert line in lines, line

    def test_overwriting_job_with_different_type_raises_error(self):
        """Can't overwrite a job with a different type of job"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            self,
            "postprocess_autobf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

        # then overwrite it with a different job
        with self.assertRaises(ValueError):
            generic_tests.running_job(
                self,
                "maskcreate.job",
                "MaskCreate",
                0,  # this should not be used because it's overwrite
                [("Import/job001", "emd_3488.mrc")],
                ("run.out", "run.err", "mask.mrc"),
                5,
                overwrite=True,
                target_job=job_run,
            )

    def test_overwriting_job(self):
        """Overwrite a job with a new run of the same type"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            self,
            "postprocess_autobf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

        # then overwrite it with another job
        generic_tests.running_job(
            self,
            "postprocess_skipfsc.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
            overwrite=True,
            target_job=job_run,
            n_metadata_files_expected=2,
        )

    def test_overwriting_job_with_children(self):
        """Overwrite a job with children, make sure it is archived and
        The change is noted on the children"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            self,
            "postprocess_autobf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            2,
        )

        # copy in a pipeline with fake child jobs
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/children_pipeline.star"),
            "default_pipeline.star",
        )

        # make the childjob dirs
        os.makedirs("ChildProcess/job004/")
        os.makedirs("ChildProcess/job005/")

        # then overwrite it with another job
        generic_tests.running_job(
            self,
            "postprocess_adhocbf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            2,
            overwrite=True,
            target_job=job_run,
            n_metadata_files_expected=2,
        )
        # check that the overwritten markers have been written
        for child_dir in ["ChildProcess/job004/", "ChildProcess/job005/"]:
            PO_marker = glob(child_dir + "PARENT_OVERWRITTEN*")
            assert len(PO_marker) == 1, PO_marker

        # find the archive directory
        alldirs = glob(".*")
        for i in alldirs:
            if "PostProcess.job003" in i:
                archive_dir = i
                break

        # get the files in it
        adir_files = glob(archive_dir + "/*")
        expected_files = [
            RELION_SUCCESS_FILE,
            SUCCESS_FILE,
            "continue_job.star",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in expected_files:
            assert archive_dir + "/" + f in adir_files, f

    def test_postprocess_skipfsc(self):
        generic_tests.running_job(
            self,
            "postprocess_skipfsc.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

    def test_postprocess_withmtf(self):
        generic_tests.running_job(
            self,
            "postprocess_adhocbf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )

    def test_disable_results_and_metadata(self):
        touch(STARFILE_READS_OFF)
        generic_tests.running_job(
            self,
            "postprocess_adhocbf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )
        results_file = os.path.join(
            self.test_dir, "PostProcess/job003/.results_display_text_001.json"
        )
        with open(results_file, "r") as rf:
            results = json.load(rf)
        assert results == {
            "title": "Result display disabled",
            "display_data": "Results display has been disabled for this project. "
            "This is usually done because of memory issues caused by reading large"
            " starfiles.  To re-enable this feature delete the file "
            ".PIPELINER_SKIP_STARFILE_READS",
            "associated_data": [],
        }

        mdfile = glob("PostProcess/job003/*_job_metadata.json")
        with open(mdfile[0]) as mdf:
            metadat = json.load(mdf)
        assert metadat == {
            "No metadata collected": "This project has metadata collection disabled. "
            "This is usually done because of memory issues caused by reading large "
            "starfiles.  To re-enable this feature delete the file .PIPELINER_SKIP_"
            "STARFILE_READS"
        }


if __name__ == "__main__":
    unittest.main()
