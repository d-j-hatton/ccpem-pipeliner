#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import io
import unittest
import os
import shutil
import stat
import tempfile
import time

from gemmi import cif
from glob import glob

from pipeliner import star_writer, job_factory
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import touch
from pipeliner.api.api_utils import write_default_jobstar
from pipeliner_tests import test_data
from pipeliner.data_structure import (
    NODES_DIR,
    STATUS2LABEL,
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    CLASS2D_PARTICLE_NAME_EM,
    INIMODEL_JOB_NAME,
    CLASS3D_PARTICLE_NAME,
    REFINE3D_PARTICLE_NAME,
    EXTRACT_PARTICLE_NAME,
    CTFFIND_GCTF_NAME,
)
from pipeliner.job_runner import JobRunner
from pipeliner.jobs.relion import (
    refine3D_job,
    extract_job,
    motioncorr_job,
    ctffind_job,
)
from pipeliner.jobs.relion import class2D_job, initialmodel_job, import_job, class3D_job
from pipeliner_tests.generic_tests import (
    read_pipeline,
    check_for_relion,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.star_writer import COMMENT_LINE

UNKNOWN_STAR_NODE = "UnknownNodeType.star"
has_relion = check_for_relion()


class ProjectGraphTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_converting_old_style_to_new_style_pipeline(self):
        """Make sure the nodes edges and process lists have
        the right number of entries"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        os.makedirs(".relion_lock")
        touch(".relion_lock/lock_tutorial_pipeline.star")

        pipeline.write()

    def test_reading_old_style_tutorial_pipeline(self):
        """Make sure the nodes edges and process lists have the
        right number of entries"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        # Check pipeline was read correctly
        assert pipeline.job_counter == 30, pipeline.job_counter
        assert len(pipeline.process_list) == 28, len(pipeline.process_list)
        assert len(pipeline.node_list) == 62, len(pipeline.node_list)

    def test_reading_new_style_tutorial_pipeline(self):
        """Make sure the nodes edges and process lists have the
        right number of entries"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/converted_relion31_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="converted_relion31")
        pipeline.read()

        # Check pipeline was read correctly
        assert pipeline.job_counter == 32
        assert len(pipeline.process_list) == 31
        assert len(pipeline.node_list) == 72

    def test_star_writer_formatting_with_tutorial_pipeline_old_style(self):
        """Check the star writer writes out a converted pipeline
        in the old style - this is depricated now because piplines have
        a new format different from other starfiles in the dev-pipeliner version"""

        # read the pipeline and then immediately rewrite it
        pipeline_file = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        doc = cif.read_file(pipeline_file)
        out_stream = io.StringIO()
        star_writer.write_to_stream(doc, out_stream)
        actual = out_stream.getvalue()
        with open("wrote_pipeline.star", "w") as outfile:
            outfile.write(actual)

        # read the originl and written pipelines
        with open(pipeline_file) as f:
            expected = f.readlines()
        with open("wrote_pipeline.star", "r") as outfile:
            written = outfile.readlines()
        lineindex = 0

        # make sure all the lines are the same except the comment lines
        for line in expected:
            if "#" not in line:
                assert line.split() == written[lineindex].split()
            lineindex += 1

    def test_star_writer_formatting_with_tutorial_pipeline_new_style(self):
        """Check the star writer writes out a converted pipeline when
        reading a new style pipeline"""
        # read the pipeline and immediatly rewrite it
        pipeline_file = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        doc = cif.read_file(pipeline_file)
        out_stream = io.StringIO()
        star_writer.write_to_stream(doc, out_stream)
        actual = out_stream.getvalue()

        # read the written files
        with open("wrote_pipeline.star", "w") as outfile:
            outfile.write(actual)
        expected = read_pipeline(pipeline_file)
        actual = read_pipeline("wrote_pipeline.star")
        # they should be identical
        for line in actual:
            if line != ["#"] + COMMENT_LINE.split():
                assert line in expected, (line, COMMENT_LINE.split())
        for line in expected:
            if line[:2] != ["#", "version"]:
                assert line in actual, line

    def test_reading_nonexistent_pipeline_raises_exception(self):
        with self.assertRaises(AttributeError):
            ProjectGraph().read()

    def test_writing_new_empty_pipeline(self):
        pipeline = ProjectGraph()
        # have to put the lockfiles in like they were created by a read
        os.makedirs(".relion_lock")
        touch(".relion_lock/lock_default_pipeline.star")
        pipeline.write()

        assert os.path.isfile("default_pipeline.star")

    def test_rename_pipeline_to_existing_name_raises_error(self):
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )
        touch("default_pipeline.star")
        assert os.path.isfile("tutorial_pipeline.star")
        assert os.path.isfile("default_pipeline.star")

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read(do_lock=True)

        # Rename pipeline and write to new STAR file
        with self.assertRaises(ValueError):
            pipeline.set_name("default", new_lock=True)

    def test_remake_pipeline_nodes(self):
        # Copy short tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # make the nodes directory, make sure it's empty
        os.makedirs(".Nodes")
        assert len(os.listdir(".Nodes")) == 0

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="short")
        pipeline.read(do_lock=True)

        # remake the directory with nothing to write
        pipeline.remake_node_directory()
        assert len(os.listdir(".Nodes")) == 0

        # now make the files exist
        os.makedirs("Import/job001/")
        os.makedirs("MotionCorr/job002/")
        os.makedirs("CtfFind/job003/")
        pretend_files = [
            "Import/job001/movies.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/logfile.pdf",
        ]
        for pfile in pretend_files:
            touch(pfile)

        pfalias = [
            import_job.OUTPUT_NODE_MOVIES[0] + "/Import/movies/movies.star",
            motioncorr_job.OUTPUT_NODE_MICS.split(".")[0]
            + "/MotionCorr/own/corrected_micrographs.star",
            motioncorr_job.OUTPUT_NODE_LOG.split(".")[0]
            + "/MotionCorr/own/logfile.pdf",
            ctffind_job.OUTPUT_NODE_MICS.split(".")[0]
            + "/CtfFind/gctf/micrographs_ctf.star",
            ctffind_job.OUTPUT_NODE_LOG.split(".")[0] + "/CtfFind/gctf/logfile.pdf",
        ]

        # remake the node dirs again now that the files exist
        pipeline.remake_node_directory()
        for pfa in pfalias:
            thefile = ".Nodes/" + pfa
            assert os.path.isfile(thefile), thefile
            mode_str = stat.filemode(os.stat(thefile).st_mode)
            assert mode_str == "-rwxrwxrwx", mode_str

    def test_remake_pipeline_nodes_with_scheduled(self):
        # Copy short tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_sched_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_sched_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # make the nodes directory, make sure it's empty
        os.makedirs(".Nodes")
        assert len(os.listdir(".Nodes")) == 0

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="short_sched")
        pipeline.read(do_lock=True)

        # remake the directory
        pipeline.remake_node_directory()

        pfalias = [
            import_job.OUTPUT_NODE_MOVIES[0] + "/Import/movies/movies.star",
            motioncorr_job.OUTPUT_NODE_MICS.split(".")[0]
            + "/MotionCorr/own/corrected_micrographs.star",
            motioncorr_job.OUTPUT_NODE_LOG.split(".")[0]
            + "/MotionCorr/own/logfile.pdf",
            ctffind_job.OUTPUT_NODE_MICS.split(".")[0]
            + "/CtfFind/gctf/micrographs_ctf.star",
            ctffind_job.OUTPUT_NODE_LOG.split(".")[0] + "/CtfFind/gctf/logfile.pdf",
        ]

        for pfa in pfalias:
            thefile = ".Nodes/" + pfa
            assert os.path.isfile(thefile), thefile

    def test_checking_pipline_for_finished_jobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_running_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="short_running")
        pipeline.read()

        # no jobs have been completed but pipeline has been
        # renamed, so new one has been written
        # another copy needs to be written to convert it to the
        # new format
        pipeline.set_name("converted", new_lock=False)
        pipeline.set_name("updating", new_lock=False)
        pipeline.check_process_completion()

        assert os.path.isfile("updating_pipeline.star")

        with open("converted_pipeline.star") as updated_pipe:
            converted_pipe = updated_pipe.read()

        # make the files
        os.makedirs("Import/job001/")
        os.makedirs("MotionCorr/job002/")
        os.makedirs("CtfFind/job003/")
        for f in [
            "Import/job001/" + FAIL_FILE,
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "MotionCorr/job002/" + SUCCESS_FILE,
            "CtfFind/job003/" + ABORT_FILE,
        ]:
            touch(f)
            assert os.path.isfile(f), f

        # update the pipeline
        pipeline.check_process_completion()
        with open("updating_pipeline.star") as updated_pipe:
            updated = updated_pipe.read()
        assert updated != converted_pipe
        uplines = updated.split("\n")
        assert uplines[17].split()[-1] == STATUS2LABEL[3], uplines[17]
        assert uplines[18].split()[-1] == STATUS2LABEL[2], uplines[18]
        assert uplines[19].split()[-1] == STATUS2LABEL[4], uplines[19]
        mics_nodesfile = os.listdir(
            f".Nodes/{motioncorr_job.OUTPUT_NODE_MICS.split('.')[0]}/MotionCorr/own/"
        )
        assert mics_nodesfile == ["corrected_micrographs.star"]
        log_nodesfile = os.listdir(
            f".Nodes/{motioncorr_job.OUTPUT_NODE_LOG.split('.')[0]}/MotionCorr/own/"
        )
        assert log_nodesfile == ["logfile.pdf"]

    def test_checking_pipeline_for_finished_jobs_read_outnodes_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_running_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="short_running")
        pipeline.read()

        # make the files
        os.makedirs("Import/job001/")
        os.makedirs("MotionCorr/job002/")
        os.makedirs("CtfFind/job003/")
        os.makedirs("SomeOtherJob/job400/")

        for f in [
            "Import/job001/" + FAIL_FILE,
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "MotionCorr/job002/" + SUCCESS_FILE,
            "CtfFind/job003/" + ABORT_FILE,
            "SomeOtherJob/job400/extrafile.star",
        ]:
            touch(f)
            assert os.path.isfile(f), f
        outnodes_file = os.path.join(
            self.test_dir, "MotionCorr/job002/RELION_OUTPUT_NODES.star"
        )
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/RELION_OUTPUT_NODES.star"),
            outnodes_file,
        )
        assert os.path.isfile(outnodes_file)

        pipeline.check_process_completion()
        with open("short_running_pipeline.star") as updated_pipe:
            pipe_lines = [x.split() for x in updated_pipe.read().split("\n")]
        assert ["SomeOtherJob/job400/extrafile.star", UNKNOWN_STAR_NODE] in pipe_lines
        assert [
            "MotionCorr/job002/",
            "SomeOtherJob/job400/extrafile.star",
        ] in pipe_lines

    def test_removing_nodes_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        os.makedirs("CtfFind/job003/")
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        touch("CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="short")
        pipeline.read(do_lock=True)
        pipeline.set_name("deleted", new_lock=False)
        pipeline.delete_node(pipeline.node_list[3])
        with open("deleted_pipeline.star") as updated_pipeline:
            new_pipe = updated_pipeline.read()
        removed_lines = [
            "CtfFind/job003/micrographs_ctf.star {}".format(
                ctffind_job.OUTPUT_NODE_MICS
            ),
            "CtfFind/job003/ CtfFind/job003/micrographs_ctf.star",
        ]
        for rl in removed_lines:
            assert rl not in new_pipe, rl
        assert not os.path.isfile("CtfFind/job003/micrographs_ctf.star")

    def test_removing_process_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        os.makedirs("CtfFind/job003/")
        os.symlink(
            os.path.abspath("CtfFind/job003/"),
            os.path.join(self.test_dir, "CtfFind/gctf"),
            True,
        )
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        touch("CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        pipeline = ProjectGraph(name="short")
        pipeline.read(do_lock=True)
        pipeline.set_name("deleted", new_lock=False)
        pipeline.delete_job(pipeline.process_list[2])

        with open("deleted_pipeline.star") as updated_pipeline:
            new_pipe = updated_pipeline.read()
        removed_lines = [
            "CtfFind/job003/micrographs_ctf.star {}".format(
                ctffind_job.OUTPUT_NODE_MICS
            ),
            "CtfFind/job003/logfile.pdf {}".format(ctffind_job.OUTPUT_NODE_LOG),
            "CtfFind/job003/ CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/ CtfFind/job003/logfile.pdf ",
            "CtfFind/job003/ CtfFind/gctf/      CtfFind    Succeeded ",
        ]
        for rl in removed_lines:
            assert rl not in new_pipe, rl
        assert not os.path.isfile("CtfFind/job003/logfile.pdf")
        assert not os.path.isfile("CtfFind/job003/micrographs_ctf.star")
        assert not os.path.isdir("CtfFind/job003/")

    def test_removing_multiple_processs_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        os.makedirs("CtfFind/job003/")
        os.makedirs("MotionCorr/job002/")
        os.symlink(
            os.path.abspath("MotionCorr/job002/"),
            os.path.join(self.test_dir, "MotionCorr/own"),
        )
        os.symlink(
            os.path.abspath("CtfFind/job003/"),
            os.path.join(self.test_dir, "CtfFind/gctf"),
        )
        files = [
            "CtfFind/job003/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
        ]

        for f in files:
            touch(f)
            assert os.path.isfile(f), f

        pipeline = ProjectGraph(name="short")
        pipeline.read(do_lock=True)
        pipeline.set_name("deleted", new_lock=False)

        pipeline.delete_job(pipeline.process_list[1])

        with open("deleted_pipeline.star") as updated_pipeline:
            new_pipe = updated_pipeline.read()
        removed_lines = [
            "CtfFind/job003/micrographs_ctf.star {}".format(
                ctffind_job.OUTPUT_NODE_MICS
            ),
            "CtfFind/job003/logfile.pdf {}".format(ctffind_job.OUTPUT_NODE_LOG),
            "CtfFind/job003/ CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/ CtfFind/job003/logfile.pdf ",
            "MotionCorr/job002/ MotionCorr/own/   MotionCorr    Succeeded ",
            "CtfFind/job003/ CtfFind/gctf/      CtfFind    Succeeded ",
            "MotionCorr/job002/corrected_micrographs.star {}".format(
                motioncorr_job.OUTPUT_NODE_MICS
            ),
            "MotionCorr/job002/logfile.pdf {}".format(motioncorr_job.OUTPUT_NODE_LOG),
            "MotionCorr/job002/corrected_micrographs.star CtfFind/job003/",
            "MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/ MotionCorr/job002/logfile.pdf",
        ]
        for rl in removed_lines:
            assert rl not in new_pipe, rl
        assert not os.path.isfile("CtfFind/job003/logfile.pdf")
        assert not os.path.isfile("CtfFind/job003/micrographs_ctf.star")
        assert not os.path.isdir("CtfFind/job003/")

    def test_new_statis_same_as_current_error(self):
        # copy in pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_running_pipeline.star")

        # make the directry structure
        os.makedirs("CtfFind/job003/")
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        touch("CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        # read the pipeline
        pipeline = ProjectGraph(name="short_running")
        pipeline.read()

        # get process to mark finished
        fin_proc = pipeline.process_list[2]

        # mark it finished
        with self.assertRaises(ValueError):
            pipeline.update_status(fin_proc, JOBSTATUS_RUN)

    def test_mark_as_finished_nonrefinejob(self):
        # copy in pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_running_pipeline.star")

        # make the directry structure
        os.makedirs("CtfFind/job003/")
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        touch("CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        jstar = os.path.join(
            self.test_data, "JobFiles/CtfFind/tutorial_job003_job.star"
        )
        shutil.copy(jstar, "CtfFind/job003/job.star")

        # read the pipeline
        pipeline = ProjectGraph(name="short_running")
        pipeline.read()

        # get process to mark finished
        fin_proc = pipeline.process_list[2]

        # mark it finished
        mark_as_fin = pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)
        assert mark_as_fin

        # check pipeine lines were written as expected
        written = read_pipeline("short_running_pipeline.star")
        assert [
            "CtfFind/job003/",
            "CtfFind/gctf/",
            CTFFIND_GCTF_NAME,
            JOBSTATUS_SUCCESS,
        ] in written
        assert [
            "CtfFind/job003/",
            "CtfFind/gctf/",
            CTFFIND_GCTF_NAME,
            JOBSTATUS_RUN,
        ] not in written
        assert os.path.isfile("CtfFind/job003/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        pipeline = ProjectGraph(name="short_cl2d")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl2d_pipeline.star")
        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            ["Class2D/job008/run_it005_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Class2D/job008/run_it005_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/run_it005_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = ["Class2D/job008/", "Class2D/LoG_based/", "Class2D", "Running "]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_as_finished_inimodel(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_inimod_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_inimod_pipeline.star")

        dirs = [
            "Extract/job007",
            "InitialModel/job009",
        ]
        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("InitialModel/job009"),
            os.path.join(self.test_dir, "InitialModel/symC1"),
        )
        assert os.path.islink("InitialModel/symC1")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
            "_class001.mrc",
        ]
        for i in range(0, 103):
            for f in files:
                ff = "InitialModel/job009/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        in_files = [
            "Extract/job007/particles.star",
        ]

        for f in in_files:
            touch(f)
            assert os.path.isfile(f)

        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")

        pipeline = ProjectGraph(name="short_inimod")
        pipeline.read()

        fin_proc = pipeline.process_list[8]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_inimod_pipeline.star")

        newlines = [
            [
                "InitialModel/job009/",
                "InitialModel/symC1/",
                INIMODEL_JOB_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "InitialModel/job009/run_it102_optimiser.star",
                initialmodel_job.OUTPUT_NODE_OPT,
            ],
            [
                "InitialModel/job009/run_it102_data.star",
                initialmodel_job.OUTPUT_NODE_PARTS,
            ],
            [
                "InitialModel/job009/run_it102_class001.mrc",
                initialmodel_job.OUTPUT_NODE_INIMODEL,
            ],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_optimiser.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_data.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_optimiser.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_class001.mrc"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "InitialModel/job009/",
            "InitialModel/symC1/",
            INIMODEL_JOB_NAME,
            JOBSTATUS_RUN,
        ]

        assert removed_line not in written
        assert os.path.isfile("InitialModel/job009/" + SUCCESS_FILE)

    def test_mark_as_finished_class3d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl3d_pipeline.star")

        dirs = [
            "Class3D/job010",
            "InitialModel/job009",
            "Extract/job007",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )
        assert os.path.islink("Class3D/first_exhaustive")

        cl3_files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
            "_class001.mrc",
            "_class002.mrc",
            "_class003.mrc",
            "_class004.mrc",
        ]
        for i in range(0, 13):
            for f in cl3_files:
                ff = "Class3D/job010/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        in_files = [
            "InitialModel/job009/run_it150_class001.star",
            "Extract/job007/particles.star",
        ]
        for f in in_files:
            touch(f)
            assert os.path.isfile(f)

        jstar = os.path.join(
            self.test_data, "JobFiles/Class3D/tutorial_job016_job.star"
        )
        shutil.copy(jstar, "Class3D/job010/job.star")

        pipeline = ProjectGraph(name="short_cl3d")
        pipeline.read()
        fin_proc = pipeline.process_list[9]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl3d_pipeline.star")

        newlines = [
            [
                "Class3D/job010/",
                "Class3D/first_exhaustive/",
                CLASS3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["Class3D/job010/run_it012_optimiser.star", class3D_job.OUTPUT_NODE_OPT],
            ["Class3D/job010/run_it012_data.star", class3D_job.OUTPUT_NODE_PARTS],
            ["Class3D/job010/run_it012_class001.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it012_class002.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it012_class003.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it012_class004.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/", "Class3D/job010/run_it012_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_data.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class001.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class002.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class003.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class004.mrc"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class3D/job010/",
            "Class3D/first_exhaustive/",
            CLASS3D_PARTICLE_NAME,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written, removed_line

    def test_mark_as_finished_ref3d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_ref3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_ref3d_pipeline.star")

        dirs = [
            "Refine3D/job011",
            "Class3D/job010",
            "Extract/job007",
            "InitialModel/job008",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Refine3D/job011"),
            os.path.join(self.test_dir, "Refine3D/first3dref"),
        )
        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )

        assert os.path.islink("Refine3D/first3dref")

        in_files = [
            "InitialModel/job008/run_it150_class001.star",
            "Extract/job007/particles.star",
            "Class3D/job010/run_it025_class001.mrc",
            "Class3D/job010/run_it025_class002.mrc",
            "Class3D/job010/run_it025_class003.mrc",
            "Class3D/job010/run_it025_class004.mrc",
        ]
        for f in in_files:
            touch(f)
            assert os.path.isfile(f), f

        r3d_files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
            "_half1_class001.mrc",
            "_half2_class001.mrc",
        ]
        for i in range(0, 18):
            for f in r3d_files:
                ff = "Refine3D/job011/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
        jstar = os.path.join(
            self.test_data, "JobFiles/Refine3D/tutorial_job025_job.star"
        )
        shutil.copy(jstar, "Refine3D/job011/job.star")

        pipeline = ProjectGraph(name="short_ref3d")
        pipeline.read()

        fin_proc = pipeline.process_list[10]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_ref3d_pipeline.star")

        newlines = [
            [
                "Refine3D/job011/",
                "Refine3D/first3dref/",
                REFINE3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["Refine3D/job011/run_it017_optimiser.star", refine3D_job.OUTPUT_NODE_OPT],
            ["Refine3D/job011/run_it017_data.star", refine3D_job.OUTPUT_NODE_PARTS],
            [
                "Refine3D/job011/run_it017_half1_class001.mrc",
                refine3D_job.OUTPUT_NODE_HALFMAP,
            ],
            ["Extract/job007/particles.star", "Refine3D/job011/"],
            ["InitialModel/job009/run_it150_class001.mrc", "Refine3D/job011/"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_optimiser.star"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = ["Refine3D/job011/ Refine3D/first3dref/", "Refine3D", "Running"]
        assert removed_line not in written
        assert os.path.isfile("Refine3D/job011/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 14):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
        pipeline = ProjectGraph(name="short_cl2d")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl2d_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/run_it013_optimiser.star",
                class2D_job.OUTPUT_NODE_OPT,
            ],
            ["Class2D/job008/run_it013_data.star", class2D_job.OUTPUT_NODE_PARTS],
            [
                "Class2D/job008/run_it013_optimiser.star",
                class2D_job.OUTPUT_NODE_OPT,
            ],
            ["Class2D/job008/", "Class2D/job008/run_it013_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it013_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it013_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_as_finished_inimodel_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_inimod_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_inimod_pipeline.star")

        dirs = [
            "Extract/job007",
            "InitialModel/job009",
        ]
        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("InitialModel/job009"),
            os.path.join(self.test_dir, "InitialModel/symC1"),
        )
        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")
        assert os.path.islink("InitialModel/symC1")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
            "_class001.mrc",
        ]
        for i in range(0, 144):
            for f in files:
                ff = "InitialModel/job009/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        in_files = [
            "Extract/job007/particles.star",
        ]

        for f in in_files:
            touch(f)
            assert os.path.isfile(f)

        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")

        pipeline = ProjectGraph(name="short_inimod")
        pipeline.read()

        fin_proc = pipeline.process_list[8]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_inimod_pipeline.star")

        newlines = [
            [
                "InitialModel/job009/",
                "InitialModel/symC1/",
                INIMODEL_JOB_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "InitialModel/job009/run_it143_optimiser.star",
                initialmodel_job.OUTPUT_NODE_OPT,
            ],
            [
                "InitialModel/job009/run_it143_data.star",
                initialmodel_job.OUTPUT_NODE_PARTS,
            ],
            [
                "InitialModel/job009/run_it143_class001.mrc",
                initialmodel_job.OUTPUT_NODE_INIMODEL,
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_optimiser.star",
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_optimiser.star",
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_class001.mrc",
            ],
            ["InitialModel/job009/", "InitialModel/job009/run_it143_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "InitialModel/job009/",
            "InitialModel/symC1/",
            INIMODEL_JOB_NAME,
            JOBSTATUS_RUN,
        ]

        assert removed_line not in written
        assert os.path.isfile("InitialModel/job009/" + SUCCESS_FILE)

    def test_mark_as_finished_class3d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl3d_pipeline.star")

        dirs = [
            "Class3D/job010",
            "InitialModel/job009",
            "Extract/job007",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )
        assert os.path.islink("Class3D/first_exhaustive")

        cl3_files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
            "_class001.mrc",
            "_class002.mrc",
            "_class003.mrc",
            "_class004.mrc",
        ]
        for i in range(0, 20):
            for f in cl3_files:
                ff = "Class3D/job010/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
        for i in range(12, 20):
            for f in cl3_files:
                ff = "Class3D/job010/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
        in_files = [
            "InitialModel/job009/run_it150_class001.star",
            "Extract/job007/particles.star",
        ]
        for f in in_files:
            touch(f)
            assert os.path.isfile(f)

        jstar = os.path.join(
            self.test_data, "JobFiles/Class3D/tutorial_job016_job.star"
        )
        shutil.copy(jstar, "Class3D/job010/job.star")

        pipeline = ProjectGraph(name="short_cl3d")
        pipeline.read()

        fin_proc = pipeline.process_list[9]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl3d_pipeline.star")

        newlines = [
            [
                "Class3D/job010/",
                "Class3D/first_exhaustive/",
                CLASS3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class3D/job010/run_it019_optimiser.star",
                class3D_job.OUTPUT_NODE_OPT,
            ],
            ["Class3D/job010/run_it019_data.star", class3D_job.OUTPUT_NODE_PARTS],
            ["Class3D/job010/run_it019_class001.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it019_class002.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it019_class003.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/run_it019_class004.mrc", class3D_job.OUTPUT_NODE_MAP],
            ["Class3D/job010/", "Class3D/job010/run_it019_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it019_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class001.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class002.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class003.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class004.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class3D/job010/" "Class3D/first_exhaustive/",
            "9" "0",
        ]
        assert removed_line not in written
        assert os.path.isfile("Class3D/job010/" + SUCCESS_FILE)

    def test_mark_as_finished_ref3d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_ref3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_ref3d_pipeline.star")

        dirs = [
            "Refine3D/job011",
            "Class3D/job010",
            "Extract/job007",
            "InitialModel/job008",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Refine3D/job011"),
            os.path.join(self.test_dir, "Refine3D/first3dref"),
        )
        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )

        assert os.path.islink("Refine3D/first3dref")

        in_files = [
            "InitialModel/job008/run_it150_class001.star",
            "Extract/job007/particles.star",
            "Class3D/job010/run_it025_class001.mrc",
            "Class3D/job010/run_it025_class002.mrc",
            "Class3D/job010/run_it025_class003.mrc",
            "Class3D/job010/run_it025_class004.mrc",
        ]
        for f in in_files:
            touch(f)
            assert os.path.isfile(f), f

        r3d_files = [
            "_data.star",
            "_optimiser.star",
            "_half1_class001.mrc",
            "_half2_class001.mrc",
        ]
        for i in range(0, 18):
            for f in r3d_files:
                ff = "Refine3D/job011/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
        jstar = os.path.join(
            self.test_data, "JobFiles/Refine3D/tutorial_job025_job.star"
        )
        shutil.copy(jstar, "Refine3D/job011/job.star")

        pipeline = ProjectGraph(name="short_ref3d")
        pipeline.read()

        fin_proc = pipeline.process_list[10]

        pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_ref3d_pipeline.star")

        newlines = [
            [
                "Refine3D/job011/",
                "Refine3D/first3dref/",
                REFINE3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["Refine3D/job011/run_it017_optimiser.star", refine3D_job.OUTPUT_NODE_OPT],
            ["Refine3D/job011/run_it017_data.star", refine3D_job.OUTPUT_NODE_PARTS],
            [
                "Refine3D/job011/run_it017_half1_class001.mrc",
                refine3D_job.OUTPUT_NODE_HALFMAP,
            ],
            ["Extract/job007/particles.star", "Refine3D/job011/"],
            ["InitialModel/job009/run_it150_class001.mrc", "Refine3D/job011/"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_optimiser.star"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_data.star"],
        ]

        for line in newlines:
            assert line in written, line

        removed_line = [
            "Refine3D/job011/",
            "Refine3D/first3dref/",
            REFINE3D_PARTICLE_NAME,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written
        assert os.path.isfile("Refine3D/job011/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d_aborted(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_abort_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_abort_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        pipeline = ProjectGraph(name="short_cl2d_abort")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl2d_abort_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            ["Class2D/job008/run_it005_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Class2D/job008/run_it005_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, (line, written)

        removed_line = ["Class2D/job008/", "Class2D/LoG_based/", "Class2D", "Aborted"]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_failed_job_aborted_error(self):
        """Take a job that is currently failed and try to mark it aborted
        should raise error"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_fail_pipeline.star"),
            self.test_dir,
        )
        os.makedirs("Class2D/job008")
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.isfile("short_cl2d_fail_pipeline.star")

        pipeline = ProjectGraph(name="short_cl2d_fail")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        with self.assertRaises(ValueError):
            pipeline.update_status(fin_proc, JOBSTATUS_ABORT)

    def test_mark_class2d_failed(self):
        """Take a job that is currently failed and mark it as finished"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_fail_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_fail_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")

        pipeline = ProjectGraph(name="short_cl2d_fail")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = read_pipeline("short_cl2d_fail_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            ["Class2D/job008/run_it005_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Class2D/job008/run_it005_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_FAIL,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_class2d_mark_aborted_as_failed(self):
        """Take an aborted job and mark it as failed"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_abort_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_abort_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        pipeline = ProjectGraph(name="short_cl2d_abort")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_FAIL)

        written = read_pipeline("short_cl2d_abort_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_FAIL,
            ],
        ]
        for line in newlines:
            assert line in written, (line, written)

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_ABORT,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + FAIL_FILE)
        assert not os.path.isfile("Class2D/job008/" + ABORT_FILE)

    def test_mark_as_aborted_class2d_running(self):
        """Take a job marked as running and mark it aborted"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff

        pipeline = ProjectGraph(name="short_cl2d")
        pipeline.read()

        fin_proc = pipeline.process_list[7]

        assert pipeline.update_status(fin_proc, JOBSTATUS_ABORT)

        written = read_pipeline("short_cl2d_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_ABORT,
            ],
        ]

        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + ABORT_FILE)

    def test_delete_job(self):
        # opy over files
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_del_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_del_pipeline.star")

        # make the files and their symlinks for aliases
        fdirs = {
            "Class2D/job008": "Class2D/LoG_based",
            "Extract/job007": "Extract/LoG_based",
        }
        for fdir in fdirs:
            os.makedirs(fdir)
            os.symlink(
                os.path.abspath(fdir), os.path.join(self.test_dir, fdirs[fdir]), True
            )
            assert os.path.isdir(fdir)
            assert os.path.islink(fdirs[fdir])

        cl2d_files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        file_list = list()
        for i in range(0, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
                file_list.append(ff)

        cl2d_other_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]

        for f in cl2d_other_files:
            ff = "Class2D/job008/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)

        extract_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "particles.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]

        for f in extract_files:
            ff = "Extract/job007/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)

        moviedir = "Extract/job007/Movies"
        os.makedirs(moviedir)
        assert os.path.isdir("Extract/job007/Movies")

        for i in range(1, 11):
            f = moviedir + "/movie_parts{:03d}.mrcs".format(i)
            touch(f)
            assert os.path.isfile(f)
            file_list.append(f)

        # make to .Nodes files
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS
            ),
            ".Nodes/{}/Class2D/job008/run_it025_data.star".format(
                class2D_job.OUTPUT_NODE_PARTS
            ),
        ]
        for f in nodes_files:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            touch(f)

        # read the pipeline
        pipeline = ProjectGraph(name="short_cl2d_del")
        pipeline.read(lock_wait=0.1)

        # get thr process to delete
        del_proc = pipeline.process_list[6]
        pipeline.delete_job(del_proc)

        # makesure the lines have been removed from the pipeline
        written = read_pipeline("short_cl2d_del_pipeline.star")

        removed_lines = [
            [
                "Extract/job007/ Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/ Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            ["Extract/job007/particles.star", extract_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/run_it025_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        for removed_line in removed_lines:
            assert removed_line not in written, removed_line

        # make sure the files are gone
        for f in file_list:
            assert not os.path.isfile(f), f
            trashname = "Trash/" + f
            assert os.path.isfile(trashname), trashname

        # make sure the .Nodes entries are gone
        for f in nodes_files:
            assert not os.path.isfile(f), f
            dirname = os.path.dirname(f)
            assert not os.path.isdir(dirname), dirname

    def make_alias_file_structure(self, pipe_name, alias):
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/{}_pipeline.star".format(pipe_name)
            ),
            self.test_dir,
        )

        assert os.path.isfile("{}_pipeline.star".format(pipe_name))

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        if alias is not None:
            os.symlink(
                os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
            )
            assert os.path.islink("Class2D/LoG_based")

        real_files = [
            "Class2D/job008/run_it025_optimiser.star",
            "Class2D/job008/run_it025_data.star",
        ]
        for f in real_files:
            touch(f)

        outnodes = [class2D_job.OUTPUT_NODE_PARTS, class2D_job.OUTPUT_NODE_OPT]
        if alias is None:
            alias = "job008"
        for node in outnodes:
            onode = "{}{}/Class2D/{}".format(NODES_DIR, node, alias)
            os.makedirs(onode)
            assert os.path.isdir(onode)

        files = [
            "{}{}/Class2D/{}/run_it025_optimiser.star".format(
                NODES_DIR, class2D_job.OUTPUT_NODE_OPT, alias
            ),
            "{}{}/Class2D/{}/run_it025_data.star".format(
                NODES_DIR, class2D_job.OUTPUT_NODE_PARTS, alias
            ),
        ]
        for f in files:
            touch(f)
            assert os.path.isfile(f), f

    def test_change_job_alias(self):

        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        pipeline = ProjectGraph(name="short_cl2d_finished")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        pipeline.set_job_alias(change_proc, "pretty_new_one")

        assert os.path.islink(os.path.join(self.test_dir, "Class2D/pretty_new_one"))
        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        oldnodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/LoG_based/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/LoG_based/run_it025_optimiser.star",
        ]

        new_nodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/pretty_new_one/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/pretty_new_one/run_it025_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = read_pipeline("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "Class2D/pretty_new_one/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line

    def test_change_job_alias_to_none(self):

        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        pipeline = ProjectGraph(name="short_cl2d_finished")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        pipeline.set_job_alias(change_proc, "")

        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        oldnodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/LoG_based/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/LoG_based/run_it025_optimiser.star",
        ]

        new_nodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/job008/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/job008/run_it025_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = read_pipeline("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "None",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line

    def test_change_job_alias_from_none(self):

        self.make_alias_file_structure("short_cl2d_noalias", None)

        pipeline = ProjectGraph(name="short_cl2d_noalias")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        pipeline.set_job_alias(change_proc, "from_none")

        new_nodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/from_none/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/from_none/run_it025_optimiser.star",
        ]

        old_nodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            + "/Class2D/job008/run_it025_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            + "/Class2D/job008/run_it025_optimiser.star",
        ]

        for n in old_nodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = read_pipeline("short_cl2d_noalias_pipeline.star")

        new_line = [
            "Class2D/job008/",
            "Class2D/from_none/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        removed_line = [
            "Class2D/job008/",
            "None",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line

    def test_job_alias_illegal_names(self):

        self.make_alias_file_structure("short_cl2d_noalias", None)

        pipeline = ProjectGraph(name="short_cl2d_noalias")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        badnames = [
            "l",
            "JAH!!",
            "*nice*",
            "lala&",
            "(best",
            "worst)",
            "#HASH|",
            "pointy^^",
            "'quotes'",
            "'quotes'",
            "job420",
            "{left",
            "right}",
            ",comma",
            "100%",
            "any_more?",
            "\\slash",
            "otherslash/",
        ]
        for badname in badnames:
            with self.assertRaises(ValueError):
                pipeline.set_job_alias(change_proc, badname)

    def test_change_job_alias_not_unique(self):
        """Test trying to use a non unique alias raises and error"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        pipeline = ProjectGraph(name="short_cl2d_finished")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        with self.assertRaises(ValueError):
            pipeline.set_job_alias(change_proc, "LoG_based")

    def test_change_job_alias_not_unique_after_fixing_space(self):
        """Test the case where removing an illegal space makes the
        alias not unique"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        pipeline = ProjectGraph(name="short_cl2d_finished")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        with self.assertRaises(ValueError):
            pipeline.set_job_alias(change_proc, "LoG based")

    def test_change_job_alias_spaces_are_OK(self):
        """Test automatically replacing spaces in aliases with _"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        pipeline = ProjectGraph(name="short_cl2d_finished")
        pipeline.read()

        change_proc = pipeline.process_list[7]
        pipeline.set_job_alias(change_proc, "pretty new one")

        assert os.path.islink(os.path.join(self.test_dir, "Class2D/pretty_new_one"))
        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        twod_parts = class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
        twod_opt = class2D_job.OUTPUT_NODE_OPT.split(".")[0]
        oldnodes = [
            twod_parts + "/Class2D/LoG_based/run_it025_data.star",
            twod_opt + "/Class2D/LoG_based/run_it025_optimiser.star",
        ]
        dirname = "/Class2D/pretty_new_one/run_it025"
        new_nodes = [
            class2D_job.OUTPUT_NODE_PARTS.split(".")[0] + dirname + "_data.star",
            class2D_job.OUTPUT_NODE_OPT.split(".")[0] + dirname + "_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = read_pipeline("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "Class2D/pretty_new_one/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line

    def make_undelete_file_structure(self):
        # make the directories needed
        dirs = [
            "Class2D/job008",
            "Extract/job007",
            "Trash",
        ]

        for d in dirs:
            os.makedirs(d)
            assert os.path.isdir(d), d

        # make files common to all run types
        common_files = [
            "run.out",
            "run.err",
            "note.txt",
            "run.job",
            "default_pipeline.star",
            SUCCESS_FILE,
        ]

        outfiles = list()
        for d in dirs[:-1]:
            for f in common_files:
                fn = d + "/" + f
                touch(fn)
                assert os.path.isfile(fn), fn
                outfiles.append(fn)

        # copy in the pipeline
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_cl2d_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Class2D/job008/job_pipeline.star"),
        )
        # add the specific files for each run
        outfiles.append("Class2D/job008/job_pipeline.star")

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extract_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Extract/job007/job_pipeline.star"),
        )
        outfiles.append("Extract/job007/job_pipeline.star")

        touch("Extract/job007/particles.star")
        assert os.path.isfile("Extract/job007/particles.star")
        outfiles.append("Extract/job007/particles.star")

        os.makedirs("Extract/job007/Movies")
        assert os.path.isdir("Extract/job007/Movies")

        # make particles files
        for i in range(1, 11):
            f = "Extract/job007/Movies/movie_{:03d}.mrcs".format(i)
            touch(f)
            assert os.path.isfile(f), f
            outfiles.append(f)

        # make class2d files
        cl2d_files = ["_data.star", "_optimiser.star", "_optimiser.star"]
        for i in range(1, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{:03d}{}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff)
                outfiles.append(ff)

        shutil.move("Class2D", "Trash/Class2D")
        shutil.move("Extract", "Trash/Extract")

        assert not os.path.isdir("Class2D")
        assert not os.path.isdir("Extract")
        assert os.path.isdir("Trash/Class2D")
        assert os.path.isdir("Trash/Extract")

        return outfiles

    def test_undelete_single(self):
        # make the file structure like jobs have been run
        outfiles = self.make_undelete_file_structure()

        # copy in the pipeline
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        # initialize the project
        pipeline = ProjectGraph(name="for_undelete_deleted")
        pipeline.read()

        # lines that should be restored
        restored_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure the lines to be undelete are not in the pipeline
        original = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # undelete the job
        pipeline.undelete_job("Extract/job007/")

        # make sure the undeleted lines are back in the pipeline
        wrote = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            ".Nodes/{}/Extract/LoG_based/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS.split(".")[0]
            ),
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

        # make sure the alias is restored
        assert os.path.islink("Extract/LoG_based")

    def test_undelete_single_no_alias(self):
        # set up file structure
        outfiles = self.make_undelete_file_structure()

        # get pipelines
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_noalias_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Trash/Extract/job007/job_pipeline.star"),
        )

        # load and read pipeline
        pipeline = ProjectGraph(name="for_undelete_deleted")
        pipeline.read()

        # lines to be restored
        restored_lines = [
            ["Extract/job007/", "None", EXTRACT_PARTICLE_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure lines to be restored are not in the pipeline
        original = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # undelete the extrat job
        pipeline.undelete_job("Extract/job007/")

        # make sure the lines have been restored to the pipeline
        wrote = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS.split(".")[0]
            ),
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    def test_undelete_single_alias_not_unique(self):
        #  make file structure
        outfiles = self.make_undelete_file_structure()

        # copy in the pipelines
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extra_alias_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extract_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Trash/Extract/job007/job_pipeline.star"),
        )

        # load and read pipeline
        pipeline = ProjectGraph(name="for_undelete_extra_alias")
        pipeline.read()

        #  lines to be restored
        restored_lines = [
            ["Extract/job007/", "None", EXTRACT_PARTICLE_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure lines to be restored are not in pipeline
        original = read_pipeline("for_undelete_extra_alias_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # undelete the extract job
        pipeline.undelete_job("Extract/job007/")

        # make sure the lines are back in  the pipeline
        wrote = read_pipeline("for_undelete_extra_alias_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS.split(".")[0]
            ),
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    def test_undelete_process_with_parents(self):
        outfiles = self.make_undelete_file_structure()

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        pipeline = ProjectGraph(name="for_undelete_deleted")
        pipeline.read()

        restored_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/run_it025_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Class2D/job008/run_it025_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        original = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        pipeline.undelete_job("Class2D/job008/")

        wrote = read_pipeline("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        for f in outfiles:
            assert os.path.isfile(f), f

        assert os.path.islink("Extract/LoG_based")
        assert os.path.islink("Class2D/LoG_based")

    def test_delete_job_then_undelete(self):
        # create the file structure - move things out of the trash
        # like they weren't deleted

        outfiles = self.make_undelete_file_structure()
        shutil.move("Trash/Class2D", "Class2D")
        shutil.move("Trash/Extract", "Extract")
        os.symlink(
            os.path.abspath("Extract/job007"),
            os.path.join(self.test_dir, "Extract/LoG_based"),
            True,
        )
        os.symlink(
            os.path.abspath("Class2D/job008"),
            os.path.join(self.test_dir, "Class2D/LoG_based"),
            True,
        )

        # make the .Nodes entries
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS
            ),
            ".Nodes/{}/Class2D/job008/run_it025_optimiser.star".format(
                class2D_job.OUTPUT_NODE_OPT
            ),
            ".Nodes/{}/Class2D/job008/run_it025_data.star".format(
                class2D_job.OUTPUT_NODE_PARTS
            ),
        ]
        for f in nodes_files:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            touch(f)

        # copy in the pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_del_pipeline.star"),
            self.test_dir,
        )

        # open and read the pipeline
        pipeline = ProjectGraph(name="short_cl2d_del")
        pipeline.read(lock_wait=0.1)

        # delete the process
        del_proc = pipeline.process_list[6]
        pipeline.delete_job(del_proc)
        # open the newly written pipeline
        written = read_pipeline("short_cl2d_del_pipeline.star")

        removed_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            ["Extract/job007/particles.star", extract_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/run_it025_data.star", class2D_job.OUTPUT_NODE_PARTS],
            ["Class2D/job008/run_it025_optimiser.star", class2D_job.OUTPUT_NODE_OPT],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        # make sure everything is deleted as expected
        for line in removed_lines:
            assert line not in written, line

        # makes sure the .Nodes entries are not there
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS
            ),
            ".Nodes/{}/Class2D/job008/run_it025_optimiser.star".format(
                class2D_job.OUTPUT_NODE_OPT
            ),
            ".Nodes/{}/Class2D/job008/run_it025_data.star".format(
                class2D_job.OUTPUT_NODE_PARTS
            ),
        ]
        for f in nodes_files:
            assert not os.path.isfile(f), f

        # make sure the files are in the trash
        for f in outfiles:
            assert not os.path.isfile(f), f
            trashname = "Trash/" + f
            assert os.path.isfile(trashname), trashname

        # undelete the job that has a parent
        pipeline.undelete_job("Class2D/job008/")
        # make sure the lines are restored to the pipeline
        restored = read_pipeline("short_cl2d_del_pipeline.star")
        for line in removed_lines:
            assert line in restored, (line, restored)

        # make sure the files are back
        for f in outfiles:
            assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            ".Nodes/{}/Extract/LoG_based/particles.star".format(
                extract_job.OUTPUT_NODE_PARTS.split(".")[0]
            ),
            ".Nodes/{}/Class2D/LoG_based/run_it025_optimiser.star".format(
                class2D_job.OUTPUT_NODE_OPT.split(".")[0]
            ),
            ".Nodes/{}/Class2D/LoG_based/run_it025_data.star".format(
                class2D_job.OUTPUT_NODE_PARTS.split(".")[0]
            ),
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    @unittest.skipUnless(has_relion, "Relion needed for test")
    def test_check_pipeline_entries(self):
        """Make sure a bug where input and output edges are duplicated
        is not occurring - any node that was used as an input to another
        process was duplicated in input and output edges"""

        # copy in the map
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the import job
        pipeline = JobRunner()
        map_job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        pipeline.run_job(map_job, None, False, False, False)

        # run the mask create job
        mask_job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MaskCreate/maskcreate.job")
        )
        pipeline.run_job(mask_job, None, False, False, False)
        time.sleep(0.5)

        # copy in the halfmaps
        halfmap_import_dir = "Halfmaps"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )

        # run the postprocess job
        PP_job = job_factory.read_job(
            os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess_halfmapdir.job"
            )
        )
        pipeline.run_job(PP_job, None, False, False, False)
        time.sleep(1)

        duplicated_lines = [
            ["Import/job001/emd_3488.mrc", "MaskCreate/job002/"],
            ["MaskCreate/job002/mask.mrc", "PostProcess/job003/"],
            ["Import/job001/", "Import/job001/emd_3488.mrc"],
            ["MaskCreate/job002/", "MaskCreate/job002/mask.mrc"],
        ]

        # make sure the duplicated nodes are not there
        dups = 0
        pipe_data = read_pipeline("default_pipeline.star")

        for line in duplicated_lines:
            for pipe_line in pipe_data:
                if line in pipe_line:
                    dups += 1
            assert dups <= 1, line
            dups = 0

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_getting_results_display_finished_nofile(self):
        get_relion_tutorial_data("Class2D")
        mf = "Class2D/job013/run_it100_model.star"
        pf = "Class2D/job013/run_it100_data.star"

        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class2D/job013/")
        dispobjs = pipeline.get_process_results_display(proc)

        ddata = dispobjs[0]

        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 10
        expy = sum([[n] * 10 for n in reversed(range(10))], [])
        explabels = [
            "Class 61; 2.61%; 239 particles",
            "Class 32; 2.57%; 235 particles",
            "Class 94; 2.46%; 225 particles",
            "Class 3; 2.40%; 220 particles",
            "Class 57; 2.34%; 215 particles",
            "Class 46; 2.28%; 209 particles",
            "Class 51; 2.20%; 202 particles",
            "Class 27; 2.14%; 196 particles",
            "Class 36; 2.11%; 193 particles",
            "Class 60; 2.11%; 193 particles",
            "Class 96; 2.00%; 183 particles",
            "Class 65; 1.97%; 180 particles",
            "Class 73; 1.94%; 178 particles",
            "Class 53; 1.89%; 173 particles",
            "Class 37; 1.87%; 171 particles",
            "Class 20; 1.87%; 171 particles",
            "Class 5; 1.83%; 168 particles",
            "Class 7; 1.76%; 161 particles",
            "Class 52; 1.74%; 160 particles",
            "Class 90; 1.72%; 157 particles",
            "Class 18; 1.71%; 157 particles",
            "Class 66; 1.65%; 152 particles",
            "Class 14; 1.65%; 151 particles",
            "Class 17; 1.59%; 146 particles",
            "Class 39; 1.58%; 145 particles",
            "Class 47; 1.54%; 141 particles",
            "Class 64; 1.53%; 140 particles",
            "Class 31; 1.46%; 134 particles",
            "Class 29; 1.42%; 130 particles",
            "Class 42; 1.42%; 130 particles",
            "Class 84; 1.39%; 128 particles",
            "Class 68; 1.39%; 128 particles",
            "Class 99; 1.37%; 125 particles",
            "Class 67; 1.35%; 123 particles",
            "Class 82; 1.33%; 122 particles",
            "Class 80; 1.30%; 119 particles",
            "Class 79; 1.28%; 117 particles",
            "Class 75; 1.27%; 117 particles",
            "Class 21; 1.26%; 115 particles",
            "Class 11; 1.25%; 115 particles",
            "Class 25; 1.25%; 114 particles",
            "Class 55; 1.24%; 114 particles",
            "Class 62; 1.22%; 112 particles",
            "Class 89; 1.22%; 112 particles",
            "Class 81; 1.19%; 109 particles",
            "Class 41; 1.16%; 106 particles",
            "Class 19; 1.16%; 106 particles",
            "Class 13; 1.15%; 105 particles",
            "Class 30; 1.15%; 105 particles",
            "Class 33; 1.12%; 102 particles",
            "Class 98; 1.04%; 95 particles",
            "Class 87; 1.02%; 93 particles",
            "Class 83; 1.01%; 92 particles",
            "Class 76; 1.01%; 92 particles",
            "Class 86; 1.00%; 92 particles",
            "Class 0; 0.99%; 91 particles",
            "Class 92; 0.95%; 87 particles",
            "Class 8; 0.91%; 83 particles",
            "Class 38; 0.90%; 83 particles",
            "Class 74; 0.89%; 81 particles",
            "Class 9; 0.87%; 79 particles",
            "Class 50; 0.85%; 78 particles",
            "Class 85; 0.82%; 75 particles",
            "Class 15; 0.78%; 71 particles",
            "Class 4; 0.75%; 68 particles",
            "Class 69; 0.75%; 68 particles",
            "Class 56; 0.72%; 65 particles",
            "Class 1; 0.43%; 39 particles",
            "Class 24; 0.34%; 31 particles",
            "Class 91; 0.32%; 29 particles",
            "Class 34; 0.31%; 28 particles",
            "Class 12; 0.23%; 21 particles",
            "Class 78; 0.10%; 8 particles",
            "Class 54; 0.09%; 8 particles",
            "Class 16; 0.08%; 7 particles",
            "Class 23; 0.06%; 5 particles",
            "Class 49; 0.06%; 5 particles",
            "Class 48; 0.05%; 4 particles",
            "Class 28; 0.03%; 2 particles",
            "Class 63; 0.03%; 2 particles",
            "Class 88; 0.02%; 2 particles",
            "Class 45; 0.02%; 1 particles",
            "Class 44; 0.02%; 1 particles",
            "Class 70; 0.02%; 1 particles",
            "Class 6; 0.02%; 1 particles",
            "Class 97; 0.02%; 1 particles",
            "Class 43; 0.02%; 1 particles",
            "Class 72; 0.02%; 1 particles",
            "Class 71; 0.01%; 0 particles",
            "Class 26; 0.01%; 0 particles",
            "Class 95; 0.01%; 0 particles",
            "Class 77; 0.01%; 0 particles",
            "Class 22; 0.00%; 0 particles",
            "Class 35; 0.00%; 0 particles",
            "Class 93; 0.00%; 0 particles",
            "Class 2; 0.00%; 0 particles",
            "Class 40; 0.00%; 0 particles",
            "Class 58; 0.00%; 0 particles",
            "Class 10; 0.00%; 0 particles",
            "Class 59; 0.00%; 0 particles",
        ]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels == explabels
        assert ddata.img == "Class2D/job013/Thumbnails/2dclass_montage.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf, pf]
        assert ddata.title == "2D class averages"
        assert os.path.isfile(
            os.path.join(proc.name, ".results_display_montage_001.json")
        )
        assert os.path.isfile("Class2D/job013/Thumbnails/2dclass_montage.png")

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_getting_results_display_finished_has_file(self):
        get_relion_tutorial_data("Class2D")
        results = os.path.join(self.test_data, "ResultsFiles/class2d_projectgraph.json")
        shutil.copy(results, "Class2D/job013/.results_display_montage_001.json")
        mf = "Class2D/job013/run_it100_model.star"
        pf = "Class2D/job013/run_it100_data.star"

        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class2D/job013/")
        dispobjs = pipeline.get_process_results_display(proc)

        ddata = dispobjs[0]

        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 10
        expy = sum([[n] * 10 for n in reversed(range(10))], [])
        explabels = ["Class 61; 2.61%; 239 particles", "Class 59; 0.00%; 0 particles"]
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-1] == explabels[1]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.img == "Class2D/job013/Thumbnails/2dclass_montage.png"
        assert ddata.associated_data == [mf, pf]
        assert ddata.title == "2D class averages"

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_getting_results_display_finished_has_pending_file(self):
        """Make sure existing results display files are being cleared when new ones
        are being made"""
        get_relion_tutorial_data("Class2D")
        results = os.path.join(self.test_data, "ResultsFiles/class2d_projectgraph.json")
        shutil.copy(results, "Class2D/job013/.results_display_montage_001.json")
        pending = os.path.join(self.test_data, "ResultsFiles/pending_results_file.json")
        shutil.copy(pending, "Class2D/job013/.results_display_pending_002.json")

        displayfiles = glob("Class2D/job013/.results_display*")
        assert len(displayfiles) == 2

        mf = "Class2D/job013/run_it100_model.star"
        pf = "Class2D/job013/run_it100_data.star"

        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class2D/job013/")
        dispobjs = pipeline.get_process_results_display(proc)

        displayfiles = glob("Class2D/job013/.results_display*")
        assert displayfiles == ["Class2D/job013/.results_display_montage_001.json"]

        ddata = dispobjs[0]
        print(ddata.__dict__)

        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 10
        expy = sum([[n] * 10 for n in reversed(range(10))], [])
        explabels = ["Class 61; 2.61%; 239 particles", "Class 59; 0.00%; 0 particles"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-1] == explabels[1]
        assert ddata.img == "Class2D/job013/Thumbnails/2dclass_montage.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf, pf]
        assert ddata.title == "2D class averages"


if __name__ == "__main__":
    unittest.main()
