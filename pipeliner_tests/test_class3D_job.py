#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)

from pipeliner.jobs.relion.class3D_job import (
    INPUT_NODE_PARTS,
    INPUT_NODE_PARTS_HELICAL,
    INPUT_NODE_REF3D,
    INPUT_NODE_MASK,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS_HELIX,
    OUTPUT_NODE_OPT,
    OUTPUT_NODE_MAP,
)

from pipeliner.star_writer import COMMENT_LINE


class Class3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ["RELION_SCRATCH_DIR"] = ""

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Class3D/class3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Class3D/class3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_class3D_basic(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_with_symmetry(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_c4sym.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP + ".c4sym",
                "run_it025_class002.mrc": OUTPUT_NODE_MAP + ".c4sym",
                "run_it025_class003.mrc": OUTPUT_NODE_MAP + ".c4sym",
                "run_it025_class004.mrc": OUTPUT_NODE_MAP + ".c4sym",
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C4 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_multi(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_multi.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
                "reference2": INPUT_NODE_REF3D,
                "reference3": INPUT_NODE_REF3D,
                "reference4": INPUT_NODE_REF3D,
                "reference5": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
                "run_it025_class005.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 5 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "reference2\n",
            "reference3\n",
            "reference4\n",
            "reference5\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_multi_error_not_enough_refs(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "Class3D",
                "class3D_multi_toofew.job",
                11,
                {},
                {},
                [],
            )

    def test_get_command_class3D_jobstar(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_job.star",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_mask(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_mask.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
                "MaskCreate/job012/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_2masks(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_2masks.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
                "MaskCreate/job012/mask.mrc": INPUT_NODE_MASK,
                "MaskCreate/job013/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--solvent_mask2 MaskCreate/job013/mask.mrc "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_mask_absgrey(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_mask_noabsgrey.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
                "MaskCreate/job012/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --firstiter_cc"
                " --ini_high 50.0 --dont_combine_weights_via_disc --preread_images"
                " --pool 30 --pad 2 --skip_gridding --ctf --iter "
                "25 --tau2_fudge 4 --particle_diameter 200 --K 4 --flatten_solvent "
                "--zero_mask --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_noctfref(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_noctfcorref.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_ctfintact(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_ctfintact.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --ctf_intact_first_peak"
                " --iter 25 --tau2_fudge 4 --particle_diameter 200 --K 4 "
                "--flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --offset_range 5 --offset_step 2.0"
                " --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_nozeromask(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_nozero.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_hireslim(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_hireslim.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --strict_highres_exp 12.0 --oversampling 1 --healpix_order 2 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j 6 "
                "--gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_dfferent_sampling(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_diffsamp.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 3 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_local(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_local.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --sigma_ang 1.66667 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_coarse(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_coarse.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --allow_coarser_sampling --sym C1 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_multiref(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical_multi.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
                "reference2": INPUT_NODE_REF3D,
                "reference3": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 3 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )
        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "reference2\n",
            "reference3\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_helical_noTPF(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical_noTPF.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_nosym(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical_nosym.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --ignore_helical_symmetry "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_search(self):
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical_search.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_search_relionstyle_name(self):
        """Test automatuic conversion of ambiguous relion style name"""
        general_get_command_test(
            self,
            "Class3D",
            "class3D_helical_search_relionstyle.job",
            11,
            {
                "Select/job014/particles.star": INPUT_NODE_PARTS_HELICAL,
                "InitialModel/job015/run_it150_class001_symD2.mrc": INPUT_NODE_REF3D,
            },
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_it025_class001.mrc": OUTPUT_NODE_MAP,
                "run_it025_class002.mrc": OUTPUT_NODE_MAP,
                "run_it025_class003.mrc": OUTPUT_NODE_MAP,
                "run_it025_class004.mrc": OUTPUT_NODE_MAP,
            },
            [
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_cl3d(self):
        get_relion_tutorial_data("Class3D")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class3D/job016/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = [
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class001.mrc"],
                "maps_opacity": [1.0],
                "models": [],
                "title": "Map: Class3D/job016/run_it025_class001.mrc",
                "maps_data": "25.173333 Å; 154 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class001.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class002.mrc"],
                "maps_opacity": [1.0],
                "models": [],
                "title": "Map: Class3D/job016/run_it025_class002.mrc",
                "maps_data": "20.596363 Å; 736 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class002.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class003.mrc"],
                "maps_opacity": [1.0],
                "models": [],
                "title": "Map: Class3D/job016/run_it025_class003.mrc",
                "maps_data": "25.173333 Å; 173 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class003.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class004.mrc"],
                "maps_opacity": [1.0],
                "models": [],
                "title": "Map: Class3D/job016/run_it025_class004.mrc",
                "maps_data": "9.850435 Å; 4747 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class004.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
            },
        ]
        dist_results = os.path.join(
            self.test_data, "ResultsFiles/class3d_dist_results.json"
        )
        with open(dist_results, "r") as dr:
            expected.append(json.load(dr))

        do_dicts = [x.__dict__ for x in dispobjs]
        for i in do_dicts:
            assert i in expected
        for i in expected:
            assert i in do_dicts


if __name__ == "__main__":
    unittest.main()
