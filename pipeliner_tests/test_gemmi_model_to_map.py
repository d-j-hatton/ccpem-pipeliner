#
#     Copyright (C) 2022 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("gemmi") is None else False
plugin_present = generic_tests.check_for_plugin("gemmi_model_to_map_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class GemmmiModelToMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="gemmi_model_to_map")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_model_to_map(self):
        proc = job_running_test(
            os.path.join(self.test_data, "JobFiles/Gemmi/gemmi_model_to_map.job"),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            ["run.out", "run.err", "5me2_a_10.0.mrc"],
            show_contents=True,
            print_err=True,
            print_run=True,
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        expected_display = {
            "maps": ["GemmiModelToMap/job998/Thumbnails/5me2_a_10.0.mrc"],
            "maps_opacity": [0.5],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "Map from model",
            "maps_data": "GemmiModelToMap/job998/5me2_a_10.0.mrc",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": [
                "GemmiModelToMap/job998/5me2_a_10.0.mrc",
                "Import/job001/5me2_a.pdb",
            ],
        }
        for key in expected_display.keys():
            assert expected_display[key] == dispobjs[-1].__dict__[key]


if __name__ == "__main__":
    unittest.main()
