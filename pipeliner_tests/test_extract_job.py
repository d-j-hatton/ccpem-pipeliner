#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.jobs.relion.extract_job import (
    INPUT_NODE_MICS,
    INPUT_NODE_COORDS,
    INPUT_NODE_COORDS_HELIX,
    INPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS_HELIX,
    OUTPUT_NODE_COORDS_HELIX,
    OUTPUT_NODE_COORDS,
)


class ExtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_basic(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_basic.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS,
            },
            {"particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_jobstar(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_job.star",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS,
            },
            {"particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_FOM(self):
        """Extract using the selection based on FOM added in relion 4.0"""
        general_get_command_test(
            self,
            "Extract",
            "extract_onFOM_job.star",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS,
            },
            {"particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --minimum_pick_fom 0.1 --pipeline_control"
                " Extract/job007/"
            ],
        )

    def test_get_command_rextract_wrongfile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self, "Extract", "extract_rextract_wrongfile.job", 7, 2, 1, ""
            )

    def test_get_command_rextract(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_rextract.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "Refine3D/job200/run_data.star": OUTPUT_NODE_PARTS,
            },
            {
                "particles.star": OUTPUT_NODE_PARTS,
                "reextract.star": OUTPUT_NODE_COORDS,
            },
            [
                "mpirun -n 16 relion_preprocess_mpi --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 25"
                " --white_dust -1 --black_dust -1 --invert_contrast "
                "--rextract_on_this_one --pipeline_control Extract/job007/",
            ],
        )

    def test_get_command_rextract_recenter(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_rextract_recenter.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "Refine3D/job200/run_data.star": INPUT_NODE_PARTS,
            },
            {
                "particles.star": OUTPUT_NODE_PARTS,
                "reextract.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--recenter --recenter_x 10 --recenter_y 20 --recenter_z 30 "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --norm --bg_radius 125"
                " --white_dust -1 --black_dust -1 --invert_contrast"
                " --pipeline_control Extract/job007/",
            ],
        )

    def test_get_command_rextract_setbgdia(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_rextract_set_bgdia.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "Refine3D/job200/run_data.star": INPUT_NODE_PARTS,
            },
            {
                "particles.star": OUTPUT_NODE_PARTS,
                "reextract.star": OUTPUT_NODE_COORDS,
            },
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 24"
                " --white_dust -1 --black_dust -1 --invert_contrast --pipeline_control"
                " Extract/job007/",
            ],
        )

    def test_get_command_helical(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_helical.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS_HELIX,
            },
            {
                "particles.star": OUTPUT_NODE_PARTS_HELIX,
                "helix_segments.star": OUTPUT_NODE_COORDS_HELIX,
            },
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_helical_relionstyle_name(self):
        """Test automatic conversion of ambiguous relion style name"""
        general_get_command_test(
            self,
            "Extract",
            "extract_helical_relionstyle.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS_HELIX,
            },
            {
                "particles.star": OUTPUT_NODE_PARTS_HELIX,
                "helix_segments.star": OUTPUT_NODE_COORDS_HELIX,
            },
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_helical_nosegs(self):
        general_get_command_test(
            self,
            "Extract",
            "extract_helical_nosegs.job",
            7,
            {
                "Select/job005/micrographs_selected.star": INPUT_NODE_MICS,
                "AutoPick/job006/autopick.star": INPUT_NODE_COORDS_HELIX,
            },
            {"particles.star": OUTPUT_NODE_PARTS_HELIX},
            [
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --pipeline_control "
                "Extract/job007/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_extract_generate_display_data(self):
        get_relion_tutorial_data(["Extract"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Extract/job007/")
        dispobjs = pipeline.get_process_results_display(proc)

        expres = os.path.join(self.test_data, "ResultsFiles/extract_results.json")
        print(dispobjs[0].__dict__)
        with open(expres, "r") as er:
            expected = json.load(er)

        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_reextract_generate_display_data(self):
        get_relion_tutorial_data(["Extract"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Extract/job018/")
        dispobjs = pipeline.get_process_results_display(proc)

        expres = os.path.join(self.test_data, "ResultsFiles/reextract_results.json")
        with open(expres, "r") as er:
            expected = json.load(er)
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
