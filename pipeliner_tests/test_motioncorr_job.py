#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.jobs.relion.motioncorr_job import (
    INPUT_NODE_MOVIES,
    OUTPUT_NODE_MICS,
    OUTPUT_NODE_LOG,
)


class MotionCorrTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_own(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own_relionstyle.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_jobstar(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own_job.star",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_continue(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own_continue.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --only_do_unfinished --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_noDW(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own_noDW.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --grouping_for_ps 4"
                " --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_savenoDW(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_own_save_noDW.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting "
                "--save_noDW --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_mocorr.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 2 relion_run_motioncorr_mpi --i "
                "Import/job001/movies.star --o MotionCorr/job002/ "
                "--first_frame_sum 1 --last_frame_sum -1 "
                "--use_motioncor2 --motioncor2_exe fake_motioncorr2.exe"
                " --other_motioncor2_args --mocorr_test_arg --gpu 0:1"
                " --bin_factor 1 --bfactor 150 --dose_per_frame 1.5 --preexposure 2"
                " --patch_x 5 --patch_y 5 --eer_grouping 32 --group_frames 3 "
                "--gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --save_noDW "
                "--pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_mocorr2_relionstyle_job.star",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "relion_run_motioncorr --i Import/job001/movies.star --o "
                "MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0"
                " --use_motioncor2 --motioncor2_exe /public/EM/MOTIONCOR2/MotionCor2"
                " --other_motioncor2_args some other args --gpu 0 --bin_factor 1 "
                "--bfactor 150 --dose_per_frame 1.277 --preexposure 0 --patch_x 5"
                " --patch_y 5 --eer_grouping 32 --group_frames 3 --gainref "
                "Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting "
                "--pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr_defectfile(self):
        general_get_command_test(
            self,
            "MotionCorr",
            "motioncorr_mocorr_defect.job",
            2,
            {"Import/job001/movies.star": INPUT_NODE_MOVIES},
            {
                "corrected_micrographs.star": OUTPUT_NODE_MICS,
                "logfile.pdf": OUTPUT_NODE_LOG,
            },
            [
                "mpirun -n 2 relion_run_motioncorr_mpi --i "
                "Import/job001/movies.star --o MotionCorr/job002/"
                " --first_frame_sum 1 --last_frame_sum -1 "
                "--use_motioncor2 --motioncor2_exe fake_motioncorr2.exe"
                " --other_motioncor2_args --mocorr_test_arg --gpu 0:1"
                " --defect_file defect_file.mrc --bin_factor 1 --bfactor 150"
                " --dose_per_frame 1.5 --preexposure 2 --patch_x 5 --patch_y 5"
                " --eer_grouping 32 --group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip"
                " 0 --dose_weighting --save_noDW --pipeline_control MotionCorr/job002/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_mocorr_generate_results(self):
        get_relion_tutorial_data(["MotionCorr"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("MotionCorr/job002/")
        dispobjs = pipeline.get_process_results_display(proc)
        expfile = os.path.join(self.test_data, "ResultsFiles/mocorr_results.json")
        with open(expfile, "r") as ef:
            exp = json.load(ef)
        assert dispobjs[0].__dict__ == exp
        assert os.path.isfile(dispobjs[0].img)


if __name__ == "__main__":
    unittest.main()
