#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
    expected_warning,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.jobs.relion.class2D_job import (
    INPUT_NODE_PARTS,
    INPUT_NODE_PARTS_HELICAL,
    INPUT_NODE_OPT,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS_HELICAL,
    OUTPUT_NODE_OPT,
)


class Class2DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_scratch_env_var(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Class2D/class2D_basic.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Class2D/class2D_basic.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_class2D_basic_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_basic.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--dont_combine_weights_via_disc --preread_images --pool 30 "
                "--pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --zero_mask"
                " --center_classes --oversampling 1 --psi_step 12.0 --offset_range 5"
                " --offset_step 4.0 --norm --scale --j 6 --gpu 0:1:2:3"
                " --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_basic_jobstar_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_job.star",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent "
                "--zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control "
                "Class2D/job008/"
            ],
        )

    def test_get_command_class2D_scratch_comb_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_scratch_comb.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--scratch_dir Fake_scratch_dir --pool 30 --pad 2 --ctf "
                "--iter 25 --tau2_fudge 2 --particle_diameter 200 --K 50 "
                "--flatten_solvent --zero_mask --center_classes --oversampling 1"
                " --psi_step 12.0"
                " --offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_corsesample_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_coarsesample.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --zero_mask"
                " --center_classes --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0"
                " --allow_coarser_sampling --norm --scale --j 6 --gpu 0:1:2:3 "
                "--pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_grad_nozero(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_grad_nozero.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "relion_refine --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 200 --grad "
                "--class_inactivity_threshold 0.1 --grad_write_iter 10 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control"
                " Class2D/job008/"
            ],
        )

    def test_get_command_class2D_higres10_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_highres10.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --strict_highres_exp 10.0 --center_classes"
                " --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --high_res_10_this_one --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_helical.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS_HELICAL},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELICAL,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --helical_outer_diameter 200 --bimodal_psi --sigma_psi 2.0"
                " --helix --helical_rise_initial 4.75 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_vdam(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_helical_vdam.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS_HELICAL},
            {
                "run_it200_optimiser.star": OUTPUT_NODE_OPT,
                "run_it200_data.star": OUTPUT_NODE_PARTS_HELICAL,
            },
            [
                "relion_refine --i Extract/job007/particles.star --o "
                "Class2D/job008/run --dont_combine_weights_via_disc "
                "--preread_images --pool 30 --pad 2 --ctf --iter 200 "
                "--grad --class_inactivity_threshold 0.1 --grad_write_iter "
                "10 --tau2_fudge 2 --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0 --helical_outer_diameter 200 "
                "--bimodal_psi --sigma_psi 2.0 --helix --helical_rise_initial 4.75 "
                "--norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_norestrict_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_helical_norestrict.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS_HELICAL},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS_HELICAL,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50"
                " --flatten_solvent --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --helical_outer_diameter 200 --bimodal_psi --sigma_psi 2.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control "
                "Class2D/job008/"
            ],
        )

    def test_get_command_class2D_nogpu_nompi_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_nogpu_nompi.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": OUTPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "relion_refine --i Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_continue_em(self):
        general_get_command_test(
            self,
            "Class2D",
            "class2D_continue.job",
            8,
            {"Extract/job007/particles.star": INPUT_NODE_PARTS},
            {
                "run_it025_optimiser.star": INPUT_NODE_OPT,
                "run_it025_data.star": OUTPUT_NODE_PARTS,
            },
            [
                "mpirun -n 5 relion_refine_mpi "
                "--continue run_it023_optimiser.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --j 6 --gpu 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_continue_badfile_em(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "Class2D",
                "class2D_continue_badfile.job",
                8,
                1,
                2,
                "",
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_cl2d(self):
        get_relion_tutorial_data("Class2D")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class2D/job008/")
        with expected_warning(RuntimeWarning, "true_divide", nwarn=1):
            dispobjs = pipeline.get_process_results_display(proc)
        efile = os.path.join(self.test_data, "ResultsFiles/class2d_results.json")
        with open(efile, "r") as ef:
            expected = json.load(ef)
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
