#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.relion.joinstar_job import (
    INPUT_NODE_PARTS,
    INPUT_NODE_MICS,
    INPUT_NODE_MOVIES,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_MICS,
    OUTPUT_NODE_MOVIES,
)

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.api.api_utils import job_parameters_dict, write_default_jobstar
from pipeliner.job_factory import active_job_from_proc
from pipeliner.onedep_deposition import expand_nt
from pipeliner_tests.generic_tests import check_for_relion, expected_warning


class JoinStarTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_join_parts(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_parts.job",
            13,
            {
                "parts_file1.star": INPUT_NODE_PARTS,
                "parts_file2.star": INPUT_NODE_PARTS,
                "parts_file3.star": INPUT_NODE_PARTS,
                "parts_file4.star": INPUT_NODE_PARTS,
            },
            {"join_particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_star_handler --combine --i parts_file1.star "
                "parts_file2.star parts_file3.star parts_file4.star "
                "--check_duplicates rlnImageName --o JoinStar/job013/"
                "join_particles.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_parts_relionstyle_name(self):
        """Automatic conversion of ambiguous relion style names"""
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_parts_relionstyle_job.star",
            13,
            {
                "Parts1.star": INPUT_NODE_PARTS,
                "Parts2.star": INPUT_NODE_PARTS,
            },
            {"join_particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_star_handler --combine --i Parts1.star "
                "Parts2.star --check_duplicates rlnImageName --o JoinStar/job013/"
                "join_particles.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_parts_jobstar(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_parts_job.star",
            13,
            {
                "parts_file1.star": INPUT_NODE_PARTS,
                "parts_file2.star": INPUT_NODE_PARTS,
                "parts_file3.star": INPUT_NODE_PARTS,
                "parts_file4.star": INPUT_NODE_PARTS,
            },
            {"join_particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_star_handler --combine --i parts_file1.star "
                "parts_file2.star parts_file3.star parts_file4.star "
                "--check_duplicates rlnImageName --o JoinStar/job013/join"
                "_particles.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_too_few_files(self):
        with self.assertRaises(ValueError):
            generic_tests.general_get_command_test(
                self,
                "JoinStar",
                "joinstar_too_few_files.job",
                13,
                4,
                1,
                "",
            )

    def test_join_parts_1and2_missing(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_parts_1and2_missing.job",
            13,
            {
                "parts_file3.star": INPUT_NODE_PARTS,
                "parts_file4.star": INPUT_NODE_PARTS,
            },
            {"join_particles.star": OUTPUT_NODE_PARTS},
            [
                "relion_star_handler --combine --i "
                "parts_file3.star parts_file4.star --check_duplicates"
                " rlnImageName --o JoinStar/job013/join_particles.star"
                " --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_mics(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_mics.job",
            13,
            {
                "micrographs_file1.star": INPUT_NODE_MICS,
                "micrographs_file2.star": INPUT_NODE_MICS,
                "micrographs_file3.star": INPUT_NODE_MICS,
                "micrographs_file4.star": INPUT_NODE_MICS,
            },
            {"join_mics.star": OUTPUT_NODE_MICS},
            [
                "relion_star_handler --combine --i micrographs_file1.star "
                "micrographs_file2.star micrographs_file3.star micrographs_"
                "file4.star --check_duplicates rlnMicrographName --o "
                "JoinStar/job013/join_mics.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_mics_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style jobname"""
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_mics_relionstyle_job.star",
            13,
            {
                "Mics1.star": INPUT_NODE_MICS,
                "Mics2.star": INPUT_NODE_MICS,
            },
            {"join_mics.star": OUTPUT_NODE_MICS},
            [
                "relion_star_handler --combine --i Mics1.star "
                "Mics2.star --check_duplicates rlnMicrographName --o "
                "JoinStar/job013/join_mics.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_movies(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_movies.job",
            13,
            {
                "movies_file1.star": INPUT_NODE_MOVIES,
                "movies_file2.star": INPUT_NODE_MOVIES,
                "movies_file3.star": INPUT_NODE_MOVIES,
                "movies_file4.star": INPUT_NODE_MOVIES,
            },
            {"join_movies.star": OUTPUT_NODE_MOVIES},
            [
                "relion_star_handler --combine --i movies_file1.star "
                "movies_file2.star movies_file3.star movies_file4.star "
                "--check_duplicates rlnMicrographName --o "
                "JoinStar/job013/join_movies.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_movies_relionstyle_jobname(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_movies_relionstyle_job.star",
            13,
            {
                "Movies1.star": INPUT_NODE_MOVIES,
                "Movies2.star": INPUT_NODE_MOVIES,
            },
            {"join_movies.star": OUTPUT_NODE_MOVIES},
            [
                "relion_star_handler --combine --i Movies1.star "
                "Movies2.star --check_duplicates rlnMicrographName --o "
                "JoinStar/job013/join_movies.star --pipeline_control JoinStar/job013/"
            ],
        )

    def test_join_movies_additional_args(self):
        generic_tests.general_get_command_test(
            self,
            "JoinStar",
            "joinstar_movies_extra_args.job",
            13,
            {
                "movies_file1.star": INPUT_NODE_MOVIES,
                "movies_file2.star": INPUT_NODE_MOVIES,
                "movies_file3.star": INPUT_NODE_MOVIES,
                "movies_file4.star": INPUT_NODE_MOVIES,
            },
            {"join_movies.star": OUTPUT_NODE_MOVIES},
            [
                "relion_star_handler --combine --i movies_file1.star "
                "movies_file2.star movies_file3.star movies_file4.star "
                "--check_duplicates rlnMicrographName --o "
                "JoinStar/job013/join_movies.star --Here_are_the_extra_arguments Blah"
                " --pipeline_control JoinStar/job013/"
            ],
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_join_movies_prepare_deposition_object(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")
        write_default_jobstar("relion.import.movies", "Import/job001/job.star")

        mf1 = os.path.join(self.test_dir, "Movies")
        mf2 = os.path.join(self.test_dir, "Movies2")
        header = [
            "# version 30001",
            "data_optics",
            "loop",
            "_rlnOpticsGroupName #1",
            "_rlnOpticsGroup #2",
            "_rlnMicrographOriginalPixelSize #3",
            "_rlnVoltage #4",
            "_rlnSphericalAberration #5",
            "_rlnAmplitudeContrast #6",
            "opticsGroup1            1     0.885000   200.000000"
            "   1.400000     0.100000",
            "# version 30001",
            "data_movies",
            "loop_",
            "_rlnMicrographMovieName #1",
            "_rlnOpticsGroup #2",
        ]
        for f in (mf1, mf2):
            with open(f, "w") as fout:
                for line in header:
                    fout.write(line + "/n")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
            with open(mf1, "a") as fout:
                fout.write(target + "\n")
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
            with open(mf2, "a") as fout:
                fout.write(target + "\n")

        # run the joinstar job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.joinstar.movies")
        params["fn_mov1"] = mf1
        params["fn_mov2"] = mf2
        proj.run_job(params)

        joinstar_job = active_job_from_proc(proj.pipeline.process_list[0])
        with expected_warning(RuntimeWarning, "overflow"):
            result = expand_nt(joinstar_job.prepare_onedep_data())
        expected_result = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                "Import/job001/movies.star; Prepared by ccpem-pipeliner vers 0.0.1",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629_000*_frame"
                "Image.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                "Import/job001/movies.star; Prepared by ccpem-pipeliner vers 0.0.1",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629_000*_"
                "frameImage.mrcs",
            },
        ]
        for i in expected_result:
            assert i in result
        for i in result:
            assert i in expected_result


if __name__ == "__main__":
    unittest.main()
