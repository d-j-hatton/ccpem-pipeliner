#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest

from pipeliner import job_factory
from pipeliner.jobs import dummy_job
from pipeliner.jobs.relion import refine3D_job
from pipeliner.jobs.other import cryolo_plugin_job


class JobFactoryTest(unittest.TestCase):
    def test_gather_all_jobs(self):
        job_dict = job_factory.gather_all_jobtypes()

        # Check a few example jobs
        assert job_dict["dummyjob"] is dummy_job.DummyJob
        assert job_dict["relion.refine3d"] is refine3D_job.RelionRefine3D
        assert job_dict["cryolo.autopick"] is cryolo_plugin_job.CrYOLOAutopick


if __name__ == "__main__":
    unittest.main()
