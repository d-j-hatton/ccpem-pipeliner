#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)

skip_live_tests = False
do_full = generic_tests.do_slow_tests()
# do_full = True
if (
    shutil.which("molprobity.molprobity") is None
    or shutil.which("molprobity.molprobity") is None
) and shutil.which("ccpem-python") is None:
    skip_live_tests = True
plugin_present = generic_tests.check_for_plugin("model_validation_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class ModelValidateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="model-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        print(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_validation_task(self):
        proc = job_running_test(
            os.path.join(self.test_data, "JobFiles/ModelValidation/validate.job"),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            (
                "molprobity.out",
                "run.out",
                "run.err",
            ),
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        rf = [
            "validation_smoc_1.json",
            "validation_smoc_2.json",
            "validation_smoc_3.json",
            "validation_smoc_4.json",
            "validation_molprobity.json",
            "validation_clusters.json",
        ]
        rfiles = [os.path.join(self.test_data, "ResultsFiles", x) for x in rf]
        for n, do in enumerate(dispobjs):
            with open(rfiles[n], "r") as rf:
                expected = json.load(rf)
            assert do.__dict__ == expected, print(do, expected)


if __name__ == "__main__":
    unittest.main()
