#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests.plugins_tests import test_data
from pipeliner_tests.generic_tests import (
    check_for_plugin,
    general_get_command_test,
)
from pipeliner.jobs.other.icebreaker_plugin_job import (
    INPUT_NODE_MICS,
    INPUT_NODE_GROUPED_MICS,
    INPUT_NODE_PARTS,
    OUTPUT_NODE_GROUPED_MICS,
    OUTPUT_NODE_GROUPED_PARTS,
    OUTPUT_NODE_FLAT_MICS,
    OUTPUT_NODE_FIVE_FIG,
)

progs = [shutil.which("ib_group"), shutil.which("ib_job")]
skip_live_tests = True if None in progs else False

plugin_present = check_for_plugin("icebreaker_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class IceBreakerTests(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_icebreaker_group_mics(self):
        general_get_command_test(
            self,
            "IceBreaker",
            "icebreaker_analysis_micrographs_job.star",
            2,
            {"MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS},
            {
                "grouped_micrographs.star": OUTPUT_NODE_GROUPED_MICS,
            },
            [
                "ib_job --j 8 --mode group --in_mics "
                "MotionCorr/job002/corrected_micrographs.star --o IceBreaker/job002/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_icebreaker_group_parts(self):
        general_get_command_test(
            self,
            "IceBreaker",
            "icebreaker_analysis_particles_job.star",
            2,
            {
                "IceBreaker/job003/corrected_micrographs.star": INPUT_NODE_GROUPED_MICS,
                "Extract/job005/particles.star": INPUT_NODE_PARTS,
            },
            {
                "ib_icegroups.star": OUTPUT_NODE_GROUPED_PARTS,
            },
            [
                "ib_group --j 4 --in_mics IceBreaker/job003/corrected_micrographs.star"
                " --in_parts Extract/job005/particles.star --o IceBreaker/job002/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_icebreaker_flatten_mics(self):
        general_get_command_test(
            self,
            "IceBreaker",
            "icebreaker_enhancecontrast_job.star",
            4,
            {"IceBreaker/job003/corrected_micrographs.star": INPUT_NODE_GROUPED_MICS},
            {
                "flattened_micrographs.star": OUTPUT_NODE_FLAT_MICS,
            },
            [
                "ib_job --j 4 --mode flatten --in_mics "
                "IceBreaker/job003/corrected_micrographs.star --o IceBreaker/job004/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_icebreaker_five_fig(self):
        general_get_command_test(
            self,
            "IceBreaker",
            "icebreaker_summary_job.star",
            4,
            {"IceBreaker/job003/corrected_micrographs.star": INPUT_NODE_GROUPED_MICS},
            {
                "five_figs_test.csv": OUTPUT_NODE_FIVE_FIG,
            },
            [
                "ib_5fig --j 4 --in_mics "
                "IceBreaker/job003/corrected_micrographs.star --o IceBreaker/job004/"
            ],
            jobfiles_dir="{}/{}",
        )


if __name__ == "__main__":
    unittest.main()
