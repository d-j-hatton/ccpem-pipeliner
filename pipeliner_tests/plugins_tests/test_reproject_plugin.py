#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner.utils import get_pipeliner_root
from pipeliner_tests.plugins_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.other.reproject_plugin_job import (
    INPUT_NODE_ANGLES,
    INPUT_NODE_MAP,
    OUTPUT_NODE_PROJS,
    OUTPUT_NODE_STAR,
)

plugins_dir = os.path.join(get_pipeliner_root(), "jobs/other")
test_plugins_dir = os.path.abspath(
    os.path.join(
        get_pipeliner_root(), "../pipeliner_tests/plugins_tests/testing_plugins"
    )
)
do_full = generic_tests.do_slow_tests()


class PlugInsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_project_plugin(self):
        shutil.copy(
            os.path.join(self.test_data, "../../test_data/emd_3488.mrc"), self.test_dir
        )
        shutil.copy(
            os.path.join(self.test_data, "Reproject/reproj_angles.star"), self.test_dir
        )
        generic_tests.general_get_command_test(
            self,
            "Reproject",
            "project_plugin_job.star",
            3,
            {
                "emd_3488.mrc": INPUT_NODE_MAP,
                "reproj_angles.star": INPUT_NODE_ANGLES,
            },
            {
                "reproj.mrcs": OUTPUT_NODE_PROJS,
                "reproj.star": OUTPUT_NODE_STAR,
            },
            [
                "relion_project --i emd_3488.mrc --ang reproj_angles.star"
                " --angpix 1.07 --o Reproject/job003/reproj --pipeline_control "
                "Reproject/job003/"
            ],
            jobfiles_dir="{}/{}",
        )

    # relion_project seems to have a bug in relion 3.1; maybe it's sorted in relion 4.0
    @unittest.expectedFailure
    @unittest.skipUnless(do_full, "Slow test: Only runs in full unittest")
    def test_running_projection_plugin(self):
        shutil.copy(
            os.path.join(self.test_data, "../../test_data/emd_3488.mrc"), self.test_dir
        )
        shutil.copy(
            os.path.join(self.test_data, "Reproject/reproj_angles.star"), self.test_dir
        )
        generic_tests.running_job(
            self,
            "project_plugin_job.star",
            "Reproject",
            3,
            [],
            (
                "run.out",
                "run.err",
                "reproj.mrcs",
                "reproj.star",
            ),
            1,
            jobfiles_dir="{}/{}",
            print_err=True,
        )


"""TO DO
Add tests for:
using uniform dist get command and run
error when no file and no uniform dist
"""

if __name__ == "__main__":
    unittest.main()
