#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests.plugins_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import touch
from pipeliner.jobs.other.proshade_plugin_job import (
    INPUT_NODE_MAP,
    INPUT_NODE_COORDS,
    OUTPUT_NODE_MAP,
    OUTPUT_NODE_COORDS,
)
from pipeliner.pipeliner_job import OUTPUT_NODE_RUNOUT

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("proshade") is None else False

plugin_present = generic_tests.check_for_plugin("proshade_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class ProShadeTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="proshade_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set up the files threeDFSC needs to run

        # set the path to find proshade
        # see if there is a functioning copy of proshade
        self.oldpath = os.environ["PATH"]

        # if not use a fake one for the get commands tests
        if skip_live_tests:
            self.skip_live_tests = True
            proshade_path = os.path.join(self.test_data, "fake_exes")
            os.environ["PATH"] = self.oldpath + ":" + proshade_path

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_command_proshade_plugin_symmetry(self):
        """Doesn't include the output node because it is created by postrun
        actions()"""
        generic_tests.general_get_command_test(
            self,
            "ProShade",
            "proshade_symmetry_job.star",
            2,
            {"test.mrc": INPUT_NODE_MAP},
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "proshade --rvpath /xtal/ccpem-20220125/share -S  -f  test.mrc",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_proshade_plugin_rebox(self):
        generic_tests.general_get_command_test(
            self,
            "ProShade",
            "proshade_reboxing_job.star",
            2,
            {"test.mrc": INPUT_NODE_MAP},
            {"reboxed_map.mrc": OUTPUT_NODE_MAP},
            [
                "proshade --rvpath /xtal/ccpem-20220125/share -E  -f  test.mrc"
                "  --clearMap  ProShade/job002/reboxed_map.mrc --cellBorderSpace 1.0",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_proshade_plugin_overlay(self):
        generic_tests.general_get_command_test(
            self,
            "ProShade",
            "proshade_overlay_job.star",
            2,
            {"test1.mrc": INPUT_NODE_MAP, "test2.mrc": INPUT_NODE_MAP},
            {"test2_overlaid.mrc": OUTPUT_NODE_MAP},
            [
                "proshade --rvpath /xtal/ccpem-20220125/share -O  -f  test1.mrc  -f"
                "  test2.mrc  --clearMap  ProShade/job002/test2_overlaid.mrc "
                "--cellBorderSpace 20.0",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_proshade_plugin_overlay_with_coords(self):
        generic_tests.general_get_command_test(
            self,
            "ProShade",
            "proshade_overlay_with_coords_job.star",
            3,
            {"test1.mrc": INPUT_NODE_MAP, "test2.pdb": INPUT_NODE_COORDS},
            {"test2_overlaid.pdb": OUTPUT_NODE_COORDS},
            [
                "proshade --rvpath /xtal/ccpem-20220125/share -O  "
                "-f  test1.mrc  -f  test2.pdb  --clearMap  "
                "ProShade/job003/test2_overlaid.pdb",
                "mv proshade_report ProShade/job003/proshade_report",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_symmetry_writing_displayobjs(self):
        os.makedirs("ProShade/job001")
        touch("test.mrc")
        pipe = os.path.join(self.test_data, "ProShade/proshade_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        log = os.path.join(self.test_data, "ProShade/logfile.txt")
        shutil.copy(log, "ProShade/job001/run.out")
        jobstar = os.path.join(self.test_data, "ProShade/proshade_symmetry_job.star")
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        axes = os.path.join(self.test_data, "ProShade/axes_results.json")
        elems = os.path.join(self.test_data, "ProShade/elements_results.json")
        alts = os.path.join(self.test_data, "ProShade/alternatives_results.json")

        with open(axes, "r") as ax:
            exp_ax = json.load(ax)
        with open(elems, "r") as elm:
            exp_elem = json.load(elm)
        with open(alts, "r") as alt:
            exp_alt = json.load(alt)

        assert dispobjs[0].__dict__ == exp_ax
        assert dispobjs[1].__dict__ == exp_elem
        assert dispobjs[2].__dict__ == exp_alt

    def test_rebox_writing_displayobjs_dir_not_found(self):
        os.makedirs("ProShade/job001")
        touch("test.mrc")
        pipe = os.path.join(self.test_data, "ProShade/proshade_pipeline_rebox.star")
        shutil.copy(pipe, "default_pipeline.star")
        jobstar = os.path.join(self.test_data, "ProShade/proshade_reboxing_job.star")
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "title": "Results pending...",
            "message": "Error creating results display",
            "reason": "Error creating ResultsDisplayRvapi: Directory "
            "ProShade/job001/proshade_report does not exist",
        }

    def test_rebox_writing_displayobjs(self):
        os.makedirs("ProShade/job001/proshade_report")
        touch("test.mrc")
        pipe = os.path.join(self.test_data, "ProShade/proshade_pipeline_rebox.star")
        shutil.copy(pipe, "default_pipeline.star")
        jobstar = os.path.join(self.test_data, "ProShade/proshade_reboxing_job.star")
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {"rvapi_dir": "ProShade/job001/proshade_report"}


if __name__ == "__main__":
    unittest.main()
