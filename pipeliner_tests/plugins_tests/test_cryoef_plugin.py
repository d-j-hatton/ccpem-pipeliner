#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests.plugins_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import generic_tests
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.jobs.other.cryoef_plugin_job import (
    INPUT_NODE_PARTS,
    OUTPUT_NODE_PS,
    OUTPUT_NODE_TF,
    OUTPUT_NODE_LOG,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("cryoef") is None else False

plugin_present = generic_tests.check_for_plugin("cryoef_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class CryoEFTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set up the files threeDFSC needs to run

        # set the path to find cryoEF
        # see if there is a functioning copy of cryoEF
        self.oldpath = os.environ["PATH"]

        # if not use a fake one for the get commands tests
        if skip_live_tests:
            self.skip_live_tests = True
            cryoEF_path = os.path.join(self.test_data, "fake_exes")
            os.environ["PATH"] = self.oldpath + ":" + cryoEF_path

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_command_cryoEF_plugin_input(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        generic_tests.general_get_command_test(
            self,
            "CryoEF",
            "cryoef_star_job.star",
            2,
            {"Refine3D/job001/run_data.star": INPUT_NODE_PARTS},
            {
                "cryoef_angles_K.mrc": OUTPUT_NODE_TF,
                "cryoef_angles_R.mrc": OUTPUT_NODE_PS,
                "cryoef_angles.log": OUTPUT_NODE_LOG,
            },
            [
                "mv cryoef_angles.dat CryoEF/job002/cryoef_angles.dat",
                "cryoEF -f CryoEF/job002/cryoef_angles.dat -B 150.0 "
                "-D 80.0 -b 128 -r 3.8 -g C1",
            ],
            jobfiles_dir="{}/{}",
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: only run in full unittest"
    )
    def test_run_cryoEF(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        out_proc = generic_tests.running_job(
            self,
            "cryoef_star_job.star",
            "CryoEF",
            2,
            [],
            (
                "run.out",
                "run.err",
                "cryoef_angles_K.mrc",
                "cryoef_angles_R.mrc",
                SUCCESS_FILE,
            ),
            5,
            jobfiles_dir="{}/{}",
        )

        # make sure cleanup happened
        assert not os.path.isfile("cryoef_angles.dat")
        dispobjs = active_job_from_proc(out_proc).create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Tilted collection recommendations",
            "headers": ["Tilt angle", "Reccomended particles to collect"],
            "table_data": [["42", "~3.2x"], ["39", "~3.0x"]],
            "associated_data": ["CryoEF/job002/cryoef_angles.log"],
        }

        hd = os.path.join(self.test_data, "CryoEF/results_histo.json")
        with open(hd, "r") as hdf:
            exp_histo = json.load(hdf)
        assert dispobjs[1].__dict__ == exp_histo

        assert dispobjs[2].__dict__ == {
            "maps": ["CryoEF/job002/Thumbnails/cryoef_angles_K.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Fourier space information density",
            "maps_data": "CryoEF/job002/cryoef_angles_K.mrc",
            "models_data": "",
            "associated_data": ["CryoEF/job002/cryoef_angles_K.mrc"],
        }


if __name__ == "__main__":
    unittest.main()
