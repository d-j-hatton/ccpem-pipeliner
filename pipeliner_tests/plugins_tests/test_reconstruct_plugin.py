#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner.utils import get_pipeliner_root
from pipeliner_tests.plugins_tests import test_data
from pipeliner.job_factory import new_job_of_type

from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner.jobs.other.reconstruct_plugin_job import (
    INPUT_NODE_PARTS,
    OUTPUT_NODE_MAP,
)

plugins_dir = os.path.join(get_pipeliner_root(), "jobs/other")
test_plugins_dir = os.path.abspath(
    os.path.join(
        get_pipeliner_root(), "../pipeliner_tests/plugins_tests/testing_plugins"
    )
)


class PlugInsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.pipeliner_test_data = os.path.join(
            os.path.abspath(os.path.join(__file__, "../..")),
            "test_data",
        )
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_reconstruct_plugin(self):
        general_get_command_test(
            self,
            "Reconstruct",
            "reconstruct_plugin_job.star",
            2,
            {"test_particles.star": INPUT_NODE_PARTS},
            {"reconstruction.mrc": OUTPUT_NODE_MAP},
            [
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --angpix 1.07"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_reconstruct_mpi_plugin(self):
        general_get_command_test(
            self,
            "Reconstruct",
            "reconstruct_plugin_mpi_job.star",
            2,
            {"test_particles.star": INPUT_NODE_PARTS},
            {"reconstruction.mrc": OUTPUT_NODE_MAP},
            [
                "mpirun -n 8 relion_reconstruct_mpi "
                "--i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --angpix 1.07"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_reconstruct_plugin_with_no_angpix_and_maxres(self):
        general_get_command_test(
            self,
            "Reconstruct",
            "reconstruct_plugin_angpx_maxres_job.star",
            2,
            {"test_particles.star": INPUT_NODE_PARTS},
            {"reconstruction.mrc": OUTPUT_NODE_MAP},
            [
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --maxres 2.0"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
            jobfiles_dir="{}/{}",
        )

    def test_create_results_display(self):
        mrc = os.path.join(self.pipeliner_test_data, "emd_3488.mrc")
        os.makedirs("Reconstruct/job001")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction.mrc")
        job = new_job_of_type("relion.reconstruct")
        job.output_name = "Reconstruct/job001/"
        dispobj = job.create_results_display()
        expected = {
            "maps": ["Reconstruct/job001/reconstruction.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Reconstructed map",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
        }
        assert dispobj[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
