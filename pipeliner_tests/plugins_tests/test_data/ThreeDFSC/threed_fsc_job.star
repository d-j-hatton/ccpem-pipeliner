# version 30001

data_job

_rlnJobType                            threedfsc.map_analysis
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
fn_halfmap	Refine3D/job001/run_class001_half1_unfil.mrc
fn_fullmap Refine3D/job001/run_class001.mrc
fn_mask		MaskCreate/job002/mask.mrc
apix	1.04
dtheta	20.0
fsc_cutoff	0.143
sphericity_threshold	0.5
highpass	200
nr_sphericity_thresholds	0
use_gpu	No
gpu_id ""
other_args ""
do_queue         	No
min_dedicated       1 
qsub       			qsub 
qsubscript 			submission_script.sh 
queuename    		openmpi 
other_args         	"" 
qsub_extra_1			'matthew.iadanza@stfc.ac.uk'
qsub_extra_2			'kwanze queuesub extra var: moja'
qsub_extra_3			'pili queuesub extra var: mbili'
qsub_extra_4			'wa tatu queuesub extra var: tatu'