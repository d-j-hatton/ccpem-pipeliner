
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMicrographOriginalPixelSize #3 
_rlnVoltage #4 
_rlnSphericalAberration #5 
_rlnAmplitudeContrast #6 
_rlnImagePixelSize #7 
_rlnImageSize #8 
_rlnImageDimensionality #9 
_rlnEvenZernike #10 
_rlnCtfDataAreCtfPremultiplied #11 
opticsGroup1            1     0.885000   200.000000     1.400000     0.100000     1.770000          128            2 [-8.12886683387,13.9661723227,-12.0852889468,1.89788006156,-1.69454806797,4.84537324617,-3.91829259584,0.598583991157,3.65821148843]            0 
 

# version 30001

data_particles

loop_ 
_rlnCoordinateX #1 
_rlnCoordinateY #2 
_rlnAutopickFigureOfMerit #3 
_rlnClassNumber #4 
_rlnAnglePsi #5 
_rlnImageName #6 
_rlnMicrographName #7 
_rlnOpticsGroup #8 
_rlnCtfMaxResolution #9 
_rlnCtfFigureOfMerit #10 
_rlnDefocusU #11 
_rlnDefocusV #12 
_rlnDefocusAngle #13 
_rlnCtfBfactor #14 
_rlnCtfScalefactor #15 
_rlnPhaseShift #16 
_rlnAngleRot #17 
_rlnAngleTilt #18 
_rlnOriginXAngst #19 
_rlnOriginYAngst #20 
_rlnNormCorrection #21 
_rlnLogLikeliContribution #22 
_rlnMaxValueProbDistribution #23 
_rlnNrOfSignificantSamples #24 
_rlnGroupName #25 
_rlnGroupNumber #26 
_rlnRandomSubset #27 
 1614.217657   112.882354     0.163130            1   143.703305 1@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10930.998912 10648.859957    76.311170     0.000000     1.000000     0.000000    30.356169    85.572836     -9.48537     0.293883     0.953404 49548.144547     0.270265            4  group_003            1            1 
  304.782355   440.241179     0.143135            1    -40.97251 2@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 11017.583623 10735.444667    76.311170     0.000000     1.000000     0.000000    21.541107    31.897428    14.453883     6.909258     0.935761 49461.474043     0.414816            2  group_003            1            2 
  643.429416  1264.282360     0.133688            1    88.852625 3@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10701.737636 10419.598681    76.311170     0.000000     1.000000     0.000000   157.268066    79.135810    13.148508     -9.02074     0.926514 49374.919222     0.414622            4  group_003            1            1 
 1727.100010  1986.729423     0.127264            1    -28.33236 4@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10979.596272 10697.457317    76.311170     0.000000     1.000000     0.000000    25.786622    86.472639    -15.63612     6.909258     0.934979 49437.348066     0.506946            5  group_003            1            1 
  158.035295   643.429416     0.125821            1    33.648661 5@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 11093.297307 10811.158352    76.311170     0.000000     1.000000     0.000000   138.671149    17.935728    20.228508     7.373883     0.940612 49364.563771     0.674301            1  group_003            1            1 
 2381.817661   620.852945     0.124636            1    -15.68637 6@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10788.491143 10506.352187    76.311170     0.000000     1.000000     0.000000    15.590987    83.179770     -7.71537     -1.94074     0.939778 49393.964823     0.597071            2  group_003            1            2 
 2336.664720  1309.435302     0.118252            1   111.820228 7@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10921.164273 10639.025317    76.311170     0.000000     1.000000     0.000000   159.023541    72.672017    -13.02537     -4.17537     0.952189 49451.430472     0.410431            2  group_003            1            2 
 2483.411779  3239.723548     0.112953            1    99.665708 8@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 11091.124890 10808.985934    76.311170     0.000000     1.000000     0.000000    60.225006    73.073135     5.139258     -5.01612     0.945830 49441.761322     0.738558            3  group_003            1            1 
 1715.811775  2426.970602     0.111875            1    72.462136 9@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10712.390460 10430.251505    76.311170     0.000000     1.000000     0.000000   142.844125    68.888711     6.444633     -6.78612     0.953643 49453.023148     0.539492            2  group_003            1            1 
 3251.011784   948.211770     0.111297            1   -168.51023 10@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 11018.095293 10735.956337    76.311170     0.000000     1.000000     0.000000     5.046141    90.360299     -5.48074    -14.79537     0.937217 49394.909099     0.900312            3  group_003            1            2 
  383.800002  1467.470597     0.110026            1   169.946117 11@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10919.713549 10637.574593    76.311170     0.000000     1.000000     0.000000    21.561225    69.952284     8.679258     -1.94074     0.934266 49420.720390     0.431653            5  group_003            1            2 
  270.917649   880.482358     0.109058            1   -141.76636 12@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10734.396072 10452.257116    76.311170     0.000000     1.000000     0.000000    28.946811    79.551117     -3.24612     -3.71074     0.919153 49409.562141     0.538476            2  group_003            1            1 
 1535.200009  3330.029431     0.103874            1    71.496103 13@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 10956.351651 10674.212695    76.311170     0.000000     1.000000     0.000000    10.315644    70.589904     -4.17537     8.214633     0.940226 49454.974133     0.597343            2  group_003            1            2 
 2156.052954   316.070590     0.100104            1   141.849521 14@Polish/job027/Movies/20170629_00021_frameImage_shiny.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.874623     0.131143 11032.210440 10750.071485    76.311170     0.000000     1.000000     0.000000   124.915745    75.486983     1.599258    -10.32612     0.923394 49381.719987     0.389888            4  group_003            1            1 
