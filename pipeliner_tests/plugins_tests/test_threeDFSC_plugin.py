#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests.plugins_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.utils import touch, get_pipeliner_root
from pipeliner.jobs.other.threeDFSC_plugin_job import (
    INPUT_NODE_HALFMAP,
    INPUT_NODE_MAP,
    INPUT_NODE_MASK,
    OUTPUT_NODE_THREEDFSC,
)
from pipeliner_tests.generic_tests import read_pipeline

skip_live_tests = True if shutil.which("ThreeDFSC_Start.py") is None else False
plugin_present = generic_tests.check_for_plugin("threeDFSC_plugin_job.py")
cc_path = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class ThreeDFSCPlugInsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set up the files threeDFSC needs to run
        os.makedirs("Refine3D/job001")
        touch("Refine3D/job001/run_class001_half2_unfil.mrc")
        touch("Refine3D/job001/run_class001.mrc")
        touch("Refine3D/job001/run_class001_half1_unfil.mrc")
        model_file = os.path.join(self.test_data, "run_model.star")
        shutil.copy(model_file, "Refine3D/job001")

        # set the path to find ThreeDFSC_Start.py
        self.oldpath = os.environ["PATH"]
        if skip_live_tests:
            os.environ["PATH"] = (
                self.oldpath + ":" + os.path.join(self.test_data, "fake_exes")
            )

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_command_3dfsc_plugin(self):
        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_apix_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--mask=MaskCreate/job002/mask.mrc"
                " --apix=1.244 --dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_3dfsc_plugin_queued(self):
        sub_script = os.path.join(
            self.test_data, "../../test_data/submission_script.sh"
        )

        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1: ",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2: ",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3: ",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4: ",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }

        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        shutil.copy(sub_script, self.test_dir)
        os.makedirs("ThreeDFSC/job003")

        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_queued_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            ["qsub ThreeDFSC/job003/run_submit.script"],
            jobfiles_dir="{}/{}",
            qsub=True,
        )

        exp_sub_script = [
            " \n",
            "#$ -cwd -V\n",
            "#$ -l h_rt=96:00:00     # specifiy max run time here\n",
            "#$ -m be\n",
            "#$ -e ThreeDFSC/job003/run.err\n",
            "#$ -o ThreeDFSC/job003/run.out\n",
            "#$ -P openmpi\n",
            "#$  -M matthew.iadanza@stfc.ac.uk    # put your email address here \n",
            "#$  -l coproc_v100=4   # GPUS in relion should be left blank\n",
            "## load modules \n",
            "module load intel/19.0.4 cuda/10.1.168\n",
            "## Here are some more extra variables\n",
            "kwanze queuesub extra var: moja\n",
            "pili queuesub extra var: mbili\n",
            "## set library paths\n",
            "export PATH=$CUDA_HOME/bin:$PATH\n",
            "export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH\n",
            (
                "export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/"
                "relion/build/lib/:\n"
            ),
            "## print some diagnostic info \n",
            "module list\n",
            "nvidia-smi -L\n",
            "which relion_refine_mpi\n",
            "## run relion\n",
            "let NSLOTS=NSLOTS/2\n",
            (
                "time -p ThreeDFSC_Start.py --halfmap1=Refine3D/job001/run_class"
                "001_half1_unfil.mrc --halfmap2=Refine3D/job001/run_class001"
                "_half2_unfil.mrc --fullmap=Refine3D/job001/run_class001.mrc"
                " --mask=MaskCreate/job002/mask.mrc --apix=1.04\n"
            ),
            "time -p mv Results_3DFSCOutput/* ThreeDFSC/job003/\n",
            "time -p rm -r Results_3DFSCOutput\n",
            (f"time -p {cc_path} ThreeDFSC/job003/ " "3DFSCOutput.mrc\n"),
            "#One more extra command for good measure \n",
            "wa tatu queuesub extra var: tatu\n",
            "dedicated nodes: 1\n",
            "output name: ThreeDFSC/job003/\n",
        ]
        expected_lines = [x.split() for x in exp_sub_script]

        sub_script_data = read_pipeline("ThreeDFSC/job003/run_submit.script")

        for line in sub_script_data:
            assert line in expected_lines, line
        for line in expected_lines:
            assert line in sub_script_data, line

        for envvar in envvars:
            del os.environ[envvar]

    def test_get_command_threedfsc_with_mask(self):

        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--mask=MaskCreate/job002/mask.mrc --apix=1.04 "
                "--dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_threedfsc_with_mask_1gpu(self):

        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_mask_1gpu_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--mask=MaskCreate/job002/mask.mrc --apix=1.04 "
                "--dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0"
                " --gpu --gpu_id=1",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_threedfsc_with_mask_toomany_gpu(self):
        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_mask_2gpu_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--mask=MaskCreate/job002/mask.mrc --apix=1.04 "
                "--dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0"
                " --gpu --gpu_id=1",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_threedfsc_no_mask(self):
        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_nomask_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--apix=1.04 "
                "--dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_threedfsc_auto_apix(self):
        ref3d = "Refine3D/job001/run_class001"

        generic_tests.general_get_command_test(
            self,
            "ThreeDFSC",
            "threed_fsc_apix_job.star",
            3,
            {
                ref3d + "_half1_unfil.mrc": INPUT_NODE_HALFMAP,
                ref3d + ".mrc": INPUT_NODE_MAP,
                "MaskCreate/job002/mask.mrc": INPUT_NODE_MASK,
            },
            {"3DFSCOutput.mrc": OUTPUT_NODE_THREEDFSC},
            [
                "ThreeDFSC_Start.py "
                "--halfmap1=Refine3D/job001/run_class001_half1_unfil.mrc "
                "--halfmap2=Refine3D/job001/run_class001_half2_unfil.mrc "
                "--fullmap=Refine3D/job001/run_class001.mrc "
                "--mask=MaskCreate/job002/mask.mrc --apix=1.244 "
                "--dthetaInDegrees=20.0 --FSCCutoff=0.143 "
                "--ThresholdForSphericity=0.5 --HighPassFilter=200.0",
                "mv Results_3DFSCOutput/* ThreeDFSC/job003/",
                "rm -r Results_3DFSCOutput",
            ],
            jobfiles_dir="{}/{}",
        )

    def test_get_command_threedfsc_error_halfmap1_misnamed(self):
        f = "Refine3D/job001/run_class001_half1_unfil.mrc"
        shutil.move(f, f.replace("half1", "junk"))

        with self.assertRaises(ValueError):
            generic_tests.general_get_command_test(
                self,
                "ThreeDFSC",
                "threed_fsc_badmap_job.star",
                3,
                {},
                {},
                "",
                jobfiles_dir="{}/{}",
            )

    def test_get_command_threedfsc_error_halfmap1_missing(self):
        os.remove("Refine3D/job001/run_class001_half1_unfil.mrc")
        with self.assertRaises(ValueError):
            generic_tests.general_get_command_test(
                self,
                "ThreeDFSC",
                "threed_fsc_job.star",
                3,
                {},
                {},
                [],
                jobfiles_dir="{}/{}",
            )

    def test_get_command_threedfsc_error_halfmap2_missing(self):
        os.remove("Refine3D/job001/run_class001_half2_unfil.mrc")
        with self.assertRaises(ValueError):
            generic_tests.general_get_command_test(
                self,
                "ThreeDFSC",
                "threed_fsc_job.star",
                3,
                {},
                {},
                [],
                jobfiles_dir="{}/{}",
            )

    def test_get_command_threedfsc_error_fullmap_missing(self):
        os.remove("Refine3D/job001/run_class001.mrc")
        with self.assertRaises(ValueError):
            generic_tests.general_get_command_test(
                self,
                "ThreeDFSC",
                "threed_fsc_job.star",
                3,
                {},
                {},
                "",
                jobfiles_dir="{}/{}",
            )


if __name__ == "__main__":
    unittest.main()
