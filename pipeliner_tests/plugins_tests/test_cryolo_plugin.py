#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from glob import glob
from gemmi import cif


from pipeliner.utils import touch
from pipeliner_tests.plugins_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.other.cryolo_plugin_job import INPUT_NODE_CRYOLOMODEL
from pipeliner.jobs.other.cryolo_plugin_job import (
    INPUT_NODE_MICS,
    INPUT_NODE_JANNIMODEL,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.pipeliner_job import OUTPUT_NODE_RUNOUT

plugin_present = generic_tests.check_for_plugin("cryolo_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class CrYOLOTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def set_up_micrographs_file(self):
        """Make the micrographs file that the cryolo plugin needs to read for
        doing comparisons at command 3"""
        os.makedirs("CRYOLO_TMP")
        os.makedirs("CtfFind/job003")
        infile = os.path.join(self.test_data, "CrYOLO/micrographs_ctf.star")
        file_path = "CtfFind/job003/micrographs_ctf.star"
        shutil.copy(infile, file_path)
        return file_path

    def test_get_command_crYOLO_plugin_with_file_input(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        generic_tests.general_get_command_test(
            self,
            "CrYOLO",
            "crYOLO_file_input_job.star",
            2,
            {
                "CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS,
                "cryolomodel.h5": INPUT_NODE_CRYOLOMODEL,
            },
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "cryolo_gui.py config AutoPick/job002/cryolo_config.json 256 "
                "--filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c AutoPick/job002/cryolo_config.json -w "
                "cryolomodel.h5 -i CRYOLO_TMP/ -g 0,1 -o AutoPick/job002/"
                " -t 0.3",
            ],
            jobfiles_dir="{}/{}",
            job_out_dir="AutoPick",
        )
        linked_files = glob("CRYOLO_TMP/*.mrc")
        assert len(linked_files) == 24

    def test_get_command_crYOLO_plugin_with_file_input_continue(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        os.makedirs("AutoPick/job002")
        apfile = os.path.join(self.test_data, "CrYOLO/autopick.star")
        shutil.copy(apfile, "AutoPick/job002/autopick.star")
        generic_tests.general_get_command_test(
            self,
            "CrYOLO",
            "crYOLO_continue_job.star",
            2,
            {
                "CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS,
                "cryolomodel.h5": INPUT_NODE_CRYOLOMODEL,
            },
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "cryolo_gui.py config AutoPick/job002/cryolo_config.json 256 "
                "--filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c AutoPick/job002/cryolo_config.json -w "
                "cryolomodel.h5 -i CRYOLO_TMP/ -g 0,1 -o AutoPick/job002/"
                " -t 0.3",
            ],
            jobfiles_dir="{}/{}",
            job_out_dir="AutoPick",
        )
        linked_files = glob("CRYOLO_TMP/*.mrc")
        assert len(linked_files) == 14

    def test_get_command_CrYOLO_plugin_with_file_input_use_JANNI(self):
        """Test using a starfile as in input, using JANNI instead of LOWPASS"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        generic_tests.general_get_command_test(
            self,
            "CrYOLO",
            "crYOLO_file_JANNI_job.star",
            2,
            {
                "CtfFind/job003/micrographs_ctf.star": INPUT_NODE_MICS,
                "path/to/JANNI": INPUT_NODE_JANNIMODEL,
                "cryolomodel.h5": INPUT_NODE_CRYOLOMODEL,
            },
            {"run.out": OUTPUT_NODE_RUNOUT},
            [
                "cryolo_gui.py config AutoPick/job002/cryolo_config.json 256 "
                "--filter JANNI --janni_model path/to/JANNI",
                "cryolo_predict.py -c AutoPick/job002/cryolo_config.json -w "
                "cryolomodel.h5 -i CRYOLO_TMP/ -g 0,1 -o AutoPick/job002/"
                " -t 0.3",
            ],
            jobfiles_dir="{}/{}",
            job_out_dir="AutoPick",
        )

    def test_get_command_CrYOLO_plugin_file_missing(self):
        """Test using a starfile as in input but file is missing"""
        with self.assertRaises((ValueError, RuntimeError)):
            generic_tests.general_get_command_test(
                self,
                "CrYOLO",
                "crYOLO_file_input_job.star",
                2,
                {},
                {},
                "",
                jobfiles_dir="{}/{}",
            )

    def test_counting_and_comparing_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_name = "AutoPick/job003/"
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        stardir = "AutoPick/job003/STAR"
        self.set_up_micrographs_file()
        os.makedirs(stardir)
        for n in range(21, 50):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        yolojob.post_run_actions()

        in_star = cif.read_file("AutoPick/job003/autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 45):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 45):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with one missing"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_name = "AutoPick/job003/"
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"

        self.set_up_micrographs_file()
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        for n in range(21, 44):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        yolojob.post_run_actions()

        in_star = cif.read_file("AutoPick/job003/autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 44):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 44):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning_multi(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with multiple
        missing"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_name = "AutoPick/job003/"
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        self.set_up_micrographs_file()
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        for n in range(21, 36):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        yolojob.post_run_actions()
        in_star = cif.read_file("AutoPick/job003/autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 36):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 36):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_fail_if_no_part_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Fail if no Particle files were written"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_name = "AutoPick/job003/"
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        self.set_up_micrographs_file()
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        with self.assertRaises(ValueError):
            yolojob.post_run_actions()


if __name__ == "__main__":
    unittest.main()
