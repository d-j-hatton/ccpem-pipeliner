#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data, generic_tests
from pipeliner.jobstar_reader import modify_jobstar
from pipeliner_tests.generic_tests import check_for_relion
from pipeliner.star_writer import COMMENT_LINE


class JobStarWriterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def split_lines(self, linelist):
        return [x.split() for x in linelist]

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_with_postprocess_withmtf(self):
        generic_tests.running_job(
            self,
            "postprocess_adhocbf.job",
            "PostProcess",
            3,
            [
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            (
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            1,
        )
        actual_path = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_job.star"
        )
        wrote_path = os.path.join(self.test_dir, "PostProcess/job003/job.star")
        with open(actual_path) as actual:
            actual_file = actual.readlines()
        with open(wrote_path) as wrote:
            wrote_file = wrote.readlines()
        for line in actual_file:
            if "# version" not in line:
                assert line.split() in self.split_lines(wrote_file), line
        for line in wrote_file:
            if line != f"# {COMMENT_LINE}\n":
                assert line.split() in self.split_lines(actual_file), line

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_with_maskcreate_job(self):
        generic_tests.running_job(
            self,
            "maskcreate.job",
            "MaskCreate",
            2,
            [("Import/job001", "emd_3488.mrc")],
            ("run.out", "run.err", "mask.mrc"),
            5,
        )
        actual_path = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )
        wrote_path = os.path.join(self.test_dir, "MaskCreate/job002/job.star")
        # subprocess.call(["cat",wrote_path])
        with open(actual_path) as actual:
            actual_file = actual.readlines()
        with open(wrote_path) as wrote:
            wrote_file = wrote.readlines()
        for line in actual_file:
            if "# version" not in line and len(line) > 5:
                assert line.split() in self.split_lines(wrote_file), line
        for line in wrote_file:
            if line != f"# {COMMENT_LINE}\n" and len(line) > 5:
                assert line.split() in self.split_lines(actual_file), line

    def test_modify_jobstar(self):
        """modify a parameter in a jobstar"""

        # template file
        jobstar_file = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )

        shutil.copy(jobstar_file, os.path.join(self.test_dir, "test_job.star"))

        modify_jobstar(jobstar_file, {"width_mask_edge": "100"}, "changed_job.star")

        # read the two files
        with open(jobstar_file) as original:
            original_lines = original.readlines()
        with open("changed_job.star") as written:
            written_lines = written.readlines()

        # make sure the change has been written
        for line in original_lines:
            if "# version " not in line and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(written_lines), line
                else:
                    assert line.split()[1] == "6", line
                    assert line.split() not in self.split_lines(written_lines)

        for line in written_lines:
            if line != "# " + COMMENT_LINE + "\n" and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(original_lines), line
                else:
                    assert line.split()[1] == "100", line
                    assert line.split() not in self.split_lines(original_lines)

    def test_modify_jobstar_fix_filename(self):
        """modify a parameter in a jobstar, fix a bad filename"""

        # template file
        jobstar_file = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )

        shutil.copy(jobstar_file, os.path.join(self.test_dir, "test_job.star"))

        modify_jobstar(jobstar_file, {"width_mask_edge": "100"}, "changed")

        # read the two files
        with open(jobstar_file) as original:
            original_lines = original.readlines()
        with open("changed_job.star") as written:
            written_lines = written.readlines()

        # make sure the change has been written
        for line in original_lines:
            if "# version " not in line and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(written_lines), line
                else:
                    assert line.split()[1] == "6", line
                    assert line.split() not in self.split_lines(written_lines)

        for line in written_lines:
            if line != "# " + COMMENT_LINE + "\n" and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(original_lines), (
                        line,
                        "# " + COMMENT_LINE,
                    )
                else:
                    assert line.split()[1] == "100", line
                    assert line.split() not in self.split_lines(original_lines)

    def test_modify_jobstar_fix_filename_withext(self):
        """modify a parameter in a jobstar, fix a bad filename
        that has .star extension"""

        # template file
        jobstar_file = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )

        shutil.copy(jobstar_file, os.path.join(self.test_dir, "test_job.star"))

        modify_jobstar(jobstar_file, {"width_mask_edge": "100"}, "changed.star")

        # read the two files
        with open(jobstar_file) as original:
            original_lines = original.readlines()
        with open("changed_job.star") as written:
            written_lines = written.readlines()

        # make sure the change has been written
        for line in original_lines:
            if "# version " not in line and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(written_lines), line
                else:
                    assert line.split()[1] == "6", line
                    assert line.split() not in self.split_lines(written_lines)

        for line in written_lines:
            if line != "# " + COMMENT_LINE + "\n" and len(line) > 5:
                if "width_mask_edge" not in line:
                    assert line.split() in self.split_lines(original_lines), line
                else:
                    assert line.split()[1] == "100", line
                    assert line.split() not in self.split_lines(original_lines)

    def test_modify_jobstar_change_datablock(self):
        """modify a parameter in a jobstar data block"""

        # template file
        jobstar_file = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )

        shutil.copy(jobstar_file, os.path.join(self.test_dir, "test_job.star"))

        modify_jobstar(
            jobstar_file, {"_rlnJobTypeLabel": "SomthingElse"}, "changed_job.star"
        )

        # read the two files
        with open(jobstar_file) as original:
            original_lines = original.readlines()
        with open("changed_job.star") as written:
            written_lines = written.readlines()

        # make sure the change has been written
        for line in original_lines:
            if "# version " not in line and len(line) > 5:
                if "_rlnJobTypeLabel" not in line:
                    assert line.split() in self.split_lines(written_lines), line
                else:
                    assert line.split()[1] == "relion.maskcreate", line
                    assert line.split() not in self.split_lines(written_lines), line

        for line in written_lines:
            if line != "# " + COMMENT_LINE + "\n" and len(line) > 5:
                if "_rlnJobTypeLabel" not in line:
                    assert line.split() in self.split_lines(original_lines), line
                else:
                    assert line.split()[1] == "SomthingElse", line
                    assert line.split() not in self.split_lines(original_lines), line


if __name__ == "__main__":
    unittest.main()
