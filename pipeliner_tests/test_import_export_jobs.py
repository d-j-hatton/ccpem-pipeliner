#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    read_pipeline,
    general_get_command_test,
    expected_warning,
    get_relion_tutorial_data,
    tutorial_data_available,
    check_for_relion,
)
from pipeliner.data_structure import (
    IMPORT_MOVIES_NAME,
    JOBSTATUS_SCHED,
    MOTIONCORR_OWN_NAME,
    CTFFIND_GCTF_NAME,
)
from pipeliner.utils import touch
from pipeliner.jobs.relion import motioncorr_job, ctffind_job
from pipeliner.job_factory import active_job_from_proc
from pipeliner.onedep_deposition import expand_nt
from pipeliner.api.api_utils import write_default_jobstar, job_parameters_dict
from pipeliner.api.manage_project import PipelinerProject

OUTPUT_NODE_GENERAL_MICS = "MicrographsData.star.relion"
OUTPUT_NODE_MOVIES = "MicrographMoviesData.star.relion"
OUTPUT_NODE_COORDS_DEP = "MicrographsCoords.star.deprecated_format"


class ExportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_import_export_test(self):
        dirs = [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
        ]
        for d in dirs:
            os.makedirs(d, exist_ok=True)

        files = {
            "Import/job001/": os.path.join(
                self.test_data, "Pipelines/import_job_pipeline.star"
            ),
            "MotionCorr/job002/": os.path.join(
                self.test_data, "Pipelines/motioncorr_job_pipeline.star"
            ),
            "CtfFind/job003/": os.path.join(
                self.test_data, "Pipelines/ctffind_job_pipeline.star"
            ),
        }
        for f in files:
            shutil.copy(files[f], f + "job_pipeline.star")

        import_text = (
            " ++++ Executing new job on Fri May 29 11:25:32 2020"
            "\n++++ with the following command(s):"
            "\necho importing..."
            "\nrelion_star_loopheader rlnMicrographMovieName > "
            "Import/job001/movies.star ls -rt Micrographs/*.mrc >> "
            "Import/job001/movies.star \n++++"
        )

        with open("Import/job001/note.txt", "w") as note:
            note.write(import_text)

        mocorr_text = (
            " ++++ Executing new job on Fri Jul 10 13:59:07 2020"
            "\n++++ with the following command(s):"
            "`which relion_run_motioncorr` --i Import/job001/movies.star --o "
            "MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum -1 --use_own"
            "  --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1 "
            "--preexposure 0 --patch_x 1 --patch_y 1 --group_frames 3 "
            "--gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0  "
            "--pipeline_control MotionCorr/job002/\n++++"
        )

        with open("MotionCorr/job002/note.txt", "w") as note:
            note.write(mocorr_text)

        ctffind_text = (
            " ++++ Executing new job on Fri Jul 10 14:00:58 2020"
            "\n++++ with the following command(s):"
            "\n`which relion_run_ctffind` --i "
            "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
            " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
            "--FStep 500 --dAst 100 --use_gctf --gctf_exe /public/EM"
            '/Gctf/bin/Gctf --ignore_ctffind_params --gpu "" '
            "--pipeline_control CtfFind/job003/\n ++++"
        )

        with open("CtfFind/job003/note.txt", "w") as note:
            note.write(ctffind_text)

    def test_export(self):
        self.setup_import_export_test()
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_sched_noalias_pipeline.star"),
            self.test_dir,
        )
        pipeline = ProjectGraph(name="short_sched_noalias")
        pipeline.export_all_scheduled_jobs("export1")

        assert os.path.isfile("ExportJobs/export1/exported.star")
        assert os.path.isdir("ExportJobs/export1/Import/exp001")
        assert os.path.isdir("ExportJobs/export1/MotionCorr/exp002")
        assert os.path.isdir("ExportJobs/export1/CtfFind/exp003")

        with open("ExportJobs/export1/exported.star") as wrote:
            wrote_pipe = wrote.read()
        for f in ["Import/exp001", "MotionCorr/exp002", "CtfFind/exp003"]:
            assert f in wrote_pipe, f

        job_pipe_stars = {
            "Import/exp001": [
                ["Import/exp001/", "None", IMPORT_MOVIES_NAME, JOBSTATUS_SCHED],
                ["Import/exp001/movies.star", OUTPUT_NODE_MOVIES],
                ["Import/exp001/", "Import/exp001/movies.star"],
            ],
            "MotionCorr/exp002": [
                ["MotionCorr/exp002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SCHED],
                [
                    "MotionCorr/exp002/corrected_micrographs.star",
                    motioncorr_job.OUTPUT_NODE_MICS,
                ],
                ["MotionCorr/exp002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
                ["Import/job001/movies.star", "MotionCorr/exp002/"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/corrected_micrographs.star"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/logfile.pdf"],
            ],
            "CtfFind/exp003": [
                ["CtfFind/exp003/", "None", CTFFIND_GCTF_NAME, JOBSTATUS_SCHED],
                ["CtfFind/exp003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
                ["CtfFind/exp003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
                ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/exp003/"],
            ],
        }
        for exp_line in job_pipe_stars:
            jp_star_path = os.path.join(
                "ExportJobs/export1/", exp_line + "/job_pipeline.star"
            )
            job_pipe_data = read_pipeline(jp_star_path)

            for line in job_pipe_stars[exp_line]:
                assert line in job_pipe_data, line

        with open("ExportJobs/export1/Import/exp001/note.txt") as note:
            note_data = note.read()
        assert "Import/job001" not in note_data
        assert "Import/exp001" in note_data

        with open("ExportJobs/export1/MotionCorr/exp002/note.txt") as note:
            note_data = note.read()
        assert "MotionCorr/job002" not in note_data
        assert "MotionCorr/exp002" in note_data

        with open("ExportJobs/export1/CtfFind/exp003/note.txt") as note:
            note_data = note.read()
        assert "CtfFind/job003" not in note_data
        assert "CtfFind/exp003/" in note_data

    def test_export_with_aliases(self):
        self.setup_import_export_test()
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_sched_pipeline.star"),
            self.test_dir,
        )
        pipeline = ProjectGraph(name="short_sched")
        pipeline.export_all_scheduled_jobs("export1")

        assert os.path.isfile("ExportJobs/export1/exported.star")
        assert os.path.isdir("ExportJobs/export1/Import/exp001")
        assert os.path.isdir("ExportJobs/export1/MotionCorr/exp002")
        assert os.path.isdir("ExportJobs/export1/CtfFind/exp003")

        with open("ExportJobs/export1/exported.star") as wrote:
            wrote_pipe = wrote.read()
        for f in ["Import/exp001", "MotionCorr/exp002", "CtfFind/exp003"]:
            assert f in wrote_pipe, f

        job_pipe_stars = {
            "Import/exp001": [
                ["Import/exp001/", "None", IMPORT_MOVIES_NAME, JOBSTATUS_SCHED],
                ["Import/exp001/movies.star", OUTPUT_NODE_MOVIES],
                ["Import/exp001/", "Import/exp001/movies.star"],
            ],
            "MotionCorr/exp002": [
                ["MotionCorr/exp002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SCHED],
                [
                    "MotionCorr/exp002/corrected_micrographs.star",
                    motioncorr_job.OUTPUT_NODE_MICS,
                ],
                ["MotionCorr/exp002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
                ["Import/job001/movies.star", "MotionCorr/exp002/"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/corrected_micrographs.star"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/logfile.pdf"],
            ],
            "CtfFind/exp003": [
                ["CtfFind/exp003/", "None", CTFFIND_GCTF_NAME, JOBSTATUS_SCHED],
                ["CtfFind/exp003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
                ["CtfFind/exp003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
                ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/exp003/"],
            ],
        }
        for exp_line in job_pipe_stars:
            jp_star_path = os.path.join(
                "ExportJobs/export1/", exp_line + "/job_pipeline.star"
            )

            job_pipe_data = read_pipeline(jp_star_path)
            for line in job_pipe_stars[exp_line]:
                assert line in job_pipe_data, line

        with open("ExportJobs/export1/Import/exp001/note.txt") as note:
            note_data = note.read()
        assert "Import/job001" not in note_data
        assert "Import/exp001" in note_data

        with open("ExportJobs/export1/MotionCorr/exp002/note.txt") as note:
            note_data = note.read()
        assert "MotionCorr/job002" not in note_data
        assert "MotionCorr/exp002" in note_data

        with open("ExportJobs/export1/CtfFind/exp003/note.txt") as note:
            note_data = note.read()
        assert "CtfFind/job003" not in note_data
        assert "CtfFind/exp003/" in note_data

    def test_export_nothing_to_export(self):
        self.setup_import_export_test()
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )
        pipeline = ProjectGraph(name="short")
        pipeline.export_all_scheduled_jobs("export1")

    def test_import_jobs(self):
        self.setup_import_export_test()
        # first export some jobs so there is something to import
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_sched_noalias_pipeline.star"),
            self.test_dir,
        )
        pipeline = ProjectGraph(name="short_sched_noalias")
        pipeline.export_all_scheduled_jobs("export1")

        assert os.path.isfile("ExportJobs/export1/exported.star")
        assert os.path.isdir("ExportJobs/export1/Import/exp001")
        assert os.path.isdir("ExportJobs/export1/MotionCorr/exp002")
        assert os.path.isdir("ExportJobs/export1/CtfFind/exp003")

        with open("ExportJobs/export1/exported.star") as wrote:
            wrote_pipe = wrote.read()
        for f in ["Import/exp001", "MotionCorr/exp002", "CtfFind/exp003"]:
            assert f in wrote_pipe, f

        job_pipe_stars = {
            "Import/exp001": [
                ["Import/exp001/", "None", IMPORT_MOVIES_NAME, JOBSTATUS_SCHED],
                ["Import/exp001/movies.star", OUTPUT_NODE_MOVIES],
                ["Import/exp001/", "Import/exp001/movies.star"],
            ],
            "MotionCorr/exp002": [
                ["MotionCorr/exp002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SCHED],
                [
                    "MotionCorr/exp002/corrected_micrographs.star",
                    motioncorr_job.OUTPUT_NODE_MICS,
                ],
                ["MotionCorr/exp002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
                ["Import/job001/movies.star", "MotionCorr/exp002/"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/corrected_micrographs.star"],
                ["MotionCorr/exp002/", "MotionCorr/exp002/logfile.pdf"],
            ],
            "CtfFind/exp003": [
                ["CtfFind/exp003/", "None", CTFFIND_GCTF_NAME, JOBSTATUS_SCHED],
                ["CtfFind/exp003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
                ["CtfFind/exp003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
                ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/exp003/"],
            ],
        }

        for exp_line in job_pipe_stars:
            jp_star_path = os.path.join(
                "ExportJobs/export1/", exp_line + "/job_pipeline.star"
            )
            job_pipe_data = read_pipeline(jp_star_path)

            for line in job_pipe_stars[exp_line]:
                assert line in job_pipe_data, line

        with open("ExportJobs/export1/Import/exp001/note.txt") as note:
            note_data = note.read()
        assert "Import/job001" not in note_data
        assert "Import/exp001" in note_data

        with open("ExportJobs/export1/MotionCorr/exp002/note.txt") as note:
            note_data = note.read()
        assert "MotionCorr/job002" not in note_data
        assert "MotionCorr/exp002" in note_data

        with open("ExportJobs/export1/CtfFind/exp003/note.txt") as note:
            note_data = note.read()
        assert "CtfFind/job003" not in note_data
        assert "CtfFind/exp003/" in note_data

        # now import the exported jobs
        pipeline.import_jobs("ExportJobs/export1/exported.star")

        pipeline.read()

        allnodes = [
            "Import/job001/movies.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/logfile.pdf",
            "Import/job004/movies.star",
            "Import/job001/movies.star",
            "MotionCorr/job005/corrected_micrographs.star",
            "MotionCorr/job005/logfile.pdf",
            "test.star",
            "CtfFind/job006/micrographs_ctf.star",
            "CtfFind/job006/logfile.pdf",
        ]
        allprocs = [
            "Import/job001/",
            "MotionCorr/job002/",
            "CtfFind/job003/",
            "Import/job004/",
            "MotionCorr/job005/",
            "CtfFind/job006/",
        ]

        nodelist_names = [x.name for x in pipeline.node_list]
        proclist_names = [x.name for x in pipeline.process_list]
        for n in allnodes:
            assert n in nodelist_names, n
        for p in allprocs:
            assert p in proclist_names, p

        new_dirs = [
            "Import/job004/",
            "MotionCorr/job005/",
            "CtfFind/job006/",
        ]
        new_files = [
            "job_pipeline.star",
            "note.txt",
        ]
        for nd in new_dirs:
            for nf in new_files:
                nff = nd + nf
                assert os.path.isfile(nff), nff

        jobs = {
            "Import": [1, 4],
            "MotionCorr": [2, 5],
            "CtfFind": [3, 6],
        }
        for j in jobs:
            assert os.path.isdir("{}/job{:03d}".format(j, jobs[j][0]))
            assert os.path.isdir("{}/job{:03d}".format(j, jobs[j][1]))
            for ft in ["note.txt"]:
                with open("{}/job{:03d}/{}".format(j, jobs[j][0], ft)) as old:
                    old_file = old.read()
                with open("{}/job{:03d}/{}".format(j, jobs[j][1], ft)) as new:
                    new_file = new.read()
                oldjob = "job{:03d}".format(jobs[j][0])
                newjob = "job{:03d}".format(jobs[j][1])

                assert new_file == old_file.replace(oldjob, newjob)
        oldprocs = [
            ["Import/job001/", "None", IMPORT_MOVIES_NAME, JOBSTATUS_SCHED],
            ["MotionCorr/job002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SCHED],
            ["CtfFind/job003/", "None", CTFFIND_GCTF_NAME, JOBSTATUS_SCHED],
        ]
        newprocs = [
            ["Import/job004/", "None", IMPORT_MOVIES_NAME, JOBSTATUS_SCHED],
            ["MotionCorr/job005/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SCHED],
            ["CtfFind/job006/", "None", CTFFIND_GCTF_NAME, JOBSTATUS_SCHED],
        ]
        oldnodes = [
            ["Import/job001/movies.star", OUTPUT_NODE_MOVIES],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                motioncorr_job.OUTPUT_NODE_MICS,
            ],
            ["MotionCorr/job002/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
            ["CtfFind/job003/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
            ["CtfFind/job003/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
        ]
        newnodes = [
            ["Import/job004/movies.star", OUTPUT_NODE_MOVIES],
            [
                "MotionCorr/job005/corrected_micrographs.star",
                motioncorr_job.OUTPUT_NODE_MICS,
            ],
            ["MotionCorr/job005/logfile.pdf", motioncorr_job.OUTPUT_NODE_LOG],
            ["test.star", OUTPUT_NODE_GENERAL_MICS],
            ["CtfFind/job006/micrographs_ctf.star", ctffind_job.OUTPUT_NODE_MICS],
            ["CtfFind/job006/logfile.pdf", ctffind_job.OUTPUT_NODE_LOG],
        ]

        pipe_data = read_pipeline("short_sched_noalias_pipeline.star")
        for node_proc in oldnodes + newnodes + oldprocs + newprocs:
            assert node_proc in pipe_data, node_proc

    def test_import_movies_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        general_get_command_test(
            self,
            "Import",
            "import_movies_relionstyle.job",
            1,
            {},
            {"movies.star": OUTPUT_NODE_MOVIES},
            [
                "relion_import --do_movies --optics_group_name opticsGroup1 "
                "--optics_group_mtf mtf_k2_200kV.star --angpix 0.885 --kV 200"
                " --Cs 1.4 --Q0 0.1 --beamtilt_x 0 --beamtilt_y 0 --i Movies/*.tiff"
                " --odir Import/job001/ --ofile movies.star "
                "--pipeline_control Import/job001/"
            ],
        )

    def test_import_other_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        general_get_command_test(
            self,
            "Import",
            "import_coords_relionstyle.job",
            1,
            {},
            {"coords_suffix.box": OUTPUT_NODE_COORDS_DEP},
            [
                "relion_import --do_coordinates --i mycoords/*.box "
                "--odir Import/job001/ --ofile coords_suffix.box "
                "--pipeline_control Import/job001/"
            ],
        )

    def test_import_movies_prepare_deposition_object(self):

        self.setup_import_export_test()
        # setup the import dirs
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()
        write_default_jobstar("relion.import.movies", "Import/job001/job.star")
        import_job = active_job_from_proc(pipeline.process_list[0])

        with expected_warning(RuntimeWarning, "overflow"):
            result = expand_nt(import_job.prepare_onedep_data())
        expected_result = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                "Import/job001/movies.star; Prepared by ccpem-pipeliner vers 0.0.1",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629_000*_"
                "frameImage.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                "Import/job001/movies.star; Prepared by ccpem-pipeliner vers 0.0.1",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629_000*_"
                "frameImage.mrcs",
            },
        ]
        for i in expected_result:
            assert i in result
        for i in result:
            assert i in expected_result

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_movies_running(self):
        # copy in fake files for the movie stacks
        mn = list(range(21, 32)) + [35, 36, 37] + [39, 40] + list(range(42, 50))
        get_relion_tutorial_data("Import")
        os.makedirs("Movies")
        for n in mn:
            fn = f"Movies/20170629_{n:05d}_frameImage.tiff"
            shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), fn)
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Import/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        efile = os.path.join(self.test_data, "ResultsFiles/import_movies.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected
        assert os.path.isfile("Import/job001/Thumbnails/montage_f100.png")

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_micrographs_running(self):
        get_relion_tutorial_data("MotionCorr")
        # run another import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "MotionCorr/job002/corrected_micrographs.star"
        params["node_type"] = "Micrographs STAR file (.star)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job032/")
        # check the results
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_mics.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_create_display_import_particles(self):
        get_relion_tutorial_data(["Class2D", "Extract"])
        # run another import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "Class2D/job008/run_it025_data.star"
        params["node_type"] = "Particles STAR file (.star)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job032/")
        # check the results
        dispobjs = proj.pipeline.get_process_results_display(impp)
        print(dispobjs[0].__dict__)
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_parts.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_2drefs_running(self):
        get_relion_tutorial_data(["Select", "Class2D"])
        # run another import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "Select/job009/class_averages.star"
        params["node_type"] = "2D references (.star, .mrcs)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job032/")
        # check the results
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_2drefs.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        print(dispobjs[0].__dict__)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_3dref_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["node_type"] = "3D reference (.mrc)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job001/")
        dispobjs = proj.pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_create_display_import_mask(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["node_type"] = "3D mask (.mrc)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job001/")
        dispobjs = proj.pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        print(dispobjs[0].__dict__)
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_halfmap_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["node_type"] = "Unfiltered half-map (unfil.mrc)"
        proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process("Import/job001/")
        dispobjs = proj.pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_mic_running(self):
        get_relion_tutorial_data("MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = mic
        params["node_type"] = "2D micrograph (.mrc)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert dispobjs[0].__dict__ == {
            "title": "Imported 2D micrograph (.mrc)",
            "image_path": "Import/job032/Thumbnails/image.png",
            "image_desc": "Import/job032/20170629_00021_frameImage.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
        }
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile(f"Import/job032/{os.path.basename(mic)}")

    @unittest.skip("There seems to be a relion bug that needs fixing")
    def test_import_parts_files_running(self):
        for n in range(10):
            touch(f"parts{n:03d}_pick.box")

        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = "*_pick.box"
        params["node_type"] = "2D/3D particle coordinates (*.box, *_pick.star)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        os.system("ls -al Import/job001")
        os.system("cat Import/job001/run.err")
        assert dispobjs[0].__dict__ == {
            "title": "! Deprecated particle import !",
            "display_data": (
                "Import successful, but this type of import is not supported"
                "by Relion4/CCPEM-pipeliner\nThis will be updated in a future"
                "Pipeliner version"
            ),
            "associated_data": "Import/job001/coords_suffix.star",
        }
        assert os.path.isfile("Import/job001/coords_suffix.star")

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_coordsstar_running(self):
        get_relion_tutorial_data("MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        shutil.copy(mic, "MotionCorr/job002/micrograph001.mrc")
        ap_file = os.path.join(self.test_data, "autopick.star")
        movs = "AutoPick/job006/Movies/"
        os.makedirs(movs)
        pfile = os.path.join(self.test_data, "micrograph001_autopick.star")
        shutil.copy(pfile, os.path.join(movs, "micrograph001_autopick.star"))
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = ap_file
        params["node_type"] = "Relion coordinates starfile (.star)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "image_path": "Import/job032/Thumbnails/picked_coords.png",
            "image_desc": "MotionCorr/job002/micrograph001.mrc: 242 particles",
            "associated_data": [
                "MotionCorr/job002/micrograph001.mrc",
                "AutoPick/job006/Movies/micrograph001_autopick.star",
            ],
        }, print(dispobjs[0].__dict__)
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile("Import/job032/autopick.star")

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_model_running(self):
        model = os.path.join(self.test_data, "5me2_a.pdb")
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = model
        params["node_type"] = "Atomic model (.pdb, .mmcif, .cif)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert dispobjs[0].__dict__ == {
            "maps": [],
            "maps_opacity": [],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "Model: Import/job001/5me2_a.pdb",
            "maps_data": "",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": ["Import/job001/5me2_a.pdb"],
        }, print(dispobjs[0].__dict__)
        assert os.path.isfile("Import/job001/5me2_a.pdb")

    # TODO: This test will need to be rewritten when a display
    #  object for tomograms is added
    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_tomo_running(self):
        fake_tomo = os.path.join(self.test_data, "image_stack.mrcs")
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = fake_tomo
        params["node_type"] = "Tomogram (.mrc, .mrcs)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert dispobjs[0].__dict__ == {
            "title": "No results display for tomograms yet",
            "display_data": "Fancy 3D viewer for tomograms has not been"
            " implemented yet :(",
            "associated_data": ["Import/job001/image_stack.mrcs"],
        }, print(dispobjs[0].__dict__)
        assert os.path.isfile("Import/job001/image_stack.mrcs")

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_2Dmask_running(self):
        get_relion_tutorial_data("MotionCorr")
        fake_mask = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject()
        params = job_parameters_dict("relion.import.other")
        params["fn_in_other"] = fake_mask
        params["node_type"] = "2D mask (.mrc)"
        job = proj.run_job(jobinput=params)
        impp = proj.pipeline.find_process(job)
        dispobjs = proj.pipeline.get_process_results_display(impp)
        assert dispobjs[0].__dict__ == {
            "title": "Imported 2D mask (.mrc)",
            "image_path": "Import/job032/Thumbnails/image.png",
            "image_desc": "Import/job032/20170629_00021_frameImage.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
        }
        assert os.path.isfile(dispobjs[0].image_path)


if __name__ == "__main__":
    unittest.main()
