#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json
from pipeliner_tests import test_data
from pipeliner import __version__
from pipeliner.project_graph import ProjectGraph
from pipeliner.onedep_deposition import (
    get_procs_list,
    prepare_EMPIAR_mics_parts,
    prepare_EMPIAR_raw_mics,
    EmpiarParticlesType,
    EmpiarCorrectedMicsType,
    EmpiarRefinedParticlesType,
    EmpiarMovieSetType,
    OneDepDeposition,
)
from pipeliner.onedep_deposition import expand_nt
from pipeliner_tests.generic_tests import (
    get_relion_tutorial_data,
    tutorial_data_available,
    do_slow_tests,
    expected_warning,
)

tutorial_data_available = tutorial_data_available()
do_slow = do_slow_tests()


class DepoToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_procs_list(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()
        procslist = get_procs_list("LocalRes/job029/", pipeline)
        expected = [
            "LocalRes/job029/",
            "Refine3D/job027/",
            "Polish/job026/",
            "CtfRefine/job024/",
            "MaskCreate/job022/",
            "Refine3D/job021/",
            "Extract/job020/",
            "Select/job019/",
            "Class3D/job018/",
            "InitialModel/job017/",
            "Select/job016/",
            "Class2D/job015/",
            "Select/job014/",
            "Extract/job012/",
            "AutoPick/job011/",
            "Select/job009/",
            "Class2D/job008/",
            "Extract/job007/",
            "AutoPick/job006/",
            "Select/job005/",
            "ManualPick/job004/",
            "CtfFind/job003/",
            "MotionCorr/job002/",
            "Import/job001/",
        ]
        assert [x.name for x in procslist] == expected

    def test_EMPIAR_raw_mics_mrcs(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        for n in range(1, 25):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_EMPIAR_raw_mics("Import/job001/movies.star")

        expected = {
            "name": "Multiframe micrograph movies",
            "directory": "Import/job001/Movies",
            "category": "('T2', '')",
            "header_format": "('T2', '')",
            "data_format": "('OT', '16-bit float')",
            "num_images_or_tilt_series": 24,
            "frames_per_image": 215,
            "voxel_type": "('OT', '16-bit float')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "Voltage 1.4; Spherical aberration 1.4; "
            "Movie data in file: Import/job001/movies.star; "
            f"Prepared by ccpem-pipeliner vers {__version__}",
            "image_width": 64,
            "image_height": 64,
            "micrographs_file_pattern": "Import/job001/Movies"
            "/20170629_000*_frameImage.mrcs",
        }

        result = expand_nt(depoobjs[0])

        for i in result:
            assert expected[i] == result[i], (i, expected[i], result[i])
        for i in expected:
            assert result[i] == expected[i], (i, expected[i], result[i])

    def test_EMPIAR_raw_mics_mrcs_multiple_dirs(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_EMPIAR_raw_mics("Import/job001/movies.star")

        exp_image_sets = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; "
                "Movie data in file: Import/job001/movies.star; "
                f"Prepared by ccpem-pipeliner vers {__version__}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629"
                "_000*_frameImage.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; "
                "Movie data in file: Import/job001/movies.star; "
                f"Prepared by ccpem-pipeliner vers {__version__}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629"
                "_000*_frameImage.mrcs",
            },
        ]

        for i in depoobjs:
            assert expand_nt(i) in exp_image_sets

    @unittest.skipUnless(tutorial_data_available and do_slow, "slow, needs tutorial")
    def test_EMPIAR_corr_mics(self):
        # setup the import dir
        os.makedirs("CtfFind/job003/")
        os.makedirs("MotionCorr/job002/Movies")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs_ordered.star"),
            os.path.join(self.test_dir, "CtfFind/job003/micrographs_ctf.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "single_100.mrc")
        for n in range(1, 25):
            target = os.path.join(
                self.test_dir,
                f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc",
            )
            shutil.copy(movie_file, target)

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()

        depoobjs = prepare_EMPIAR_mics_parts(
            "Ctffind/job003/micrographs_ctf.star",
            is_parts=False,
            is_cor_parts=False,
        )

        expected = {
            "name": "Corrected micrographs",
            "directory": "MotionCorr/job002/Movies",
            "category": "('T1', '')",
            "header_format": "('T1', '')",
            "data_format": "(T7, '')",
            "num_images_or_tilt_series": 24,
            "frames_per_image": 1,
            "voxel_type": "(T7, '')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "Voltage 1.4; Spherical aberration 1.4; "
            "Image data in file: Ctffind/job003/micrographs_ctf.star; "
            "Prepared by ccpem-pipeliner vers 0.0.1",
            "image_width": 100,
            "image_height": 100,
            "micrographs_file_pattern": "MotionCorr/job002/Movies/"
            "20170629_000*_frameImage.mrc",
        }

        result = expand_nt(depoobjs[0])

        for i in result:
            assert expected[i] == result[i], (i, expected[i], result[i])
        for i in expected:
            assert result[i] == expected[i], (i, expected[i], result[i])

    def test_EMPIAR_parts(self):
        """Parts and Corr parts are essentially the same, only need to test 1"""
        # setup the import dir
        os.makedirs("CtfRefine/job024/")
        os.makedirs("Extract/job007/Movies/")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "class2d_data.star"),
            os.path.join(self.test_dir, "CtfRefine/job024/particles_ctf_refine.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        for n in list(range(21, 32)) + list(range(35, 41)) + list(range(42, 50)):
            target = os.path.join(
                self.test_dir, f"Extract/job007/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Read pipeline STAR file
        pipeline = ProjectGraph()
        pipeline.read()

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_EMPIAR_mics_parts(
                mpfile="CtfRefine/job024/particles_ctf_refine.star",
                is_parts=True,
                is_cor_parts=False,
            )

        expected = {
            "name": "Particle images",
            "directory": "Extract/job007/Movies",
            "category": "('T5', '')",
            "header_format": "('T2', '')",
            "data_format": "('OT', '16-bit float')",
            "num_images_or_tilt_series": 10,
            "frames_per_image": 1,
            "voxel_type": "('OT', '16-bit float')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "2377 total particles; Voltage"
            " 1.4; Spherical aberration 1.4; Image"
            " data in file: CtfRefine/job024/particles"
            "_ctf_refine.star; Prepared by ccpem-pipeliner vers 0.0.1",
            "image_width": 64,
            "image_height": 64,
            "micrographs_file_pattern": "Extract/job007/Movies/20170629_"
            "000*_frameImage.mrcs",
            "picked_particles_file_pattern": "CtfRefine/job024/particl"
            "es_ctf_refine.star",
        }

        result = expand_nt(depoobjs[0])

        for i in result:
            assert expected[i] == result[i], (i, expected[i], result[i])
        for i in expected:
            assert result[i] == expected[i], (i, expected[i], result[i])

    @unittest.skipUnless(tutorial_data_available and do_slow, "slow, needs tutorial")
    def test_full_EMPIAR_deposition_all_types_selected(self):
        with expected_warning(RuntimeWarning, "overflow", nwarn=2):
            get_relion_tutorial_data()

            pipeline = ProjectGraph()
            pipeline.read()
            dep = OneDepDeposition(pipeline, "PostProcess/job026/")

            expected_depobjs = {
                "PostProcess/job026/": [],
                "Refine3D/job025/": [EmpiarParticlesType],
                "CtfRefine/job024/": [EmpiarRefinedParticlesType],
                "CtfRefine/job023/": [EmpiarRefinedParticlesType],
                "CtfRefine/job022/": [EmpiarRefinedParticlesType],
                "MaskCreate/job020/": [],
                "Refine3D/job019/": [EmpiarParticlesType],
                "Extract/job018/": [EmpiarParticlesType],
                "Select/job017/": [],
                "Class3D/job016/": [EmpiarParticlesType],
                "InitialModel/job015/": [],
                "Select/job014/": [],
                "Class2D/job013/": [EmpiarParticlesType],
                "Extract/job012/": [EmpiarParticlesType],
                "AutoPick/job011/": [],
                "CtfFind/job003/": [EmpiarCorrectedMicsType],
                "MotionCorr/job002/": [EmpiarCorrectedMicsType],
                "Import/job001/": [EmpiarMovieSetType],
            }

            for job in dep.procs_depobjs:
                for i in dep.procs_depobjs[job]:
                    assert type(i) in expected_depobjs[job], (job, type(i))

            assert len(dep.upstream) == 18, len(dep.upstream)
            assert (
                dep.terminal_proc.name == "PostProcess/job026/"
            ), dep.terminal_proc.name
            assert dep.final_depo == {}, dep.final_depo

            # prepare the depodict and check it
            final_dep, depdir = dep.prepare_empiar_deposition_dict(
                True, True, True, True
            )

            assert final_dep["experiment_type"] == 3
            assert len(final_dep["imagesets"]) == 4
            expdata = os.path.join(self.test_data, "Metadata/EMPIAR_depo_full.json")
            with open(expdata, "r") as f:
                expected = json.load(f)

            for imst in final_dep["imagesets"]:
                assert imst in expected["imagesets"], imst
            for imst in expected["imagesets"]:
                assert imst in final_dep["imagesets"], imst
            assert final_dep["experiment_type"] == expected["experiment_type"]

            # check the deposition directory was created properly
            dep_files = os.walk(depdir)
            expected = [
                (["Refine3D", "MotionCorr", "Extract", "Import", "CtfRefine"], []),
                (["job025"], []),
                ([], ["run_data.star"]),
                (["job002"], []),
                (["Movies"], []),
                (
                    [],
                    [
                        "20170629_00048_frameImage.mrc",
                        "20170629_00039_frameImage.mrc",
                        "20170629_00031_frameImage.mrc",
                        "20170629_00024_frameImage.mrc",
                        "20170629_00040_frameImage.mrc",
                        "20170629_00023_frameImage.mrc",
                        "20170629_00036_frameImage.mrc",
                        "20170629_00047_frameImage.mrc",
                        "20170629_00037_frameImage.mrc",
                        "20170629_00022_frameImage.mrc",
                        "20170629_00046_frameImage.mrc",
                        "20170629_00025_frameImage.mrc",
                        "20170629_00030_frameImage.mrc",
                        "20170629_00049_frameImage.mrc",
                        "20170629_00043_frameImage.mrc",
                        "20170629_00027_frameImage.mrc",
                        "20170629_00044_frameImage.mrc",
                        "20170629_00035_frameImage.mrc",
                        "20170629_00028_frameImage.mrc",
                        "20170629_00029_frameImage.mrc",
                        "20170629_00045_frameImage.mrc",
                        "20170629_00021_frameImage.mrc",
                        "20170629_00042_frameImage.mrc",
                        "20170629_00026_frameImage.mrc",
                    ],
                ),
                (["job018"], []),
                (["Movies"], []),
                (
                    [],
                    [
                        "20170629_00044_frameImage.mrcs",
                        "20170629_00027_frameImage.mrcs",
                        "20170629_00028_frameImage.mrcs",
                        "20170629_00031_frameImage.mrcs",
                        "20170629_00036_frameImage.mrcs",
                        "20170629_00043_frameImage.mrcs",
                        "20170629_00039_frameImage.mrcs",
                        "20170629_00037_frameImage.mrcs",
                        "20170629_00021_frameImage.mrcs",
                        "20170629_00042_frameImage.mrcs",
                        "20170629_00045_frameImage.mrcs",
                        "20170629_00026_frameImage.mrcs",
                        "20170629_00029_frameImage.mrcs",
                        "20170629_00030_frameImage.mrcs",
                        "20170629_00035_frameImage.mrcs",
                        "20170629_00040_frameImage.mrcs",
                        "20170629_00023_frameImage.mrcs",
                        "20170629_00024_frameImage.mrcs",
                        "20170629_00047_frameImage.mrcs",
                        "20170629_00048_frameImage.mrcs",
                        "20170629_00025_frameImage.mrcs",
                        "20170629_00046_frameImage.mrcs",
                        "20170629_00049_frameImage.mrcs",
                        "20170629_00022_frameImage.mrcs",
                    ],
                ),
                (["job001"], []),
                (["Movies"], []),
                (
                    [],
                    [
                        "20170629_00049_frameImage.tiff",
                        "20170629_00025_frameImage.tiff",
                        "20170629_00046_frameImage.tiff",
                        "20170629_00022_frameImage.tiff",
                        "20170629_00040_frameImage.tiff",
                        "20170629_00023_frameImage.tiff",
                        "20170629_00035_frameImage.tiff",
                        "20170629_00048_frameImage.tiff",
                        "20170629_00024_frameImage.tiff",
                        "20170629_00047_frameImage.tiff",
                        "20170629_00021_frameImage.tiff",
                        "20170629_00042_frameImage.tiff",
                        "20170629_00037_frameImage.tiff",
                        "20170629_00029_frameImage.tiff",
                        "20170629_00030_frameImage.tiff",
                        "20170629_00045_frameImage.tiff",
                        "20170629_00026_frameImage.tiff",
                        "20170629_00028_frameImage.tiff",
                        "20170629_00031_frameImage.tiff",
                        "20170629_00044_frameImage.tiff",
                        "20170629_00027_frameImage.tiff",
                        "20170629_00043_frameImage.tiff",
                        "20170629_00039_frameImage.tiff",
                        "20170629_00036_frameImage.tiff",
                    ],
                ),
                (["job024"], []),
                ([], ["particles_ctf_refine.star"]),
            ]
            for i in dep_files:
                assert tuple(i[1:]) in expected, i

    @unittest.skipUnless(tutorial_data_available and do_slow, "slow, needs tutorial")
    def test_full_EMPIAR_deposition_onlyparts_selected(self):
        with expected_warning(RuntimeWarning, "overflow", nwarn=2):
            get_relion_tutorial_data()

            pipeline = ProjectGraph()
            pipeline.read()
            dep = OneDepDeposition(pipeline, "PostProcess/job026/")

            expected_depobjs = {
                "PostProcess/job026/": [],
                "Refine3D/job025/": [EmpiarParticlesType],
                "CtfRefine/job024/": [EmpiarRefinedParticlesType],
                "CtfRefine/job023/": [EmpiarRefinedParticlesType],
                "CtfRefine/job022/": [EmpiarRefinedParticlesType],
                "MaskCreate/job020/": [],
                "Refine3D/job019/": [EmpiarParticlesType],
                "Extract/job018/": [EmpiarParticlesType],
                "Select/job017/": [],
                "Class3D/job016/": [EmpiarParticlesType],
                "InitialModel/job015/": [],
                "Select/job014/": [],
                "Class2D/job013/": [EmpiarParticlesType],
                "Extract/job012/": [EmpiarParticlesType],
                "AutoPick/job011/": [],
                "CtfFind/job003/": [EmpiarCorrectedMicsType],
                "MotionCorr/job002/": [EmpiarCorrectedMicsType],
                "Import/job001/": [EmpiarMovieSetType],
            }

            for job in dep.procs_depobjs:
                for i in dep.procs_depobjs[job]:
                    assert type(i) in expected_depobjs[job], (job, type(i))

            assert len(dep.upstream) == 18, len(dep.upstream)
            assert (
                dep.terminal_proc.name == "PostProcess/job026/"
            ), dep.terminal_proc.name
            assert dep.final_depo == {}, dep.final_depo

            # prepare the depodict and check it
            # ignore numpy warning
            final_dep, depdir = dep.prepare_empiar_deposition_dict(
                False, False, True, False
            )

            assert final_dep["experiment_type"] == 3
            assert len(final_dep["imagesets"]) == 1
            expdata = os.path.join(self.test_data, "Metadata/EMPIAR_depo_parts.json")
            with open(expdata, "r") as f:
                expected = json.load(f)

            for imst in final_dep["imagesets"]:
                assert imst in expected["imagesets"], imst
            for imst in expected["imagesets"]:
                assert imst in final_dep["imagesets"], imst
            assert final_dep["experiment_type"] == expected["experiment_type"]

            # check the deposition directory was created properly
            dep_files = os.walk(depdir)
            expected = [
                (["Refine3D", "Extract"], []),
                (["job025"], []),
                ([], ["run_data.star"]),
                (["job018"], []),
                (["Movies"], []),
                (
                    [],
                    [
                        "20170629_00044_frameImage.mrcs",
                        "20170629_00027_frameImage.mrcs",
                        "20170629_00028_frameImage.mrcs",
                        "20170629_00031_frameImage.mrcs",
                        "20170629_00036_frameImage.mrcs",
                        "20170629_00043_frameImage.mrcs",
                        "20170629_00039_frameImage.mrcs",
                        "20170629_00037_frameImage.mrcs",
                        "20170629_00021_frameImage.mrcs",
                        "20170629_00042_frameImage.mrcs",
                        "20170629_00045_frameImage.mrcs",
                        "20170629_00026_frameImage.mrcs",
                        "20170629_00029_frameImage.mrcs",
                        "20170629_00030_frameImage.mrcs",
                        "20170629_00035_frameImage.mrcs",
                        "20170629_00040_frameImage.mrcs",
                        "20170629_00023_frameImage.mrcs",
                        "20170629_00024_frameImage.mrcs",
                        "20170629_00047_frameImage.mrcs",
                        "20170629_00048_frameImage.mrcs",
                        "20170629_00025_frameImage.mrcs",
                        "20170629_00046_frameImage.mrcs",
                        "20170629_00049_frameImage.mrcs",
                        "20170629_00022_frameImage.mrcs",
                    ],
                ),
            ]
            for i in dep_files:
                assert tuple(i[1:]) in expected, i


if __name__ == "__main__":
    unittest.main()
