#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import new_job_of_type

from pipeliner_tests.job_testing_tools import (
    job_running_test,
)
from pipeliner_tests.generic_tests import expected_warning

do_full = generic_tests.do_slow_tests()
plugin_present = generic_tests.check_for_plugin("confidenceMap_plugin_job.py")


@unittest.skipIf(not plugin_present, "Skipping because plugin not active")
class confidenceMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="confidencemap")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(do_full, "Slow test: Only runs in full unittest")
    def test_confidence_map(self):
        job_running_test(
            os.path.join(self.test_data, "JobFiles/confidenceMap/FDR.job"),
            [
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            ("run.out", "run.err", "emd_3488_confidenceMap.mrc"),
            sleep_time=10,
        )

    def test_create_results_display(self):
        # make a confidencemap job that looks like it has run
        conjob = new_job_of_type("confidencemap.map_analysis")
        jobname = "ConfidenceMap/job001/"
        conjob.output_name = jobname
        conjob.joboptions["input_map"].value = "emd_3488.mrc"

        # make the output file
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs(jobname)
        shutil.copy(mapfile, f"{jobname}emd_3488_confidenceMap.mrc")

        # create results and check
        with expected_warning(RuntimeWarning, "true_divide"):
            dispobjs = conjob.create_results_display()
        exp_mapmodel = {
            "maps": ["ConfidenceMap/job001/Thumbnails/emd_3488_confidenceMap.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D Confidence map",
            "maps_data": "ConfidenceMap/job001/emd_3488_confidenceMap.mrc",
            "models_data": "",
            "associated_data": ["ConfidenceMap/job001/emd_3488_confidenceMap.mrc"],
        }
        resfile = os.path.join(self.test_data, "ResultsFiles/confidence_map.json")
        with open(resfile, "r") as res:
            exp_montage = json.load(res)

        assert dispobjs[0].__dict__ == exp_montage
        assert dispobjs[1].__dict__ == exp_mapmodel

        assert os.path.isfile(dispobjs[0].img)
        assert os.path.isfile(dispobjs[1].maps[0])


if __name__ == "__main__":
    unittest.main()
