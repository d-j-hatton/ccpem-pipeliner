#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner.job_runner import JobRunner
from pipeliner_tests import generic_tests
from pipeliner_tests import test_data
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.utils import touch
from pipeliner_tests.generic_tests import check_for_relion

do_full = generic_tests.do_slow_tests()
relion_available = check_for_relion()


class JobRunnerScheduleTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_schedule_locked(self):
        touch("RUNNING_PIPELINER_sched_simple_schedule1")
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/sched_simple_pipeline.star"),
            self.test_dir,
        )

        pipeline = JobRunner(project_name="sched_simple")
        with self.assertRaises(ValueError):
            pipeline.run_scheduled_jobs(
                fn_sched="schedule1",
                job_ids=["Import/job002/", "PostProcess/job003/"],
                nr_repeat=1,
                minutes_wait=0,
                minutes_wait_before=0,
                seconds_wait_after=0,
            )

    @unittest.skipUnless(relion_available, "Relion needed for test")
    def test_simple_schedule(self):
        """import a mask and run PostProcess"""
        # Prepare the directory structure as if Refine3D jobs have been run

        fake_refine3D = os.path.join(self.test_dir, "Refine3D/job001")
        os.makedirs(fake_refine3D)

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            fake_refine3D,
        )

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            fake_refine3D,
        )
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        pp_dir = os.path.join(self.test_dir, "PostProcess/job003/")
        os.makedirs(pp_dir)
        import_dir = os.path.join(self.test_dir, "Import/job002/")
        os.makedirs(import_dir)

        shutil.copy(
            os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess_for_sched_job.star"
            ),
            os.path.join(pp_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Import/import_mask_job.star"),
            os.path.join(import_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/sched_simple_pipeline.star"),
            self.test_dir,
        )

        pipeline = JobRunner(project_name="sched_simple")
        pipeline.run_scheduled_jobs(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=1,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=0,
        )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        for f in import_files:
            ff = os.path.join(import_dir, f)
            assert os.path.isfile(ff), ff
        for f in pp_files:
            ff = os.path.join(pp_dir, f)
            assert os.path.isfile(ff), ff

        with open("pipeline_schedule1.log") as logfile:
            log_data = logfile.read()

        loglines = [
            "-- Starting repeat 1/1",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ performed all requested repeats in scheduler schedule1. Stopping "
            "pipeliner now ...",
        ]
        for line in loglines:
            assert line in log_data

    @unittest.skipUnless(
        do_full and relion_available, "Slow test: Only runs in full unittest"
    )
    def test_simple_schedule_with_repeats(self):
        """import a mask and run PostProcess do it three times to test repeating"""
        # Prepare the directory structure as if Refine3D jobs have been run

        fake_refine3D = os.path.join(self.test_dir, "Refine3D/job001")
        os.makedirs(fake_refine3D)

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            fake_refine3D,
        )

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            fake_refine3D,
        )
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        pp_dir = os.path.join(self.test_dir, "PostProcess/job003/")
        os.makedirs(pp_dir)
        import_dir = os.path.join(self.test_dir, "Import/job002/")
        os.makedirs(import_dir)

        shutil.copy(
            os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess_for_sched_job.star"
            ),
            os.path.join(pp_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Import/import_mask_job.star"),
            os.path.join(import_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/sched_simple_pipeline.star"),
            self.test_dir,
        )

        with open("PostProcess/job003/job.star") as original_jobstar:
            original_js_data = [x.split() for x in original_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "0"] in original_js_data
        assert ["_rlnJobIsContinue", "1"] not in original_js_data

        pipeline = JobRunner(project_name="sched_simple")
        pipeline.run_scheduled_jobs(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=3,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=1,
        )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        for f in import_files:
            ff = os.path.join(import_dir, f)
            assert os.path.isfile(ff), ff
        for f in pp_files:
            ff = os.path.join(pp_dir, f)
            assert os.path.isfile(ff), ff

        with open("pipeline_schedule1.log") as logfile:
            log_data = logfile.read()

        loglines = [
            "-- Starting repeat 1/3",
            "-- Starting repeat 2/3",
            "-- Starting repeat 3/3",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ performed all requested repeats in scheduler schedule1. Stopping "
            "pipeliner now ...",
        ]

        with open("PostProcess/job003/job.star") as final_jobstar:
            final_js_data = [x.split() for x in final_jobstar.readlines()]

        os.system("grep '_rlnJobIsContinue' -b 2 */*/job.star")

        assert ["_rlnJobIsContinue", "1"] in final_js_data
        assert ["_rlnJobIsContinue", "0"] not in final_js_data

        # test that postprocess actually ran three times
        linecount = 0
        with open("PostProcess/job003/run.out") as pp_runout:
            pp_data = pp_runout.readlines()
        for line in pp_data:
            if "+ FINAL RESOLUTION:" in line:
                linecount += 1
        assert linecount == 3

        for line in loglines:
            assert line in log_data, line

    @unittest.skipUnless(
        do_full and relion_available, "Slow test: Only runs in full unittest"
    )
    def test_simple_schedule_with_repeats_nonzero_waititime(self):
        """import a mask and run PostProcess do it three times to test repeating
        one minute wait between jobs to make sure it works"""
        # Prepare the directory structure as if Refine3D jobs have been run

        fake_refine3D = os.path.join(self.test_dir, "Refine3D/job001")
        os.makedirs(fake_refine3D)

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            fake_refine3D,
        )

        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            fake_refine3D,
        )
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        pp_dir = os.path.join(self.test_dir, "PostProcess/job003/")
        os.makedirs(pp_dir)
        import_dir = os.path.join(self.test_dir, "Import/job002/")
        os.makedirs(import_dir)

        shutil.copy(
            os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess_for_sched_job.star"
            ),
            os.path.join(pp_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Import/import_mask_job.star"),
            os.path.join(import_dir, "job.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/sched_simple_pipeline.star"),
            self.test_dir,
        )

        with open("PostProcess/job003/job.star") as original_jobstar:
            original_js_data = [x.split() for x in original_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "0"] in original_js_data
        assert ["_rlnJobIsContinue", "1"] not in original_js_data

        pipeline = JobRunner(project_name="sched_simple")
        pipeline.run_scheduled_jobs(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=3,
            minutes_wait=1,
            minutes_wait_before=0,
            seconds_wait_after=1,
        )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        for f in import_files:
            ff = os.path.join(import_dir, f)
            assert os.path.isfile(ff), ff
        for f in pp_files:
            ff = os.path.join(pp_dir, f)
            assert os.path.isfile(ff), ff

        with open("pipeline_schedule1.log") as logfile:
            log_data = logfile.read()

        loglines = [
            "-- Starting repeat 1/3",
            "-- Starting repeat 2/3",
            "-- Starting repeat 3/3",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ performed all requested repeats in scheduler schedule1. Stopping "
            "pipeliner now ...",
        ]
        with open("PostProcess/job003/job.star") as final_jobstar:
            final_js_data = [x.split() for x in final_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "1"] in final_js_data
        assert ["_rlnJobIsContinue", "0"] not in final_js_data

        # test that postprocess actually ran three times
        linecount = 0
        with open("PostProcess/job003/run.out") as pp_runout:
            pp_data = pp_runout.readlines()
        for line in pp_data:
            if "+ FINAL RESOLUTION:" in line:
                linecount += 1
        assert linecount == 3

        for line in loglines:
            assert line in log_data, line


if __name__ == "__main__":
    unittest.main()
