#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.jobs.relion.bayesianpolish_job import (
    INPUT_NODE_POLISHPARAMS,
    INPUT_NODE_MICS,
    INPUT_NODE_PARTS,
    INPUT_NODE_POST,
    OUTPUT_NODE_POLISHPARAMS,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_LOG,
)


class BayesianPolishTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_bayesian_train(self):
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_train.job",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"opt_params_all_groups.txt": OUTPUT_NODE_POLISHPARAMS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --min_p 4000 --eval_frac 0.5 "
                "--align_frac 0.5 --params3 --j 16 --pipeline_control Polish/job014/"
            ],
        )

    def test_get_command_bayesian_train_relionstyle_jobname(self):
        """Check conversion of ambiguous jobname"""
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_train_relionstyle.job",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"opt_params_all_groups.txt": OUTPUT_NODE_POLISHPARAMS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --min_p 4000 --eval_frac 0.5 "
                "--align_frac 0.5 --params3 --j 16 --pipeline_control Polish/job014/"
            ],
        )

    def test_get_command_bayesian_polish_own_params(self):
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_polish_ownparam.job",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"logfile.pdf": OUTPUT_NODE_LOG, "shiny.star": OUTPUT_NODE_PARTS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --s_vel 0.2 --s_div 5000 "
                "--s_acc 2 --combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16"
                " --pipeline_control Polish/job014/"
            ],
        )

    def test_get_command_bayesian_polish_jobstar(self):
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_job.star",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"logfile.pdf": OUTPUT_NODE_LOG, "shiny.star": OUTPUT_NODE_PARTS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --s_vel 0.2 --s_div 5000 "
                "--s_acc 2 --combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16"
                " --pipeline_control Polish/job014/"
            ],
        )

    def test_get_command_bayesian_polish_param_file(self):
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_polish_paramfile.job",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "paramfile.txt": INPUT_NODE_POLISHPARAMS,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"logfile.pdf": OUTPUT_NODE_LOG, "shiny.star": OUTPUT_NODE_PARTS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --params_file paramfile.txt "
                "--combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16 "
                "--pipeline_control Polish/job014/"
            ],
        )

    def test_get_command_bayesian_polish_continue(self):
        general_get_command_test(
            self,
            "Polish",
            "bayesianpolish_polish_continue.job",
            14,
            {
                "Refine3D/job025/run_data.star": INPUT_NODE_PARTS,
                "PostProcess/job026/postprocess.star": INPUT_NODE_POST,
                "paramfile.txt": INPUT_NODE_POLISHPARAMS,
                "MotionCorr/job002/corrected_micrographs.star": INPUT_NODE_MICS,
            },
            {"logfile.pdf": OUTPUT_NODE_LOG, "shiny.star": OUTPUT_NODE_PARTS},
            [
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job014/ --params_file paramfile.txt "
                "--combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 "
                "--only_do_unfinished --j 16 --pipeline_control Polish/job014/"
            ],
        )

    def test_create_display_training_nofiles(self):
        pipeline = os.path.join(self.test_data, "Pipelines/relion40_tutorial.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/Polish/bayesianpolish_job.star"
        )
        os.makedirs("Polish/job027/")
        shutil.copy(pipeline, "default_pipeline.star")
        shutil.copy(jobstar, "Polish/job027/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Polish/job027/")
        dispobjs = pipeline.get_process_results_display(proc)
        res = "[Errno 2] No such file or directory: " "'Polish/job027/bfactors.star'"
        expected = {
            "title": "Results pending...",
            "message": "The result not available yet",
            "reason": res,
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_training(self):
        get_relion_tutorial_data(["Polish", "Refine3D"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Polish/job027/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected_dict = {
            "title": "Trained Polishing parameters",
            "headers": [
                "Optics Group",
                "Sigma velocity (\u212b/dose)",
                "Sigma divergence (\u212b)",
                "Sigma for acceleration",
            ],
            "table_data": [["opticsGroup1", "0.4035", "1155", "2.715"]],
            "associated_data": ["Polish/job027/opt_params_all_groups.txt"],
        }
        assert dispobjs[0].__dict__ == expected_dict

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_polishing(self):
        get_relion_tutorial_data("Polish")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Polish/job028/")
        dispobjs = pipeline.get_process_results_display(proc)
        bffile = os.path.join(
            self.test_data, "ResultsFiles/polish_particles_bfactor.json"
        )
        with open(bffile, "r") as bf:
            exp_bf = json.load(bf)
        assert dispobjs[0].__dict__ == exp_bf
        gfile = os.path.join(
            self.test_data, "ResultsFiles/polish_particles_guinier.json"
        )
        with open(gfile, "r") as gf:
            exp_guinier = json.load(gf)
        assert dispobjs[1].__dict__ == exp_guinier


if __name__ == "__main__":
    unittest.main()
