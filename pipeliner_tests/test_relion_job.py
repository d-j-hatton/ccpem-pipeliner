#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner import data_structure, job_factory
from pipeliner.jobs.relion import relion_job
from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.jobs.relion.import_job import (
    OUTPUT_NODE_HALFMAP,
    OUTPUT_NODE_MASK3D,
    make_node_name,
)
from pipeliner_tests.generic_tests import print_coms
from pipeliner.job_factory import new_job_of_type


class RelionJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_relion_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            relion_job.RelionJob()

    def test_reading_import_mask_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_name == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 3
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert job.joboptions["fn_in_other"].value == "emd_3488_mask.mrc"
        assert job.joboptions["node_type"].label == "Node type:"
        assert job.joboptions["node_type"].value == "3D mask (.mrc)"

    def test_reading_import_map_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_name == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 3
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert job.joboptions["fn_in_other"].value == "emd_3488.mrc"
        assert job.joboptions["node_type"].label == "Node type:"
        assert job.joboptions["node_type"].value == "3D reference (.mrc)"

    def test_reading_import_halfmap_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_halfmaps.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_name == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 3
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert (
            job.joboptions["fn_in_other"].value == "3488_run_half1_class001_unfil.mrc"
        )
        assert job.joboptions["node_type"].label == "Node type:"
        assert job.joboptions["node_type"].value == "Unfiltered half-map (unfil.mrc)"

    def test_get_import_mask_commands(self):
        generic_tests.general_get_command_test(
            self,
            "Import",
            "import_mask.job",
            1,
            {},
            {"emd_3488_mask.mrc": make_node_name(OUTPUT_NODE_MASK3D, ".mrc")},
            [
                "relion_import --do_other --i emd_3488_mask.mrc "
                "--odir Import/job001/ --ofile emd_3488_mask.mrc "
                "--pipeline_control Import/job001/"
            ],
        )

    def test_get_import_halfmaps_commands(self):
        generic_tests.general_get_command_test(
            self,
            "Import",
            "import_halfmaps.job",
            1,
            {},
            {
                "3488_run_half1_class001_unfil.mrc": make_node_name(
                    OUTPUT_NODE_HALFMAP, ".mrc"
                )
            },
            [
                "relion_import --do_halfmaps --i "
                "3488_run_half1_class001_unfil.mrc --odir Import/job001/ --ofile "
                "3488_run_half1_class001_unfil.mrc --pipeline_control Import/job001/"
            ],
        )

    def test_get_import_halfmaps_jobstar(self):
        generic_tests.general_get_command_test(
            self,
            "Import",
            "import_halfmaps_job.star",
            1,
            {},
            {
                "3488_run_half1_class001_unfil.mrc": make_node_name(
                    OUTPUT_NODE_HALFMAP, ".mrc"
                )
            },
            [
                "relion_import --do_halfmaps --i "
                "3488_run_half1_class001_unfil.mrc --odir Import/job001/ --ofile "
                "3488_run_half1_class001_unfil.mrc --pipeline_control Import/job001/"
            ],
        )

    def test_get_import_map_commands(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )

        outputname = "Import/job001/"
        job.output_name = outputname
        commandlist = job.get_commands()
        commands = job.prepare_final_command(outputname, commandlist, True)[0]

        expected_commands = [
            "time -p relion_import --do_other --i emd_3488.mrc "
            "--odir Import/job001/ --ofile emd_3488.mrc "
            "--pipeline_control Import/job001/"
        ]
        assert commands[0] == expected_commands[0].split(), print_coms(
            commands[0], expected_commands[0]
        )

    def test_import_job_directory_creation_with_default_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "Import/job023/")
        assert not os.path.isdir(job_dir)

        outputname = "Import/job023/"
        job.output_name = outputname
        commandlist = job.get_commands()
        job.prepare_final_command(outputname, commandlist, True)

        assert os.path.isdir(job_dir)

    def test_import_job_directory_creation_with_defined_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "job_directory/")
        assert not os.path.isdir(job_dir)

        job.output_name = job_dir
        commandlist = job.get_commands()
        job.prepare_final_command(job_dir, commandlist, True)
        assert os.path.isdir(job_dir)

    def test_import_job_movies_parameter_validation(self):
        job = new_job_of_type("relion.import.movies")
        job.joboptions["fn_in_raw"].value = "particles.star"
        job.joboptions["optics_group_name"].value = "@pticGroups!"
        exp_err = [
            (
                "error",
                "Value format must match pattern '^[a-zA-Z0-9_]+$'",
                "Optics group name:",
            )
        ]
        errors = job.validate_joboptions()
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_err
        for i in exp_err:
            assert i in formatted_errors

    def test_import_job_additional_parameter_validation(self):
        job = new_job_of_type("relion.import.other")
        job.joboptions["node_type"].value = "Particles STAR file (.star)"
        job.joboptions["fn_in_other"].value = "particles.star"
        job.joboptions["optics_group_particles"].value = "@pticGroups!"
        # the 2nd error is extranious but is just there for demo purposes
        # to do remove this once it is removed

        exp_err = [
            (
                "error",
                "Value format must match pattern '^[a-zA-Z0-9_]*$'",
                "Rename optics group for particles:",
            ),
        ]

        errors = job.validate_joboptions()
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_err
        for i in exp_err:
            assert i in formatted_errors


if __name__ == "__main__":
    unittest.main()
