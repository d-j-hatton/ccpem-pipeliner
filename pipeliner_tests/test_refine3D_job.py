#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.jobs.relion.refine3D_job import (
    INPUT_NODE_MAP,
    INPUT_NODE_MASK,
    INPUT_NODE_PARTS,
    INPUT_NODE_PARTS_HELIX,
    OUTPUT_NODE_MAP,
    OUTPUT_NODE_OPT,
    OUTPUT_NODE_HALFMAP,
    OUTPUT_NODE_PARTS,
    OUTPUT_NODE_PARTS_HELIX,
)
from pipeliner.project_graph import ProjectGraph


class Refine3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ["RELION_SCRATCH_DIR"] = ""

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_refine3D_basic(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job018/particles.star --o Refine3D/job012/run --auto_refine"
                " --split_random_halves --ref Class3D/job016/run_it025_class001"
                "_box256.mrc --firstiter_cc --ini_high 50 --dont_combine_weights_via"
                "_disc --preread_images --pool 30 --pad 2 --skip_gridding --ctf "
                "--particle_diameter 200 --flatten_solvent --zero_mask "
                "--oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 "
                "--norm --scale --j 6 --gpu 4:5:6:7 --pipeline_control "
                "Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_jobstar(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3d_job.star",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_samplingcheck(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_sampling.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.sta"
                "r --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 4 --auto_local_healpix_order 7 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_mask(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_mask.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
                "MaskCreate/job200/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_mask_solvflat(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_mask_solvflat.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
                "MaskCreate/job200/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --solvent_mask MaskCreate/job2"
                "00/mask.mrc --solvent_correct_fsc --oversampling 1 --healpix_order 2 "
                "--auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D"
                "2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_absgrey(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_absgrey.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_ctf1stpeak(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_ctf1stpeak.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --ctf_intact_fi"
                "rst_peak --particle_diameter 200 --flatten_solvent --zero_mask --over"
                "sampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_ra"
                "nge 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_finerfaster(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_finerfaster.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --auto_ignore_angles --auto_resol_angles "
                "--ctf --particle_diameter"
                " 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym "
                "D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_relionstyle_jobname(self):
        """Make sure an ambiguous relion style job name is converted"""
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical_relionstyle.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_noprior(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical_noprior.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --j 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_noHsym(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical_noHsym.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 "
                "--ignore_helical_symmetry --sigma_tilt 5 --sigma_psi 3.33333 --sigma_"
                "rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_search(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical_search.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --helical_symmetry_search --helical_twist_"
                "min 20 --helical_twist_max 30 --helical_twist_inistep 1 --helical_ris"
                "e_min 3 --helical_rise_max 4.5 --helical_rise_inistep 0.25 --sigma_ti"
                "lt 5 --sigma_psi 3.33333 --sigma_rot"
                " 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_sigmarot(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_helical_sigmarot.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS_HELIX,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS_HELIX,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_sigma_distance 5 --helical_keep_tilt_prior_fixed --j"
                " 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_nompiGPU(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_nompiGPU.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "relion_refine --i Extract/job018/particles.star "
                "--o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 "
                "--pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_2masks(self):
        general_get_command_test(
            self,
            "Refine3D",
            "refine3D_2masks.job",
            12,
            {
                "Extract/job018/particles.star": INPUT_NODE_PARTS,
                "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
                "MaskCreate/job200/mask.mrc": INPUT_NODE_MASK,
                "MaskCreate/job013/mask.mrc": INPUT_NODE_MASK,
            },
            {
                "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                "run_data.star": OUTPUT_NODE_PARTS,
                "run_optimiser.star": OUTPUT_NODE_OPT,
            },
            [
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --solvent_mask2 MaskCreate/job013/mask.mrc --pipeline_control "
                "Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_continue_nofile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "Refine3D",
                "refine3D_continue_nofile.job",
                12,
                2,
                3,
                "",
            )

    def test_get_command_refine3D_continue(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                self,
                "Refine3D",
                "refine3D_continue_nofile.job",
                12,
                {
                    "Extract/job018/particles.star": INPUT_NODE_PARTS,
                    "Class3D/job016/run_it025_class001_box256.mrc": INPUT_NODE_MAP,
                },
                {
                    "run_class001.mrc": OUTPUT_NODE_MAP + ".d2sym",
                    "run_half1_class001_unfil.mrc": OUTPUT_NODE_HALFMAP + ".d2sym",
                    "run_data.star": OUTPUT_NODE_PARTS,
                    "run_optimiser.star": OUTPUT_NODE_OPT,
                },
                [
                    "mpirun -n 5 relion_refine_mpi --continue "
                    "run_ct01_it003_optimiser.star --o Refine3D/job011/run_ct3 "
                    "--dont_combine_weights_via_disc --preread_images  --pool 3 --pad "
                    "2 --skip_gridding  --particle_diameter 200 --j 6 --gpu 4:5:6:7"
                    "--pipeline_control Refine3D/job012/"
                ],
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_refine3d_generate_results(self):
        get_relion_tutorial_data(["Refine3D"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Refine3D/job019/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "maps": ["Refine3D/job019/Thumbnails/run_class001.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Refine3D/job019/run_class001.mrc: 3.89 Å",
            "maps_data": "Refine3D/job019/run_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_class001.mrc"],
        }

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_refine3d_generate_results_not_finished(self):
        get_relion_tutorial_data(["Refine3D"])
        os.remove("Refine3D/job019/run_class001.mrc")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Refine3D/job019/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "maps": ["Refine3D/job019/Thumbnails/run_it011_half1_class001.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "title": "Refine3D/job019/run_it011_half1_class001.mrc: 7.59 Å",
            "maps_data": "Refine3D/job019/run_it011_half1_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_it011_half1_class001.mrc"],
        }


if __name__ == "__main__":
    unittest.main()
