#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
import tempfile
import unittest

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import generic_tests
from pipeliner_tests import test_data

from pipeliner.jobs.relion import (
    extract_job,
    refine3D_job,
    ctffind_job,
    motioncorr_job,
)
from pipeliner.jobs.relion import (
    class2D_job,
    initialmodel_job,
    import_job,
    maskcreate_job,
    manualpick_job,
    bayesianpolish_job,
    class3D_job,
    autopick_job,
    select_job,
    ctfrefine_job,
)

from pipeliner.flowchart_illustration import ProcessFlowchart


do_full = generic_tests.do_slow_tests()
do_interactive = generic_tests.do_interactive_tests()


@unittest.skipUnless(do_full, "Slow test, only do in full")
class ProjectGraphTestFlowcharts(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # copy in a few test files for particle counts
        # dirs[item][0] = the name of the file in test_data
        # dirs[item][1] = the name when it"s copied over
        dirs = {
            "Import/job001": ("import1.star", "movies.star"),
            "MotionCorr/job002": ("mocorr2.star", "corrected_micrographs.star"),
            "CtfFind/job003": ("ctffind3.star", "micrographs_ctf.star"),
            "Select/job005": ("select5.star", "micrographs_selected.star"),
            "Extract/job007": ("extract7.star", "particles.star"),
            "Extract/job012": ("extract12.star", "particles.star"),
            "Select/job014": ("extract12.star", "particles.star"),
            "Select/job016": ("select16.star", "particles.star"),
            "Select/job019": ("select19.star", "particles.star"),
            "Extract/job020": ("extract20.star", "particles.star"),
            "Refine3D/job021": ("refine3d21.star", "run_data.star"),
            "CtfRefine/job024": ("refine3d21.star", "particles_ctf_refine.star"),
            "Polish/job026": ("refine3d21.star", "shiny.star"),
        }
        flowchart_dir = os.path.join(self.test_data, "Flowchart_data")
        for adir in dirs:
            os.makedirs(adir, exist_ok=True)
            thefile = os.path.join(flowchart_dir, dirs[adir][0])
            shutil.copy(thefile, os.path.join(adir, dirs[adir][1]))

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_tutorial_whole_pipeline_show(self):
        """Show the interactve flowchart for the entire project"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            do_full=True,
            show=True,
        )
        flowchart.full_process_graph()
        assert flowchart.drew_full

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_fullfunction_whole_pipeline_show(self):
        """Show the interactve flowchart for the entire project
        do the drawing at the initilization of the class rather than
        testing the individual function"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            do_full=True,
            show=True,
        )
        assert flowchart.drew_full

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_flowchart_tutorial_whole_pipeline_save(self):
        """Save the flowchart for the entire project"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        flowchart = ProcessFlowchart(pipeline=pipeline, do_full=True, save=True)
        flowchart.full_process_graph()
        assert flowchart.drew_full
        assert os.path.isfile("full_project_flowchart.png")

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_flowchart_fullfunction_whole_pipeline_save(self):
        """Save the flowchart for the entire project
        do the drawing at the initialisation of the class rather than
        testing the individual function"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()

        flowchart = ProcessFlowchart(pipeline=pipeline, do_full=True, save=True)
        flowchart.full_process_graph()
        assert flowchart.drew_full
        assert os.path.isfile("full_project_flowchart.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_tutorial_job011_pipeline_show(self):
        """Show an interactive downstream graph"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[10]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            show=True,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_flowchart_tutorial_job011_pipeline_save(self):
        """Save a downstream graph"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[10]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            save=True,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down
        assert os.path.isfile("AutoPick_job011_child.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_tutorial_job017_pipeline_show(self):
        "Another test of downstream graphs"
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[15]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            show=True,
            save=False,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down

    def test_flowchart_tutorial_job017_pipeline_save(self):
        "Another test of saving downstream graphs"
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[15]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            save=True,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down
        assert os.path.isfile("InitialModel_job017_child.png")

    def test_flowchart_downstream_no_jobs(self):
        """no downstream jobs, so don't draw anything"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[9]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            show=True,
        )
        flowchart.downstream_process_graph()
        assert not flowchart.drew_down

    def test_flowchart_no_upstream_jobs(self):
        """No upstream jobs, so don't draw anything"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[0]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            show=True,
        )
        flowchart.upstream_process_graph()
        assert not flowchart.drew_up

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_very_large_downstream_show(self):
        "interactive large downstream plot"
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[1]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            show=True,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down

    @unittest.skipUnless(do_full, "Slow test: only run in full unittest")
    def test_flowchart_very_large_downstream_save(self):
        "save large downstream plot"
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[1]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            save=True,
        )
        flowchart.downstream_process_graph()
        assert flowchart.drew_down
        assert os.path.isfile("MotionCorr_job002_child.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_flowchart_tutorial_downstream_fullfunction_show(self):
        """Dothe drawing at initialization of the class rather than
        testing the individual function"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[20]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            show=True,
        )
        assert flowchart.drew_down

    def test_flowchart_tutorial_downstream_fullfunction_save(self):
        """Do the drawing at initialization of the class rather than
        testing the individual function, save an image"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[19]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_downstream=True,
            save=True,
        )
        assert flowchart.drew_down
        assert os.path.isfile("Refine3D_job021_child.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_upstream_fullfunction_show(self):
        """Do the drawing at initialization of the class rather than
        testing the individual function"""

        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[1]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            show=True,
        )
        assert flowchart.drew_up

    def test_upstream_fullfunction_save(self):
        """Do the drawing at initialization of the class rather than
        testing the individual function, save the result"""

        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[1]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            save=True,
        )
        assert flowchart.drew_up
        assert os.path.isfile("MotionCorr_job002_parent.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_upstream_fullfunction_save_and_show(self):
        """Do the drawing at initialization of the class rather than
        testing the individual function, save the result and show
        the interactve"""

        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[1]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            save=True,
            show=True,
        )
        assert flowchart.drew_up
        assert os.path.isfile("MotionCorr_job002_parent.png")

    @unittest.skipUnless(do_interactive, "Interactive: only run in interactive mode")
    def test_upstream_flowchart_tutorial_job021_pipeline_show(self):
        """Show a large interactive upstream"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )
        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[19]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            show=True,
        )
        assert flowchart.drew_up

    def test_upstream_flowchart_tutorial_job021_pipeline_save(self):
        """Save a large upstream"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )
        # Read pipeline STAR file
        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[19]
        flowchart = ProcessFlowchart(
            pipeline=pipeline,
            process=proc_name,
            do_upstream=True,
            save=True,
        )
        assert flowchart.drew_up
        assert os.path.isfile("Refine3D_job021_parent.png")

    def test_getting_downstream_network(self):
        """Just get the downstream network info as a list for other applications"""

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[10]
        downstream_list = pipeline.get_downstream_network(proc_name)
        formatted = [
            (x[0].name, x[0].type, x[1].name, x[2].name) for x in downstream_list
        ]
        expected_list = [
            (
                "AutoPick/job011/coords_suffix_autopick.star",
                autopick_job.OUTPUT_NODE_COORDS,
                "AutoPick/job011/",
                "Extract/job012/",
                None,
            ),
            (
                "Extract/job012/particles.star",
                extract_job.OUTPUT_NODE_PARTS,
                "Extract/job012/",
                "Select/job014/",
                9482,
            ),
            (
                "Select/job014/particles.star",
                select_job.OUTPUT_NODE_PARTS,
                "Select/job014/",
                "Class2D/job015/",
                9482,
            ),
            (
                "Class2D/job015/run_it025_optimiser.star",
                class2D_job.OUTPUT_NODE_OPT,
                "Class2D/job015/",
                "Select/job016/",
                None,
            ),
            (
                "Select/job016/particles.star",
                select_job.OUTPUT_NODE_PARTS,
                "Select/job016/",
                "InitialModel/job017/",
                5367,
            ),
            (
                "Select/job016/particles.star",
                select_job.OUTPUT_NODE_PARTS,
                "Select/job016/",
                "Class3D/job018/",
                5367,
            ),
            (
                "Class3D/job018/run_it025_optimiser.star",
                class3D_job.OUTPUT_NODE_OPT,
                "Class3D/job018/",
                "Select/job019/",
                None,
            ),
            (
                "Select/job019/particles.star",
                select_job.OUTPUT_NODE_PARTS,
                "Select/job019/",
                "Extract/job020/",
                4501,
            ),
            (
                "Extract/job020/particles.star",
                extract_job.OUTPUT_NODE_PARTS,
                "Extract/job020/",
                "Refine3D/job021/",
                4501,
            ),
            (
                "Refine3D/job021/run_class001.mrc",
                refine3D_job.OUTPUT_NODE_MAP,
                "Refine3D/job021/",
                "MaskCreate/job022/",
                None,
            ),
            (
                "MaskCreate/job022/mask.mrc",
                maskcreate_job.OUTPUT_NODE_MASK,
                "MaskCreate/job022/",
                "PostProcess/job023/",
                None,
            ),
            (
                "Refine3D/job021/run_data.star",
                refine3D_job.OUTPUT_NODE_PARTS,
                "Refine3D/job021/",
                "CtfRefine/job024/",
                4501,
            ),
            (
                "CtfRefine/job024/particles_ctf_refine.star",
                ctfrefine_job.OUTPUT_NODE_PARTS,
                "CtfRefine/job024/",
                "Polish/job025/",
                4501,
            ),
            (
                "CtfRefine/job024/particles_ctf_refine.star",
                ctfrefine_job.OUTPUT_NODE_PARTS,
                "CtfRefine/job024/",
                "Polish/job026/",
                4501,
            ),
            (
                "Polish/job026/shiny.star",
                bayesianpolish_job.OUTPUT_NODE_PARTS,
                "Polish/job026/",
                "Refine3D/job027/",
                4501,
            ),
            (
                "MaskCreate/job022/mask.mrc",
                maskcreate_job.OUTPUT_NODE_MASK,
                "MaskCreate/job022/",
                "PostProcess/job028/",
                None,
            ),
            (
                "Refine3D/job027/run_half1_class001_unfil.mrc",
                refine3D_job.OUTPUT_NODE_HALFMAP,
                "Refine3D/job027/",
                "LocalRes/job029/",
                None,
            ),
            (
                "Refine3D/job027/run_half1_class001_unfil.mrc",
                refine3D_job.OUTPUT_NODE_HALFMAP,
                "Refine3D/job027/",
                "PostProcess/job028/",
                None,
            ),
            (
                "Refine3D/job021/run_class001.mrc",
                refine3D_job.OUTPUT_NODE_MAP,
                "Refine3D/job021/",
                "Refine3D/job027/",
                None,
            ),
            (
                "MaskCreate/job022/mask.mrc",
                maskcreate_job.OUTPUT_NODE_MASK,
                "MaskCreate/job022/",
                "Refine3D/job027/",
                None,
            ),
            (
                "Refine3D/job021/run_half1_class001_unfil.mrc",
                refine3D_job.OUTPUT_NODE_HALFMAP,
                "Refine3D/job021/",
                "PostProcess/job023/",
                None,
            ),
            (
                "Class3D/job018/run_it025_class001_box256.mrc",
                class3D_job.OUTPUT_NODE_MAP,
                "Class3D/job018/",
                "Refine3D/job021/",
                None,
            ),
            (
                "InitialModel/job017/run_it150_class001_symD2.mrc",
                initialmodel_job.OUTPUT_NODE_INIMODEL,
                "InitialModel/job017/",
                "Class3D/job018/",
                None,
            ),
        ]

        for line in zip(formatted, expected_list):
            for n, i in enumerate(line[0]):
                assert i == line[1][n], (i, line[1][n])

    def test_getting_upstream_network(self):
        """Just get the upstream network info as a list for other applications"""

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        pipeline = ProjectGraph(name="tutorial")
        pipeline.read()
        proc_name = pipeline.process_list[10]
        upstream_list = pipeline.get_upstream_network(proc_name)
        formatted = [
            (x[0].name, x[0].type, x[1].name, x[2].name) for x in upstream_list
        ]
        expected_list = [
            (
                "CtfFind/job003/micrographs_ctf.star",
                ctffind_job.OUTPUT_NODE_MICS,
                "CtfFind/job003/",
                "ManualPick/job004/",
                24,
            ),
            (
                "CtfFind/job003/micrographs_ctf.star",
                ctffind_job.OUTPUT_NODE_MICS,
                "CtfFind/job003/",
                "Extract/job007/",
                24,
            ),
            (
                "CtfFind/job003/micrographs_ctf.star",
                ctffind_job.OUTPUT_NODE_MICS,
                "CtfFind/job003/",
                "AutoPick/job011/",
                24,
            ),
            (
                "Select/job009/class_averages.star",
                select_job.OUTPUT_NODE_CLASSES,
                "Select/job009/",
                "AutoPick/job011/",
                None,
            ),
            (
                "MotionCorr/job002/corrected_micrographs.star",
                motioncorr_job.OUTPUT_NODE_MICS,
                "MotionCorr/job002/",
                "CtfFind/job003/",
                24,
            ),
            (
                "Class2D/job008/run_it025_optimiser.star",
                class2D_job.OUTPUT_NODE_OPT,
                "Class2D/job008/",
                "Select/job009/",
                None,
            ),
            (
                "Import/job001/movies.star",
                import_job.OUTPUT_NODE_MOVIES[0] + ".star.relion",
                "Import/job001/",
                "MotionCorr/job002/",
                24,
            ),
            (
                "Extract/job007/particles.star",
                extract_job.OUTPUT_NODE_PARTS,
                "Extract/job007/",
                "Class2D/job008/",
                1158,
            ),
            (
                "AutoPick/job006/coords_suffix_autopick.star",
                autopick_job.OUTPUT_NODE_COORDS,
                "AutoPick/job006/",
                "Extract/job007/",
                None,
            ),
            (
                "Select/job005/micrographs_selected.star",
                select_job.OUTPUT_NODE_MICS,
                "Select/job005/",
                "AutoPick/job006/",
                5,
            ),
            (
                "ManualPick/job004/coords_suffix_manualpick.star",
                manualpick_job.OUTPUT_NODE_COORDS,
                "ManualPick/job004/",
                "Select/job005/",
                None,
            ),
        ]

        for line in zip(formatted, expected_list):
            for n, i in enumerate(line[0]):
                assert i == line[1][n], (i, line[1][n])


if __name__ == "__main__":
    unittest.main()
