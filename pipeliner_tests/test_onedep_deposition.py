import os
import tempfile
import shutil
import unittest

from pipeliner_tests import test_data
from pipeliner.onedep_deposition import (
    SoftwareType,
    OtherType,
    PhaseReversalType,
    AmplitudeCorrectionType,
    CtfCorrectionType,
    ParticleSelectionType,
    RandomConicalTiltType,
    OrthogonalTiltType,
    EmdbType,
    PdbChainType,
    merge,
    expand_nt,
    PdbModelType,
    InSilicoStartupModelType,
    OtherStartupModelType,
    RandomConicalTiltStartupModelType,
    EmdbStartupModelType,
    PdbModelStartupModelType,
    ProjectionMatchingProcessingType,
    InitialAngleType,
    FinalAngleType,
    FinalMultiReferenceAlignmentType,
    Final2DClassificationType,
    Final3DClassificationType,
    DimensionsType,
    BackgroundMaskType,
    SpatialFilteringType,
    SharpeningType,
    BfSharpeningType,
    ReconstructionFilteringType,
    HelicalParamsType,
    SymmetryType,
    FinalReconstructionType,
)


class OneDepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_default_SoftwareType(self):
        sf = SoftwareType().check()
        assert len(sf) == 3
        assert sf._name == "software_type"
        for att in [
            sf.name,
            sf.version,
            sf.processing_details,
        ]:
            assert att is None

    def test_SoftwareType(self):
        name = "test software"
        version = "1.0"
        processing_details = "I did stuff"
        sf = SoftwareType(
            name=name, version=version, processing_details=processing_details
        ).check()
        assert sf._name == "software_type"
        assert sf.name == name
        assert sf.version == version
        assert sf.processing_details == processing_details

    def test_OtherType(self):
        software_list = ["prg1", "prg2"]
        details = "Some stuff"
        ot = OtherType(software_list=software_list, details=details).check()
        assert len(ot) == 2
        assert ot._name == "other"
        assert ot.software_list == software_list
        assert ot.details == details

    def test_defalt_OtherType(self):
        ot = OtherType().check()
        assert len(ot) == 2
        assert ot._name == "other"
        assert ot.software_list == []
        assert ot.details is None

    def test_default_PhaseReversalType(self):
        pr = PhaseReversalType().check()
        assert pr._name == "phase_reversal"
        assert pr.anisotropic is None
        assert pr.correction_space is None

    def test_PhaseReversalType(self):
        aniso = True
        corr_space = "real"
        pr = PhaseReversalType(anisotropic=aniso, correction_space=corr_space).check()
        assert pr._name == "phase_reversal"
        assert pr.anisotropic is aniso
        assert pr.correction_space is corr_space

    def test_PhaseReversalType_error(self):
        aniso = True
        corr_space = "magic"
        with self.assertRaises(ValueError):
            PhaseReversalType(anisotropic=aniso, correction_space=corr_space).check()

    def test_default_AmplitudeCorrectionType(self):
        at = AmplitudeCorrectionType().check()
        assert len(at) == 2
        assert at._name == "amplitude_correction"
        assert at.factor is None
        assert at.correction_space is None

    def test_AmplitudeCorrectionType(self):
        factor = 1.0
        corr_space = "reciprocal"
        at = AmplitudeCorrectionType(factor=factor, correction_space=corr_space).check()
        assert len(at) == 2
        assert at._name == "amplitude_correction"
        assert at.factor == factor
        assert at.correction_space is corr_space

    def test_AmplitudeCorrectionType_error(self):
        factor = 1.0
        corr_space = "upsidedown"
        with self.assertRaises(ValueError):
            AmplitudeCorrectionType(factor=factor, correction_space=corr_space).check()

    def test_default_CtfCorrectionType(self):
        cc = CtfCorrectionType().check()
        assert cc._name == "ctf_correction"
        assert len(cc) == 5
        for i in [
            cc.phase_reversal,
            cc.amplitude_correction,
            cc.correction_operation,
            cc.details,
        ]:
            assert i is None
        assert cc.software_list == []

    def test_CtfCorrectionType(self):
        pr = PhaseReversalType()
        ac = AmplitudeCorrectionType()
        co = "multiplication"
        sol = [SoftwareType(), SoftwareType()]
        details = "did some stuff"

        cc = CtfCorrectionType(
            phase_reversal=pr,
            amplitude_correction=ac,
            correction_operation=co,
            software_list=sol,
            details=details,
        ).check()
        assert len(cc) == 5
        assert cc.phase_reversal == pr
        assert cc.amplitude_correction == ac
        assert cc.correction_operation == co
        assert cc.details == details
        assert cc.software_list == sol

    def test_CtfCorrectionType_typeerrors(self):
        pr = "bad"
        ac = "worse"
        co = "not multiplication"
        sol = [SoftwareType(), "prg1"]
        details = "did some stuff"

        with self.assertRaises(ValueError):
            CtfCorrectionType(
                phase_reversal=pr,
                amplitude_correction=ac,
                correction_operation=co,
                software_list=sol,
                details=details,
            ).check()

    def test_default_ParticleSelectionType(self):
        ps = ParticleSelectionType().check()
        assert ps._name == "particle_selection"
        assert len(ps) == 5
        for i in [
            ps.number_selected,
            ps.reference_model,
            ps.method,
            ps.software,
            ps.details,
        ]:
            assert i is None

    def test_ParticleSelectionType(self):
        ns = 100000
        rm = "my model"
        meth = "the way I did it"
        software = SoftwareType()
        details = "them deets"
        ps = ParticleSelectionType(
            number_selected=ns,
            reference_model=rm,
            method=meth,
            software=software,
            details=details,
        ).check()
        assert len(ps) == 5
        assert ps.number_selected == ns
        assert ps.reference_model == rm
        assert ps.method == meth
        assert ps.software == software
        assert ps.details == details

    def test_ParticleSelectionType_error(self):
        ns = 100000
        rm = "my model"
        meth = "the way I did it"
        software = "wrong type!"
        details = "them deets"
        with self.assertRaises(ValueError):
            ParticleSelectionType(
                number_selected=ns,
                reference_model=rm,
                method=meth,
                software=software,
                details=details,
            ).check()

    def test_default_RandomConicalTiltType(self):
        rt = RandomConicalTiltType().check()
        assert rt._name == "random_conical_tilt"
        assert len(rt) == 4
        assert rt.software_list == []
        for i in [rt.number_images, rt.tilt_angle, rt.details]:
            assert i is None

    def test_RandomConicalTiltType(self):
        ni = 10
        ta = 76.8
        sl = [SoftwareType(), SoftwareType()]
        details = "som deets"

        rt = RandomConicalTiltType(
            number_images=ni,
            tilt_angle=ta,
            software_list=sl,
            details=details,
        ).check()

        assert rt._name == "random_conical_tilt"
        assert len(rt) == 4
        assert rt.software_list == sl
        assert rt.number_images == ni
        assert rt.tilt_angle == ta
        assert rt.details == details

    def test_RandomConicalTiltType_error(self):
        ni = 10
        ta = 76.8
        sl = [SoftwareType(), "BAD!!"]
        details = "som deets"

        with self.assertRaises(ValueError):
            RandomConicalTiltType(
                number_images=ni,
                tilt_angle=ta,
                software_list=sl,
                details=details,
            ).check()

    def test_default_OrthogonalTiltType(self):
        ot = OrthogonalTiltType().check()
        assert ot._name == "orthogonal_tilt"
        assert len(ot) == 5
        for i in [
            ot.number_images,
            ot.tilt_angle1,
            ot.tilt_angle2,
            ot.details,
        ]:
            assert i is None
        assert ot.software_list == []

    def test_OrthogonalTiltType(self):
        ni = 100
        ta1 = 180.0
        ta2 = 179.9
        sl = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        ot = OrthogonalTiltType(
            number_images=ni,
            tilt_angle1=ta1,
            tilt_angle2=ta2,
            software_list=sl,
            details=details,
        ).check()
        assert ot._name == "orthogonal_tilt"
        assert len(ot) == 5

        assert ot.number_images == ni
        assert ot.tilt_angle1 == ta1
        assert ot.tilt_angle2 == ta2
        assert ot.details == details
        assert ot.software_list == sl

    def test_OrthogonalTiltType_error(self):
        ni = 100
        ta1 = 180.0
        ta2 = 179.9
        sl = [SoftwareType(), "Nah!"]
        details = "deez deets"
        with self.assertRaises(ValueError):
            OrthogonalTiltType(
                number_images=ni,
                tilt_angle1=ta1,
                tilt_angle2=ta2,
                software_list=sl,
                details=details,
            ).check()

    def test_default_EmdbType(self):
        et = EmdbType().check()
        assert et._name == "emdb_id"
        assert len(et) == 1
        assert et.emdb_id is None

    def test_EmdbType(self):
        et = EmdbType(emdb_id="emdb-27636").check()
        assert et._name == "emdb_id"
        assert len(et) == 1
        assert et.emdb_id == "emdb-27636"

    # TODO: test regex validation of EmdbType when it is added

    def test_default_PdbChainType(self):
        pt = PdbChainType().check()
        assert pt._name == "chain_id_list"
        assert len(pt) == 2
        assert pt.chain_id is None
        assert pt.residue_range is None

    def test_PdbChainType(self):
        pt = PdbChainType(chain_id="A", residue_range="100-200").check()
        assert pt._name == "chain_id_list"
        assert len(pt) == 2
        assert pt.chain_id == "A"
        assert pt.residue_range == "100-200"

    def test_default_PdbModelType(self):
        pm = PdbModelType().check()
        assert pm._name == "pdb_model"
        assert len(pm) == 2
        assert pm.pdb_id is None
        assert pm.chain_id_list == []

    def test_PdbModelType(self):
        pdbid = "2G53G"
        chain_id = PdbChainType()
        pm = PdbModelType(pdb_id=pdbid, chain_id_list=[chain_id])
        assert pm._name == "pdb_model"
        assert len(pm) == 2
        assert pm.pdb_id == pdbid
        assert pm.chain_id_list == [chain_id]

    def test_PdbModelType_error_not_list(self):
        """chain_id_list must be list"""
        pdbid = "2G53G"
        chain_id = PdbChainType()
        with self.assertRaises(ValueError):
            PdbModelType(pdb_id=pdbid, chain_id_list=chain_id).check()

    def test_PdbModelType_error_wrong_itemtype(self):
        """chain_id_list must contain PdbChainType objects"""
        pdbid = "2G53G"
        chain_id = ["1", "2"]
        with self.assertRaises(ValueError):
            PdbModelType(pdb_id=pdbid, chain_id_list=chain_id).check()

    # TODO: add test for regex validation of PdbChainType when it is added

    def test_default_InSilicoStartupModelType(self):
        ist = InSilicoStartupModelType().check()
        assert ist._name == 'startup_model type_of_model="insilico"'
        assert len(ist) == 2
        assert ist.startup_model is None
        assert ist.details is None

    def test_InSilicoStartupModelType(self):
        model = "my_model.mrc"
        details = "deets"
        ist = InSilicoStartupModelType(startup_model=model, details=details).check()
        assert ist._name == 'startup_model type_of_model="insilico"'
        assert len(ist) == 2
        assert ist.startup_model == model
        assert ist.details == details

    def test_default_OtherStartupModelType(self):
        ist = OtherStartupModelType().check()
        assert ist._name == 'startup_model type_of_model="other"'
        assert len(ist) == 2
        assert ist.startup_model is None
        assert ist.details is None

    def test_OtherStartupModelType(self):
        model = "my_model.mrc"
        details = "deets"
        ist = OtherStartupModelType(startup_model=model, details=details).check()
        assert ist._name == 'startup_model type_of_model="other"'
        assert len(ist) == 2
        assert ist.startup_model == model
        assert ist.details == details

    def test_default_RandomConicalTiltStartupModelType(self):
        ist = RandomConicalTiltStartupModelType().check()
        assert ist._name == 'startup_model type_of_model="random_conical_tilt"'
        assert len(ist) == 2
        assert ist.startup_model is None
        assert ist.details is None

    def test_RandomConicalTiltStartupModelType(self):
        model = RandomConicalTiltType().check()
        details = "deets"
        ist = RandomConicalTiltStartupModelType(
            startup_model=model, details=details
        ).check()
        assert ist._name == 'startup_model type_of_model="random_conical_tilt"'
        assert len(ist) == 2
        assert ist.startup_model == model
        assert ist.details == details

    def test_RandomConicalTiltStartupModelType_error(self):
        """Startup model must be the correct type"""
        model = "BAD MODEL!!"
        details = "deets"
        with self.assertRaises(ValueError):
            RandomConicalTiltStartupModelType(
                startup_model=model, details=details
            ).check()

    def test_default_EmdbStartupModelType(self):
        ist = EmdbStartupModelType().check()
        assert ist._name == 'startup_model type_of_model="emdb_id"'
        assert len(ist) == 2
        assert ist.startup_model is None
        assert ist.details is None

    def test_EmdbStartupModelType(self):
        model = EmdbType()
        details = "deets"
        ist = EmdbStartupModelType(startup_model=model, details=details).check()
        assert ist._name == 'startup_model type_of_model="emdb_id"'
        assert len(ist) == 2
        assert ist.startup_model == model
        assert ist.details == details

    def test_EmdbStartupModelType_error(self):
        """Startup model must be the correct type"""
        model = "BAD MODEL!!"
        details = "deets"
        with self.assertRaises(ValueError):
            EmdbStartupModelType(startup_model=model, details=details).check()

    def test_default_PdbStartupModelType(self):
        ist = PdbModelStartupModelType().check()
        assert ist._name == 'startup_model type_of_model="pdb_model"'
        assert len(ist) == 2
        assert ist.startup_model is None
        assert ist.details is None

    def test_PdbStartupModelType(self):
        model = PdbModelType()
        details = "deets"
        ist = PdbModelStartupModelType(startup_model=model, details=details).check()
        assert ist._name == 'startup_model type_of_model="pdb_model"'
        assert len(ist) == 2
        assert ist.startup_model == model
        assert ist.details == details

    def test_PdbStartupModelType_error(self):
        """Startup model must be the correct type"""
        model = "BAD MODEL!!"
        details = "deets"
        with self.assertRaises(ValueError):
            PdbModelStartupModelType(startup_model=model, details=details).check()

    def test_default_ProjectionMatchingProcessingType(self):
        pmp = ProjectionMatchingProcessingType().check()
        assert pmp._name == "projection_matching_processing"
        assert len(pmp) == 3
        for i in (
            pmp.number_reference_projections,
            pmp.merit_function,
            pmp.angular_sampling,
        ):
            assert i is None

    def test_ProjectionMatchingProcessingType(self):
        nr = 100
        mf = "function"
        asmp = 2.4
        pmp = ProjectionMatchingProcessingType(
            number_reference_projections=nr, merit_function=mf, angular_sampling=asmp
        ).check()
        assert pmp.number_reference_projections == nr
        assert pmp.merit_function == mf
        assert pmp.angular_sampling == asmp
        assert pmp._name == "projection_matching_processing"

    def test_default_InitialAngleType(self):
        ia = InitialAngleType().check()
        assert ia._name == "initial_angle_assignment"
        assert len(ia) == 4
        assert ia.type is None
        assert ia.projection_matching_processing is None
        assert ia.software_list == []
        assert ia.details is None

    def test_InitialAngleType(self):
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        ia = InitialAngleType(
            type=atype,
            projection_matching_processing=pmp,
            software_list=sw,
            details=details,
        ).check()
        assert ia._name == "initial_angle_assignment"
        assert len(ia) == 4
        assert ia.type == atype
        assert ia.projection_matching_processing == pmp
        assert ia.software_list == sw
        assert ia.details == details

    def test_InitialAngleType_error_badatype(self):
        """Invalid option for initial angle assignment type"""
        atype = "INVALID CHOICE"
        pmp = ProjectionMatchingProcessingType()
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        with self.assertRaises(ValueError):
            InitialAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_InitialAngleType_error_badpmp(self):
        """wrong type of object for projection_matching_processing"""
        atype = "ANGULAR RECONSTITUTION"
        pmp = "INVALID"
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        with self.assertRaises(ValueError):
            InitialAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_InitialAngleType_error_badsoftware(self):
        """wrong type of objects in software list"""
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = ["bad", "sobad"]
        details = "deez deets"
        with self.assertRaises(ValueError):
            InitialAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_default_FinalAngleType(self):
        ia = FinalAngleType().check()
        assert ia._name == "final_angle_assignment"
        assert len(ia) == 4
        assert ia.type is None
        assert ia.projection_matching_processing is None
        assert ia.software_list == []
        assert ia.details is None

    def test_FinalAngleType(self):
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        ia = FinalAngleType(
            type=atype,
            projection_matching_processing=pmp,
            software_list=sw,
            details=details,
        ).check()
        assert ia._name == "final_angle_assignment"
        assert len(ia) == 4
        assert ia.type == atype, ia.type
        assert ia.projection_matching_processing == pmp
        assert ia.software_list == sw
        assert ia.details == details

    def test_FinalAngleType_error_badatype(self):
        """Invalid option for final angle assignment type"""
        atype = "INVALID CHOICE"
        pmp = ProjectionMatchingProcessingType()
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        with self.assertRaises(ValueError):
            FinalAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_FinalAngleType_error_badpmp(self):
        """wrong type of object for projection_matching_processing"""
        atype = "ANGULAR RECONSTITUTION"
        pmp = "INVALID"
        sw = [SoftwareType(), SoftwareType()]
        details = "deez deets"
        with self.assertRaises(ValueError):
            FinalAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_FinalAngleType_error_badsoftware(self):
        """wrong type of objects in software list"""
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = ["bad", "sobad"]
        details = "deez deets"
        with self.assertRaises(ValueError):
            FinalAngleType(
                type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_defaultFinalMultiReferenceAlignmentType(self):
        mrat = FinalMultiReferenceAlignmentType()
        assert mrat._name == "final_multi_reference_alignment"
        assert len(mrat) == 5
        for i in [
            mrat.number_reference_projections,
            mrat.merit_function,
            mrat.angular_sampling,
            mrat.details,
        ]:
            assert i is None
        assert mrat.software_list == []

    def test_FinalMultiReferenceAlignmentType(self):
        nrp = 420
        merit = "mymeritfunct"
        angs = 1.23
        details = "deetz"
        sf = [SoftwareType()]
        mrat = FinalMultiReferenceAlignmentType(
            number_reference_projections=nrp,
            merit_function=merit,
            angular_sampling=angs,
            details=details,
            software_list=sf,
        ).check()
        assert mrat._name == "final_multi_reference_alignment"
        assert len(mrat) == 5
        assert mrat.number_reference_projections == nrp
        assert mrat.merit_function == merit
        assert mrat.angular_sampling == angs
        assert mrat.details == details
        assert mrat.software_list == sf

    def test_default_Final2DClassificationType(self):
        td = Final2DClassificationType().check()
        assert td._name == "final_two_d_classification"
        assert len(td) == 4
        assert td.number_classes is None
        assert td.average_number_members_per_class is None
        assert td.software_list == []
        assert td.details is None

    def test_Final2DClassificationType(self):
        nc = 100
        anmc = 1234
        sf = [SoftwareType(), SoftwareType()]
        details = "test details"
        td = Final2DClassificationType(
            number_classes=nc,
            average_number_members_per_class=anmc,
            software_list=sf,
            details=details,
        ).check()
        assert td._name == "final_two_d_classification"
        assert len(td) == 4
        assert td.number_classes == nc
        assert td.average_number_members_per_class == anmc
        assert td.software_list == sf
        assert td.details is details

    def test_default_Final3DClassificationType(self):
        td = Final3DClassificationType().check()
        assert td._name == "final_three_d_classification"
        assert len(td) == 4
        assert td.number_classes is None
        assert td.average_number_members_per_class is None
        assert td.software_list == []
        assert td.details is None

    def test_Final3DClassificationType(self):
        nc = 100
        anmc = 1234
        sf = [SoftwareType(), SoftwareType()]
        details = "test details"
        td = Final3DClassificationType(
            number_classes=nc,
            average_number_members_per_class=anmc,
            software_list=sf,
            details=details,
        ).check()
        assert td._name == "final_three_d_classification"
        assert len(td) == 4
        assert td.number_classes == nc
        assert td.average_number_members_per_class == anmc
        assert td.software_list == sf
        assert td.details is details

    def test_default_DimensionsType(self):
        dt = DimensionsType().check()
        assert dt._name == "dimensions"
        assert len(dt) == 4
        for i in (dt.radius, dt.width, dt.height, dt.depth):
            assert i is None

    def test_DimensionsType(self):
        r, h, w, d = 1.0, 1.0, 1.0, 1.0
        dt = DimensionsType(radius=r, height=h, width=w, depth=d).check()
        assert dt._name == "dimensions"
        assert len(dt) == 4
        for i in (dt.radius, dt.width, dt.height, dt.depth):
            assert i == 1.0

    def test_default_BackgroundMaskType(self):
        bgm = BackgroundMaskType().check()
        assert bgm._name == "background_masked"
        assert len(bgm) == 4
        assert bgm.geometrical_shape is None
        assert bgm.dimensions is None
        assert bgm.software_list == []
        assert bgm.details is None

    def test_BackgroundMaskType(self):
        gs = "SPHERE"
        dims = DimensionsType().check()
        sf = [SoftwareType()]
        details = "deets"
        bgm = BackgroundMaskType(
            geometrical_shape=gs, dimensions=dims, software_list=sf, details=details
        ).check()
        assert bgm._name == "background_masked"
        assert len(bgm) == 4
        assert bgm.geometrical_shape == gs
        assert bgm.dimensions == dims
        assert bgm.software_list == sf
        assert bgm.details == details

    def test_BackgroundMaskType_error_shape(self):
        """error is geometrical_shape not in approved list"""
        gs = "ERROR"
        dims = DimensionsType().check()
        sf = [SoftwareType()]
        details = "deets"
        with self.assertRaises(ValueError):
            BackgroundMaskType(
                geometrical_shape=gs, dimensions=dims, software_list=sf, details=details
            ).check()

    def test_BackgroundMaskType_error_dims(self):
        """error if dimensions not DimensionsType object"""
        gs = "SPHERE"
        dims = "error"
        sf = [SoftwareType()]
        details = "deets"
        with self.assertRaises(ValueError):
            BackgroundMaskType(
                geometrical_shape=gs, dimensions=dims, software_list=sf, details=details
            ).check()

    def test_default_SpatialFilteringType(self):
        sf = SpatialFilteringType().check()
        assert sf._name == "spatial_filtering"
        assert len(sf) == 4
        assert sf.low_frequency_cutoff is None
        assert sf.high_frequency_cutoff is None
        assert sf.filter_function is None
        assert sf.software_list == []

    def test_SpatialFilteringType(self):
        lf = 10
        hf = 20
        funct = "function"
        st = [SoftwareType()]
        sf = SpatialFilteringType(
            low_frequency_cutoff=lf,
            high_frequency_cutoff=hf,
            filter_function=funct,
            software_list=st,
        ).check()
        assert sf._name == "spatial_filtering"
        assert len(sf) == 4
        assert sf.low_frequency_cutoff == lf
        assert sf.high_frequency_cutoff == hf
        assert sf.filter_function == funct
        assert sf.software_list == st

    def test_default_SharpeningType(self):
        st = SharpeningType().check()
        assert st._name == "sharpening"
        assert len(st) == 2
        assert st.details is None
        assert st.software_list == []

    def test_SharpeningType(self):
        details = "details"
        sl = [SoftwareType(), SoftwareType()]
        st = SharpeningType(software_list=sl, details=details).check()
        assert st._name == "sharpening"
        assert len(st) == 2
        assert st.details == details
        assert st.software_list == sl

    def test_default_BfSharpeningType(self):
        bft = BfSharpeningType().check()
        assert bft._name == "b-factorSharpening"
        assert len(bft) == 3
        assert bft.brestore is None
        assert bft.software_list == []
        assert bft.details is None

    def test_BfSharpeningType(self):
        b = 100.0
        details = "deetz"
        sl = [SoftwareType()]
        bft = BfSharpeningType(brestore=b, details=details, software_list=sl).check()
        assert bft._name == "b-factorSharpening"
        assert len(bft) == 3
        assert bft.brestore == b
        assert bft.software_list == sl
        assert bft.details is details

    def test_defeult_ReconstructionFilteringType(self):
        rft = ReconstructionFilteringType().check()
        assert rft._name == "reconstruction_filtering"
        assert len(rft) == 5
        assert rft.spatial_filtering is None
        assert rft.sharpening is None
        assert rft.background_masked is None
        assert rft.bfactorSharpening is None
        assert rft.other is None

    def test_ReconstructionFilteringType(self):
        sf = SpatialFilteringType()
        sh = SharpeningType()
        bf = BfSharpeningType()
        bm = BackgroundMaskType()
        o = OtherType()
        rft = ReconstructionFilteringType(
            spatial_filtering=sf,
            sharpening=sh,
            background_masked=bm,
            bfactorSharpening=bf,
            other=o,
        ).check()
        assert rft._name == "reconstruction_filtering"
        assert len(rft) == 5
        assert rft.spatial_filtering == sf
        assert rft.sharpening == sh
        assert rft.background_masked == bm
        assert rft.bfactorSharpening == bf
        assert rft.other == o

    def test_ReconstructionFilteringType_error_spatial(self):
        """Must be correct type"""
        sf = "Error"
        sh = SharpeningType()
        bf = BfSharpeningType()
        bm = BackgroundMaskType()
        o = OtherType()
        with self.assertRaises(ValueError):
            ReconstructionFilteringType(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorSharpening=bf,
                other=o,
            ).check()

    def test_ReconstructionFilteringType_error_sharpening(self):
        """Must be correct type"""
        sf = SpatialFilteringType()
        sh = "Error"
        bf = BfSharpeningType()
        bm = BackgroundMaskType()
        o = OtherType()
        with self.assertRaises(ValueError):
            ReconstructionFilteringType(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorSharpening=bf,
                other=o,
            ).check()

    def test_ReconstructionFilteringType_error_bf(self):
        """Must be correct type"""
        sf = SpatialFilteringType()
        sh = SharpeningType()
        bf = "Error"
        bm = BackgroundMaskType()
        o = OtherType()
        with self.assertRaises(ValueError):
            ReconstructionFilteringType(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorSharpening=bf,
                other=o,
            ).check()

    def test_ReconstructionFilteringType_error_mask(self):
        """Must be correct type"""
        sf = SpatialFilteringType()
        sh = SharpeningType()
        bf = BfSharpeningType()
        bm = "Error"
        o = OtherType()
        with self.assertRaises(ValueError):
            ReconstructionFilteringType(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorSharpening=bf,
                other=o,
            ).check()

    def test_ReconstructionFilteringType_other(self):
        """Must be correct type"""
        sf = SpatialFilteringType()
        sh = SharpeningType()
        bf = BfSharpeningType()
        bm = BackgroundMaskType()
        o = "Error"
        with self.assertRaises(ValueError):
            ReconstructionFilteringType(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorSharpening=bf,
                other=o,
            ).check()

    def test_default_HelicalParamsType(self):
        hp = HelicalParamsType().check()
        assert hp._name == "helical_parameters"
        assert len(hp) == 3
        assert hp.delta_z is None
        assert hp.delta_phi is None
        assert hp.axial_symmetry is None

    def test_HelicalParamsType(self):
        z = 2.5
        phi = 1.3
        ax = "sym"
        hp = HelicalParamsType(delta_z=z, delta_phi=phi, axial_symmetry=ax).check()
        assert hp._name == "helical_parameters"
        assert len(hp) == 3
        assert hp.delta_z == z
        assert hp.delta_phi == phi
        assert hp.axial_symmetry == ax

    def test_default_SymmetryType(self):
        st = SymmetryType().check()
        assert st._name == "applied_symmetry"
        assert len(st) == 3
        assert st.space_group is None
        assert st.point_group is None
        assert st.helical_parameters is None

    def test_SymmetryType(self):
        sg = 1
        pg = "d"
        hp = HelicalParamsType()
        st = SymmetryType(
            space_group=sg,
            point_group=pg,
            helical_parameters=hp,
        ).check()
        assert st._name == "applied_symmetry"
        assert len(st) == 3
        assert st.space_group == sg
        assert st.point_group == pg
        assert st.helical_parameters == hp

    # TO DO write the rest of these tests

    def test_expand_nt(self):
        rt = RandomConicalTiltType(
            number_images=10,
            tilt_angle=180.0,
            software_list=[SoftwareType(name="soft1", version="1.0")],
            details="test",
        )
        expanded_dict = {
            "number_images": 10,
            "tilt_angle": 180.0,
            "software_list": [
                {"name": "soft1", "version": "1.0", "processing_details": None}
            ],
            "details": "test",
        }

        rt_dict = expand_nt(rt)
        assert rt_dict == expanded_dict, rt_dict

    def test_default_FinalReconstructionType(self):
        fr = FinalReconstructionType().check()
        assert fr._name == "final_reconstruction"
        assert len(fr) == 6
        assert fr.number_of_classes_used is None
        assert fr.applied_symmetry is None
        assert fr.algorithm is None
        assert fr.resolution is None
        assert fr.resolution_method is None
        assert fr.reconstruction_filtering is None

    def test_FinalReconstructionType(self):
        nc = 100
        aps = SymmetryType()
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = ReconstructionFilteringType()
        fr = FinalReconstructionType(
            number_of_classes_used=nc,
            applied_symmetry=aps,
            algorithm=algo,
            resolution=reso,
            resolution_method=resomet,
            reconstruction_filtering=rf,
        ).check()
        assert fr._name == "final_reconstruction"
        assert len(fr) == 6
        assert fr.number_of_classes_used == nc
        assert fr.applied_symmetry == aps
        assert fr.algorithm == algo
        assert fr.resolution == reso
        assert fr.resolution_method == resomet
        assert fr.reconstruction_filtering == rf

    def test_FinalReconstructionType_error_sym(self):
        """value must be correct type"""
        nc = 100
        aps = "ERROR"
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = ReconstructionFilteringType()
        with self.assertRaises(ValueError):
            FinalReconstructionType(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_FinalReconstructionType_error_recalgo(self):
        """value must be in list of valid options"""
        nc = 100
        aps = SymmetryType()
        algo = "ERROR"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = ReconstructionFilteringType()
        with self.assertRaises(ValueError):
            FinalReconstructionType(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_FinalReconstructionType_error_res_method(self):
        """value must be in list of valid options"""
        nc = 100
        aps = SymmetryType()
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "ERROR!"
        rf = ReconstructionFilteringType()
        with self.assertRaises(ValueError):
            FinalReconstructionType(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_FinalReconstructionType_error_filtering(self):
        """value must be correct type"""
        nc = 100
        aps = SymmetryType()
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = "ERROR!"
        with self.assertRaises(ValueError):
            FinalReconstructionType(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_merge_nts_dont_duplicate_software(self):
        """number_images should be overwritten, tilt_angle should not,
        software list should not be duplicated"""
        rt1 = RandomConicalTiltType(
            number_images=10,
            tilt_angle=180.0,
            software_list=[SoftwareType(name="soft1", version="1.0")],
        )
        rt2 = RandomConicalTiltType(
            number_images=11,
            software_list=[SoftwareType(name="soft1", version="1.0")],
            details="test",
        )
        expdict = {
            "number_images": 11,
            "tilt_angle": 180.0,
            "software_list": [
                {"name": "soft1", "version": "1.0", "processing_details": None}
            ],
            "details": "test",
        }

        rt1_dict = expand_nt(rt1)
        rt2_dict = expand_nt(rt2)
        merged = merge(rt1_dict, rt2_dict)
        assert merged == expdict

    def test_merge_nts_merge_software(self):
        """number_images should be overwritten, tilt_angle should not,
        software list should be merged"""
        rt1 = RandomConicalTiltType(
            number_images=10,
            tilt_angle=180.0,
            software_list=[SoftwareType(name="soft1", version="1.0")],
        )
        rt2 = RandomConicalTiltType(
            number_images=11,
            software_list=[SoftwareType(name="soft2", version="2.0")],
            details="test",
        )
        expdict = {
            "number_images": 11,
            "tilt_angle": 180.0,
            "software_list": [
                {"name": "soft1", "version": "1.0", "processing_details": None},
                {"name": "soft2", "version": "2.0", "processing_details": None},
            ],
            "details": "test",
        }

        rt1_dict = expand_nt(rt1)
        rt2_dict = expand_nt(rt2)
        merged = merge(rt1_dict, rt2_dict)
        assert merged == expdict


if __name__ == "__main__":
    unittest.main()
