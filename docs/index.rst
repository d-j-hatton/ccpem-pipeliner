.. CCPEM-pipeliner documentation master file, created by
   sphinx-quickstart on Tue Jul  6 13:40:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============
CCPEM-pipeliner
===============

.. image:: source/header.png
  :width: 800
  :alt: ccpem-pipeliner header image

The CCPEM-pipeliner is an integrated suite of software tools for processing 
single particle cryoEM data, from preprocessing raw image data through building 
and fitting atomic models.

The pipeliner brings together a variety of 3rd party software in a single unified
framework for seamless integration of the different programs, along with tools
for management and analysis of the project.


General info and installation
-----------------------------
.. toctree::
   :maxdepth: 1

   README <source/readme_link>

Getting started
---------------

.. toctree::
   :maxdepth: 1

   source/getting_started

Pipeliner API
-------------
.. toctree::
   :maxdepth: 2

   source/pipeliner_api

Comand line tools
-----------------
.. toctree::
   :maxdepth: 1
   
   source/command_line_tools

Additional tools
----------------

.. toctree::
   :maxdepth: 1

   source/using_the_benchmark_tool

Core Modules 
------------
.. toctree::
   :maxdepth: 2

   source/core_modules_project_graph
   source/core_modules_job_runner
   source/star_file_utilities
   source/pipeliner_utilities

Pipeliner jobs and plugins
--------------------------
.. toctree::
   :maxdepth: 2
   
   source/pipeliner_jobs

.. toctree::
   :maxdepth: 1
   
   source/job_options

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
