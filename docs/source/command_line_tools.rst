==================
Command Line Tools
==================

CL_pipeline allows pipeliner functions to be run from the UNIX command line

.. argparse::
   :module: pipeliner.api.CL_pipeline
   :func: get_arguments
   :prog: CL_pipeline