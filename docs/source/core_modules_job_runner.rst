======================
Tools for Running Jobs
======================

The JobFactory and JobRunner create and run jobs respectively.  These
objects and the functions inside them generally do not need t be accessed
directly, usually the desired action can be done more easily through the
functions in the :doc:`pipeliner api<pipeliner_api>`

The JobRunner
-------------

.. automodule:: pipeliner.job_runner
    :members:
    :undoc-members:
    :show-inheritance:


Check_completion
----------------

.. automodule:: pipeliner.scripts.check_completion
    :members:
    :undoc-members:
    :show-inheritance:
    
The job factory
---------------

.. automodule:: pipeliner.job_factory
    :members:
    :undoc-members:
    :show-inheritance: