===================
CCPEM-Pipeliner API
===================

The pipeliner api provides access to all of the main functions of the pipeliner 

PipelinerProject
----------------

To interact with a pipeliner project it must be created as a 
:class:`~pipeliner.api.manage_project.PipelinerProject` object

.. automodule:: pipeliner.api.manage_project
    :members:
    :undoc-members:
    :show-inheritance:
    
api_utilities
-------------

Utility functions do not require an existing project

.. automodule:: pipeliner.api.api_utils
	:members:
	:undoc-members:
	:show-inheritance:
