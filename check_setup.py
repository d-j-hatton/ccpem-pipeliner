#!/usr/bin/env python

#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import sys

ersp = " " * 7
RED = "\033[91m"
GREEN = "\33[32m"
YELLOW = "\033[33m"
END = "\033[0m"
BLUEBG = "\33[44m"

try:
    from shutil import which
except ImportError:
    print(
        RED + "\nERROR" + END + ": Problem importing the `shutil.which` module."
        "\n{0}This is most likely due to using an older version of python"
        "\n{0}The Pipeliner is designed for python 3.8+\n{0}The current python "
        "version is: {1}\n".format(" " * 7, sys.version.split("\n")[0])
    )
    sys.exit(0)
try:
    from pipeliner.flowchart_illustration import ProcessFlowchart  # noqa: F401
except SyntaxError:
    print(
        RED + "\nERROR" + END + ": Problem importing the `__future__`annotations "
        "module.\n{0}This is most likely due to using an older version of python"
        "\n{0}The Pipeliner is designed for python 3.8+\n{0}The current python "
        "version is: {1}\n".format(" " * 7, sys.version.split("\n")[0])
    )

try:
    from pipeliner.api.api_utils import get_available_jobs
except ImportError:
    print(
        RED + "\nERROR" + END + ": Basic Pipeliner functionality could not be opened."
        " The pipeliner is not installed correctly."
    )

jobs = get_available_jobs()

pipeliner_programs = [
    "CL_pipeline",
    "pipeliner_job_create",
]

print("\n{0}\nCCPEM pipeliner setup check\n{0}\n".format("=" * 27))
print(BLUEBG + "-- checking python version --" + END)
print("Python vers: " + sys.version)

sys.stdout.write("\n" + BLUEBG + "-- checking available jobtypes --" + END)
sys.stdout.flush()
# check for relion in path
relion_errors = False

for job in jobs:
    sys.stdout.write("\n{}.{}.".format(job[0], "." * (50 - len(job[0]))))
    if job[1]:
        sys.stdout.write(GREEN + "Success" + END)
        sys.stdout.flush()
    else:
        sys.stdout.write(YELLOW + "Unavailable" + END)
        sys.stdout.flush()
        relion_errors = True
pyvers = float(sys.version.split()[0].rpartition(".")[0])


sys.stdout.write("\n\n" + BLUEBG + "-- checking pipeliner setup --" + END)
sys.stdout.flush
# check for pipeliner programs
pipe_errors = False
for pipex in pipeliner_programs:
    sys.stdout.write(
        "\n Looking for pipeliner programs: {}.{}.".format(
            pipex, "." * (20 - len(pipex))
        )
    )

    findit = which(pipex)
    if findit is not None:
        sys.stdout.write(GREEN + "Success" + END)
        sys.stdout.flush()
    else:
        sys.stdout.write(RED + "ERROR" + END)
        sys.stdout.flush()
        pipe_errors = True

print("")

if not pipe_errors and not relion_errors:
    msg = (
        GREEN + "SUCCESS: The pipeliner appears to be correctly installed and"
        " ready to use" + END
    )

else:
    if relion_errors:
        print(
            YELLOW + "\nWARNING" + END + ": Some jobtypes are unavailable\n"
            "{0}Make sure the executables for these jobs are installed\n{0}and "
            "available in your system path \n".format(" " * 9)
        )
        msg = "The pipeliner is correctly installed, but some jobs are unavailable"

    if pipe_errors:
        print(
            RED + "\nERROR" + END + ": There appears to be a problem with the "
            "installation of the pipeliner\n{}Could not find one or more of the "
            "pipeliner programs, try reinstalling".format(ersp)
        )
        print(
            RED
            + "SETUP FAILURE"
            + END
            + ": Problems were found with the pipeliner installation!"
        )
        msg = "The pipeliner is not installed correctly"


if pyvers < 3.9:
    print(
        "\n" + YELLOW + "WARNING" + END + ": Your python version is {0}\n{1}Some "
        "prototype functions require python 3.9 or higher\n{1}The basic "
        "pipeliner functions should work but some\n{1}advanced prototype features may "
        "behave strangely".format(sys.version.split()[0], " " * 9)
    )
if pyvers < 3.6:
    print(
        RED + "\nERROR" + END + ":\tYour python version is {}\n\tThe pipeliner "
        "requires python > 3.6"
    )

print("\n{0}\n{1}\n{0}\n".format("=" * len(msg), msg))
